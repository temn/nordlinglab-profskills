

This diary file is written by Angeline Wijaya E64095346 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* I get startled when I got into this course because it's my first time to get taught by a foreign professor, so cool to experience it
* I got to know what is bitbucket about and how to use it although I'm still a little confused, but I bellieve I will get used to it later
* In the videos that professor asked us to watch, one of the video title is “ How to seek the truth in the era of fake news” , It taught me that truth and wisdom cannot come just by the degree 

# 2021-10-07 #

* Quote of the day “trust yourself before trust other”
* I just meet my groupmates last week and now I feel sad to change groupmates again.
* I know more functions of money that I didn't have notice and think before. Beside for medium of exchange, there are unit of account and store of value which have diffrent meaning from each other.
* I find something funny for today course about the teacher question " how to print millions of ntd legally?" and the question is by taking a loan hahaha.

# 2021-10-14 #

* I don't know much about finance, but after the presentations of several groups, I got to undestand more aout it.
* Things that caught my attention is the video of Ted's talks about the descent into America's-neo Nazi movement, the sentence "Hatred is born of ignorance, fear is its father and isolation is it's mother. I don't know why but it related so much with my daily life problems.
* I have something curious about the statement that said facism is different with the nationalism, because from what I disgest, facism is one of the way for a person or people that believe in it to show their nationalism.

# 2021-10-21 #

* Actually ,it feels so good when there's a teacher who cares about our mental condition.
* I know more about what the neurosciene study is.
* whenever we were doing exercises, it was actually changing our brain without us knowing it. 
* Our physiology determines feeling; emotion is interpretation of feeling; emotion has to do with cognitive processes, thinking. And last our thinking influenced most of our daily performances.

# 2021-10-28 #

* We did a supergroup discussion today but our group was so silence, as a result we were late for our presentation today
* I've been looking forward to today course because I would like to see the old of me from the perspective of new of me
* From my opinion I agree that those who suffer from depression actually didn't lose their desire to connect with people, but they lose the ability. 
* In my past society, I used to not to care anything about my friendship because it's fine through the year.But when I arrived here, society hits diffrent. You can't really trust people easily.
* I can relate that if there's your friend who feel depressed or stress, all they need is someone who is willing to listen all of their worriness.
* Most of the people that were thinking suicide,all they know are they wouldn't hurt another person. They just want their own pain to end. But for me it's not really true to think like that. You didn't know that in this life there's still someone who adore and love you so much, that you wouldn't notice  you will acidentally hurt them if you commit suicide.
* Being strong like somebody who have no feeling is killing us, and having feelings isn't a sign of weakness. Feelings mean we're human. When we happy we smile and laugh and we were sad we cry. We're human, we are free to expressed our own feelng.
* Actually life is beautiful if we enjoy it. Although sometimes it's messy, and unpredictable. But "everything will be okay" when you have somebody who support you.
* If anything is going hard, ask for help. We're all human and we know that humans as social beings mean we cannot live alone. If humans do not relate or interact with other human beings, then that person cannot be said to be human. 
* It's great lecture to understand.

# 2021-11-04 #

* I'm really grateful could listen both of our classmate story about depression, it's such a pleasure and great experience for me who can listen to the awesome and special people like them.
* I'm still a little confused about the purpose of the manga x diary and how to use it actually.
* For me, living in a good environment is immportant, but if you can survive in a bad environment , then you are awesome.
* From my opinion,  the most important thing to live a healthier life is the mindset of ourselves, we should have a great mindset like how we think for every problem we faced in our daily life. It's really up to us to think the problem we faced in a positive way or negative way. If we want to think it in a positive way, then I believe we can get more positive response better than we think it in a negative way.

# 2021-11-11 #

* All of the presentaions of today supergroup disussed about many problems that happened in our soicety in this era.
* In the presentation of the 1st group I realized that I have a sleep inconsistency where I don't have enough of sleep time during weekdays and I paid it by oversleeping during the weekend that actually resulting the lack of concentration during learning.
* From my point of view , I know that noise pollution really disturb our concentration when doing something, but in here I just know that noise pollution is a serious problem where it is also unconsciously may affect our mental health condition.
* I really love the story about the Martin Luther King where he led a movement of non-violent, peaceful protests to fight racial injustice in the United States in the 1955 year. I love this inspirational story because in the really early year like that he could think so deep and pay attention to this problem where not all of the people nowdays may pay attention to it and still being racist.


# 2021-11-25 #

* Today's the first physical class through this semester
* I'm startled that the teacher is so tall. hahaha
* "We cannot have a society in which if two people wish to communicate the only way that can happen is if it's financed by a third person who wishes to manipulate them."  - Jaron
* A society that uses a behavior modification scheme will fail because they won't be aware of real world problems they need to solve” - Norbert Weiner. this quotes is really relate this world problem these days.
* From the millenial in workplace video,  i feel like almost all of what he mentioned other than the narcisism is just a result of bad parenting... i know plenty of millenials who where raised properly and dont have these negative traits. 
* Algorithms don't make things fair, but they repeat our past practices and our patterns. They automate the status quo.That would be great if we had a perfect world, but but we don't here.

# 2021-12-02 #

* Today the teacher ask us to hand our phone and I search my phone during the three hour class for about 10 times, and it felt like something is missing hahahha
* Our group is a little sad because we have no chance to do the presentation that we have prepared
* On the video of how to choose your news by Damon Brown, it let us know that we must search for the truth of a news before directly believe them because the amount of false news out there is overwhelming. 
* Also, the professor give us the example of two different country news that has the same news topic  (title) but different story in there where it might be so dangerous if it was a sensitive news and may caused misunderstanding between countries.

# 2021-12-09 #

* Today, I didn't attend the physical class due to the flu that I caught suddenly.
* I found it a little bit hard to understand the lecture and interact more with the other students while attending online class
* Our group had presentation today but I'm not there and i realized it's really dangerous to be wrongly understand about a news that may cause an unintentional misunderstanding between people.
* From this video " Let the environment guide our development" by Johan Rockstrom  I realized that system changes is an important issue. If people continue like they usually do, endlessly consuming and creating masses of waste without thinking about stability, sustainablity and efficency, as a result the ecosystems resilience will shut down completely. 

# 2021-12-16 #

* Today presentations method is a bit unique where all of the presenters were told to sit infront and queue for their turn
* There's also debate session today where the audience is given chance to ask something that they were curious about and I think it is good choice.
* From all of the group the idea I agree the most is the idea of the first group "change textbooks into PDFs read on tablets" because it seems that, this idea tend to be more real(visible) than the other idea.
* Today I just meet my supergroup members. It's the first time that my group has so many members. And all of my groupmate are very cool

# 2021-12-23
* Today's debate session is so funny and exciting 
* I'm doing my first presentation offline in this course and I'm trembling when doing it
* Today is a successful day and productive day for me.
* I felt successful because today I succes for not spending any money for eating , otherwise I also work part time in a small restaurant which means I get salary and food there:)
* For the next day, I'll also be working in my another part time job and hope to not spend a limit money that I set in my usual day.

# 2021-12-24
* Today is a productive day but not too successful day
* i felt productive because I have no time to rest and I don't feel succcessful beause I failed to do my homework project due to my tired condition
* For the next day, my goal is get my homework done to feel successful

# 2021-12-25
* Today is not a productive day also not a successful day
* I don't feel productive because the whole day I was at my room doing homework and I fell not successful because my homework haven't done yet:(
* For the next day, I will work at my part time job working to be more productive and finishing all my homework. 

# 2021-12-26
* Today is a productive day but not a successful day also
* Same as before, I worked for the whole day and I feel so prductive today but I forgot to do another homework that I had
* For the next day, I'm planning of working half day and finish all my homework again.

# 2021-12-27
* Today is not a really productive day and also not a successful day
* Beacause today I felt sick and I just do little things that I can manage to do , and I also don't feel very successful because I can't do much homework.
* For the next day, I hope can finished all my homework and to have more successful day

# 2021-12-28
* It's a productive day but not a successful day
* I have done exercise today but I eat too much in the night that lost my healthy diet again.
* For the next day, I will have my part time job and don't eat too much to have more healthy diet.

# 2021-12-29
* It's a productive day and successful day 
* I have done my part time job today and I have done all my homework so nothing serious happened and I just have my normal day
* For the next day, I want to wake up early in the morning and have my healthy diet to have a healthy body.

# 2021-12-30
* I cannot attend the class both online and offline today
* I cannot attend the offline class because I caught a flu and diarrhea at the same time
* And I cannot attend the online class because my dorm internet cable snapped and my smartphone internet is suck too, and I cannot go out from my dorm:), so I wait until my roomate ome and lend from them but it's already too late and when I try to join the meeting , I can't get in anymore:(
* So for today's course I asked my groupmate and my senior that have attended today's class about the content today.

* FIVE RULES FOR SUCCESS
* 1. Make a list of your goals
* 2. Don't procrastinate
* 3. Learn to time manage
* 4. Wake up early in the morning and do exercise
* 5. Believe in yourself

# 2022-01-06
* Today , the professor invite a special guest from Vietnam to talk her experience for how to attract people in her sandwich business in the past year, and all of it was from small step that ususally everybody can do.
* For me the guest sound a little funny because when it's started, the business they do suddenly booming up and they had to stop because can't handle all of it. But it's understanable for doing the business in term exam period.
* Professor played a Ted talk video that talk about how to attract people on the purpose of promoting some event or project we are doing. 
* Our group failed to find a store that would donate a phone to us.
* Gender inequality is still one of the most serious problem these day.
* The professor son is so cute:) 