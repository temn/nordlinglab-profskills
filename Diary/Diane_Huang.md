# 2023-09-07 #

* The first lecture let us know the details of this course and what to do in the following classes.
* I am really inspired and puzzled by the exponential growth.
* After watching the video in the class, I started to think about what our lives will be like in the world of abundance.
* In the following days, I discussed my thought with my group member and put our ideas into the presentation.

# 2023-09-14 #

* The second lecture talked about the difference of conflict views between harmony view and conflict view.
* The different conflict behavier can be seen in the chart.
* We can judge our own behavior based on whether we are trying to satisfy one's own concerns or attempting to satisfy others' concerns.I
* Fight, avoid, dampen, collaboration, and compromise are five different conflict behavior.
* Professor gave ous some interesting examples of glasl conflict escalation model.

# 2023-09-21 #

* The third lecture started with creating our diary file. 
* We esplore the data of the SDG closest to the heart of each group member in the Goalkeepers report.
* We watched three different video regarding to statistics, objectivity, and fake news.
* We also learned how to cite websites.

# HW1:Summary of what I think my future would be like after 10 years #
* After graduating from NCKU with a master's degree in machanical engineering, I started working as an engineer in a foreign company in Taichung. No shifts, but working from 9am to 5pm Monday to Friday. Every moring, I wake up at 7 o'clock every morning, and eat toasted bread and coffee for breakfast. During work time, I try my best to deal will every task and problem without idea of fishing in troubled waters. After a whole day of hardworking, I drive to home to eat dinner with my family. The time after dinner is my personal time, I can take a walk at the parks near home, reading some books, and watching some dramas. After taking a shower, it's my rest time. I can do whatever I want in this period of time. The daily schedule during the working day is probably like this. On the weekend, I have more time to spend with myself. I can go to supermarket to buy some ingredient to cook, and go to the cafe and read some books. I achieve a good balance between work and life. In additional to giving myself a good rest, I also hope to make good use of holidays and off-duty time to learn additional expertise. There are five trends shaping my future.
* 1. AI Integration: The integration of AI into every aspect of our lives will continue to accelerate. While AI will enhance efficiency and productivity, there will also be ongoing discussions about ethical considerations and job displacement.
* 2. Sustainability and Climate Action: Environmental concerns will be at the forefront of global agendas. In my daily life, I will make conscious choices to reduce my carbon footprint, such as using electric or hydrogen-powered vehicles and supporting eco-conscious products and services.
* 3. Remote Collaboration and Flexible Work: Remote collaboration will remain a prevalent mode of working, allowing me to collaborate with colleagues and experts from around the world seamlessly. This flexibility will empower me to balance work with personal pursuits and family life effectively.
* 4. Healthcare Transformation: Advanced AI systems will revolutionize diagnostics, treatment planning, and drug discovery. Personalized medicine will be the norm, with treatments tailored to an individual's unique genetic makeup.
* 5. Digital Privacy and Security: As more of our lives become digitized, concerns about data privacy and security will intensify. I will take proactive measures to protect my digital identity and advocate for robust data protection regulations. My work will involve developing AI systems that prioritize data privacy and minimize vulnerabilities to cyber threats.

# 2023-10-12 #

* The topic of this week is "money".
* We are asked to answer a form of questions about finance.
* There were lots of question that I don't know how to reply.
* This is probably also because I am not familiar with this field and I start to feel sleepy while watching the video.
* It is interesting to know the origin of money nowadays.
* I think the last video the professor showde us is more understandable because it used so many picture to describe the concept.
* I also think that we can buy less things with the same amount of money because the price of everything in Taiwan is becoming more expensive nowadays.

# 2023-10-19 #

* This week's topic is about why extremism flourish.
* The factors leading to violent extremism include: individual backgrounds and motivation; collective grievances and victimization stemming from domination, oppression, subjugation or foreign intervention; distortion and misuse of beliefs, political ideologies and ethnic and cultural differences; and leadership and social networks.
* What are the impacts of extremism?  It undermines peace and security, human rights and sustainable development.
* The TED talk vedio played in the class were great and helpful to let me understand the concept of fascism and how does it affect our mind.
* By knowing the idea of extremist and fascist, we can better understand the reason why they join the group and what they hope for.

# 2023-10-26 #

* today's class was really relatable since I have many problems with anxiety
* Alan watkins gave a perfect explaination on how our body is direct related to our behaviour
* It was already in my knowledge that exercise have many benefits to our mental health
* the last ted talk that we watched was quite boring

# 2023-11-02 #

* today we had our first super group presentation, I panicked a bit on how many members we had and how we would organize everything but in the end we did alright
* depression is a very heavy topic to discuss, we watch many stories about people that had experienced it in different perspectives
* ometimes just listening to what a people want to say might mean much more than you can imagine
* after listening all these stories, it left a bad taste in my mouth, but I also agree it's a topic that must be discussed our new projects are hard

# 2023-11-09 #

* some people shared their experiences with depression and anxiety, to be honest it was a pretty relatable moment
* anxiety has a really heavy impact on college students, I got curious on how english speakers would have to do if they wanted pychologist support
* find purpose on life was already something I was aware, and I still chasing for one until this day, but it really is not a easy task
* we sit with our big group together today, we discuss about our big project next week

# 2023-11-16 #
* This week was our big group's project. I heared a lot of interesting topic from other groups, and also learned some good ways to present our presentation.
* Out of all the the topic, the presentation about the improtance of sleep really impresed me. I always stay up late when I have a lot od homework and exams. However this is not good for my health and productivity.
* When we are sleeping, our brain is connecting different information and knowledge we learned during all day. If we don't let our brain to work, our effort may dissapear with time.
* This week we do our project too late, so we have to racing with time before the deadline. To prevent the same thing happen again, we decide to prepare it earlier. And we will have our first meeting this wednesday evening in the discussion room.
* Afer about two week of rest, we will have a lot of exams in the following three weeks. Hope that I can prepare all the exam subject as scheduled, and have enough sleep during the midterm period. I believe that I can conquer all the exam.

# 2023-11-23 #
* Today I listen to a lot of presentattion. However I was too tired and could not help but fall asleep because I had too many exams this week, which made me could not have enough sleep hours.
* I met my teammate this wednesday night at school's discussion space. We came to discuss the big project and the presentation for next week.
* In the beginning we came up with a lot of different ideas, then we just find that we had to combine the topic we choosed several weeks ago.
* A society that uses a behavior modification scheme will fail because they won't be aware of real world problems they need to solve� - Norbert Weiner. this quotes is really relate this world problem these days.

# 2023-11-30 #
* Today's lecture is mainly the large groups' presentation.
* The presentation is to show each group's three different actions ragarding to their relative topic.
* The only regret is that no time to listen to all the reports of each group, because the class time is not enough. Each group's report is very interesting, I really want to listen to every report.
* I have a lot of homework next week, and I have to interview two friends about their views on peer support group. I am very much looking forward to the course next week.
* We were asked to put our phones on the table in front of the classroom and to count the number of times we missed our phones or checked our phones. I did not check my phone at all nor miss it so it might be counted as 0 I guess. I even almost forgot to take my phone back after class.
* Each of us is just a small citizen in this city, but as long as we are united and speak for ourselves, there's still a chance to change the city. This is a democratic society, to win the votes, I believe that the government should listen to our voices.

# 2023-12-7 #
* Today we had a discussion together with different group about the points of view of different news media on a particular news.
* The professor talked about plametary boundaries which I still super confused about.
* After the class, I researched this concept on the Internet. It said that planetary boundaries are a framework to describe limits to the impacts of human activities on the Earth system. Beyond these limits, the environment may not be able to self-regulate anymore. This would mean the Earth system would leave the period of stability of the Holocene, in which human society developed.
* The normative component of the framework is that human societies have been able to thrive under the comparatively stable climatic and ecological conditions of the Holocene.
* I have never compared the differences between newspaper from two different countries reporting the same incident. And it's quite interesting to know how these media affecting the readers unknowingly with their own views.
 
# 2023-12-14 #
* This week's lecture is mainly the presentations of the large groups. It was nice to see the ideas of other groups.
* I like the format to vote for the best idea. It makes the class more interactive, I think it should happen more often.
* We need to invest a lot of time and interest in this project. It's interesting but I'm scared with the coming of the finals exams that I will be a bit short in time and uncapable to conscare the time that I initially wnated to do on the project.
* We are required to watch a roughly one hour and 45 minutes video this week. The presenation we need to do next week will need the imforamtion in this video. I think that the video is way too long. Recently, I am really stressed with everything I need to do. I think I am a little overwhelmed. I don't understand why are there so many thing to do even though it is currently not exam week. Luckly, this semester is about to end. There is only a few weeks end. It may be a torture in the last few week but I am still happy that the winter break is coming.
 
# 2023-12-21 #
* Today's topic is about industrial revolution and how to suceed.
* And we were asked to write the diary everyday.
 
# 2023-12-22 Friday #
* Unseccessful and unproductive
* I woke up very late today because I don't have class this morning. Before going to the class, I ate lunch with my friend. However I slept whole night and do nothing.
* I should spend more time on studying tomorrow because I not only have to prepare for the final, but also have to discuss the self-control topic with my classmates.
 
# 2023-12-23 Saturday #
* Successful and productive
* I woke up at around 10 a.m. this morning. Because I have mechanical design final next Monday and Wednesday, I study hard this morning. After eating lunch with my friend, we went to library to study. We just studied the until 9 p.m., and we also went to theater to watch Aquaman. I really like the movie although I did not see the season 1.
* Hope I can keep in the same pace in studying tomorrow.
 
# 2023-12-24 Sunday #
* Successful and unproductive 
* I did a lot of exercises for the final exam tomorrow. But I was in very poor efficiency.
* Hope I can get a good grade tomorrow.
 
# 2023-12-25 Monday #
* Unsecessful and unproductive
* I took the mechanical design final this morning. I thought that I did my best to prepare but the result seen to be not that good for me. (Accepting the failure will make you happier)
* I was sick today so I did nothing for the whole day. Hope that I can get my energy back tomorrow.
 
# 2023-12-26 Tuesday #
* Unsucessful and unproducitve
* Today marked the beginning of my intense preparation for the upcoming finals. I started by creating a detailed study schedule, outlining specific topics to cover each day. However, I soon realized that my schedule was overly ambitious, and I struggled to keep up.
* Hope I can get a good grade tomorrow
 
# 2023-12-27 Wednesday #
* Successful and productive
* These days were characterized by long hours spent cramming information. While I managed to cover a significant amount of material, I could feel the toll it took on my mental and physical well-being. Lack of breaks and sleep left me fatigued and less productive.
* I need to keep the condition.
 
# 2023-12-28 Thursday #
* Successful and unproductive
* I decided to reassess my study plan and make it more realistic. It was essential to prioritize topics based on their weight in the exams and my understanding of them. I also incorporated short breaks to recharge my mind and body.
 
# Five Rules for my Success and Productivity #
* Prioritization and Planning: The first step to efficiency is setting clear priorities. I create a to-do list and prioritize tasks based on their importance and deadlines. Planning my day in advance allows me to stay focused and ensures that I allocate time to critical activities.
* Time Blocking: I use the time-blocking technique to allocate specific time periods for different tasks. This helps prevent multitasking and allows me to concentrate fully on one task at a time. Knowing that I have a dedicated time slot for each activity enhances my productivity.
* Breaks and Refreshers: Breaks are essential for maintaining focus and preventing burnout. I incorporate short breaks between tasks to recharge my mind. Engaging in activities like stretching, taking a short walk, or practicing mindfulness helps me return to tasks with renewed energy and concentration.
* Technology Tools: Leveraging productivity tools and apps has been a game-changer. I use calendar apps for scheduling, task management tools for organizing to-dos, and note-taking apps for quick ideas. These tools streamline processes and keep me organized, allowing me to work more efficiently.
* Continuous Learning and Improvement: I believe in the power of continuous improvement. Regularly assessing my workflow, identifying bottlenecks, and seeking more efficient methods contribute to long-term productivity. Embracing new techniques, staying open to feedback, and adapting to changing circumstances are key elements in this process.
 
# 2023-12-28 #
* Today is also mainly about big group's presentation. Because I spent too much time on the electrical control project, I don't have enough time to prepare for this week's project.
* We watched two different video during class. First one is "This farm of the future uses no soil and 95% less water". And the second one is "What if we replaced politicians with randomly selected people". 

# 2023-12-28 Thursday #
* Successful and productive
* We finally present our dual rotor project on the automatic control course. We tried to make it better from 1 p.m. to 11 p.m. yesterday in TA's lab. Besides, I am responsible for presenting the hardware part of our project, which made me feel really nervous. Luckily we passed it! And I finally will have more time to prepare for the final exam.
* Hope I can get on the track as soon as possible.

# 2023-12-29 Friday#
* Unsuccessful and unproductive
* With the adjusted plan, I felt more in control and focused. I diversified my study methods, incorporating active learning techniques such as flashcards, summaries, and teaching concepts to others. This approach not only enhanced my understanding but also made the study sessions more engaging.
* Hope I can get on the track as soon as possible.

# 2023-12-30 Saturday #
* Successful and productive
* As the pressure increased, moments of self-doubt surfaced. I found myself questioning if I had done enough and if my efforts would yield positive results. It was crucial to remind myself of the progress made and to maintain a positive mindset.
* Fighting!!! I believe I can conquer the final exam.

# 2023-12-31 Sunday #
* Unsuccessful and unproductive
* These days were characterized by long hours spent cramming information. While I managed to cover a significant amount of material, I could feel the toll it took on my mental and physical well-being. Lack of breaks and sleep left me fatigued and less productive.
* I decided to reassess my study plan and make it more realistic. It was essential to prioritize topics based on their weight in the exams and my understanding of them. I also incorporated short breaks to recharge my mind and body.
* Hope that I will have better study condition tomorrow.

# 2024-1-1 Monday #
* Unsuccessful and unproductive
* I decided to reassess my study plan and make it more realistic. It was essential to prioritize topics based on their weight in the exams and my understanding of them. I also incorporated short breaks to recharge my mind and body.
* Looking back, I realize the importance of balance in the preparation process. While it's crucial to cover the material thoroughly, it's equally important to prioritize self-care to maintain peak mental and physical condition. As I approach the exams, I carry with me the lessons learned during these 10 days, knowing that I've put in my best effort.
* Here's to hoping that my hard work pays off in the end.

# 2023-1-2 Tuesday #
* Unsuccessful and productive
* I kicked off with a comprehensive review of the syllabus, identifying key areas that require focused attention. The realization of the upcoming challenges has motivated me to stay disciplined and dedicated to the study plan ahead.
* These days have been a whirlwind of textbooks, notes, and practice papers. I've immersed myself in a marathon of studying, covering a wide range of topics. While the workload is intense, I can sense a growing confidence in my understanding of the materials. However, I also remind myself to take short breaks to avoid burnout.
* Hope I will get better grade tomorrow.

# 2023-1-3 Wednesday #
* Successful and unproductive
* As the pressure mounted, I introduced variety into my study routine. From group discussions to teaching concepts to imaginary students, I explored diverse methods to reinforce my knowledge. These days emphasized the importance of active learning and keeping the study sessions engaging.
* With the exams on the horizon, anxiety crept in. I revisited key concepts and practiced time management for each section. A sense of accomplishment mingled with nervous anticipation as I visualized the exam setting. The support from friends and family played a crucial role in boosting my confidence.
* The eve of the exams has arrived. Today, I dedicated my time to a final review, focusing on the most critical topics. Emotions fluctuate between excitement and nervousness. I take solace in knowing that I've given my best effort, and now it's time to trust in my preparation.
* Wish I can have a good grade tomorrow.



