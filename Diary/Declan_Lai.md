This diary file is written by Declan Lai E14091025 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #

* The first lecture was great.
* While writing the quiz, I realized many interesting things.
* I don't know that tiger and southern white rhino are no longer endangered species.

# 2022-09-15 #

* It's the second lecture.
* I'm getting more and more confused as the course progressed.
* Many things troubled me, for example, I don't have money:( , I don't know how to use Bitbucket, and even don't know what homework should I do.

# 2022-09-22 #

* The third lecture.
* Starting to know what to do through the QA at the beginning of the class.

# 2022-09-29 #

* Today's lecture is about economic.
* My aunt was major in economic, therefore I was familiar with today's topic.
* It's easy for me.

# 2022-10-06 #

* Today's lecture mentioned about Fascist.
* I know it from history class and Peaky Blinders.

# 2022-10-13 #

* I think knowledge is everything we learn from anything no matter it's good or bad.
* When professor asked me the question, I was struggling to come up with the answer, but I don't want the other student to wait.
* Because of that, I feel anxious and I can even hardly to focus.
* I think I should take it easy next time.

# 2022-10-27 #

* Today's lecture is about depression.
* Due to the exam week, I often feel anxiety.

# 2022-11-10 #

* Our team project's three action ideas are too hard to do.
* We need to think a new one.
* It's pretty hard.

# 2022-11-17 #

* The most surprising thing in today's lecture to me is the destruction that technology can have.
* I also consider that the Internet are not good for kids. 

# 2022-11-24 #

* We were shown a video that demonstrated the similarities between Rome and America.
* Making a rule that everyone accepts is never easy; there is always a long way to go.

# 2022-12-01 #

* My Dad has a business trip to Tainan, and he had a presentation at NCKU so he visits me by the way.
* I had two midterm exams this week, on Monday and Tuesday, and I spent hours studying for those subjects.

# 2022-12-08 #

* Today I read a book by my favorite author, Guy de Maupassant.
* We got to vote on which social foundations and planetary boundaries we thought were the most important.

# 2022-12-15 #

* A. successful and productive
* B. I received a perfect score on today's class quiz.
* C. I'm feeling proud of myself.

# 2022-12-16 #

* A. unsuccessful and unproductive
* B. It was so cold that I overslept this morning and didn't go to class.
* C. I need to set an alarm and stop being lazy.

# 2022-12-17 #

* A. successful and productive
* B. Today is Saturday, and I get up early and study a lot.
* C. I'm in a good mood.

# 2022-12-18 #

* A. successful and productive
* B. I manage to keep up with yesterday progress and spent quite some times doing it.
* C. I should keep it up.

# 2022-12-19 #

* A. successful and productive
* B. I considered myself successful because I got up early this morning, ate a delicious breakfast, and studied for my final exam.
* C. I will make an effort to wake up early on a regular basis.

# 2022-12-20 #

* A. unsuccessful and unproductive
* B. I stayed up too late last night and nearly missed my alarm this morning. I didn't have enough time to plan my outfit, and I forgot to bring the materials for today's class.
* C. I should have completed the day's tasks earlier rather than waiting until bedtime.

# 2022-12-21 #

* 5 rules to be more successful and productive :
* Make a to do list. Keeping a daily to-do list can help you improve your overall memory by reinforcing your short-term memory. You may retain more information if you write down tasks and short-term goals and constantly check the list.
* Set small goals for the tasks. The scope of each new project or assignment may appear to be too large. However, once you start breaking it down and realizing what is possible, you'll notice how each component builds on the previous one.
* Concentrate on one goal at a time. Multitasking takes far more time than focusing on one task at a time. Furthermore, focusing on one thing at a time allows you to think clearly, creatively, become more solution-oriented, and become more pensive overall with your task.
* Take breaks to recharge. Build in breaks to avoid forgetting or ignoring the need. Breaks are scheduled at specific intervals in good time-management methods like the Pomodoro Technique. You can also schedule breaks, such as lunch or an afternoon walk. It doesn't matter when or for how long you take a break as long as you do it on a regular basis.
* Get enough sleep. If you've ever had a good night's sleep, you'll remember feeling refreshed and well-rested the next day. It's the polar opposite of the foggy-headed gloom that can result from a late night or insufficient sleep. Sleep is important not only for repairing your body, but also for refreshing your mind, improving memory, and strengthening problem-solving skills—all of which improve your work performance significantly. A good night's sleep is essential for being as productive as possible.

# 2022-12-22 #

* We had to share our thoughts on how to be more successful and productive today.
* We also watched a few TED talks.

# 2022-12-23 #

* A. successful and productive
* B. I arrived in time for the train to Taichung.
* C. It went so well because I left early today.

# 2022-12-24 #

* I spent Christmas with my girlfriend and her family today. We made a big Christmas dinner, played fun games, and everyone got a gift.
* I had a fantastic time.

# 2022-12-25 #

*
