2024-09-12
The first class was highly interactive, offering a clear outline of what to expect in the future. The unexpected exam on the first day set the tone for a challenging course. Group homework was assigned, encouraging teamwork right from the start.

2024-09-19
Today introduced the Git repository, which seemed complicated at first but essential for project management. The class also covered renewable energy, and tips were shared on improving presentation skills.

2024-09-26
as a National Cheng Kung University student i start the beginning of the electrical engineering major, chosen for its vast opportunities across mastering physcis, calculus, and some laboratory programs. The program promises a broad exploration of the field, allowing students to gain a comprehensive understanding before focusing on areas of interest.

Flexibility and adaptability are key approaches in the curriculum, encouraging students to explore various industries related to electrical engineering before deciding on a specialization. The dynamic nature of the program prepares students to navigate challenges like language barriers and transitions from academic life to the professional world.

The rapid advancement of technology and evolving industries, such as the rise of electric vehicles and renewable energy development, will require students to continuously update their knowledge to stay relevant and make impactful contributions.

This Week's Presentations
The presentations this week were engaging, showing how statistics highlight improvements in many areas. We watched three videos on statistics and fake news. The TED interview with Christiane Amanpour caught my attention, especially when she said that all sides should be given an equal hearing, but not necessarily equal treatment. She also suggested creating an algorithm to filter fake news, which is a good idea but could lead to over-reliance on technology and reduced critical thinking.

Mona Chalabi's three questions for identifying bad statistics:

Is there any uncertainty visible in the data?
Can I personally relate to the data?
How was the data gathered?
Task 1: Future Trends
Five trends I believe will shape my future:

Advanced Education and Expertise
Business Digitization and Automation
Technological Innovation in Business
Globalization and Market Expansion
Sustainability and Eco-Friendly Practices
Ten years from now, I plan to return to Indonesia after completing my master's abroad to modernize and expand the family business. I will focus on using AI and automation for efficiency, incorporating eco-friendly practices, exploring international markets, and leveraging technology to stay competitive.

Task 3: Summary of "Vietnam Estimates $3.31 Billion in Damages from Typhoon Yagi"
Center-aligned sources report $3.31 billion in damages from Typhoon Yagi, with widespread flooding, infrastructure damage, and 299 deaths. Left-leaning sources emphasize the impact on rural communities and the need for climate action. Right-leaning sources focus on the economic consequences, including disruptions to major industries.

2024-10-03
Due to the typhoon, all work and class activities were canceled from October 1st to October 3rd. The homework involves creating figures on trends in purchasing power, central bank balance sheets, government debt, and credit, analyzing income and wealth distribution, and explaining what gives money its value. We must submit a 2-minute video summarizing our findings.


2024-10-17
Today's lesson broadened the understanding of money, explaining how new money can be created legally through loans. Investment credit was highlighted as an effective method to manage money, fostering economic growth for both countries and businesses by generating more profit.

2024-10-24

In today's lecture, we discussed extremism and racial profiling. A video showed a young American boy who was radicalized when he lacked parental attention, eventually turning against foreigners despite his immigrant background. This highlighted how easily vulnerable youth can be drawn into radical movements'a disturbing and sad reality.

We also watched a video by Yuval Noah Harari, who described our society as a "fictional reality" built by human-made systems. While his view was interesting, I found the term a bit extreme, and I still see value in our structured society compared to ancient, tribal living.

We explored the scientific method, discussing how a single counterexample can disprove a hypothesis, while numerous confirmations are needed to support one. Even Newton's laws have been challenged by Einstein's theory of relativity.

Later, two videos covered exercise and health care. The benefits of exercise are well-known, but there was new insight into the idea of a proactive healthcare system that rewards people for staying healthy. Although it's a compelling concept, I'm unsure how feasible it is given current healthcare limitations. AI might help in the future.

For a healthy life, I believe three key habits are:

Finding purpose - Meaningful work and hobbies boost mental well-being.
Building social connections - Social support improves mood and longevity.
Prioritizing sleep - 7-9 hours of sleep improves immunity and mental health.

2024-11-07

Today's lecture focused on depression, including videos of people who've experienced it. According to the WHO, depression is the leading cause of illness, which surprised me as it's rarely discussed openly. Unlike physical injuries, depression is often misunderstood or dismissed, even though it's a genuine illness.

A main takeaway was that listening is more valuable than speaking when supporting someone with depression. Simply being present can help them feel valued.

Key Points on Recognizing and Supporting a Friend with Depression

Depression has diverse triggers and often affects those we might not expect.
It's challenging to recognize, as people may hide it well.
Friends can help by being there and doing everyday activities together, rather than trying to provide therapy.
This lecture broadened my understanding of how common and widespread depression is. After talking with friends, it's evident that many people have connections to someone who's faced depression.

2024-11-14
Today, the professor shared the results of a survey on depression and anxiety, revealing that the main cause stems from failing to meet personal or external expectations.

We had a small class debate about coal-fired power plants in Taiwan, particularly in the Taichung area. The discussion highlighted various perspectives on environmental and energy issues.

The professor encouraged us to take initiative and conduct independent research, emphasizing the abundance of free online resources that provide valuable knowledge.

We also watched videos showcasing how human choices can be manipulated. One experiment demonstrated how people, when given an option they didn’t originally choose, often rationalize or create reasons to support the new choice.

2024-11-21

Claims and Arguments from Videos
How We Need to Remake the Internet

Claim: The current structure of the internet, led by platforms like Facebook and Google, is fundamentally flawed. This has resulted in widespread harm, and the internet must be redesigned to undo this damage.
Argument: The internet has led to addiction and exploitation of personal data. Additionally, misinformation spreads easily, manipulating public decisions.
Simon Sinek - Millennials in the Workplace

Claim: Social media and smartphones negatively impact millennials, fostering addictive behaviors that affect their lives, especially in professional environments.
Argument: Social media triggers dopamine responses akin to addictive substances like drugs or alcohol, harming mental health and focus.
The Era of Blind Faith in Big Data Must End

Claim: Over-reliance on big data and algorithms without scrutinizing their biases is dangerous and requires reevaluation.
Argument: Algorithms reflect the biases of their creators and lack transparency, leading to unfair societal outcomes.
Enforceable Laws Suggested

Clearly label the origin of online content (user-generated, expert-verified, or sponsored).
Platforms must take responsibility for controlling disinformation with transparent moderation systems.
Users should control their personal data, including deletion rights and opting out of targeted ads.
Regulate platforms using manipulative techniques like addiction-inducing design.
Protect freedom of speech but prohibit content that incites violence, hate, or discrimination.

2024-11-28
Today, the professor conducted a small experiment to measure smartphone addiction among students. We also listened to group presentations showcasing creative ideas, which I found inspiring and hope to see implemented. Additionally, the lecture touched on the concept of "dark ages" and their recurrence throughout history.

2024-12-05
During today’s class, we analyzed the relationship between press freedom scores and sentiment, discovering that no direct correlation exists. In groups, we worked on a 15-minute discussion to analyze a news headline and presented our findings. The day’s theme revolved around planetary boundaries and the potential crises we face without urgent action. This motivated us to think critically and address global issues proactively to avoid another "dark age."

2024-12-12
We began with a survey on individual actions to reduce CO2 emissions, finding that having one less child is the most impactful action. Next, two groups presented their final projects, which were followed by a debate and Q&A session. A survey determined the most critical foundation discussed. Lastly, we watched a YouTube video about the third industrial revolution, broadening our understanding of sustainable development.

20 Dec 2024:
Unproductive but successful because I played games all day.

21 Dec 2024:
Productive but unsuccessful. I completed two projects in a day, but I wasn’t too happy about it.

22 Dec 2024:
Unproductive but successful because I spent the whole day exploring Tainan and trying different foods.

23 Dec 2024:
Productive and successful. I made a new friend who helped make my project easier, and I came up with lots of new ideas for upcoming projects.

24 Dec 2024:
Productive and successful. I was happy because I played with my friends and managed to complete two projects.

25 Dec 2024:
Productive but unsuccessful. I completed my daily tasks, but Christmas didn’t feel like Christmas. It’s sad when a happy day feels so ordinary.

26 Dec 2024:
Unproductive but successful. I didn’t study even though I have a quiz coming up. My club was closed for the day, but I was happy because my project turned out to be a massive success.

27 Dec 2024:
Productive but unsuccessful. Even though I completed another successful project, I slept at 5 a.m., which made me uncomfortable and tired for the whole day.

28 Dec 2024:
Productive but unsuccessful. I studied and speed-ran my next big project, but it wasn’t very enjoyable.

29 Dec 2024:
Productive but unsuccessful. I continued studying and speed-running my next big project, and again, it wasn’t much fun.

30 Dec 2024:
Productive and successful. I finished my big project, and it was a success. I’m happy with how it turned out.

31 Dec 2024:
Unproductive and unsuccessful. I ran out of ideas and went to bed at 7 p.m. to skip New Year’s celebrations, but I woke up at 11 p.m. and couldn’t sleep for the rest of the night. At least I caught a glimpse of the fireworks from my 10th-floor dorm. I also read a little calculus, but I couldn’t focus.

1 Jan 2024:
Productive but unsuccessful. I studied, came up with new ideas, and finished my diary. However, I wasn’t too happy since most of the restaurants were closed—I love eating, and it made the day feel a bit dull. :(

In Loving Memory of Kennard

Kennard’s life was a vibrant mosaic of curiosity, passion, and individuality. He approached every day with purpose, seeking to live fully rather than simply exist. At the age of 82, he passed away peacefully in his sleep, leaving behind a legacy as unique and remarkable as the person he was.

Born in jakarta, indonesia, Kennard was a quiet force who had an extraordinary way of leaving a lasting impact on those he met. His relentless pursuit of knowledge led him down many paths, from mathematics and programming to his deep interest in electrical, where his work contributed to meaningful, albeit humble, advancements. Despite his academic achievements, Kennard valued balance in his life, always making time for the relationships and experiences that brought him happiness.

Kennard’s legacy lies not in dramatic milestones but in the quiet yet powerful influence he had on others. He challenged minds, created smiles, and had a rare gift for making people feel truly seen. Though his absence will be deeply felt, his spirit will live on in the memories of those who knew him, the ideas he shared, and the kind, thoughtful way he carried himself.

Rest well, Kennard. You made a difference.

"A world of facts, challenges, and ideas"
Know the current world demographics
Suggestion: Regularly review reliable sources like UN or World Bank demographic reports and summarize key trends and statistics in a study journal. Use online tools like Gapminder to visualize data.

Understand planetary boundaries and the current state
Suggestion: Watch introductory videos and read articles from the Stockholm Resilience Centre to learn about planetary boundaries. Track current environmental news through platforms like BBC Earth or National Geographic.

Understand how the current economic system fails to distribute resources
Suggestion: Read case studies from organizations like Oxfam or World Economic Forum on wealth distribution. Discuss in group sessions to explore real-world examples of systemic inequalities.

Be familiar with future visions and their implications, such as artificial intelligence
Suggestion: Follow reputable sources like MIT Technology Review or Stanford’s AI Index to stay updated on AI trends. Participate in online AI workshops or free MOOCs for beginners.

Ability to use demographics to explain engineering needs
Suggestion: Practice analyzing demographic data to identify engineering challenges (e.g., urban planning in rapidly growing cities). Use tools like Google Public Data Explorer to connect demographic trends to engineering solutions.

"Personal health, happiness, and society"
Understand how data and engineering enable a healthier life
Suggestion: Explore case studies on data-driven healthcare innovations, such as wearable health monitors. Consider taking a free course on platforms like Coursera about data applications in health.

Know that social relationships give a 50% increased likelihood of survival
Suggestion: Actively engage in group projects or join a community club to build interpersonal skills and foster meaningful relationships.

Be familiar with depression and mental health issues
Suggestion: Take part in mental health awareness seminars or read materials from organizations like WHO or Mental Health Foundation. Reflect on ways to maintain personal mental well-being.

Know the optimal algorithm for finding a partner for life
Suggestion: Study basic decision-making algorithms, such as the "37% rule," and reflect on how they apply to personal relationships. Participate in workshops or group discussions about relationship building and communication.

"Professional skills to success"
Develop a custom of questioning claims to avoid “fake news”
Suggestion: Use fact-checking websites like Snopes or FactCheck.org to verify news articles. Practice evaluating claims by comparing multiple sources during class discussions.

Be able to do basic analysis and interpretation of time series data
Suggestion: Practice analyzing datasets using beginner-friendly tools like Excel or Google Sheets. Complete free online tutorials on time series analysis using these tools.

Experience collaborative and problem-based learning
Suggestion: Form study groups to solve case-based problems from your coursework. Rotate leadership roles within the group to build team dynamics and collaborative decision-making skills.

Understand that professional success depends on social skills
Suggestion: Attend networking events, both in-person and virtual, to practice communication skills. Use platforms like LinkedIn to build and nurture professional relationships.

Understand that the culture of the workplace affects performance
Suggestion: Read case studies on workplace cultures from sources like Harvard Business Review or Forbes. Observe and reflect on different organizational cultures during internships or part-time work.

Know how to do basic analysis and interpretation of time series data
(For emphasis, in case it's a critical skill) Suggestion: Download open datasets (e.g., weather, stock market trends) and analyze them using Excel, focusing on trends and patterns. Follow step-by-step guides for beginner time series analysis.

