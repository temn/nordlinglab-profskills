# 2022-09-08 #

* In my opinion, Elon Musk's capacity to work with so many people in such different futuristic proyects is amazing.
* The first class challenged me to think more deeply about where our world is heading at.
* I don't think we'll be able to measure all the human-related consequences of the technology's advancement.
* I think we could find a stronger meaning of our lives if robots replace our work.

# 2022-09-15 #

* I liked how the students who did presentations were challenged by the professor.
* I'd like to hear more detailed explanations of the graphs showed in the class.
* It is very interesting to learn how to use professional platforms/apps.
* I still don't understand quite well what's the purpose of Bitbucket's work system.

# 2022-09-22 #
* I think knowing how to identify fake news is an essential skill nowadays.
* I agree with the idea that trust among citizens and politic leaders is incredibly powerful.
* I also agree with the fact that fake news is destroying trust.
* I love the following quote regarding graphs: "The differences are much bigger than the weakness of the data"

## Homework ##
### Part 1 ###

*How I see myself in ten years from now*

I'm living a Asuncion City, in Paraguay. I’m giving my best everyday for my family and because of the need to feel I am contributing something significant to the society as a whole. I live in a small but nice house, which only consumes clean energy thanks to the improvement the paraguayan government has made over the years to increase even more the production of renewable energy. 

My house not only consumes this kind of energy, but was also built based on a biophilic design. This view focused on making environments as connected to nature as possible is something that also has gained value recently thanks to the increasing awareness over climate change. I am so happy to live here because I can see how my kids are slowly learning more about the importance of natural environments. Moreover, my husband and I find so much peace here whenever we feel stressed about daily life problems. I also find joy in spending time cooking at home, especially because I have my own little farm. Even though I don’t eat meat, I have no problems eating outside either because all Paraguayan restaurants offer good vegetarian and vegan food thanks to the increasing demand of these products over the years. 

My family and I normally use public transportation whenever we need to go outside, and also try to ride bicycles or other non-contaminating transportations whenever possible. My husband and I also share an electric car and use it sometimes for further distances. We were able to afford one quite easily thanks to Tesla’s wide production of electric cars all over the world, along with other relatively new companies. 

After studying Environmental Engineering during university in Taiwan, I obtained a Master's degree related to sustainability while living in Europe. I am now working on the improvement of the waste disposal systems of one of  the biggest manufacturers in Asuncion city. At the same time, I am taking part in some environmental education programs for highschool and university students all around Paraguay. My knowledge of both English and Chinese is helping my team maintain communication with foreign leaders who guide us throughout the development of these programs. Thanks to the widespread use of the Chinese language, more and more people are trying to learn it along with the English language. 

At this point, I feel like I have actually achieved many things over the years, but I still need to improve as much as possible so the work I do for others can also keep improving. 

### Part 2 ###

* The article I chose is **"Attraction closed after 3rd dolphin death in 5 months"**
* Each article offered very similar information about how the deaths of the 3 dolphins have occurred.  
* There were still a few differences. 
* Out of the 5 articles, PEREZHILTON was the only that stated 3 dolphins died in 6 months instead of 5 on the article’s title.
* Moreover, PEREZHILTON’s article was written in a more dramatic way. 
* For example, PEREZHILTON mentioned that the dolphin “tragically lost his life” while the other articles simply stated it died.

# 2022-09-29 #

* I think is was very interesting reading and listening to my classmates' projection of their lives.
* This course made me think about how useful it would have been to have a better financial education during highscool.
* It was very interesting to learn a lot things about money, but it was also a little overwhelming for me.

# 2022-10-06 #

* I thought this class gave us an important reminder that we (young people) really need to get engaged in politics.
* I liked the idea that sometimes a person is not bad, but has a really different mindset.
* It was nice having an in person class. I think we are very shy anyways, but it’ll probably get better!

## Homework ##
### Religion ###
These past days I’ve been trying to form a good habit. I started sleeping and getting up more or less at the same time everyday. I’ve been doing this not only for myself, but also as an offer to God and Marry, because it is not something easy for me. I learned this improvement method through a catholic movement I joined long ago.
### Money ###
I recently sold a book to my roommate, it was a calculus book I bought for my class, but I decided to use the pdf version. I sold it to her for the same price I got it.
### Marriage ###
This past week I just had a discussion with my boyfriend about marriage. We’re not thinking about getting married now or anything, but we were talking about what we think  are the ideal conditions for a marriage to happen (as in general).

# 2022-10-13 #

* It was the first time I heard about the idea of going from turning the sick-care system into health-care, it opened my eyes.
* I already knew that doing exercise is a good thing, but the way its benefits were explained during one of the videos was fantastic.
* I loved hearing the point of view of my classmates during some of the conversations held in class.

# 2022-10-27 #

* I think all the videos we watched today were very interesting.
* I could identify some of the common behaviors of depressed people in some people close to me.
* These videos made me really think about the way I behave towards others, and appreciate that.

# 2022-11-03 #

* I really loved the video mentioning the importance of having meaning.
* I think the sense of meaning and belonging I have is what keeps me going during the hard moments.
* I also love the idea of taking something good from the bad experiences.
* I am happy because I was able to meet early with my project team to discuss our work.

# 2022-11-10 #

* I like the fact that the project we need to work on for the course has to include specific actions.
* I agree that doing this kind projects for our community can make us feel empowered.
* It was the first time I heard about CLT, and I think is an interestesting construction system.
* I worked very well with my project group for the first presentation!

# 2022-11-17 #

* I think it was interesting learning about the different laws regarding mainly environmental pollution and health from different countries.
* I realized I am one of those people who often use their phones instead of socializing.
* I think it's scary of often we use social media nowadays.

# 2022-11-24 #

* This class gave me an important reminder: "With great power comes great responsibility."
* I think it is important to compare sources for information, but I also think it's pretty tiring doing it on a daily basis.
* It was interesting seeing how much we can learn from past civilizations and how similar their problems were to ours.

# 2022-12-01 #

* I liked learning about the lefside and liberalism concepts, which are some of the ones I always struggled to understand.
* I think having debates during class is an interesting was to learn from each other.
* Is the first time I hear about the planetary boundaries, I would like to keep learning more about it!

# 2022-12-08 #

* It was very shocking for me to learn the impact of having one fewer child over the environment, though it makes complete sense.
* I enjoyed hearing and discussing the ideas from the other project groups.
* It was cool to see the impact of a plant based diet in the graphs shared in class because it is related to my project topic.

# 2022-12-15 #
* I'm just loving the debates in class, especially because I can see my classmates getting more comfortable speakig their mind :) and so am I!
* My favorite frase from this class: "Don’t pick a job. Pick a Boss."
* The following video mentions many of the climate change aspects discussed in class, and it's really inspiring!
* Video suggestion: https://youtu.be/LxgMdjyw8uw 

