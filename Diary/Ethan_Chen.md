This diary file is written by Ethan Chen E14093815 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #

* The first lecture was fun. Everything went smooth and enjoyable.
* I initially expected this class to be otherworldly difficult, but it turned out to be ok.
* I am somehow more motivated to learn this semester.
* It was surprising I got a low score for the exam considering I had read *Factfulness* before.
* I hope I will do well in this class.

# 2022-09-15 #

* Me and my group members prepared our presentation for multiple hours and we had fun working on it.
* We were a bit skeptical on whether we should choose the topic we gave presentation to, but we went with it and it turned out great.
* Props to my group member on his fantasitic delivery.
* Today's class was a bit rushed.
* Nevertheless, Professor Nordling has been very attentive and patient to our questions.
* A thought on the Steven Pinker's TED Talk:
*	-	It is very inspiring; especially the part where he says that the world will never be perfect, we shouldn't try to perfect the world but we should try enhance it.
*		This saying reminds me of my friend's mom who used to tell her, "I cannot give you the best, but I will give you my best."

# 2022-09-22 #

* **Diary:**  
	- It is cool to learn a website like Ground News exists; definitely gives me more motives to read US news.
	- I spent way too much time on my diary today.  
  
* **What I imagine my future will look like 10 years from now:**
> In 2032, we live in a time when information has blown craters on the Earth. I live in Toronto, Ontario, Canada as a UX engineer. The slow rise of the metaverse gave my job more influence than ever. I now design UIs and sometimes work as a front-end engineer for my company's new VR experience. By a "new VR experience," I mean a partially decentralized world that people will be able to live inside. Since it is still in development, you can imagine it as a means to connect people's shops, personal spaces, games, or whatnot with the infrastructure and tools we build.
>
> Although the unemployment rate is lower than ever, the number of homeless men seems to have increased as my salary has not. The World Happiness Report 2032 shows an almost-consistent drop in happiness levels (Cantril ladder) as the world population has reached 8.5 billion. I used to expect governments would set more laws for ruling the online world and restrict monopolies like Google and Amazon, but it has not happened yet. Nevertheless, governments worldwide have been encouraging people to install solar panels. Thanks to this action and other green movements, global warming has slowed down significantly. Ultimately, the world has not changed much other than some improvements, optimistically speaking.
> - - -
> The following are some miscellaneous predictions I have for the year 2032:  
> Flying capsules are close to being viable. China has announced dozens of China's final warnings toward Taiwan and the US since ten years ago. SpaceX will set off its first trip to Mars next month. Tesla has released four new EV models since 2020: model L, model M, model A, and model O. Investments in technology in the Netherlands have surpassed France. Korea announced the foundation of K-llywood. An API developed by a Japanese computer scientist is slowly making HTML and CSS obsolete. Matthew McConaughey is in his third year as president of the United States. Movements to accept different forms of polygamy marriages started in New York. Meta's attempt to build the metaverse failed big time when a firm called Immerxion came along and took the throne of the metaverse. Finally, and I mean finally, Apple has published a patent on a holographic display.

* **Summary of news from Ground News:**
	* The news I chose is of the title ** *Bank of America economists expecting mild recession this year* ** (articles written roughly 2 months ago with 15% leftist view and 46 rightist view).
	* **Left view: *US recession will come this year as 'inflation tax' weighs on consumers and the Fed stays aggressive, BofA says* **  
				https://markets.businessinsider.com/news/stocks/us-recession-bofa-fed-inflation-tax-americans-economy-markets-bank-2022-7?utm_source=ground.news&utm_medium=referral (last visited 2022-9-24)
		- BofA's forecast consumer price growth is 0.3% higher than expected, which gives Federal Reserve a reason to be hawkish.
		- "Financial conditions have tightened as the Fed leans into more rapid interest rate hikes."
		- The stimulus the Fed gave during pandemic is fading, (due to high inflation,) the high prices of goods is slowly suffocating people's spending power.
	* **Center view: *Bank of America economists expecting mild recession this year* **  
				https://thehill.com/policy/finance/3558041-bank-of-america-economists-expecting-mild-recession-this-year/?utm_source=ground.news&utm_medium=referral (last visited 2022-9-24)
		- BofA's economists forecasted the economy to be weak while still having positive growth outcome. Yet, the economy has slowed faster than they expected.
		- The main concer is that consumer spending will also slow down.
		- Multiple other banks have the same opinion about a recession coming in the near future unless the Fed could "soft-land" the high inflation.
	* **Right view: *Bank of America Forecasts a Recession This Year with Economy Shrinking 1.4%* **  
				https://www.breitbart.com/economy/2022/07/13/bank-of-america-forecasts-a-recession-this-year-with-economy-shrinking-1-4/?utm_source=ground.news&utm_medium=referral (last visited 2022-9-24)
		- Cites the same prediction note as the above two articles. However, put more emphasis on the number -1.4% and says that "it forecasts just one percent growth in 2023."
		- Includes many quotes from the note that describes: the Fed's eagerness to help stablize its economy; the Fed will need to accept more pain than it likes to overcome this situation; that the unemployment rate will increase from 3.6% to 4.6%; and even includes a suggestive solution in the note.
		
# 2022-09-29 #

* Today's meeting was the first time I share my diary in class. I wonder what others think about my image of the future.
* Today also marks the new grouping, exciting stuff.
* I like today's topic (financial system) although I only understand a fraction.

# 2022-10-06 #

* **Diary:**  
	- Today's lecture was a bit shorter than usual, but the theme is clear.
	- It is funny how I got a chance to speak again this week.
	- The homework tasks are confusing tbh.

* **3 fictional stories and how they affected my life (with examples of how, in the past week):**  
	1. I used to hate Koreans. However, after years of befriending a number of Koreans while learning about their esports and artistic talents, I have changed my mind radically.
		- I felt hurt knowing Kim Jung Gi's passing, and felt happy to see how welcoming the Koreans acted when Neal won the Kart Rider championship.
	2. I used to support Feminism, but I have learnt to support gender equality instead. They may sound the same, but they are not.
		- I had a conversation last week with a female friend about gender equality. It was refreshing to hear how she thinks girls in Taiwan should have mandatory military training if the boys do.
	3. I hated China as a whole, but I have grown to know that some of them were told to hate Taiwan when I encounter them.
		- The aforemention conversation was with a female who was from Shang-Hai.

* **Describe my future:**
> I am writing in response to Mr. Peter Thiel's statement:  
> Easily enough, the ultimate fate of Taiwan is to either be devoured by China or claim independence. Though We were unfortunate to be kicked out from the U.N. Yet, we still have the chance to shift the tides if we can prove to the world that we produce significant results. First of all, TSMC must remain untill the next TSMC exists (Taiwan has plenty of Start-ups that has the potential to become an unicorn; or, in fact, I think firms like Gororo already met the standards to be one). Secondly, to nurture an (or more) unicorn. Our government could create even more prizes, or hold more quality keynotes or speeches to encourage tech startups. Thirdly, we most certainly need more high-end digital developers. Since the marginal benefits of software copies, games, etc is far higher than manufacturing. Fourth, we need to distribute talents out from TSMC. TSMC right now is acctrating more fresh livers than they need, it could be helpful for Taiwan's tech scene if some of these talents could go to other Taiwanese firms. Fifth, although I can only hope, but it would be ideal if Taiwan's politic scene could have more young blood and or capable people.

# 2022-10-13 #

* Today's lecture was well-structured.
* It was an interesting experience meeting my new teammates and had the opportunity to speak to some other classmates.
* In response to the second TED Talk video, I agree that the US, in particular, should shift their common health goal from sick-care to health-care.
However, I do not agree with him that we should require people to submit health data. It ultimately boils down to the popular safety-versus-privacy argument.
The fact he mentioned - that about 86% of death by cancer was caused by late discovery of cancer - is nonessential. We shouldn't sacrifice people's privacy for longer life expectancy.
Not to say he is persuading people to provide personal data to one of the most dangerous countries to hold data.
Nevertheless, I could be wrong. The reason airplane has become the safest way of travel is only because the world promoted aviation safety.
And by that, it includes everyone having to clarify their identity and share some of their personal info whilst.
Yet, again, the reason why aviation safety measures were approved widely was possibly due to 911.
* In response to Justin's - my former groupmate's - opinion shared in class, all due respect, I agree that students in class usually take the materials taught in class as the truth.
However, I don't think it has to do with Taiwanese classroom culture where people seldom ask questions or speak opinions aloud.

# 2022-10-27 #

* The in-class discussion was surprising.
* The explanation for the course project was rather confusing.
* The videos on depression were inspiring, especially the one that encourage us to talk to depressed people normally. It sparked me and made me recall memories of seeing people do just that. And, it was shown to me that works, inspiring.

# 2022-11-03 #

* The first discussion - what would we do if a friend we knew on the first day of college expresses that he/she intends to commit suicide while we are having lunch with him/her - we had in class was interesting for me. 
It is surprising to hear that many (a couple) people would choose to straight up leave the depressed person.
I mean, it is understandable that they are afraid of being affected by the negative emotion.
But, it's shocking because I doubt anyone would choose to let a friend die.
I cannot imagine how it would feel to leave the friend at lunch and later get his/her death message.
* The "magic" video showed a interesting fact. The Nihilist in me just grew bigger, I feel.
* I am looking forward to League of Legends Worlds Championship finals this Sunday.

# 2022-11-10 #

* I gave my first presentation today (though in video format). Also, I did my first QA this semester.
* Although I dislike my delivery of the presentation, I was calm handling the QA; possibly because I know a few people in class.
* I am a bit worried that my group won't be able to complete the 3-actions before semester ends. We will see though.

# 2022-11-17 #

* I genuinely enjoyed the TED Talk we watched today. Everything to do with tech-ethics ("tethics") is interesting.
* I have watched several videos or shows documenting and/or discussing the effects of social media including the ones we watched in-class today. It is interesting how similar these arguments are; it is worrying, it is terrifying, and it is depressing.
* The question now is how we can reform social network. The professor had us decide in class whether changing social network from free to subscription-based would solve the problem.
* I personally think it could. But, we would need governments to restrict these Internet giants' behavior also. We need to set laws to stop them from exploiting our data for profit, we need to have some organizations supervise their brain-washing effects, and it is probable we need to have a tech-ethics or Internet manner course mandatory for everyone with access to the Internet.
* There are many aspects that is subconsciously making me worried about having kids in the future - global warming, population boom, lack of resouces, etc. I am starting to think the ever-worsening Internet culture is becoming one of them as well.

# 2022-11-24 #

* I personally am not super interested in the topic we had today - hyper-inflation and how civilizations could fall.
* Nevertheless, it's interesting to see Roman's downfall from another perspective from what I have read in the past (though I can barely remember what I read.)
* The Tasks are gettings harder now, as are other subjects. Save me, Jesus.  
Also, I just want to stress how fast this semester has gone. We have just over a month before the term ends, insanity.

# 2022-12-01 #

* Professor's son is very cute, very entertaining to watch.
* Yet, the same cute little devil picked me to read my diary.  
To which, I want to address that I am not Catholic or Christian (but an Agnostic).
* Today's task was interesting mostly because it gave me the opportunity to talk to different people in our class.
* One thing I find recently is that I seem to feel a lot more comfortable speaking in front of strangers now. I have lost the touch since my gap year, but I feel like I have regained the ability again. I am happy.

# 2022-12-08 #

* Statement: I will not miss classes for the rest of the semester.
* I am a bit worried about my group's course project.
* (This is a few days later. I want to take a bit of time to reflect on my view for the 3rd industrial revolution. But, I don't have time to revise, so my writing here could be weird.)  
I did some research to work on this week's presentation, and it really painted the future in my head.  
Basically, in the future, every household or building will have individual power plants that drive on renewable sources, things will be connected and optimized with the help of IoT, and power levels will be lateral instead of vertical. It is a cool picture, to be honest. Just imagining things being distributed and collaborative, and adding in the vision of eventual blockchain infrastructure, the future sounds awesome!  
Nevertheless, looking at how the blockchain is doing right now, it undeniably makes me worried about whether if the future could be as nice as it sounds. I personally think capitalism will win everytime, because capitalists do not have a reason to hold back for.  
There are many aspects that we can discuss about the 3rd IR - how many jobs will be eliminated, how many jobs will be created, how fast can we pull it off, how could we design distributed systems while free market exists, how much more complicated will lives become, etc.  
It is also true that that future is not realizing in the near future. Yet, the future is harnessed by us and our decisions in the present.  
I think it is reasonable to design the distributed system collectively and arguably earlier than it arrives. The ultimate question for myself in the present is how.  
P.S. I am just rambling.

# 2022-12-15 #
* **Diary:** 
	* I missed class last week, so I did not get to experience debate in class last week.  
	However, I would say I have had a quite an experience this week.  
	And, from this experience, I have to say, it feels really hard to debate. I was a bit nervous about coming up with strong arguments and stressing them in a logical way.  
	I was a bit lost because I was not sure what conclusion my arguments should lead to. Yet, it was quite fun and fulfilling to roleplay as a capitalist and made a few people laugh.  
	* Things aside, I think the professor's request to have us dress up professionally is a bit too much and too sudden. Oh well.

* **2022-12-15**  
	* Unsuccessful:
		1.  I planned to finish two homeworks on that day but I only had the will strength to finish one.  
	- I am going to wake up early tmr and begin work early tmr.
* **2022-12-16**  
	* Successful: 
		1. I began working early in the morning to do homework.  
		2. I met up with my Automatic Control group to work on our project, and we are now 95% done with the project.  
		3. I was fortunate to have a nice time hanging out with friends and stayed up late until 4am (the time I went to bed).
	- I have an appointment next morning, so I will wake up at a reasonable time as well. And, I will try to finish at least two homework tmr.
* **2022-12-17**
	* Successful:
		1. The appointment was decent.
		2. I have made a bit of progress on our Automatic Control project.
		3. I have spent some time on our course project.
		4. I have had a meeting with my course project group.
	- I will try to have a small walk in the morning tmr to get myself in the mood to work.

* **2022-12-18**
	* Semi-successful:
		1. I submitted my CG assignment in time. But, I don't know if it was me, but it took me way too much time.
		2. I barely got other things done.
	- I will try to study before class tmr.
	
* **2022-12-19**
	* Unsuccessful:
		1. I failed to do everything I have planned for today.
	- I suspect it was because I wore too less over night. I will try to put on more clothes.

* **2022-12-20**
	* Semi-successful:
		1. The test today felt nice.
		2. But I skipped two classes today, so it was more like a mortgage.
	- I will listen to chill music tmr because I feel like I have too many distractions in life rn.
	
* **2022-12-21**
	* Unsuccessful:
		1. The weather defeats me. My bedsheet warms me. Today went badly.
	- I will drink more water because my eyes have been sore the past few days, presumably due to the dry weather.
	
# 2022-12-22 #

* Today was a relaxing class having everyone dress nicely. It is fun to see what people would wear when they have limited wardrobes.
* After attending the class, though, I realized that the reason Pr. Nordling asked us to dress professionally was to see how we and each other do on our dressing-ability.
* I had read my diary the third time today.
* I received 4 votes for the dress-up contest. lol

# 2022-12-29 #

* I feel more emotional today, so I am writing more than usual.
* I think Lux Narayan's view on obituaries is interesting - the most memorable and honored thing a person has done will be kept in his/her obituary.
* The professor has asked us to write a sentence that we hope will be on our obituary:  
I think, for me, that line would be "I-Hsien Chen, a warm-hearted philanthropist and entrepreneur, died yesterday at 99."
* I do not know what people will perceive me as after I reach 40 (perhaps I die sooner then said), but one thing I have been doing is to try and make people around me feel comfortable and at ease. The reason I really want to do UX-related jobs is because I am a very empathetic person. I can almost always tell, by a person's behavior, if they feel good or not. This trait of mine benefits me in some occasions though other times drags me down. Nevertheless, I will say, I am more willing to believe that this is good for me and I should keep doing this till the day I die.
* One thing the speaker from BuzzFeed mentioned touched me - the appeal to emotion has a tremendous effect that naturally attracts people.  
This evoked my feeling because I only learnt this a while ago. Before then, I was a bit confused about what gives artworks value. I thought it could be the fame it or the creator has accumulated, I thought it could be the technique, topic, or composition that makes an artwork good; but, no. It is the emotion it evokes that makes an artwork valuable. Two pictures of the same scene could have different values because they have different color tone and they evoke different feelings; different artworks resonates with different people. It is true that it is subjective, but if an artwork can cast the same effect on many people, then it would have a higher chance of being appreciated. It is true that it is subjective still, but imo, this is the best explanation I have heard, and it sounds like it could be a target to aim for. Same thing applies for literature, architecture, etc. People seem to have that desire for emotional resonance innately.
* I am a bit depressed because my group's proposal was rejected again and requires a major change. I am sure we will find a way to get it done, but, thus far, it is definitely not enjoyable.
* I just realized that this class was my last class in 2022.  
I hope all bad things will be left behind, and may all good things and merits be brought to the new year with us. To a New Year.

# 2022-01-05 #

* Despite we still have class on Saturday, it felt like it was the last lecture of the semester. Which low-key gave me a bit of an emotional feeling.
* My presentation was okay. I am glad my Q&A session felt easy and fun. It went well.
* I really liked this class. It gave me the chance to meet many new people and gave me multiple opportunities to speak and show my personalities in front of my classmates.
* I hope everyone in the class to pass every course this semester, may everyone have a nice time over winter vacation, and may all be wealthy and well in the future. Good luck, people!