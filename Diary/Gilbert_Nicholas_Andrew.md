# 2022-9-15
* First time using Bitbucket
* The world is getting better and worse at the same time

# 2022-9-22
* This week the lecture was still online meeting
* Get to know more about SDGs

This week we have a task to describe what we think our future will be like in the next ten years. In ten years, a vision I have always had of myself is a wealthy chef who owns a restaurant, gets married, and lives happily with my own family,  also it’s really important to have healthy physical and mental well-being, and becomes financially free. I’m not quite sure where I will live ten years from now, either I choose to stay in Taiwan or go back to my hometown, Jakarta. Although, most likely it depends on what my partner would want. I will have graduated with a bachelor's in Transportation and Communication Management from NCKU in two years. After graduating my options are only working or taking a culinary school. I know culinary school is not a requirement in order to have a career in a professional kitchen, but it can certainly help to improve career prospects.
Things that I do now will most affect my future :

Never stop learning. Knowledge is very important these days. It is rightly said never stop learning because life is the teacher which gives us the opportunity to learn new things each and every day. It motivates us to learn and defeat the challenges which we come across. The greatest teacher we could ever have is our life. Knowledge can come from anywhere and everywhere. Learning is a never-ending journey. From birth until death, we are in a continuous cycle of learning new things. It is a necessary part of our life. By observing new things and experiencing it in our lives, our sight of perspective broadens and changes the way we see the entire world. It improves our behavior and the way we think by expanding and challenging our understanding.

Disciplines. When you have things to do but you make excuses, they may make today easy but they make tomorrow hard. Disciplines make today hard and make tomorrow easy. If it's possible to be done immediately then stop making excuses.

Self-control. When people trigger our emotions and if I feel triggered, they have complete control over me. I can’t give them satisfaction, we need to control our reactions to everyone and everything. As we grow old we will realize we can’t control everything, the only thing we can control is ourselves.

Investments. When it comes to investment, not all investments are successful. Each type of investment has its own risk. Previously I don't have any income to invest. Now, every month from my income I would save mostly for investment. I would also set a budget spending plan to clarify my objectives to prevent me to spend lavishly. Usually, I would set 30 percent of my income for needs, and the rest for savings and investment. Lastly, to maintain a healthy lifestyle, what is the point of being successful if we don’t have a healthy body? Health is the most important thing in everyone’s life in this world.

* The article I choose is "Lifting weights regularly could cut risk of dying early, study finds"
* A little bit confusing because there were a lot of different views from different sources.
* Most of the articles were categorized high factuality and 56% of the sources are Center.
* But one similarity i found was combine regularly excercising and weights lifting could help us live longer. 

# 2022-9-29
* Professor shown us few fake photo of Earth
* Get to know more about value of money and financial system works
* New teammate to work with