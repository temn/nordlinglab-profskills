This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-17 #
* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation 

# 2024-10-3 #
* I am honestly not so sure where I will be working, and what my job will be in 10 years time. 
* 10 years is a very long time, but I can try to guess where I might be going. I am extremely passionate about aircraft and spacecraft, so I definitely prefer to work in some sort of aerospace company. 
* But currently, the companies I can see myself working for after graduation are UAV or drone companies. Currently, a lot of countries have their own UAV companies, so I have many options. 
* But my country, Indonesia has a few, and I have friends there who can help me get a job, so I will likely work there after I graduate. In these companies, 
* I think most of what I will be doing is programming,tuning the controls of the drones, and maybe deciding which configuration suits the UAV’s missions best. 
* With AI technology on the rise, I think I may need to study it as well. But this is 10 years from now, I have plenty of time. 
* Another thing is that with AI chat models improving rapidly, learning new things might be easier, since some of my questions may be answered with ease, accelerating my process of becoming a more proficient engineer.

* But my lifelong dream is to work on full sized aircraft that carry people, so I do plan on moving to a company like Airbus at some point. 
* If all goes according to  plan, I will get a job in a bigger company where I get to work on my desired type of aircraft (airliners or fighter jets). 
* As for where I’d be working if or when I end up in these companies, it’s a little complex. Currently, not a lot of Asian companies are capable of developing and manufacturing aircraft on their own, 
* and my best option is to look for work in Europe. However, with economies all over the world growing rapidly, it may not take that long until Asian countries begin indigenously producing aircraft. 
* And if that happens, I intend on going back to my own country to contribute.

* Either ways, I need to learn to work with other people. Aircraft are very complicated machines, and consequences for mistakes in aviation are severe. 
* I am not very good at socializing, let alone working with a team. I suppose that’s where this course comes in. With the economy growing, and employment rate increasing, 
* there will be more people joining in different sectors of the job market, including aircraft manufacturing and development. With newer workers joining the workforce, communication ability becomes all the more important. 

* Moreover, with more people being able to afford things and services they previously couldn’t afford, demand for different goods and services would rise. 
* I would assume demand for air travel would rise too, which means more opportunities for my intended field of work. 
* And with more people being able to afford internet, that would also increase the demand for satellites in space. 
* There are many problems in the field of space technology, such as the price of launches being expensive, as well as space debris posing a threat to orbiting satellites and spacecraft.

* Furthermore, eco friendly technologies are also rapidly improving, and are slowly making their way into the aerospace industry. More and more prototypes and concepts of fully electric VTOLs are emerging, 
* and it’s an exciting thing to see. Personally for me, this also means I will have to get familiar with these new technologies, and up my skills and knowledge in electronics (I am not particularly good with electronics ) 

* Another trend that shows this world getting better is that air travel has gotten so much safer since 70 years ago, and this will impact my work greatly, 
* as there will be more complicated systems for me to study (for good reason), and the burden of maintaining and improving the current standards of safety will be even heavier to us future engineers.

* The 3 questions to ask to spot bad statistics : 
* Can you see uncertainty ?
* Do you see yourself in the data ?
* How was the data collected ?

# 2024-10-18 #
* In class, I got to watch some videos that gave me perspective as to when banks print money, and what gives money its value
* money used to be backed by gold before Nixon put an end to that. And now, as far as I have learned, money is simply backed by the trust of the people who use it.
* I'm still not so sure what else affects the value of money, but I will do some of my own research for that.
* Unfortunately, I had a little incident with my gas stove yesterday, which gave me mild carbon monoxide poisoning, and a very unpleasant headache which affected my ability to learn during this lecture
* So I am going to have to catch up somehow

# 2024-10-24 #
* The easiest "fictional" story that affected me within the past week is money's value. I am able to get my daily necessities by giving traders pieces of paper thanks to this "fictional story" 
* Another "fictional" story that has affected me last week is the concept of "nationality". Because I'm not from this country, some people treat me differently.
* The last fictional story I can think of is physics, in a way. the laws of physics we see on our textbooks. It is a story that attempts to describe reality as accurately as possible, and I have to study it almost everyday

# 2024-11-07 #
* Today's topic is quite important to me, as I actually do have a friend who is depressed. I've always found it challenging to cheer him up, and I constantly fear that he would take his own life suddenly
* The videos we watched today gave me some pretty valuable insights as to how my friend feels, and made me thought of several new ways I can help him.
* The part from the golden gate bridge video where the people who survived jumping talk about how they regretted it as soon as they jumped off the rail, I got quite emotional hearing that. There is something that happened in my life that reminds me of this ( I'd rather not talk about it )

# 2024-11-14 #
* Today's first video is about choice blindness. The experiment results highlight how we tend to make choices "on autopilot" without realizing it. *
* In the experiment, the innterviewer switches out the pictures which the participants choose, and most of the times, they don't notice the change.*
* Its mind bogling to me that they did not notice it. I feel like I would notice it. But after thinking about it again, it made me wonder if I'm fully concious of all the choices I make in life. *
* This reminds me of an experiment where the mid section of the two brain hemispheres is cut, and the visual input to both brains are separated. *
* During the experiment, one side of the body made movements that the other side was not aware of. afterwards, the results are revealed to the other side of the brain *
* the test subjects tend to explain that the actions of the body's other side with full confidence that they are deliberate. However, the researchers concluded that their explanations are illogical, and comes off as they are making excuses *
* Makes me question whether or not humans actually do have free will. But I'd like to believe that we do. *
* Though the main lesson or message intended was that we should be more thoughtful of our actions and choices *

# 2024-11-21 #
* The professor spoke about how sweden is planning on phasing out ICE vehicles by 2050.
* many of my classmates commented that switching out ICE vehicles for electrics will only have a positive impact on the environment only if the grid produces electricity cleanly.
* which means pushing to change to EVs now is unwise for taiwan, and they should focus on making their power generation green.
* I personally think shutting down the nuclear powerplants was a stupid move on the taiwanese government's part
* Later, the videos we were shown covered topics around the internet, social media, and algorithms.
* However, I could not come up with a law that could help solve the problem of data exploitaion and apps intentionally designed to be addictive. It is difficult to cover up the loopholes.

# 2024-11-28 #
* The professor instructed us to put all our phones at the front desk, and keep track of how many times we reach for our pockets looking for our phones.
* Apparently it is an experiment to see how addicted we are to our phones.
* Later, all groups got to do their presentations and tell us about their action ideas for the course project.
* I forgot to upload our PPT, so I had to shamefully pick up my phone at the front desk to do so.
* Later, the professor showed us the concept of dark ages, and how they have happened multiple times in history, the next one always lasting longer than the one before it.
* If we do nothing about the climate crisis, we are going to face another dark age

# 2024-12-5 #
* It seems only 2 groups are presenting, hmm... I wonder why ?
* The professor then told us that if we do not do our course projects, he will need to fail us
* a lot of my groupmates have not been replying to me. I've practically done the entire PPT of our last presentation alone last time, at the cost of my 2nd Dynamics midterm
* I decided to not do anything this time around because I am NOT going to jeopardize my required subjects' grades for this because of unresponsive groupmates
* But with what the professor said, it seems I can't just continue being quiet about this. I'm letting the cat out of the bag...

# 2024-12-7 #
* My group was responsive for this one. but things could've gone better tbh, we issues communicating
* and to make things worse, I caught a bad fever. My resting heart rate was 120 bpm while i was trying to sleep
* I contacted my groupmates on monday over LINE to try and discuss work distribution, but one of them just sent me a file, containing his research on the topic
* I was very sick on the day I re-read the slides containing the instructions, and I thought we were supposed to answer ALL 17 QUESTIONS
* I made a PPT answering all of it, using input from my group mate's word file. and my other groupmate helped with about half of it.
* the group mate who made the word file then sent us another PPT file with no explanation, answering only the 10th question.
* I asked why he made another, he simply answered with "yes". I was so confused
* I uploaded the one my other groupmate and I worked on together, and only when I saw the first presentation today, I realized I messed up.
* We were supposed to answer only number 10, because we are group 10, I'm a friggin retard
* that groupmate who did the other ppt all on his own was right... I JUST WISH HE'D TELL US 
* well, anyway, I was the designated speaker for this one too. and I was already having a nervous breakdown before the presentation started
* I did horribly. I owe my group an apology...
* Later, we had a debate based on the 3 perspectives
* Truth be told, I was quite tempted to show some of my own extreme views on this, but I'd rather not turn this debate into a war.

# 2024-12-27 #
* unsuccessful and unproductive 
* practiced sections 6.6 and 6.7 of calculus but I don't feel like I've done enough 
* redesigned the speech and questionnaire into an e-brochure with a questionnaire segment
* I was expecting to get to at least chapter 7 of calculus but im too slow.

# 2024-12-28 #
* unsuccessful and unproductive 
* practiced 6.6 again, and then 6.8. but progress is slower than I expected expected 1pm - 4pm
* finished my coding homework 4pm - 7pm
* did more practice on 6.8
* couldn't handle the stress, spent about 1h playing games at night when I could have studied more

# 2024-12-29 #
* unsuccessful and unproductive 
* practiced more calculus, reached 7.3
* figured out some new tricks with u-substitutions for integrations
* but i dont feel like im good enough at using trigonometric identities
* again, I'm too slow

# 2024-12-30 #
* unsuccessful and unproductive 
* I still need to cover 7.4, 7.8, 8.1, 8.2, and 8.8
* I was able to finish the assigned practice questions for 7.4 and part of 7.8
* I wish I could have finished 7.8 too but I was too tired 
* ended up chatting with my friends at night, an unproductive activity, but i feel like if I study more I would get too stressed out

# 2024-12-31 #
* unsuccessful and unproductive 
* spent most of today on 7.8 practice questions. they're much harder than the ones on the book. 
* I have a history exam on Wednesday.. that's gonna take time away from studying calculus .

# 2025-01-01 #
* unsuccessful and unproductive 
* spent all day studying last minute for the history exam tomorrow 
* zero progress on calculus 
* I need an 80 on the final to pass calculus, If I don't study as much as I can, I am beyond screwed


