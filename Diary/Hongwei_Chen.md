# 2024-09-12 #

* The first lecture was engaging and interactive. We learned about the professor’s background, the course structure, and upcoming topics.
* We introduced ourselves to nearby classmates and had discussions, which set a collaborative tone. 
* A Google Forms quiz, which will reappear in the final exam, was also completed.
* The discussion on exponential growth was particularly intriguing, sparking curiosity and lively conversations. Overall, the class felt fresh and enjoyable due to its dynamic format and interactive approach.

# 2024-09-19 #

* Today we have listened a TED Talk speech that provided valuable insights. 
* It highlighted that while human morals may be declining, progress in technology and society suggests overall improvement. 
* Also, The talk emphasized the need for data to support ideas, showing that faith without evidence can be harmful
* Overall, the lecture reinforced the importance of perspective, critical thinking, and data-driven conclusions.
* some tips are given to improve our presentation  

# 2024-09-26 #

* We’ve learned how to identify fake news.
* Learn how to determine whether you are happy or not by how often you have to worry about something.

# task 1 #
Ten years from now, I think I will still be living in Taiwan, and I hope to find a well-paying job related to mechanical engineering, doing my best in this job to create a meaningful and impactful career. I hope I will still have my free time such that I could relax by watching movies, anime, or editing videos. While the future is uncertain and subject to change, I can speculate on how these aspects might evolve based on current trends and my aspirations.

Trends Shaping My Mechanical Engineering Job Future:

* Artificial Intelligence (AI) and Automation: AI and automation are revolutionizing engineering by creating smarter systems and streamlining processes. These advancements allow professionals to focus on complex and creative problem-solving tasks. In data science, AI enhances predictive modeling and analytics, offering new opportunities for innovation. However, ethical concerns such as job displacement and accountability in automated decision-making must be addressed to ensure equitable and sustainable progress. Balancing the benefits of AI with its potential challenges will be crucial.	
* Sustainability: Sustainability is a necessity rather than a choice, driven by the urgency to combat climate change. The development of greener technologies in energy and materials is central to achieving this goal. Renewable energy systems, eco-friendly materials, and sustainable manufacturing processes will reshape industries. However, sustainability requires a collective effort—from governments, businesses, and individuals—to make long-term commitments to environmentally responsible practices. Success in this area will hinge on innovation, investment, and global collaboration.
* Globalization: Globalization has made international collaboration a norm in solving complex challenges. Working with diverse teams fosters innovation by integrating unique perspectives and skills. However, this interconnectedness also presents challenges, such as navigating cultural differences and communication barriers. To thrive in a globalized world, professionals need adaptability, strong cross-cultural communication skills, and the ability to build trust within international teams. These qualities will enable effective collaboration and problem-solving across borders.
* Lifelong Learning: The rapid pace of technological advancements has emphasized the importance of lifelong learning. Traditional education is evolving to prioritize ongoing skill development through certifications and specialized training. Professionals must continually update their knowledge to remain competitive in their fields. Lifelong learning empowers individuals to adapt to new challenges and opportunities, but it also requires self-discipline and a proactive mindset. This trend ensures that workers can navigate shifting career landscapes successfully.
* Digitalization: Digitalization is transforming daily life, from smart cities to digital currencies. It offers opportunities to design systems that improve efficiency and convenience. In engineering and data science, professionals contribute to these innovations, ensuring that they are ethical and inclusive. However, digitalization also brings challenges, including concerns about data privacy, cybersecurity, and the ethical use of technology. Balancing technological advancements with societal well-being is critical as digital systems become increasingly integrated into our lives.

In conclusion, these trends will shape my future, blending professional ambitions in engineering and data science with personal faith. By addressing the challenges and embracing the opportunities presented by AI, sustainability, globalization, lifelong learning, and digitalization, I hope to create a meaningful and impactful career. Guided by these trends and my values, I am confident in building a future that balances technology, sustainability, and purpose.

# task 2 #
I have found a intriguing topics in https://ground.news/. It is "OpenAI ‘considered’ building a humanoid robot".After I have read this passage and other different sources(https://www.theinformation.com/ and https://www.republicworld.com) of the same topic, here is the different views of these sources:

* Ground News reported that OpenAI has recently explored building its own humanoid robot, citing "two people with direct knowledge" of these conversations.
* The Information(https://www.theinformation.com/) offered an in-depth look into OpenAI's internal discussions about creating a humanoid robot. The article highlighted the company's investments in robotics startups and its efforts to expand its robotics team.
* Republic World(https://www.republicworld.com) noted that while OpenAI is considering the development of a humanoid robot, it remains a low-priority project. The company is currently focusing on enhancing its reasoning model and developing an AI agent to automate various software engineering and analysis tasks.

It's fascinating to observe how each publication sheds light on different facets of OpenAI's endeavors in robotics, reflecting the multifaceted nature of technological advancements in AI.

# 2024-10-03 #

* Due to typhoon, the goverment announce that all people should stay home for safety, so there is no class today.

# 2024-10-17 #

* After two weeks of disruptions, we finally had an onsite class again.  
* The lesson focused on understanding money: its value, creation, and influencing factors.  
* Key insights from the class:  
  * Money was historically backed by gold, but now relies solely on public trust after President Nixon ended the gold standard.  
  * The trend of deposits and loans in Taiwan shows a yearly spike in public-held currency around Chinese New Year.  
  * Banks play a central role in creating money through loans, as explained in class videos.  
* We debated whether Bitcoin could replace Taiwan’s currency (TWD). And I think the answer is no, due to the trust and complexity in traditional systems.  
* In this class, I learn how the financial knowledge gained will help me better manage money in the future.


# 2024-10-24 #

* Listened to student presentations on the economic development of countries like Taiwan and Indonesia.
* Key Learnings from Videos today:
  * People are influenced by stories, money, religion, and politics, often leading to poor judgments.
  * Extremists often seek belonging rather than adhering to strict dogma.
  * Knowledge is awareness, understanding, or familiarity gained through experience, education, or reasoning.
  * A health-care system (focused on prevention) saves more money compared to a sick-care system (focused on treatment).
* overall, this week’s class gave me deeper insights into human behavior, knowledge, and systems that impact our lives.

# how to live healthy #

* Focus on whole foods: fruits, vegetables, whole grains, lean proteins, and healthy fats.
* Sleep 7–9 hours each night to allow your body and mind to recover.
* Manage stress to prevent chronic illnesses.

# three fictional stories #
  1. career 

  * Story: A signature can create money legally, and the value of currency is determined by collective trust and economic systems.
  * How It Affected Me: Last week, I transferred money for an online purchase, knowing that the transaction was entirely dependent on shared trust in the banking system. It struck me how a simple digital approval could create or move "value" that exists only in our collective imagination.

  2. Law 

  * Story: Laws are human-made constructs that dictate how we live, and breaking these rules comes with consequences.
  * How It Affected Me: I recently noticed how strictly I adhere to everyday regulations, like traffic lights and tax declarations. Last week, I overheard a friend talk about receiving a fine for a minor infraction, which reminded me how much we rely on these rules to maintain order, despite their entirely constructed nature.

  3. Career 

  * Story: A career is a structured path of work that determines success and self-worth in society.
  * How It Affected Me: Over the past two weeks, I’ve been rushing to complete homework and prepare for exams, believing that academic performance directly impacts my future career. This belief has driven me to prioritize study over leisure, reflecting how deeply the idea of a "career" influences my daily life.

# The future offered by some famous politicians in Taiwan #

The future offered by some famous politicians in Taiwan will face complexities and challenges 
On one hand, the recent political chaos following the 2024 elections reveals the fragility of domestic politics. Continued polarization and the propagation of hatred could significantly harm Taiwan's social cohesion and international reputation. If such instability persists, it raises concerns about the country’s ability to maintain a stable and prosperous future. A divided political environment may hinder decision-making, economic growth, and Taiwan's standing in the global community.  

On the other hand, former President Tsai Ing-wen’s vision offers a more optimistic and forward-thinking roadmap. Her emphasis on resilience, self-determination, and leadership in technology and green energy represents a path toward innovation and sustainability. Her vision underscores the importance of democracy and independence, proposing that Taiwan can thrive as a global leader while maintaining economic security and contributing to a greener planet.  

here are some critical issues Taiwan must address:
1. Unity and Stability: Political leaders must prioritize reducing division and fostering collaboration to ensure domestic harmony and effective governance.
2. Innovation and Sustainability: Taiwan’s future lies in leveraging its strengths in technology and green energy while balancing economic growth with environmental responsibility.
3. Global Leadership: Taiwan must continue to assert its independence and strengthen its democracy, despite external pressures, to secure its place on the global stage.

Ultimately, the future of Taiwan depends on its leaders’ ability to balance these competing visions—addressing immediate political challenges while pursuing long-term resilience and progress.

# 2024-11-07 #

* I listened to other students’ diaries and how they envision their country’s future.
* I have learnt that it’s important to engage normally with depressed people, invite them to activities, and avoid focusing excessively on their depression.
* we explored the differences between profitable and non-profit organizations.
* Watched TED Talks about depression and suicide, which prompted reflections on the struggles of those who face such challenges.
* Discussed signs of suicide, such as losing interest in life and feeling hopeless, and the regret survivors express after suicide attempts.
* After this class,I’ll be more mindful of the mental health of those around me and aim to be supportive while knowing my limits. When overwhelmed, I’ll help others connect with professionals for appropriate assistance.

# 2024-11-14 #

* Reviewed results from an anxiety survey conducted two weeks ago. It was unsurprising that many experienced anxiety during late October due to midterms and reports.
* People often make decisions based on feelings and rationalize them afterward.
* Discussions and engagement can help change opinions and deepen understanding.
* Positive relationships with coworkers are crucial for success and efficiency.
* If faced with a toxic coworker, it’s better to address the issue early or consider leaving.
* Identify personal values to align actions with beliefs for greater satisfaction.
* Cultivate positive relationships with supportive and uplifting people.

# 2024-11-21 #

* We listened to two diaries about different topics.
* We have watched two videos, here are some main ideas of these videos:

# 1st #

  * This video raises a critical concern: the advertising model has transformed from simple marketing into a sophisticated tool for behavior modification.
  * Internet ads are no longer neutral.
  * Algorithms are powerful tools that can enhance efficiency and solve problems, but they also perpetuate biases and allow for manipulation. 
  * Platforms profit from polarization because it drives engagement, but this comes at the cost of societal harmony.
  * For instance, Facebook selling user data and ads being used to manipulate elections demonstrate how unchecked capitalism in tech threatens democracy.

# 2nd #
  
  * The second video sheds light on the inherent flaws of algorithms and the blind trust society has placed in them.
  * Since algorithms are designed by humans, they carry the biases of their creators. These biases can perpetuate inequality, discrimination, and unfairness.
  * For example, using biased algorithms in the criminal justice system, as mentioned in the video, has led to unjust sentencing decisions.
  * Data washing, where creators claim fairness while embedding personal desires into algorithms, shows how the technology can be manipulated for profit.
  * Unlike human errors, algorithmic problems often go unnoticed until significant damage has been done. This makes oversight and transparency critical.

# how to prevent with enforceble laws #

1. Data Privacy and Protection Laws
  
  * Ensure individuals have control over their personal data.
    * Informed Consent: Companies must obtain clear, explicit consent before collecting or sharing user data.
    * Transparency Requirements: Users must have access to detailed information about how their data is being collected, used, stored, and shared.
    * Right to Opt-Out: Users should have the right to opt out of data collection entirely without losing access to essential services.
    * Penalties for Violations: Heavy fines for companies that misuse data or fail to protect it adequately.

2. Regulation of Online Advertising
   
  * Prevent manipulation and ensure ethical advertising practices.
    * Limit Targeted Ads: Restrict the use of hyper-targeted ads, especially those based on sensitive personal data such as political views, health information, or religion.
    * Ban Dark Patterns: Outlaw manipulative design tactics (e.g., fake urgency or hidden opt-outs) used in advertising.
    * Ad Transparency: Platforms must disclose who paid for an ad, why the user was targeted, and how the targeting was determined.
    * Election Ad Regulation: Stricter rules for political ads, including mandatory fact-checking and a public archive of all political ads.
    
# 2024-11-28 #

* Many groups present their slides about final project.
* I was so glad to see so many different topics from the presentations.
* After the group presentation, I started thinking about which of the ideas mentioned I could put into practice.
* the professor collected our cell phones during this class. This is a small experiment to see how addicted we are to our smart phone
* In today's class, professor also talk about the "dark age" from time to time.

# 2024-12-05 #

* In today’s class, group presentation focused on news analysis and critical thinking from group presentation . We learned that press freedom scores don’t necessarily correlate with the tone of news reporting, whether positive or negative. 
* The central topic of today's class was planetary boundaries, highlighting the potential crises humanity faces if we fail to act. This underscores the importance of collective action to address global challenges and prevent regression into a "dark age."
* Additionally, group reports revealed that the same issue can elicit varying or even contradictory responses across different countries. This complexity emphasizes the need for us to stay informed, think critically, and form independent opinions by analyzing information and understanding diverse viewpoints.

# 2024-12-12 #

* Climate change poses a significant threat to humanity, and achieving net-zero carbon emissions is crucial to preventing Earth from becoming uninhabitable.
* While individuals can make meaningful contributions, such as reducing waste by using reusable containers (a practice that often comes with incentives like discounts), the true change lies in systemic efforts. 
* Climate change also has severe economic consequences, as evidenced by projections of an 18% GDP loss due to its effects. 
* A common mindset—"I'll focus on my immediate concerns and let others handle the big picture"—needs to shift toward collective accountability.
* In my view, the key lies in creating a balance between individual responsibility and large-scale efforts. By amplifying stories of environmental success and showing tangible benefits, we can encourage widespread participation and foster a shared commitment to a healthier planet.

# How I feel everyday #

# 2024-12-19 # 

A. Unsuccessful and unproductive.

B. I felt unproductive and unsuccessful because I can't figure out why I have studied hard but still can't comprehend the contents in the textbook?

C. I will figure out the way to solve this issue.

# 2024-12-20 #

A. unsuccessful and unproductive

B. Even taking an 8 hours sleeps, I was still tired and feel upset, so I didn't do anything today.

C. Maybe I need more rest and adjust my daily routine.

# 2024-12-21 #

A. successful and unproductive

B. I spend my time playing League Of Legend all the day. It make me get a huge rise at rank, but in other hand, I didn't make any progress in my homework and presentation.  

C. I just want some happiness to release my pressure. I will start to work on what I should do the next day.

# 2024-12-22 #

A. successful and productive

B. After two days' rest, I can start to work on my homework and eventually finish them.

C. I still have things to do, keep fighting!

# 2024-12-23 #

A. successful and productive

B. I start to work on my presentation and make a huge progress.

C. I hope I can finish this tomorrow.

# 2024-12-24 #

A. successful and productive

B. I finish my presentation eventually.

C. Time to sleep sufficiently .

# 2024-12-25 #

A. successful and productive

B. I sleep for 9 hours today, having a good mood so that I can easily focus on the lecture.

C. I really like the content professor have talked about today.

# 2024-12-26 #

A. successful and productive

B. I have the professional skills for engineering the third industrial revolution class today, having learnt a lot of things. After the class, I back to the dormitory, studying for my exam next week.

C. I think the content of the subject I have studied is very hard ro comprehend, so I will find someone else to help with my question. 

# Five rules to become more successful or productive #

1. have enough time to rest.

2. maintain balance and discipline

3. continuous learning and adaptation

4. set clear goals and prioritize

5. make sure when doing important tasks, focus on it.

# 2024-12-27 #

A. successful and unproductive

B. Today I go to Kaohsiung where my home is to celebrate my mom's birthday with her. 

C. After so many weeks I finally have the time to come home and accompany with my family.

# 2024-12-28 #

A. successful and unproductive

B. Today, my family and I attended my cousin's wedding.

C. It happy to see my cousin get married.

# 2024-12-29 #

A. successful and productive

B. After two days rest, I start my studying again.

C. I have solved the problem I come across in 12/26.

# 2024-12-30 #

A. successful and productive

B. I keep going on my studying.

C. I take a big step in my studying progress.

# 2024-12-31 #

A. successful and unproductive

B. I go to a restaurant, having a big meal there to celebrate new year's coming.

C. The meal is very delicious.

# 2025-1-1 #

A. successful and unproductive

B. happy new year!

# 2025-1-2 #

A. successful and productive

B. I have the exam I have studied for since last week, and feel good after exam because it quite simpler than I thought.

C. Time to prepare for the project next week now.

Jensen Huang
A Taiwanese-American business executive and the co-founder, president, and CEO of NVIDIA Corporation, a leading technology company specializing in graphics processing units (GPUs) and artificial intelligence (AI).Known for his visionary leadership and charismatic stage presence, Huang is recognized as a transformative figure in the tech industry. He has received numerous accolades, including being named one of Time magazine's 100 most influential people. 

Steve Jobs 
An American entrepreneur, inventor, and visionary best known as the co-founder of Apple Inc., a company that revolutionized technology, design, and consumer electronics.Jobs was widely admired for his creativity, leadership, and ability to anticipate consumer needs, though his management style was sometimes described as intense.

# 2025-1-9 #

* The video that our group made is very good. Thanks to Briam and Elias, we can clearly know what topic is and how the plan be executed. 

# suggestion for course objectives #

1. Know the current world demographics

* Dedicate 20 minutes weekly to explore demographic data on platforms such as the UN World Population Dashboard or World Bank. Summarize key trends in your own words.

2. Ability to use demographics to explain engineering needs

* Analyze a real-world engineering case study (e.g. water supply systems in growing cities) and link it to demographic changes. Write a short report explaining the connection.

3. Understand planetary boundaries and the current state

* Watch an introductory video on planetary boundaries (e.g. from Stockholm Resilience Centre). Then, map out which boundaries are currently overstepped and suggest engineering solutions to address one of them.

4. Understand how the current economic system fail to distribute resources

* Read a summary of a key text (e.g. The Spirit Level or Piketty’s Capital in the 21st Century) and create a mind map linking economic inequality to resource distribution issues.

5. Be familiar with future visions and their implications, such as artificial intelligence

* Attend a webinar or TED Talk on AI’s societal implications. Reflect in your diary on one way AI could positively or negatively impact the world by 2030.

6. Understand how data and engineering enable a healthier life

* Identify one example of wearable health technology (e.g. Fitbit). Research how it collects and uses data to improve health, and document its impact on users.

7. Know that social relationships give a 50% increased likelihood of survival

* Write a brief reflection on how maintaining strong relationships has impacted your own well-being or those around you. Identify ways to strengthen social ties weekly.

8. Be familiar with depression and mental health issues

* Research the common signs of depression and write a list of supportive actions you could take if a friend or colleague is struggling.

9. Know the optimal algorithm for finding a partner for life

* Study the "37% rule" (an optimal stopping problem) and write a practical example of how it could apply to decision-making in relationships or careers.

10. Develop a custom of questioning claims to avoid "fake news"

* Practice fact-checking by reviewing one trending news article weekly. Use tools like Snopes or fact-check.org and document any discrepancies you find.

11. Be able to do basic analysis and interpretation of time series data

* Download a small dataset (e.g. stock prices or weather trends) and practice plotting it using Excel or Python. Write a summary of your observations.

12. Experience of collaborative and problem-based learning

* Join a study group or collaborate on a small project with peers. Document your role and how you solved a specific challenge together.

13. Understand that professional success depends on social skills

* Practice active listening in your conversations this week. Afterward, write a reflection on how it changed the interaction.

14. Know that the culture of the workplace affects performance

* Research an example of a company with a positive workplace culture (e.g. Google). Identify three practices they use and consider how you could implement them in future workplaces.
 
 




















