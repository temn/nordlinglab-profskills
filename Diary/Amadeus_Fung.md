This diary file is written by Amadeus Axel Fung F54105050 in the course Professional skills for engineering the third industrial revolution.
# 2023-09-07 #
* I really enjoyed my first lecture and happy to meet Mr. Nordling.
* I hope this coure can picture my future work prospect as I major in Environmental Engineering.
# 2023-09-14 #
* The few graphs that were shown filled my cup with water.
* I belive that EVs will be the future, but not as fast as 2030.
# 2023-09-21 #

* Hans Rosling's presentation was the best presentation that I have ever seen, it was beautiful and mesmerising.
* I would defenitely watch Hans Rosling's talks and learn from him in the future.
* I didn't understand about Bitbucket at first, but now I do.
* The Markdown Cheatsheet is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.
# 2023-10-19 #
* I have zero knowledge about finance. I had no idea what to say when doing the homework. But luckily, I can understand what my classmates' presentation about, as their graphs were clear. 
* I really like the words "Hatred is born of ignorance. Fear is its father and isolation is its mother". It is when facing a problem, going into potholes, we need to face them not fear them. 
* It is okay to fear problems, but not okay to let them fill up. When facing problems, we need to find the solution then face the problem when light and map is in our hand.
# 2023-10-26 #
* I was really excited when watching Wendy Suzuki's TED Talk, I have always been having exercises in between tests and felt my brain can process lessons more, and she confirmed about this
* I am going to have more exercises from now on to be more healthy.
* I am really glad there was a data about what makes students nervous are tests, and that makes mehave a feeling of I am not alone facing tests.
# 2023-11-02 #
* I was confused about claims and evidences, I wrote evidences as claims. Luckily my team mate did a research more than I did.
* Not every people are open about their depression. Until eventually when the depression becomes very big, they tend to meet God early.
* We need to be aware of our surrounding and friends, they might need our help, they might need someone to talk to. Because people with depression, they just need someone to listen to them.
# 2023-11-09 #
* I found out a lot of people have depression and anxiety. I think I need to look after my friends and environment more.
* Although I understand that depression and anxiety is an important thing to solve, I still don't see the connection between depression and anxiety with professional skills for the engineering world, our class big topic.
* Apparently the best source of learning is from our co workers, besides experience and failures.
# 2023-11-16 #
* I presented about social relationships connection with stress. Meeting friends and loved ones are one of the keys to prevent stress.
* We may feell like not needing friends but we are humans after all, that needs social relationship. So maintaining friendship and relation is key.
* Many many groups presented about the importance of sleeping. I have been always ignorant to sleep especially when midterm season. I now need to rethink my sleep schedule.
* I think I will have more sleep before biology tests that requires memory, because many of the groups presentated about sleep can restructure and reinforce memory stems.
# 2023-11-23 #
* Now i understand about noise limits. I have always thought that noise limits had only a bit of regulation, but apparently countries do take noise limits seriously.
* Apparently noise limits are only for noises that are extreme. If we report our neighbor that had a little period of peak that was over the noise limit, we can only tolerate them.
* I think when all of us are instructed to group we got shy because we never got to see each other in person
* Social media has changed our definition of happy. Some people have their happiness rely on likes and views, and it is not healthy.
* Dependency on gadgets hit me. I just realized I needed my phone everywhere I go and I feel like I can not do anything without phones in my pocket.
* Our world is too big to change, and we are too small to make impact. The one thing we can do is to focus on ourselves and people around us.
* The scatter chart for the teachers were very very funny. Apparently there teachers who got 100 for their first examination but 1 for their second. It is not relly consistent.
* Although the scatter chart really does scatter, I still can see there is a linear line forming.
# 2023-11-30 #
* I feel like it is fascinating that during covid-19, we all were part of history, alongside the black death and the Dark Age.
* The more chaostic an event is, the more we need to be careful about.
* Reference is key, seeing only from one source of news is not really wise.
# 2023-12-07 #
* Now the Israel and Palestine conflict is clear. I have always been ignorant because I am always confused of HAMAS and hummush, the food.
* From looking at their map, the only way for the conflict to end is either Israel stop their attacks and make peace treaty or for Israel to finish obtain all the lands.
* It is good to know about CO2 rate on every lecture room to avoid O2 runout.
* Global warming, one of the world's renowned problem, apparently is only one of nine planetary boundaries. I hope with what I am studying now I can make positive changes to the planetary boundaries.
# 2023-12-14 #
* I am very surprised that when having less children make up to number one on the CO2 emission, although it makes sense.
* I am happy that my thought of using electric cars makes less positive impact to the environment than it is advertised. To make real impact, taking public transportations are better.
* From seeing few ideas from different groups, it is impossible for me to exchange my time for studying with sleeping. Sleeping is important, but tests are way more important as it have relation with the future.
* Although before the test I sleep very little and it is not healthy, staying up late to study then catching up some sleep after the exam is better.
# 2023-12-21 Thu #
* Successful and productive
* I felt productive because I managed to get to the laundry for a dry clean, something I have been very lazy for.
* I tried finding ideas for public speaking exam but there was no outcome
# 2023-12-22 Fri #
* Successful and productive but not productive
* I was late for christmas dinner because I did not managed my time good enough to buy christmas presents.
* Christmas dinner was fun and successful
# 2023-12-23 Sat #
* Unproductive
* Woke up late for meeting, then spent my day sleeping again
* Only went out during the day for meeting, then go home, then eat with friends
# 2023-12-24 Sun #
* Successful and productive
* Spent a bit of time during the day to study for next day exam
* Went to the the church for christmas eve mass, it was cheerful and joyous
# 2023-12-25 Mon #
* Successful and joyous
* Christmas day, the moset wonderful time of the year
* Studied not so well, but I still can do the test even though not the outcome was not maximum
* This year's christmas is not really christmassy due to the was a test during the day
# 2023-12-26 Tue #
* Successful and productive but not productive
* This day's test went great, I will definitely pass the course
* After the test I went sleeping for the entire day
# 2023-12-27 Wed #
* Sucessful
* The test I had went well, the ball bounced perfectly for the PE test
* Met my friends and ate together to discuss future meetups for the rest of 2023
# Things That Make Me Productive #
* Plan a time schedule for the tomorrow during the night before
* Write what to do, visuals are better than memories for productivity
* Take breaks and not freak out, as thinking about problems will not solve the problem, but actions do
* Focus on one thing at a time
* Turn on do not disturb mode, close youtube


