This diary file is written by Christy Vianda B54095024 in the course Professional Skills for Engineering the Third Industrial Revolution.

# 2023-09-07 #

Back to school!

* It had been raining cats and dogs all week long, presumably due to the typhoon Haikui passing by Taiwan.
* I vaguely remember feeling under the weather, thus ended up missing the first lecture this day.

# 2023-09-14 #

*  The Professor started off this week's class by quickly going over the topics we would discuss in this semester, and I remember being slightly overwhelmed by how quickly things were developing.
*  I was concerned about my ability to keep up in class, especially due to my major, which is not at all related to Mechanical Engineering nor Engineering in general.
*  One of my teammates who did attend class the previous week informed me about a group assignment we need to finish by this week.
*  Never ever did I think I would be giving a presentation on stage this early into the semester. Needless to say, I was pretty nervous about the whole thing.
*  But looking back now, I actually think I did the right thing by volunteering to present, since I could get valuable constructive criticism from the Professor, which would definitely be of help for my future assignments.

# 2023-09-21 #

*  We finally started writing diaries today. I haven't wrote a single diary entry since last year because I kept on failing to keep this habit up, so this little classroom assignment was surprisingly fun and refreshing to me.
*  I thoroughly enjoyed Group IV's presentation, and found it interesting how they chose to contrast each country's happiness rankings with their suicide rates.
*  Regarding suicide, I was surprised by the Professor's critical remarks toward the whole issue. He pointed out that typically, suicide is actually a cry for help in disguise, to which I completely agree.
*  As a young adult, there has been many instances in which me and my friends would be plagued by depressing, even suicidal thoughts, from time to time. However, when we talk about wanting to commit suicide, we actually don't mean it.
*  What we wished for, instead, was for our pain to go away. And sometimes when we were trapped in a rather dark period of our lives, it could be hard to see a way out. To the point where suicide seems to be the only answer.
*  In other words, since happiness is a rather abstract concept that is generally hard to define, one should instead ask the question of whether life has been improving. Only then we will start to measure something meaningful.
*  Even decades after his Ted Talk was published, Hans Rosling remained a fantastic statistician. He presented his data with a lot of visual aspects and made it fun to observe the changes in the graph, even for someone who is not interested in the topic in the slightest.
*  Another interesting example worth learning from was Mona Chalabi's visualization of statistics, which made it way easier to grasp and understand the point she was trying to make.
*  Christiane Amanpour is a well-known television host I've come to recognized even before this class. Once, she famously said, "There are some situations one simply cannot be neutral about, because when you are neutral, you are an accomplice", and I cannot agree more.
*  She then proceeds to challenge the notion of 'objectivity', that even though it means giving all sides an equal hearing, it doesn't necessarily mean treating all sides equally.

## Homework ##

For starters, I could see myself living somewhere between Taiwan and Indonesia. By then, I would hopefully have found a stable job as a Chinese teacher or translator. The reason behind this is because as of right now it is the one of the few jobs I can actually see myself doing for tens of years, which was also the main reason why I went ahead and chose such an unconventional major as Taiwanese Literature in college. In recent years, the demand for Chinese language proficiency has been increasing, and being able to at least communicate in Chinese has become such a huge plus point for a lot of companies. And so parents would have their kids start learning Chinese as early as possible.

Interestingly enough, I used to think that teaching professionals, or teachers in simpler terms, would be one of the first professions to be eliminated due to technological advancements, or AI in particular. However, now that I think about it, Google Translate was introduced way earlier before AI, yet teacher as a profession remains as stable as ever. With that being said, perhaps the universal need for human interaction is why people still prefer actual teachers to robots.

A lot of things would be different ten years from now. Yet in my opinion there will be a few trends that would continue until then, namely: Artificial Intelligence, Electric cars, Avant-Garde Fashion, Cashless payment methods, and Digitization of information.

*  **Artificial Intelligence:** As I've said before, the development of AI in recent years has been groundbreaking, if not revolutionary, proved by the immense popularity of ChatGPT. Unfortunately, it was also frequently misused by students who wanted a shortcut to finishing their homeworks.
*  **Electric Cars:** In Taiwan, electric cars and scooters are quite literally everywhere. They are incredibly convenient, and make little to no sound at all, so sometimes you don't even realize a car is right behind you. Seeing how many people have switched to electric modes of transportation made me envision the future as a place full of them.
*  **Avant-Garde Fashion:** As someone who is interested in fashion, I've noticed a new trend featuring Avant-Garde wear. People have long treated fashion as an art expression, and with that the expectation for fashion pieces in runways have inevitably become higher. This phenomenon encourages fashion designers to come up with new original ideas that are unusual, or in other words, Avant-Garde.
*  **Cashless payment methods:** On the other hand, Indonesia was one step further in regards to cashless payment methods. Some businesses even stopped allowing cash payments altogether. From what I see, this would then lead to what we call digitization.
*  **Digitization of Information:** Nowadays students prefer iPads note-taking apps to paper and pen, and teachers have started to opt for eBook versions of teaching materials.

The article I have chosen is 'Trump says he's pleased by Putin's praise: ‘I like that he said that' from the Blindspots column.

*  The reason was simply because I was intrigued as to how could there possibly be no lean right news sources opposing, or at least covering this news?
*  A little side note: The column was probably named 'Blindspots' because if we only get our news from one side, it would result in us having a blindspot when viewing the whole incident.

# 2023-09-28 #

Moon Festival Eve

*  No class today!
*  I just realized it was supposed to be mid-autumn already, that's why the weather has turned chilly these days.
*  My roommates all went out to celebrate. I stayed behind to finish some paperworks by myself.
*  I decided to try out a new pizza joint for lunch, and the owner was kind enough to gift me a 蛋黃酥. Yum!
*  The next day a Taiwanese friend invited me to a barbecue party with her family, and my heart was filled with gratitude and warmth.

# 2023-10-05 #

*  No class today.
*  Really strong winds yesterday night, my dorm windows were ratling like crazy.

# 2023-10-12 #

*  Money, is obviously, something I'm very interested in.
*  I was excited to learn about how the monetary system work, but unfortunately today's lesson felt too theoretical, thus came off to be a bit confusing overall.
*  Turns out majority of the world's countries agreed to keep their currencies fixed to the dollar, and the dollar fixed to gold.
*  Maybe that's why we look immediately to the dollar when we want to know how the economy's doing.
*  But now the currency is backed by absoltuely nothing except our belief in it having value.

# 2023-10-19 #

*  I've always known about the word 'fascism', but only now does it occur to me that I don't really know what it means.
*  Fascism and nationalism both highlights one's dedication to his country.
*  The fine line, however, lies in the fact that fascism takes no regard to any other things in life.
*  In a sense, fascism sounds more like a religious belief, rather than just a political ideology.
*  Instead of God, one worships his own countrry.

# 2023-10-26 #

*  After the Industrial Revolution, mass production makes it easier to make people want what one produces, rather than produce what people want.
*  Hence the main reasoning behind advertisements.
*  Mental illness such as anxiety and stress could be as debilitating as physical sickness.
*  Understanding a term and having to explain it in other words are, in my opinion, very different.
*  Matthias Mullenback's hypothesis on Health-care system is certainly controversial, and I honestly don't know what my stance is on this one.

# 2023-11-2 #

*  Depression, though a hard topic to talk about, is definitely one we must first understand before we can act on it.
*  I know from first-hand experience just how hard it is to have a friend who's depressed and suicidal.
*  On one hand, I want to be by their side and help them go through this phase of their life.
*  But on the other, them constantly feeling negative took a toll on my mental health, too.
*  Prof said, "You don't have to be there for them all the time; You just need to be there when you can," and I think that's well said.

# 2023-11-9 #

*  At times depression may not have any symptoms at all, that's what makes it a difficult illness to diagnose.
*  We usually make decisions based on our feelings, and then rationalize it afterwards.
*  I reckon forgiveness is no easy task, but when one forgives another I'd like to think that they heal in the process too.
*  A meaningful life is something I wish for, more than anything.
*  I've been able to connect with my teammates well, and I'm looking forward to doing the final project together.

#  2023-11-16 #

*  Our group had a physical meet-up yesterday to discuss today's presentation.
*  To my surprise, all members showed up and the meeting went smoothly.
*  It was a very productive discussion! I can't wait to implement our action ideas.
*  In another note, I think it will be very empowering to see how our action affects actual peple.
*  The weather has been getting chilly these days, perhaps winter's finally here? I sure hope so.

#  2023-11-23 #

*  At lunchtime I waited in line for nearly an hour to be seated, only to be served so-so food.
*  I was appointed to be our group's representative to present our homework.
*  For some reason, I was extremely nervous and couldn't even maintain eye contact properly.
*  I agree that countries aren't doing enough to prevent global warming, or even try very hard to.
*  However I wonder if I'd be any different if I were in their shoes.
*  If we have age restrictions for cigs and alcohols, why don't we have age restrictions for mobile phone & social media then?
*  Coincidentally I'm trying to restrict my mobile phone usage this past week, as I find my screen time being way too high.
*  Remove the temptation, and it becomes easier!
*  Keywords that intrigue me: Superficial Relationships, Self-Gratification, Slowy Steady Consistencies
*  Simon Sinek is a terrific speaker, and the video was very informative and interesting. I'll surely rewatch it once I get home.

#  2023-11-30 #

*  Previously mentioned, as I've been trying reduce my screen time to 2 hours a day, this challenge is practically perfect for me.
*  I would probably uninstall Instagram sometime later this week, as it's affecting my focus in a rather negative way.
*  A month 'till New Years! I can't think of where to go this year, though.
*  The fireworks show at Taipei 101 has been in my bucket list for a long while, but I'm not sure I want to go to Taipei just for said event.
*  In any case, I hope it'll be fun times this year too.

#  2023-12-7 #

*  The conflict between Palestine and Israel is definitely a topic I'd like to delve into.
*  Media are usually pro-palestine, and that led to third-parties being biased without knowing what really went down.
*  My schedule has been quite hectic recently, to say the least. I hope things settle down quickly...

#  2023-12-14 #

*  This week's presentation was, needless to say, very hurried and rushed.
*  In the team, I was assigned to interview someone regarding the topic of sleep and to inform them on our action idea.
*  In all honesty, it was a really interesting experience which helps me learn more about my interviewee, whom in this case is a friend.

#  2023-12-21 #

*  An interesting task has appeared! Let's start it off with a successful, productive day!
*  Despite the cold weather and my warm banklet preventing me from ever leaving my bed, I managed to drag myself to my 9 a.m.
*  Read approx. 100 pages of Killing Commendatore, and is, to put it mildly, very intrigued by the plotline.
*  A quote I underlined: "Would I be able to find even a shred of affection for myself?"
*  Without delving into much detail, I'd just say the above sentence resonate deeply with my current state.
*  Didn't check my phone that much, either. My plan to cut down on social media has been successful thus far.
*  Will try to keep up the motivation for the next few days! (Or weeks, or months, or, if possible, for ever.)

# 2023-12-22 #

Winter solstice; and a night to remember.

*  Worked for 4 hours straight and earned myself some pocket money! :'
*  After having Thai Food for dinner, I went to a gathering/buffet held in my department building.
*  I had previously known about this event by chance when I was scrolling through our website.
*  Honestly I was contemplating on whether or not I should go, but thankfully I decided to come anyway.
*  The participants were mostly Master's/Ph.D. International Students, with a few teachers joining us as well.
*  Interestingly enough, though I've never met any of them before, somehow our conversations flowed naturally through the night.
*  We sit together in a makeshift circle while taking turns introducing ourselves and talk about our experiences in NCKU, or just Taiwan in general.
*  It's difficult to put into words, but I wasn't as nervous as usual, and poured my heart out when my turn came.
*  Luckily, a lot of them gave me supportive nods every now and then, encouraging me to continue.
*  The occurences that happened today left me with a pleasant feeling inside, and warmed me up thoroughly in the cold weather.

# 2023-12-23 #

*  Unproductive but somewhat successful (?)
*  Went with a friend to Kaohsiung to check out the new Don Don Donki store, but it was a letdown.
*  We spent more time queueing up than actually strolling around the store.
*  Despite not doing anything productive today, I still view it as a successful day.
*  Taking breaks is necessary at times, to prevent one from getting burned out.

#  2023-12-24 #

*  Yet another Unproductive/Somewhat Successful day.
*  Went to church in the morning, but I felt a little bit out of it for some reason.
*  Had a hotpot night with friends. The weather's really quite chilly today, and it's the perfect time for hotpot.

#  2023-12-25  #
We wish you a merry christmas~~

*  Ironically an unproductive and unsuccessful day. 
*  Spent the entire day holed up in my room.
*  It didn't feel much like Christmas, honestly.
*  Randomly stumbled across the new hunger games and within a few minutes I was hooked.
*  I should've done something about my college apps, anything at all, but I gave in to procrastination again.

#  2023-12-26  #

*  Productive but unsuccessful.
*  Had class in the morning and afternoon, did HWs after school, yet I still felt oddly unaccomplished?
*  It just feels like no matter how much I did, there would still be new sets of responsibilities awaiting me.

#  2023-12-27  #

*  Productive and successful.
*  I finished my part in a group project and progressed substantially towards my college apps.
*  Honestly this morning I felt a bit numb, somehow. Perhaps I'm severely burned out and needed another break, but I know this is not the time for that.
*  Luckily in the evening I became fired up and actually accomplished a lot while I'm in the flow.
*  Feeling super happy and fulfilled right now, hoping for a good day tomorrow.

#  2023-12-28  #

*  Productive but unsuccessful.
*  A pretty rough day, overall. I caught the flu and don't feel like doing anything today.
*  I managed to be somewhat productive, but honestly I just can't wait until winter break comes.

#  2023-12-29  #

*  Productive and somewhat successful.
*  Started my day early, and got quite a lot of things done today.

#  2023-12-30  #

*  Unproductive and unsuccessful.
*  Feeling a bit burned out, and did nothing of use today.

#  2023-12-31  #

*  Unproductive but quite successful.
*  Spent the day with close friends, ate hotpot and watched the annual new year's eve concert.

#  2024-1-1  #
First day of 2024!

*  Productive and somewhat successful.
*  Studied a bit for the upcoming final exam. Fingers crossed I do well.

#  2024-1-2  #

*  Productive and successful !!
*  Got a 2000 word essay done in one sitting. Spent the rest of the evening watching YouTube and eating good food.

#  2024-1-3  #

*  Productive and successful, I guess.
*  Did some final preparation for the final exam tomorrow.

#  2024-1-4  #

*  Productive and successful,
*  Last day of classes ever, yay!