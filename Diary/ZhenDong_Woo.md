This diary file is written by Zhen Dong Woo D44095016 in the course Professional skills for engineering the third industrial revolution.

# 2019-09-08 #

* Sadly I missed the first lecture
* I was on a flight to TPE

# 2019-09-15 #

* I was a little confused because it seemed a little too overhelmingly technical to me
* I haven't get to use GIT before
* However aside from the technical parts, the content of the lecture was very entertaining

# 2019-09-22 #

* On the **lesson**
	- We live in an age of information, and fake news can be weaponised by the ill-intentioned
	- I already have Ground News installed on my mobile, and I am glad that I'm using a reputable source of news
>
* A **summary of (my) future**
>As a Singaporean studying in Taiwan, Singapore and Taiwan are both viable options for living in the future, although Singapore stands a higher chance if the geopolitical situation (war across the Taiwan Strait) is taken into consideration. As a bilingual person proficient in English and Mandarin, I see myself working in areas that requires the usage of multiple languages, especially the translation sector. As China’s rise to the global stage become more prominent, there are increased levels of communications that are required between the Chinese-speaking world and the world (or specifically the Anglosphere, which includes the US), and to reduce the barriers towards communication is an area I’m interested to work for.
>
>The five current trends of change that will affect me the most are as follows: Remote work, geopolitics in Asia, falling birth rates, rising life expectancy and worsening mental health in youths. 
>
>Firstly, as the Covid-19 pandemic has shown us that remote work is possible for many jobs, many employees are preferring to work remotely, which has two implications, one is that in-person contact will be reduced, which reduces stress unrelated to the work itself; the other implication is that commuting to work will be no longer required, and that saves costs (financially and mentally, imagine commuting during busy hours!).
>
>Secondly, due to the rise of China, war across the Taiwan Strait is bound to happen if China thinks it surpasses the US in terms of military. War will disrupt my plans in Taiwan and certainly cause an economic downturn across Asia, and even the world.
>
>Thirdly, falling birth rates would mean that there will be lesser future population to support the current population when it inevitably ages, and that could mean a looser social security net.
>
>Fourthly, which relates to my third point, is rising life expectancy. With a looser social security net, living a longer life means that more plans are needed to ensure a comfortable life after retirement (or will there even be retirement?).
>
>Lastly, mental health amongst youth is said to be worsening. There were suicides that happened within our campus here in NCKU, and as a Gen Z I can feel the pinch where many of us do not see a secure future ahead of us and that leads to our unhappiness, and at its worse depression for some.
>
>In conclusion, out of the five changes that I mentioned, only the first point seems to be a positive one, whilst others signal that there are many challenges to be overcome, failure to find solutions for them could mean a morbid future life for me.
>
<br/>
* **News story** - "As shelters fill, NYC weighs tents to house migrants"
	- [Syracuse NY (Centre): "As shelters fill, NYC weighs tents to house migrants"](https://www.syracuse.com/state/2022/09/as-shelters-fill-new-york-city-weighs-tents-to-house-migrants.html)
		- The city is considering setting up tents as it struggles to find housing for an estimated 13,000 migrants
		- It is likely that the facility will have its tents heated, due to cold weather
		- Overall, the number of people staying nightly in New York City's homeless shelters had fallen in recent years, partly due to the Covid-19 pandemic, leaving the system unprepared
	- [The Guardian (Left): "New York City mayor plans giant tents to house migrants sent by Republicans"](https://www.theguardian.com/us-news/2022/sep/23/new-york-city-migrants-tents)
		- International migrants who have been bussed into the city as part of **a campaign by Republican governors** to disrupt federal border policies
		- Arizona and Texas **officials are being accused of deceiving passengers** on the bus
		- Overall, the number of people staying nightly in New York City's homeless shelters had fallen in recent years, partly due to Covid-19. That led city officials to reduce shelter capacity
	- [New York Post (Right): "NYC to set up large tents, prepare cots to house mass influx of migrants"](https://nypost.com/2022/09/22/nyc-to-set-up-tents-prepare-cots-to-house-flood-of-migrants)
		- More than 2,000 **additional** migrants — predominantly asylum seekers **fleeing Venezuela** — had streamed into the five boroughs over just the last week
		- The temporary buldings will be weatherized and able to withstand the cold and snow
		- The administration **declined to provide a price tag** or an opening date for the new facilities
	
	Leftist news focuses more on the faults of the Republican officials whilst rightist news emphasises more migrants and the cost of facilities
	
# 2022-09-29 #

* The lecture focused mainly on macroeconomics concepts
* I've taken economics classes before and thus am not unfamiliar with them
* As the Federal Reserve increase interest rates, the US dollar will continue to rise against other world currencies
* Against the backdrop of high inflation across the world economy, food prices have been rising rapidly in Taiwan where Taiwan dollar has also been dropping against the USD
* I've learnt that the Taiwanese central bank is less transparent as compared to US' Federal Reserve

# 2022-10-06 #

* I learnt that in order to present graphs meaningfully, exponential graphs are preferred over linear ones
* It is rather shocking to be realised that democracy is in shambles and needs change
* I feel that it is not easy for a person who was brainwashed with neo-Nazi ideology to distance himself away from it, but the man still managed to do it

# 2022-10-13 #

* The lesson has reminded me that getting physically active is crucially beneficial to our mental health, as it stimulates our brain
* This further helps me to better my studies as I am more able to focus and think critically
* There is a need for a mindset change from "sick-care" to actual "health-care" where prevention is better than cure

# 2022-10-27 #

* I personally feel that big group discussion online is a little bit too chaotic for me
* It is important to recognise depression for self and peers
* However, it is a touchy subject and I don't think I enjoy it too much

# 2022-11-03 #

* Dealing with depression is not an easy task
* I feel that playing games with friends helps me tremendously, as it involves healthy (happy) connections between people
* Suicide is not the way

# 2022-11-10 #

* It was interesting to hear constructive (pun intented) criticism towards the practice of the construction industry
* Maybe this is when we should hold politicians accountable to ensure sustainable construction of buildings
* There were some hurdles for the group project, but luckily it was resolved with our professor

# 2022-11-17 #

* Lots of assignments are due next week
* I hope I am able to bear through this
* Luckily the weather is becoming cooler and it helps a lot

# 2022-11-24 #
* Carbon-related problems are rather complex to solve
* Taiwan has to find a way to reduce carbon emissions by 2030 to avoid being taxed
* I think global warming is a systematic issue and it is hard to solve it based on the individual level

# 2022-12-01 #
* Unfortunately, during the bigger group discussion that happened, not everyone was able to partake in the discussion due to the lack of understanding of the topic
* Few of us had some ideas during the discussion and eventually I presented my own on regarding NHK's stance on the Ukraine war
* If we look at the graph disecting the Earth's different atmospheric layers, it seems that humanity has got a lot of problems that are largely neglected
* Is it up to us to change the world? Or the people with actual power (ie. politicians)?

# 2022-12-15 #
* Headaches are becoming more common, not sure if it's due to the weather and/or stress of work

# 2022-12-22 #
* It's the Dongzhi festival today, but unfortunately I'm abroad so I do not get to eat tangyuan together with my family
* I didn't buy tangyuan to eat them for myself either
* I need to rush the PowerPoint slides and get it done by tomorrow for a presentation

# 2022-12-23 #
* I had a presentation in class today, it went surprisingly smooth as thought I would be too nervous for it
* The professor for the lecture seemed very intrigued by my "foreignness" presented in the topic

# 2022-12-24 #
* It's Chirstmas eve and the dip in temperature is becoming obvious to me
* It's no wonder that people would like to snuggle up with their closed ones during Christmas as it is the coldest time of the year
* I've decided to order a hot cup of cocoa to warm myself up

# 2022-12-25 #
* Merry Christmas, it's lucky how this year's Christmas falls on a Sunday as it is not a public holiday here in Taiwan
* It's still hard to get used to how Christmas not being an official public holiday
* Christmas gives me a mental pause for the busy schedule I have been on

# 2023-01-07 #
* It's the final lesson and it's time for the holidays
* I'll be catching a flight on Monday