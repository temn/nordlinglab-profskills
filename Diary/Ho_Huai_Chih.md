12/19

Somewhat successful and productive:
After the class, we had dinner together with the capstone group and work on it for one last time for the last presentation, which is the v.1 of the model. We worked on it for over 3hr and there’s still something that needs to be tweaked
And I went back home and worked on the new material for 3D printing until the next day.

12/20:

Somewhat unsuccessful and somewhat unproductive:
So continue on the last note, I worked on one of the many parameters for the 3D slicer and print multiple test prints until 4am. Hop into sleep and wake up in the next morning feeling like I dug myself form the grave. After class, I went back home and drove back from my hometown for the capstone session tomorrow

12/21:

Successful and productive:
I woke up at 12 and I feel good about the fact that I finally had a good night of sleep for a whole two weeks. I took my time to wake myself up and worked on the capstone model with my group mates until 8pm. I did a lot of work but still lacking a bit for the final model.

12/22

Successful and productive:
I worked from 10 to 5pm to finally finish the model, just need to wait the groupmates to upload the work.

12/23

Can’t really judge 
I drove back to the campus last night and took a train back to campus. After class, I went back home to work on the new material for 3D printing, and I finally got the right value for the printer and the material.

12/24 

unsuccessful and unproductive 
I woke up late and did almost nothing for the whole day, but thankfully, another roll of material just arrived, and I must tune my printer to match the new material

12/25:

Somewhat successful and productive 
Finally got the right value for the printer and got the first print out form using the new material.

12/26 

Successful but unproductive 
After class, I worked on the final presentation for the capstone project and got the final v.1 model ready. But I have to rely on my group mate for the ppt presentation, so I felt a little guilty.

12/27 

Unsuccessful but productive 
Got to class early but the teacher wasn’t there, and it came out to be a break because the professor was absent. Worked on the programming homework and got back home for the new year

12/28

Unsuccessful and unproductive 
I did nothing irl because there’s nothing to work on and I took pleasure for taking a day off for doing absolutely nothing

12/28

Successful but unproductive 
Hop online into a game that I wanted to play with friends for a while after some study, and earned a heck ton of money in game with some help from friends

12/29

Successful but unproductive 
Same as the day before but make more money then ever. And I guarded my fellas for the missions and they’re all happy with it

12/30

Successful but somewhat unproductive 
Prepared a little more then the day before for the finals and hop on game for the night

12/31 

Successful and more productive 
Prepared more for the finals and, yes, hop on the game with the boys until new year

1/1 

Successful and productive 
Participate volunteer activities in a shrine which I’m a part of. Make some new friends and improve my social skills in the event.




