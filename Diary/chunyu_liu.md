2024/09/19

Today, we had our first presentation activity. I learned a lot from the speeches of others, especially in organizing logical flow and presenting charts effectively.

While preparing for the presentation, I found myself feeling overwhelmed by the process of gathering information and often didn’t know where to start. Watching other students present their ideas so clearly and comprehensively made me admire their ability to deliver data and visuals with such professionalism. This showed me that I still have much to learn and improve.

2024/09/26

This week’s topic was about fake news. The professor shared three methods for identifying misleading statistics:

Do you see uncertainty in the data?
Do you see yourself in the data?
How was the data collected?
These methods reminded me of a book I once read, which discussed similar ideas. It pointed out how we often trust data at face value, failing to notice its inherent biases and limitations. For example, if a dataset lacks explanations of uncertainty, should we take it as absolute truth? Similarly, we should question whether the data genuinely represents everyone or just a subset of the population. Finally, the way the data is collected may reflect hidden biases that directly influence its reliability. These reflections underline the importance of critical thinking when interpreting information.

2024/10/17

Today’s lecture was about the financial system. The professor introduced some interesting facts about deposits and loans, such as gold being a stable safe-haven asset and the risks of a bank's loan-to-deposit ratio exceeding one.

What struck me the most was the professor’s example of how, during Chinese New Year, the amount of money held by the government spikes due to the tradition of giving red envelopes.

I also learned a new word, "fungible," which refers to something that can be replaced by another of equal value or quantity. Money is a prime example.

This reminded me of a book I read about how money, as a medium of exchange, is also a constantly evolving societal tool. Its fungibility makes it a crucial driver of progress but can also exacerbate problems like inflation and wealth inequality.

2024/10/24

Today, a classmate gave a presentation about hatred between China and Taiwan, which left a deep impression on me.

During our discussion, I struggled to understand some of the English conversations among group members, leaving me a bit lost. However, seeing others actively share their opinions and inspire one another made me appreciate the power of collaboration.

The presenter mentioned how historical events continue to shape today’s cultural biases, reminding me of the saying, "history repeats itself."

I felt, though, that the presenter approached the topic too seriously. Perhaps we lack the capacity to solve these complex problems, and merely complaining adds unnecessary negativity. Finding common ground, rather than emphasizing divisions, might be the first step toward meaningful change.

During class, one of my classmates shared their thoughts, and I found their courage admirable. However, the content of their ideas felt overly radical and somewhat impractical—ambitious yet lacking a realistic foundation. It reminded me how important clear and achievable goals are when presenting ideas to others.

We also watched a video by Yuval Noah Harari, who argued that we live in a “fictional reality” created by our own systems. While it was an interesting perspective, I personally prefer the structured society we live in today rather than the primitive, tribal lifestyle he hinted at.
2024/11/14

Yesterday, our team held an in-person meeting to discuss today’s presentation. To my delight, all members showed up, and we came up with many practical ideas.

During the preparation, I researched a lot about exercise, the brain, and health, and I found an unexpected study result: short bursts of high-intensity exercise, such as sprinting or jumping, can significantly boost creativity. What’s more surprising is that the effect can last for several hours. Research suggests that this might be due to increased blood flow to the brain and the release of endorphins after exercise, both of which encourage innovative thinking.

This discovery gave me a fresh perspective on our project plan and made me realize that exercise could play a much more profound role in daily life than I had previously imagined.

2024/11/21

The Subjective Nature of Misinformation
The biggest challenge in addressing misinformation lies in its subjective nature. History is full of examples where ideas once dismissed as misinformation later became widely accepted truths.

For instance, in the 17th century, heliocentrism—the idea that the Earth revolved around the Sun—was considered heretical and condemned by the Catholic Church. Similarly, in the 19th century, Hungarian doctor Ignaz Semmelweis promoted handwashing to reduce hospital infections, but his ideas were ridiculed and rejected by his peers.

These examples show that the definition of misinformation changes with time and prevailing beliefs. Today’s misinformation could very well become tomorrow’s truth.

Misinformation in Modern Politics
In today’s polarized political landscape, misinformation becomes even more subjective. One party’s “truth” is often seen as manipulation by the opposing side. This creates cycles of misinformation and counter-misinformation, making it difficult to establish a universally accepted narrative.

This divide also presents challenges for social platforms, which face criticism no matter their approach: removing content risks accusations of censorship, while leaving it unchecked could amplify harmful misinformation.

Striking a Balance
The solution lies in finding a balance. Instead of outright removing content, platforms could provide disclaimers or links to verified information, enabling users to access context while maintaining freedom of speech.

Additionally, transparency is crucial. Platforms must clearly explain how and why they moderate content, fostering trust among users. This balanced approach protects freedom of expression while minimizing the harm caused by misinformation.

2024/11/28
Today's class focused on final group projects, where each team shared their objectives. We also discussed balancing freedom of speech and combating misinformation, and we conducted an experiment by handing in our phones to stay focused.

Additionally, we analyzed historical data showing how cities take longer to recover from "dark ages" over time, raising the question of how long recovery might take if another dark age occurs.

2024/12/05
Today’s lecture covered fascism and climate change. We discussed how fascism uses appealing imagery to lower people's defenses and analyzed the impact of climate change on planetary boundaries, such as rising temperatures potentially causing severe global economic downturns. Strategies to mitigate extreme weather and achieve sustainable development within planetary boundaries were also explored.

2024/12/12
Groups presented action plans for tackling environmental challenges, using tools like WBS and Gantt charts to outline tasks and goals. We also watched Jeremy Rifkin’s The Third Industrial Revolution documentary, which highlighted a collaborative economy based on renewable energy and digital innovation, emphasizing the importance of sustainability and systemic change.

This week’s discussions underscored the importance of teamwork, clear planning, and aligning actions with sustainability goals to address complex challenges effectively.

2024/12/19
Today, I stood at the front of the class to answer questions. We each took on roles as workers, capitalists, or environmentalists. Although I sat at the worker's table, I shared my perspective during the discussion: inequality is necessary because true equality is unrealistic. Everyone is different, and complete equality doesn’t align with reality.

this is my mood this wwek
2024/12/20

Today is successful and productive. Our group successfully finalized the timeline for the final project and assigned tasks clearly. Everyone contributed valuable ideas, and I feel confident about the progress. Additionally, I completed my personal tasks earlier than expected, leaving extra time to organize my review notes.

Evaluation: Today’s success came from smooth teamwork and efficient communication. My time management also allowed me to accomplish extra tasks.

Tomorrow’s Plan: Organize the framework for the final project report.

2024/12/21

Today is successful and productive. I spent the morning reorganizing my notes from recent lectures, which helped clarify some concepts I found confusing. In the afternoon, I attended a seminar on sustainable development, where the speaker shared innovative approaches to addressing resource scarcity. These ideas inspired me to bring new insights to our group project.

Evaluation: The day’s success came from effectively managing time to resolve academic challenges while gaining practical insights from the seminar.

Tomorrow’s Plan: Write the draft for the final project report.

2024/12/22

Today is successful and productive. Out of the blue, I had a sudden craving for a burger, so I decided to bike an hour round trip to get one. Although it was tiring, the burger was incredibly satisfying, making the trip absolutely worth it!

Evaluation: Today’s success came from indulging in a simple yet fulfilling desire, which brought me joy. The bike ride also served as a refreshing activity for both body and mind.

Tomorrow’s Plan: Review course materials and prepare for exams.

2024/12/23

Today is successful and productive. I refined the visual elements of our group presentation, creating a cohesive and polished slideshow. My teammates were pleased with the outcome. Additionally, I attended a workshop on time management, where I learned practical strategies for balancing multiple deadlines.

Evaluation: The day’s success came from completing a creative and detailed task and gaining valuable insights on time management.

Tomorrow’s Plan: Proofread and submit the group report.

2024/12/24

Today is successful and productive. I finished a long-overdue essay draft and helped a classmate revise their portion of our group project, which deepened my understanding of the topic. In the evening, I joined a study session and reviewed several key points effectively.

Evaluation: Today’s success stemmed from completing a delayed task and boosting my confidence for upcoming challenges.

Tomorrow’s Plan: Finalize the essay draft for submission.

2024/12/25

Today is successful and productive. Despite the festive atmosphere, I stayed focused and completed the final revisions of our group report, ensuring it met the highest standard. In the evening, I attended a holiday gathering with friends, where we shared our favorite memories of the year.

Evaluation: The day’s success lay in maintaining focus during a celebratory day, completing important tasks, and enjoying quality time with friends.

Tomorrow’s Plan: Prepare for the final presentation.

2024/12/26

Today was a mix of frustration and admiration. Due to issues with my laptop, I went to the city to consult a repair service and found out it needed to be sent back to the manufacturer. Instead, I opted to reinstall and reassemble everything on-site, which took so long that I missed class.

Later, I was notified that I hadn’t submitted my daily journal, which made me feel quite dejected. When I logged into Bitbucket to check, I couldn’t find my files, possibly because I missed something important along the way.

That said, I’m incredibly grateful for my group members. They are so skilled and always seem full of energy and enthusiasm. Seeing how hardworking and passionate they are makes me a bit envious but also inspired to do better.

2024/12/27
Today was successful and productive. I focused on polishing our final project report, ensuring the content was cohesive and impactful. Reviewing each section reminded me of the importance of clarity and vision—qualities I want to embody as a future leader. Completing this step felt rewarding and reaffirmed my determination to excel.

2024/12/28
Today was successful and productive. I dedicated the day to reviewing notes for my upcoming exams. Reflecting on how far I’ve come this semester motivated me to push even harder. One day, as a dean or principal, I want to inspire students to find the same drive within themselves.

2024/12/29
Today was successful and productive. I met with my team to rehearse our final presentation. Their enthusiasm and collaboration were inspiring, reminding me how effective teamwork is built on shared goals. I aspire to foster this energy and synergy in any team I lead in the future.

2024/12/30
Today was successful and productive. I spent the day refining my public speaking skills for our final presentation. The thought of addressing larger audiences as a future principal motivates me to improve my confidence and articulation. Each practice session brings me closer to that goal.

2024/12/31
Today was successful and productive. Wrapping up the year, I reflected on the progress I’ve made—both academically and personally. My ambition to become a leader capable of inspiring trust feels more tangible than ever. This reflection fuels my determination for the new year ahead.

2025/01/01
Today was successful and productive. I began the year by organizing a detailed plan for the semester’s remaining exams and tasks. Structure and foresight are essential traits for a leader, and I’m committed to honing them further.

2025/01/02
Today was successful and productive. I finalized our presentation materials and reviewed the feedback from earlier drafts. Attention to detail and adaptability are qualities I admire in leaders, and I strive to embody them in my work.

2025/01/09

Today marked the last day of finals (my Friday course completed its final exam in 2024). I scored 94 on the exam for this course. While it’s a result to be proud of, I can’t help but feel a little undeserving—it seems almost too good for the amount of effort I put in, leaving me with a sense of guilt.

Additionally, something happened today that left me quite unsettled. After the results were released, one of my classmates immediately went to the teaching assistant to ask about specific details, completely ignoring the professor who was still speaking at the front of the room. I find this behavior intolerable. It reflects a mindset where grades are prioritized above all else, overshadowing the importance of basic respect and courtesy. It’s disheartening to see how some students in Taiwan’s education system are so fixated on academic achievements that they neglect essential human qualities.

Reflecting on this course, the most impactful moment for me was when the professor spoke about his children and the future of education. His cautious and heartfelt tone revealed a profound sense of care and responsibility, which deeply moved me.

As this course comes to an end, I feel a mixture of gratitude and reflection. The professor’s wisdom, the discussions, and the lessons have not only expanded my academic knowledge but also shaped my perspectives on leadership, respect, and the purpose of education. This class has been more than a series of lectures—it’s been a call to action to become a better person and, someday, a better leader.

This scene prompted me to think about my own future. As someone who aspires to take on significant responsibilities, I must lead by example, inspiring those around me to value respect and character development. While academic performance is important, what truly matters is whether we understand the importance of empathy and mutual respect.

The professor’s words have left a lasting impression on me. His passion for education and his concern for the next generation transformed this class into more than just an academic experience—it became a lesson in life. I hope that one day, I too can influence others with both wisdom and compassion, using education as a force to truly change the world.

At the same time, I aspire to be a genuine leader—not just someone who excels academically or professionally but someone who can shift the mindset and actions of my peers. I want to move beyond the obsession with grades, fostering an environment where respect, unity, and collaboration take precedence. In the future, I hope to set an example for the next generation, helping them understand that the true purpose of education lies in cultivating character and wisdom, not in chasing superficial achievements like high grades.