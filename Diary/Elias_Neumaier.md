This diary file is written by Elias Neumaier P06138406 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #

* In the first lecture, it was very interesting to get to know the teacher, the professional career and to get an impression of the colleagues, studying this course with me.
* The teacher has been a little bit challenging, just handing the microphone to some people, but I think it's an important step to pull people out of their comfort zone (and part of the course's purpose)
* The course outline was very interesting, covering a very wide range of topics, but it seems like there will we very different things to think and talk about.
* In the "try-out" exam, the course's expected content was pointed out even more. However, it was very frustrating to know (much) less than one was anticipating. But that's a part of why we are all here!
* The group formation went very good! Fortunately, my group mates were very open and approached me at the end, so we could dive right into starting to organize ourselves.


# 2024-09-19 #

* Speaking about the conflict behaviour and potential outcomes has been very interesting to me. Although I think that I am aware what kind of a conflict person I am, the potential outcomes with different counterparts was very interesting and will have an effect on my reflection in upcoming conflicts.
* The escalation model was an interesting point regarding the steps, as they come after another. However, I could not fully internalize that after a certain point, one cannot win anymore and after a further point that both cannot win anymore, since it does not consider differing human behaviour in certain conflict situations. 
* After some failed "GitHub" attempts in my Bachelors, GIT finds the way back into my studies... However, it seems more logical using it again and might be very handy, once I know my way around.
* Although I'm not happy to use yet another typing language Markdown (after spending the summer with LaTeX), it seems much less complicated and should not pose any challenges
* Today was also presentation day - the first time using a microphone for me. After a short discussion in the end, iit went quite good overall. However, there was some feedback from following presentations that we also need to improve towards next week
* The most interesting part of the lecture was Wright's law for me. It clearly showed how the pace of our industry and economy has increased, compared to a decade ago, which was very shocking to see. Of course, we experience this everyday (in certain ways), but seeing the facts and figures behind is truly impressive.
* After all, there was unfortunately a little confusion that had me leaving my old group and teaming up with two new study colleagues. Although it's unfortunate to end the old collaboration already after a week, I'm also looking forward to meeting two more people

   
# 2024-09-26 #

* During the first part of the lecture, we focused on the 17 Sustainable Development Goals (SDGs), defined by the Bill and Melinda Gates Foundation
* Out of the development goals, it shocked me that goal number four "Quality Education" is one aspect where we do not only lack behind, but we are even taking steps backwards. While it might be driven through the increasing immigration, it is also rather clear to see how our school system hasn't changed much (at least in my home country Germany). Moreover it was interesting, that a previous class mate presented that poverty is decreasing, while education is improving, which is the opposite of the statistics from the SGDs
* Moreover, we watched three videos of a journalist, and two persons, working with statistics
* The free jounrnalist was the most concerned about fake news and unethical topics being easily distributed over the available social media platforms, where no journalist filters out unethical or wrong information
* This journalist also proposed some "moral technology" (AI) to filter out such fake and unethical topics. While I think it is a good intention from her own profession behind this idea, I believe it is extremely challenging to set such a technology up properly and would significantly reduce available (also true) information, depending on the persons in charge
* Mona, working with statistics highlighted in her talk how important statistics are, but also that they are very quickly misused by companies to just show the right numbers, instead of the numbers right. Here, it is always critical to check the axisis, know the bigger picture and where the statistic comes from
* Regarding statistics, questions that stood out in creating statistics were: Which parts of the country shall we go to? Whom shall we speak to? Which questions should we ask?
* Moreover, she posed three main questions that shall help to spot a bad statistic: 1.Can you see uncertainty? 2.Can I see myself in the data? 3.How was the data collected?
* Lastly, we listened to a video, showing trends in life expectancy and family sizes and how that developed over the last years. While it was initially visible that countries with bigger families tend to have a lower life expectancy, those countries have made a change, moving to smaller family sizes and higher life expectancy. 
* Furthermore, it was interesting to see how certain continents are often considered as one (e.g. Africa), while they have certain countries lacking behind and others, standing out 
* Over the last hour of the lecture, we spoke about fake news, how to detect them and how to handle it. This is still a very new topic, since one tends to trust the public media such as BBC and Focus online. However, while I still believe that they are rather free media platforms, it has definitely openend my eyes regarding other information that circles the internet


## Task 1: 5 trends of change and in how far they affect my life in 10 years

1. Sustainability and green technology
2. Artificial intelligence and automation
3. Digital transformation and remote work
4. Electrification of traffic
5. Healthcare and AI-driven diagnostics

In 10 years from now, it will be 2034, and I will be 34 years old. By then, all the moving around during my studies would be over and I expect to live somewhere in southern Germany around Stuttgart. There, I would wish to be back in Bosch Home Comfort, where I could already work for several years. In their business, the core are heat pumps, which pose a strong potential in the future. 
Although their job market doesn't look too promising today, I believe that this will strongly improve again, since sustainability and green technology are a major trend, that we all can exerience today. So, in contrast to the situaton today, I hope that this trend of change would get me back into Bosch Home Comfort, to be a part of this journey again.
A second trend of change is artificial intelligence in the context of automation. When also looking at my potential job at Bosch Home Comfort and the manufacturing facilities, where I already worked, automation is lacking very much as of today. The production volume is still rather small and doesn't provide enough revenue, compared to e.g., the very prominent car industry in Germany. However, automation becoming more and more available, combined with the 1st trend of change, this industry sector could experience a strong boost in this aspect.
Another trend of change is the digital transformation and remote work. Already today, employees and employers have discovered the opportunities in remote work. Thereby, the private life can strongly benefit, since I could save up to 60 min of a daily commute. It enables us to be at home more often, spend our coffee breaks with our kids and be at home the second after work. Despite the opportunities our employers give us, the 5G extension (in Germany) improves the required basics such as a stable internet connection continuously and lays an important foundation for that. 
However, once it's time to get to work, many aspects of the traffic might have changed by then. For once, the share of electric vehicles will have continued to increase, leaving fewer combustion engines behind. So either I will have a new car by then, or be a rarity on the "German Autobahn". Despite the change in only the drive-train of the car, it is also uncertain how the "car ownership" of today will look like. Some bigger citites already provide a car-rental service, just as if it was a cheap e-scooter (such as Miles in bigger Germany cities). Correspondingly, it will be interesting to see if this trend also reached my own life by then. Furthermore, the whole traffic aspect might also change in a way that autonomous driving will move into our daily lives more and more. Driving assistances are already part of every modern car, but will certainly expand to cheaper models and also expand in terms of their capabilities. 
Finally, healthcare and AI-driven dignostics is a strong improvement potential for the coming 10 years, where I anyways hope that I will not need it yet. Today's doctors are often confronted with situations where they need to identify some patterns of a tissue or patterns in a patient's data. The introduction of AI would pose the immense potential of uniting countless patient's datasets in a central database, where and AI could detect some patterns and correlations to learn from old patients to give hints on the diseases of new patients. While everybody obviously wnats to stay healthy, it could be a strong improvement, that e.g. family members could benefit from. 


## Task 2: One fake and one real picture

In the following overview, there are three pictures pointed out. The first picture is the one that has been published at the time, the second picture is the proof why this was fake, and the third picture is the original and unedited picture.
The pictures are about a test-launch of missiles through the Iranian military in 2008. By publishing an edited picture, the Iranian government wanted to show their capabilities when launching "four" missiles and tried to hide, that one of the missiles had a failed launch.

1. [link](https://www.researchgate.net/figure/Manipulated-photo-of-the-Iranian-missile-launch-30-a-manipulated-image-b-original_fig2_328180774) (last accessed on 2024-09-27)
2. [link](https://archive.nytimes.com/thelede.blogs.nytimes.com/2008/07/10/in-an-iranian-image-a-missile-too-many/) (last accessed on 2024-09-27)
3. [link](https://www.denverpost.com/2008/07/10/iran-doctored-missile-test-photo/) (last accessed on 2024-09-27)


## Task 3: Different sources on: "US Presidential Election 2024: What are the odds of Kamala Harris replacing Joe Biden?"

In the "election year", American media is very often cited in Europe to represent the trends and changes during the run for presidency of the Democrats and Republicans. As Donald Trump was settled as a contestant at an early point, Joe Biden got into more pressure regarding his age and mental health, and whether he should really be running for presidency again. During that time, his vice president Kamala Harris was already discussed as a potential replacement for Joe Biden in the run for the next period.
On the given website Ground News [link](https://ground.news/), three sources from about 2 months ago were shown for this question. Hereby, I found it particularly interesting that the articles were from one American source, and furthermore from a Russian and Ukranian source. Although I have no experience about the Russian and Ukranian source, I wanted to investigate the different reporting of three countries, often discussed together regarding the Ukraine war.

1. American: "The Economic Times"
* national surveys and betting markets are losing faith in Joe Biden and show a trend that Kamala Harris becomes more likely
* Harris is named as expected candidate to replace Biden on the ballot box
2. Russian: 
* There are many Harris sceptics among the US democrats, and the opinion that Harris would not gather enough votes
* Attention is drawn to Michelle Obama as a potential alternative candidate
* Overall any younger candidate would have a higher chance against Trump than Biden would have
3. Ukranian:
* High pressure on Joe Biden, especially after a "failed" TV debate that pushes him to resign from run for presidency
* Harris (although not beloved at the Americans) would score 3 percentage points more than Biden
* Just replacing Biden would lead to an increase of younger Americans to vote 

Overall, the sources alltogether were sceptical towards Joe Biden. After he was replaced in the meantime, this has shown to be a reasonable statement to make. However, comparing the reporting about Kamala Harris, who was a likely replacement candidate at the time already, the Russian newspaper was the only one to point out that Harris might face some sceptics among potential votes, and that overall a younger candidate would score higher chances than Joe Biden. So, her advantages of having been vice president were not pointed out, which is however an advantage that she has towards other alternative candidates at the time. 
However, overall all newspapers remained very neutral and were not polarizing on one or the other potential outcome or candidate. 


# 2024-10-03 #

* The today's lecture was unfortunately cancelled due to the typhoon warning in Tainan. Still, there was a questionnaire and a task to deal with the topic on our own
* In the questionaaire as well as the task, it was about money. This tackled what money is, where its value comes from and how it has evolved and changed over the past decade
* I found this topic particularly interesting, since it is something that we use in our everyday life, but where I still need to think about what it actually is
* Through some travel adventures and living abroad, I could already experience the value differences of different currencies, but still there is much more to explore
* Although we would probably skip a session due to National Day in Taiwan again, I hope to discuss this topic in class anyways


# 2024-10-17 #

* In today's lecture, we started with the diaries and the "10 year visions" of our course mates. Here, it was very nice to heat what they expect and dream of. The main difference I noticed to myself was that they were more honest about their dreams on where they want to be. Although I had similar wishes to some of them, I was not "honest enough" to mention this here as well
* Regarding the last lecture, we made a recap of the homework to identify fake pictures. The shown example was a picture of the earth from a space mission, which was actually a picture merged from several ones taken. It was obviously very hard to notice but even more interesting to see, how easy modified or fake pictures encouter us during our lives
* Aterwards we discussed the lecture's topic, which was money. We started with a historical insight how money's value was linked to gold as tangible good, and how Nixon decoupled money from gold. This was very intersting, as we are very used to money being standing alone today, but to see how different that was only half a decade ago
* In the further course of the lecture, we watched some videos of people that carry responsibilities in the financial field. Hereby, the banking system (having central planning and decentralized decision making) works, and that centralized system used to fail in the past
* A very interesting takeaway was that money is actually created, once a loan is issued. While this means that anyone can initiate money being created, I still decided to remain very thoughtful about loans that I might take at any time in the future.
* In the last video, it very interesting to see that money being spent actually drives the economy. Although I was already familiar with this, the effects and consequences were pointed out in much more detail


# 2024-10-24 #

* In today's lecture, we discussed extremism and racial profiling
* I was very curious about this lecture, since extremism often brings aspects, that are hard to comprehend for a "regular person", where it is accordingly interesting to see some videos to get insights
* In one of the videos, an American young boy was "found" and radicalized at times, when his parents didn't give the right attention, that he needed. Although his parents were foreigners themselves, he soon began to riot against foreigners and became violent, without any other than racial reasons
* The particular part that was very concerning is just the fact, that a radical movement can identify young and lost kids, give them a purpose and they would follow. While you can't argue against the vulnerable boys at this point, it's just scaring but also extremely sad in what situation some people are, and what opportunity they take as their "way out"

* Afterwards, we watched a video of Yuval Noah Harari, where he elaborated on that we live in a fictionate reality and in systems that we created - just animals would live in the real reality
* My main perception here was that he certainly has a point about all the systems we created, and that we encounter in our daily lives. However, there was honestly not much more I could take from this video. It might be true that we have many things that are not naturally grown or here since eternity, but calling this a fictional reality is a quite strong statement from my perspective. Also, there was no conclusion on what we shall do different, but just spreading this word. So, in the end it was interesting to get this point of view, but I still prefer living in our society, compared to the tribes that humans used to live a long time ago

* Afterwards, we discussed assumptions, hypotheses and the rejection or acceptance of this hypotheses, leading to falsification or knowledge.
* Here, I find the thought experiement very interesting, that just a single falsification can reject a hypotheses, whereas you need countless approvals to label a hypotheses correct. However, we agreed in the lecture that a reasonable number of hypothesis approvals can be considered as overall approval of the hypothesis.
* Here, our professor also mentioned that even Newtons laws are disproven, as they don't consider Einstein's relaitvity theory

* In two videos, we learned about the effect of exercise on the body and the health care system
* While the statement of exercise having a positive effect on the whole body is nothing new to me, it was interesting to go into more detail and I got the impression that there is much more about it than simply to take the stairs
* In the redesign of our health care system, a system was proposed that is supposed to actively keep people healthy than to treat them when they're sick. Hereby, incentives shall be designed to give reward for everyday that someone is kept healthy.
* Although this sounds very interesting, it's hard to imagine if this would be feasible, given the capacity of the health care system and that it even struggles to just "look" at the sick patients. But maybe the propsep improvement potentials of AI would be a good help here


## Three rules on how to live healthy

In the end, I would like to present three rules or tips, which I believe I should stick to most to, in order to live a healthy life. 

1. Find your purpose and fulfillment - Engaging in meaningful work or hobbies boosts satisfaction and mental well-being, contributing to a balanced life.
2. Strive for social connections - Strong social connections can improve mood, reduce stress, and contribute to a longer life, according to studies on social support and mortality
3. Seek adequate sleep - Adults should get 7 - 9 hours of sleep per night. Consistently good sleep is linked to better immune function, mental health and a lower risk of chronic diseases

The already mentioned part of exercising does obviously apply as well, but is not mentioned specifically again. 


# 2024-11-07 #

* In today's lecture, we spoke about depression. It was a lecture, where we once again had some videos , but in this case about people that have somehow been experience depression and suicide. Since it was a topic that is usually very tough to digest, I also feel like the diary would be shorter today, as I was more listening and comprehending, than being able to formulate some take-aways out of each of it
* Throughout the videos, it was repeatedly said that depression is the leading cause of illness, according to the WHO. I was very surprised about that, since depression tends to be something, that people rarely speak about, and that is often perceived in a weird way. While a broken bone is obvious to detect and accept as injury or illness, I feel like the society isn't that far yet to accept some constantly ""unhappy" people as being sick as well. 
* From the videos, one interesting takw-away was that mostly it's not the talking that helps people in need, but that we should rather just listen to whatever such people can tell us. Although I hope to never come across such a situation, this will very likely be something that I will recall.


## Questions regarding depression

1. How to recognize when a friend is depressed? --> It's sometimes very hard to recognize whether a related person suffers from depression or not. Such personalities also tend to be very bipolar, meaning that we can't notice anything in public, but that they have a second side of their feelings once they are at home. Otherwise, staying aware of each persons live and how things behave could help to spot potential triggers for depressive phases.
2. What to say to a depressed friend? --> Especially at the beginning / discovery or also when not being an expert, it is extremely difficult to know what to say. Therefore, it tends to be better to rather listen and let the affected person sort their thoughts in their head, by speaking it out loud. Just being there and listening to all they've been through helps to show that someone is still interested in them and values them as a person.
3. How to help a friend with depression? --> Not everybody is a psychiatrist, and just as that, friends don't everybody to make a therapy with them. For once, because I / we probably couldn't, but secondly, because affected friends need to be out in their normal lives as well, just doing everyday stuff and enjoying to be with people.


## Guide on how to overcome depression


1. How to spot and help a depressed friend --> Depression could come from various triggers, have different reasons and can become apparent in a variety of forms. An overall guide could not target how to detect or help a depressed friend, especially not in the length of this diary. However, the lecture could teach me, that depression is much more wide spread, than I could anticipate before. So, regarding spotting and helping a depressed friend, my main take-aways are the ones of the questions regarding depression just above, combined with the gained awareness that depression is widely spread across people where you wouldn't expect it.
2. Claims from the guide: Depression is widely spread, depression has different triggers and underlying causes, depression is affecting people where we might not expect it.
3. Everybody knows a person (1st or 2nd degree) that already had to deal with depressions. --> Just by speaking with friends, I could sadly confirm the hypothesis. Obviously, a wider survey could give more clarity, but with the gained knowledge that depression is the most common cause of illness, it is also not very surprising.


# 2024-11-21 #

* In today's lecture, we watched a very interesting video about algorithms and how they are used
* The presenter pointed out, how big data works and what it is applied in (e.g., insurance systems, credibility for loans, ...)
* Throughout the presentation, a major criticism was towards the fact that we only base these algorithms on the past, which makes it difficult to get rid of old, differently influenced "trends"
* A given example was a company, where women were much less promoted than men, which didn't change after the CEO retired, because the algorithm used the sex as a factor to distinguish the success potential of people
* While she mentioned a very crucial and dangerous point here, I am still convinced that the data we are gathering is a precious good, making many things more fair overall
* But of course, the information contained in the data must be used thoroughly and constantly revised from a purely human position, to validate the correct "behaviour" of used algorithms
* Ultimately, the amount of data is cnstantly growing. This gives enterprises the possibility, to use only the more recent data, to give a better representation of how things actually are in the past ~1-5 years


## Rules on how to reach consensus

* Everybody's opinion counts --> Listen to it carefully
* Be condifent in your own point of view, but value everybody else's
* Stay objective and don't go / speak against a certain group of people
* Respect other people's boundaries, don't get personal


# 2024-11-28 #

* Today, we started by a short discussion regarding the last week's content. I thought it was really interesting to discuss our consensus rules from the previous week, since I found it very hard to have rules that achieve all of our Professors given targets (e.g. freedom of speech vs. no false information)
* In today's lecture, it was mainly about the final group projects and what each of the groups want to achieve in the end. Here, I'm very curious to see the work in progress and what all of it is going to result into
* Furthermore, we had an experiment throughout the whole class of handing our phones in and trying to not use it for the entire time of the lecture

* Since I had to leave earlier for a private alignment, I could just rewatch the remaining content and couldnt't join any discussions
* Here, it was really interesting in a video about the recent conflicts between Israeli and Dutch people, that collided in the Netherlands. What I had heart so far was that the Israelis were attacked by the Dutch, while it actually seemed to have been vice versa
* This is really interesting, since too quick reports seem to lead to a wrong presentation of events (on purpose or by accident) 


# 2024-12-05 #

* In today's lecture, we started to present the comparison of newspapers of different countries, that we dealt with over the past week. Here, it was also my turn to present our findings for my group. In our case, it was a bit unfortunate that not too many were similar to other papers, which made it difficult to compare the reporting of each paper. However, one common headline was the movement in Syria, where Aleppo was taken over by some rebels and the government (al-Assad) lost control. 
* As this is still a very hot topic, it was interesting to look into it a bit deeper.
* Afterwards, we gathered in bigger groups and tried to compare even more headlines with each other, and to assess them with their political views how they were presented. However, as there were not any other presentations with more similar headlines, we analyzed the just mentioned headlines of the Syrian conflict. 

* In terms of lecture content, we dealt with the planetary boundaries.
* Here, it was stated that our temperature is constantly rising. Although this is not a new statement, it was the first time for me to hear that this might even have an effect on our GDP and can even lead to a drop of 38%, if we continue unchanged until 2050. 
* This is mainly through the increasing temperatures, which are currently at 1.2 degree per year, but could climb up to 2.7 degrees, if no counter measures are implemented. 
* What has been very interesting regarding the temperature incline is, that we would likely have a temperature overshoot, that would eventually settle again after many years. In all the forecasts that I saw about rising temperatures so far, this was one of the few ones that also accounted for a drop (after the overshoot).
* Furthermore, we discussed the role of heat sinks over our planet, which currently serve to keep the temperature at a stable level. Some of them, such as icebergs or other colder regions might get smaller and smaller and eventually tip, that they do not serve as heat sink anymore, and also contribute to other negative trends such as rising sea levels. 
* This, as well as the overshoot will probably not be avoided anymore, but there is still a high change to mitigate the consequences as good as possible.
* Overall, the main priority must be to focus on the temperature incline as well as the other planetary boundaries, and to improve them as much as possible.
* If that does not succeed, the temperature overshoot would likely lead to a period (50 - 75 years), in which we would experience hotter temperatures, more floods, winds, typhoons, and overall more extreme weather events. 


# 2024-12-12 #

* Group Debate on Planetary Boundaries: This week, we focused on planetary boundaries through a group debate about proposed actions for our final project. Teams presented action plans aimed at supporting or maintaining specific planetary boundaries, followed by a Q&A session where strategies were clarified. The session concluded with a vote on the most impactful idea.
* Key Insights from Jeremy Rifkin’s Documentary:We watched a segment of *The Third Industrial Revolution: A Radical New Sharing Economy,* which explored the global economic crisis and advocated for a collaborative and sustainable economic model. The documentary emphasized integrating digital innovation and renewable energy while aligning economic growth with ecological needs.
* Takeaways and Reflections: The group debate showcased the importance of practical solutions, clear communication, and collaboration in addressing planetary boundaries. Additionally, the documentary reinforced the need for collective action, technological integration, and sustainability to drive systemic change—valuable insights for our final project.


# 2024-12-19 #

* This week’s activities focused on the theme "How to Succeed," starting with a debate session led by Professor Nordling. The class was divided into three groups: the Capitalists, the Workers, and the Environmentalists. The main discussion centered on the question "Why does inequality exist?" The Workers group presented compelling arguments, attributing inequality to greed and providing examples such as CEOs maintaining high salaries during financial struggles while cutting workers’ wages. Their perspective resonated strongly with many participants.
* After the debate, Professor Nordling introduced several models for personal and professional development. The 7 Habits of Highly Effective People by Stephen Covey emphasizes principles like being proactive, setting clear goals, and prioritizing effectively. Ikigai, a Japanese concept, highlights the intersection of passion, skill, societal needs, and financial viability. Radical Candor by Kim Scott promotes balancing personal care with direct feedback to foster trust and improve communication.
* An assignment was introduced to help apply these concepts. We are tasked with maintaining a daily journal for a week, where each day is evaluated as successful/unsuccessful and productive/unproductive. The journal requires analyzing the reasons for these outcomes and identifying one actionable change to improve success and productivity the next day.


# Daily diary between 20th and 25th of December #

## 20th of December ##
* Productive, successful
* With the weekend ahead, it was time to explore Taiwan once again. This time, I rented a scooter (for the first time in Taiwan), and got the chance to show a very close friend around the beauty of Alishan
* It was unfortunate since our Yushan permit was cancelled, but we made the best out of the day and enjoyed a beautiful hike in best weather
* Furthermore, I tried fried squid for the first time, and was once again delighted by the Taiwanese kitchen
* As it was just the start of some adventures in Taiwan together, I was relieved to see her enjoying it. However, the Yushan troubles could have been anticipated better and handled with a two-day permit

## 21st of December ##
* Productive, successful
* Plenty of waterfalls around Chiayi and the Tsengwen Reservoir were a great invitation for a waterfall adventure.
* Unfortunately, we couldn't reach any during the day, since the accessibility was worse than expeected
* Overall, getting up a bit earlier could have given the time to be more persistent in hiking through the wildlife of the area. However, as we focused on staying safe, we were happy with the good experiences we could make even without waterfalls.

## 22nd of December ##
* Unproductive, successful
* After two active days, the return trip to Tainan led us past some well-known hot springs. With that still on my bucket list, we stopped there and enjoyed a few hours.
* Although it was very nicely made, that still left unnatural / untouched hotsprings on my bucket list for Taiwan
* Overall, it was more a travel day than a day to explore a lot. Still, with the rain coming in, it was just fine to keep it a bit calmer.

## 23rd of December ##
* Productive, successful
* Today was the final presentation in sustainable supply chain management
* I could use the morning to practice the presentation once more and got ready for the presentation. (Unfortunately) I ate a very good hot pot just before, which left me overly full for the presentation itself. Still, it went very well, which left one less item on the to-do list for the semester
* After some calm days, it felt very good to get some productive things done again

## 24th of December ##
* Unproductive, sucessful
* The day before christmas used to be very special and calm back home. While the feeling didn't quite come up here, we used the day for a little hike on the monkey mountain in Kaoshiung during the morning. While this was definitely different to all traditions, it is still a christmas day I won't forget that quick.
* In the afternoon, it was time for the regular last-minute shopping, which led to the festivities with a group of other European friends
* Overall, the expectedly calm day brought more than anticipated, but still a very good day

## 25th of December ##
* Unproductive, unsuccessful
* Usually not something I would mention anywhere related to university, but a diary is a diary... 
* After a good evening before, the day was rather calm. We started off with a big breakfast, and enjoyed great weather and the Anping harbor front during the morning. Afterwards, it was time to catch-up on some missed sleep.
* Although not much productive things were done, it was incredibly nice to have everyone around, to celebrate christmas as good as possible! 

## 5 Rules to be more productive and successful ##
1. Ensure you get enough sleep to maintain energy and focus throughout the day.  
2. Plan your next day in detail, assigning explicit time frames to each task.  
3. Spend time with friends to create enjoyable moments and something to look forward to.  
4. Minimize phone usage to avoid wasting time and stay more productive.  
5. Immediately complete tasks that take less than five minutes to avoid unnecessary mental clutter.  


# 2024-12-26 #
* For this lecture, I was unfortunately not able to join due to other conflicts
* From what I've heart, everybody was supposed to come dressed, as if it was their job interview. This would have been very interesting to observe but also to get feedback on, just since this topic had some dynamics in Germany, which makes me wonder how other cultures are with this
* Afterwards, they discussed how to tie a tie. Here, it is similar as just mentioned - in Germany, a tie would not be necessary for an entrance level job interview anymore. It would certainly make a difference, but very likely appear overdressed (at least in the field of Engineering)
* Finally, we received our last instructions for the final project presentation. This should be done in a short video, which is a very nice change in mode of presentation, but also very much more effort than just a presentation.


## 27th of December ##
* Unproductive, successful
* Today, I was on the east coast of Taiwan. There, it was the first time I could swim in some natural hot springs, which was truly amazing. Although it was "just hot water", nature can be very impressive and just made my day.
* On top of that, a deadline came to an end today. However, I did already manage to hand in this document a week before, which made these days rather calm
* Overall a very nice day, but as always - I could have gotten up earlier.

## 28th of December ##
* Unproductive, unsuccessful
* Today suffered from a very bad night sleep. Consequently, I didn't do much and just had some food and coffee here and there
* Although this was very annoying, there was not much to do than to enjoy the calm day as good as possible

## 29th of December ##
* Productive, successful
* Today, I got up very early to reach a spot to see the sunrise. It felt great being on the move so early, with the full day still being ahead.
* Consequently, I managed to go for a little hike around Keelung, which was an incredible scenery. Taiwan is just a very beautiful place.

## 30th of December ##
* Productive, successful
* When doing an east coast trip, Tarokko is usually a must-do on the list. However, the heavy earthquake months before seemed to make that impossible. 
* Furtunately, through the hint of a friend, we discovered that the park had some entrance times, where it was allowed to enter and drive through the park. It was only limited to the main road since the rest was still under construction, but still such an incredible day!
* It was very sad we couldn't do a nice hike. But being safe throughout the day was definitely the priority which made me a very cautious member of the group that day.

## 31st of December ##
* Unproductive, successful
* New Years eve was there, which is usually a very nice evening when celebrating at home. This year (and after a very different Christmas) the expectations were a bit lower. Anyways, people said that Taipei 101 is the plave to be, so we also aimed to see the fireworks there.
* The scenery at Taipei 101 was impressive - a full concert stage with changing acts over the evening, followed by a stunning firework
* The remainder of the evening was very calm, since we didn't find a bar that fit / suites us all. So in the end, a bit less than what I expected from "going out in Taipei"

## 1st of January ##
* Unproductive, unsuccessful
* The day was exectedly calm, especially after it started at 6am at the Taipei train station, with not having slept so far. 
* The train ride back to Tainan was very alright, but unfortunately, the favourite dumpling restaurant was closed for lunch that day.


# 2025-01-02 #
* Orbituary: Elias Neumaier - Loved and remebered by his family and friends - Dies in peace at the age of 89
* People that achieved it: Nobody I know that close has it on their grave stone like this. However, when experiencing grandparents of friends passing, that's what they're remembered for. So, although it would be a small achievement that would only concern my smalles circle, it's still the greatest you can leave behind.
* In today's lecture, we had the final exam as well as the presentation of the videos of the projects.
* Regarding the exam, I was very pleased to have improved my own score significantly, while it was fun going through all the topics in advance of the exam. I was also lucky to share the preparation with two others of the class and enjoyed discussing the questions.
* The shown videos were also very interesting. Some were more advanced regarding the editing "skills", while I felt comfortable in comparison to others. Overall, I am satisfied with the project, while some topics are still to be updated over the next days.


## Suggestion to learn the skill for each of the course's objectives ##
1. **Know the Current World Demographics**  
   Research a specific demographic trend, such as urbanization, using UN or World Bank reports, and identify its impact on engineering needs.

2. **Use Demographics to Explain Engineering Needs**  
   Use city-building games to simulate and observe how demographic changes influence infrastructure demands.

3. **Understand Planetary Boundaries and the Current State**  
   Connect your daily habits, like commuting or water usage, to specific planetary boundaries and identify ways to reduce your environmental impact.

4. **Understand How the Current Economic System Fails to Distribute Resources**  
   Participate in a simulated economy game to understand resource allocation and the impact of decisions on wealth distribution.

5. **Be Familiar with Future Visions and Their Implications, Such as Artificial Intelligence**  
   Use AI tools responsibly in studies or tasks and reflect on how they enhance your work while understanding ethical implications.

6. **Understand How Data and Engineering Enable a Healthier Life**  
   Track a personal habit, like mood or fitness, using an app, and analyze the data to identify patterns that improve your well-being.

7. **Know That Social Relationships Give a 50% Increased Likelihood of Survival**  
   Dedicate a week to fostering intentional connections, such as reconnecting with old friends or participating in group activities, and reflect on the impact.

8. **Be Familiar with Depression and Mental Health Issues**  
   Regularly check in with friends by asking meaningful questions and listening attentively to create a supportive environment.

9. **Know the Optimal Algorithm for Finding a Partner for Life**  
   Reflect on your past relationships to identify key values and traits that align with your long-term happiness.

10. **Develop a Custom of Questioning Claims to Avoid Fake News**  
    Trace one piece of information daily back to its original source to evaluate its reliability and learn to spot misinformation.

11. **Be Able to Do Basic Analysis and Interpretation of Time Series Data**  
    Gather data on a personal activity, like sleep or exercise, and use it to practice analyzing time series patterns and insights.

12. **Experience Collaborative and Problem-Based Learning**  
    Compare the outcomes of solving a problem individually versus in a group to observe the benefits of collaboration.

13. **Understand That Professional Success Depends on Social Skills**  
    Start meaningful conversations with people in everyday settings to practice listening and building connections.

14. **Know That the Culture of the Workplace Affects Performance**  
    Reflect on past experiences where the environment influenced your engagement and performance to understand workplace culture's importance.

