This diary file is written by Gabriel Chang E14096033 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #
* The first lecture was a little bit difficult to me owing to my terrible English.
* It really shocked me that we had to do the presentation every week, and I thought that would be a trouble.
* I exactly believe that the good teammates are acturally important in this course.
* Last, I sure that I need to train my English abilities, such as listening, speeking and writing.

# 2022-09-15 #
* Perhaps my English had a little improvement because I watched some movie in English, for example, Thor.
* I feeled sad for having no presentation on lecture.
* In my opnion, electricity may not be cheaper and better than gasoline because we generate electricity by combusting fossil fuel like coal. Consequently, there's still a lot of emission of carbide.
* On top of that, the power trasmission cannot avoid power loss because of heat trasfer.

# 2022-09-22 #
<p>In my assumption, in my future, I want to be a pilot or aviation engineer, and perhaps live in Seattle if I can become aviation engineer of Boeing, my dream job.</p>
<p>In my opinion, there are five current trends of change which can affect my future and I have to adopt them.</p>
<p>First of all, remote work or study. In present, thanks for development of internet, everyone, including engineers, can do their job everywhere, such as home, Starbucks, library, and so on.</p>
<p>Because of this change, we don't need to work in office, stay there from 9am, dress business casual. In other word, we just work after waking up with our delicious brunch, maybe.</p>
<p>However, due to this change, also, we may not lose our regular schedule and feel more unfamiliar with our co-workers, and I have got unfamiliarity with my classmate. By the way, with the facemasks, I definitely cannot recgonize some acquaintance on class recently.</p>
<p>Next one is automated transportation. One of the jobs I want to engage in is pilot, but nowadays automation is more and more advanced, maybe, one day, there are no driver on the ground as well as no pilot on the sky.</p>
<p>The reason why pilot is my dream job owe to my dad, who worked at Committee for Aviation and Space Industry Development more than 10 years ago. Owing to his past job, first time, I experience the gorgeous, spectacular, and powerful game, Flight Simulator X 2004 from Microsoft. That is an exactly great game, include its almost real landscape and flights matching aerodynamics, such as Boeing 747, 777, and Airbus families. Above of all let me like the plane, and I originally want to study development of aeronautics and astronautics.</p>
<p>The third change is our century, an information explosion era. The information and the knowledge update very fast, and we have to receive and learn those messages more quickly. To be competitive people, we must catch up the news every moment, or we would lose the connection to our friend, even the society.</p>
<p>Last but one, aging society, that is to say, our parents or grandparents. with age, our families need us to take care of them. Nevertheless, if I want to work at Boeing, I need to not only bring my families to Seattle, but also look after them. That will be a economic and time problem.</p>
<p>At last, inflationary spike. In addition to Taiwan, acturally, every country is facing the inflationary pandemic, and the higher and higher prices will be a big difficulty if I move to United State or other foreign region.</p>
<p>In conclusion, I wish my futrue live in 10 years can be fulfilling and unboring.</p>
<p>that's all.</p>

# 2022-09-29 #
* Financial system is difficult for me.
* The first video is really creative and vivid. I think she made the powerpoint by Goodnote.

# 2022-10-06 #
* This is the first physical lecture today.
* I very think my english listening should be improved.
* Physical presentation looks so horrible.
* In my opinion. there are two problems today. At first, our lecture delayed about 10 mins, and I was sad about that. Secondly, I thought the air conditioner was not cold enough. so I were sweating bullet.

# 2022-10-13 #
* I'm really nervous today because I answered two questions with my terrible English.
* I'm sorry about misunderstanding the problem meaning from classmate.
* To justfy truth is really important, and Newton laws are so familiar to me due to my major.

# 2022-10-20 #
* There were too many problems in the midterm exam of Automatic Control. I could not finish that in time, so sad.
* But the quiz of Electric is easier.

# 2022-10-27 #
* The topic of class today is a little heavy.
* I was deflated due to the frustrating news in our campus.
* The internet seems unstable. Professor's voice sounds like a robot.

# 2022-11-03 #
* There's a terrible new that we have a very big presentation next week.
* The classification of professtional skill, personal skill, and engineering science was confusing. I didn't really understand that concept.

# 2022-11-10 #
* Our group chosed a selected course project. That's terrible and means we have to do all work again.

# 2022-11-17 #
* We found a certain procedure to do our presentation.
* The good procedure can let us have adequate time to make our presentation better.

# 2022-11-24 #
* The task this week is really difficult because of our topic.
* The election was done, and it's time to go to interview the new councilors.

# 2022-12-01 #
* The midterm exams all ended, and I finally have some time to take a break.
* Computer Graphic is funny but very tough. I have to sit in front of my computer 6 to 8 hours averagely, just for only one homework.

# 2022-12-08 #
* The quiz of eletrical engineering is a little bit difficult. There' re to many things having to be remember.

# 2022-12-15 #
* There were to many thing I had to do this week, but I got a cold and my head was swimming.
* The medicine made me drowsy. I couldn't focus on my studies and works. That's really annoying.

# 2022-12-22 Thu #
- Unuccessful and Unproductive
	- I felt unsuccessful because I wrote the wrong question in my final exam of electronics. I felt unproductive because I failed to finish my homework of fluid mechanics.
	- The fiction is so funny and attractive that I lost time to study.

# 2022-12-23 Fri #
- Successful and Unproductive
	- I felt successful because I know the answer of the quiz of quiz today. I felt unproductive because I didn't study anything today.
	- As the situation yesterday, I have to control myself.

# 2022-12-24 Sat #
- Successful and Unproductive
	- I felt successful because I almost finished my homework of Computer Graphic and studied. I felt unproductive because I waste too much time on fiction and spacing out.
	- The situation seems better than yesterday.
	- Merry Christmas eve.
	
# 2022-12-25 Sun #
- Successful and Productive
	- I felt successful because I finished my homework of Machine Design. I felt productive because I figured out every example today.
	- I wish I can stay productive until week after next.
	- Merry Christmas.
	
# 2022-12-26 Mon #
- Unsuccessful and Unproductive
	- I felt Unsuccessful because I wrote many wrong answer on the final exam of my GE and suffered from hives. I felt unproductive because I couldn's focus to study due to my itchy skin.
	- Hives is really annoying...
	
# 2022-12-27 Sun #
- Successful and Unroductive
	- I felt successful because I overcame the disease. I felt unproductive because I couldn't finish my homework of Fluid Mechanics due to my bad mood.
	- I said goodbye to her.
	- Maybe it's not successful today.
	
# 2022-12-28 Wen #
- Successful and Productive
	- I felt successful because the quiz today was kind of easy. I felt Productive because I not only finished my studies in time but interviewed the councilor .
	- The situation seems not bad today.
	
# 2022-12-29 #
* I'm feeling stressful this week. That were too many thing to handle out, including my personnal emotion, final exam, and uh... final exams.
* Fortunately, I have good friends. We can work and study hard together.
* The gibson 1959 is so fancy and wonderful. I hope I can earn enough money and buy this guitar someday.

# 2023-01-05 #
* There is only the final exam of fluid mechanics remaining.
* I hope that I will have a productive vacation.

# 2023-01-07 #
* The semester is completed, and finally we can enjoy our winter vacation.
* However, unfortunately, I still have so many thing to do.
* I wish I will have a produtive vacation.