This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Yi Cheng Lee e14096279 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I learned how to make ppt better.
# 2022-09-22 #

* Gender equality is still a promblem nowadays.
* It is nice to hear that we can have a phisical lesson nextweek.
* It is very interesting to discuss the presentation with my teammate.

# 2022-09-29 #
* I learned how to print the money legally,which is by buying a loan.
* The video is difficult to understand.
* economy is hard to understand but it is interesting.

# 2022-10-06 #
* This is the first physical lecture today.
* I very think my english listening should be improved.
* Physical presentation looks so horrible.

# 2022-10-20
* There is no class this week.
* I use the class time to do the homework for next week.
* I went back to Taichuang to visit my grandmaother yesterday.

# 2022-10-27 #
* The topic of class today is a little heavy.
* I was shocked due to the news in our college.
* There are a lots of test next week,I am very worried about it.

# 2022-11-03 #
* I bought a new shoes since my old ones was too dirty.
* Tainan has been getting colder recently.
* The professor taught a lot about how to deal with stress

# 2022-11-10
* Today was doing the project presentation, and everyone's presentation were very great.
* I skipped the class after professor went out the lecture at 4 p.m.
* Not until today did I know that we need to upload the link of video on Bitbucket.

# 2022-11-17
* Today I was being asked to go on the stage to talk about the time limit of presentation's country, and I felt very nervous since I didn't search for the time limit but volume limit.
* I thought that today's class was great.

# 2022-11-24
* Today I was going on the stage as a volunteer of presentation for responding the question.
* In my memory, there was one group that addressed the noise problem by buying the earplugs, and I thought that their idea was very interseting.

# 2022-12-01 #
* There are so many exams next week. I don't know how survive next week.
* I lost some money buying lottery tickets.
* I can't believe that Spain lost to Morocco in World Cup

# 2022-12-08 #
* Tainan is getting colder. Especially in the morning.
* We got a new group this week.
* It's almost the end of this semester, there are lots of exams. Hope I can pass all the subjects.

# 2022-12-15 #
* Today we have a debate on the question of inequality.
* I have learned what is gini coefficient
* I watched a movie with my friends.

# 2022-12-23 #
* I felt successful because I start to exercising after fininshing the homework.

# 2022-12-24 #
* I felt unsuccessful because I sleep too much and didn't have time to study the test.

# 2022-12-25 #
* I felt unseccessful becuase I spent a lot of time on studying mechanical engineering design and still get confused.

# 2022-12-26 #
* I felt unseccessful because I think I have failed the test.

# 2022-12-27 #
* I felt seccessful becuase I finish studying the test on Wednesday quickly and have a great time playing video games with friends.

# 2022-12-28 #
* I felt seccessful becuase the test was easy for me. I played basketball with friends after finish the test.
