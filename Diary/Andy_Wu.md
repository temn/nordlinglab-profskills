This diary file is written by Andy Wu E24085327 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* I've decided to continue on this course since it helped me improve my researching skill and get to understand what's going on in the world.
* For my group's previous presentation, I've learned not to give credits or mention a member's name if the person didn't contribute and is no longer a part of our group.
* I hope that this class would inspire me to see the opportunity and pathway to make in the future's society.

# 2021-10-7 #

* Money types I learned are officially issued legal tender or fiat-types, representative, and even bit-coin.
* Printing more money during Chinese new year causes price to increase.
* I learned that when we take a loan from the bank, money will be created and not borrowed from other people's account.

# 2021-10-14 #

* I like the idea of professor giving students the opportunity to get presentation points by creating videos instead of presenting directly in class.
* I hope that everyone would get the minimum points for the presentation. When everyone gets their minimum score, students who volunteer to present more will get extra points.
* Because there are less people who want to share their dairy, maybe those who volunteer can get extra presentation points.

# 2021-10-21 #

* I agree with improving health care instead of sick care. Why not take care of yourself to live healthier and longer instead of treatment after the discovery of one's sickness?
* Exercising 30 minutes each day helps improve not only your physical health but also your mental health.
* I will consider in changing my university lifestyle in terms of performing other activities, such as exercising and socializing, instead of studying too much each day.

# 2021-10-28 #

* We had to form a supergroup with 3 groups of 3 people, which there were about 9 people in the group. It was interesting to see how we interact with others by having online discussion because it was way different from having face to face conversation.
* We all face the conflict within ourselves, such as having anxiety and depression. Even though it is natural for us to feel those negative emotions, but I also believe that our genetics plays a role in the way we control our mental health.
* There's a popular point of view on suicide, which is "suicide affects people around you". Some people, like me, agree with the quote, but some feels if the quote is terrible as it means people only care about you when you aren't here anymore, and others are selfish in thinking they are more important than those who have suicidal thoughts.

# 2021-11-4 #

* We had a discussion on how to be successful when we grow up and have a job. It is better to know many people and have connections, because life would be easier when we help each other when facing difficulties.
* When you are new in your job's environment, if you don't like the place you are in, then you should quit 2 weeks later. Why wait longer to see if things improves even though there is a high chance that it would get worse.
* The upcomming group project is going to be challenging, since researches have to be made in the library and we had no experience in this before.

# 2021-11-11 #

* It was easy to cooperate with everyone in my supergroup, and each of us did our own tasks well. We held meetings 2 times at the library, and the experience of having group discussion in the discussion room was great!
* I had high expectations for our group's presentation, in which we did tons of researches and managed to think of 3 creative ways to help people in NCKU with the knowledge from our topic.
* For the upcoming supergroup presentation, I hope I can make improvements in researching faster and efficiently so I won't have to waste too much time being lost on what I have to accomplished.

# 2021-12-03 #

* I was lucky to be in a supergroup where everyone participated, and we also went to the library to have a discussion and get things done.
* Even though doing the presentation is super tiring, I got to understand more of the society we live in.
* It is also interesting to listen to others presenting, in which we learn a lot on other people's opinion and ideas.
* In terms of having our phones taken away, I didn't feel insecure that much because we weren't going anywhere anyway. The only problem was it wasn't convenient for us to take pictures of the assignment the professor showed on screen.

# 2021-12-10 #

* The environmental issue has been bothering me, such as global warming and huge amount of products being wasted.
* I learned about 9 planteary boundaries, and they are biosphere integrity, climate change, novel entities, stratospheric ozone depletion, atmospheric aerosol loading, ocean acidification, biogeochemical flows, freshwater use, and land-system change.
* One of the substances made by human that negatively affected the environment is pesticide, which is toxic to bees. With no bees, there will be no fruit, thus no food for all living species.
* One of the bad news I got is getting the topic that I didn't vote for and didn't want to do, but has to do, is the topic of "Convince church to stop noise that disturbs the students living in the female dorm".

# 2021-12-16 #

* The discussion we had was intense, since the professor asked detailed questions and the presenter must answer them seriously.
* The three charts we had to create was challenging, since we had no experience in creating those graphs before.
* I decided to vote for the goup that presented their idea of creating a social media page on instagram and deliver the concept publicly.
* My topic of "Convincing the church to stop loud noises" wasn't eliminated.
* Hopefully the new members who joined my group will contribute much.

# 2021-12-24 #

* (Successful and Productive) Today, I took an opportunity to ask my electric circuit professor questions on my worn out macbook pro mid 2012 charger with peeled of wire to see how dangerous it is. I've even asked him about if we should charge our electronic devices to the maximum, and at what percentage should we charge our devices. It was cool to talk with the professor on the topic outside of what we learn from class, and I hope to learn more stuffs that are more related to the real world rather than from the book.

# 2021-12-25 #

* (Successful and Productive) Today was the day where I got to know more about my new friend. Since my friend initially wanted to go to Anping's beach, I introduced her to Yuguang island instead because it is easier to locate. I studied the whole day even though it's Christmas, and later discovered how to celebrate alone without being depressed by refraining from social media. I recommend myself to overcome furthermore obstacles eventhough it takes time to resolve.

# 2021-12-26 #

* (Successful and Productive) Since studying a whole day is just another normal day for an engineering major like me, I tried to find something else to do today. What I did was to watch Spiderman: No way home with my close friend during late afternoon after studying whole day of electronics. It was the first time I bought movie ticket online, and surprisingly it was faster and more convenient than buying the tickets at the theater since there were too many people. I believe that one day technology would took over numerous jobs, but at least they are making our lives easier. I hope to find out new ways to improve my life with the help from technology in the future.

# 2021-12-27 #

* (Successful and Productive) Studied the whole day at the cafe during the morning, then afternoon, and went to NCKU Knowledge study area at night. 

# 2021-12-28 #

* (Successful and Productive) I almost finished studying electronics, but still have other courses to catch up to. Same as everyday, study all the time. I hope to take care more of my body, and walk around instead of riding the bicycle sometimes.

# 2021-12-29 #

* (Unsuccessful and Productive) Probably because of too much studying, but my expressing skill became worse when I want to give my opinions to the group members. The productive part was that I luckily finished my work at the end.

# Notes for 5 Rules that make me Successful and Productive #

* 1. Our brain is like a battery, we charge if we sleep, and use until the end of the day. Remember to sleep for more than 6 hours!
* 2. Study when you feel motivated, stop when you are stuck at something and do something else, such as walk around and socialize with others.
* 3. Make use of simplicity, since everything will be easier to organized.
* 4. Don't overthink and be more confident in your actions.
* 5. Break big problems into smaller parts to make problems easier to define.

# 2021-12-30 #

* We were judged based on our outfits, and it was interesting to see how each person dresses up. Some took an effort on wearing suits and some didn't even bother to change into the interview outfit. I hope that the final project won't be too challenging to do, since the noise created by church topic had been changed to investigating on the loud noise created from the construction area. It would be hard to implement the new goal.
* (Unsuccessful and Productive) Probably because of too much studying, but my expressing skill became worse when I want to give my opinions to the group members. The productive part was that I luckily finished my work at the end.
e
# 2023-9-9 #
* The first class was unique in a way where professor challenges us to think and answer questions related to the problem of climate change. I believe the world is approaching towards environmental changes, such as global warming, polluted sea water and an increasing in trash that are hard to recycle. The main problem is that people don't bother on using their time to fight or acknowledge the issue, since not much of people in their surroundings are doing it. I personally like the development of the technology because of their usefulness and efficiency in helping us performing tasks, but on the other hand, they are what causes climate change and pollution. If large industries are crucial for the humanity to achieve society advancement, I hope they would look forward to adopt concept of carbon zero or getting certificates for measuring the greenness of the company to the environment. 

# 2023-9-20 #
* After taking a gap semester due to the issue on my grades, I am back to studying this course again. I am confident enough that I will be more successful in passing my classes and graduate from the university. During the first class, I was surprised to see the professor had given himself a fresh haircut. He had improved in asking questions to students in class and make them participate by encouraging them to talk when he passes the microphone.

* Because I took this course halfway before, I felt I am experienced in knowing how to handle tasks and participation part in class. I knew I had to present in order to get the grade, which I took the chance during the second class and volunteered in presenting. The complicated past experience from last time, such as dividing presentation work and preparing my script for presenting, was no longer a burden for me. It would be interesting and fun to see how I will do this time for this course.

# 2023-9-23 #
* The most interesting thing in last class was meeting our group members and had a discussion on the topic our professor gave. Even though it is challenging for people who knew less about each other to collaborate and express opinions, sometimes we will meet people who have liked minds and are easy to interact with. Connecting with others is hard, because there could be different point of views on how one wants to do their task and contribute to the team. Luckily, my group members are friendly this time, and we got along with each other.

# 2023-10-15 #
* Last class, I learned a bit about economics. I really like the video on the explanation about money by Ray Dalio because of how easy it is to understand. When I saw the line graph or the curve for how we can increase our money, it is either you work hard by doing your job or getting loans from banks so that you can use it to start your own business, and then pay them back with some interest. People must work hard to produce a product and give value to the society, and in return, the reward is money. To have a stable economy and abundance in wealth, people should not have the mindset of money is the only solution to problems in life. Without products or services to be traded with money, money is worthless without value. This leads to inflation, so that people would work hard to create new values to the society, in which things and services become affordable to everyone and supply meets demand.

# 2023-10-19 #
* At the beginning of the class, one of the student forgot to do his diary, and he was told to present it in class. The professor told him to pass the microphone to others, like passing his problem to others, and nobody dared to look at him. Sadly, he handed me the microphone. His excuse was that I was his teammate, which I had no choice but to accept my fate. I then presented my diary successfully with some compliment from the teacher. Since I already went up to present my diary, I had the courage to present again on last week's assignment through powerpoint. After some presentations, which lasted for half of the class, the teacher made us watch Ted talk videos. It was like a presentation day on that day.

# 2023-10-26 #
* I like the second video, where the woman talked about how exercise will affect your health positively. I do agree with her, but due to tons of assignments and tests, I don't think I have time to go exercise. I did go to gym sometimes, and I realized my body would need 9 hours of sleep after, or I will be tired the next day. If I spend some time to exercise and rest, I don't think I would have time to study. For the other video, I agree with health care over sick care. But, sick care has it's advantages in long term for humanity, because technology and knowledge can both expand if new issues are introduced, such as disease, virus, and others. Overall, this class reminded me to focus on my health and don't exchange our precious body for grades. 

# 2023-11-2 #
* In this class, I arrived to the class late because I had to meet TA to check on my electromagnetic field and waves midterm exam paper. The class average is 38, but luckily, I got more than the average. The TA assigned the time to check on the grades in this class time period, so I had no choice but to skip a bit of the class and go ask for extra points. After everything was done, I arrived to the class and watched Ted talk videos about the topic of depression. Depression is a serious issue that could lead to suicide. I learned that to help others with depression, it is important to listen to them instead of talking to them. Now when ever someone is sad and I want to cheer them up, I will listen to them more.

# 2023-11-9 #
* I got to meet new people when we got the opportunity to sit in our big group. I actually like professor's idea of being in a big group, since there's a higher chance to meet people with similar energy.
* I believe the course would be even interactive if people would sit in a new group and discuss on a certain topic that the teacher assign. Maybe the second period of the class could be classroom discussion with the new group of the day, and the third period would be tedTalk videos with a little of discussion. 

# 2023-11-16 #
* Many groups presented their presentation, and I got to learn something from each of the presentations. 
* My group presented well, but it was overtime. Next time, it would be better if I inform my group members to speak for a certain amount of minutes, so that we don't take more than 15 minutes of presentation.
* Because our group had all members presenting, we had to decide who should get the presenation point, in which we gave it to a group member that never presented before.

# 2023-11-23 #
* About the laws on noise in Taiwan, it is not clearly stated how people would be fined. If there's a complaint reported, the authorities wouldn't do much about it since the problem isn't that big. In other countries, like Croatia, they have similar problems with noise, where it's hard to control noise in both places.
* The second part is about how being on your phone too much can be bad. When we get texts or play games on our phones, our brains feel happy, but if we do it too much, it's not good for us. young people, like teenagers, are having a hard time making real friends because they're always on their phones. It's important to learn about enjoying little things in life, not just on screens. To create real friendships, we should talk and hang out with people in person.

# 2023-11-30 #
* All the groups presented their action idea out. I was the one presenting on our group's topic, which is about displaying posters to remind students to get enough sleep to improve their study. 
* Some people asked questions on my project idea, and I tried my best in replying them. It was a good opportunity for me to practice my responding skills, since I don't get to do much within electrical engineering department.
* The group that I found interesting was NCKU cafe, since the idea was kind of cute in a way students get the opportunity to bond with one another within the cafe shop.

# 2023-12-7 #
* This class made me understand more on what's going on related to the issue between Israel and Palestine. I know that there's a war going on, but not the story behind it. After doing a quick research using chatgpt 4 powered Bing search while discussing with 2 other groups, I started to understand a bit of what is going on. It is still unclear because people have their own beliefs, and if I want to know more, I need to see from both perspectives and come up with my own opinion. 
* For the supergroup project, I hope that the implementation of the action would be convenient and effective in changing people's life. Let's see if everything will go well.

# 2023-12-14 #
* I had to represent my group and presented our planning on the project. It was tiresome because I spent a long time finishing the powerpoint. I actually learned that you really need like minded people with motivation and optimism to carry out the project well. If not, then you would be the only one doing all the hard work. It is important to have people that would help you in a team, so we need to be choose and find the right team members. 
* Doing WBS, Pert and Gantt chart was interesting because I felt like I was planning on a big project. A problem I faced was that I wasn't sure when is the deadline, the date we need to be finishing the project. This leads to scheduling problems for the Gantt chart.

# 2023-12-22 #
* Productive / successful
* I went to eat The Harbour at Nanfung, and I picked many healthy foods to eat, like fish and salad. I want to eat a lot of healthy food to prepare for intense studying later next week.

# 2023-12-23 #
* Productive / sucessful
* I studied in the library at a comfortable area. The lighting was great, and the place was quiet. I finished my study of the day.

# 2023-12-24 #
* Productive / sucessful
* I did my assignment and wrote essay for my general education course in Starbucks. The essay writing made me practice my writing skill. I finished my task successfully.

# 2023-12-25 #
* Productive / sucessful
* I took my first exam, and it went well. I later skipped a class to study in my department, which made me feel productive.

# 2023-12-26 #
* Productive / successful
* Studying in library was good, but the lighting at my seat was a bit dark, which affected my study.

# 2023-12-27 #
* Productive / Not successful
* I studied in the library this afternoon, but I realized after 2 hours of study, I started to study slower. Cafe studying suits me better.

# Five Rules To Be Successful
1. Set a goal and don't be lazy. Make a day productive because you will see a difference in the future when looking back at your past self.
2. Always be learning something new, where it could be knowledge or skill that will improve you.
3. Don't forget to take care of body, such as eat more, sleep more, and exercise more.
4. Learn how to communicate and interact with others because you will be working with people out there in the society.
5. Be confident with yourself because everyone has their own special quality.

# Summary of my Daily Lives #
* Exam week is coming and 2024 is arriving. I had to study as usual, but with lesser sleep to catch up with my lectures and prepare for the exams. I studied at the library, Starbucks, and Donutes, and out of them I like Donutes the most because the drinks and not too pricy and the place was cozy. I would study there for the whole afternoon and the whole night, which was super effective because I like studying at the cafe more than at home or at the library. I also invited my friends to study and do assignments together. I am hoping that after the exam week, I will be free and do anything I want instead of studying nonstop during the semester.