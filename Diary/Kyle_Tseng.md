this diary is written by Kyle_Tseng or Bokai_Tseng E14114124
# 2024-9-17

- The first course surprised me that there were lots of interactions in the lecture. It was refreshing to see a dynamic exchange of ideas rather than a one-way dissemination of information.
- The lesson gave me a perspective of the future, allowing me to envision where the industry is heading and how we can prepare for it.
- This lesson had a little problem about checking in; there was some confusion regarding the attendance procedure.
- I believe the lesson can improve my communication skills, especially in presenting and discussing technical topics effectively.

# 2024-9-19

- The AC didn’t work 🥵, making it quite uncomfortable to focus during the lesson.
- The lesson mentioned things regarding GIT and its founder, Linus Torvalds, which sparked my interest in version control systems.
- I had a problem in adding a file in the Diary folder, which caused a bit of frustration.
- Thanks to the TA’s help, I was able to add this sheet successfully and learned more about navigating the system.

# 2024-9-26

- The presentation talked about climate change and highlighted the urgent need for action.
- The data should contain the absolute numbers, not only the changes, to give a clearer picture of the situation.
- The AC was working 🤩, which made the learning environment much more comfortable.
- The SDGs (Sustainable Development Goals) are truly the goals people should dedicate themselves to achieving. They encompass a broad range of important issues, from poverty to climate action.

# 2024-10-17

- Chinese New Year and red envelopes affect Taiwan's financial growth significantly, impacting consumer behavior and market trends.
- We learned from experts who explained how the financial system works, providing us with valuable insights.
- I was frozen by the cold AC 🥶, which was quite distracting during the lesson.
- Money is created by loans, which was an eye-opening concept about how modern banking operates.

# 2024-10-24

- Potholes can make life more confident and resilient, as dealing with challenges strengthens character.
- The hatred between Taiwan and China must be considered, including the truth and history behind the conflict.
- How to define knowledge? Plato's definition: well-justified true belief, offers a philosophical perspective on understanding.
- To falsify a hypothesis only needs a contradiction, which is a fundamental concept in scientific methodology.

### 3 friction stories:

1. **Taiwan as a Nation**: I've heard and seen lots of discussions between Taiwan and our biggest neighbor - China. A group of people in Taiwan is too sensitive to information from China, which leads them to not see the truth or strictly judge and falsify without any evidence. In my opinion, only peace and communication can lead both parties to unite together.
2. **Currency**: The price of housing in Taiwan is massively inflated literally everywhere. Affordability is a crucial factor that makes me anxious about the future and affects the nation's economy and wellbeing.
3. **Marriage**: One of my friends posted her opinion on a thread, which provoked my thought and made me rethink the importance of marriage. She mentioned that if she doesn't marry or have any relationship when she gets old, she can't bear the loneliness and disability by herself.

### The most influential politician in Taiwan - President William Lai

1. **Deepening Taiwan's Democracy**: He emphasizes that Taiwan's democratic values are deeply rooted in its people and are a crucial foundation for national policy. He aims to strengthen Taiwan's political system through the deepening of democracy.
2. **Maintaining Regional Peace**: He states that maintaining regional peace will be a central focus of future governance. He is open to dialogue with Beijing but has set broad conditions that may not be easily accepted by Chinese leaders.
3. **Promoting International Cooperation**: He stresses that Taiwan should firmly establish itself in the international community. He believes that no political party should sacrifice the nation's sovereignty for power, and the interests of the people should not be influenced by any authoritarian state.

# 2024-11-14

- Last week, I felt anxious due to the plenty of exams. The workload was overwhelming, and managing time effectively became a challenge.
- I learned something essential for success in work groups, like the importance of clear communication and setting realistic goals.
- Some classmates shared a lot of fabulous opinions and experiences, enriching the discussion and providing diverse perspectives.
- Rationalizing ourselves may create a new attitude that didn’t happen before, encouraging personal growth and open-mindedness.
- Be careful to ask ‘Why’ and forgive to regret, because it’s normal for humans to question and feel regret. Understanding this helps in accepting and moving forward.

# 2024-11-21

- The presenter provided a comparison of laws between Taiwan and Indonesia regarding demonstrations, highlighting the differences and similarities.
- We discussed various opinions between the presenter and the classmates, leading to a lively and insightful debate.

# 2024-11-28

- No way I am phone addicted, so I wouldn’t put my phone on the front desk. However, I understand the need to minimize distractions.
- Many groups presented their final projects, and the alarm system was pretty impressive. The creativity and effort put into the projects were commendable.
- We could see the progress of the projects that aim to make the community or the world better, showcasing the potential for positive impact.
- We have obligations to clearly and honestly express information or news, as journalists might misunderstand and spread misinformation. Transparency and integrity are crucial.

# 2024-12-5

- News stories are increasingly biased to capture attention, as non-engaging content garners no viewership. This trend raises concerns about the quality and reliability of news.
- All news is told from a subjective perspective, often with a bias to provoke a response. Recognizing this helps in critically analyzing the information.
- Mainstream media maintains some credibility by grounding stories in truth, distinguishing itself from fake news.
- There is a hypothesis that mainstream media now adopts stylistic elements of fake news more frequently compared to 30 years ago, blurring the lines between factual reporting and sensationalism.
- Some planetary boundaries that we should concern about were discussed, emphasizing the importance of environmental sustainability.

# 2024-12-12

- We figured out some methods to reduce CO2, and the answer is having fewer children! This controversial solution sparked intense debate.
- The debate made the action more clear, allowing us to understand different perspectives and refine our arguments.
- The boundary to reduce CO2 made me more serious about climate change, and so did the video which emphasized the frightening future and results of severe climate change.
- The debate truly connected the topics of our presentations and the main theme of the course, highlighting the interconnectedness of the issues.

# 2024-12-19

- I was quite astonished to discover that the presentation our group had prepared was edited by another group without our consent, and to make matters worse, the original file was missing entirely. This unexpected situation not only disrupted our plans but also raised concerns about the integrity of our work and collaboration dynamics within the project.
- the concept of efficiency in this context aligns closely with the principles defined in thermodynamics. Understanding this correlation helps frame our analysis in a structured and scientifically grounded manner, offering valuable insights into system performance.
- Additionally, many graphs presented throughout the discussion effectively illustrated the progression of technology over time. These visual representations underscored key trends, highlighting both advancements and challenges, and provided a clear roadmap for understanding technological evolution.

---

### week 14 diary

- 241219 Thu.
    - successful / productive
    - automobile car is well designed and moved on the path on the ground and the fluid mechanics homework problems are almost completed. I think it’s a successful and productive day.
    - I will not procrastinate many tasks such as homework, presentations, etc…
- 241220 Fri.
    - successful / productive
    - fluid mechanics quiz is well prepared so I deem it as a pice of cake and also accomplished some tasks and homeworks, which make me feel successful and productive.
    - I will not stay up late to watch videos
- 241221 Sat.
    - unsuccessful / productive
    - a get up at noon but I still work on my homeworks and finish as scheduled. Thus I feel unsuccessful but productive.
    - I will not get up so late !!!!
- 241222 Sun.
    - unsuccessful / unproductive
    - i get up at 11am so a fail to get up early and I have poor efficiency doing my homework. Therefore I feel unsuccessful and unproductive.
    - I will ask my friends to help me solve the questions and I have take rest now……
- 241223 Mon.
    - successful / productive
    - blue Monday keeps me working hard and so does the final exams’ approaching. Both make my day successful and productive.
    - I should organize and use my time more efficiently.
- 241224 Tue.
    - successful / productive
    - today I and my auto control team member are confined in ME department because the EV3 car is super problemetic and arduous to set in a proper way that makes it track the black line and accomplish moving on the path. Somehow we did it and spent 5 or more hours to finish it. It’s quite hard but worth so I think it’s a succesful and productive day.
    - I have to study my final exam instead of doing any presentation that isn’t urgent to submit.
- 241225 Wed.
    - unsuccessful / unproductive
    - today I have a christmas meal which shrinks the time to prepare the electronic final exam well so I feel unsuccessful and unproductive and I have a bad headache so I go to bed early.
    - Take some rest.

### 5 criteria to be more successful

1. tasks should get finished as scheduled.
2. accomplish tasks in high efficiency and accuracy.
3. get up early and ensure the brain is clear.
4. keep motivation to complete the tasks.
5. taking rest is also essential.

> In conclusion : discipline
> 

# 2024-12-26

1. many groups presented their ambitious plans.
2. Learn about many inspiring rules for how to be more successful.

---

### week 15 diary

- 241226
    - successful/unproductive
    - I have a gift exchange activity with friends in the club but don’t do any homework or task, so I feel successful with social activity but unproductive in my school tasks.
    - today is the day in the week that I have to attend to the club activity so I regard it as a relax day in a week, so I will study hard tomorrow.
- 241227
    - successful/productive
    - I have a  good progress in studying my final exams which indicates that the day is pretty successful and productive.
    - keep the study pace or be mroe efficient.
- 241228
    - successful/productive
    - I finished my final exams note and also hang out with friends buying new clothes since the weather is getting colder. Thus I think it’s a fabulous day for me.
    - don’t spend so much time on scrolling screen.
- 241229
    - successful/ unproductive
    - I had a nice meal at bridge brunch restaurant which provided tasty omelet, crunch chips and a piece of toast with butter. After a wonderful meal I crushed out for the entire afternoon. Consequently, it was apparently a success in having good meal as well as good mood while the productiveness was not progressive enough😮‍💨
- 241230
    - unsucccessful / productive
    - Today I have 2 final test, mechine design and biotechnology. However I had a very bad sleep last night (about woke up 3 times I think…) and then my mood is so bad at facing everyone. Hopefully, duo to the test I had not to talk to any people just concentrating on my paper. I feel bad when I answer the questions in biotech exam since I have prepared all the content but it turns out that I can only answer about 70% of the test questions, that’s quite frustrating. In conclusion, today is unsccessful but productive.
    - Try to avoid any disturbance that affects my sleep.
- 241231
    - unsuccessful/ unproductive
    - Today is the last day of 2024 but I have no idea how to celebrate or have fun. So I decided to watch the new series of the popular drama recently - Squid game 2 and I finish all the episodes in one night, but I feel a little sad when the mid night passed, the sparkle was blossoming accompanied with lots of cheering. I looked around my surroundings, there was no one with me but the squid game in my computer screen. I feel a bit bored but I have no idea what to do even though I have friends, so I back to my drama. I find myself a boring person who has nothing to do without studying, testing, working …I realized it was not a normal life or the life I don’t want to have but sadly I can’t escape this so I feel unsuccessful and unproductive.
    - I have to figure out how to define my life and what I want to be.
- 250101
    - successful/productive
    - I well prepared for the fluid mechanics test and also had a good time with the astonishing and breathtaking story in Genshin Impact game. I think it was not only a success but also a productive day.