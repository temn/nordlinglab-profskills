This diary file is written by Douglas Lu I94126065 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-21 #

* This is my very first lecture. Despite facing some technical issues and feeling a bit nervous, I found the lesson excellent.
* I thought there would be some complicated maths and physics, but the topics discussed are interesting and equally important to our society as maths and physics.
* Some of the presentations are incredible, especially the one discussing issues in the presenter's home country.
* **Task: What I think my future will look like and what it will be so ten years from now** I am currently a dental school student. Six years later, I’ll graduate from university, and I plan to pursue a master’s degree because I want to gain more knowledge in the field of oral science that I am interested in. Also, I want to teach in dental school in the future so further studying is necessary. I want to study in an overseas graduate school so I won’t be in Taiwan ten years from now. As for the five current trends of change, there have already been some transformations in dentistry. First, there is the involvement of information technology in the management of patient records. Yet, with the digitalization of patient records, how to protect the patient's privacy and prevent attacks from hackers can be one of the challenging problems I, as well as everyone in this profession, may face. Second, with the help of artificial intelligence, accuracy during surgery can be more easily achieved. Third, regenerative medicine has increased in popularity recently, and I believe that it can have a tremendous impact on dentistry. For example, we may be able to grow a completely new tooth with a certain type of stem cell. Fourth, there may be changes toward more sustainable dentistry in the future, including efficiency in the use of energy, water, etc. Fifth, a doctor’s ability to communicate with a patient will be as important as his professional skill. As artificial intelligence becomes increasingly powerful, I believe the ability to interact with patients will become significant. I should also find out what are my abilities that AI can’t replace. Ten years from now, I believe I'll be equipped with knowledge in much more advanced dentistry than now, and the ability to make use of the knowledge. In spite of the challenges, I am confident that the future will be bright with the help of technology in dentistry, and hopefully, I can be part of this bright future.
* **Task: Summarise the different views in the sources in bullet point format**
* News: Leading Hong Kong journalist found guilty of obstructing a police officer
* Left-leaning view: Provide factual information on the event and its relation with Hong Kong media freedom, and **express a grim view of Hong Kong’s freedom**.
* Example: *Hong Kong, once seen as a bastion of media freedom in Asia, ranked 140th out of 180 countries and territories in Reporters Without Borders’ latest World Press Freedom Index. The organization said the city saw an “unprecedented setback” since 2020, when the security law was imposed. But Beijing and Hong Kong authorities said the law helped bring stability back to the city following the anti-government protests in 2019.* abc News
* Center view and right-leaning view: Provide factual information on the event and critics' views.

# 2023-10-12 #

* I think the presentation about Macron is good, with good evidence supporting the groups argument that the news is fake.
* I've learned some the definition of money, which is an economic unit used as a medium of exchange.
* Money's five characteristics: fungible, durable, portable, stable, recognizable.
* I found it quite interesting that red envelope can actually contribute to the fluctuation of currency held by people.
* Central bank's objectives: ensure a stable value of money, guide other banks, foster economy.
* Video shown in lecture tells us that decentralization of bank is good.
* By taking a loan, we create money.

# 2023-10-19 #

* From today's presentation I have understood how complicated the financial system works, and the way to display datas of financial system is not an easy task.
* **Video1: Why Facism is so tempting**
* People now comfuse Facism with nationalism. Nationalism help unite people who don't know each other living in an country. Facism denies all identity beside *nationality*. Human relationship is compllicated, we have family, friends, etc., but Facism simplify those things and focus only on nationality. If the nation want you to do something, you do because it's beneficial to nation.
* Without nationalism, we would probably be in endless tribal conflicts. Countries in peace have strong nationalism, like Japan, Sweden.
* Facism may look *beautiful*. It makes us see us more beautiful than what we really are.
* Dictatorship comes from the *centralisation*. The right to control is limited to few people (government, corporation, elites, etc). Things being centralised has changed thoughout history: Land->Machine->Data
* Democracy is ultimately determined by *emotions* of us. So if the data is centralised and manipulated by few people, that would be particularly dangerous. Our emotion could be easily controlled.
* **Video2: My descent into America's neo-Nazi movement & how I got out**
* The neo-Nazi organization find those feeling *marginalised*, and recruit them in.
* Blame Jews, blame colored for using drugs, blame the immigrants for taking jobs
* If we hit the *potholes* in our journey in our life, we struggle with our identities. The speaker didn't join the neo-Nazi group because he agree with the organization, he joined because lacking belonging.
* If we don't understand something, we tend to hate it. 
* Don't tell those who have racism that they are wrong, instead, bring them in and fill their potholes. 
* Those who don't think they deserve compassion deserve compassion the most.
* **Video3: Imagined reality-Yuval Noah Harari**
* Human can believe in stories based on simply our imagination.(e.g. money can't be eaten, used as weapon,... yet we believe in its value)
* Money, EU, Google, exist in nothing but in our *fictional imagination*.
* Professor's comment: 1.When something like nation is our fictional immagination, why we let them damage us. 2.We should distinguish between fixed law of physics and our own imagination.

# 2023-10-26 #

* **What is knowledge**
* Knowledge is a "well-justified true belief"
* **Video: What if we pay doctors to keep us healthy**
* Because of problems of architecture of current healthcare system, which focus on number of a practitioner's cases and the profit of each case. Some practitioners may perform unnecessary treatments.
* A new, transformative idea: Sick-care -> Health-care
* Doctors get paid when you are healthy, and don't get when you are sick.
* Every actors in the system should change.
* **Video: The brain-changing benefits of exercise**
* Benefit of exercising: better mood, better attention, better memory.
* Immediate effect
* Long-lasting increase
* Protect your brain from incurable diseases
* Minimum exercise: 3-4 times a week, 30 minutes each
* **Mental Health**
* Mental disorder cases are increasing.
* Climate anxiety. Feeling of betrayal from government's inaction.

# 2023-11-02 #

* The new way to present homework came as a surprise. Yet, we got through it eventually.
* **Video1: Don't suffer from your depression in silence**
* You can't shake the depression easily because it's related to gene and unbalanced chemical substances.
* Social stigma can make the depression worse.
* Sharing stories can help.
* LIfe is beautiful. Support network is essential.
* **Video2: How to connect with depressed friends**
* Depression doesn't diminish a person's desire to connect with other people, just their ability.
* Don't say "just get over with it"
* Don't try to fix the depressed people.
* Don't take a negatively response personally.
* *People can be sad, but ok, at the same time.*
* Talk naturally.
* Clearly state what you can do and not do. Give the depressed peoople a sense of control.
* Interact about not depression, a.k.a. normal things. Depressed person don't necessarily want to talk about depression. 
* Invite them to contribute to your life.
* **Video3: The bridge between suicide and life**
* It's not just the talking you do. Just listen, just be in there and accompany them.
* Confront them, asking them if the idea of suicide ever come to their mind.
* "You listened" is the key for Kevin's coming back from the other side of the rail.
* Ending your own pain via suicide may deprieve others' chance of knowing your pain.
* The second they let go of the rail, they regret it.

# 2023-11-09 #

* First, we talk more about **depression**:
* It is the fact that we will die someday that makes life worthy.
* If you don't take care of yourself, you cna't help others.
* Then, we discuss **professional skills for future worklife**:
* *Process* matters the most.
* When you start a new job, invest in building a good relationship with *coworkers*.
* Some tips: building *non-work* relationship, *collaboration* with others about tasks/projects, do *beyond* your responsibility
* The work interview is not only the company interviewing you, but also you interviewing the company.
* Last, we talk about **rich and fulfilled life**:
* We are *flexible*, our decision-making is fragile.
* We often make the decision first and then rationalise the dicision.
* We are *inconsistent*. What we like may change.
* *Forgiveness* is important, it brings peace. A person who doesn't forgive is tormented.
* Focus on *meaningful life* is more important than pursuing happy life.
* 4 pillars for meaningful life: belonging(love. don't devalue others), purpose(less about what you get, more about what you give), transcendence(for example, going to museum), story-telling(how you know yourself)

# 2023-11-16 #

* The presentation from other groups are interesting. I've gained some basic knowledges on these topics.
* I found the one about sleep especially interesting. It's a topic related to me and the rest of the classmates and we should all get enough sleep.

# 2023-11-23 #

* After one of the classmates presented her diary, the professor praised her honesty. Before you're able to change something, you've to accept. Acceptness requires *being honest to yourself*.
* **Video: Simon Sinek on millenials in the workplace**
* Millenials grew up subjected to failed parenting strategy. That is even losing or doing the worst, you still get reward. However, it's not the case in workplace.
* We get a rise in dopamine when we get a response on social media, and we're addicted to it. Yet, when using social media, no deep meaning friendship is created. What's worse, people seek comfort from the devices when they're depressed, not from direct human accompanying.
* Instant gratification is another problem(like when you want to watch a movie, you get it on the internet). Things except job satisfication and deep relationship work in this way. Millenials want to quit just working for 8 months, thinking that they're not making an impact.
* To solve the "instant gratification problem", mellenials should learn to be patient. See the mountain, don't just see the summit.
* And millenials should know that relation forms in a slow, steady way, unlike the relation on social media.
* The environment, including current corporate system and leaders, is not helping the kids to learn the job fulfillment and social skills.
* Removing your temptation can be an effective way. Don't bring your cell phone to the restaurant. Charge your phone in the living room so you won't check your phone right after waking up.
* **Reflection:** I love this video. I think the point Simon Sinek provided are valuable to me. After coming to the college, I've met much more people and follow much more people on social media. I also check my phone more frequently, like when I'm wainting in line or before the lecture starts. Now I know that actually these moments are perfect for ideas to pop out or build up relation withe others.
* **Video: we need to remake the internet**
* On Google and Facebook, ads become behaviour modification.
* social punishment and social reward
* remake bad decision: sometimes when you pay for stuff, you get good result
* **Reflection:** I found the idea of paying for internet quite interesting. However, after further thinking, I think that may also lead to another problem. Just like movie streaming service, there are some illegal platforms, and the same scenario may occur if we develop a search engine that requires people to pay for the service.
* **Video: math**
* Algotithm have three characteristics that's worrying: widespread, mysterious, destructive.
* **Reflection:** Probably because I've lost my focus, I didn't get the point that the presentor was telling us.

# 2023-11-30 #

* The professor encourages us to form a more concrete action idea.
* In order to be concrete, we should include where, who, when, what, how, etc.
* I'm particularly interested in the presentation about sleep, cause I really want adequate sleep.
* **Video1: How to choose your news**
* Find the original source. Compare multiple truth.
* The more chaotic the event is, the less you should follow it every few minutes.
* Verify the truth before spreading on social media.
* We have more freedom on the flow of information, but with that freedom comes responsibilities.
* **Video2: 1177**
* The speaker suggests that multiple factors combined led to the destruction of the mediterranean societies around 1177BC, and the cities couldn't survive when one is destroyed because they are connected, or globalised. In nowadays world, the situation could be even worse should similar events happen, and I think it's important to strengthen the relationships between countries. It may be difficult for a single country to solve the problem, but together, human can have a better chance surviving.
* **Video3: Fall of empire**
* After watching the video, I think I will have to ask my boss to give me gold and silver rather than money as salary and build an undergroung vault to store them.

# 2023-12-07 #

* I've learned from the lecture about the planet boundary. It's quite surprising to me that except for issues frequently heard like climate change the nitrogen and phosphorous are overly consumed by us as well.
* I am not optimistic about what the world is gonna look like many years later if we don't make some changes. I really don't understand why this isn't an issue that people *really* care. The news I've read recently are mainly about the Isarel-Hamas conflicts. The wars initiated by stupid people are really distracting us.
* In the TED Talk about planetary boundary, the idea of threshold is interesting to me. We don't experience a very serious consequence now, but if we go beyond the threshold, there would be deadly consequences for us.

# 2023-12-14 #
* I've learned some more precise knowledges on how to plan things, including WBS, PERT, Gantt chart.
* The professor encourged us to focus on outcome. Indeed, if one don't even have an idea about what the outcome is going to be, it'd be impossible to finish a thing.
* After learning from lectures on issues regarding planetary boundary, I've understood that our planet is sicker than I'd previously thought. What we can do to solve this problem, based on scientific evidences, is surprising and kind of weird. If we have less children, can human survive? (In Taiwan, for example, most family nowadays have only one kid, if they reduce the number to zero, then there won't be next generation!)

# 2023-12-21 #
* The week's lecture about how to become successful is inspiring. I wish I could have learned this earlier.
* I like the chart of important/not important, urgent/not urgent, it's a good way to manage and I've started using it. I also combine it with the WBS to determine how much time each task would consume.
* **2023-12-21** not successful, not productive: I fell behind of my schedule and even dosed off while I was studying. I've tried to implement the strategies to manage time learned in this course to plan tomorrow's schedule.
* **2023-12-22** successful, not productive: I've passed the final exam of my swimming course.(In the beginning of the semester, I don't know how to swim, and now I can swim 400m.) However, I went home today and chatted with my parents all evening, which casued a totally delay of my schedule. I will stick to my schedule tomorrow.
* **2023-12-23** not successful, not productive: I didn't do much things today. The schedule is just not working.
* **2023-12-24** not successful, not productive: It appears that the environment at home is too relaxing for me. I'm not very productive being here.
* **2023-12-25** successful, productive: I've finished the final project of another GE course. Because Tuesday is the deadline, I'm very productive.
* **2023-12-26** not successful, not productive: My tummy isn't feeling well. My studying performance was affected as well.
* **2023-12-27** not successful, productive: Nothing gives me senses of achievement today, but I'm still productive.
* **Five rules for success**
* Make a practical plan, focus on outcome.
* Make something you want to do a habit.
* Put distractions away.
* Enjoy it if you feel happy.
* Remember that it may be painful to do something that will later on give you a sense of achievement.

# 2023-12-28 #
* How to tie a tie: Windsor knot and half windsor(suitable for informal occasions) are simple and useful.
* The idea that we can randomly choose who to be our leader randomly is interesting, and I like it.
* However, I think there's a problem. It is that not everyone want to be in the parliament.
* The idea of verticle farms work may fine in cities in developed countries, but actually, a lot of people living in poor countries may not be able to access this kind of technology, and they are people who are truly threatened by hunger.