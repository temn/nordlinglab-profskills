This diary file is written by Christian Marc Narmada F04047024

## 2018-09-20

* Got enrolled into the class in the second week, so this was my first lecture.
* The average score that we got on our test was not much different compared to the score monkey's would have scored if they had took the test.
* Conflicts are not necessarily bad and sometimes unavoidable.

## 2018-09-27

* Strong importance of proper referencing which is a skill that should be second nature.
* When showing graphs, charts, figures etc, on a presentation, simplicity is always better and should be claerly visible to the readers.
* A majority of fake news is a deliberate act of misinformation and is not bad journalism.


* Strong importance of proper referencing which is a skill that should become second nature to ones self.
* When showing graphs, charts, figures etc, on a presentation, simplicity is always better and should be clearly visible to the readers.
* A majority of fake news is a deliberate act of misinformation and is not bad journalism.

## 2018-10-04

* If trying to prove a claim which is based on statistics, do not use the data from the same source for proving.
* Why are engineers not taught about economics when it plays a big factor when it comes to funding for their projects etc.
* Banks are just financial intermediaries, they don't take deposits or money but borrow from the public.
* Most of what people call money is actually credit.

## 2018-10-11

* Facism is used as a kind of general purpose abuse or they confuse facism with nationalism.
* Facism tells me that my nation is supreme and that I have exclusive obligations towards it.
* Now data is replacing both land and machines as the most important asset.
* Facism is what happens when people try to ignore the complications and to make life too easy for themselves.


## 2018-10-18

* Knowledge is a well justified true belief.
* One example cannot prove a claim, it does not enable you to fulfill the criteria.
* If you have knowledge, you have the responsibility to share it with the people around you to benefit.
* Sick care is treating the sick, where as healthcare is to treat the healthy and help maintain their health.
* Moving our body has immediate long lasting effecct on the brain.

## 2018-10-25

* Exercise helps to increase supply of oxygen to the brain, hence leading to the increase in neurons produced.
* Research suggests that most mental disorders develop, at least in part, because of a chemical imbalance in the brain, and/or an underlying genetic predisposition.
* By just being there for someone who is experiencing depression, it could be a turning point for them.
* People suffering depression do not have broken or damaged brains, they just work differently.
* Don't take negative responses personally.
* Social stigma is a huge cause of developing mental disorder for black Americans, with 63 percent of black Americans mistaking depression for a weakness.

## 2018-11-01

* One should show support and compassion towards those with depression.
* Do not treat people with depression differently, they are humans just like the rest of us.
* Getting people o see the opposite view and engage in a conversation with themselves could actually make them change their views.
* A lot fo what we call self-knowledge is actually self-interpretation.
* Violence is a learned behaviour.
* Chasing happiness can make people unhappy.

## 2018-11-08

* Put Last Visited in a parenthesis () and use standardised date presentation. Ex: 2018-11-08.
* Lawful relates to common law.
* Legal pertains to legal system, doesn't relate to common law. They are not one in the same.
* Parliament has no say in common law.
* Jurisdiction is not tied to land, it is tied to party
* If you have not broken any laws, you do not have to engage in conversation.

## 2018-11-15

* The modern generation of millenials are growing up with lower self-esteem than previous generations.
* Corporate enviroments today care more about numbers and figures than the well-being of the kids.
* Dopamine is highly addictive and for an entire generation who have easy access to addictive numbing( such as social media), this is becoming a big problem.
* Millenials don't have as much meaningful relationships with one another when compared to previous generations.
* Millenials are expecting instant gratification.

## 2018-11-22

* We must check all information thoroughly to avoid getting flooded by fake news.
* To forget our past, may lead to repetition.
* Through examining the past, we can find occurences and patterns. By studying this we maybe be able to "Predict" the future.
* Roman Republic and modern USA a very similar in terms of politics and finance.

## 2018-11-29

* Modern civilization is addicted to growth.
* How can global economy keep growing exponentially while our ressources are finite?
* Boundaries for our ressources might actually be a good thing because it will force us to think creativily and unleash our full potential.
* Have we entered the antropocene?

## 2018-12-06

* Is it actually possible to reverse the impact we have made to Earth.
* The statistics on planetary boundaries do not seem to be up to date.
* Freshwater use may have aalready been exceeded.
* All planetary boundaries are inter-related.
* A 3rd industrial revolution is much needed if we are to continue living on this Earth

## 2018-12-13

* Automation can be a positive if we can allocate profits more equally. 
* Fossil fuels are an easy solution to energy but we should find a more sustainable method of producing energy.
* Some day we will have exhausted all our reserves of fossil fuels but we cannot wait till that day to find a solution, we must find one now.
* Being stable could also mean being relatively poor.

## Lesson 14 Daily Diaries

2018-12-14

* Unproductive and successful.
* Caught a really bad cold.
* Helped a friend out to take care of a visiting friend from another country until he was off from work.

2018-12-15

* Productive and successful
* Friend was celebrating birthday at my apartment, so helped him to prepare and cook food for guests.
* Started working on a presentation work for Monday.

2018-12-16

* Productive and successful
* Started day off really tired and late due to party ending at 4 a.m. 
* In the afternoon, went sightseeing with roommate's visiting friend to refresh.
* Finished final presentation which was for the following day.

2018-12-17

* Productive and Unsuccessful
* Had to postpone final presentation due to really bad cough.
* Went to work (part time teaching job) in the evening and managed to make it through till the end.

2018-12-18

* Productive and Successful
* Woke up early to get some paperwork done, but was unsuccesful due to other party not showing up.
* But was very productive in class and finished preparing half of a presentation for the following week.
* Went to work and it went great.

2018-12-19

* Productive and successful
* Woke up early and finally finished paperwork which could not be completed from the previous day.
* Had a good discussion session at school today.

## 2018-12-20

* The brainstorming session was a really fun and interactive activity for the class.
* There are so many problems in our daily lives which cannot be instantly fixed but with small steps it is achievable.
* Problems of some people may not be a problem for others but overall it is still a problem.

Lecture 15, Arguements for UnityChain:

1. How do you plan to deal with the large energy consumption that other companies faced?
2. There are a lot of penalties for the customers, but are there any rewards?
3. What happens if there was a situation when the nodes within a tribe cannot reach a consensus?
4. Is there an initial investment that the users would have to provide?
5. How are u going to deal with miners?

## 2018-12-27

* The speech by the invited the guests was really interesting.
* In class today, I initially had no questions to ask, but with the concept of brainstorming as people started asking questions, it gave me the opportunity to come up with more follow up quesitons.
* The topics that we are learning in this class, is to help us one day reverse or reduce the damages/effects we have brought upon our Earth and hopefully our future generations would have the privilege to experience it too.

## 190103

* Building trust and connections is really important as it may turn to be very beneficial in the future.
* Mathematics can also give us an overview on how a relationship has succeeded or failed.
* When looking for a partner in life may not be easy and there is no guarantee of a 100% success rate. But the experience you gain from hardships and heartbreaks can allow us to learn a lot about ourselves and others. So we must not be discouraged after every bad relationship.

## Daily Journal

* 190103 Thursday

		* Successful and Productive
		* Today had a final presentation for Engineering graphics class and I made it on time to class. The presentation also went really smoothly. Then I also presented for Professional skills class, which i would also say turned out to be very good. At work today, I tried a different teaching method with my kids and the outcome was better than expected.
		* For tomorrow I would try to wake up early yet again to continue this good feeling of productivity.
		
* 190104 Friday
		
		* Successful and Productive
		* Today I finished yet another course by having a final presentation/exhibit. Another day work again in the evening, kids were well behaved today and very responsive in class.
		* Set some milestones which are to be accomplished tomorrow.

* 190105 Saturday

		* Successful and Productive.
		* Completed all my milestones set for today, such as cleaning my room and doing laundry. Went over to a friends place at night to have a few drinks.
		* Countinue completing milestones.

* 190106 Sunday

		* Successful and Unproductive.
		* Unproductive because had a bit more than a few drinks and ended up getting really drunk. Then woke up late for the next day. But Successful because made it to an appoint with group members to work of final project of professional skills.
		* Recover from being lazy and tired, and try to get back to setting daily goals and achieving them.
		
* 190107 Monday

		* Successful and productive.
		* Woke up early. Cooked lunch at home. Then worked on a final report for a class. Went to work, doing the same usual teaching routine. After work went to dinner with a friend/former roommate. Then gathered some items from the dorms which I am in the process of moving out from.
		* Continue the slow process of moving out so that by the end of the week I could have comletely moved out.
		
* 190108 Tuesday

		* Unsuccessful and productive.
		* I missed my morning alarms, so I did not make it to the last class of the semester for a specific course. But then in the afternoon class, my group and I have successfully completed the final presentation. After class went to interview some more restaurants for Professional skills class.
		* Try not to oversleep through my alarms again.
		
* 190109 Wednesday
		
		* Productive and Successful.
		* Woke up and did laundry in the morning. Went to go get a haricut. FInally completed another final report for the end of the semester. Moved some more things out of the dorms.
		* Continue to do what I did today as it seemed like it was very well carried out.
		
## 190109

* The world we live in today is very scary, as we no longer have much privacy and can easily be surveillenced from anywhere.
* I learned that living in China is really scary and worse than what I could have imagined.
* Just because we can collect a lot of data about a person doesn't mean that the analysis and judgement may be good.