This is Hao-Teng Chang's diary. E14106155
# 2024-09-12 #
* We talked about how abundance will change the world. This topic is pretty fresh to me. Because we usually talk about energy and resource crises that may happen in the future rather than abundance.
* In the future, we may face abundance and lack of resources at the same time. Now we have so many resources, everyone has electricity, education……etc. But the amount of unrenewable energy is decreasing and the environment condition is getting worse. The future Earth won’t be able to fulfill our expanding desires forever if we don’t replace oil with green energy. We may live in the abundant future like what the video describes. But judging from the current trend. I think what is likely to happen in Taiwan is that we will put ourselves in hell first because our people doesn’t like to change, even some of my well-educated friends dislikes the governments green energy policy, thinking it is some kind of way for the government to earn dirty money.  
* Turns out that the poorest countries are also the sunniest countries, they can benefit a lot from solar power.
# 2024-09-19 #
* Know what kind of people your teammates are (harmony or conflict view) to avoid conflict. 
* No matter how angry you are, ask yourself which stage of anger you and your teammates are in.
* EV is cheaper than an internal combustion engine car. Because batteries have become cheaper.
* Even the best oil engine has only 55% efficiency
* Biomass energy generation creates a lot of greenhouse gas.1111
* Most of Steven Pinker’s claims make sense, humans are making progress. But he also ignores the fact that we are also not making progress in some ways. For example, he said that Americans produce less SO2 now than 30 years ago to strengthen the statement that we are getting better. However, he ignored the fact that Americans are also producing more CO2 now.
# 2024-09-26 #
* Know what kind of people your teammates are (harmony or conflict view) to avoid conflict. 
* Use how often you have to worry about something to determine if you are happy.
* More percentage of people are not able to understand a simple text by age 10 is increasing. This could be due to immigration. The immigrants can’t understand the language that their new home uses. Another reason could be that the education system is failing.
* Ask yourself three questions to determine if the imformation is fake or not: 1. Can you see uncertainties? 2.Can you see yourself in the data? 3. How is the data collected?
* I read a news on ground news titled "Dozens of buildings razed in Israeli strikes on south Beirut: AFP" I will Summarise the different views in the sources in the following bullet point.
* In the leftist perspective sources, they uaually claim that both sides are guilty and they should stop enlarging the conflict.
* In the righttist perspective sources, they don't clearly say that This war is entirely Hezbollah's fault, but in the way they tell the story, I can smell that's what they think.
* In the leftist perspective sources, the way they describe how the battle and war is going is usually surrounding civilians and buildings in Beirut getting killed and destroyed.
* In the righttist perspective sources, the way they describe how the battle and war is going is usually surrounding how good the isrealians are doing and what have they achieved.
* During the next 10 year, I will be working as a wind turbine mechanic of Taiwan power company. Where I will live depends on what my missions are. Hopefully, I will be maintaining and building wind turbine at the offshore of Taichung, my home. If so, I can live near to the Taichung port, the place where I take a ship to arrive to the wind turbine.
	There are five current trends of change that I think will affect me the most. The first one is the growth of renewable energy. Taiwan is aggressively pursuing a transition to renewable energy, aiming for renewables to comprise a significant portion of its energy mix by 2035. This trend means that as a wind turbine mechanic, my job will be in high demand. With more wind farms being developed both onshore and offshore, I’ll find plenty of work opportunities and career stability. The expansion of wind energy will also lead to more advanced technologies, making my job increasingly interesting as I’ll be dealing with state-of-the-art equipment and machinery. I will gain valuable experience with the latest technologies, hopefully becoming a highly skilled specialist in my field. This job’s demand might also offer me opportunities for career advancement, such as becoming a senior technician or a maintenance manager.
	The second trend is the technological advancements. Technological improvements in wind turbine design, efficiency, and maintenance processes will greatly affect my work. In ten years, wind turbines will likely be equipped with more advanced sensors and AI-based monitoring systems, enabling real-time diagnostics and predictive maintenance. This means I’ll spend less time performing routine checks and more time addressing specific issues identified by these systems. I’ll need to develop new skills to work with these advanced technologies, possibly requiring additional training in data analysis or remote monitoring. This will keep my job engaging and ensure I remain up-to-date with the latest industry trends.
	The third trend is the climate change and environmental policies. As climate change continues to drive policy changes, Taiwan is expected to implement stricter environmental regulations and sustainability targets. These policies will accelerate the shift towards renewable energy sources, solidifying the importance of wind power in Taiwan’s energy landscape. My role will be critical in achieving Taiwan’s sustainability goals. I may feel a sense of pride and responsibility, knowing my work contributes to combating climate change. I think working in an industry that aligns with environmental preservation will be both rewarding and socially impactful.
	The fourth trend is the energy independence and security. Taiwan's efforts to achieve energy independence will play a significant role in the next decade. Currently, Taiwan relies heavily on imported energy, but by investing in domestic renewable energy sources like wind power, it aims to reduce this dependency. As a wind turbine mechanic, I’ll be part of a strategic shift that enhances Taiwan’s energy security. I’ll be part of a growing and stable sector, ensuring Taiwan can produce its own energy. This stability may translate into job security, better working conditions, and potentially higher wages as the government and private sector invest more heavily in wind energy infrastructure.
	The fifth trend is the workforce demographics and aging population. Taiwan’s aging population could lead to a shortage of skilled workers in technical fields like wind turbine maintenance. As younger generations are encouraged to pursue careers in renewable energy, I can be among a select group of experts with valuable experience.
# 2024-10-17 #
* The free money that the government gave to me during the pandemic actually caused inflation. 
* If the government stops lending money to those who buy already existing houses and sell it but lend money to those who build houses and produce things, we can avoid inflation.
* Bitcoin has such a limit on use. That's one of the reasons why it is always fluctuating.
* We can’t use credit creation in Bitcoin.
* Bitcoin is not a good currency because it doesn't satisfy 2 of the factors that make a good currency.
# 2024-10-24 #
* Potholes are one of the reasons why people become extremists. To avoid letting people become extremists, we have to fill in the potholes.
* People become extremists because they want to belong, not because of their dogma.
* Taiwanese can make friends with Chinese to avoid war because they will object to killing their friends.
* There are two kinds of realities: objective reality and fictional reality. And nation, money and human rights are fictional reality in humans' mind.
* Plato thinks that knowledge is well-justified true belief. And I think knowledge is the awareness, understanding, or familiarity gained through experience, education, or reasoning.
* Health care system can save a lot of money compared to the sick care system.
* ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
* Come up with three rules for how to live healthy and present evidence to support them. (Be specific. Don’t just say eat healthy and exercise.
* 1. Prioritize Sleep Quality Over Quantity
* Rule: Get at least 7-9 hours of sleep each night, but focus on improving sleep quality by reducing light exposure before bed and maintaining a consistent sleep schedule.
* Evidence: Studies show that good sleep quality is essential for cognitive function, mood, and overall health. Poor sleep has been linked to increased risks of cardiovascular disease, diabetes, and mental health disorders. One study published in Sleep Medicine Reviews emphasized that consistent sleep patterns, rather than just total hours, improve health outcomes like weight management and mental acuity 
* 2. Eat Protein-Rich Breakfasts to Improve Focus and Reduce Cravings
* Rule: Start your day with a high-protein breakfast, aiming for at least 20-30 grams of protein within the first hour of waking up.
* Evidence: Research from the American Journal of Clinical Nutrition suggests that a high-protein breakfast helps regulate hunger hormones, reduce cravings throughout the day, and improve cognitive function and focus . A protein-rich meal can help stabilize blood sugar levels, avoiding the energy crashes that come with sugary or carb-heavy breakfasts.
* 3. Move Every Hour to Combat Sedentary Behavior
* Rule: Stand up and move for at least 5 minutes every hour if you spend long periods sitting, such as at a desk job.
* Evidence: Research has shown that prolonged sitting is linked to metabolic problems, cardiovascular disease, and even early mortality. A study from the Annals of Internal Medicine found that even 5 minutes of light movement, like walking or stretching, every hour significantly improves circulation, reduces blood pressure, and lowers the risks associated with prolonged sitting .
* ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
* Describe three fictional stories, e.g. currency, nation, god, marriage, and career, that you believe in and how they have affected your life at some point during the last week. (You must give a tangible example of how the story affected you within the last week.)
* First fictional story: currency. I believe in NTD, without it, I won’t be able to get any food within the last week.
* Second fictional story: nation. I believe in my nation. Without my nation’s protection , me and my friends could be brainwashed by Chinese nationalism and racism within the last week.
* Third fictional story: self-esteem. I believe in self esteem. This fictional concept stops me from cheating on tests within the last week.
* ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
* Describe your future or the future offered by some famous named politician in your country.
*  Former president of Taiwan, Tsai Ing-wen’s vision for Taiwan’s future is one of resilience, self-determination, and global leadership in technology and green energy. She offers a future where Taiwan strengthens its democracy, leads in cutting-edge industries, and defends its right to exist independently on the world stage, while also making significant strides toward sustainability and economic security.
# 2024-11-07 #
* About 7% of Americans have depression.
* When talking to people with depression, don't always talk about depression or try to make them happy. Just talk normally and make them enjoy being companied.
* Ace studends gets depression easier than others.
* People can be sad and ok at the same time.
* It is good to invite depressed friends to activities. Like play ball or cleaning yor room. Depressed people can be free labor.
* Signs of suicide tendencie: Losing intrest in life and feeling hopeless.
* Some people thinks that suiciding at the  golden gate bridge can send them to another dimension. But the fact is they will either die painfully because of the fall damage or get drowned to death.
* Surviors that jump of the bridge regret instanly at the moment they start falling. At that moment, they want to live but it's all too late.
* Depression is the number one cause of illnesses. Cancers, low inmunity and cardiovascular dieseases may occure because of depression.
* The percentage of Black people having depression is higher than that of white people in the USA, but the percentage of Black people visiting psychiatrist is lower than the percentage of white people visiting psychiatrist.
* 63% of black women thinks that having a depression is weak.
# 2024-11-14 #
* The topic of the first video is choice blindness.
* People choose their side first and find reasons for their choices.
* We take our decisions on feelings and rationalize it afterwards.
* We can change other people's opinions on an issue by making them engage in a problem and discussion. This will make them understand the issue and their opinions better, and possibly change their opinions.
* We don't have to like something just because we liked it 10 years ago. We are not that stubborn.
* Coworkers are the most important people in our jobs.
* If you have your assistant or coworker against you every time, resign on the first day.
* Become friends with coworkers is very important, it can increase working efficiency.
* Non-work relationships with coworkers are key to success.
* Inviting coworkers to BBQ parties or join their BBQ parties is a good way to improve Non-work relationships with coworkers
* Many plastics are not recycled. It's a problem with the economy. We have to make it profitable for people to recycle it.
# 2024-11-21 #
* The EU's carbon emission regulations are one of the reasons that make Taiwan set a goal to reach net 0 carbon emission, because products have to reach net 0 carbon emission to be imported to the EU.
* Claims of video:
* 1st
* Advertising models become behavior modification tools.
* Internet ads are created because people think everything on the internet should be free.  In order to make profit, ads show up.
* It's much easier to lose trust than gain trust.
* Negative feedback is stronger than positive feedback.
* The algorithm system amplifies the voices of cynical persons and conspiracy theorists.
* Sometimes we have to pay for useful knowledge, not getting free shit and conspiracy.
* Facebook is selling our data to companies.
* Middle people use ads to manipulate the elections.
* 2nd
* The algorithm observes what leads to success and tries to repeat the past success.
* Algorithms are opinions embedded in code.
* We have to stop the blind faith in big data.
* Algorithms can cause trouble silently without anyone realizing there's a problem with it.
* We have biases, and we can inject the biases into the algorithm.
* There was one time a judge used the wrong algorithm to decide how big the criminal punishment is.
* Data washing:  People use their private algorithm that is created to fulfill their personal desires, and lie to people saying it’s really fair.
* There is a lot of money to make in unfairness 
* Task:
* To stop fake news from spreading and maintaining freedom of speech. We can develop AI tools for verification, enforce accountability on platforms, and foster media literacy education. I reach this consensus because I find it impossible to have people verify all the news. Therefore, we have to use AI to do it.
# 2024-11-28 #
* Following the original reporter of the incident can make us get closer to the truth. Following the middle person who uses the original reporters’ images may give us the untrue story.
* News with anonymous sources should not be believed because that anonymous source maker could be planning to manipulate people’s minds or just want to make profit using false information.
* When everyone is a reporter, no one is.
* The larger a civilization is, it is harder for it to recover from the collapse and dark time.
* If the global supply chain of a really important material is off due to disasters, wars etc. It can’t be repaired quickly and thus cause the degeneration of cities. This kind of situation once happened in the late bronze age, due to various reasons: drought, famine, wars and refugees, the supply chain of the very important bronze is off. And the dark age begins.
* Human civilization has experienced many dark times, and now it is possible that climate change and many other reasons start a new dark age and bring the biggest degeneration of human civilization. We live in an age that heavily depends on the global supply chain and transportation, this means that any bad event in one place can bring huge destruction to the whole world. And it will be really hard to repair from the destruction since we’ve already become so big.
* After watching the truth of the Israeli football conflict incident, I find that fascism has a really bad impact on people. The Israelis often claim themselves as victims of genocide, but ironically they have become the executors of genocide and feeling super justified to do it. This incident reminds me of the words from a good book, A German's Story - What I Remember (1914-1933). “ fascism essentially a form of self-aggrandizement and self-worship of a nation—is undoubtedly a dangerous mental disorder wherever and whenever it manifests. It has the power to distort and tarnish the character of a people, much like how vanity and selfishness affect an individual, reversing right and wrong and exposing their ugliest traits.” It is fascism that creat so many blood thirsty monsters that not only want to crush their enemies, but also humiliate and rape them.
* The disturbing images of crowds cheering for genocide song and a women smiling when football fans sings about raping women shocked me. If a Taiwanese person behaves this way, he would likely be seen by Taiwanese society as a failure of the education system and a disgraceful individual. But in the countries that worships nationalism, like Isreal, China, South Korea and North Korea, the moral standard is totally the opposite. Treating and abusing "enemy races" with cruelty is not only not regarded as wrongdoing but is even praised by the government and society.
# 2024-12-05 #
* Green water is the water that exists in plants.
* 18% of GDP lost because climate change
* Just dropping fossil fuel is not enough, we have to recover nature.
* If Russia gets Ukraine, they get more natural resources.
* North Africa is not always desert, it is possible to plant trees and turn part of it green.
* Show the progress of planting trees to show people hope and how to move on, instead of showing unimportant news.
* One of the reasons why people don't care about climate change is that some people think:  “I focus on what is in front of me, and hope others take care of the big picture.”
# 2024-12-12 #
* 40% of people earn less than 2 USD per day.
* Climate change changes the water cycle .
* The 6th extinction on earth may be coming.
* Ice melting changes the sea current.
# 2024-12-19 #
* Building nuclear power plants and using nuclear power is more expensive than before. 
* Using solar and wind power is cheaper than before.
* We need friends that we can talk to in order to discover ourselves.
* A. Successful and unproductive
* B. I felt successful because I didn't fail at doing too many things today. I felt unproductive because I was feeling tired after class and only studied for a few time.
* C. I will use nasal spray during day time to breath better thus avoid tiredness.
# 2024-12-20 #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I failed to understand many fluid mechanic stuff. I felt unproductive because it took me a long time to understand a small chapter of fluid mechanics, and that make me only able to finish one homework problem today.
* C. I will ask my friends for help when I can't understand something.
# 2024-12-21 #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I failed to understand many fluid mechanic stuff. I felt unproductive because it took me a long time to understand a small chapter of fluid mechanics, and that make me only able to finish one homework problem today.
* C. I will try to see the pattern of the problems.
# 2024-12-22 #
* A. Successful and unproductive
* B. I felt Successful because I understand many fluid mechanic stuff sucessfully and I also do some other homeworks. I felt unproductive because I slept too much.
* C. I will sleep earlier so that I can have better sleep quality instead of having a long bad quality sleep.
# 2024-12-23 #
* A. Successful and unproductive
* B. I felt Successful because I managed to understand everything I learn in class today. I felt unproductive because it took me 7 hours to solve a homework problem.
* C. I will try go outside or do something else when I got stuck on a problem for too long, maybe that can help.
# 2024-12-24 #
* A. Unsuccessful and unproductive.
* B. I felt unsuccessful because I find it really hard to understand electronics. I felt unproductive because I don't feel motivated to understand it at all.
* C. I will try brain washing myself that studying electronics is fun.
# 2024-12-25 #
* A. Unsuccessful and unproductive.
* B. I felt unsuccessful and unproductive because I felt really tired the entire day. And I felt really tired because I can't breathe normally.
* C. The nasal spray that I used has a strong effect, but makes me harder to breath after the effect is gone. I should have use steroid sprays, not vasopressors sprays. This vasopressors spray made me breathe so comfortabaly that it makes me feel the spray that the doctors gave me are so useless. And it turns out that I am completely wrong. I will use steroid nassal sprays from now on.
* Five rules to become more successful or productive:
* 1. Sleep early and wake up early.
* 2. Use the correct nasal spray to help breathing.
* 3. Eat good food.
* 4. Study with friends.
* 5. Take a walk, exercise or do something else when I got stuck on a problem for too long.
# 2024-12-26 #
* A. Successful and unproductive.
* B. I felt successful because I can breathe well today and didn't failed at too many things. I felt unproductive because I don't feel motivated do the fluid mechanics homework.
* C. I will try give myself some awards after finishing a difficult task.
# 2024-12-27 #
* A. unsuccessful and unproductive.
* B. I felt unsuccessful because I gave up finishing the fluid mechanics homework. The more I do, the more I feel hopeless . I felt unproductive because I felt so tired and slept the whole afternoon, and in the night, I don't feel motivated to study at all.
* C. Don't do the impossible and solve the unsolvable, just solve what I can solve and try to steal some score and pass every course.
# 2024-12-28 #
* A. unsuccessful and unproductive.
* B. I felt unsuccessful and unproductive because I don't know how to study composite material.
* C. The composite material exam is all about memorizing all the details in the textbook. So I will try my best to read the textbook.
# 2024-12-29 #
* A. unsuccessful and unproductive.
* B. I felt unsuccessful and unproductive because I don't know how to study fluid mechanics.
* C. Try draw some pictures to understand the fluid mechanics problem.
# 2024-12-30 #
* A. unsuccessful and productive.
* B. I felt unsuccessful because I didn't attend the class at 9a.m. I had a terrible sleep last night because of the earthquake alarm and I failed to get up on time. I felt productive because I did the homework smoothly.
* C. I will sleep earlier lest any accident wake me up in the early morning.
# 2024-12-31 #
* A. unsuccessful and productive.
* B. I felt unsuccessful because today is quite boring. I felt productive because I did the homework smoothly.
* C. I will make a video call to my dog to get an interesting day.
# 2025-1-1 #
* A. unsuccessful and unproductive.
* B. I felt unsuccessful because I can't solve a machine design problem. There was always something wrong with my calculation. I kept checking and correcting my answer again and again, but I don't even know if they are correct since I don't have the answer. Maybe after all these hard work, the numbers that I got turns out to be wrong. I felt unproductive because I can't even finish 1 simle problem.
* C. I will ask AI and my friends. And if none of those work I may have to watch an Indian guy's video on YouTube.
# 2025-1-2 #
* Hao-Teng Chang is a good guy, he always helps his family and friends actively. He has an inexplicable insistence on achieving goals through the correct means. Even if everyone else uses dirty methods to achieve their goals and mocks him for being rigid or foolish, he still holds firmly to his sense of justice and principles.
* My father and brother are good guys, they always help their family and friends actively. I think they achieve this accomplishment because their family emphasizes the importance of treating good people with kindness.
# 2025-1-9 #
* The video that we make is really bad. Fortunately, most videos that the other groups made are either trash or some nonsense that doesen't match the requirements.