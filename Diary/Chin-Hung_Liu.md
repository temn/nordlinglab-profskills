This diary file is written by Ching-Hung Liu H24081058 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08

- The first week attending the class.
- One reason I enrolled this class is to adapt English teaching courses, it is very kind that Professor Nordling speaks slowly for me to understand what his words.
- I think changing crew members every three weeks is a good policy, which makes me familiar with many classmates and learn from them.
 
# 2022-09-15

- The second lecture was great.
- I learn about the advantages and usages of Bitbucket.
- Some classmates present their opinions organizedly and completely, I should learn from them.

# 2022-09-22
- The videos are great, and Hans Rosling is funny.
- I'm happy to know how the world has changed from Sustainable Development Goals.
- All my team members have abandoned this course, so I asked professor to assign me to a new group.
- But my new team member also abandoned this course, I'm so sad.

### Homework 1:

In your Diary for this week, write a 400-600 word summary of
what you think your future will look like and why it will be so 10
years from now. Start by defining where you will live and what
work you will do. Then describe the five current trends of change
that you think will affect you the most and their effect on your
life 10 years from now.

   I think I will live in America and work at SpaceX operating space farm, many experts predicted that global population would double before 2050. Maybe there will be enough land for everyone to live on, but there is definitely not enough space for agriculture, the only two solutions are ocean and outer space. If we can reduce the expense of going back and forth the space, which is exactly SpaceX trying to achieve, outer space will be the better option.
     
   In the future, 3D printer will be well developed, we can print things at home, company will be selling designs instead of real items. The technique can be used in many regions, especially in outer space. Limited by transportation capacity, we can’t launch many equipments to space, so we only have to bring a few components and raw materials and use 3D printer to build the machine we needed. Besides, materials can be obtained from asteroids, 3D printer can be very useful and very convenient.
     
   AI will also be developed in the future, repetitive work will be done by robots, many people will lose their jobs, but new jobs will be created, too. The jobs that only human can do, so we don’t have to worry about human being replaced. AI is also very important for space farming, with AI, we only need very few personnel to function a space farm.
     
   Online lecture and remote working are widely used because of COVID-19 epidemic, they will also be useful in the future. However, interactions between people are considered important, so students and workers are asked to go to school or the office, but online meeting can still be popular in space. It is impossible to go to space arbitrarily, so if somethings go wrong at the space farm, I will want engineers, agriculturists and other experts to work on Earth. We can use online meeting to teach the crew stationed on the space farm to solve the problem.
     
   As medication advance, people’s lifespans are getting longer. One is never too old to learn, there should be many ways for the elder to learn more and share their lifetime experiences. Last come I consider the most important trends of change, technique progress rapidly, people should pay attention to themselves. Health, knowledges, skills, moral, and kindness should be placed ahead of money and authority. I hope people can look inside others, find the internal beauty of humans.

### Homework 2:

Select one news story published on https://ground.news/ and read all the different sources. Summarise the different views in the sources in bullet point format in your diary.

I select *Labour members sing national anthem and hold minute's silence in patriotic opening to conference* at https://ground.news/article/labour-members-sing-national-anthem-and-hold-minutes-silence-in-patriotic-opening-to-conference_fd670d, which has 6 sources.

- At the Labour conference, Sir Keir paid tribute to the late Her Majesty, Elizabeth II.
- The delegates stood a minute's silence and sang the national anthem, which is the first time the anthem has been sung at the party’s conference.
- Disruption or boos from republican delegates have been feared but never happened, the moment is said to be a great signifier of “how the party has changed”.
- The former Labour leader Jeremy Corbyn had criticised the decision to sing the national anthem at party conference, arguing there was no precedent for doing so.

# 2022-09-29
- It is interesting to know how bank really work.
- This is the first time I learn that the bank create money.
- But the theory is still a little confusing, I am a little busy this week, but I will find some time to watch the video again next week.

# 2022-10-06
- I was selected to read my diary this week.
- There are some things I learn from the presentation. It is important to understand the data and the plots we get. We must know the axis units, explain the causality of the change of data, and verify the data with our daily lives.
- I hear Yuval Noah Harari said that data is the most valuable asset in 21st century, which astonished me and I think he makes sense.
- Bad things happen because good people remain silence.
### Task
- Story 1.
- An installer is hired to install soundproof glass at an office, and after finishing installation, the staffs and the boss of the office located on the other side of the glass with the installer to test the soundproof glass. The installer opened and closed his mouth with on sound coming out, pretending the soundproof glass is of good quality. The installer left with his payment, one of the staff started playing with the glass. She walked to the other side of the glass and started complaining. She said she worked overtime so much and the boss paid her so little, the boss is stupid and ugly, etc. She thought the boss couldn’t hear her, but actually the boss could hear every words. It was so embarrassed.
- This story tells me that wall has ears, we should be careful about the words come out of our mouths no matter there is anyone around or not. That is why I never speak badly behind others' back. Besides, test the quality of products ourselves, don’t believe anything the sellers said.

- Story 2.
- A man accidently dropped one of this new shoes when crossing the bridge. Pedestrians felt regret for him. Unexpectedly, the man threw the left shoe down the bridge. Pedestrians felt surprised, the man explained: “No matter how expensive the shoe is, it is useless to me. If someone receives both the shoes, maybe he can wear them.”
- When we loss something and we can’t retrieve, just let it go. I didn't do well on a report last week, I should let go and prepare the next report.
- Story 3.
- When William Henry Harrison was a child, people said that he was stupid because when others gave him a dollar and a cent, he always took a dime. After William became the nineth president of the America, people asked him why did him do that. He answered: ”If I took a dollar, who will ever give me a dime?”
- The smartest person is the one who acts like fool.

# 2022-10-13
- Plato said: ” knowledge is well-justified true belief"
- I think knowledge is the explanation of natural phenomenon, that is close enough to the fact to solve the problem we encounter.
- Neuroscientist, Wendy Suzuki claims that exercise cause so many benefits to our brain, and we shortly exercise in class today following her instruction.
- Matthias Müllenbeck’s idea of redesign the health care system is fantastic. I think one reason that people get sick is that people hate regular physical check-up because it takes a lot of time to do, there are so many examinations. With new technique, we can finish physical check-up quickly and with Müllenbeck’s health care system, medical companies should pay attention to help customers to preserve health. Although we have to share our health data and even genetic sequence, there is a concern for privacy, I would like to be in this new health care system.

# 2022-10-20
- The lecture today will be moved to Jan 7th.

# 2022-10-27
- We are grouped to discuss online using Google Meet, I think it is inconvenient because I open 2 Google Meet, and they interfere each other. I have to close the one for course and keep the one for group discussion, so I can’t hear what the professor is saying.
- We discuss about depress this week. When my friends are depressed, I usually just leave them alone. Maybe I can try to talk to them thereafter.

# 2022-11-03
- We talked about depression in class. I think we can bring our depressed friend to bungee jumping because there is a rumor saying suicide people often feel regret the moment they jump out of the roof. Maybe they will regain meanings of life during bungee jumping, or be terrified of suicide, since suicide is just like bungee jumping without a rope.
- I still haven’t found the meaning of life, but what I can do is bringing happiness to my friends and family.

# 2022-11-10
- Listen to other groups’ presentation, I think all course projects are interesting and important, but some action ideas are not feasible enough. Coming up a good idea is really the difficult part of the project.
- “Strawman: The Nature of the Cage” reminds me about our rights, which we may lose if we don’t pay close attention to. A message concerns me that after three grand Justices’ term expires next year, all fifteen grand Justices will be nominated by President Tsai, one of the most important protector of our rights will be significantly affected by the president.

# 2022-11-17
- We talk about how to reach a consensus today. I think one important thing is deadline, which is the time we should end the discussion and take an acceptable idea, because in many occasions the meeting is too dragging results from Conflict.

# 2022-11-24
- We have an experiment today. We put our cellphone on the table in the front of the classroom to test how addicted we are to our cellphone.
- It turns out that I looked for my cellphone several times because I wanted to search for vocabularies in the dictionary, beside that, I can put away cellphone.
- It seems that I am not addicted to cellphone, but the experiment only last for three hours, I wonder how I will be if the experiment last longer.

# 2022-12-01
- My presentation was selected this week, I recorded the video by part, but unfortunately, I missed one video when I merged them. I haven’t discover the mistake until a classmate ask about the missing part, thanks to her asking, I have the opportunity to present the missing part.
- In the class, all the classmates help us check the feasibility of our action ideas, thank them a lot.

# 2022-12-08
- My mom has a business trip to Tainan, so she visits me by the way.
- I finish several reports last week, so I have more free time this week.
- We are going to finish some detail and start the the 5-day veggie challenge next Monday.

# 2022-12-15
- A.unsuccessful and unproductive.
- B.On machine learning course project, I try a difficult algorithms and failed.
- C.I should try an easier algorithms that is feasible. 
# 2022-12-16
- A.unsuccessful and productive.
- B.I finished coding a machine learning model, but my teammate say he has done it already.
- C.I should check what my teammate has done, and tell him what I am going to do eariler.
# 2022-12-17
- A.successful and productive.
- B.I do 2 reports from different courses today.
- C.Rewiew corese handouts to do the reports better.
# 2022-12-18
- A.successful and productive.
- B.I correct some mistakes in the report I did yesterday.
- C.Rewiew corese handouts to do the reports correctly.
# 2022-12-19
- A.successful and productive.
- B.I finish a report and study for final exam.
- C.Consult TA to do the report better.
# 2022-12-20
- A.successful and productive.
- B.Discuss with teammates both for other coures presentation and Professional Skills course project.
- C.Taiwanese students usually avoid commenting others performance. I should ask my teammates to propose some defects of my performance.
# 2022-12-21
- A.Unsuccessful and productive.
- B.I write programs all day, but not getting good result.
- C.I should relax and come up with some idea.

## five rules that you think make me more successful or productive
- Discuss with teammates more often.
- Split task to small pieces and finish each everyday. Maybe  will come up with better idea.
- Consult professionals for help.
- Ask others for advices.
- Sleep early for clear mind.

# 2022-12-22
- Democracy is a good thing, but it is not functioning well now. Maybe randomly select representatives can work.
- Bhutan is a great country. I would like to visit Bhutan if available.

# 2022-12-29
- It seems that I can find many interesting knowledges from obituaries.
- In this era, going viral is valuable.
- If I want my voice being heard,  going viral is a convenient way.
- I hope my obituary says,"Who is so rich and donates all his money to charity" because I want to be rich in the future, and after I get bored of having so much money, I will donate all of it.

# 2022-01-05
- The lecture was funny, it taught us how to find the best partner. Thought I am not interesting in a relationship now.
- I made a mistake on the final exam, I didn't remember perfectly.
- Make a video is so hard, we spent so much time on the course project video.

