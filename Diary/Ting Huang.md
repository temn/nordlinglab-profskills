This diary file is written by Ting Huang E14094073 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #

*The first lecture was nice.

*I improve my english listening ability.

*I have good communication with the team members on the report and they are very kind as well as responsible.

# 2022-09-22 #

*I think my future life should still be living in Taiwan, but maybe not in Tainan but in Taichung. Because the pace of Taichung is not as slow as Tainan but not as fast as Taipei. My job might be in a factory, or maybe an office.

*The first is about the Ukrainian-Russian war, which may lead to higher oil prices in the days to come. The second is that I have changed to a new rental house. This house can cook, so I may learn to cook in the future. The third is that I like a game, and this game may still be in operation in ten years, so that I can relax and play games on the days off work in the future. The fourth one is about the recent earthquake, which made me realize that I should cherish my family, spend more time with them, and go home often in the days to come. The fifth is that I have made some new friends who are very nice and responsible and may appear in my future.

*Fire breaks out at world's biggest produce market in Paris 

*The difference between Evening Standard's news and Times of India point of view is that:
1.Evening Standard's focus on rescue operations and Times of India focus on the role of the fire place.

# 2022-09-29 #

*I changed to a new group and got new team members.

*It is interesting to know how the bank actually work.

# 2022-10-6 #

*Today'class is onsite class,I see real people finally.

*In this class, the teacher talked about a lot of financial current affairs and surveyed each student from different countries about the price of their country.

*In the last class,the teacher let us watch fascist and neo-Nazi videos that I knew considerable new knowledge.

*The three things affected me is:
1.Sterling won't fall if UK doesn't print a lot of money.
2.If Trump hadn't been printing much money during his period,America wouldn't inflate seriously now.
3.If the U.S. doesn't lift rates,the US dollar won't rise.

# 2022-10-13 #

*Knowledge is the know of a field.

*It is the fact of knowing something with familiarity gained through experience or association.

# 2022-10-27 #

*The first video told me that depression should not be demonized. In fact, they are normal people just like us. You don't have to be particularly afraid to get along with them, you just need to accompany them.

*Suicide may not be a good idea, but we can't deny the choice they made.

*The best way to deal with depression is to accept him, know that you are sick, and get along with him slowly.

# 2022-11-3 #

*Learned a lot about depression and anxiety.

*In the face of anxiety and depression, you should seek professional assistance, and it is best not to face it alone.

*Satisfaction for me comes when you get something for your hard work, even if you don't get as much as you did.

*Formed a new super group today,met different people, it's cool.

*I agree with the reasons for the freshman resignation.

# 2022-11-10 #

*met a lot of different people.

*Learned a lot of different things today.

*It's getting colder, but it's still a little hot at noon.

# 2022-11-17 #

*I discussed this week's tasks with the team members, and the team members completed this week's tasks efficiently and humorously. 
I am very glad that I am in the group with them.

*The teacher also instructed a lot of knowledge in different aspects. 
I think the teacher is very gifted. He can prepare different aspects of knowledge to teach us every week.

# 2022-11-24 #

*Today teacher asked us to turn our phones to mute and put it on the table in front of the class to count how many times we looked for our phone during the class.

*Today I learned how to understand events. I think this is a very important thing, because there are all kinds of news happening every week. The ability to read the media is indispensable. It is necessary to identify which is true and which is not. fake, avoid being misled.

*There are a lot of homework assigned by the teacher this week. Although there are a lot of exams, I will try my best to be perfect.

*I played the lottery yesterday,but I bet on the wrong country,so I lost money.

# 2022-12-1 #

*I changed a new team member this week, and I feel very scared, and I have to adapt to a new person again.

*It's finally getting colder, the world is sick and it's not really getting cold until December.

*There are a lot of exams this week, I hope I will do my best to get the best score, and I also hope that I will not be lazy in the next few weeks and can study hard.

# 2022-12-8 #

*It's so cold this week. Humans are a contradictory species. They like summer in winter and winter in summer.

*There is only one exam this week, and there is no exam next week. I can finally take a good rest. I advise you who are reading my diary not to study mechanical engineering.

*This week's World Cup is Brazil vs Croatia, I bet Brazil will win, but Brazil lost, costing me a hundred dollars,Croatia is the god of PK.

*Croatia vs Argentina this Wednesday, I bet on Croatia to win,Croatia vs Argentina this Wednesday, I bet on Croatia to win, hope Croatia won't make me lose money.

# 2022-12-15 #

*This is annoying, the weather is too cold, the willingness to produce will be reduced, because you don't want to get out of bed.

*One thing I have realized recently is that there is really not enough time, 24 hours a day is too little.

*I went to see Avatar 2, but I didn't think it was very good. It may be that my expectations were too high, so I was more disappointed.

# 2022-12-22 #

*Today the teacher taught us how to tie a tie, I think the teacher is so cute.

*The teacher's class is as diverse and interesting as usual.

*I failed a test this week, I hope I won't score too low.

*Today is the winter solstice, everyone is eating glutinous rice balls, but I prefer the peanut flavor, and the sesame flavor is a cult.

*I bought a warm bag, otherwise my hands will be frozen in winter.

# 2022-12-15 #

*This is annoying, the weather is too cold, the willingness to produce will be reduced, because you don't want to get out of bed.

*One thing I have realized recently is that there is really not enough time, 24 hours a day is too little.

*I went to see Avatar 2, but I didn't think it was very good. It may be that my expectations were too high, so I was more disappointed.

# 2022-12-15 #

*Unsuccessful and productive.

*I felt unsuccessful because I didn't follow my schedule.I felt productive because I read the exam scope in four hours.

*Compromise and laziness are not allowed, today is the end of the day.

# 2022-12-16 #

*Unsuccessful and productive.

*I felt unsuccessful because I didn't finish my homework.I felt productive because it only took me half an hour from waking up to going out on such a cold day .

*I should not sleep too much, I can only sleep after finishing my homework.

# 2022-12-17 #

*Successful and productive.

*I felt successful because I managed to get up and buy hash browns for breakfast at McDonald's.I felt productive because it only took me fifteen minutes to finish my lunch and go to read.

*I will continue to maintain the habit of going to bed early and getting up early, for McDonald's breakfast.

# 2022-12-18 #

*Unsuccessful and unproductive.

*I felt unsuccessful because I ate supper in the middle of the night.I felt unproductive because I only did one homework.

*I should do my best.

# 2022-12-19 #

*Successful and productive.

*I felt successful because I try hard to force myself to go to the library to study. I felt productive because it took me an hour and a half to finish my homework.

*I should try to maintain the habit of going to the library.

# 2022-12-20 #

*Successful and unproductive.

*I felt successful because I remember to do the laundry.I felt unproductive because I went to a speech that didn't make sense.

*I should have chosen my speech carefully.

# 2022-12-21 #

*Unccessful and unproductive.

*I felt unccessful because I forgot to look at the traffic lights when I was riding a bike and almost got into an accident.I felt unproductive because I didn't do my homework all night.

*I will focus on my next ride.

# 2022-12-22 #

*successful and productive.

*I felt successful because I remember that the cold snap was coming and I wore a lot.I felt productive because it took me nine minutes to ride from my house to school.

*I will maintain the good habit of checking the weather forecast the day before.

# 2022-12-22 #

*Today the teacher taught us how to tie a tie, I think the teacher is so cute.

*The teacher's class is as diverse and interesting as usual.

*I failed a test this week, I hope I won't score too low.

*Today is the winter solstice, everyone is eating glutinous rice balls, but I prefer the peanut flavor, and the sesame flavor is a cult.

*I bought a warm bag, otherwise my hands will be frozen in winter.

# 2022-12-29 #

*Happy new year! I wish all those struggling in the mechanical department can achieve their ideal goals.

*This year's New Year's Eve originally wanted to go to the New Year's Eve concert, but after listening to it for less than fifteen minutes, I felt like peeing, so I went to pee nearby and then left. I ended up watching the rebroadcast of the New Year's Eve concert at home.

*It's really great that there is no rain this year! But it seems to be raining in the north, what a pity.

*In the new year, I hope that I will get better and better. Whether it is academics, love, or career, I hope to find what I want.

*In 2022, I still want to thank those who treated me well and those who treated me sincerely, thank you, I am very lucky to meet you.

*2022 is not a bad year for me, but I still have a little complaint here, people who have hurt me in 2022, people who are not kind, people who are false and love to spread rumors, please get out of my world without you Injury, I will still grow. (Is this tone too harsh?

*I still have to face the reality after the crossing. I have a lot of final exams next week, and I hope I can get a high grade.

*This year's New Year's Eve also has a small episode. In fact, I was going back to Taoyuan to spend the New Year's Eve with my family, but because my mother was diagnosed, I didn't dare to take the risk of being diagnosed. 
After all, if I was really diagnosed, I would go home I can’t take final exams on weekdays, so I ended up staying in Tainan for New Year’s Eve, but Tainan’s New Year’s Eve is very boring, and there are no famous artists.

*Although I said it didn't rain during the New Year's Eve, it started to rain at three o'clock in the morning.

*Another thing happened this week, that is, I failed the fluid mechanics test, which made me cry on the last day of 2022. It's ridiculous. I didn't cry because of the new year, but because of my grades.

*I hope there will be no more virus outbreaks in 2023, and no more bad things will happen. Putin will stop attacking Ukraine. I hope the world will be peaceful.

*I hope that after 2023, no one will say #FUCK2023 hahaha.

*In 2023, I hope that I will become more and more mature, and the people I meet will be good people.

*There is another thing about the New Year's Eve this year, that is, couples in many countries have officially announced that they are dating or getting married and pregnant. I think seeing these good news, I will be infected with the same good mood.

*In 2022, I have some friends who have successfully transferred to other departments. 
I am very happy for them to see them get what they want step by step with their own efforts, but at the same time, I also feel that a group of people who were very good at the beginning are now only The few remaining in the mechanical department are a little sad. 
The reason for the sadness is that everyone is making progress, and only I am still staying where I am, and I don’t know what my future ambitions are.

*I hope you are all in good health and all the best.

*Of course, I also hurt some people this year. I have been in an irresponsible state for a long time. I will work hard to improve and become a more responsible person.

*Let me talk about my regrets in 2022. It should be that I failed to express my love clearly to my family.

*In 2022, of course, couples will inevitably break up. I hope they can find each other's true belonging.

*In the end, I hope that those who love me and those who love me can be well.

*Finally, I hope that the teacher can be happy too, that the son and wife are obedient, and that the teaching assistant papers are produced smoothly.

# 2023-01-07 #

*1. What will our world look like? 
objective:artificial intelligence
suggestion:learn programming language

*2. What does our world look like?
objective:big data 
suggestion:learning data science

*3. How to check facts and give sources?
objective:Don't believe all the reports on the Internet
suggestion:Learn how to media literacy

*4. How does the financial system work?
objective:break even
suggestion:Learn about stocks, ETFs, and financial management

*5. Why does extremism flourish?
objective:no more extremism
suggestion:Learn history and learn from it

*6. How to live healthy?
objective:Have good health
suggestion:Climbing, walking, swimming

*7. How to handle depression?
objective:happy soul
suggestion:Learn to be grateful and meditate

*8. How to choose happiness/fulfilment?
objective:high score
suggestion:Learn how to review before class and review after class

*9. How does the legal system work?
objective:country has good leaders
suggestion:Look at each candidate's political views

*10. How to stay connected and free?
objective:Balance socializing and solitude
suggestion:learn how to take time for yourself

*11. How to make sense of events?
objective:Fair and neutral understanding
suggestion:Learn how to analyze rationally

*12. Planetary boundaries
objective:know how the universe works
suggestion:Read more scientific magazines and periodicals to understand the mysteries of the universe

*13. Why a third industrial revolution?
objective:sustainable development
suggestion:Learn how to cherish resources without wasting them

*14. How to succeed?
objective:Admitted to a good institute
suggestion:Find out how to study effectively

*15. How to solve our problems?
objective:have a healthy mind
suggestion:learn to communicate with yourself

*16. How to make a difference?
objective:have a good job
suggestion:Learn eloquence to face interview questions

*17. How to find the best partner?
objective:have a good partner
suggestion:learn to observe people

*18. Why is surveillance your problem?
objective:don't surveill on others
suggestion:Learn to trust others and give others privacy
