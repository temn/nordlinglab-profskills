This diary file is written by Olivier Jani N16128402 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-07
- The first lecture was interisting.
- I like to do in first the quizz at the beginning of the year and after at the end. It is a good method.

# 2023-09-14
- The second lecture was great.
- The method to using GIT for keeping course diary is interesting. I think it is a good method.
- I liked the other student's presentation, it was very interisting to see which object they took.

# 2023-09-21
- The member of my group Ivan passed to present our homework on the subject of the hapiness. He did a great presentation.
- I really liked the TED talk on the hapiness, it was really interisting.
### Homework 1
- In 10 years' time, I hope I'll have a good life situation. Both professionally and personally.
- Professionally, I'd like to work in France, if possible in Toulouse because I love the city. I'm thinking of working in the aeronautics or aerospace sectors. I'm really interested in these fields.
- I'd like to work for a big company like Airbus, and be a project manager. I like the aspect of managing a team.
- On a personal level, I hope that I'll keep a foot in Corsica, like my mother's house, because that's where I grew up and I want to go back there often for holidays etc.
- I hope that I'll also have a good relationship, a girlfriend with whom I've been living and working for a while, and that we'll start thinking about having children...

# 2023-10-12
- The lecture of today was interesting. I think to have basics on finance is really important in the everyday life. I had the impression of learn something of really useful.
- The video which explain how the banks work was really nice.
- I will do some research by myself on this subject.

# 2023-17-12
- The presentations of the groups on the country's economies were very interesting. It was really enriching to learn a it of economies on different countries all around the world like Thailand, Indonesia, Peru or Czek Republic. We did our presentation on France but we didn't present it sadly :(.
- The lecture of the day was interesting. I liked the guy who had a hard life and had a lot of twist in his life. It could be really hard to resist at some bad influences. It was an interisting testimony.

### About the future 
- I see a very futurist future. The technology will be everywhere in our life.
- The IA will continue to growth a lot. The humans will create robots with human appearances for help us in the everyday life. Like a robot to keep a baby, to do all the cleaning stuffs, to help a child with his homeworks...
- The transports ways will be very futurist. There will be fly cars, flys bikes, really fast train everywhere. Everything will be electric.
- Everyone will have a microchip in his body to track everything and do everything. 
	- Your health state, to detect if something is wrong. If you have a cancer you will can detect it really early for example.
	- Your sportive performance
	- You will can pay with it, open your car, your house 
- The propers energies like the solar or hydraulic will continue to growth a lot and will constitue the main way of energy
- We will explore more the space, and we will arrive to discover and explore new planet. Maybe some people will live in the space or on ther planet like Mars or an other with better life conditions.

# 2023-26-10
- I liked the presentation of today, a lot of student presented what they did. It was interesting to see some future visions of political people from differents country like Taiwan or Paraguay 
- It was interesting too to discover some visions of student about how they see the futur. Some was like mine, very futurist.
- I really like the today�s topic on how to be healthy. The TED-x from the girl was so good and I really believe in what she said. I think to do a lot of exercices help a lot to be in a good health, and happy in your life. It learn you the value of discipline and determination. I personally do a lot of exercices and I was totally agree about what she said. 
- It was really fun when everyone got up and did the choreography in same time than as the woman. A lot of people laughed, it was a good moment.

# 2023-02-11
- It was cool to mix the groups and discover new peoples, but the communication was a bit hard with the big group. Maybe we were too much in a small area i don't know.
- I discovered my new group, just one person of the group. His name is Christian he is from Beliz and he looks really kind. He has a good English too, which is nice. 
- The lecture of today was a bit sad. Depression is always a delicate subject to discuss, but it was interesting. I've never suffered from depression and I don't know anyone who has and I'm very glad I did because it sounds very horrible.
- The story of the TED-X wife was really sad with his friend who committed suicide. We could see that this woman was still suffering from it, and she will never forget it. 
- I learned that the most important thing when you know someone who is suffering of depression is not to tell him what to do or how to deal with it, but listen him, understand the source of his issues and support him.

# 2023-09-11
- I did the presentation on depression today, I liked to work on this topic, it was interesting.
- I had never been interested in depression because I never knew anyone who suffered from it. But now if one day a friend suffers from it I would know how to react.
- The subject on personal skills was interesting. I think in the professional world, personal skills like social skills are really important. Maybe more than technical skills. 
- I worked like a barman during 2 summer, and I learnt a lot. How to manage stress, communicate with people, work in team…  This was very useful for me.

# 2023-16-11
- It was nice to listen to the presentations and ideas from the other groups. Working on a practical solution for a real-world problem is a positive experience.
- Our group should focus on feasible solutions.
- I look forward to the next group project presentations and hope that the other groups make progress with their ideas.
- The lecture of today was interesting, I learnt a lot of things about how the law system is working.

# 2023-23-11
- It was interesting to learn some laws about Taïwan, and others on others countries like Indonesia, Croatia or Czech.
- The video on how the phone is bad for our attention and concentration was really nice. Since 1 month, I try to be less on my phone to avoid this distraction. For example, when I put the work mode on the phone, all my screen is in black and white tones, and it remind me that I have nothing to do on my phonr when I'm working. Same when it's 11pm, my phone switch automatically in the blanck and white mode, and I go to bed early since that.
- I think it's really important to keep some moments of our day when we do nothing, to think and have reflexions. With the constant use of the phone, it's really hard.

# 2023-30-11
- Today, it was the big groups who did the presentation. I t was very interinsting. I think with my group, we improved our presentaiton comparing to the 1st time.
- Every group seems to improved their job.
- It's true that everyone is completely addicted to the phone, we lose our concentration very quickly.
- But also, the phone is really useful for a lot of things, we have cards on it, some apps are very useful. And for example, if you go in a foreign country, to have the phone is vital. Use google translate to communicate or gooles maps to find your way.
- I travelled a lot in other countries during this exchange semester, and without my phont, it couldn't be possible.

# 2023-07-12
- I liked the new format of the class, when we mixed the groups after the presentations. It permits to meet new people and discuss about the topic of the day.
- The topic of the today's lecture was nice. I think the climate change is very important. It represents our futur. 
- I didn't know about the specific existence of planetary boundaries, it was very interisting.
- I'm worried when I see that we cross many planetary boundaries. We need to change a lot of things, and faster, because it's starting to become irreversible.

# 2023-14-12
- Today, most of the time was spent for the big groups presentation. It was nice to see the ideas of each group.
- I liked the format to vote for the best idea. It makes the class more interactive, I think it should happen more often.
- I think with our group, we don't have an easy topic and it's hard to find a doable action but we will try to do our best.
- We need to invest a lot of time and interest in this project. It's interesting but I'm scared with the coming of the finals exams that I will be a bit short in time and uncapable to consacre the time that I initially wanted to do on the project.

## 2023-22-12 friday
- A. Sucessful and productive
- B. I felt sucessful because I suceed to get up early, took a cold shower, went to the gym. I called also some friends of my home-country.
   I felt productive because I achieved my day-goals. I started to work on my finals andI took some flights tickets for february.
- C. I will get up early around 8:30, take a cold shower, go to the gym and study.

## 2023-23-12 saturday
- A. A bit unsucessful and productive
- B. I felt a bit unsuccesful because I woke around 10am instead of 8:30. But I went to the gym and do my days-goals.
   I felt productive because I continued to work on my finals, and do a big part of my oral chinese exam.
- C. Tomorrow I will go to the barber to have a new haircut. I will also call my mother, it's Christhmas tomorrow. I will go to the gym also.


## 2023-24-12 sunday
- A. Sucessful and unproductive
- B.I felt sucessful because I achieved my day-goals. I went to the barber, I call my mother, I went to the gym and I ate with my friends for dinner to celebrate Xmas. 
    I felt unproductive because I didn't work today but I didn't plan to work so it's ok.
- C. Tomorrow, I will go to the gym, I will work for my finals and I will look the accomodations for my trip in Thailand at the end of the semester.


## 2023-25-12 monday
- A. Unsucessful and unproducitve
- B. I felt unproductive because I didn't work that much. But I still have time for finals so it's ok. I felt unsucessful because I didn't book accomodations for Thailand.
- C. Tomorrow, I have class so I will need to get up early. The afternoon I will go to the gym and I'll work for my finals.

## 2023-26-12 tuesday
- A. Sucessful and unproductive
- B. I had my classes, I went to the gym, I worked and I booked my accomodations for Thailand. So i felt sucessful and productive
- C. Tomorrow I have my oral chinese, so I will work on it.

## 2023-27-12 wednesday
- A. Sucessful and productive
- B. I felt sucessful because I got a really good grade for my oral exam. I felt unproductive because I didn't work for school during the evening.
- C. Tomorrow, I will go to the gym the morning and in class the afternoon.

## rules to sucess
- From the previous day, fix objectives for the next day
- Wake up early 
- Take a cold shower
- Do exercices, gym, run, swimming...
- Stick to your goals for your work, be proactive and efficient

## 2023-31-12 sunday
- A. Sucessful and unproductive
- B. I felt sucessful because I did some sports, and pass a goot time with my friends for the new year. I felt unproductive because I didn't work for school during the evening, it was the new year so I hang out with my friends.
- C. Tomorrow, I will work hard for my final exams.

## 2024-01-01 monday
- A. Sucessful and productive
- B. Today is the new year. I felt productive because I worked hard for my finals.
- C. Tomorrow, I have 3 of my finals, I hope I will succed.

## 2024-02-01 tuesday
- A. Sucessful and productive
- B. I felt sucessful because I think I did a good job for my today's exam. I should pass the classes. I felt productive because I went to the gym, and study for my chinese test that I have tomorrow.
- C. Tomorrow, I will do my chinese test, and finish the video for the project.

## 2024-03-01 wednesday
- A. Sucessful and productive
- B. I felt sucessful because I did a good job for the chinese test, I will pass the class. I felt productive because I finished the video for the project group.
- C. Tomorrow, it's my last day of class. I am impatient to be in holidays and go to Thailand.

