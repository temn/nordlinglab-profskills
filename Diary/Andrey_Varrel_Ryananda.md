This diary file is written by Andrey Varrel Ryananda E64085309 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #

* Today is the second lecture.
* I got the chance to present and i have the same topic as the previous presenter, but my conclusion is not the same.
* Professor told a very important thing to always put the referrence on the slides rather than at the end.

# 2022-09-22 #
* Today is the third lecture.
* I learnt an important lesson from the professor to always follow the standards of our company when doing things such as powerpoint in order to do things smoothly.
* The professor showed us three videos about statistics and it is very interesting and make me to think twice when i saw such statistics and also about fake news.
* I learnt about the website of StopFake that show some news that are fake news, i should start to see if the news is fake news or not as it could impair my judgement.
* I realized that i was too gullible because i was too ignorant to the outside world and i should do something about it.
* 10 years from now I will live in a country which is now a developing country and I hope in the next 10 years will be a developed country. I hope that after I finished my bachelor’s degree, I could continue my studies still here in Taiwan to study master’s. After I am done with my master’s degree, I will then continue to find work in Taiwan and eventually will land back to my home country Indonesia. Even though living in Taiwan provides a more secure place in the financial side I cannot deny the possibility of Taiwan going to a political dispute with China which they said they will go to war with Taiwan and allies on 2027 frightens me, hence I prefer to live back in my home country Indonesia. I will first work according to my Bachelor’s degree which is becoming a civil engineer and hopefully earn enough money in order for me to start my own business, which I prefer as I could not stand doing the same thing for years as it will for sure burn my mental energy out. 
  - The five current trend of change that could affect my life 10 years from now is:
   - Politics in Asia: With the current way politics is being held, we do not know weather there will be another Russia-Ukraine war that will happen in the next 10 years and now that Taiwan and China is heating up it will for sure affect the decisions I will take in the next 10 years.
   - My current mental health: To be honest I do not have the passion at all to be in the engineering field, let’s just say bad decisions are made and currently I could handle being in the field but who knows what will happen when I eventually get burnt out?
   - Automation: With the rapid growth of technology, we could live in a more prosperous world that will reduce the hassle of most of our daily routines such as communing to our workplace and many more, and this is a positive thing as it could reduce our mental pressure to just get up in the morning.
   - Climate Change: How we have been treating the world we currently is not going to a positive future, and with all the global disasters happening around the world it might effect on where I will settle to live in.
   - Scarce Recourses: In order to build all of the things that we now could enjoy resources were mined from our mother earth, now the resources are depleting and we would need to mine more which could cause a further damage to our earth.
* The news story i read is that "Israeli forces kill Palestinian militant in West Bank"
* All of the news sources that has high factuality has the same views that only cited facts about the events that happened.
* About 70 Palestinians have been killed in the West Bank.
* 30 palestinan prisoners in israel refused to eat to protest their conditions of detention.
* The news sources that have a mixed factuality cited that 2022 is the deadliest year in the occupied territory since 2016.

# 2022-09-29 #
* Todays lecture was very informative.
* I could not grasp every single thing in the lecture as it is quite complex to me.
* I have known that managing to keep the worlds economy is hard but i didnt know it was this complex.
* This lectures makes me realize i should have a better financial plan.

# 2022-10-06 #
* Today lecture is physical class!
* the lecture is quite deep talking about how there are bad things because the good people stay silent, facists and extremists, and of course still about finance
* I was able to grasp a lot of new things from this lecture for example what a facists is and etc.

# 2022-10-13 #
* whoops forgot to make a diary before 12 PM, welp my fault haha
* anyways the lecture was great makes me motivated to start working out again coz honestly i was gaining more weight.
* learnt about proving knowledge

# 2022-10-27 #
* today we have a new group
* we also get to present in a supergroup consisting of 3 groups
* todays lecture is about depression and how we can talk with depression safely and joke around them

# 2022-11-03 #
* todays lecture is very deep talking about difference scenarios of depression
* to be honest this week lecture in my opinion there is a lot of interactions of discussion
* this week lecture i didnt remember much due to probably i wasnt paying enough attention as i was getting a little ammount of sleep due to exams

# 2022-11-10 #
* Last week lecture i get to see a lot of other groups presentation and it is interesting
* last week lecture is about law which i kinda dont like to see haha
* we get to go home earlier because the professor went home early

# 2022-11-17 #
* last week lecture i got to go do live presentation
* i got to know that certain countries maintain a 1.5 degrees
* last week is kind of a good week because i got to rest plenty

# 2022-11-24 #
* Taken phone from class and i went looking for it a total of 3 times
* interesting because i was also thinking am i addicted to my phone

# 2022-12-01 #
* This week is a little too fast for me and we will end the semester in 1 month crazy fast
* got a lot of insight for the course project

# 2022-12-08 #
* This week we got to see the updated group actions and some of them is really interesting
* Got to learn Novel Entities is mainly for creating new molecules
* Can't believe time flies really fast

# 2022-12-15 #
* The professor started by introducing us to how voting matters.
* computers doesnt really start third industrial revolution.
* roll out of electric vehicle is twice than combustion vehicle and the price of electric vehicle will be cheaper.
* I learnt that giving out electricity is not that easy and it requires a lot of planning and effort.
* Energy storage is the future.
* Learnt about peaker power plant that is on stanby when we need more power on peak times during the day.
* learnt that batteries have a lot of variation.
* Learnt about inequality that there is a poor distribution in money hence inequality exists.

# 2022-12-16 (Friday) #
* Successful and unproductive
* i Felt successful because i passed my exams but i dont really feel productive as i didnt do anything.
* I will try to find more materials for my final presentation to be more productive.

# 2022-12-17 (Saturday) #
* Successful and Productive
* I felt successful because i catch up with a friend that i havent met in a long time and productive at the same time as i started to work on my finals presentation.
* I will try to continue my finals presentation and hopefully do up to half of it.

# 2022-12-18 (Sunday) #
* Unsuccessful and Unproductive
* I felt both unsuccessful and unproductive because i was lying in my bed all day as the weather is freezing cold and i was lazy to do anything
* I will try to take an early hot bath when i wake up to build up more energy to start my day.

# 2022-12-19 (Monday) #
* Successful and unproductive
* I felt successful because i actually woke up early and take a hot bath as i mentioned yesterday but then i didnt do anything major today.
* I will try to be consistent in waking up early and maybe buy a few books since my finals is mostly about presentations and i have done almost everthing.

# 2022-12-20 (Tuesday) #
* Unsuccessful and unproductive
* I felt unsuccessful because i failed my exam and spent the whole day sulking
* I will start revising for the finals so that i can pass the course

# 2022-12-21 (Wednesday) #
* Successful and unproductive
* I still managed to find the topics to study and arranged it in a timetable but then i didnt do much rather than that
* I will start to follow the timetable i made

* 5 rules to be successful and Productive
* Set a timer: Because i am a person who oftenly loses track of time it will be helpful to have a timer to check your progress.
* Planning: It is very hard to get a thing to go on when there is no goal, hence planning it will help create the motivation to do it and be more productive.
* Have breaks: If we as a human keep on working 24/7 we would burn ourselves out and will lead to more problems.
* Don't multitask: It is really hard to do it as you are splitting your focus and eventually will finish the task longer.
* Don't be afraid of mistakes: we will not start as we are afraid it will go wrong, hence just do it, mistakes are normal.

# 2022-12-22 (Thursday) #
* Today's presentation about the course is very good and on time
* The professor made some interesting point about dressing to success
* I will try to improve how i dress to be better in the work place
* Successful and Productive
* I start to pick back up on my tasks and did some work
* I will keep on following the timetable i made

# 2022-12-23 (Friday) #
* Successful and Unproductive
* I did follow some of the timetable and missed some because i wake up late but i still count it as a success
* I will set up multiple alarms to stay awake and avoid drinking coffee late at night

# 2022-12-24 (Saturday) #
* unsuccessful and unproductive
* I didnt do anything major and just procastinate the whole day because it was so cold
* I will try to wear socks and eat soup the next day to warm our bodies up

# 2022-12-25 (Sunday) #
* successful and productive
* I finished my finals video presentation
* I will continue to let this momentum build up

# 2022-12-26 (Monday) #
* unsuccessful and productive
* i butchered my finals live presentation but i still continue the rest of the day
* I will cheer myself up tomorrow by eating good food

# 2022-12-27 (Tuesday) #
* unsuccessful and unproductive
* my friend told me that one of my friend was talking bad about me because of yesterdays presentation and i spent the rest of the day thinking wanting to slap him
* I will try to control my emotions and let go of the past

# 2022-12-28 (Wednesday) #
* Successful and productive
* I no longer feel hatred towards my friend and i cleared all of my homeworks just finals to go
* I will try to take a break tomorrow and focus on my mental health to prepare me for the finals week

# 2022-12-29 (Thursday) #
* final exams is coming oh no
* the professor showed us videos on how to get viral all the tips to be viral
* also told us why he wanted to teach us this course is to open our minds to be better
* he also told us to write what obituary we want and for me i want it to be like "Who help the blind see again"

# 2022-01-05 #
* it is the final exam! good thing i have studied and i got a pretty decent score
* last week of school hooray!
* professor showed us some tips to get a partner
* honestly i havent really find a partner mostly because of religious practises
* hope i can pass this course