# 2022-12-29
* Today's lesson was fantastic! The teacher's explanation was very lively and interesting, which kept me listening attentively.
* I especially like the examples he uses, they are very visual to help me understand concepts that are difficult to grasp. Also, he interacted with us a lot, allowing us to ask questions freely.
* After this class, I have a deeper understanding of the topic and feel more confident to explore it. I really enjoyed today's session and hope future sessions will be as great!
* I heard a very interesting report today. The speaker was very passionate and made me listen attentively.
* He used lots of vivid pictures and examples which made it easier for me to understand what he was talking about. In addition, he often stopped for us to ask questions, which allowed us to gain a deeper understanding of his thinking.
* The whole reporting process was really interesting and I learned a lot of new things. I really enjoyed this presentation and hope there will be more opportunities like this in the future!
* The winter in Taiwan is actually not very cold, but I still like this season very much. When I stand on the balcony and look at the winter scenery, I feel very relaxed.
* The fresh air in winter makes me feel sober and energetic. I like to walk in nature in winter and feel the tranquility of nature.
* Winter nights are also beautiful, with the moon and stars shining in the crystal clear sky, which is reassuring. I also like winter festivals, like New Year's Eve, where I can spend quality time with my family and friends.
* Winter in Taiwan has brought me countless fond memories, and I love this season very much.
* New Years Eve is really exciting! I can't wait to spend this wonderful evening with my family and friends. We have been planning for a long time, with various party games and food prepared.
* We still have many New Year's resolutions, hoping that the new year will bring us unlimited hope and opportunity. When the bell strikes, we will celebrate the arrival of the new year together and wish for the year ahead.
* I am really looking forward to the New Year's Eve and hope to spend an unforgettable night with my family and friends.
* Final exams are really stressful. I've started preparing for my final exams, but I'm not in a good mood.
* I am very nervous because I know this is an important exam and it will affect my future. I often feel tired because I do a lot of preparation for exams and I often have to study for a long time.
* I was also a little worried because I wasn't sure if I would do well in the exam.
* Winter vacation is really something to look forward to! I can't wait to take a break because I've worked so hard this semester.
* I want to spend some time with my family and friends and have a good holiday together. I also want to travel, to places I've never been.
* That way, I can focus all my energy on enjoying the various activities of the holiday. I'm really looking forward to the winter break, which I hope will allow me to relax and get ready for the new semester.

# 2022-12-22
* At the beginning of class today, the teacher first invited us to share the contents of the diary, and I realized that not many people actually completed the assignment according to the teacher's request.
* But I feel that in the process of keeping a diary every day, I can examine what I have done every day and whether I have achieved my goal for today.
* Or you can also record interesting things that happen every day as memories. I think this is a very good habit, and I hope I can maintain this habit.
* After sharing the diary, the report of each project group started, and I was the reporter of our group. I was actually quite nervous before the report, because I was afraid that the professor would ask some tricky questions.
* The reporting process went smoothly at the beginning, but when it came to the biggest problem we encountered, because the other team members didn't have time to discuss it together, I roughly thought of a reason.
* Unexpectedly, this became the key to the teacher's question. Fortunately, a team member took his GOGORO carbon reduction mileage to the stage and resolved the embarrassing crisis.
* After the report, we started today's class content. The professor showed us the TED speech as usual, and then began to talk about today's class content.
* After the lecture, the professor suddenly reminded us that we still have 15 minutes to change clothes, which made me very nervous, because I did not bring the clothes prescribed by the professor to class. Part-time job, the second reason is that it is too cold outside.
* Then the professor invited us to come to the stage to share the five secrets of success written in the diary. I still remembered what I was going to say, but when I talked about the last one, my mind suddenly went blank, so I had to say a secret of success in a hurry .
* After talking about the secret, I went to the part of voting for the costume, because I didn't wear the costume that I would wear in the interview, so as expected, no one chose my costume.
* Finally, after class, the teaching assistant showed something important to us on the screen, which was the grades.
* Then according to my follow-up understanding, I can know that the teaching assistants give everyone grades according to the number of words, so I worked very hard in writing the diary this time, hoping to make up for my lack of seriousness before and save some grades by the way.
* Today I gave a report on stage, and I feel very excited!
* Although I was a little scared, I was very proud when I saw everyone looking at me.
* I spend a lot of time preparing my presentation, so when I start speaking, I feel very confident.
* I'm glad my presentation was clear and easy to understand, and everyone seemed interested.
* I was very relieved to receive a lot of compliments and compliments after the presentation.
* The whole experience was truly an unforgettable day!

# 2022-12-15 How successful and productive I felt each day.
## 2022-12-15
* A. Successful/Productive.
* B1. I felt successful since the manager in my work place said that I did a great job in teaching senior-high students.
* B2. I felt productive since I make my report done in only 30 minutes.
* C. I would maintain today's work efficiency to the following day.
## 2022-12-16
* A. Unsuccessful/Unproductive.
* B1. I felt unsuccessful since during the automatic control class, we didn't bring the transport wire so that we could't output our Arduino into the EV3.
* B2. I felt unproductive since after the end of the courses, I slept from 6 p.m. to 8 p.m., and I didn't study any subject.
* C. I would remember to bring the transport wire next time, and I would keep myself awake after the end of the courses.
## 2022-12-17
* A. Successful/Unproductive.
* B1. I felt successful since I solved one of the most difficult example problem.
* B2. I felt unproductive since today was very cold, and I stayed in my bed too long.
* C. I would exert myself overcoming the cold weather.
## 2022-12-18
* A. Successful/Productive.
* B1. I felt successful since I had already read the final exam of automatical control.
* B2. I felt productive since I went to the Donutes for studying all the afternoon.
* C. I hoped that I could maintain the habit of study.
## 2022-12-19
* A. Successful/Unproductive.
* B1. I felt successful since the lecture I attending today was skipped, and I used this time to finish my report.
* B2. I felt unproductive since I went to bed at half past 11 p.m., and I thought it's too early for me to sleep.
* C. I would try to sleep later and use this time to read more book.
## 2022-12-20
* A. Successful/Productive.
* B1. I felt successful since I successfully learned how to calculate the root locus.
* B2. I felt productive since I attended my friend's study of automatic control.
* C. I hoped that I could have a good grade for tomorrow's automatic-control final exam.
## 2022-12-21
* A. Successful/Unproductive.
* B1. I felt successful since I filled all the blanks of the final exam, although I was not sure whether it's correct or wrong.
* B2. I felt unproductive since I had a class skipped today's afternoon, but I used this time to sleep.
* C. I would try not to sleep that much.

> In our society, we use the word "success" a lot.
> We think we know what it means: money, status, fame and power.
> But look it up in the dictionary and, thankfully, things start to look a lot more complicated.
> Because in fact, success is more neutral and less value-laden than we usually assume.
> Success simply means being able to do anything well and being good at something.
> And that can include a lot of different activities: 100-meter races of course, selling your app and making a lot of money counts.
> But there are also more exotic and less hyped things.
> Like listening to a child intently for a long time, or being very nice to strangers, or filling your head with interesting ideas and associations, or knowing when to cuddle someone who is about to overwhelm the stress.
> The people who succeed here are great success stories too.
> No one can succeed in everything.
> No matter what they say, it is almost impossible to be successful in both career and family.
> Or a mix of fame and integrity at the same time. There is always something to sacrifice.
> Success is great, but what's even better is being sure you're following your own unique but not always obvious path.
> The path to success that leads to being able to truly express yourself fully.

> What is productivity?
> Simply put, productivity is the output obtained per unit of resource invested.
> In the engineering world, output can often be expressed in terms of weight, length, or volume.
> Inputs are usually expressed in labor costs or hours worked.
> The actual progress made per hour per person is known as labor productivity.
> It occupies a large proportion in the overall productivity calculation of the engineering industry.
> Two important indicators to measure labor productivity refer to the efficiency of labor in the construction process.
> And the relative labor efficiency of people performing a specific job at a specific time and place.
> When calculating labor productivity for engineering projects, you can consider the following questions:
> How much work can a 20-person engineering team get done per day?
> How many days will it take to complete the work if the number of construction workers is fixed?
> If the time to complete the project is limited, how many construction workers will be required to complete the project on time?
> If you invest in equipment that makes work faster, how much time can you save completing related work items?
> Why is productivity important?
> Labor efficiency is the basis for most bid valuations and the standard by which performance is measured and monitored.
> However, labor efficiency is closely related to physical resources and competencies in terms of knowledge and skills.
> If construction personnel can only use old tools, their construction efficiency should not be able to achieve the ideal high productivity.

# 2022-12-08
* We was doing our course project today, and many groups' idea was really interseting.
* I would go to the McDonald after the lecture ended.

# 2022-12-01
* Today, Professor's son was coming to our class, and he was so cute.
* I forgot to write my diary this time, hoping that I could get my attendent score.

# 2022-11-24
* Today I was going on the stage as a volunteer of presentation for responding the question.
* In my memory, there was one group that addressed the noise problem by buying the earplugs, and I thought that their idea was very interseting.

# 2022-11-17
* Today I was being asked to go on the stage to talk about the time limit of presentation's country, and I felt very nervous since I didn't search for the time limit but volume limit.
* I thought that today's class was great.

# 2022-11-10
* Today was doing the project presentation, and everyone's presentation were very great.
* I skipped the class after professor went out the lecture at 4 p.m.
* Not until today did I know that we need to upload the link of video on Bitbucket.

# 2022-11-03
* Today's class was talking about depression, and I had learn a lot of methods used to helping those who had depression from professor and classmates.
* I thought that today's first class was a little heavy, so I choosed to do my own private thing during the presentation.
* During today's class, I found one thing that lots of students were abscented.

# 2022-10-27
* Today was the online course, and professor's wifi didn't have good signal.
* After listening one-hour course, I realized that it's time for me to sleep, but I steel try my best keeping my eyes on the screen.
* I didn't know what to write today.

# 2022-10-13
* Today is the second day that we had the realistic class, and I felt very nervous, too.
* HAHA.

# 2022-10-06
* Today was the first day that we had the realistic class, and I felt very nervous.
* Professor had ask some of problems to the presenter, but I thought some conclusion said by professor was not needed to say since it was just a personal habit.
* During the lecture, the computer's volume was too quite to hear, and I had the method to solve it.
* First, you could click this URL, "[Volume Master 音量控制器](https://chrome.google.com/webstore/detail/volume-master/jghecgabfgfdldnmbfkhmffcabddioke?hl=zh-TW), and then added it to the GOOGLE, then clicked the puzzle and used it.
#### 🔥Three fictional stories that I believe in and how they have affected my life at some point.🔥
> The first story is "**Auntie Tiger.**"  
> When I was little, my mom would sing a song when I was going to go to sleep.  
> The song's lyrics were...  
> 🎵 A long, long time ago.  
> 🎵 Mommy told me so.  
> 🎵 In the dark and silent midnight, Auntie Tiger is there.  
> 🎵 Crying baby, please don’t cry.  
> 🎵 She might bite your little ears.  
> 🎵 Sleepless baby, go to sleep.  
> 🎵 She might bite your pinkie.  
> 🎵 I remember, remember, I narrowed eyes and said “ Auntie Tiger, give me a break.”  
> 🎵 Then I fell asleep in my bed.  
> After listening this song, I felt that if I didn't fell asleep at night, I would be caught by Auntie Tiger.

* Aunt Tiger's most commonly heard version of the story:
* The tiger spirit is practicing and must eat a few children to become a human, so he went down the mountain to find the children to eat.
* After going down the mountain, it hid outside a house and eavesdropped, knowing that its mother was going out and there was only a pair of siblings in the house.
* So she pretended to be an aunt and tricked the child into opening the door and entering the house.
* Sleeping until midnight, Aunt Tiger ate her younger brother, making chewing noises.
* After hearing this, my sister asked Aunt Hu what she was eating. Aunt Hu said she was eating peanuts, and then threw a piece of her brother's finger to her sister.
* My sister calmly pretended to go to the toilet, and then hid in the tree outside the door.
* When Aunt Tiger found out that she was going to eat her, she wisely asked Aunt Tiger to boil a pot of hot water (otherwise it was hot oil) for her.
* And asked Aunt Tiger to hang hot water to the tree for her, and she wanted to jump into the pot by herself.
* When Aunt Hu hung the hot water to the tree with a rope, my sister told Aunt Hu to close her eyes and open her mouth.
* Then pour hot water down Aunt Tiger's throat, and Aunt Tiger died.
* But there are different versions of the story in different regions, so the story also has a different beginning and ending.
* For example, in some versions the children in the house are a pair of sisters rather than siblings.
* The process of subduing the tiger essence is that the gods incarnate as mice to instruct the two sisters how to escape from the tiger's mouth, and finally subdue the tiger essence with amulets.
* There is also a version that after the hot oil scalds the tiger spirit, the sisters beat the tiger to death with sticks.
* But there are different versions of the story in different regions, so the story also has a different beginning and ending. For example, in some versions, the children in the house are a pair of sisters rather than siblings, and the process of subduing the tiger spirit is that the fairy incarnated as a mouse instructed the two sisters how to escape from the tiger's mouth, and finally subdued the tiger spirit with amulets. There is also a version that after the hot oil scalds the tiger spirit, the sisters beat the tiger to death with sticks.  

> The second story is "**Cry Wolf.**"  
> The story was about...  
> 📖 A little child didn't see any wolf, but he cried that a wolf was coming.  
> 📖 Then every adults were runing the house and ready to fight, but there wasn't any wolf.  
> 📖 And then the second time, the boy did the same thing, when villagers came out, there wasn't any wolf, either.  
> 📖 One day, the wolf really came, and the boy cried everyone that the wolf was really coming.  
> 📖 But no one trusted him, and then the little boy was eaten by the wolf.  
> This story affected me a lot since I believed that if someone lied too many time, nobody would not trust his or her anymore.  
> Since then, I didn't say too many lies.  

* Cry Wolf's most commonly heard version of the story:
* A boy shepherd drove the sheep to the hillside to graze every day.
* The little boy felt bored doing the same thing every day, so he came up with an idea.
* He suddenly rushed down the hillside in despair, and shouted at the village:
* "Wolf is coming! Wolf is coming!"
* The people in the village heard this, and they came running with sticks, shotguns and other weapons to help the boy beat the wolf away.
* But when we got to the hillside, we only saw a laughing little boy and a group of sheep grazing quietly, but no wolf at all.
* The little boy felt that the prank was very successful, so he played this kind of "wolf is coming" game every now and then, which made the villagers very angry.
* One day, the wolf really came, the little boy was frightened, and he called for help loudly.
* But this time not even a single person came to rescue him. As a result, all his sheep were eaten by wolves.
  
> The third story is "**A Crow And The Pitcher.**"  
> The story was about...  
> 📖 There was a bottle with half water on the ground in the dry-weather day.  
> 📖 Then there was a bird coming to the bottle, wanting to drink water.  
> 📖 But it couldn't drink any water since the water level was low.  
> 📖 And then the bird went to pick some small stone inside the bottle to make the water level higher.  
> 📖 At the end, the bird used its smart to drink the water.  
> This story told us that you needed to use you brain to solve the problem.  
> Since I had heard this story, when I confronted the obstacle, I would try to find a smart to solve it.  

* A Crow And The Pitcher's most commonly heard version of the story:
* A crow was thirsty. It circled low in the sky looking for water to drink.
* After searching for a long time, it found a water bottle not far away, so it flew over happily, and landed firmly on the mouth of the water bottle, ready to drink water happily.
* However, there was too little water in the water bottle, the mouth of the bottle was small, and the neck of the bottle was long, so the crow's mouth could not reach the water anyway.
* What should I do?
* The crow thought, knock down the water bottle, and you can drink the water.
* So, it rushed down from a high altitude and slammed into the water bottle.
* But the water bottle was too heavy, and the crow exhausted all his strength, but the water bottle remained motionless.
* In a fit of anger, the crow grabbed a stone from not far away and threw it at the water bottle.
* It wanted to drink water after smashing the water bottle, but it didn't expect the stone to be impartial, and it just fell into the water bottle with a "plop".
* The crow flew down and saw that the water bottle was not broken at all.
* The careful crow found that the stone sank into the bottom of the bottle, and the water inside seemed to be higher than before.
* "There is a way, now I can drink water.
* "The crow was very happy, it yelled "Wow" and started to move.
* It took many stones and threw them one by one into the water bottle.
* As the number of stones increases, the water in the water bottle rises slowly bit by bit...
* Finally, the water in the water bottle was almost up to the top of the bottle, and the crow could finally drink the water.
* He stood at the mouth of the water bottle, drinking the sweet and delicious water, feeling so happy and comfortable in his heart.

# 2022-09-29
* In today's class, I heard few of prople presentating, and all of their presentation was amazing.
* I hoped someday I could be a person who can presentating like those guys.
* I still didn't know what to do after class, hoping teacher could **upload the sheet of homework on Moodle** that we could easily check what we need to do after class.
* There was something coming up in my mind that whether should I quit this class or not.
* After listening three-weeks lecture, I thank that my English was really suck.
* This was the video I shared today: [【放火】我做過有史以來最殘酷的二選一，我崩潰到哭了…🥺 【hololive二選一】](https://youtu.be/ToRmU2TXTPw)

# 2022-09-22
* Not until today did I create my diary since there was an activity in our department, and I needed to prepare for it.
* I needed to say sorry to professor since I was too busy to complete the presentation of 3rd week, and I would upload it before next-week-class.
* This was my first time using this "Bitbucket" application, and it was really an unforgetful experience to me.
* Although I rarely wrote the diary, I would try my best to write the dairy fulled with meaning.
* Another decision I made was to post a funny video I seen on YouTube, making the students, professor, and TA in this class can relax for minutes.
* This was the video I shared today: [HowHow Power O-oooooooooo AAAAE-A-A-I-A-U- JO-oooooooooo](https://youtu.be/tCFJfti-gXo)

# 2022-09-15
* I thought that this was not an proper application for us to write diary, since I had write this diary for many time due to the system error.
* I wrote this diary in 2022-09-22.
* Today was our first time to give the presentation, and I felt very nervous since I had never had the English presentation before.
* After listening second-group presentation, the first thing came up in my mind was " I suck."
* Hoping professor would not treat me like second group, because I couldn't answer professor's question precisely.
* Eating is a very happy thing. Whether it's a delicious meal at home with family and friends, or a dinner with colleagues in a restaurant, it can make people feel happy and satisfied.
* When you sit at the dining table and see your favorite food in front of you, you will definitely feel very relaxed and happy. Each of those carefully prepared dishes exudes a charming aroma, making your mouth water.
* When we eat, we are not only satisfying our appetite, but also having a good time. Eating with family and friends can not only share food, but also share each other's life.
* When eating, everyone can chat and laugh, making the whole atmosphere relaxed and happy.
* When you are full, sit on a chair and feel the satisfaction of being full, you will find how happy it is.
* Eating is not just to satisfy appetite, but also to enjoy the fun of life. So, when you eat, be sure to enjoy it and make yourself feel good.
* This was the video I shared today: [學士服內「抽出2光劍」！畢業生揪老師舞台上決鬥｜搞笑｜師生｜畢業典禮](https://youtu.be/fxC3Bfuoj0A)

# 2022-09-08
* I wrote this diary in 2022-09-22.
* This was the first lecture of NordlingLab-ProfSkills, and I was little nervous since this was the first full English class I took.
* The situation today was so messy, and I sencerely suggested TA or professor that you could just use the GOOGLE FORM instead of GOOGLE SHEET.
* Since there were too many people online the sheet, and after 100 people in it, the sheet would not let other person enter it.
* Although the system was not really convient, I would try my best finishing this semester's class.
* I feel really happy when school starts! Finally, I can reunite with my friends and share the experiences and feelings of the past summer vacation together.
* I am also looking forward to meeting new teachers and learning new knowledge. Listening to the teacher's explanation, asking questions, and discussing with the students in class is one of my favorite things.
* Whether in the classroom or on campus, I can feel the full learning atmosphere. School is full of activities and opportunities that allow us to try new things and discover our interests and talents.
* As students, our possibilities and potential are endless. I really hope that in this new school year, I can study hard, challenge myself, and prepare myself for the future.
* I believe that here, we will be able to achieve success!
* This was the video I shared today: [孫子講髒髒的笑話給99歲阿公聽，阿公聽懂後整個笑翻 ](https://youtu.be/ScJk2gwR8CE)