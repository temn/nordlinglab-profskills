# 2022-09-15 #
 
* Before the class, I thought that it can be short and easy for just a three-minute presentation. But after several groups had given the presentation, I found that everyone in the class had all prepared so well. 
* Since I look forward to hearing different ideas from the class and also enjoy presenting, I think it is a bit of pity that there is not much time for every group to share their ideas. 
* It seems like we were all busy with our schoolwork, so our group had few discussions this week. Hope that we can go with a swing next week.
* Since that many files for the course this year are not differentiated from the old ones, it really confuses me a lot during the class.
* GIT is the new technique thing that I am really not good at, but I’ll try to figure it out… 



# 2022-09-22 #
`DIARY`
 
* It’s the third week of the lecture, and with the online course, I am easily driven to distraction. Looking forward to the class in person.
* At the beginning of today’s lecture, we spent some time reviewing and dealing with the problems and issues raised by the class last week. Though many of us may share the same problems, there were different advice offered, I think it is great that we can discuss with the professor and adjust the way of the lecture right after every class.
* I think it is hard for me to think about the future 10 years after. Since I am a person who believes in seizing the day, 10 years is too long for me to imagine. Thus, I write the me-after-10-years according to the industry trends related to my department and what I will probably study during my master’s years, but I don’t think that’s the life I’ll have after 10 years from now. 
* It’s new to me to know the ground news, and it’s quite interesting to discover and study the different views of different sources and media. It also recalls me that someone once told me that the content of news not only includes the truth but also the political spectrum of the media.



`ME AFTER 10 YEARS`

>It’s 2032, after completing my master’s degree and working for several years in the US, I am now back in Taiwan. I am a research engineer on materials in a company producing storage batteries. During the past 10 years, the most common transportation tool has transferred from gasoline cars to electric cars. The revolution of transportation causes high demands on car-used batteries and thus bring me this job. On the other hand, the issue of the energy crisis, global warming, and environmentally friendly has never stopped being discussed. Therefore, our company is now dedicated to developing new products with environmentally protecting materials, which is the research project I’m now participating in.

>* The first trend that can easily affect our life is the rise of electric and automated cars. Due to the issue of the energy crisis and air pollution, electric cars have become not only a new choice but also a trend. Automated cars, most people now buy them for their disposition, but in the future, these cars could probably replace drivers’ work. The needs for these cars from markets make the correlated industry flourish.
>* The second trend that may change the global and society is the importance and the general trend of cooperation and union between different countries. Take the economic aspect for example, while the agreement with other countries can affect the trade mode, it can indirectly affect the domestic industry of one country. The cooperation can protect the rights and benefits of the member country.
>* The third trend that may change our close life is the power and impact of the internet. Since the development and the use of the internet have been more and more mature, people can get and spread information so widely and timely. However, easy access to the internet or unfiltered information from it may also cause ethical problems and mental problems to people. There might be more laws set to prevent crimes on the internet.
>* The fourth trend is the rise of artificial intelligence. The technique of AI has been developed for a period and put into practice in real life. It can relate to the industry and the progress of robots. In the future, robots may become important characters in our life. For example, works with high risks can be taken by them. Take another example, which is the trend of highly developed countries, for the aging society, robots will become an important supplement of manpower.
>* The final trend is the change in lifestyle and thoughts of human beings. Since the covid-19 has affected the world so seriously about two years ago, I think how people think and act has changed a lot. It makes people hard to get along or hang out with others. New kinds of activities or ways of life have been developed such as online courses, working from home, workout at home, and so on. Also, of the unexpected and difficult situation caused by the virus, more people tend to live for the moment and spend more time thinking about their own.


`SUMMARY OF NEWS FROM GROUND NEWS`

**Saudi Arabia plans to send female astronauts to space in 2023**

* **Left side** https://news.cgtn.com/news/2022-09-23/Saudi-Arabia-s-first-space-program-to-include-female-astronaut-report-1dyuQeeBAmA/index.html?utm_source=ground.news&utm_medium=referral 
>It fetched and emphasized the point mentioned in the report from Saudi Press Agency: “The program will enable Saudi astronauts to conduct scientific experiments and research for the betterment of humanity in priority areas.”
* **Center** https://www.local10.com/tech/2022/09/22/saudi-arabia-plans-to-send-female-astronaut-to-space-in-2023/?utm_source=ground.news&utm_medium=referral
>It describes the past work, the important part, and the future work of the program announced by the Saudi Arab in an objective tone.
* **Right side** https://www.news18.com/news/world/giant-leap-for-saudi-women-in-a-first-female-astronauts-set-to-go-to-space-6014581.html
>It highlights the policy progress related to women in recent years in Muslim countries and draws a conclusion that the program is a significant move and ambitious project for these countries.

# 2022-09-29 #
* This week, we learned about the concept of the value of money. Including how countries define and measure its value. It makes me feel that money is a thing that can be so real and so abstract at the same time.
* Our group finally had our first discussion in person, though it was the last time to work on the task with this group. The work was much better than the former two weeks, but it still turned into a bad result because we didn’t prepare the video presentation.
* There is a member that he had dropped the class very early, so we have just two members in my new team. I think it will be better if the TA can check out and renew the member list.

# 2022-10-06 #
`Diary`

* This week was the first physical class. Because the professor would ask us randomly to talk about our opinions, it forced me to pay more attention to the class. Though the workload of the course seemed to increase, it became more interesting than the online course and also made me think and learn more about the issue that the professor wanted to talk about,
* In the group report from last week, we introduced the economic situation of our own country. Among the three groups that had the presentation during the class, there was one that discuss Indonesia’s economy, we thus interviewed the Indonesians in the class. I was then surprised that we have so many classmates from different countries.
* In the TED talks video that we had this week, one of them talked about the relationship between data and the future of democracy. The speaker explained how fascism rose, and how to avoid repeating the same mistake. One thing that we could do was mentioned by him: to know the weakness of yourself and not to be manipulated by the fascists. 

`Three fictional stories that affected my life`

* Recently, many foreign countries have gradually lifted lockdowns. Traveling abroad could be free as the day before covid-19. Thus, me and my friends started to talk about our graduate trip to a foreign country.
* According to the seniors now studying or working in the US, the US government may do something to deal with inflation these years in the future. It may then cause international students to find internships or jobs more difficult. After knowing that, I can’t help to worry about my future.
* With the law relating to military service has been amended and the threat from China being more serious recently, our family jokingly discussed the possibility of women doing military service in Taiwan. 

# 2022-10-13 #
* Though it is unexpected to know that we have a new teammate this week, we have a pleasing and thorough discussion about the task given this week. We have thought of so many ideas that are related to the topic in the TED talks. Some of these ideas are associated with our own experience while others come from what we have in Taiwan. 
* In today’s course, we discuss how to define “knowledge” with our neighbors in the class. Knowledge is quite abstract to be given a definition, and we think that all we have learned about is probably information instead of “knowledge”. It is quite interesting to discuss with other classmates. I thus met some others, and we have a joyful chat during the break time.
* For the question raised by the professor: why are we still being taught in school though these rules and information are wrong? One classmate answered that though what we learn is not completely right, it is approximate to the reality that happens in our daily life, and they are just simplified to let us understand. It recalls me that the exact same concept had been discussed by my high school physic teacher.

# 2022-10-27 #
* We have a supergroup composed of several small groups this week. I found it rather inconvenient for us to meet up with the group members and have discussions online. 
* We have videos about depression this week. I am impressed by one of the points from the speaker: speak normally in the way you are while facing someone suffering from depression. 
* There is a bunch of work and exams waiting for me in the following weeks. Feeling a bit anxious; hope I can handle them well…

# 2022-11-03 #
`This week - Professional skills`

* We share that if we have taken any class teaching professional skills that an engineering student should know. Though, I still have no idea what’s the difference between technical skills and professional skills mentioned by the professor after the class this week. Maybe it is more like skills that you need not only in your job but also in daily life.

`Last week - Anxiety survey`

* We spent a bit of time discussing why we felt more anxiety about school work rather than climate change since the environmental issue matters bigger worldwide. It reminded me that I used to care a lot about climate change when I was in elementary school. We watched many videos about the severe results of climate change and discussed them in many classes. 
* However, as I grew up, got into high school, and until now, I feel myself not caring that much about climate change. I am not sure if it is because I am getting used to the media discussing the climate apocalypse every day or if I really can’t concentrate on my study and this large and serious issue at the same time. Or, maybe it is just because I was always frightened by the scene in disaster movies, so I cared a lot when I was small (lol) . 
* Anyway, the view mentioned by the class that most of us feel anxiety about what we can see now is quite reasonable since these are things that we can do something to change right away. 

# 2022-11-10 #
* We have listened to several course projects from other groups. And not until the class this week did I realize that we will have to complete the actions we set in the project.

* The class this week ended early, and the videos that were supposed to watch in class became assignments. But I think maybe many of us won’t watch them lol.

# 2022-11-17 #
* For the presentation from last week, our group was the only group that chose a topic that didn’t supplicate, and I was asked to come up to the stage to answer questions about the presentation for the first time. 
* We formed a larger group to discuss and think of some enforceable laws to safeguards freedom of speech but also prevent fake news. A first, it was quite hard for us to figure out what that actually meant. But we soon somehow come up with some ideas.

# 2022-11-24 #
* This week we have reviewed the course projects from last week. The professor mentioned that we can just choose one or we can choose all the topics based on the “or/and”. Our group had done all of them since we didn't even notice the term lol.
* This week, the professor asked us to cooperate with an experiment that handing in our cellphone to the front desk to test if we feel ourselves  addicted to cellphones.  I think I am fine without my cellphone during the class, but it was quite inconvenient when we wanted to check out anything uploaded on GitHub. 

# 2022-12-01 #
* Professor’s son is sooooo cute, babbling and immersing himself in his own world. 
* Since feeling tired after just coming back from my pleasant tour of Japan Wednesday evening, and having to face a bunch of delayed work and reports, I had considered skipping the class this week. Yet, well… I am still here then to stop myself keep vegging out.
* This week, we have a discussion in an instant-formed group. Everyone’s speech was quite funny (and I had several times burst into a laugh lol) though, our group quickly came up with a clear and specific conclusion.

# 2022-12-08 #
* I accidentally found someone copied one bullet point of my diary without changing a word last week, awkward…
* This week, we voted for the action and boundary of every group’s course project. Actually, I don’t get the point of the whole vote. 
* We are going to carry out our action of the course project this week.
* I went to a seminar before the class, so I got to the class late at 15:00. However… it did not seem to matter whether I get to the first class or not, I can still catch the class progress. Umm… maybe that’s why many people don’t even get to the class lol. Whatever. It was an English seminar talking about the technology of making passive components. Since the lecturer spoke in a really flat tone, I just can’t help falling asleep…

# 2022-12-15 #
* Unsuccessful and productive
     - There is going to have a quiz in my British literature class, and we can prepare the answer to the essay question in advance. However, the given essay question this time is much more difficult to explain than the last quiz, and I spent the whole night figuring it out, organizing my answer into an article, and trying hard to memorize it. On the other hand, I made an effort and did a lot of work to prepare for my quiz.
# 2022-12-16 #
* Successful and productive
     - I have tried my best to write down all my analysis of the given passages in the quiz though I almost couldn't finish it.
     - I finally got to eat the brunch I have always wanted to eat. 
     - I spent my afternoon working on the essays for applying to graduate schools, and I think I had finally figured out what to write.
# 2022-12-17 #
* Successful and unproductive
     - I retook the TOEFL test today, and the score seemed to reach my goal, hooray! 
     - I was supposed to rush back to Tainan to take graduation photos, but it was then canceled due to the bad weather. Thus, I spent my rest day at home with my family.
# 2022-12-18 #
* Unsuccessful and productive
     - Our department volleyball team had an intramural contest today. The first game was scheduled for 8:00 am. We were all freezing and played badly. This is my last year to play this cup, it is a bit of a pity. 
     - It is close to the end of the semester; I get to work harder on my final exams. I couldn't believe that there are going to have seven or eight exams during the two weeks before and after New Year’s Day. How tragic…hope I am not to die...
     - A few days ago, my friend told me that she had fallen down and broken her front teeth. Though the whole thing sounds a bit exaggerated, I was so nervous asking if she was doing well. She just sent several emojis leaving me questionable. Not until today did she send her selfie telling me that it was just a joke. I am too nice like a fool so that often been fooled by her from high school till now...
# 2022-12-19 #
* Successful and productive
     - I spent the whole morning catching up on the pre-recorded online accounting class from the week in which the teacher was absent and I felt myself really productive. Then… I got back to take a nap (lol). 
     - Actually, I wake up naturally early at six or seven every morning no matter how late I stay up at night. I think it has been a habit since my summer internship. So...I think what I should do is get to bed early at night.
# 2022-12-20 #
* Not really successful or unsuccessful and productive
     - I had classes all day long and department volleyball team training at night, so it was kind of productive…? 
     - I will plan well what to do tomorrow and get to sleep early tonight.
# 2022-12-21 #
* Successful and productive
     - I was so surprised that my French class partner gave me a Christmas card this morning. I actually thought that we didn't know each other that well (lol). I finally finished one of the troublesome final group reports.

# 2022-12-22 #
* I gave the live presentation this week. Well…I didn’t expect that I would be the one to present on stage, so I didn’t prepare at all and kept tripping over my own tongue (lol). But at least I had been involved in the discussions of our group, so I had a rough idea of what to say. 
* Last week the professor asked us to dress up successfully. I tried hard to find clothes in my closet that looked formal and would not make me freeze to death… It has been freezing these days (but Ada still dressed in a t-shirt only haha). Everyone with the most voted said that they would not dress like that for an important interview. As for me… though only a few people approved of my outfit, I think mine was acceptable (lol).

# 2022-12-29 #
* I totally forget we have an additional class on January 7th until my group member mentioned that. I was supposed to hang out with my friend right after the end of the semester, but the plan has to change now QAQ.

* I finally found out how the diary is graded and where I can get to check out my scores. It is quite fun to look through the data. It will be the last few chances to write the diary to get my score up in this class. So, I’m gonna write a little more this week, though I take it seriously every week on writing this (lol). It has kind of become a routine for me to review my week and write something in English. It was also quite entertaining to peep at others' diaries. I think I've gonna miss this. Or maybe not (lol).  

* This week, the professor asked us to write down a sentence that we want to put on our obituary. Wow, I think it’s really hard for me. Though all the ceremonies for the dead seem to be a memory and respect for them, I think all these are more done for the live people. So, maybe any words in positive would be okay, even sentences such as "An ordinary person with an extraordinary life."  I don't think I could do something that is really great to contribute to the world. I just try my best to make the life around me a little more extraordinary (lol). That's how I think at least at my age now. But who knows, maybe I would change my thoughts and find these thoughts that I have now funny within several years after graduating.

* One of the TED talks this week talked about how people in society may expect you to have some achievement or status at a certain age in your life. It makes me think about some views I have had about life plans. As an engineering student born in Taiwan with such an industrial chain and working environment, I have always thought that people from engineering are like clones, and maybe nearly 80% of us are having the same life plan. This often makes me consider my future life will be restricted and boring.

* There is another TED talks about a company arranging a customized activity. I disapprove of some ideas about providing personal information from the speaker. I often feel horrible finding an advertisement on the internet recommending something I have just searched for. I don't like being exposed to big data collectors, but it seems to be unavoidable nowadays. Enjoy the convenience as well as being exposed, it is like the two sides of the same coin.

* The course project we have for this semester has gradually come to the end. Fortunately, our group has worked in a right and simple direction from the beginning. Some of the other groups' actions were quite large and expected. Look forward to their results!

* Happy New Year to everyone! Let me think about my new year’s wishes after my exam week... I need more time to sleep rather than think about the wonderful future I am going to practice.

* Wow, I think this week I’ve written content that is not only technical things about my engineering student life but such “professional” stuff (lol).
