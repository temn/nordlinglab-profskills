This diary file is written by Jung-Ping_Xu C14111166 in the course Professional skills for engineering the third industrial revolution.

# 2024-9-19 #

* Data can change our thought.
* The news may be very different from data.
* We need to make sure our data is true.
* The figures are strong evidence to convince the world is getting better.

# 2024-9-26 #
3 ways to spot a bad statistic

1. Do you see uncertainty in the data
2. Do you see yourself in the data 
3. How was the data collected

I will most likely live in Taiwan and be a data scientist or math researcher and spend the rest of my life on my normal life with endless work.

Here are 5 future trends on the list

1. AI and Automation: AI will potentially affect job markets and learning AI-related skills or fields will be an obvious trend.

2. Remote Work: The trend of remote work is more flexibility, but also potential challenges in work-life balance.

3. Cybersecurity: As our lives become more digital, cybersecurity becomes more critical. 

4. Sustainability and Green Technologies: Expect a growing focus on environmental sustainability. Learning about green technologies could be advantageous.

5. Quantum Computing: Quantum computers could revolutionize how we process information.