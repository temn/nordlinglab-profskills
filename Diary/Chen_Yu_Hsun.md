## This diary file is written by Chen Yu Hsun (E24131500) in the course Professional skill for engineering the third industrial revolution

## 2024-09-12
* Today, I took some time to reflect on the progress I have made so far, and in doing so, I came to the realization that I still have a significant distance to cover before reaching my ultimate goals. 
This insight was both humbling and motivating, serving as a gentle reminder that the journey toward success is rarely straightforward or easy. It emphasized the importance of maintaining a strong sense 
of determination and a relentless drive to push forward, even when the path seems daunting or the progress feels slow. It reminded me that growth is a continuous process, one that requires patience, dedication, 
and an unwavering commitment to learning and self-improvement. With this understanding, I am encouraged to embrace each challenge as an opportunity to develop further, knowing that every effort I make, no matter
how small it may seem, brings me closer to the person I aspire to become. Thus, I remain steadfast in my resolve to persevere, stay motivated, and keep pushing the boundaries of my potential, recognizing that the journey 
itself is as valuable as the destination.

## 2024-09-17
* This week’s lesson focused on the multifaceted topic of conflict management, offering a deep dive into the dynamics that govern interpersonal disputes. I found the framework contrasting harmony and conflict particularly insightful, 
as it provided a structured and clear lens through which to analyze and understand the complexities of disagreements. The chart that categorized conflict behaviors, ranging from problem-solving strategies to avoidance tactics, was especially 
eye-opening. It illuminated how different approaches can drastically alter the outcomes of conflicts, emphasizing that aiming for mutual satisfaction and collaborative solutions is often the most effective path, while avoidance, on the other hand,
tends to exacerbate tensions and leave issues unresolved.

In addition to conflict management, we delved into the technical realm with an exploration of Git and Bitbucket, tools that closely mirror the functionalities of GitHub. These version control systems are becoming increasingly relevant and indispensable
to my work, as they streamline the process of collaborative coding and project management. Understanding their practical applications has been immensely beneficial, enhancing my ability to manage code repositories efficiently and work cohesively with team
members on software development projects.

Another captivating discussion centered on disruptive technologies, with artificial intelligence (AI) being a prominent example. We explored how AI has the potential to revolutionize various industries, from healthcare to transportation, while simultaneously
posing significant challenges, such as the displacement of workers. This conversation left me contemplating the broader societal impact of AI, including ethical considerations and the need for thoughtful implementation to mitigate its disruptive effects.

Lastly, the lesson touched on Wright’s Law, which offers a nuanced perspective on technological advancement by highlighting the relationship between cumulative production and cost reduction, proving to be more intricate than the well-known Moore’s Law. This was 
complemented by a discussion on Steven Pinker’s optimistic view of global progress. At first, Pinker’s arguments seemed counterintuitive, given the myriad challenges the world faces today. However, his data-driven analysis reshaped my perspective, illustrating that, 
despite setbacks, humanity is making substantial improvements in many areas. This insight reminded me of the importance of balancing realism with optimism, fostering a more nuanced understanding of global trends and progress.## 2024-09-26
* This week’s focus on Sustainable Development Goals (SDGs) was both inspiring and sobering. It’s encouraging to see progress in some areas, but disheartening to realize how far we are from achieving most targets by 2030.

Additionally, I learned how to properly cite sources using the APA format, a crucial skill for academic and professional work. In a related discussion, we analyzed the challenges of seeking truth in the digital age. Key points included the perils of sensationalism and
unchecked social media content. While the idea of filtering misinformation algorithmically is appealing, implementing such a system faces technical and ethical hurdles.

Mona Chalabi’s tips for spotting fake statistics—checking for uncertainty, finding personal relevance in data, and understanding collection methods—stood out as practical tools for critical thinking.
## 2024-10-03
* Unfortunately, a typhoon disrupted plans this week, causing widespread changes to both personal and communal activities. It's strange and humbling to observe how these powerful natural events, which are entirely beyond human control, 
can so abruptly shape and redirect the course of our daily lives. From the halting of public transportation to the closure of schools and businesses, the impact of the typhoon serves as a stark reminder of nature's dominance over our meticulously 
planned routines. These disruptions force us to pause, adapt, and sometimes completely alter our schedules, highlighting our vulnerability and the necessity of resilience in the face of such unpredictable forces
## 2024-10-10
* We celebrated Taiwan’s National Day with a well-deserved break, taking full advantage of the holiday to unwind and immerse ourselves in the vibrant festivities that swept across the nation. The day was filled with patriotic spirit, 
reflected in the elaborate parades that showcased Taiwan's cultural diversity, with performers donning traditional attire and marching bands filling the air with lively tunes. Streets were adorned with the national flag, and public spaces 
became gathering spots for families, friends, and communities, all united in a shared sense of pride and celebration.

Throughout the day, various events and performances highlighted Taiwan’s rich history and achievements, from the early struggles for independence to the modern advancements that have positioned the country as a leader in technology and innovation. 
Fireworks lit up the night sky, providing a dazzling spectacle that capped off the celebrations, leaving an indelible mark on the hearts of all who witnessed it.

The break from routine offered more than just rest; it served as an opportunity for reflection on the nation’s journey and the collective efforts that have shaped its identity. It was also a time to reconnect with loved ones, partake in traditional 
festivities, and savor local delicacies that are integral to the celebration. For many, it was a moment to recharge, appreciate the freedoms and progress achieved over the years, and look forward to the future with hope and determination.
## 2024-10-17
Today’s lecture on monetary systems was a revelation, offering a comprehensive understanding of the intricate mechanisms that underpin modern economies. Key insights included the concept that loans create money, shedding light on how banks play a pivotal 
role in the economy by generating money through the lending process. This process directly influences interest rates and, subsequently, economic growth, as responsible lending can promote stability and sustainable development, while excessive or unproductive 
credit often leads to economic imbalances and financial crises.

The discussion also delved into the functions of central banks and their ability to print money. While this practice can be perilous when overdone, strategic and controlled monetary expansion can serve as a powerful tool to stimulate economic growth, 
particularly during periods of recession or stagnation. The nuanced balance central banks must maintain to avoid inflationary pressures while fostering economic vitality highlighted the complexity of monetary policy.

Another significant topic was deleveraging, the process of reducing debt levels, which often becomes a critical focus following financial crises. This adjustment period can have profound effects on economic stability, as it requires a delicate balance between 
reducing debt burdens and maintaining sufficient economic activity to prevent further downturns.

These concepts collectively illuminated the profound impact of monetary policies on shaping national and global economies. The lecture underscored the importance of financial literacy, emphasizing that a solid grasp of these principles is crucial for understanding 
the broader implications of economic decisions and policies. It reinforced the necessity for individuals and policymakers alike to comprehend these foundational aspects to navigate the complexities of global financial systems effectively
## 2024-10-24
The discussion on extremism provided a nuanced perspective. Dissatisfaction with society often fuels radicalization, with marginalized groups unjustly blamed. Combating extremism requires empathy, understanding, and humanizing "the other."

We also explored the nature of scientific theories, emphasizing that they are evidence-based frameworks, not absolute truths. Regular physical activity was highlighted as vital for cognitive health—a reminder to maintain balance amidst academic pressures.

## 2024-10-31
Another typhoon struck, making it feel surreal to witness such events so late in the year.

## 2024-11-07
This week’s lecture focused on depression and mental health. Key takeaways included:

Depression is not a defect; it’s a different way of functioning, often rooted in physical causes.
Empathy is crucial when supporting those affected. Avoid trying to “fix” them and instead, set boundaries while offering regular check-ins.
Suicide mentions must always be taken seriously, with professional help sought immediately.
This discussion emphasized the importance of mental health awareness and compassionate communication.

## 2024-11-14
A debate on nuclear power shed light on its classification as green energy. Despite risks like those highlighted by the Fukushima disaster, nuclear energy offers a cleaner alternative to coal, which significantly contributes to Taiwan’s electricity but also its pollution. Misguided fear often hinders progress,
but proper safety measures could unlock nuclear power’s potential as a sustainable solution.

I also learned valuable lessons about self-awareness, non-work relationships, and the importance of supplementing formal education with external resources. These insights will guide both my academic journey and personal growth.

## 2024-11-21
This week’s task on misinformation highlighted its subjective nature, shaped by prevailing ideologies and historical contexts. Examples like Galileo’s heliocentrism and Semmelweis’s handwashing theory demonstrated how groundbreaking truths were once dismissed as misinformation.

Modern platforms face a paradox: overly strict moderation can stifle free speech, while inaction enables harmful content. Solutions include shadow banning, flagging content with disclaimers, and promoting digital literacy. Transparency in algorithms and user-driven context also offer promising ways to balance freedom of expression with accountability.

Finally, I reflected on the environmental impact of electric cars. Their “emission-free” label depends on the energy sources powering them, emphasizing the need for clean energy infrastructure. Governments must support industries in transitioning to greener practices, rather than imposing regulations without offering resources.

## 2024-11-28
The professor conducted a small experiment today to assess how addicted we are to our smartphones. This exercise brought to light the pervasive influence of digital devices on our daily lives, prompting reflections on the balance between technology use and personal well-being. The class focused primarily on the presentation of group projects, where I heard
a variety of wonderful ideas, sparking excitement for their potential implementation. It was inspiring to see the creativity and thoughtfulness my peers brought to their work.

Additionally, today's lecture touched on the concept of the "Dark Ages," drawing parallels to potential future crises if proactive measures are not taken to address current global challenges. This historical context served as a cautionary tale, reinforcing the urgency of thinking critically and collaborating on solutions to prevent a regression into societal 
and environmental turmoil.

## 2024-12-05
We listened to group presentations on news analysis, which revealed that a country's press freedom score does not necessarily correlate with the positivity or negativity of its media sentiment. This finding was intriguing, suggesting that other factors, such as cultural norms and political climates, play significant roles in shaping media narratives.

We also engaged in a brief 15-minute group exercise to discuss a news headline and present our analysis to the class. The main topic of the day was the concept of planetary boundaries, focusing on the looming crises the world faces if immediate actions are not taken to mitigate environmental damage. This discussion was a call to think critically about our 
collective responsibility and to act decisively to address global issues, emphasizing that inaction could lead to dire consequences reminiscent of a "dark age.
## 2024-12-12
Today, we conducted a survey on individual actions that could reduce CO2 emissions, with the data indicating that having one less child is the most impactful among various options. This sparked a thought-provoking discussion on the ethical and societal implications of such measures.

Following this, we heard presentations from two groups about their final projects. The session included a debate and discussion, where we posed questions to each group about their proposed actions and later participated in a survey to determine which foundation we believed was most important. The class concluded with a thought-provoking YouTube video on the 
Third Industrial Revolution, which explored the transformative potential of emerging technologies and sustainable practices in shaping a future less reliant on fossil fuels.
## 2024-12-19
Today was filled with diverse activities and discussions. We explored various topics, deepening our understanding of both theoretical concepts and practical applications. This multifaceted approach to learning keeps the classroom dynamic and engaging, fostering a well-rounded educational experience.
## 2024-12-26
Today was a day of reflection and preparation, setting the stage for the tasks ahead. Although it was a day off from structured learning, I mentally geared up for the upcoming academic challenges.
## 2024-01-01
The first day of the new year was successful in a personal sense, albeit unproductive academically. I woke up late and spent the day playing games with friends, focusing on relaxation and enjoyment after a busy period of study.
## 2024-01-10
Marxist Version: Briam Amarilla
Briam Amarilla lived a life full of passion, embracing every aspect of existence with an open heart and mind. He discovered profound spiritual truths and shared the goodness within him to make the world a better place. His actions had a lasting impact on countless lives.

Through his art—whether in songs, books, or heartfelt journal entries—he captured the raw spectrum of human emotion: joy, pain, love, trauma, philosophical musings, and everything in between. His work deeply resonated with those facing similar struggles, offering them solace, inspiration, or simply the comfort of knowing they weren’t alone.
His creations inspired people to grow, heal, and find greater meaning in their lives.

The world, even if only by a small measure, is brighter because he existed.

People with Similar Achievements
Bill Viola (1951–2024)
Bill Viola, a pioneering video artist, passed away at 73 due to complications from Alzheimer's disease. His work was heavily influenced by a near-drowning experience at age six, which he described as a transcendent and beautiful moment.

He strove to capture this essence in his art, focusing on fundamental human experiences such as birth, death, and consciousness. His notable works include Nantes Triptych, depicting birth, suspension in water, and death, and The Messenger, which faced censorship at Durham Cathedral.
Viola's career spanned significant exhibitions and retrospectives, including at Tate Modern and the Venice Biennale. His work often merged technological innovation with profound spiritual exploration, integrating elements of Christian iconography and Zen Buddhist philosophy. Despite mixed critical reception,
Viola's impact on contemporary art and video installations remains profound.

What Enabled Him to Achieve This
Bill Viola’s success came from turning personal experiences, like his near-drowning as a child, into universal art exploring life, death, and spirituality. He embraced emerging video technology, combined it with deep philosophical influences like Zen Buddhism, and maintained meticulous dedication to his craft.
His wife and collaborator, Kira Perov, ensured his vision reached global audiences, while his resilience and focus on universal human themes cemented his legacy as a pioneering video artist.