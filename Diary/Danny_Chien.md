This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Danny Chien E14086062 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #
* It is interesting t join a class which only speak in English.
* Group work must help us learining the source of this lecture.
* I should improve my ablilty about English listening.
* There are presentation every week.

# 2022-09-15 #

* Each presentations will be chanlanged by professor so we should take more care about the sources of each topics.
* I need to focus on class or I have no idea to finish the diary.
* GIT is new to me,I'm glad that I got a chance to learn about it.
* I'm still a little confuse about what should I do after each classes.

# 2022-09-22 #

* In 2032 years,I live in another with my parents.However,I'm still live in Taiwan in the past 9 years so I afford to integrate myself with new socity and culture.
  I work as an engineer about quality controling first and promote to product manager recently because I am really good at communication and expert in machanical ergineering. 
  Althought that,It's still a big challenge for me but I'm glad that my supervisor approve my ability.In nowaday(2032),the world has changed because of several claims.
  The first is human's health caring.After the first operation which was completed only by artificial Intelengence detoned the topic,Artificial Intelligence had been used in a lot of medical technology.
  As the progressing of these technology, average life of humans in developed countries extend to almost 90 years old. However, lots of nurses and doctor also lost their work and caused new problems.
  But thanks for the progressing, both my parents got effective treatment and look great after their retirement.
  Second one is a revolution of food .It’s not news that several kind of insect are edible. Moreover, the cost of breeding insect is more lower than any other meat such as chicken or beef but produce more nutrition.
  With the effort of popularizing edible insect, people nowadays accept this new product gradually. Lots of supermarkets now had divided and area for selling insect.
  I had tried it before. And now it is a now choice of daily meals.
  Third is power supplying. The world’s map has updated because some country finally sink.It’s hard to imagine this when I was only a student.
  Because of this issue, lots of country accelerated to developing green power. Nuclear looks promising, the world cost a lot in developing related technology.
  Now the percentage of nuclear power generation rise rapidly all over the world.
  Forth is popularity of electric cars. They now can consist driving double of distance 10 years before and apply with quick charging which make electric vehicles replaces the hybrid ones exactly.
  Now in a lot of countries, a legal parking lot needs to equipped with charging pile overs 70% of parking grid.
  Last but not least, working from home become the main approach for working. This give us a lot of time to pay with family but unfamiliar with co-workers.
  So my company hold a banquet monthly to glue the team together and I love it so much.
  There are a lot of changing from 2022 and 2032.It's must same on the next ten years.So I must keeping learning to face the unknowing future.

* The news I read from Ground News was ""Israeli forces kill Palestinian militant in West Bank"".
* All of the news sources has the same views that only cited facts about the events that happened.
* 70 people have been killed in the West Bank. 
* 30 Palestinian prisoners in israel refused to eat to protest.
* The news sources that have a mixed factuality cited that 2022 is the deadliest year in the occupied territory since 2016.

# 2022-09-29 #

* I can't acteally focus on the topics today because of a slight headache.
* My new team will communicate with gmail chat which is new to me.
* I must review the lectures today after I finishing the application task of graduate school.

# 2022-10-06 #

* Econimucs is too difficult to me for understanding everything in about this topics.
* However,issues about extrenism successfully got my attention.
* To find out some fiction stories which I believe is a hard task because I actually don't believe religious things.
* I got plenty of homework this week,bless me. :'(
* I can't find the connection of each topics every week.They're looked be random.

# 2022-10-13 #

* Excwecising is good for people's brain is not a news to me.
* Had some excercise during the class wasn't that bad.
* No class on next thursday ,what a good news ~~
* I finish the task mentioned last week,cheers :D
* The task this week is ambiguious,luckly we had one more week to prepare it.
* I start reviewing the knowledge about Automatic Control for the oral quiz of graduate school on 10/29.

# 2022-10-27 #

* My interview will be held on Saturaday, I was too nervous to focus on the class
* I apologize for this, but I really have no comment on the llectuer this week

# 2022-11-03 #

* I had a friend in jounior high school and he was suffered from depression.
* After realizing he had depression, I kept myself away from him to avoid something annoy,which let me fell very guilt.
* However,today class offered that we should take care of ourselves made me much better about this.
* The courase project is such complicate and I hope we can finish it on time.
* There are less oppurtunities to leran professional ability in university.
* In my opnion, the key points of one's professional ability is communication and networking.

# 2022-11-10 #

* Damn,I for got finish this on time. :'(
* I really hate the video presentation, it's always make me boring and tend to sleep.
* I will know weather I get the admission of NTU EE.Uhh...so nervous.
* Cause we had less communicatino on topics this week,I can't tell my opinion yet.

# 2022-11-24 #

* Uhh,I forgot finishing dairy last week, this is a really busy semester.
* I need to go to Taipei to visit some professors.
* Lots of midtern are coming.Bless me ...

# 2022-12-01 #

* I forgot tell the result of experiment last week.I look for my phone nearly 5 times.
* I skipped the course this week for visiting some professors.

# 2022-12-15 #

* I discovered that I often forgot finish my diary on time.
* A.Today mest be a sucessful day
* B.Cause I finish several task today and finally finish all course about graduate school.
* C.Well, I will go somewhere different from my place where can let focus on studying.

# 2022-12-22 #

* 12-22(Thr)
  A.Successful and productive.
  B.I completed my task from other lesson.
  C.Keep this condition.
* 12-23(Fri)
  A.Unsuccessful and unproductive.
  B.I slept during the class.
  C.Sleeping on time.
* 12-24(Sat)
  A.Successful and productive.
  B.I finished the report of other class and start preparing for live presentation.
  C.Keep this condition.
* 12-25(Sun)
  A.Successful and productive.
  B.I practiced live presentation lots of time to ensure every detail.
  C.Sleep on time or I must miss the tomorrow class(Which I prepared presentation for)
* 12-26(Mon)
  A.Successful and productive.
  B.Teacher commend our presentation because of focusing on the novel technology(Transformer in deep learning).
    Although we didn't really understand this field we still got a good point.
  C.Focus on the remaining course.
* 12-27(Tus)
  A.Unsuccessful and unproductive.
  B.I sleep all over afternoon which mean I just wasted severals hours.
  C.Compensating the work I should finsih.
* 12-28(Wen)
  A.Successful and productive.
  B.I have no class today so I finish the work yesterday and today's one.
  C.Keep this condition.
  
# 2022-12-29 #

* Acteally,this is the last lesson I have to prepare for.
* I had some task from other lesson but I finished them faster than imagine.
* I hope everything in the world can be better than 2022.
* Wish professor a happy new year ~~

# 2023-01-05 #

* Wow,I got a good point in the final exam , I hope this mean I can pass this lesson ~
* I'll like to know that students from which country is better at memorizing.(I'm the one who pass the exam by memorizing.)
* Having a class on Saturday is really depressing.
* I feel a little blue because I will graduate from colledge after 6 months.