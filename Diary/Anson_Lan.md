This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

2019-02-20
The first lecture was great.
I am really inspired and puzzled by the exponential growth.
I don't think exponential growth applies to food production.
I don't think exponential growth applies to food production.
I think everyone should follow the Markdown Syntax and the international standard for dates and time - ISO 8601.
2019-02-27
The second lecture was a little borring because I had seen Steven Pinker's TED talk.
Why are houses becoming more expensive when all technical products are becoming cheaper?
I am really inspired and puzzled by the exponential growth in data and narrow AI.
The Markdown Cheatsheet is helpful.
Now I understood that I should write the diary entry for all weeks in this same file.

2023-10-19
there was a joke goes:"in asian parents' eye, there're only three career, Doctor, Lawyer, or Failure."
althought everyone knows that it was just a joke, but this kind of culture still more or less affect my ways of thinking.
i tend to choose a career that are more promising to earn more money instead of exploring what i really want.

2023-10-26
In the not-so-distant future, our lives will be seamlessly intertwined with a tapestry of technological marvels that redefine the very fabric of our existence. One of the most notable shifts will be the transformation of our transportation landscape. The era of personal car ownership will be relegated to the annals of history, replaced by a utopia of efficient, shared mobility solutions.

Picture a world where the relentless traffic snarls and parking nightmares of today are mere echoes of the past. The rise of autonomous vehicles and advanced ride-sharing systems will render the idea of owning a car obsolete for many. Why invest in a personal vehicle when a sleek, AI-driven taxi can effortlessly navigate the urban labyrinth for you? Commutes become moments of reprieve, opportunities to immerse oneself in a good book, catch up on work, or simply savor the passing scenery.

This shift will not only revolutionize our daily travels but also usher in a new era of environmental consciousness. With a drastic reduction in the number of cars on the road, cities will breathe easier, and the specter of pollution will fade into the background. Imagine cleaner air, quieter streets, and a collective sigh of relief from a planet on the mend.

As our reliance on AI burgeons, the way we acquire knowledge will undergo a metamorphosis. The days of painstakingly memorizing facts and figures will be replaced by a symbiotic relationship with intelligent machines. Why spend hours absorbing information when an AI companion can distill the essentials in seconds? Education will shift its focus from rote learning to cultivating critical thinking, creativity, and emotional intelligence.

The burden of acquiring practical skills will be lifted, allowing individuals to delve into pursuits that spark joy and passion. Whether it's mastering a musical instrument, exploring the arts, or nurturing a newfound interest in philosophy, the luxury of time and mental bandwidth will pave the way for a society enriched by diverse talents and pursuits.

Yet, amidst the dazzling allure of technological progress, questions of ethics and societal impact linger. As AI takes on more roles in our lives, the need for responsible development and ethical considerations becomes paramount. Striking the delicate balance between innovation and safeguarding human values will be an ongoing challenge, requiring a collective commitment to navigating the uncharted waters of a brave new world.

In this future landscape, the emphasis will shift from the relentless pursuit of material success to the cultivation of holistic well-being. Mental health and creativity will be prized commodities, and societal benchmarks will be recalibrated to measure not just economic prosperity but the overall flourishing of individuals and communities.

In conclusion, the future promises a tapestry of interconnected possibilities where the mundane gives way to the extraordinary. As we relinquish the wheel to autonomous vehicles and let AI shoulder the burden of information overload, our lives will be liberated to explore the boundless realms of creativity and mental well-being. It's a future where the journey is as rewarding as the destination, and the human spirit soars to new heights, propelled by the winds of innovation and the ever-expanding horizons of the mind.

2023-11-02
Extremism thrives due to a mix of social, economic, and psychological factors. It often provides identity and belonging to marginalized individuals, capitalizes on political instability, and exploits fear and insecurity. Propaganda, online echo chambers, and a lack of critical thinking also contribute. Addressing extremism requires tackling root causes like socioeconomic disparities, promoting inclusivity, and countering online radicalization.


2023-11-09
today i learned some instruction of handling depression, one thing i learn the most is that we should actually make sure we-self are fine before reach out to help depress people, as i usually felt guilty rejecting to hear their story.
absorbing too much negative emotion will make us sick too.

2023-11-16
it was quite hard for me to understand some law terminology in chinese, not to mention in english. But in the video, there's a cool idea that i really like, that is, the identity given by our goverment isn't really us, it's only the legal friction of ourself. 
so when the authority asking your name, id, lisence or anything that shows your legal identity for no good reason, you can actually decline.
I wonder if i'll do that tho, i felt like i'm always a person that avoid conflict or argument, so i might just give them my ID so that i can leave without encontering futher troubles.

2023-11-23
I think im alwasys a free oriented person, i'll always choose free rather than social activity, but sometimes it still feels lonely and disconected for me, i think being socialtive one a month is good for me, but that's not the frequency that you'll be connected with ohter people, after a month they almost forget that you ever exist.

2023-12-07
The course project is kinda hard for us to came out of an idea, how could we make materials more sustainable just buy ourself? so we end up choosing to make a "Action" more sustainable. this'll make more sense to us.

2023-12-21
Thur.
with nothing to do but studying for my final exam, i felt unmotivated and lazy, but i end up showing up at library and trying to push myself a little bit, the productivity wasn't the best, but at least not sero.
i'll take this as a micro success for me.

2023-12-22
Fri.
Friday is always been my day off of the week, so after finish the class in the morning, i just lay down and start enjoy my friday evening all till friday night, i went to christmas market just for fun, althought final is approaching, i still think a day off is nassary and i'm confident to go back on track tomorrow.
produtivity is sero, but mentally it's a successful rest day.

2023-12-23
Sat.
Slept still almost noon today, after a cup of coffee i showed up in the library and study still it was close. i felt so burnt out. and things does goes so well as i expect, i fail to catch up the pace that i set for myself, hopefully i can still make it before final.
productivity high.
successful wise, not so much.

2023-12-24
Sun.
still slept till noon, but i went to library quite late today, since i just left lazy, i don't know if there's because i study too much yesterday.
productiveity medium.
sucessful medium.

2023-12-25
Mon.
Had a final this morning, i was doing okay, nice mood to go through a busy day, but i got quite sleepy in the after noon.
productivity high.
successful yes.

2023-12-26
Tue.
i forgot what i do today

2023-12-27
Wed.
Had another final exam today, after that i take a big nap.
productivity medium.
successful midium.

