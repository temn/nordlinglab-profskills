This diary file is written by YunShiou Chiou D84106035 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* This was the first time that I added in a course given by a foreign teacher. 
* I was confused about how to use git and bitbucket because it was my first time to use them.
* I was considering to drop the class because it was taught in English but what professor said in the class encouraged me to challenge myself.
* Being objective is not to treat everyone equally but to give everyone the equal chance to be listened.
* Stick to the information sources that you know or those are reliable.

# 2021-10-07 #

* "If you can't buy an apartment or a house, it is the fault of financial system, not yours."
* Bank actually borrow money from people(the deposit), and buy people's bond(the loan).
* When we take a loan, the bank print more money.
* If the amount of money is much more than the goods' value, it will cause financial crisis.
* We can print money by taking a loan!

# 2021-10-14 #

* People will be afraid of things they don't familiar with .
* We belong to a lot of places at the same time ,like Taiwan,Asia,and the earth .
* Embrace the differece between people and look at the similarties we share .
* See what are truly important to you and cherish them .
* There is no "liquid country" and"solid country" .

# 2021-10-21 #

* Sometimes, when  we change the way we solve problems, we can come up with new solution.
* Keep a habit of exercising every week, help us live longer and healthier.
* "Health care" is better than "Sick care".
* But change the relationship between doctor and patient must cost a lot.
* To take control of my performance, I have to take control of my physiology.

# 2021-10-28 #

* Depression is not contagious, when we talk with people live with depression, we just speak normally.
* Sometimes the biggest help we can give is to listen carefully. Being listened make us feel understood.
* Having emotions doesn't mean that we aer not strong enough, it means that we are human. 
* Because of the video I knew that a lot of people regreted since they let go the railing.

# 2021-11-04 #

* After listening to others' experience, I think it is an important thing to reach out our hand to those in need.
* If the working environment is not healthy and it makes you feel uncomfortable, try to speak out,and if communication is not working, just leave the environment as soon as possible.
* It's important to have good relationship with people in your working environment.
* Most of time, we made up the reason why we do something after people asked why.

# 2021-11-11 #

* Farming is a big burden, if all the crops grown were all directly consumed by people, world hunger could end.
* We can actually do thing to make the world a better place.

# 2021-11-25 #

* The method of building trust is to express your care to people.
* Put down smartphone when you are chatting with somebody face to face.

# 2021-12-23 #

* 211223 Thu
        A.Successful and productive
        B.I felt successful because I did a good job in group presentation. A I felt productive today because I finished the homework of statistics.
        C.I will get up earlier tomorrow to do more things even that I don't have any course in the morning.
* 211224 Fri
        A.Unsuccessful and productive
        B.I felt unsuccessful because I didn't carry out my promise yesterday, I didn't get up until 11:00. I felt productive because I finished half of my calculus homework this afternoon.
        C.Get up earlier to do more things.
* 211225 Sat
        A.Succesful and unproductive
        B.I felt succesful because I got up earlier today. But I was attracted by a novel that I want to read for a long time so I fail to do anything, and I went to T.S. Dream Mall to watch the christmas tree.
        C.Do things other than reading novel.
* 211226 Sun    
        A.Unsccesful and productive
        B.I felt unsuccessful because I still spent a lot of time reading the novel and I will not do anything until the last moment. I felt productive today because I finished my preparation of the English oral exam and the rest of my calculus homework, but it is just because today is the last day I can do these things.
        C.Don't put everything to myself tomorrow.
* 211227 Mon
        A.Successful and unproductive
        B.I felt successful because I did a great job in the English final oral exam. I felt unproductive because I slept from 10a.m. to 3p.m. and I participated the activity held by my department without studying anything.
        C.Study for my psychology final exam.
* 211228 Tue
        A.Unsuccessful and unproductive
        B.I felt unsuccessful today because I was late for the training in volleyball on our team. I felt unproductive because I read the novel all day and didn't prepare for the final exam.
        C.Do something productive tomorrow.
* 211229 Wed
        A.Successful and productive
        B.I felt successful today because I got a lot of commend on the volleyball court. I felt productive because I am doing the jobs that due by tomorrow.
        C.Don't put off.:(
* five rules for success
* Have a regular schedule.
* make a to do list
* Don't spend too much time in something not important nor urgent.
* Exercise when you can't be focused.
* Only do one thing at one time.
 
# 2021-12-30 #

* If we replace politicians with randomly selected people, we can implement true democracy, which means that we have a government represent the society.
* Though our democracy now is not a perfect answer to solve the problem"how people live together", we can change it slowly, and make it better.

# 2022-01-06 #

* To make things go viral, we have connect things with the people who see it. Who I am, why I am here and where I am going.
* We should let the nouns become verbs. Instead of telling people the impressive words like "respect","honesty", we should tell them how to practice them in life. If we don't, these words would be just slogans.
* Regardless you are famous or not, helping others is the most common reason why you are remembered on your obituary.
* When you see people, don't seperate them into women, men, white or color, you have to see a humanbeing first. By caring everybody in our society, we can make the world better.

# course objective #

* Theme 1 - “A world of facts, challenges, and ideas”
* 1.Know the current world demographics
* 2.Ability to use demographics to explain engineering needs
* 3.Understand planetary boundaries and the current state
* 4.Understand how the current economic system fails to distribute resources
* 5.Be familiar with future visions and their implications, such as artificial intelligence

* Theme 2 - “Personal health, happiness, and society”
* 1.Understand how data and engineering enable a healthier life
* 2.Know that social relationships gives a 50% increased likelihood of survival
* 3.Be familiar with depression and mental health issues
* 4.Know the optimal algorithm for finding a partner for life


* Theme 3 - “Professional skills to success”
* 1.Develop a custom of questioning claims to avoid “fake news”
* 2.Be able to do basic analysis and interpretation of time series data
* 3.Experience of collaborative and problem-based learning
* 4.Understand that professional success depends on social skills
* 5.Know that the culture of the workplace affect performance

# suggestion for how to help me learn the knowledge/skill required to fulfil the objective #

* 1-1.Know the current world demographics : I think the best way to help me learn the world demographics is to learn the history. Knowing when did the area begin to have a industrial revolution and modernization or if the area was colonized helps me have a concrete understanding of the world demographics.
* 1-2.Ability to use demographics to explain engineering needs : By knowing the thread of thought, we can come up with the problems and needs of the area might meet with experience and speclation.
* 1-3.Understand planetary boundaries and the current state : We have to understand the environment on the Earth is rare and fragile. In the universe we can observe now, the Earth is the only planet allow creatures to live on. So if we change our environment at will, it might lead to a catastrophe that the Earth is not suitable for living anymore.
* 1-4.Understand how the current economic system fails to distribute resources : The key to understand why the current economic system fails to distribute resources is to know what would the bank do when people loan from bank.
* 1-5.Be familiar with future visions and their implications, such as artificial intelligence : If we Know how artificial intelligence learn, we will be able to know what kind of job might disapeare in near future and have a crystal visions to future and its implication.

* 2-1.Understand how data and engineering enable a healthier life : I think we have to know how does a healthy life look like, in this case we will be able to know what data and engineering can help to improve people's health.
* 2-2.Know that social relationships gives a 50% increased likelihood of survival : I think if we have the chance to experience a life without social relationship or even just imagine, can help us understand this knowledge.
* 2-3.Be familiar with depression and mental health issues : I think listen to people with depression or mental diseases can help us know more about it. When doing this, we should know that they are people just like us, don't take them as another species.
* 2-4.Know the optimal algorithm for finding a partner for life : Reject the first 37% you date and settle down with the next person you meet that is better than people you have dated with.

* 3-1.Develop a custom of questioning claims to avoid “fake news” : I think the best method to let me question anything is to give some examples. Some news that sounds really real but actually it's not and some news seems like a fake news but it's real.
* 3-3.Experience of collaborative and problem-based learning : I think the experience this semaster in this course can teach me this skill.
