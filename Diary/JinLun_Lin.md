This diary file is written by JinLun_Lin in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* I have not joined the course today
# 2024-09-19 #

* This day was productive
* I also had a group discussion for an upcoming presentation
* Overall it was a pretty productive and successful day
* Since my English is not very good, it was a bit difficult to take this class, but I believe it will improve in the future.
# 2024-09-26 #
* In the next ten years, I have a few goals that I want to achieve one by one. I am currently pursuing a bachelor’s degree in 
  electrical engineering, and I am a freshman. To be honest, coming to university, an open and free environment, makes me feel 
  quite scared. From selecting courses, joining clubs and teams, to submitting a pile of enrollment documents, I have to handle 
  everything by myself. Although there are many people around me to ask for advice, they can't give me accurate answers. In
  university, whether it's choosing a major, selecting courses, or planning your university life, there’s no standard 
  answer—there’s no absolute right or wrong. So in the end, the decision is still in my hands.
  After being in university for a few weeks, I’ve become more familiar with it and no longer resist university life as much. 
  Life is gradually stabilizing, and I don't feel the urge to go home as often. Now, I’ve started to think about what I want 
  to do after graduation and what I will be doing ten years from now. I have listed the following expectations for myself after 
  graduation and in ten years:

* 1. I want to stay in Taiwan and work as an excellent engineer, preferably in a big company like TSMC or MediaTek. I want to
  stay in Taiwan because I’m afraid of flying, get homesick easily, and my English isn’t very good.  
* 2. In the next ten years, I want to continue honing my skills and expanding my knowledge. Referring to the first point, having
  a solid technical and knowledge background will make it easier to find a good job. I also plan to read more books and stay 
  informed about global trends so that I won’t be left behind or replaced by AI.
* 3. I want to make some contributions to society, such as participating in beach cleanups, conserving energy, volunteering, 
  or donating to foundations. There’s no specific way it has to be done, but I want to make sure I achieve this and keep doing 
  it throughout my life.
* 4. I need to take care of my health, exercise more, drink water, and eat less fried food. After all, without good health, I won’t 
  be able to do anything else.
* 5. I want to have a girlfriend.
* North Korean defector in South stole bus in bid to return home, media reports say
* left:Excessively pointing out that defectors’ living or environment in North Korea is extremely harsh.
* center:Without any political stance, truthfully state the background, process and impact of the entire incident.
* right:Avoid many controversial words.
# 2024-10-03 #
* Holidays during typhoon days
# 2024-10-10 #
* National Day holiday 
# 2024-10-17 #
* In class, I watched some videos that gave me new insights into when banks print money and the sources of money's value
, furthering my understanding of "money." Money cannot be printed at will by the government just because it needs cash; 
various aspects need to be considered, such as market mechanisms, inflation, and investment.
# 2024-10-24 #
* Money: Money is a man-made concept that we all agree has value and use to exchange goods and services. Last week,
I made a payment to purchase study materials, which has helped me better prepare for exams. Although it's just fictional
paper currency, it allows me to obtain the resources I need to make progress.

* Nation: A nation is a concept that is commonly recognized, serving as a way for us to connect with others. Last week,
I watched a video about the future of Taiwan, which prompted me to reflect more deeply on how I, as a Taiwanese person,
can contribute to this country.

* Occupation: An occupation is part of the identity we give ourselves and is a central focus of our lives. Last week
, I stayed up late for a class assignment because I aspire to become an engineer in the future, and this motivates me 
to keep working hard to learn.
* Three rules for how to live healthy
* Rule 1:
     Maintain a good sleep schedule every day. Research shows that sufficient sleep not only strengthens the immune
     system but also improves memory and learning abilities. Aim for 7-9 hours of quality sleep each night.
* Rule 2:
      Consume enough dietary fiber daily. Getting enough fiber promotes gut health and reduces the risk of cardiovascular
      diseases. Sources of fiber include whole grains, fruits, and vegetables.
* Rule 3: 
      Limit daily sugar intake. High-sugar diets increase the risk of obesity, diabetes, and cardiovascular diseases. 
      It’s recommended to reduce sugary drinks and choose no-sugar or low-sugar foods.
# 2024-11-07 #
*Today’s lesson made me realize the importance of mental health. To be honest, I am a person who gets depressed very easily. 
The reason is that I just entered college and I still didn't feel comfortable with it. I feel much better after watching 
today's video about depression. I think I am motivated to face tomorrow’s challenges!!
*The video about the Golden Gate Bridge made me feel very heavy, especially when the rescuers told him so much but still chose to
jump. This is really a big blow to the rescuers. I hope this won't happen again in the future.
# 2024-11-14 #
*Strawman: The Nature of the Cage is a thought-provoking documentary that explores the interplay between legal systems, financial
structures, and personal freedom. By dissecting the concept of the "strawman," the film unveils the invisible constraints in our 
daily lives, such as debt, legal identities, and institutionalized control. It serves not only as a critique of societal norms but 
also as a wake-up call, encouraging viewers to question established rules and seek authentic freedom.
*One of the most impactful aspects of the film is its analysis of the distinction between "lawful" and "legal." It compels viewers 
to reflect on whether the rules we follow are designed to protect us or merely to restrict us. The documentary highlights how many 
people remain unaware of these "cages" and lack the courage or means to challenge them.
*After watching the film, I realized the importance of understanding the systems and laws that govern our lives. It prompts a reexamination 
of how to maintain individuality and freedom in a highly institutionalized world. This documentary is not just an exposé but also a call to 
action.
*I highly recommend this film to anyone curious about the workings of modern society and eager to delve deeper into the rules that shape our
lives. It will make you reflect on whether you, too, are confined within a "cage" and how to break free from these invisible limitations.
# 2024-11-21 #
*Today’s course mentioned the impact of big data and algorithms on us. The results of over-trusting the data, as well as the bias of the 
producers behind big data and algorithms, will greatly affect the accuracy of judgment.
# 2024-11-28 #
In today's class, the professor collected our phones, which I think was a great idea. I'm often distracted by my phone, so this helped me
focus on the lecture. However, it's a bit unfortunate that my poor English made it difficult to understand much of the content, especially
some of the technical terms. Without my phone to look things up, it was quite challenging. Still, I believe that as long as I work hard 
and persevere, I will make progress.
# 2024-12-05 #
Regarding the final report, the professor clearly stated that anyone who slacks off or avoids responsibility will fail the course. 
In our group, there was initially a member who was very dedicated to working on the report. Unfortunately, the rest of us (myself included)
, perhaps due to shyness or failing to recognize the seriousness of the situation, didn’t offer much support or engagement. It wasn’t 
until he decided to leave the group that we fully understood the gravity of the situation. Fortunately, the remaining members stepped
up and put significant effort into the report afterward. Even so, I still feel deeply sorry for that member who chose to leave.
# 2024-12-12 #
Climate change has profound impacts on the global environment and human life. First, the frequency and intensity of extreme weather 
events, such as typhoons, heavy rains, and droughts, are increasing, posing threats to agriculture, infrastructure, and human safety.
Second, rising global temperatures are causing glacier melt and sea level rise, endangering coastal ecosystems and residents' lives.
Additionally, biodiversity is gradually decreasing, with many species facing extinction due to habitat loss. At the same time, 
climate change may exacerbate food crises and health issues, increasing the risk of disease transmission. Addressing climate 
change requires global cooperation, promoting carbon reduction policies and sustainable development.
# 2024-12-19 #
*This week topic is "how to succeed"
*Success is a state of achieving goals and feeling fulfilled, and it has different meanings for everyone. For some, success may
mean having a stable job, financial freedom, or a respected status; for others, it could be pursuing their passions, maintaining
harmonious relationships, or achieving personal growth and balance. Success is not only about external accomplishments but also
includes inner satisfaction and a sense of fulfillment. It requires effort, overcoming challenges, and continuous learning and 
improvement. The key to success lies in identifying what you truly want and striving for it wholeheartedly, with no regrets.

*Success requires clear goals and strong determination. First, set specific and achievable goals, and be clear about what 
you want to accomplish. Next, develop good habits, such as effective time management, self-discipline, and continuous learning,
which can increase the efficiency of achieving goals. At the same time, be resilient when facing difficulties, never give up easily,
learn from setbacks, and adjust your strategy to keep moving forward. Additionally, seek guidance and support from others, and learn
to collaborate with a team to achieve your goals more quickly. Most importantly, maintain a positive mindset, believe in your 
abilities, and keep working toward your goals. Success comes from persistence and the accumulation of actions.
# 2024-12-20 #
* Not successful, haven't started yet. 

* Due to staying up late last night, I felt very tired today. Part of it was because I was studying calculus, and another part 
was from scrolling on my phone, which led to a less-than-ideal performance in calculus today. 

* So, my task for myself is to go to bed earlier, so that I can have the energy to continue putting in effort the next day.
# 2024-12-21 #
* Small success, some results.

* I went to bed at 12:30 AM yesterday, and I felt much more energetic the next day. However, a new problem arose—I couldn’t focus
on my books and kept wasting time, not wanting to put in the effort. 

* My task for myself is to at least study for 1.5 to 2 hours.
# 2024-12-22 #
* Small success, good results.

* I told myself I’d study for just a short time, and by maintaining that mindset, I was able to meet my goal. By the way,
I went to bed at 11:30 PM yesterday. However, I felt a bit disappointed today because I didn't get to participate in the
team matches. It must be because I'm still not strong enough, so I need to keep pushing myself harder.

* My task for myself is to play volleyball for at least 30 minutes every day. It’s a challenging goal for me, but 
I’m confident I can do it—after all,  I have already play game every day, I can make time for this too.
# 2024-12-23 #
* Unable to determine success, results unknown.
* It rained today, so I couldn’t play volleyball at all.  

* Task for myself: Recently, my general physics performance has been a bit shaky, and I’m not sure if I might fail the course. So, tomorrow I will dedicate at least two hours to studying.
# 2024-12-24 #
* Success, visible progress.
* I feel like I have a chance to pass now.  

* Task for myself: Wake up early to play badminton, as this shows I’ve completed other tasks and avoided staying up late.
# 2024-12-25 #
* Failure, poor results.  
* Couldn't get up, trapped by the blanket.  
* Task for myself: Try to set daily goals and accomplish them.  
# 2024-12-26 #
* A. successful,productive
* B. Achieved yesterday's goal
* C. Trying to plan more tasks and details to develop a habit of self-discipline
# 2024-12-27 #
* A. little successful,productive
* B. Achieved the set goals, but with limited results. The lazy side of me keeps
urging me to play on my phone, but I’m fighting hard to resist.
* C. I hope to maintain a good routine during the upcoming holidays.
# 2024-12-28 #
* A. unsuccessful,unproductive
* B. Stayed up late gaming with classmates... feels like all the effort from the past few days has gone to waste.
* C. Recently, many of my courses have finished, so I can play volleyball every day. Tomorrow, I plan to try serving a jump float serve
# 2024-12-29 #
* A. successful,productive
* B. Today, I served several jump float serves and felt quite accomplished.
* C. I saw some good training methods from others during practice today, and I want to try them out tomorrow.
# 2024-12-30 #
* A. little successful,productive
* B. The intensity of their training is a bit high for me, so it will take some time to adapt
* C. Continue practicing volleyball every day and play fewer mobile games
# 2024-12-31 #
* A. successful,productive
* B. I can feel the progress, but there's still a long way to go to reach the ideal version of myself.
* C. Today is the last day of 2024, and 2025 is a year full of hope. I hope I can live an exciting and fulfilling life.
# 2024-01-01 #
* A. To be confirmed
* B. 
* C.













