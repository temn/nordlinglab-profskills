This diary file is written by Victor Chen C24080024 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #
* I'm so excited because its the first class.
* I'm confused with the grouping system.
* Hoping to learn new things throughout this class.


# 2022-09-15 #

* The second lecture was great.
* Still trying to understand where can I get the lecture slides in Google drive.
* Hope I can practice my presentation skills in the following lectures.

# 2022-09-22 #

* I feel sad losing the chance the present today because of the time.
* It is really important to think the truth of news.
* My group members are really kind, and we have a good discussion at Physics department.
* Today I learn that it is disrepectful that turn off camera during talking. 

Homework

> _In your Diary for this week, write a 400-600 word summary of what you think your future will look like and why it will be so 10 years from now. Start by defining where you will live and what work you will do. Then describe the five current trends of change that you think will affect you the most and their effect on your life 10 years from now._

A vision I've always had of myself 10 years ahead of now, I will be 30 years old then, is working in some big technical company and live in America or around the world (because I think remote work will still be possible in the future). And find a balance between work and life. For working, I hope I can work in a cool company, such as Google, Facebook. Working on interesting project which can make some good impact for our society with some cool partners. Besides the work, I will have my time to company with my families and travel the world and learn whatever I want to learn, try whatever I want to try.

The five current trends of change that I think will affect me the most is following. First, is the pandemic, if the Covid-19 getting more serious again, or if the new pandemic appears, I will not be able to study abroad, which will decrease the opportunities for living in the America. Secondly, the flourish of the software industry. Nowadays, more and more places need some technical people to make something possible or more convenient. It brings more and more opportunity to find a technical job. Third, the peaceful of the world. Russian-Ukrainian War have lasted more than four months, and it is continuing. It is hard to believe that using violence to solve a problem happened in 21st. If there are more and more war in the future, it will not longer safe to travel the world. Next, the climate change, the area which near the sea may be disappeared because of the rising of the sea level. Which mean the space we can live will be less than past few years. Then it probably affects not only what we live but what we eat. Last but not least, health of mine and my families’. Every part of my life relies on having good health. So, maintaining a good health is important for me. And, if some of my families are in the bad situation, I’ll consider my situation to balance my optimal life and the cherish time with my beloved. 

Time flies. The only way that I’ll be proud of myself and become the person I who I want to be ten years from now is to stop thinking about time and to start focusing on the now. Always stay hungry, stay foolish.

> _Select one news story published on https://ground.news/  and read all the different sources. Summarise the different views in the sources in bullet point format in your diary._

- The news story I chose is [Defeat in Italy sees England suffer Nations League drop](https://ground.news/article/defeat-in-italy-sees-england-suffer-nations-league-drop_9d7ef1)
- There are 30 sources, 7 leaning left, 4 center, and 3 leaning right.
- In the news which is leaning left, more focus on "without a win or even a goal from open play in their past five matches less than two months".
- In the news which is leaning right, more focus on the interview of the player and coach, claiming thier performaces are not bad and they're on the wrong side of the results.
- In the center one, some are just show the data of the match.

# 2022-09-29 #

- Search for the fake news was quite shock for me.
- Today is talking about money.
- Hope the lecture will be better physically.
- I'm looking forward to meet my new groupmates.

# 2022-10-06 #

- I think it works better for taking the course in person
- Most of student who had presented doing quite well.
- I'll have my GRE test next Monday, wish me luck.

> _Describe three fictional stories, e.g. currency, nation, god, marriage, and career,that you believe in and how they have affected your life at some point during the last week. (You must give a tangible example of how the story affected you within the last week.)_

- Seeing an video talking about family. In that video they talk about how great the family of orientation will impact you. This make me think more about how a people react and what's there values.
- Reading a poem of the internet, as one of the saying goes, "New York is 3 hours ahead of California, but it does not make California slow". We all have our own timezone, so instead of thinking why others are so good, focus on yourself. Don't waste it living someone else's life.
- After knowing the economic news in England in course make me alert for not updating global issues.

# 2022-10-13 #

- I learn that "Knowledge is justified true belief" by Plato and that is quiet meaningful.
- The path of knowledge can also apply on the startup, believe something first and try to make some MVP and verify.
- I still a bit confuse about whether we will have the course next week or not.

# 2022-10-27 #

- There are many benefits when it comes to exercise, and fortunately I have exercised regularly.
- The class doesn't give enough time for presentation, and I think the online dicussion doesn't work really well.
- There are some errors in the google form and I hope it can fix for future students.
- I'm looking forward to the World Cup.
- Come on barcelona!

# 2022-11-03 #

- Taking yourself before taking care for others.
- Depression is really an important and serious issue nowadays.
- Hope I can imporve some of my soft skills in this courses, e.g. pubilc speaking, story telling etc.
- How social media influence our recognization is quite astonishing.
- I really respect to two man in the video turn the tragedy to something which can help people.

# 2022-11-10 #

- I have learnt how to search for paper with some logic operator (e.g.AND,OR) this week.
- So many good football player injuryed before World Cup, I think it will be better to held the competition in summer.
- I have my military test this week.

# 2022-11-17 #

- Recently I am finding for intership.
- How Elon Mask deal with the fake news by charging money is quiet thought-provoking.
- I'm ready for the World Cup.

# 2022-11-24 #

- This week we learn about noise limit and the laws in Taiwan and Japan.
- I need to learn to use my cellphone less often.
- Eat less red meat, and eat more chicken or fish.
- How can Argentina lost Saudi Arabia!

# 2022-12-01 #

- I have heard that some of the group don't really commit the work together.
- I'm looking forward to the action of beign vegetarian.
- Switching group every three weeks make me feel so annoying cause we need find teammate and some don't even reply.

# 2022-12-08 #

- Brasil really really enjoy football match in the last 16 game!
- I’ll have a football competition this week, hope I can handle everything well.
- Recently I’m searching for the program for CS MS in the USA.

# 2022-12-15 #

- I'm really suprised that having vegetarian diet could improve our sexual life ! 
- There are a lot of homeworks and projects these days.
- Finding internship is not easy.

* 2022-12-16 (FRI)
	- Unsuccessful and unproductive.
	- I feel so cold today and feel lazy and sleepy.
	- Make some TO-DO list for tomorrow in order to get those urgent and important things done.
* 2022-12-17 (SAT.)
	- Successful and unproductive.
	- Today I sleep untill 11 am and have the birthday meal for my grandmother in the afternoon and have the World Cup at night.
	- Put less time on the entertainment thing but World Cup once every four year and it's almost end
* 2022-12-18 (SUN.)
	- Successful and productive.
	- Finish one of my final project.
	- Energy is not so good due to lack of sleep, so hope I can have more sleep tomorrow.
* 2022-12-19 (MON.)
	- Successful and unproductive.
	- Argentina won the World Cup and I am so happy and make a target of watching World Cup in person at 2026.
	- I hope I could be more productive tomorrow cause many deadline is coming and need to work on.
* 2022-12-20 (TUE.)
	- Successful and productive.
	- Finish web crawler project for one of my elective course.
	- Hope I can spend more time work on those important but not that urgent stuffs, e.g. search for program of my master
* 2022-12-21 (WED.)
	- Successful and unproductive.
	- Finish one of my project's demo but don't really have progress for my other homework.
	- Find some classmate to work on the project together.

# 2022-12-22 #
- I'm so sorry that I foget to wear in formal today.
- I have read "The 7 Habits of Highly Effective People" before and the habit, Begin With the End in Mind, is really affect me.
- Sleeping and relaxing is really important as well.

* 2022-12-23 (FRI)
	- Successful and unproductive.
	- Chat deeply with my friend talking about how to make a choice, and I think that's quite important.
	- Leave some time box for those improtant but not urgent things, e.g. read book, understand more about myself, plan my carreer.
* 2022-12-24 (SAT.)
	- Successful and unproductive.
	- Today I am working on line bot and I don't have direction and there is only few tutorial.
	- This too shall pass.
* 2022-12-25 (SUN.)
	- Successful and productive.
	- Finish line bot project.
	- Hope all the thing doing well tomorrow.
* 2022-12-26 (MON.)
	- Successful and productive.
	- I have one final exam, two meeting, and three final project demos today.
	- I hope I can take a risk tommorrow.
* 2022-12-27 (TUE.)
	- Unsuccessful and unproductive.
	- I went to Taoyuan to have football competition but I'm really sick today.
	- Hope my condition will be better tommorrow.
* 2022-12-28 (WED.)
	- Unsuccessful and unproductive.
	- Still really sick and cannot do anything.
	- Drink more water and have a doctor.

# 2022-12-29 #
- Final course of this semester.
- Time really flies, these days I'm gonna make a year review.
- Gook luck for my final exam week.

# 2022-01-05 #
- Today we have our final exam.
- I inadvertently pressed the wrong button in google form so two of question I know the answer got wrong.
- I wonder some of the question is out of date and why don't update.

# 2022-01-07 #
- Today is the last course of this class.
- Our group has worked on out video for two days and some of us didn't even sleep.
- Hope our video go well, we really put a lot of effort on it.
- I think this is the last time I write this diary lol.
- TA need to fix on some of the score stuff.
- Hope I can get good grade on this course.
