This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by YUN PING LEE E14086541 in the course Professional skills for engineering the third industrial revolution.

# 2022-9-8 #

* The first lecture was great, and I realized that my English skills really need polishing.
* Recently, I've been preparing for TOEIC test. I wish I can pass TOEIC exam with a high score on 9/25.
* I am really inspired and puzzled by the exponential growth, and this is my first time to discuss the homework with foreign classmate.
* Moon Festival is on this Friday. I gathered with my junior high school friends and we had a fantastic BBQ party. 
* I will apply to NCKU for International Student Exchange this semester, I wish I can improve my writing skills through the task of this course.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2022-9-15 #

* This week I spent at least 1 hours per day to practice and boost reading and listening skill.
* I was nervous to give a presentation to my advisor in KLab on this Friday’s meeting.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I learned a lot from the exponential growth topic last week, hoping I can develop valuable skills along the course.
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful, and I looked for more information on other website.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2022-9-22 #

* I believe I will live in Japan as a mechanical engineer after 10 years from now. 
* I’ve lived in Tainan City since I was a student in primary school. 
* My elementary, junior high, and senior high school are all in the east district of Tainan, so my home range is just around NCKU.
* I think it’s time for me to step outside my comfort zone when I become a graduate. 
* I’ve been planning to study abroad to broaden my horizon since I was a freshman. 
* Because I’m fond of Japanese traditional culture and also its subculture, I set myself a goal to be a graduate student at Tokyo Institute of Technology,
* which is the top research-based national university in Japan dedicated to science and engineering.
* Over the past three years, I have devoted myself to improving my Japanese. 
* I'm so glad that I passed the JLPT N1 Exam this July. 
* The next step to take is preparing the documents needed for applying to university and sharpening my English.
* I hope I will achieve my goal in the future.

* The five current trends of change that I think will affect me the most are below: 
- Disease outbreaks (e.g.,COVID-19,monkeypox) 
	- The economic and social disruption caused by the pandemic is devastating.
- International Situation (e.g., Taiwan Strait Crisis, World War3)
	- China may invade Taiwan abruptly, and that is not the thing that we can control.
- Natural disasters (e.g., volcanic eruption, earthquake, tsunami)
	- Due to Japan’s unique geography, the country is prone to a variety of natural problems.
- The increase in the use of consumer electronics may cause eye problems and diseases.
	- I am suffering from myopia, and my eyes might get worse due to too much screen time.
- The competition for talents is getting harsh due to globalization.
	- Many developed economies have difficulties in producing enough talent to deal with a rapidly changing future increasingly driven by innovation and knowledge. 
	- Bringing in talent from outside can provide a quick fix to the human resource gap.
---
* I select the news from [https://ground.news/article/japans-junior-ruling-party-komeito-approves-8th-term-for-leader-the-mainichi]
* the different views in the sources:
* both of the news resources are almost the same, but one of them tells more detail about what Yamaguchi said at the press conference.
* Yamaguchi told a press conference following the convention that the new lineup is aimed at "maintaining the framework of the party, 
* preparing for the unified local elections, and focusing on winning upcoming battles," suggesting the party is currently prioritizing 
* stability over generational change. But Yamaguchi expressed his intent to bring in younger members in the future, saying, 
* "We will mark the 60th anniversary of the party's founding in two years. We will use this period to prepare for passing the baton to the next generation."
* from [https://mainichi.jp/english/articles/20220925/p2g/00m/0na/028000c?utm_source=ground.news&utm_medium=referral]

# 2022-9-29 #

* This week, it’s my turn to deliver our team presentation. 
* In fact, I was afraid of public speaking, let alone doing presentation in English.
* Because my English is not good enough, I spent the whole weekend preparing for the presentation. 
* I searched the internet to check out whether every sentence I wrote is correct.
* Surprisingly, my video presentation was played on the sharing screen and the professor said: “Excellent work!” to my presentation. 
* I could finally breathe a sigh of relief.
* Today professor talked about the financial system, and introduce some interesting facts about deposits and loans.
* It's interesting to learn that whenever the Chinese new year has come, the amount of money the government held increases drastically due to the Chinese envelope custom.
* Today’s lecture PPT showed that NASA has verified that it and all images in the 2012 series of “Blue Marble” images are composites as they cannot get far enough away and have to combine multiple photos. 
* The concept is similar to the fake image in my presentation, Milky Way galaxy. All pictures that depict our galaxy are either artists’ renditions or pictures of other spiral galaxies.

# 2022-10-6 #

* In today’s lecture, I learned about England does not have a president.
* Because it does not have a republican system of government with a presidency. 
* England doesn’t have a head of government. 
* England is part of the United Kingdom, with Scotland, Wales, and Northern Ireland, which is a constitutional monarchy.
* Liz Truss becomes the new UK prime minister.
* The advantage and disadvantages of borrowing money from the central bank or foreign country.
---
* The fictional stories I believe are told by my parents since I was a little kid, and most of them are Taiwanese superstitions. 
* These societal superstitions really affect my daily life.
* Last week, I had a discrete mathematics midterm exam, so I was several days away from a shave. 
* Every time the exam is right around the corner, I will not shave until I finish it.
* There are different versions of the superstition like if students trim their nails during the examination period, they will end up with failing grades for their subjects.
* On the other hand, I don’t eat beef before the exam.
* Because the test God Wenchang Wang, his mean of transportation is cattle.
* He has historically been called upon by scholars and writers who need inspiration or help right before an exam.
* During a meal, it is frowned upon for people to leave their chopsticks sticking up vertically in rice, be it leftover or a new bowl of rice.
* The image of having chopsticks sticking up vertically in rice is similar to the image of having incense sticks being put up when the Chinese are offering meals to their ancestors’ ghosts.
* Therefore, this similarity would come across as bringing bad luck or “unwanted spirits” to the owner of the restaurant.
* Superstitions are intangible things that really affect our daily life.

# 2022-10-13 #

* This is my first time answering the professor’s question in class.
* The question is “How to define knowledge.”
* At that time, I have no idea. My mind went completely blank.
* A few words came to my mind, and they are “Knowledge is power”.
* Today's videos are very interesting and energetic. 
* Explaining how we can be brilliant and toward a better version of ourselves in a scientific way.
* Exercise is truly the key to a healthy life, working out consistently makes our brain release an adequate amount of endorphins
* I tried to build a habit of jogging regularly this semester, but it was hard to maintain it. 
* When the exam is just around the corner, I can't be bothered to exercise.
* After watching the video, I realized how important regular physical activity is.
* Being physically active can improve our brain health, help manage weight, reduce the risk of disease, strengthen bones and muscles, and improve our ability to do everyday activities.

# 2022-10-20 #
* We don't have class this week.

# 2022-10-27 #
* This week was the online class.
* In class, every three groups were merged into a big group.
* At first, we have no idea how to discuss the upcoming presentation, so the google meet was completely silent.
* Fortunately, one of my partners broke that uncomfortable silence and ignite a discussion.
* Besides, my past group members are both in the google meet.
* It is so nice to work with them again.
* Finally, we combined three slides together, and each group chose one presenter who was in charge of presenting the slides their groups had made.
* This is my first time giving a presentation in an online class, and we received applause from the professor.
* This Sunday I have my second TOEIC Listening and Reading Test this semester.
* Last time the listening part was as hard as the simulation test and I was too nervous to focus on what the speaker said. 
* Additionally, my reading speed was not fast enough to answer all the questions in the reading part. 
* This time I still couldn’t finish the test, so I filled all the remaining questions with answer B.
* In the past TOEIC exams, I haven't finished the reading part in a limited time.
* I will keep practicing the TOEIC sample test to improve my English listening and reading skills. 
* Hope I will be able to get a score above 900 in the next exam!

# 2022-11-3 #
* This week we were distributed to new groups and my teammates both speak English fluently.
* I’ve done my best to listen to what they were discussing, but I could only understand about 50%.
* Last week I felt frustrated in my TOEIC reading part, and this week the presentation discussion frustrated me.
* At this point, I deeply realized that my English is not good enough to communicate without google translate.
* Fortunately, one of my group members was also fluent in Chinese, and she led a discussion.
* I'm beginning to worry about my life as an exchange student in Japan next year.
* Although my Japanese speaking skill is better than my English, I still need to improve my English against unexpected needs.
* For me, it will be a very tough task, and it will not be easy to achieve.
* By the way, I was preparing some application documents in order to apply to the exchange program.
* My friend’s English autobiography is an excellent piece of work, so I referred to it.
* We talked about depression these two weeks.
* My solution to help depressed friends is to take on a trip with them.
* Having new experiences is beneficial for improving brain function and boosting our mental health.
* Travel has been linked to stress reduction and can alleviate symptoms of anxiety and depression.
* Mental health is as important as physical health, and they complement each other.
* Travel may positively impact both physical and mental health.

# 2022-11-10 #
* This week was very substantial and joyful.
* Monday, I had a Japanese exam in the afternoon and I started to study for the exam two hours before it. 
* It was nice weather at night, so I jogged a 3K at the park near my home.
* Tuesday, NCKU University-level Recommended Foreign Exchange Student Program Brochure was finally announced. 
* I had been waiting for it for a long time. 
* In the afternoon, I was studying the kinematic equations of the robot with three Omni wheels. 
* In the evening, I watched a total lunar eclipse with my friend on the roof of the ME Building.
* Wednesday, I did a presentation on what I’m going to do in the term project of MECHANICAL DESIGN OF ROBOTICS. 
* In the evening, we discuss the Course project—1st presentation via Google meet for six hours!
* Thursday, I prepared the presentation video the whole morning, and it frustrated me. 
* I was working hard to meet the deadline of upload.
* This Friday is NCKU's anniversary celebration, my friends and I went on a trip to Alishan National Forest Recreation Area, and we had a ball this weekend.
* Sunday, the TOEIC exam result will release at 24:00. Hope I’ll pass the test.

# 2022-11-17 #
* I got 890 points in the TOEIC exam last month. 
* Such a pity! Almost 900.
* Last week I did a presentation.
* Professor said that our slide was not beautiful.
* Next time, we will work on it.

# 2022-11-24 #
* I did the pre-enlistment physical checkup this week.
* It’s so many people that I totally spent 2hr on it.
* The movement control of my three omnidirectional wheels mobile robot was finally done.
* It took me a lot of time to prepare the documents for the NCKU school-level recommended exchange student program this week.
* I was on the fence about exchanging to Osaka University or Tohoku University.
* l spend 9 hours glued to a screen from Friday 7 p.m. to Saturday 4 a.m. in order to write the study program.
* It’s so difficult for me to write in English even using Google Translate.
* I had too much screen time recently.
* It's always going to be a challenge to drag students away from their screens and it's likely that more and more studying will be done online, through a screen.
* I jogged a 2.5K at the park near my home on Saturday evening.

# 2022-12-1 #
* This week I finally upload the application documents for the NCKU school-level recommendation exchange student program.
* I spent over 40hr preparing the documents required.
* Result announcement will be released on 1/6/112 by 4:00 pm.
* Hope I will be one of the positive takers of Osaka University.
* This week is my turn to do the presentation for the small group.
* Because I thought that I had little contribution to the small-group presentations in the last two discussions.
* I volunteered to complete the presentation this Thursday by myself.
* In the beginning, I misunderstood what the professor what us to do, so I search for lots of news in vain.
* I found that writing a diary and googling numerous articles for presentations every week really improved my English writing and presenting skills.
* This week I worked hard on my LabVIEW project.
* I could finally create myRIO project on my own computer.

# 2022-12-8 #
* This week is the course project presentation.
* The part which I was in charge of is interviewing a person who has done the same type of action as ours.
* I interviewed my friend who usually uses the Reverse Vending Machine(RVM). 
* He has been promoting environmentally friendly actions among people around him.
* RVM is an automatic device that allows consumers to feed in used beverage containers for an instant rebate.
* If there is a significant amount of residual liquid inside the container, the container be rejected by RVMs.
* The RVM we used is equipped with the compression function to reduce the volume of beverage containers so as to enhance its handling and storage capacity.
* He taught me how to use the RVM step by step.
* First, I downloaded and registered the App. 
* Next, we inserted bottles, cans, and tea cups into the machine and pressed the “Complete” button. 
* Our points will appear in the App and we can exchange our points for the coupon voucher.
* The whole process was simple and fast.
* “I think that protecting the planet is everyone's responsibility.” he said.
* I can't agree with him more.

# 221215 Thu #
* A. Successful and unproductive
* B. I felt successful because I complete the small group video presentation on time and learned a lot from the speech“The third industrial revolution: a radical new sharing economy”.
* I felt unproductive because I spent too much time on it. 
* I should do the task more efficiently.
* C. Next time, I will prepare the task at NCKU’s library where I can focus my mind on the work.

# 221216 Fri #
* A. Successful and productive
* B. I felt successful and productive because I had a fulfilling day because I got 93 points on the exam of one of my elective subjects, I discussed the term project of Machine Design with my group member in the afternoon, and I had a wonderful dinner and watched the” Avatar: The Way of Water” with my best friends in the evening.
* C. After a whale of a time, I will work hard on the term project of Machine Design this weekend.

# 221217 Sat #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because the weather was really cold and rainy, our graduation photo was postponed, and I had the runs.
* I felt unproductive because today was a terrible day, I was so tired that I can't get charged up about anything.
* C. I will work hard to meet the deadline for the term project of Machine Design tomorrow.

# 221218 Sun #
* A. Unsuccessful and productive
* B. I felt unsuccessful because the Ansys on my laptop could work smoothly.
* I felt productive because I spent over 15 hours on the term project of Machine Design and I made a great contribution to our term project.
* C. I will get up early to practice doing the presentation of the term project tomorrow morning.

# 221219 Mon #
* A. Successful and productive
* B. I felt successful and productive because the professor of Machine Design said that I did a splendid job on the presentation. He was seriously impressed.
* C. I cherish that moment and I will start to prepare for the final exams.

# 221220 Tue #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because only spent 2 hours preparing for the exams in the library.
* I felt unproductive because I slept in till noon to catch up on some sleep.
* I burned the midnight oil last night preparing for my term project.
* C. I will study harder to catch up tomorrow.

# 221221 Wed #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I was too late to order one of the components of the robot, and my capstone project was delayed.
* I felt unproductive because I was so tired that I sleep the afternoon away. 
* C. I will need more self-discipline to face the coming exams.

# Five rules that I think make me more successful or productive: #
* (from the website: https://www.indeed.com/career-advice/career-development/ways-to-be-more-productive)

# 1. Focus on one task at a time #
* While you ultimately may be able to get things done when you juggle projects or tasks, focusing on one at a time may help you be more productive. 
* When we concentrate on more than one activity at a time, we tend to use more of that time just for transitioning between tasks. 
* This can result in some of the tasks remaining incomplete or being done at a lower quality than if each task had been a sole focus.

# 2. Take regular breaks #
* It can be tempting to avoid taking the time for a break, but when you neglect to give yourself a time-out for a few minutes, it can harm your overall productivity by resulting in fatigue or burn-out. 
* If this happens, you may not have the energy or motivation to continue making progress. 
* Consider planning out several short breaks during your workday. 
* Most workplaces have a mandatory schedule with dedicated break times, so employees can take at least a five- to 10-minute break after every few hours of active work. 
* These short breaks can allow you to recharge, clear your mind and get ready for the next task.

# 3. Focus on your biggest tasks first #
* Focusing on your biggest and most time-consuming tasks before any other assignments can help you stay more focused than working on smaller and shorter tasks first. 
* Consider planning your assignment list according to these tasks, where you may dedicate your time in the morning when you first arrive to work or at a time of day when you are most alert and energized.

# 4. Time block your schedule #
* Using time blocks within your schedule can also help you increase your productivity. 
* With this strategy, you would set a time limit for each of the tasks you work on.
* Consider 90-minute or 60-minute time blocks. You may choose to make a printout of your schedule and highlight the time frames where you want your time blocks to be. 
* So if you are blocking out 90 minutes to work on a project, note that on your printed schedule. 
* After that time block is over, block another section of your schedule similarly.

# 5. Limit interruptions #
* Interruptions can sideline us throughout the day. 
* Even though you may enjoy your relationships with your coworkers, losing track of time due to conversations, informal and quick meetings or topic discussions can hinder your workflow and decrease your overall productivity. 
* Consider using some strategies to limit the number of interruptions during your day.

# 2022-12-22 #
* The weather is getting cold recently.
* This weekend I’m preparing for the final exam of Machine Design.
* Unhappy Christmas.

# Redo Individual Task This Week #
---

# 221222 Thu #
* A. Unsuccessful and productive
* B. I felt unsuccessful because I should record all experiment data and pertinent details of my capstone project in KLab while I conducted experiments in the past 2 months.
* I felt productive because I spent over 3 hours preparing the presentation for the meeting with my thesis advisor. 
* I should do the lab work more efficiently in the future.
* C. Starting next week, I will develop the habit of conducting and recording the experiments at the same time.

# 221223 Fri #
* A. Successful and productive
* B. I felt successful and productive because today is my first time doing a 30 minutes presentation to my thesis advisor.
* I'm so glad that I made some improvements in my presentation skills this semester. 
* C. This weekend, I will work hard on the final exams of Machine Design and Discrete Mathematics.

# 221224 Sat #
* A. Successful and productive
* B. I felt successful because I gathered with friends on Christmas eve.
* I felt unproductive because I only studied below 8 hours today.
* C. I will study hard to be well-prepared for the final exam on Machine Design next Monday.

# 221225 Sun #
* A. Unsuccessful and productive
* B. I felt unsuccessful because I pushed studying until the last minute.
* I felt productive because I spent over 10 hours studying Machine Design today.
* C. I will get up early to review the whole text the next morning. Learning how to start studying and stop procrastinating are big questions in my life.

# 221226 Mon #
* A. Successful and productive
* B. I felt successful and productive because I had a smooth test today. Maybe I can get an A+ on Machine Design.
* I burned the midnight oil last night preparing for my final exam.
* C. I will start to prepare for the final exams of Discrete Mathematics.

# 221227 Tue #
* A. Unsuccessful and productive
* B. I felt unsuccessful because my test on Discrete Mathematics was not as good as I expected.
* I felt productive because I kept studying from 9 am to 3 pm.
* I had a dinner meeting with laboratory members, and I did the final presentation this semester, we all had a good time.
* C. For me, the top 2 important exams were finished and I could finally have good a res.

# 221228 Wed #
* A. Successful and unproductive
* B. I felt successful because I got really good grades on the exam and presentation on Machine Design.
* I felt unproductive because I was so tired that I sleep the afternoon away, and I waste an entire night on anime.
* C. I will need a lot of self-discipline to face the coming open book exam on Friday.

# 2022-12-29 #
* I’m surprised that obituaries can provide another perspective on what we value.
* They found that the arts — film, theater, literature, music, dance, and fine arts — accounted for 40 percent of the people in the obits.
---
* My obituary may be like this (student version):
* YUN-PING LEE, Taiwanese, master of mechanical engineering at Tokyo Institute of Technology, Dies at XX
* As a foreign student, he made a contribution to mechanical engineering and strengthened relations between Taiwan and Japan.
---
* At the present stage, I have no idea what man I will be and what positive or negative contribution I will make to the world after I complete my Master’s. 
* It’s hard to describe what achievements made by ordinary people look like over a lifetime in just a few words.
* According to the video, one way to make a mark or achieve success is to help other people succeed.
* While there was greater diversity in the professions and accomplishments of the non-famous, they noticed that almost everyone — famous or not — helped people in some way.
* And the obituaries showcased the many ways it’s possible to make an impact.
* Our lives can have great meaning without great recognition.
* Obituaries don’t exclusively cover individuals in the public eye. 
* A lot of them are people you’ve never heard of who’ve done things that make them deserve to be remembered.
* Hope someday I'll be a useful adult and do meaningful things to society, and to the world.
* This Friday NCKU’s school selection results for the school-level recommended foreign exchange student program in the first semester of the 112 academic year was announced.
* I was so excited because I am the top one of the accepted students of Osaka University.
* The news was the best new year gift. Happy New Year!
* This Saturday is new year’s eve, and I had a new year countdown celebration party with my friends. 
* We had a whale of a time.
* Next year I will countdown to New Year 2024 in Osaka.
* I'm really looking forward to my exchange student life in Japan.
* The exchange program gives me a perfect opportunity to get out of my comfort zone and have a taste of foreign life.
* By the way, I am progressing in my presentation skills in English this semester.
* And my improved performance does credit to this class.
* After the end of this class I will keep practicing English.
* I hope I can fit in the circle of exchange students, and have the best experience in my life.
* It was such a pleasure to be your student. You’ve taught me so much!
