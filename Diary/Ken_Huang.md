This diary file is written by Ken Huang E14116223 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-19 #

* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation 

# 2024-09-26 #

* below is what I learned from the first video : 
	*Take responibility for what you see on the Internet and stick on the website you believe in.*
* below is what I learned from the second video :
	*Trust the government's data and don't easily believve in the one made by private company 
	because they might only collect the data they want to see instead showing the true numbers.*
* Teacher asked us to put the three questions asked by the speaker Mona
	1. can you see the uncertainty
	2. visualization is important
	3. how was the data collected
* In ten years, I will be 30 years old and I have moved to Japan, my second favorite country in the world.
	My dream is to work and live there, having a beautiful wife and cute kids.
	The reason why I choose Japan instead of other countries is due to the culture, food, wonderful landscape.
	And I'm so obsessed with the street views in Japan, regardless of the modern ones or the countryside ones.
	Overall, everything about Japan fascinates me a lot. Therefore, I'm planing to living in Japan in my future.

Five current trends  which will affect me now and the future :

* **Electric Vehicles :** Since I'm going to throw myself into car industry, the developements and trends of car are quite important for me.
* **The developement of AI :** No need to say, AI has improved rapidly these years, which will definitely cause a huge difference in everyone's life.
* **The depreciation of yen :** Thanks to the depreciation, I'll have less burden to pay my rents or raise a kid.
* **Healthcare and Biotechnology Advancements :** This will make us live longer and longer, which let me have more time to enjoy Japan.
* **Climate Change :** It is a long-standing issue, and it may influence many industries in many aspects.

The different views from various sources on : *Trump calls Harris 'mentally impaired' in fresh rant, says 'she was born that way'*

* **The Washington Post :** It emphasizes the accuse by Trump is a false attack.
* **WION :** It focuses on what Trump said in the whole speech instead of having subjective comments and words.
* **Mediaite :** It never mentions about how savage the Trump's words are. Instead, it uses the word "says," "tells his supports" to cover those bad words from Trump.

# 2024-10-17 #


Today's class was packed with insights, we discuss some topics around money, currency creation, 
income distribution, recessions and so on. Money is something that interests a lot of people, but few of them know the meaning of money.
Many of them will say money is evil, but it's not the money that is evil, but people. 
We talked about how it would be like if NTDs are changed into bitcoins, and the outcome of it would be disastrous. 
Because money is printed every day, but bitcoins are not. After the class, I think I had a much deeper understanding of those economic aknowleges. 
It was shocking to learn how money, currency creation, and those economics things play such crucial roles in shaping our daily lives. 
Also, understanding the risks of credit bubbles gave me a new appreciation for the need for regulation and caution in financial markets.

# 2024-10-24 #

* This class we watched some videos about hate speech, benefits of exercise, and helth care

* A man from a immigrant family which joined the neo nazi movement when he was 14 years old.
He believes not knowing his place in life led him to join the movement. 
He said when he was 19 he met a woman and had a child which made him rethink his life. 
He decided to open a music store which sold 75% white power music. 
However, later down the line he decided to stop selling the racist music because he connected with some black and gay custmoers.
Unfortunately, when he got rid of the racist music the store couldn't support itself, which resulted in hitting his all time low in life.
Thankfully, he got a job at IBM for installing computers.
Later on in his corrected life he started to face his past by confronting some of the previous people he hurt, 
and even went on to help others connect with some of the people they hate.

* Students of the class also shared some hate stories between China and Taiwan.

* A video sharing a research on exercise and effects on the brain showed that after exercising your mood instantly gets better and better attention span.

* Another video also shared a story about going to the dentist and got recommended the most expensive surgery treatment. 
After research he found out that most doctors recommend the more expensive treatment methods due to profit.
Therefore the speaker believes that we should shift our focus towards preventative healthcare and not treatment based healthcare.

How to live healthier: 

*1. Eat less fried food, fried food can lead to higher rates of type 2 diabeties and heart diseases.

*2. Eat more fruits and vegetables, helps protect against cancer, diabetes and heart diseases.

*3. Eat less sugar, excess amount of sugar leads to tooth decay, high blood pressure and diabeties.

# 2024-10-31 #
typhoon holiday

# 2024-11-07 #
This week we first listened to other people’s diary about how they imagine their country in the future.
We talked about the difference between profitable and non-profit organizations.

# 2024-11-14 #
Today, we carried on with class by listening to group presentations about identifying signs of depression in friends and strategies to support them.
This part was both practical and enlightening, encouraging me to pay closer attention to my friends and regularly check in 
to stay connected with what’s happening in their lives as well as my own.
We also revisited key points from Prof. Korte, who emphasized six core areas to focus on with the help of others.
I reflected on areas where I could improve personally to better prepare myself for a future as a successful engineer.

Towards the end, we had three minutes to brainstorm three actionable principles for leading a happy, fulfilling life. Here’s what I came up with:

* Make sure it’s what you truly want
Before committing to something, pause and ask yourself,
“Is this something I really want?” If the answer is yes, be prepared to face any challenges that might arise.
If you're ready for those, it’s a good sign that you’re genuinely committed and willing to follow through.

* Understand your ‘why’
Even when choices seem straightforward, it’s important to clarify your reasons.
Having a clear understanding of why you’re doing something helps keep you motivated and ensures you stay on track.

* Pick what makes you happy
When making decisions, I prioritize the option that excites me—often the one that challenges me or gives me butterflies.
Choosing excitement ensures I’ll keep choosing it over and over again.

# 2024-11-21 #
* Jaron Lanier, a computer scientist, had a TED talk show, arguing that the Internet needs a fundamental redesign.
He highlights how the current system fosters manipulation, polarization, and inequality.
* "Weapons of Math Destruction." A YouTube video titled by Cathy O'Neil. She discusses the abuses
and destructive consequences of algorithms in various domains such as education and employment.
* She emphasizes that algorithms are not objective or fair, as they are created by individuals with their own biases and agendas.
* Also, she advocates for more transparency, accountability, and audits of algorithms to ensure fairness and justice in their application.
* It's important to be mindful of how the internet shapes my behavior and perspectives.
We should strive to use the Internet consciously, valuing authentic interactions and protecting my attention from manipulative systems.
* It really surprised me when the professor talked about how Facebook got their revenue and became a profit company.

# 2024-11-28 #

At the beginning of today’s class, the teacher told us that we couldn't use our phones for the next three hours.
It was really hard for me because my English isn't very well. I often search the vocabularies that I don't know.
Taking phone away made me anxious.
During the group project presentations, one group’s idea really impressed me.
They designed a watch alarm that you can wear on your wrist to wake you up easily.
I think this idea is so practical, especially for people who struggle to get out of bed in the morning.
After each presentation, we voted on their proposals to decide which were the most promising.
We also explored the lifecycle of civilizations and discussed how societies recover from dark ages, highlighting the resilience and adaptability of human history.
During the session, we watched a YouTube video about riots in Amsterdam involving Maccabi supporters. 
The video shed light on how media narratives often omit key facts, prompting us to think critically about the information we consume.
At the end of the class, we were allowed to pick up our phones,
and the professor asked how many times we had felt the urge to check them during the session. 
It was a subtle reminder of our growing dependence on technology.

# 2024-12-05 #
There is no clear link between press freedom and whether news is portrayed positively or negatively.  
After three presentations, larger groups discussed a story and how different countries’ media presented it. 
The discussions were engaging, but the group dynamics revealed diverse work styles and ethics.  

* Planetary Boundaries  
Climate change will have massive effects on nature, wildlife, and humans. 
By 2050, climate-related damage could shrink global GDP by 18%.
For 1,000 years, Earth’s temperature stayed stable (around 14°C), allowing civilizations to thrive.
Now, rising temperatures are turning CO2 sinks like the Amazon rainforest and oceans into CO2 sources, reducing Earth’s ability to absorb emissions.  
Sudden ocean temperature increases pose new risks, but scientists don’t yet understand the causes.
With five of 16 critical tipping points already crossed, exceeding 1.5°C is likely within five years.  
Planetary boundaries highlight the limits we must respect to avoid disaster.
Unfortunately, we are breaching most of them. Urgent action is needed to prevent severe long-term consequences.

# 2024-12-12 #
This week, each group presented their action plan, 
including the Work Breakdown Structure (WBS), PERT chart, and GANTT chart, while explaining how their plan impacts one of the planetary boundaries.
A representative from each group briefly explains their action plan to the class, followed by a Q&A session to gather input, discuss, and provide suggestions.
After the presentations and discussions, the class votes for the most compelling action plan.
The Q&A and debate sessions are valuable opportunities to learn from other groups, improve our plans, and contribute constructive feedback.
We also watched the documentary "The Third Industrial Revolution: A Radical New Sharing Economy" by Jeremy Rifkin,
where they explores solutions to global economic crises, emphasizing a shift toward new economic systems that prioritize equality and sustainability.

# 2024-12-19 #
* successful + productive
* Since I studied a lot and being productive today

# 2024-12-20 #
* successful + unproductive
* Because I did a great job on my fluid mechanics test.
* I slept basically the rest of the day since I studied it till maybe 3 AM yesterday.
* Study the exams/tests earlier.

# 2024-12-21 #
* successful + unproductive
* I hung out with my friends this day evening, and that's happy.
* I only do 1 homework after I got up at noon.
* Get up earlier.

# 2024-12-22 #
* successful + unproductive
* I had a friends came to Tainan from Hsinchu, so I took him to some famous place in Tainan.
* Since I didn't do anything related to schoolwork.
* but I thought I'd try my best.

# 2024-12-23 #
* unsuccessful + productive
* Since the weather today is really bad so it's an unsuccessful day.
* but I finished many homework today, I thought it could be productive.
* find a way to make the weather better

# 2024-12-24 #
* unsuccessful + unproductive
* The weather was still a tragedy today, what's worse is that I got sick today
* Also, today is Christmas eve, so I didn't study that much as usual.
* try hard not to get sick

# 2024-12-25 #
* successful + productive
* Today our fluid mechanics professor gave us a bar of chocolate as a Christmas gift.
* Since I didn't go out to celebrate Christmas, I studied quite a lot today.

# 2024-12-26 #
* successful + productive
At first of this week, we basically had some group presentations and comments. 
In second class and third class, the professor had us to share with everyone for our “five rules to success.” 
After listening to everybody’s suggestions, I felt more confident and learned more methods to achieve success. 
Especially, there was a guy talking about culturing good disciplines, which is a quite important point for me 
because sometimes I would have felt too lazy to work on. 
Therefore, if I make good habits I think it will be more productive.

# 2024-12-27 #
* successful + productive
* Since I studied a lot and being productive today

# 2024-12-28 #
* successful + unproductive
* Because I did a great job on my fluid mechanics test.
* I slept basically the rest of the day since I studied it till maybe 3 AM yesterday.
* Study the exams/tests earlier.

# 2024-12-29 #
* successful + unproductive
* I hung out with my friends this day evening, and that's happy.
* I only do 1 homework after I got up at noon.
* Get up earlier.

# 2024-12-30 #
* successful + unproductive
* I had a friends came to Tainan from Hsinchu, so I took him to some famous place in Tainan.
* Since I didn't do anything related to schoolwork.
* but I thought I'd try my best.

# 2024-12-31 #
* unsuccessful + productive
* Since the weather today is really bad so it's an unsuccessful day.
* but I finished many homework today, I thought it could be productive.
* Also, today is new year's eve, so it is a good day.

# 2025-01-01 #
* unsuccessful + unproductive
* The weather was still a tragedy today, what's worse is that I got sick today
* But today is the first day of 2025, so I didn't study that much as usual.
* try hard not to get sick

# 2025-01-02 #
* My obituary :
Wei-Cheng Huang, a brilliant and accomplished engineer, passed away peacefully at the age of 97, 
leaving behind a legacy of innovation, perseverance, and boundless curiosity.
Renowned for his exceptional technical expertise and visionary mind, Ken made a profound impact on the engineering world,
earning accolades and respect from peers and industry leaders alike. 

By the age of 45, Ken had reached the pinnacle of his career,
having led groundbreaking projects that transformed industries and set new standards in engineering excellence.
His dedication and ingenuity not only brought him professional acclaim but also financial success, 
allowing him to retire early and turn his focus toward personal aspirations.

In retirement, Ken embraced life with the same enthusiasm and vigor that had defined his career.
A lifelong learner and adventurer at heart, he devoted his time to exploring the intricacies of the world. 
Whether embarking on journeys to remote corners of the globe, engaging in stimulating conversations,
or diving into books on a wide array of subjects, Ken pursued knowledge and experiences with unrelenting zeal.

His travels spanned over 40 countries, each destination leaving an indelible mark on his life. 
From the serene fjords of Norway to the vibrant bazaars of Marrakech,
Ken sought to understand the beauty and diversity of our planet.
He captured his adventures through vivid photographs and journals, 
sharing stories that inspired those around him to see the world through a lens of wonder and gratitude.

Ken will be remembered as a man of profound intellect, unwavering integrity, and an unquenchable thirst for discovery.
His contributions to engineering continue to influence the field, while his adventurous spirit and love for life leave a lasting impression on all who knew him.
Ken’s friends and family celebrate his remarkable journey, cherishing the memories and lessons he imparted. As they honor his legacy, 
they find comfort in knowing that he lived a life of purpose, passion, and fulfillment, making the world a better place through both his work and his character.

# 2025-01-09 # 
This is the last week we had this course. It was a great experience to have the full-english class.
I really learned a lot from this course. I will recommend the juniors to join this class next year.