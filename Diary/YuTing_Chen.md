This diary file is written by YuTing_Chen E14104179 in the course Professional Skills for Engineering the Third Industrial Revolution.


2023/09/07
The introduction of this class was really interesting,I was inspired by the videos regarding global issues.
In the second class,after taking the quiz,I found that I have little knowledge about what's happening around the world,which forcing me to spend more time participating in this class.

2023/09/14
Other students were presenting their groups' homework which was interesting.
We should not afraid of encountering conflicts, instead we should integrate different opinions and balance.

2023/09/21
The second class was about fake news,I found the TED talks video spotting fake data very inspiring.

HW:Summary of what I think my future would be like after 10 years

	After ten years, I will become an engineer. It will be my first choice to live in Taipei or Hsinchu since I love living in metropolitan.
Because I am good at learning language and some speaking skills. My company will be an international high-tech company and my job is to interact with clients from different countries.
	Nowadays, there are five current trends of change that will affect 10 years from now. The first one is the aging population, due to the advance of the technology, life expectancy
has been increased gragually, causing people to work longer than thay expect. Therefore, the average age of the society will get higher and higher, which will definitely affect my life.
The second trend will be the growth of the electric car, since a car fueled by electricity can reduced the emission of carbon dioxide. More and more people will choose to by electric cars
instead of a traditional one, including me. The third one is the advance of the ginetic engineering, which will influence the medical science, agriculture and life science.
Maybe new creature will be created and I will be fortunate enough to witness the happening.
	The fourth trend is the decrease in the biodiversity, we human has caused many serious problems since the first industial revolution, causing a catastrophe in biodiversity.I can expect
that in the next ten years more and more species will extinct or be endangered, and this is because of the climate change and the depletion of the natural resources. The last one 
is the AI technology, during the next ten years many jobs will be replaced, more and more people will face no jobs.  AI seems to make our lives more convenient, more interesting.
On the other hand, the potential danger it causes will affect billions of people. When I graduate from university, this factor will be taken into the consideration when choosing my jobs.

HW2
Topic: Male nanny convicted in California of sexually assaulting 16 young boys in his care

Center View
1.Matthew Antonio Zakrzewski was convicted of 34 felony sex charges involving 17 victims, who suffered unimaginable terror due to the betrayal of their babysitter.
2.Zakrzewski, who filmed many of the assaults, faces a maximum sentence of 690 years to life in prison.
3.Authorities uncovered thousands of images and videos linking Zakrzewski to more victims.

Left View
1.Matthew Zakrzewski was convicted of sex crimes against 17 boys he had been hired to babysit from 2014 to 2019.
2.Zakrzewski portrayed himself as a trusty caretaker on his website, offering a "buddy" experience to the children
3.Prosecutors presented substantial evidence, including a large collection of photos and videos, resulting in Zakrzewski's conviction on all 34 counts of sex crimes.

Right View
1.Matthew Antonio Zakrzewski, was found guilty of 34 felony counts, including molesting 17 children and showing pornography to them. 
2.Zakrzewski persuaded over a dozen families to let him care for their children by claiming to have experience with autism and developmental disabilities.
3.The jury's verdict left parents sobbing, and Zakrzewski now faces up to 690 years to life in prison.


2023/10/12
Today's topic is money, the professor show us the relationship between the government and the central bank.I'm sure that I will definitely not learn this in mechanical engineering 
class. In the third class, I learned how the credit play an important role in the economic. It was very interesting!

2023/10/19
In the first class, I learned economic condition from different countries such as Thailand, Indonesia, and Czech, through others presentation. It was suprising to me that some of the graph shown
on the presentation were totally different from my country. Also, I had learned many tips when searching data.
In the second class, the TED video was great. It was really useful to let me handle the unfamiliar concept like fascism.

2023/10/26
In todays lecture, I learn the definition of knowledge, it is a kind of true belief. Later, the professor
show two TED talk videos. One is about the sick-care system and the health insurance system. The other
is regarding the importance of the exercise on our health.
In the third class topic is anxiety, through some survey done by ncku students, I learn many reasons causing
students to be anxious : quiz and exams, friendship, love, or self expectations. It is quite thought-provoking,
and very helpful to my life.

2023/11/02
Nowadays, some people cannot deal with the problem of depression. Once we can not handle these responsibilities, we will feel uncomfortable. It is not good for our mental health.
I also learned how to talk with people who have depression. We do not have treat them as a patient, instead just communicate with them like interact with our normal friends.
In the lecture, it reminds me of a friend who have depression that has seeked helps to me several times, but unfortuneately I couldn't give him useful advice because I didn't
know what he thought.

2023/11/09
1.Today some people shared their experiences with depression and anxiety, I am curious about how people seek medical help when they have depression.
2.finding purpose on life is a lifelong problem to everyone, and I still struggle with finding one.
3.We sit with our big group together today and discuss about our big project next week. We only have three members and this is definitely a tough task.

2023/11/16
1.This week is our big group's project. I heard a lot of interesting topic from other groups, and I think we are not well-prepared in this project.
2.The presentation about the improtance of sleep really impressed me. We should not stay up too late and learn to have a good sleeping quality.
3.I will have a lot of exam in the following weeks. Hope that I can have enough time to sleep and do well in our big project.

2023/11/23
1.We reform a six member group in our big project, and we spend a little bit of time discussing how to make the next week presentation.
2.Today I learn how to reach a consensus, it is very hard to reach and in most of the time it is decided by the majority.
3.The big project requires three actions to put into practice and I think it is really difficult to pop one.

2023/11/30
1.Today's first lecture we have an experiment about the addiction of cellphone. I have several times looking for my cellphone, it looks like I have severe addict on the Internet.
2.Few of my teammate attend the class, so after class the professor ask our team to the front to figure what's going on.

2023/12/07
1.In today's lecture we are seperated into groups for many times and discuss various kinds of problems, I think these questions are all thought-provoking.
2.Todays topic is about planetary boundary, I like the feeling of learning something new during the class.

2023/12/14
1.Today's lecture is our course project presentation day, my group didn't present well and there are many problems remaining,I hope the fourth presentation will be better.
2.The professor ask us to vote for the favorite project, I think the eco plastic bag has the most impact on planetary boundary/
3.I represent my team as a debater in the third lecture, I think my speaking skill is not fluent enough to answer the classmates' problem.
4.The task next week is really tough, I hope I can handle it well and I wish the winter vacation coming as soon as possible.

2023/12/22
Today is successful because my friends and I ate Tanyuan together to celebrate the Donzhi Festival,we had a really great night.
But it was not a productive day because I didn't prepare for my upcoming final exams, I hope the next day I can start to study.

2023/12/23
Today is unsuccessful and unproductive because I was a little sicked when I woke up.
I have few improvements to my exams, and tomorrow must be a productive day.

2023/12/24
Today is successful and productive because I felt much more comfortable and spent most of my day studying.
As for one thing I can do different in the next day, I think I can go to bed earlier because good sleeping quality makes a productive day.

2023/12/25
Today is successful because the final exam is really easy I expected to have good grade on the course.
Today is productive because I went to the gym after class and studied the final exams at night.

2023/12/26
Today is successful because I watched a fantastic basketball game and an interesting perdormance at night.
But it is not productive because I was distracted from video games when I started to study.
I think I should throw my cellphone out of my sight to be more productive.

2023/12/27
Today is unsuccessful because I lost my gloves when I went to school, and also I didn't do well on the quiz this morning.
But today is productive because my teamates and I figured out the problems of our course project in the automatic controll class.
Hope the next time I can preview the class to get well-prepared for the quiz.

Five rules to be more successful or productive.
1.Have good sleeping quality, do not stay up late too often.
2.Do not be distracted form the cellphone.
3.When waking up,take a minute to figure out what should do today.
4.Stay healthy all the time.
5.Talk to friends and have adequated entertainments to be in good mood all the time.

2023/12/29
successful and productive
Today we have a final exam on mechanical experiment. I did well on this exam so it was successful. 
Also, I study very hard on the following exam, so it is a productive day.

2023/12/30
successful and unproductive
Today I hung out with friends and had a interesting day, but I didn't prepare for my exams.

2023/12/31
unsuccessful and productive
Today I felt a little sad because nobody celebrated New Year Day with me. 
And I study very hard for my upcoming day.

2024/1/1
successful and productive
Today I felt very happy because I prepare for my exam with many friends. 

2024/1/2
unsuccessful and productive 
Today I was sad because my exam result is horrible, but I still study very hard for another exam.

2024/1/3
successful and productive
Today I was really tired because I have four tough exams tomorrow