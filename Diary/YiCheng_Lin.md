This diary file is written by Yi Cheng Lin C24121197 in the course Proffessional skills for engineering the third industrial revolution.

# 2024-9-19

* The video is great. 
* With reliabe evidences, speaker convince the audience the world is getting better. 
* I learned we need to use data to support our idea. 
* The faith without foundation is useless, or even harmful.
* Graphics and data show that renewable energys have huge potential. They are getting cheaper and cheaper. 

# 2024-9-26

* I can't find where should I upload my slide in the first time, but I found it later in the class.
* It's not easy for me to catch up the class. This is a great challenge for me.
* It seems hard to acheive the sustainable development goals in 2030. We need more effort.
#
* task: 3 questions Mona ask

  1. Can you see uncertainty? We need to remember the important facts, not just trying to predict with numbers. In some situations, averages can mislead people.
  2. Can I see my self in the data? Not just look averages, check how gender, educational status and other factors affect the result. Change the scale can change the story.
  3. How was the data collected? Start of finding the original questionnaire. What are the methods of distributing the survey questionnaire and who is the target audience? Do not be misled by numbers.
  
* task: summary of future
#
* Ten years later, I hope I would live in a homey house, located in my birthplace, Kaohsiung. My job is related to science and engineering, with good salary and sense of achievement. 
 I can learn a lot of new knowlege from my work. I will join the church activities in my freetime. I think the five current trends of change will affect me most are: 
 
  1. artificial intellegence 
  2. renewable energy
  3. virtual reality
  4. climate change
  5. high housing prices
  
    First is artificial intellengence. For many people, this is a revolutionary technology, we can use this tool achieve tremendous work. Nowadays, ai can produce lifelike video with simple instruction, all you need to do is typing some key word.
    It is hard to imagine how ai will be ten years later.
    
    Next is renewable energy. Pollution is a serious issue, and many governments are working to address it. Renewable energy is a promising solution.
    While it was once more expensive than gasoline fuels, advancements in technology have made renewable energy more affordable. Scientists and engineers continue to find ways to improve its efficiency and reduce costs.
    I hope in ten years, cities will have cleaner air and rely more on renewable energy rather than fossil fuels, and I don't need to suffer from allergic rhinitis and extreme weather anymore. 
    
    Third is virtual reality. When I was a junior student, I watched a movie called Ready Player One, It describe a imaginary future that people are immerse in a virtual reality game called the OASIS to escape from reality.
    The movie use impressive CGI, and the OASIS is really attractive.
    In 2023, apple produce the vision pro, a mixed-reality headset. Vision pro didn't have the incredible power like the equipment that players use in the movie, but this product is still revolutionary.
    I believe this technology will change our life forever. Maybe we can enjoy the similarly game in the movie ten years later. I really looking forward to it.
    
    Another trend will affect me most is climate change. The situation is really bad, many people suffer from extreme weather and natural disaster. In Taiwan, typhoon and high temperature are serious problem.
    If we don't take action, it's hard to withstand the rage of mother nature. I hope in ten years later, our world isn't getting worse.
    
    Last trend will affect me most is high housing prices. This problem affect many young people in taiwan. The government has introduced various policies to bring down housing prices, but the outcome isn't great.
    I wish this problem can be solve in few years later, so I can afford a house in the future.
    
* Task: summarise the different views in the sources.
#
* Left (Inquirer): A SpaceX craft has launched to rescue two stranded astronauts. The astronauts stuck in the international space station(ISS) due to Boeing's Starliner has some problem. Nasa and spaceX collaberate a plan with each other to save them. 
* Center (BBC News): A SpaceX craft has launched for crew-9 mission and rescue two astronauts. Boeing's Starliner has some problem, so NASA decided not to bring them home on the Starliner. They will stay in ISS and join Crew-9 later.
* Right (India TV News): SpaceX launched a rescue mission for the two stuck astronauts, they will return in late February. They are stucked since Boeing's Starliner has some problem.
#
* I notice that the different media has almost same report. I think this is because this event has no related to politics or other sensitive issue.
    
# 2024-10-6
* The typhoon caused a lot of damage to my city.
* Many roads are locked due to fallen tree.
* My home and family didn't get harmed. I believe that preparation beforehand is more important than hindsight. 

# 2024-10-13
* I have lots of homework in this week. It's challenging for me to manage my schedule.
* There are two midterm exams next week. I’ve got to catch up quickly.
* I recorded the Week 4 homework video many times. It took me about an afternoon to get a good version because I stuttered a lot.
* In the email, the teacher asked us to collect data from 1980, but the data on the Central Bank of Taiwan's website doesn’t go back that far. I suspect the earlier data is in paper form or may not exist at all.
* I need to improve my data collection skills.

# 2024-10-20
* I learned that money is created through loans.
* I learned what M1A, M1B, and M2 stand for in Taiwan. In other countries, different symbols are used to define money. In most countries, money supply measures are usually named M1, M2, M3, etc.
* I want to learn more about Bitcoin. The creator of Bitcoin, Satoshi Nakamoto, is a mystery. It's so unbelievable that no one can find out who he or she is.

# 2024-10-29
* In the past two weeks of my course, I learned that the concept of "money" is a fictional story. We trust its value simply because everyone else trusts it.
* Another fictional story that affects me is anime and video games. I don’t actually believe they’re real, but I spend my time and money on these fictional characters and video games to entertain myself. In last week, I buy Forza Horizon 4, a racing game.
* I have my own religion and go to church every week. To attend the gathering, I go back to Kaohsiung on Wednesday evening and come back on Thursday every week.Many people believe there is no God, but I have a different opinion. Religion has changed my life, giving me more reasons to do the right thing.
* To live a healthier life, it is important to have good sleep. I always do some stretches before I go to bed, which helps me feel comfortable and sleep well. Everyone has different ways of relaxing. The key is to find a way to relax and release your stress before you sleep.
* I hope my future is peaceful and safe, with a good job and a high quality life. Lai Ching-te is the president of ROC. Lai Ching-te’s vision for Taiwan’s future, known as the "National Hope Project," focuses on three main pillars: Democratic Peace, Innovative Prosperity, and Justice and Sustainability.

## Main Pillars:
1. **Democratic Peace**
    - Emphasizes protecting Taiwan's democratic values.
    - Strengthens national defense and diplomatic strategies to maintain stability in the Taiwan Strait.
    - Envisions Taiwan as a regional promoter of peace, working with international allies to support democratic freedoms.

2. **Innovative Prosperity**
    - Economic strategy revolves around "net-zero technology" and "smart technology."
    - Goals include:
        - Diversifying industries.
        - Strengthening Taiwan's position in global supply chains.
        - Fostering a robust environment for innovation.
    - Aims to cultivate:
        - "Hidden champion" companies.
        - Globally competitive unicorn startups to sustain Taiwan's economic edge.

3. **Justice and Sustainability**
    - Focuses on social equity with policies such as:
        - "0 to 6 National Childcare" for family support.
        - "Long-term Care 3.0" to encourage population growth.
    - Introduces "Three Arrows for Housing":
        - Expands social housing.
        - Adjusts property tax rates for fairer housing.
    - Commits to environmental goals:
        - Achieving net-zero carbon emissions by 2050.
        - Promoting a sustainable green economy to benefit all citizens.

## Vision Summary:
- Aims to holistically strengthen Taiwan’s:
    - Democratic integrity.
    - Human rights.
    - Economic strengths.
- Prioritizes social justice and environmental sustainability to ensure Taiwan’s long-term stability and prosperity.

# 2024-11-3
* It's quite rare for a typhoon to strike in November, as typhoons usually hit Taiwan in the summer. I'm uncertain if this is connected to climate change.
* The final project seems challenging, as we need to create a sustainable product that can be used in Tainan.

# 2024-11-7
* All the videos claims that we should listen to the depression friends before we say our thoughts. I agree this oppineont. In the Bible, James 1:19 says: "Know this, my beloved brothers: Everyone must be quick to listen, slow to speak."
- **A.** Observe his behavior.
    - If he seems low on energy or shows no interest in anything, he might be experiencing depression.

- **B.** Listen to him.
    - Avoid saying you understand his situation, as you may not fully understand it.
    - Instead, be kind and let him know you’re there for him.

- **C.** If you notice someone showing signs of depression:
    - Ask directly about how he’s feeling.
    - Inform his family or friends if needed.
    - Be there for him and listen attentively.
    
# 2024-11-14
* 《Strawman - The Nature of the Cage (OFFICIAL)》 discusses the issues of law and personal freedom. I find the concept of the strawman interesting. It divides the legal fiction(straw man) from the real person, highlighting their differences.
* I select "B. The citizens right to a healthy life and protection of the environment." Comparing the National Health Insurance between Taiwan and American.
### Comparison Between Taiwan's and the U.S.'s Health Insurance Systems

**1. Laws Addressing Health Insurance**

- **Taiwan:**
  - *The National Health Insurance Act (NHI Act)*  
    Introduced in 1995, the NHI Act provides universal health coverage to all citizens and residents.

- **United States:**
  - *The Patient Protection and Affordable Care Act (ACA or "Obamacare")*  
    Enacted in 2010, the ACA aims to expand health insurance coverage, improve healthcare quality, and reduce healthcare costs.

**2. Key Provisions in the Laws**

- **Taiwan's NHI Act:**
  - *Article 1*: "This Act is enacted to provide citizens with universal healthcare and enhance public health."
  - *Article 10*: "Enrollment in the National Health Insurance is mandatory for all citizens and eligible residents."

- **U.S.'s ACA:**
  - *Section 5000A*: "The individual mandate requires all Americans to have health insurance or pay a penalty, unless exempt."
  - *Title I*: "Prohibits insurance companies from denying coverage due to pre-existing conditions."

**3. Implementation of the Laws**

- **Taiwan:**
  - Funded through premiums (based on income), government subsidies, and employer contributions.
  - Covers a wide range of services, including outpatient, inpatient, dental, and traditional Chinese medicine.
  - Implementation is centralized and managed by the National Health Insurance Administration.

- **United States:**
  - Insurance coverage is through a mix of public programs (Medicare, Medicaid) and private insurance.
  - The ACA expanded Medicaid and provided subsidies for private insurance purchased through exchanges.
  - Healthcare is decentralized, leading to variations in implementation across states.

**4. Rights and Obligations for Citizens and Foreigners**

- **Taiwan:**
  - **Citizens**: Universal coverage, mandatory participation.
  - **Foreigners**: Eligible if they hold an Alien Resident Certificate (ARC) and have stayed for six months.

- **United States:**
  - **Citizens**: Coverage is not universal; citizens must purchase insurance or qualify for Medicaid/Medicare.
  - **Foreigners**: Access depends on residency and immigration status. Some non-citizens may qualify for Medicaid or marketplace subsidies.

**5. Key Observations and Comparison**

- **Accessibility:**
  - **Taiwan**: Ensures healthcare for all citizens and eligible residents, emphasizing equity.
  - **U.S.**: The system relies on market-based insurance, leading to disparities in access and coverage.

- **Costs:**
  - **Taiwan**: The system is cost-efficient due to its single-payer model.
  - **U.S.**: Healthcare costs are among the highest globally due to administrative complexity and private insurance.

- **Simplicity:**
  - **Taiwan**: Centralized approach, simplifying the healthcare delivery system.
  - **U.S.**: Fragmented system, with variations in coverage and implementation across states.
 
# 2024-11-21
* In my opinion, governments can create a better reporting system and enhance punishments for fake news producers, especially for those who are influential, as they can profit from their audience.
* When the system receives a certain number of reports, the government should investigate the post. If it is confirmed to be fake news, the government should ban it and fine the poster.
* Laws cannot solve every problem. We need to check sources and think critically to prevent being manipulated or affected by fake news.

# 2024-11-28
* Generally speaking, the Press Freedom Index is related to a nation's political system. Democratic countries often have greater freedom to report news.
* The President of South Korea declared martial law on December 3, leaving many people shocked. This event may lead to further consequences. Fortunately, no one was seriously injured in the incident.
* I will ask my Korean friends for their thoughts on this issue.

# 2024-12-5
* If humanity does not achieve net-zero carbon emissions soon, Earth will become uninhabitable for humans.
* I hope our team's project can make a tangible contribution to environmental protection.
* I also take some personal actions for environmental protection, such as using my own lunchboxes and utensils. At many stores, bringing your own container offers a discount, so it’s a win-win situation.
* Governments and large corporations should also take corresponding actions, as the pollution they cause in a single day can be comparable to what an individual might produce over several years. 
* Individual efforts are limited; only when governments and businesses actively pursue environmental protection can the current situation be reversed.

# 2024-12-12
* In today's debate, I noticed that many groups had no clear plan or actionable steps.
* Our project is actually quite simple. It won't take much time, and we only need to pay the photo printing fee.
* Although it's not very difficult, we still have some differing opinions about the plan. I hope we can finish this project.

# 2024-12-19
* successful/unproductive
* Our group project is going well, so I feel successful. I waste too much time on my mobile, so I feel unproductive.

# 2024-12-20
* unsuccessful/unproductive
* I should have had leg day today, but the machine was occupied, so I didn't train my legs. I didn't finish the experiment homework. Today was unsuccessful and unproductive.
* Tomorrow, I will try to finish the experiment homework.

# 2024-12-21
* unsuccessful/unproductive
* I tried, but I still can't finish the homework. I am always busy on Saturdays.
* I can't be distracted anymore.

# 2024-12-22
* successful/productive
* I finished my experiment homework today. I also worked out for one hour.
* Winter vacation is coming. I need to hang on.

# 2024-12-23
* successful/productive
* I achieved my goal: sleep before 11 p.m. I finished the math homework.
* I hope I can finish the group task tomorrow.

# 2024-12-24
* successful/productive
* I finished the astronomy homework today.
* I will go to the gym at 1:30 tomorrow. 

# 2024-12-25
* successful/unproductive
* I spent one and a half hours at the gym, training my arms and chest. I didn't prepare for the final exam today, so it was an unproductive day.
* Five rules to help me become more successful and productive:

  1. Use time wisely.
  2. Eat healthy food.
  3. Get enough sleep.
  4. Maintain positive thoughts.
  5. Exercise regularly.
  
# 2024-12-26
* unsucccesful/unproductive
* I'm a bit tired today, so I didn't review much homework.
* Tomorrow, I will review my homework and go to the gym in the afternoon.

# 2024-12-27
* succesful/productive
* Today, I reviewed classical dynamics and worked out. I hope I can get a good grade.
* Tomorrow, I will continue focusing on dynamics.

# 2024-12-28
* succesful/productive
* Today, I reviewed classical dynamics. 
* The test is on Monday, so I need to practice more problems.

# 2024-12-29
* successful/productive
* Today I figured out some complex concepts in classical dynamics.
* I need to make good use of my time.

# 2024-12-30
* unsuccessful/productive
* The final exam wasn't hard, but I wasted too much time on a hard problem, so I couldn't finish the simple ones. My exam strategy isn't good.
* I studied for the experiment test. It's tomorrow.

# 2024-12-31
* successful/unproductive
* I think the experiment test is easy. I felt unproductive because I didn't review my homework today.
* Tomorrow is a holiday; I will try to study more seriously.

# 2025-1-1
* successful/productive
* I finished half of my electricity and magnetism homework.
* I hope I can finish it tomorrow.

# 2025-1-2
Yi Cheng Lin passed away peacefully on April 4, 2101, at the age of 96, in the comfort of good health.

He earned his Bachelor’s degree from Cheng Kung University and went on to dedicate nearly 50 years to the field of science, making significant contributions to computer science. His work left a lasting impact on the advancement of technology and inspired countless individuals in the field.

A lover of adventure, Yi Cheng Lin achieved the remarkable feat of climbing Mount Everest twice, showcasing his determination and passion for exploration. Known for his kind and generous spirit, he was a person who consistently treated others with compassion and respect, leaving behind a legacy of warmth and humanity.

He will be fondly remembered for his dedication to his work, his adventurous spirit, and his unwavering kindness to all.

* Morris Chang: The founder of Taiwan's semiconductor industry. His contribute is very significant.
* Sir Edmund Percival Hillary: He was a New Zealand mountaineer and explorer. He and Sherpa mountaineer Tenzing Norgay became the first climbers confirmed to have reached the summit of Mount Everest.
* Both of them consist what they loved. They failed many times, but they didn't give up until they achieved the goal. 





