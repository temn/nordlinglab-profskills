This diary file is written by Ian Teng E14092047 in the course Professional skills for engineering the third industrial revolution.


# 2022-09-08
- The first lecture was little hard for me because my English is not really good.
- In this lecture I know how to work in this class and what I can learn from the class.
- I know the professor's background.
# 2022-09-15
- The second lecture has begun somebody's ppt report,and I feel a little nervous.
- Today,pofessor introduces the confict views,introduction to GIT and Markdown,and let us watch the TED video to make us more realize what professor said.
- I meet my report team mate,they are all good nice people.
# 2022-09-22
- Today,professor teach us how to use the bitbucket's diary,I think it is a little hard to use.
- I learn what is fake news and how to detect it in this class.
- my diary has some problem that make me a little blue that I can't upload my diary on time.
- I think in 10 years, I will be living in a small city in the United States, where I will work as a software engineer at a technology company.

One of the current trends that I think will affect me the most in the next 10 years is the increasing use of artificial intelligence and machine learning in various industries. I believe that this trend will continue to grow and will have a significant impact on my work as a software engineer. I think that I will be using AI and machine learning technologies in my daily work, and that these technologies will become even more advanced and sophisticated.

Another trend that I think will have a big impact on my life in the next 10 years is the rise of remote work and telecommuting. With the advancement of technology and the increasing popularity of remote work, I think that I will have the flexibility to work from anywhere as long as I have a stable internet connection. This trend will allow me to have more control over my work schedule and location, and will give me the opportunity to live in a place that I love while still being able to pursue my career.

A third trend that I think will affect me in the next 10 years is the growing importance of sustainability and environmental awareness. I believe that this trend will continue to gain momentum and that companies and individuals will place more emphasis on reducing their carbon footprint and living more sustainably. I think that I will be part of this trend by making conscious choices about my own environmental impact, and by working for a company that values sustainability.

A fourth trend that I think will have an impact on my life in the next 10 years is the increasing trend towards personalized and customized experiences. I believe that this trend will continue to grow, and that people will demand more personalized and customized products and services. As a software engineer, I think that I will be part of this trend by developing personalized and customized technologies that cater to the specific needs and preferences of individual users.

A fifth trend that I think will affect me in the next 10 years is the increasing use of virtual and augmented reality in various industries. I believe that this trend will continue to grow and that virtual and augmented reality will become more mainstream and widely adopted. As a software engineer, I think that I will be part of this trend by developing virtual and augmented reality technologies for various applications.

In summary, I think that in 10 years, I will be living in a small city in the United States and working as a software engineer at a technology company. The five current trends that I think will affect me the most and have the biggest impact on my life in the next 10 years are the increasing use of artificial intelligence and machine learning, the rise of remote work and telecommuting, the growing importance of sustainability and environmental awareness, the increasing trend towards personalized and customized experiences, and the increasing use of virtual and augmented reality.
# 2022-09-29
- I listen some group's report,I think they are so brave that they can use excellent English to tell what their idea about the fake news.
- I go to TA's office to solve my diary's problem,TA Peter is so nice and I play with other TA's cat,the cat is so cute!
- Professor introduce money,how does the financial system work and income distribution.
# 2022-10-06
- today is turn to our team's report ,my teammate is so great that I am proud for his behave.
- In the beginning of the class,professor rollcalls my diary suddenly,my diary because of some problems that I can't upload on time,I am so sorry to professor really!
- In the lecture,mentioned about if our teanger will vote when the election.
- frictional story:
  1.the tortoise and the hare,tell me the thing that even when you are behind other people,don't give up easily;and when you are in the lead,don't happy early because anybody all may pass you when you are slack.
  2.the golden ax,tell me honesty is a must,even at the expense of self-interest.
  3.a crow drinks water,the story tell me that use your smart to solve some problems you meet can make you easy and make your life more convenient.
# 2022-10-13
- In the lecture,I learn about the path to knowledge.
- I share the Newtown's second law to professor in English,for me,It is a big step to use English in the class.
- I listen about crazy friday from other team's report.
# 2022-10-20
- this week don't have lecture.
- this week is so busy that I have to prepare many tests on the next week.
- I feel little blue that I have too much subject need to study,I don't have enought time for study and sleep.
# 2022-10-27
- this week,the lecture is talk about depression.
- this week,one girl suicided in the girl's dormitory,who is someone I know in a activity,I feel a little sad for her death information.
- this week is Halloween week,I attend a special fair in this weekend and listen my favorite band's performance!
# 2022-11-03
- this week,the lecture continues the last week topic about depression.
- after many tests stuffed full my everyday,I finally take a rest for a week,but next week I will continue the life which is full of tests.
- I have done my best on the test,but some subjects' grades still so terrible that make me a liittle depressed and confused why I need to study so hard.
# 2022-11-10
- this week our team prepare the big presentation about what is green building and why we need green building?
- this week lecture we learn about how does the legal system work,it make me learn so much!
- from this week,I begins many of test,I have felt so tired.
# 2022-11-17
- this week ,my team prepare the presentation about noise restrictions in Taiwan and in Japan.
- this week, NBA super star-Howard come to Taiwan and join the T1's basketball team,he is very crazy in his first basketball game!
- next week,I will begin many tests....I feel so tired.
# 2022-11-24
- this week my team prepare the presentation about three actions to improve people life.
- this week lecture let me learn how to make sense of event.
- in the weekend I do the volunteer for the election,and take part in the invoicing process.
# 2022-12-01
- this week I learn the theory of planetary boundaries,it indicates some factors that may make the earth dangerous.
- I learn the WBS,PERT and Gantt chart to planning the timeline about our actions to make our environment more better!
# 2022-12-08
- this week my team do some interviews about our action which somebody have done it long time ago.
# 2022-12-15
- this week I learn about why the third industry revolution happened.
- this week I prepare the presentation with my teammate abot how to measure the productivity.
# 2022-12-16
- today I test the matlab for my Automatic control subjects！
# 2022-12-17
- this weekend Tainan's weather is so cold and rain all day,in the beginning I want to stay at home,but I still go out to discuss the presentation with my teammate.
# 2022-12-18
- today I go to the Tainan Library with my girlfriend to take a picture with Christmas tree and prepare my final exam of automatic control.
# 2022-12-19
- today I go to the NCKU library alone for my automatic control exam.
# 2022-12-20
- today,I had to work part-time and couldn't go to a pre-test reading session organized by my friend. What a pity!
# 2022-12-21
- today,I finally finished my automatic control final exam,I think the test is so hard that I may not get a perfect grades.
# 2022-12-22
- today,professor teaches us how to wear an interview suit and something important about the interview.I think this is a great benefit to the future senior push screening!
# 2022-12-23
- today,I joined the Christmas party to feel the joy of the coming Christmas.
# 2022-12-24
- today,I suddenly had an upset stomach and fever, and spent the whole day in bed, unfortunately Christmas Eve！
# 2022-12-25
- After a day of recuperation I finally recovered 70% to 80%, but still kept having diarrhea, but in order not to waste the rare Christmas, my girlfriend and I went to eat the underwater restaurant as Christmas dinner!
# 2022-12-26
- today,I took the composite material exam and the professor was very nice. It was very easy to write, so we finished it quickly!
# 2022-12-27
- taday,The night before, I memorized so many Christian bibles that I felt like I was going to be inspired by Jesus!
# 2022-12-28
- today,I took the Introduction to Machine Tools exam, and it was a bit difficult for me to read the book the night before, so I turned it in quickly, hoping that the final score would let me fly overhead.
# 2022-12-29
- today,our presentation team conducted final report presentation about the recycleable bottom!
# 2022-12-30
- today,Fortunately, on the advice of my friends in the other group, we installed levers on both motors so that the good motor could forcibly drive the broken motor to rotate and we managed to survive the crisis.
- in the night,I watch the movie- Let the bullets fly by the Netflix with my girlfriend.The movie Let the Bullets Fly is a 2010 action comedy film directed by Jiang Wen, and the first installment of the "Jiang Wen Northeastern trilogy". It is set in China during the 1920s, and tells the story of a bandit named Yang Tian-shan who is imprisoned in a small town controlled by bandits. Disguising himself as the new town mayor, Yang engages in a power struggle with the bandits. The film uses lighthearted and humorous storytelling to satirize the political and social situation in China during the early 20th century. It also portrays Yang as a clever, funny, and brave character who uses his wit and bravery to defeat the bandits, and sees through the hypocrisy and corruption in society, choosing the right path in life. Let the Bullets Fly was a hit in China and won numerous awards both domestically and internationally. It is not only an entertaining film, but also a meaningful work with profound significance.
# 2022-12-31
- taday is my birthday,My girlfriend and I celebrated our last day of 2022 with a birthday dinner at a famous restaurant and a birthday celebration at a friend's house!