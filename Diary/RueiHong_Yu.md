# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* I am a bit surprised by the fact of getting an exam at the first class
* It also improve our teamwork since group homework is given

# 2024-09-19 #

* It's the first time I know about git repository 
* I think it's quite complicated to use 
* The class today taught us about renewable energy 
* Some tips are given to improve our presentation 

# 2024-09-26 #
* This week we watched a lot of videos, the theme revolved around fake news, and what kind of harm fake news will bring to us.
* This week, we discussed the 17 Sustainable Development Goals (SDGs) and found signs of regression in quality education.
* I watched three videos on handling statistical data. The reporters mentioned that fake news easily spreads on social media platforms and suggested using AI to filter such information. These videos helped me understand the importance of statistical data.
* The videos mentioned three methods(1.Can you see uncertainty? 2.Can I see myself in the data? 3.How was the data collected?) that can help us identify flawed statistical data.

## Task 1: 5 current trends of change and how they affect my life in 10 years
* 1.Artificial intelligence
* 2.Remote work and Digital transformation
* 3.5G and future network infrastructure 
* 4.Internet of Things
* 5.Electrification of traffic

After ten years, by 2034, I hope that work in a cutting-edge technology company.I want to exchange idea and cooperate with
top experts from all around the world, and contribute to the advancement of our civilization. Even though Taiwan’s market now 
is focused on the semiconductor industry, I believe that in the next few years, artificial intelligence will undergo a period 
of rapid expansion and development. This growth in AI will not only revolutionize industries but also transform the way we live,
making it one of the most influential technologies of the next decade. The second major change is the rise of Remote work and 
Digital transformation. The global COVID-19 pandemic changed the way we work, live, and interact. Over the past few years, 
various solutions have been introduced, and remote work has become increasingly popular with everyone. In the field of education,
a new term has become widespread: “flipped classroom,” which integrates digital tools and internet learning environments. This 
method lets students engage with course material at their own pace, making the learning process more personalized and flexible. 
The deployment of 5G has already started to improve the speed of the internet, and as it becomes more widespread, we are closer
to the virtual work environments. The integration of 5G with VR and AR will let remote work  become realer, making virtual 
meetings feel just as real as face-to-face interactions. This will  enhance global collaboration and making  communication from 
country to country easier. Another main benefit of remote work is that we can save valuable time. For instance, eliminating the 
need to commute to an office could easily save an hour or more each day. This time could  be spent on more meaningful activities,
such as spending time with family, doing personal interests, or even studying. The efficiency and flexibility provided by remote 
work will likely become a base of future working environments. The fourth transformative change is the Internet of Things. The 
development of IoT technologies will bring us to the technological homes we have dreamed of. With everything connected, from 
household appliances to personal devices, IoT will allow us to control almost every aspect of our daily lives remotely and in 
real time. Whether it’s adjusting the thermostat, turning off the lights, or even managing security systems, IoT will make 
everything more efficient, safer, and convenient.Lastly, there will be the electrification of traffic. Over the years, 
transportation has evolved dramatically. A prime example is the train, which has progressed from steam engines to electric 
trains. Modern developments such as high-speed rail and metro systems have greatly improved efficiency and convenience in 
transportation. Electrification will take this a step further by making traffic smarter and more sustainable. Imagine a future 
where traffic jams are a thing of the past, journeys are quicker, and vehicles are more environmentally friendly. This shift 
in transportation will not only reduce travel time but also make commuting more accessible and reliable for people all over the 
world.
## Task 3: Different sources on: "Biden orders Pentagon to ‘adjust’ military as Israel launches Beirut attacks"
U.S. President Joe Biden has ordered the Pentagon to adjust its military presence in the Middle East in response 
to Israel’s airstrikes on Beirut. These airstrikes targeted Hezbollah’s headquarters in Lebanon, aiming at its leader 
Hassan Nasrallah. Israeli Prime Minister Benjamin Netanyahu stated at the United Nations General Assembly that Israel 
will continue to strike Hezbollah until its goals are achieved. The attacks resulted in the destruction of several 
buildings in the southern suburbs of Beirut and caused multiple civilian casualties.

* 1.U.S. Government:The Biden administration emphasizes that the military adjustments are meant to enhance deterrence 
and protect U.S. interests in the region. The White House stated that this decision is in response to the escalating 
regional tensions.

* 2.Israel:The Israeli government claims that the airstrikes are a response to threats from Hezbollah and stresses their 
right to self-defense. Netanyahu highlighted at the UN General Assembly that Israel will not tolerate threats from 
Hezbollah and Hamas.

* 3.Lebanon:Lebanon’s caretaker Prime Minister Najib Mikati condemned the Israeli attacks, stating that they demonstrate 
Israel’s disregard for international calls for a ceasefire. He called on the international community to intervene to 
prevent further escalation.

# 2024-10-17 #
* In today's class, I got a lot of information about currency and economics.
* The teacher asked everyone about thoughts on Bitcoin replacing real money.
* My answer is NO and I told the teacher that my answer was based on my intuition.
* I learned that there are so many factors that directly and indirectly affect the economy.
* I think what I learned today will enable me to apply it in the future.

# 2024-10-24 #
* This week, I watched two videos about the impact of exercise on the body and the healthcare system.
* Currently, healthcare systems focus on treating illnesses when they occur. However, it would likely be more effective to shift towards maintaining health in daily life. I think this approach would be difficult to implement, as the healthcare system may still face issues of demand exceeding supply.

## Three rules for how to live healthy
* 1.Balanced Nutrition
Testable Hypothesis: A balanced diet will improve immune function and energy levels.
Supporting Evidence: Studies show that diets high in fruits, vegetables, and lean proteins are linked to stronger immune responses and higher energy levels.
Contradicting Evidence: Some people may not see significant health changes due to genetic factors or pre-existing conditions that affect nutrient absorption.
Falsification: To falsify, one could analyze a group following a balanced diet who experience no improvement in energy or immune markers compared to those on a standard diet.
Knowledge: Nutrition science indicates that essential vitamins, minerals, and macronutrients play a role in immune health and energy metabolism.

* 2.Get Quality Sleep
Testable Hypothesis: Getting 7-9 hours of consistent sleep improves mental clarity, emotional balance, and overall physical health.
Supporting Evidence: Sleep research shows that consistent sleep improves cognitive function, mood, and overall health, as seen in various health and brain metrics.
Contradicting Evidence: Individuals with insomnia or sleep disorders may not experience these benefits, and factors like quality over quantity of sleep also matter.
Falsification: Test groups with varied sleep durations and compare cognitive and physical health outcomes, showing no improvement with increased sleep duration.
Knowledge: Neurological and sleep studies emphasize the brain’s need for rest for optimal functioning, mood stabilization, and health repair during sleep stages.

* 3.Fulfill purpose
Testable Hypothesis: People with a sense of purpose have higher well-being and life satisfaction than those without it.
Supporting Evidence: Studies link purpose with better mental health, resilience, and even lower mortality rates.
Contradicting Evidence: Some people report high life satisfaction without a defined purpose, and the pursuit of purpose can sometimes lead to stress.
Falsification: Compare well-being over time in groups with and without a strong sense of purpose. If those without purpose have similar or higher well-being, the hypothesis is challenged.
Knowledge: Research and theories like logotherapy suggest purpose helps people maintain positive mental health and motivation.