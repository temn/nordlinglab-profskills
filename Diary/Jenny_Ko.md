Jenny Ko 柯妤蓁 E14094154

# 2023-09-07  
Lecture 1  

- After listening to the introduction of this lecture, I have a better knowledge about what I am going to learn for this semester.  
- In the lecture I learned about a new idea, the world of abundance.  
	- I think that the limitation of resourses may stop this from happening (at least for some fields).  
	- However, I do believe that as the technology develops, more and more people in the world could have a better life.  

# 2023-09-14
Lecture 2 

- I learned about conflicts this week.   
	- Way before this class, I actually learned about the win-win, win-lose, and lose-lose ideas when I read the book "The 7 Habits of Highly Effective People" by Steven Covey.  
	- I was inspired by the idea of win-win when I first learned about it.   
	- I think more people should acknowledge the idea of win-win.  
		- When people only have win-lose in mind, it is quite often that at least one side will lose and even worse, both sides lose.   
		- On the other hand, when at least one of the people has win-win in mind, it is more likely that all of the people acknowledge that win-win is one of the options.   
			
# 2023-09-21
Lecture 3  

## HW1: Summary of what I think my future would be like after 10 years and five current trends of change
In a decade, as I step into the year 2033 at the age of 31, my life will have undergone significant changes and transformations. This glimpse into my future reveals a life deeply influenced by both personal aspirations and global trends.

First and foremost, I envision myself residing in Taichung, a vibrant city in Taiwan. This choice is driven by my desire to maintain close ties with my family, ensuring that I can visit them frequently and share precious moments together. Taichung, with its blend of modernity and tradition, provides an ideal backdrop for my future endeavors.

Professionally, I will have embarked on a rewarding career as an engineer. Having completed my master's degree and gained valuable experience over the past five to six years, I'll be in a position where I can contribute significantly to projects that excite and challenge me. Importantly, I aim to strike a balance between my career and personal well-being, favoring job roles that promote a healthy work-life equilibrium.

One of the defining aspects of my future career will be its international dimension. I envision collaborating with professionals from diverse cultural backgrounds, making it an enriching experience. Moreover, my job will afford me opportunities to undertake business trips to various countries. These journeys will not only allow me to apply my skills on a global scale but also broaden my horizons through interactions with individuals from different walks of life.

Five Current Trends of Change:  

1. Technology Advancements:  
	The technology develops exponentially. As an engineer, this development could highly affect my career. Work and project cooperation using artificial intelligence, automation, and other emerging technologies will be the norm. These changes to the working environment will change me to continuously upskill and adapt to stay at the top of my field.    
	
2. Sustainability:  
	Human beings have started to realize their damage to the enviornment and gradually value sustainability. As an engineer, incorporating sustainability to my work is a must in the future. It is clear that prioritizing eco-friendly solutions and minimizing environmental harm will be central to engineering practices in the future.   
	
3. Globalization:   
	As technology advances, our world is getting more closely connected. This brings both chances and difficulties. Because of globalization, I might have to collaborate with people from various places. Working with people who have diverse cultures and backgrounds can boost creativity at work. However, to make this happen, it's crucial to be skilled in cross-cultural communication and understanding different languages.

4. Health and Well-being:  
	Taking care of our health is becoming more important in our society, and this is something I value too. As I think about the future, I know that making my physical and mental health a priority will be really important in shaping how I live and the choices I make. I also realize that being healthy isn't just about me personally; it's also a big part of being successful in my career. 

5. Global Warming:  
	Global warming, the increase in Earth's temperature, is a major issue that needs our attention. It affects our planet and all of us. Looking ahead, I feel a sense of responsibility to do what I can. In my career, I'll work towards sustainable solutions like using less energy and creating eco-friendly designs. Outside of work, I'll make small changes in my daily life, like using public transport and reducing waste, to help fight global warming. 


## HW2: Summarize different views of a news story
- Topic: Russian official says Ukraine launches a missile strike on headquarters of Russia's Black Sea Fleet  
- Link: https://ground.news/article/russian-official-says-ukraine-launches-a-missile-strike-on-headquarters-of-russias-black-sea-fleet_08a9f1

	### Center View
	- Ukraine was hinted at behind as the attacker of the Russian Black Sea Fleet headquarters, causing a Russian soldier missing.  
	- The attack on the headquarters happened a day after the Russian attack on Ukraine cities, causing the death of five people at minimum.  
	- Tension between Ukraine and Russia has escalated after Russia has threatened to block the exporting routes of Ukraine.  
	### Left View
	- A missile attacked the headquarters of Russia's Black Sea Fleet by an unknown enemy.  
	- The Ukrainian officials held Ukraine responsible for the missile strike, while Russian official Mikhail Razvozhayev acknowledged the attack but did not attribute blame.  
	### Right View 
	- Ukraine attacked Russia's Black Sea Fleet headquarters with a missile strike resulting in one service man dying.  
	- Russia has launched another attack on cities in Ukraine, which comes after their previous missile strikes.  
	- Recently, authorities installed by Russia have accused Ukraine of attacking a crucial shipyard in Sevastopol.  

# 2023-10-12
Lecture 4  

- The lecture is mainly about the financial system.  
- I was super confused when the professor was introducing what money is.  
    - Expecially when the professor mentions M1A, M2, and M1B.  
	- I think this is because I am not familiar with the financial field and also those names.  
- I also had a hard time understanding the video the professor showed us.  
	- This is probably also because I am familiar with the field and I start to feel sleepy while watching it.  
- It is interesting to know that money nowadays is not based on anything. I had always thought that it was based on gold.  
- I think the last video the professor showed us is more understandable.  
	- I have a better vision of how the economy works.  
- I could totally feel the decreasing purchasing power of the New Taiwan Dollar. Things are getting more and more expensive these days.   

# 2023-10-19
Lecture 5  

- This lecture is about why extremism flourish.  
- When people have trouble with defining their own identity or purpose, they may become extremist more easily.  
	- When everything sucks and you feel lost, you feel better when you blame others (even the ones that you never met or barely know anything about).
	- Therefore, I think that it is important to check on my family members or my friends from time to time to ensure they feel secure and have a place to belong. 
- I think the idea of "imagined reality" by Harari is quite interesting.
	- It had never come to mind that a lot of things that are highly related to me in life are not "objectively" real.	

# 2023-10-26
Lecture 6  

- This lecture is about health.  
- I love the speech about the brain changing benefits of exercise.  
	- I think that the speaker has a sense of humor.  
	- I have also know that exercise is good for our health; however, I didn't how. This video provided me some explainations, which actually ecourages me to do exercise.  
	- Standing up and do some excerise was quite fun.   
- For the other video, I found the idea of health system and sick system is quite interesting.  
	- According to the speaker, the system we current have is a sick system, an uneffective system.   
		- The system does not keep people healthy but only heal them when they got sick.   
	- I think that the health subsrciption method may be a system changing idea. However, I think that it may be difficult to really apply this system in real life.   
		- Even though the health of each person will be tracked and health advices are provided, it is difficult to grantee that people will fully follow the advices. Therefore, people will still get sick eventually and doctor will need to cure them but not earning any money.   

# 2023-11-02
Lecture 7  

- This lecture is about depression.  
- I feel that mental health became more and more important nowadays.  
- In Nikki Webber Allen's speech, I love the sentence, "having feeling isn't a sign of weakness".
	- Sometimes I feel that most people would only show their happiness but not their sadness to the others.  
	- I think holding in feelings may be a problem when everything piles up and result in a sudden burst of emotions.  
- Listening to the person might be the best way to help he or she during his or her depression.  
	- However, I felt that I am not being helpful when I only listen or stay with my friend when she is not feeling good.
	- I learned from the professor today that I don't need to too pressured when I am unable to help. I could just do what I can do for help.

# 2023-11-09
Lecture 8

- I have always been irrational during decision making. For most of the decision I made (even the important ones), I did not think thoroughly or even gather enough information. Moreover, I clearly notice this point every time I made a decision. I am not sure why I contiuously do this.  
- I do not think I am able to forgive anyone that killed my be loved ones. I think at least it would take me a lot of time to do that.  
- I have always been wondering what is the meaning or purpose of life since I was little.  
	- I got this suggestion online for finding the purpose of life is to ask yourself the following two questions. "Why am I alive?" and "For what would I be willing to die?"  
		- I still don't have answer for those question. 
		- I think I am still pretty lost.
		
# 2023-11-16
Lecture 9

- After the presentation, there wasn't much time for class. Professor did not teach much this lecture.  
- I noticed that I misunderstood what professor ment about the big group's project. I thought the topic for presentation was only for two week, but it is actually the topic for the semseter project.
- Next few weeks are midterm weeks and I feel that I am a little overwhelmed by everything I need to do. I started to go to bed late recently. However, after listening to the presentation about sleep, I notice improtance of sleep.  
	- Without enough sleep I wouldn't be able to concentrate and learning would be ineffective. I would be wasting time studying, but not remembering what I had learned. This would be really awful.   
	- Therefore, I should better set a goal about going to bed early to keep myself productive and healthy.
		- However, sometimes I would have a hard time falling asleep. I am really sensitive to sound when I am sleeping, but I could hear the sound coming from the next door or outside of my room. This is quite bothering.
		
# 2023-11-23
Lecture 10  

- Some thoughts for the video "Simon Sinek on millennials in the workplace".  
	- I think that the speaker has a lot of interesting view points.  
		- The speaker mentioned that the millennials have some negative labels on them. However, these labels are mainly not their fault. They act like this because of their education and the parenting they got. 
		- I feel that what he mentioned could also be applied on anyone that grew up along with the internet, phone, etc.
		- I think I am impatient. The reason could be what the speaker mentioned. The reason is that you could get basically everything in a very short period of time in comparision to the past; therefore, people nowadays may lack the practice of waiting. This then turn into feeling impatient easliy.
		- Another thing is that crumbs of time are given away to unproductive distractions. A lot of people have the habit of picking up their phone quiet frequently even if it is unnecessary.  
			- This habit not of picking up the phone not only distract people from what they are doing but also take away their crumbs of time. 
			- This crumbs of time could add up and actually stands for a large percenatage of time in a day. 
			- Moreover, this habit also reduce the time for people to wonder. Wondering is important for people to be creative and come up with new solutions and thoughts for life.

# 2023-11-30
Lecture 11

- This lecture is mainly the presentations of the large groups.  
- Lifecycle of civilisations
	- There are a couple of dark age of human population in the largest city in the world. Professor mentioned that there may be another dark age in the future by observing the cycle. However, I am wondering that if large people starts to move out of the cities and move the suburbs, is this also considered as dark age? Does dark age means that there is a significant reduction in the human population in the city or does it means that there is a signifincant reduction in the population of the whole human population.
	- If the dark age is really going to come, what causes it conserns me. It might be climate change due to global warming, polluted Earth, or other catastrophic natural disaster.   
- How to choose your news
	- Due to the internet and all the other technologies, the information one can get instantly is far more than an individual could process. Moreover, fake news and information have become a new problem nowadays. People should aquire the ability to notice fake news and information. There are some techniques that one could follow to spot fake news and information. However, some occasions may require basic or indepth knowledge about specific field. Without having the knowledge, it would be difficult to distigush fake information from the real ones. However, it is not practical to have a indepth learning to some fields in order to double check every information we recieve every day. I was told that this problem could be solved by view the comments of the trusted experts in those field.   
- The Cellphone Experiment  
	-  During the whole experiment, I wanted to pick up my phone 5 times. I think this is lower than a lot of my classmates. However, I think this is because I don't have the habit of frequently picking up phone during class and I also have my ipad in hand. I think that if the experiment was held during the break time and I don't have my ipad, I would definately have a higher chance to frequently have the desire to pick up my phone. Actually, I do considered myself as cellphone addicted. I think I spent way too much time on my phone and the time was not spent wisely. I know that using phone was generally fine when I need a break from work but the time I spend on the phone is just way too much. This semester I was trying to reduce the screentime of my phone. At first, the result seems quite. However, as the work I need to do everyday increases, my stress raises too. I started to spend too much time on the phone again in order to release my stress. I think this is a vicious cycle. Since I spend more time on the phone, I would need to go to bed late in order to finish everything I need to do that day. Sadly, going to bed late would highly affect my performance for the next day. This will than result in stress. I really should stop myself from using the phone.
	
# 2023-12-7
Lecture 12

- This lecture is about the planetary boundaries  
- Planetary boundaries are the limits to the impacts of human activities on the Earth system.  
- Professor spent a lot of time discussing the climate change in class (especially the temperature part).
	- I think that whether the rapid rise of temperature is normal phenomenon on Earth is not the most important point. 
	- I think the part that people should really look into is whether human beings are able to survive if the temperature keep rising. Even if the rapid rise of temperature is a normal phenonenon, human could still die if the temperature or the enviornment is not suitable for human. 
	- Moreover, I have also been wondering why are people only focus on the reduction in the greenhouse gas emission when dealing with climate change. Wouldn't there be any other things that also causes the temperature on Earth to rise? Wouldn't there be other thing that we could do to cool down the Earth?
- Most of the plantary boundaries have already been exceeded. 
	- Sometimes I feel real powerless when facing these kind of issue since there seems to be a lot of envirnmental problems these days.
	- Luckly, we did pull the ozone depletion back into the boundary.

# 2023-12-14
Lecture 13

- This lecture is also mainly the presentations of the large groups.  
	- Our group have really similar action with one of the other group. I am not sure if we could keep our action the same. However, I think that this may mean that the action that we are planning to do are quite important since the other gourp also value this. Moreover, both of the group have been voted to be the importants ones.  
- Professor tested different voting system with the same topic. The result of differnt volting system is slightly different.  
	- This shows that people actually act differently according to different volting system.  
	- The reason of this may be becasuse that people are more willing to reveal their true self when the voting is anonymous.  
	- Even though there are difference between to voting result, the difference are not very big. Being anonymous does not seem to affect the result a lot on this topic.
		- Actually, I feel that there are more extreme speech online when the speaker is anonymous. Even though being anonymous could encourage people to say anything they want, it may be very problematic when people start to hurt othes with words. I do think that this is a problem that we need to resolve nowadays.   
- We are required to watch a roughly one hour and 45 minutes video this week. The presenation we need to do next week will need the imforamtion in this video. I think that the video is way too long. Recently, I am really stressed with everything I need to do. I think I am a little overwhelmed. I don't understand why are there so many thing to do even though it is currently not exam week. Luckly, this semester is about to end. There is only a few weeks end. It may be a torture in the last few week but I am still happy that the winter break is coming.  

# 2023-12-21
Lecture 14

- This lecture is about industrial revolution and how to suceed.  
- I read the book "7 habits of highly effective people" a few years ago.
	- I really like the ideas in the book. 
	- I wrote my mission statement back then and I would update it from time to time but I didn't really follow it.
	- I really like the idea of circle of control and circle of concern in the book. 
		- I think this is one of the few ideas from the book that I really try to implant it into my life.  
		
## Diary for each day
2023-12-21 Thursday    
After the whole day of class, I went to work on a project I had in another course. My teamates and I had alrealy spent a lot of time on it but the process is really slow. We had a hard time debugging the whole system. I think I spent way too much time on this project than what I had expected. I started to study for the final exams next week after I went back to dorm. I would consider today as unsuccessful and partially productive.

2023-12-22 Friday  
I went home today. I don't feel like doing anything after I went home. I tried to study for the exams next week but I am really unproductive. Today is unsuccessful and unproductive.

2023-12-23 Saturday  
I worked on studying for the exams next week and everything is going fine. However, I was a little lost for one of the subject since I don't really know how to study it. Today is partially successful and productive.

2023-12-24 Sunday  
I took the train back to Tainan today. Sadly, I forgot that it is Christmas tomorrow and there are a lot of people everywhere. I didn't catch the train. I ran to the platform and saw the train just run past me. I could have board on the train if I was few minutes faster. Luckily, I was still able to take other train back to Tainan. I studied on the train and I was unexpectively productive, which was really nice. I would consider today as unsuccessful but productive. 

2023-12-25 Monday  
I took two final exam today. For the first exam, there are some question in the exam that the professor didn't cover in class. I was really lost that time. Luckily, it was an open book exam so I could double check that the question is really not covered in class. For the second exam, I was really lost before I took the exam. However, when I took the exam, Ii feel relieved because it was not as difficult as I thought. I also work on the project today. The motor died. We don't know why. I would consider today as partially successful and productive. 

2023-12-26 Tuesday  
I should be studying for the closed book exam tomorrow; however, I start to read comic and went to bed really late. This is really bad. I really should go to bed early to keep my brain well functioning for the exam the next day. I would consider today and unsucessful and unproductive. 

2023-12-26 Wednesday  
I was a little late the exam because my electric water hearter has a little problem this morning and I spent some time trying to fix it. Luckily, I did manage to finish the exam on time. In the afternoon and night, I was working on my project with my team. We really spent way too much time than what we've expected. Only for today, we spent nine hour working on it. I was so tired but the whole thing did work. I hope it will still work properly when we present it to the professor tomorrow. I would consider today as successful but unproductive. 

## Five Rules for my Success and Productivity
1. Utilize the urgent and importance matrix
2. The idea of circle of control and circle of concern
3. Plan my day and week beforehand
4. Utilize SMART goal (specific, measureable, achieveable, realistic, time-bound)
5. Make it easy

# 2023-12-28
Lecture 15 

- Today is also maily about the big group's presenation  
- Videos we watched during class  
	- This farm of the futrue uses no soil and 95% less water
		- I think I heard of this type of farmming may years ago
		- It is really great that this type of farming could redue the water used and aviod to harm the land and soil
		- Agriculture actually is not that enviornmental friendly  
			- It pollutes the land and some of the lands could not be used afterwards, which is quite awlful
	- What if we replaced politicians with randomly selected people
		- Before watching the video, I thought this is a joke; however, the speaker is serious about this
		- I had never thought that randomly selecting people to work as the representatives is one possibility. It's quite interesting to be honest.
		- However, I do think that if these ramdonly selected people should go to classes for critical thinking and more in order to be one of the representatives. 

## Diary for each day
2023-12-28 Thursday
Our group presented our term project for the automatic control course today. Luckily, everything works fine today. However, after working on the project for so many days, I am really tired. Therefore, I decided to take a break and not study for today. Today is successful but not productive.

2023-12-29 Friday
I went home today. I also feel more relax when I go home. I really wanted to take more rest today; however, I have finals next week and I need to study these subjects. I worked on one of the final report I have to do for another class for the whole night but I was only half way through. I had a hard time looking for the data and analysing them. Today is unsuccessful and unproductive.

2023-12-30 Saturday
I also work on that final report today. I took way more time than I've expected. Moreover, I felt exhaulsted after finishing it. Therefore, I stop working on anything else afterward. However, I didn't really have a good rest because there is soo many subjects to study and I haven't start working on them. The thought that I don't have enough time kept bothers me. Today is unsuccessful and unproductive.

2023-12-31 Sunday
I solely studed for the electronics today. Even so, I still didn't manage to cover all chapters. Plus, its new years eve today and the fact that I cannot rest and still need to study annoys me. Today is unsuccessful and unproductive.

2024-01-01 Moday
Happy new year! I still have to study for the finals. Today is unsuccessful and a little more productive.

2024-01-02 Tuesday
I finshed two exams today! I think I did pretty well on both of them. However, I haven't start studying the finals for the next few days. I don't have enough time and I don't want to stay up late at night to study. I was more focused when I am studying today. Today is successful and productive.

2024-01-03 Wednesday
I don't have any class today so I stayed in the dorm for the whole day and study for the final. I still don't have enough time to study everything!!! Automatic control and fluid mechanics are soooo difficult!! I am running out of time. I need more time. I am even more focused today. Today is not really successful but productive.
