This diary file is written by Mark Liu E14096318 in the course Professional skills for engineering the third industrial revolution.
# 2022-09-15 #
* Submit my information on the Excel to finish the course registration.
* Know that I have a conflict view.
* Learning distributed version control system called Git.
* Learning how to write what I'm typing on this website, Bitbucket.
* Realizing how to get the scores by the presentation homework.
* Realizing other students' excellent expression ability by listening to their presentation.
* Learning the disruptive innovation and exponential growth.
* Ted：people largely agree on what goes into human well-being: life, health, sustenance, prosperity, peace, freedom, safety, knowledge, leisure, happiness. All of these things can be measured. If they have improved over time, that, I submit, is progress.
* Life is better than death, health is better than sickness, abundance is better than want, freedom is better than coercion, happiness is better than suffering and knowledge is better than ignorance and superstition.
* This course is the first EMI I have ever take, it's kind of cool and challenging.
* I have a hard time understanding the content of the course due to poor English, but I'll do my best.
# 2022-09-22 #
* Progress towards the 17 SDGs
* 3 ways to spot a bad statistic(Can you see uncertainty？Can I see myself in the data？How was the data collected？)
* A cool way to show stats by time.
* Citing articles.
* Ways we stop from the fake news.
* Task1：What I think my future will look like 10 years from now.
* Ten years ago, I was a ten-year-old without iPhone but a nokia3310, waiting for the end of the world that was not happened. A decade can change a lot of things. I think the years from now, it will be very improving but also horrifying.
* Even so, I don’t believe that Elon Musk, who is called the Iron Man in the real world, will take us to the upper space, like Mars, since he will run away alone. I think it is possible that I will still live on this small island. As my dream job, teacher should be still an irreplaceable occupation, because in my own opinion, if we are taught being a human being not by the spices ourselves, we are no longer called humans. The human nature is the reason why teacher is an important occupation.
* However, as a saying goes, “there is nothing permanent except change.” Ten years of improving of technologies and the changing of the environment where we live in do cause great impact on our daily live. Here are the five current trends of change that I think will affect me the most in the future. 
* 1.The first one is the great advancement of science and technology. The rapidity of accumulation of knowledge will enforce me to learn more. And it is hard but important to teach in systematical ways.
* 2.The few past years are the most severe and sorrow era for the teachers and students. Covid 19 cruelly stopped them from the class in person, which deprived students of their right to education. Even though the medical and sanitation advances, it is possible for us to confront this kind of catastrophic disease. 
* 3.Population aging refers to advances in health care. The longevity of the elderly leads to an increase in the average age of the population. Fewer children lead to lower numbers of young people. The population growth is slowing down which brings problem like shortage of labor force and students.
* 4.Modern children are already in the era that everyone must have a mobile phone in their hands. They are more focusing on the on-line games on their phones. Teaching with advanced digital technology can not only increase students' willingness to learn, but also overcome difficulties such asynchronous online learning.
* 5.The environment is slowly being destroyed with the advancement of science and technology. Since nature is also a natural classroom for students. We should protect our planet while technology is developing.
* Task3：the 'Little Mermaid' Live Action Trailer
* The Little Mermaid hit 1.5 million dislikes on YouTube.
* Excitement of little Black children seeing themselves reflected in a Disney princess.
# 2022-09-29 #
* The Blue Marble taken by Apollo 17
* Money
* Survey on financial knowledge which I can hardly finish it.
* How the financial system works
# 2022-10-06 #
* From other team's presentation of homework I learn that lob scales are better linear scales because it tells more details.
* Good people being silent causes people bad.
* Democracy is not rational, it just acts according to feeling controlled by government.
* However the impact of abandoning democracy make us scared.
* Attending physical class needs us to pay more attention to understand.
# 2022-10-13 #
* I think knowledge is the things that prevents us from death.
* Plato said: ”Knowledge is a well-justified true belief."
* The path to knowledge：Claim -> Testable hypothesis -> Supporting evidence -> Knowledge
* Wendy Suzuki is a woman who has no social life, but could force the audience to do some embarrassing exercises.
* Claim：exercise is the most transformative thing that you can do for your brain today.
* Claim：Sick-care -> Health-care：Treating people once they have become sick to preserving the health of the healthy before they get sick. 
# 2022-10-27 #
* Online class again
* Supergroup presentation
* Skip the part about depression
# 2022-11-03 #
* Read my own diary
* Show the results of depression survey last week
* Recommendations to spot and help a depressed friend.
* New hire success factors:1.Non-work relationships 2.Collaboration 3.Beyond the ‘call of duty’
# 2022-11-10 #
* Group 1,2,3,4,11 presentation
* watch videos on moodle
# 2022-11-17 #
* Confession of not watching the video on the last task
* Presentation for CO2
* Presentation for Rights and limitations to demonstrate
* Presentation for Noise Limits
* Ted talk and two more videos
* Consensus discussion
# 2022-11-24 #
* Group 5,6,7,8,9,10 presentation
* Cellphone confiscated
* No miss one time,but miss two important calls
* watch videos
# 2022-12-01 #
* A new TA
* Presentation
* Discussion
# 2022-12-08 #
* impact of individual actions that we can do like 
* Not having a child has the most amount of impact
* group presentation
* one member of each group share their own action and foundation
* every of the rest of us asking questions
* voting
# 2022-12-15 #
* presentation
# 2022-12-16 # unsuccessful
* watch a bad movie Avatar
# 2022-12-17 # successful
* predict the right score of a soccer game
# 2022-12-18 # successful
* watch Messi as a legend
# 2022-12-19 # unsuccessful
* have the worst presentation of my life
# 2022-12-20 # unsuccessful
* procrastinate studying for final exam
# 2022-12-21 # unsuccessful
* study for final exam
# 2022-12-22 #
* diary
* presentation
* we need to do our final project
* tie our tie
1. Write in your course diary how successful and productive you felt each day.
# 2022-12-23 #
* A. unsuccessful and productive
* B. unable to celebrate my mother's birthday
* C. celebrate during new year
# 2022-12-24 #
* A. successful and unproductive.
* B. lazy on bed, watch a great movie
* C. start to study for the final exams
# 2022-12-25 #
* A. unsuccessful and productive.
* B. lonely in a holiday
* C. have a girl friend 
# 2022-12-26 #
* A. successful productive
* B. done two final exams well
* C. start to study for the rest of the final exams
# 2022-12-27 #
* A. unsuccessful and unproductive.
* B. lazy watching useless videos on youtube
* C. start to study for the rest of the final exams
# 2022-12-28 #
* A. unsuccessful and unproductive.
* B. fail on one of the final exams 
* C. start to study for the rest of the final exams
# 2022-12-29 #
* last two class and last three day
* so tired
* presentation but no time for our teammate
# 2023-01-05 #
* last class and new year
* Final exam 94/100 successful and productive
* so many exam and project
* presentation
* Ted talks