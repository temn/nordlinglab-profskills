# 2022-09-08 #
* This is my first time to attend full English course, and this is harder than I thought. Most of the words that teacher said are so difficult.
* I am glad to join this class so I can improve my English skills.

# 2022-09-15 #
* Before the class, I am nervous about the presentation. Because I am the speaker of my group. After 5 groups had given the presentation, I am being more nervous. Because they are so good at speaking. 
* My group members are all good person. We have a good discussion at D24. 
* This class is my first time to hear GIT. But it's hard to figure it out how to use it.

# 2022-09-22 #
* In ten years from now I will still live in Taiwan, but I will move to Taipei. Because Taipei is my hometown. My family and most of my friends are all live in there. I may work as an engineer of making games which is my dream.

* After I graduate from NCKU. I want to be a master in NTU, which is the best school in Taiwan. There are better teachers and environment. I can learn more I want in there, and I would get better resume and salary after graduation. NTU is in Taipei, which means I need to move to Taipei. My family live in Taipei too, that means I can live at home. I don’t need to rent a house and pay a lot of money. It’s good for me because I am not very rich.

* My family live in Taipei, and work there. I don’t want to leave them because I love my family. We can go out to work at morning and go back home at night. Then take a sit next to dining table and eat dinner together. It’s a wonderful life. Also, we can drive car to other cities. Traveling around Taiwan and take a break from work in holiday is a good way to relax.

* Due to the developed traffic of Taipei, it will be my best place of residence in Taiwan. There is public transport everywhere, such as bus, MRT. They spread like spider web in Taipei, so I can go everywhere I want without riding a motorcycle or driving a car. There are a lot of convenient stores too. It’s my favorite because I can eat my meal very soon and there are lots of choice. Convenient stores also have some daily necessities. 

* There are many job opportunities in Taipei. Although the price is high in Taipei, the salary is high too. I think I would work in Taipei until I become old. I would go to Tainan to spend the rest of my life.

* With the development of technology, more people are starting to work on the high-tech industry, so the competition will be more intense. If I want to improve myself. I thought I need to live in Taipei. Because Taipei is the capital of Taiwan, which means Taipei is the most advanced city. Living in Taipei would let me keep up with the trend of the times.


* Left
* party would reverse Tory tax cuts for the wealthiest after Chancellor Kwasi Kwarteng announced plans to axe the 45p income tax rate in his mini-Budget on Friday.

* Center
* BBC news claims that this policy is failed. but they describe facts objectively.

* Right
* Because Labour government would introduce a minimum wage hike to £15, they thought it's good.

# 2022-09-29 #
* I am uncomfortable today, I guess I get COVID-19, but after I quick sieve, nothing happened, maybe I just get cold.
* My group members are all drop out in this course, that means I need to do my presentation all by myself, I am so sad.

# 2022-10-06 #
* Today is the first time to attend Classes in person, and it's my first time to see the teacher, I am surprised by the height of teacher.
* I am nervous about present in front of the class, because my English is very poor, I am afraid that I don't know what to say and just standing there.

# 2022-10-13 #
* What is knowledge? I think knowledge is power. We can't do anything without knowledge. with knowledge, we are human, not animal.
* We use Newton's laws to answer the question, but it is wrong which was showed by Einstein. Why we still use Newton's law? Because it is more common in real life.
* Nordling said that we didn't think what is wrong and what is correct, but just accept all things which teacher taught. I think we should fix this bad habit.

# 2022-10-27 #
* Today's topic is depression, I know a girl suicide last week, so I exit the google meet when professor talk about depression.
* I finally have teammate! I was alone last three weeks because my teemmate were all drop out this course.
* My new teammate are so great that we finish our group presentation very soon.

# 2022-11-03 #
* Today we talked about depression, If my friend have depression , I think I would take him or her to drink alcohol because once him or her get drunk, They could express their feeling easily.
* We have a supergroup this week, and this presentation is more difficult than before.

# 2022-11-10 #
* Today's group project is hard, we discuss about 3 hours.
* I went back to Taipei this week to see my family, I miss them so much.
* I watch a new movie this saturday, it was very great!

# 2022-11-17 #
* I have two exams this week, so I study hard to get good grade.
* We have a supergroup discussion this week again, and we choose reduction of Co2 of our topic, and we only use 2 hours to discussion.

# 2022-12-01 #
* Today I saw professor's son, he is so cute!
* Today was our group last presentation, I will miss our group members so much, they are so kind!

# 2022-12-08 #
* Today we discuss the media,Some media will add non-objective comments to increase the number of viewers,I think it's wrong.
* I watched a lot of world football games this week,and I hope Argentina can win the championship.

# 2022-12-22 #
* Every supergroup needs to show a live presentation, not the video anymore, and I can finally see what my teammate looks like!
* (A) Unsuccessful and unproductive
* (B) We need to wear formal clothes to look successful, but I forgot to do so, so I felt I am bad and unsuccessful.
*     I have a fluid mechanics test tomorrow, but I am tired because I have a electronics exam today , I don't want to study anymore, so I just playing games to relex and study a little bit, I am unproductive.
* (C) I will make a memo before I sleep, when I woke up, I can see my memo to remind me of what I should do today, and I will plan a schedule to decide when to do things so that I can make good use of my time.

# 2022-12-23 #
* (A) Unsuccessful and productive
* (B) I was very confident in my fluid mechanics quiz and I thought I'll get it all right, so I thought I was successful, and I skipped class to eat lunch with my classmates, but the teacher suddenly roll called, unfortunely, I was not there, so I feel I am unsuccessful.
*     Because there were only two classes today, I can plan my time well, I woke up early to review lecture notes for the fluid mechanics quiz, and I learned guitar for 2 hours through youtube, also studyed for 4 hours for the next final exam, I am so productive today!
* (C) I shouldn't have skipped class because I was lazy, it got me punished too, I won't skip class next time.

# 2022-12-24 #
* (A) Successful and productive
* (B) I went to the cafe and study with my friends there, to prepare the final exam, I studied really hard, when I felt tired because I studied for about 5 hours, the clerk treat me to a free cup of coffee because I'm a regular at the store, so I think I am Successful and productive!
* (C) I think I don't need to change anything. 

# 2022-12-25 #
* (A) Successful and unproductive
* (B) I went out with my friend to see music performance for Christmas and went to the Christmas market, I was so happy beacuse the music was fantastic and the market was awesome, It's a very very good Christmas!, but I did't study today, I am Successful and unproductive.
* (C) I should study harder next time.

# 2022-12-26 #
* (A) Successful and productive
* (B) I got two final exam today, after the exam, I felt I did a good job on the exam, maybe I could got 80 scores on both exam! and I also study for the next final exam for tomorrow, I'm well prepared for the final exam! I am Successful and productive.
* (C) I think I need don't to change anything. 

# 2022-12-27 #
* (A) Unsuccessful and unproductive
* (B) I have a final exam in psychology, I did not very well on the exam, I forgot lots of thing to answer the questions, I felt very sad about this exam because I thought I am well prepared, but I didn't, so I didn't want to study anymore, I only study 1 hours for the next test, I am Unsuccessful and unproductive.
* (C) I should prepare more harder before the exam, and I need to learn how to adjust my mood.

# 2022-12-28 #
* (A) Successful and unproductive
* (B) I know my 12/26 final exam grade! It's pretty good, but I still have lots of mistake of careless, or I could get better grade, I felt so regretful, and I didn't study for a second because I don't have a test or exam next day, so I didn't have any power to study, I just played game for a night. I am Successful and unproductive.
* (C) I should keep studying whether there is an exam tomorrow or not.

# 2023-1-5 #
* I have lots of exam this week, I am so tired.