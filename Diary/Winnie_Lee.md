This diary file is written by Winnie Lee H34095318 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #

* Not yet enrolled to this course.

# 2022-09-15 #

* My first lecture in this class. it was great
* It's more easier for me to understand the lecture since everything is taught in english.
* It was great watching presentations of my fellow classmate.

# 2022-09-22 #

* I was a bit distracted at the beginning of the class because i was sleepy
* The professor explained about how covid-19 affects the sustainable development goals
* Poverty increased during the covid-19 pandemic.
* Watched Hans Rosling's Ted Talks "The best stats you've ever seen"

# 2022-09-29 #
* Today's class is about financial system.
* Participated in a survey about financial knowledge.
* the amount of cash printed in Taiwan increase significantly every once a year during chinese new year.
* Watched Richard Werner's presentation "Today�s Source of Money Creation" on youtube

# 2022-09-26 #
* Today's the first time we have physical class.
* The professor explain about economic situation of several countries
* Watched Ted talk about facism.'

# 2022-10-13 #
* My Presentation video got played in the class.
* Exercise help to improves your brain.

# 2022-10-27 #
* We have online class today
* We were devided into supergroup
* My group was a bit quiet

# 2022-11-03 #
* Today's class is about depression
* Take care of yourself before you take care of others
* I was a bit busy today because i have exam tomorrow

# 2022-11-10 #
* Excited for tomorrow's vacation
* We watched video about law

# 2022-11-17 #
* We watched presentations made about laws
* I have so many exams coming for next week
* I hope i can handle it

# 2022-11-24 #
* The professor ask us to collect our phone
* I'm realized that i'm a bit addicted to my phone
* Still lots of exam coming next week

# 2022-12-01 #
* My presentatiom video was played in the class
* We were divided into big groups
* The task for next week is a little bit difficult

# 2022-12-08 #
* I got a lot of group project this week
* We had small debate about each group's action ideas

# 2022-12-15 #
* We watched group presentation videos about 3rd industrial revolution
* The topic about inequality is interesting
* We had small group debate

# 2022-12-16 #
* Successful and productive
* I had a quiz in the morning
* Studied a bit for final exam

# 2022-12-17 #
* unsuccessful and unproductive
* woke up in the afternoon and do nothing
* its so cold outside

# 2022-12-18 #
* unsuccessful and unproductive
* same as saturday, i woke up in the afternoon and do nothing

# 2022-12-19 #
* successful and productive
* i did my homework today
* i also studied for final

# 2022-12-20 #
* successful and productive
* woke up in the morning and go to class until afternoon

# 2022-12-21 #
* successful and productive
* woke up in the morning
* i did my laundry
* i did my homeworks

# 2022-12-22 #
* We have course presentation today
* i was so sleepy during the class since i haven't got enough sleep last night

# 2022-12-23 #
* successful and productive
* i clean my room
* i have group project today
* i went to christmas eve dinner with my fellow indonesian friends

# 2022-12-24 #
* successful and productive
* i got up in the morning
* i spend the day outside
* i go to a free concert at night, i watch a korean singer called sunmi

# 2022-12-25 #
* successful and productive
* i spent christmas with my sister

# 2022-12-26 #
* unsuccessful and unproductive
* i spent whole day in the bed

# 2022-12-27 #
* successful and productive
* woke up in the morning and go to class
* studied for final

# 2022-12-28 #
* successful and productive
* woke up in the morning and go to class
* i have vietnamese class today, it was fun

# 2022-12-29 #
* the live presentation today is great
* watched TED talk



