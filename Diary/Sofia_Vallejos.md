## 2019-09-26

In the 3rd week class we first heard most of the groups presentations, then saw a video about fake news.
I think most of the presentations were interesting but I didn't agree with all of the group's point of view, for example, one of the groups agree that United States is a good place to live economically speaking, but they showed only the best years, not even one down spot, every country have down spots, maybe United States is not a good place to live afterall.
The video the teacher showed us is an actual problem. Nowadays people have access to lot of information with only the simple effort of have internet connection. That information could be true or fake, is there a way to proof it?, is there a way to avoid it? I will take those questions in mind and research more about it.


## 2019-10-03

4th lesson

* Listen to group's presentations
* Brainstorm about money
* See videos about economy, possible finantial problems


## 2019-10-18 (2019-10-17)

5th lesson

We first listen to the claims and point of view of some groups
After we saw some videos about facism and nacionalism. For some people fascism is the next step of nationalism, but actually they are two different concepts.
Other video talk about nacism. I did not know nowadays people still believe in that kind of atrocities.
I did knew about the illegal inmigrant problem in USA. Is still sad to think about the situation of those people.


## 2019-10-24

6th lesson 

1) checked diary 
2) created supergroups to combine claims and present 
3) see videos about excercise and the brain and new ways to "improve society's health". Maybe to add a fee to people for getting sick is not fair.
4) collected claims from the videos and discuss.


## 2019-10-31

7th lesson

1) listen to other groups presentation and learn from their errors. 
2) see videos about depression. Is nice to here what people that had depression feel. It makes me ponder about the serious impact that these condition can have in someones's life. 
3) debate in groups questions about how to treat or what to do when our friend has depression.


## 2019-11-07

8th lesson

* Listen to group's presentations
* See videos about happiness and fulfiltment. I agree with the girl that said that people are less depress and more happy when they have a purpose to live for.
* Discuss with team members some claims from the videos and write hypothesis about them


## 2019-11-14

9th lesson

* Check diary and listen to group's presentations
* Discussion by groups of internal characteristics and external factors someone wants to change
* See videos about british law and british police. It was a little bit confusing but is important to understant our rights as civilians




## 2019-11-21

10th lesson

 

## 2019-11-28

11th lesson

* Listen to the rules from each group. 
* Debate about the rules and how to rule different situations, like 1 person in the hole world or overpopulation. 
* See videos about periodic and magazine's subjective information, how USA copy Rome's Empire and so on.

## 2019-12-05

12th lesson

* Listen to the presentation of each group. 
* See videos about news, media and analyze if they describe the situations as neutral or getting support to only one of the sites.

## 2019-12-12

13th lesson

* Listen to presentations from each group. 
* Each group have a representant to answer questions about the group's topic. 
* Vote for the best way to take care of our planet.

## 2019-12-19

14th lesson

# 12-19
* A) Succesful and unproductive.
* B) Succesful due to I was for my roomate in his birthday and unproductive since I did not study.
* C) Organize better my time.

# 12-20
* A) Succesful and unproductive. 
* B) Succesful since I finished with a documentation. Unproductive by the reason of spend too much time in small details for a dinner's organization.
* C) Wake up earlier and organize stuff quicker.

# 12-21
* A) Succesful and productive. 
* B) Succesful because the christmas dinner I organized had good feedbacks. Productive since I worked hard the hole day. 
* C) Maybe I can be productive in other aspects also, like studies or self care.

# 12-22
* A) Succesful ans unproductive. 
* B) Succesful since I got in time to my responsabilities. Unproductive because I slept half of the day and did not advance with my studies. 
* C) Understand that sunday is not only for rest. 

# 12-23
* A) succesful, unproductive. 
* B) Succesful because I did all the stuff I should have done. Unproductive because I did not study enough. 
* C) I can wake up earlier.

# 12-24
* A) succesful, unproductive. 
* B) Succesful because I finished my homeworks. Unproductive because I did not study enough. 
* C) I can make a list of priorities for the week or for each day.

## 2019-12-26

15th lesson

# 12-25
* A) Succesful and unproductive.
* B) Succesful due to I made some friends and unproductive since I went to a friend´s house instead of studying.
* C) I thing it was ok to be unproductive today... is chrismaaas!

# 12-26
* A) Succesful and productive. 
* B) Succesful since I got A in the Taichi final exam and, even though I slept few hours I went to class the hole day, that is quite productive for me.
* C) Nice day.

# 12-27
* A) Succesful and productive. 
* B) Succesful due to I studied long time. Productive since I got my things done. 
* C) Keep going like this.

# 12-28
* A) Succesful ans productive. 
* B) Succesful since I got in time to my responsabilities. Productive because I went to my meeting and studied. 
* C) Everything good for today. 

# 12-29
* A) Unsuccesful, unproductive. 
* B) Unsuccesful because I slept the hole sunday. Unproductive since spended the hole day sleeping. 
* C) I can recharge my phone before sleeping to not miss my alarm.

# 12-30
* A) succesful, productive. 
* B) Succesful because I finally bought a flight ticket. Unproductive because I did my GE courses homeworks. 
* C) I can do stuff faster.

# 12-31
* A) Unsuccesful, productive. 
* B) Unsuccesful because I did not finish with my vacation preparations. Productive because I advanced with my vacation preparation. 
* C) I can ask for help.

# 01-01
* A) succesful, productive. 
* B) Succesful since I fincally made a videocall with my family and productive because I have the number of pages I need to study the hole week to do it well in thenext week finals. 
* C) I can star to study now.







