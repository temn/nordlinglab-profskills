# 2022-09-08 #
* I'm happy to enroll in this class, since I'm hoping to improve my English ability.
* Group disscusion every week is really helpful for us to brainstorm the topic of every class, and make friends at the samw time.
* My thoughts about the world have been change after listening to Elon Musk's view about the future world.

# 2022-09-15 #
* The second lecture is pretty enlighting, I've learn more about how the world is progressing.
* Althought our group doesn't think exponential growth will lead to a world of abundance, after the presentation, I've changed my mind of whether exponential growth lead to a world of abundance due to the flaw of our thought.


# 2022-09-22 #
* homework

What you think your future will look like and why it will be so 10 years from now?
	
If I have to summary what I think my future will look like and why it will be so 10 years from now, I will describe it like this: Ten years from now, I will still live in Taiwan. Since my hometown is in Taipei and my family is also there, it will be my first chose to live and work in Taipei in the future. After I have graduate form National Cheng Kung University, I will be a master in National Taiwan University since it’s the best university in Taiwan and it’s one of my dreams to be there. Also, having an engineering degree can help me to get a job in high tech company or car industry like google, tesla, etc. Why would I work in these company? It is because of the future trends. Soon, human would be running out of non-renewable energy and the Moore’s law would still work. The importance of electric vehicle and high-tech product will become higher and higher. I will be working on how to improve the efficiency of electric motor or battery, also the vehicle design or even make flying cars possible. Furthermore, I would like to work on artificial intelligence like self-driving which is an inevitable future trend. By the way, all these things I would like to try are also somehow have relation with my major in college. 
Due to the aging society, pressure physically and mentally might be higher, and disease might be more lethal in the future (like covid 19), medical cares are also crucial in the future. This is also why I would like to live in Taiwan which has one of the best medical systems in the world and has great health insurance. More, Taipei is the capital of Taiwan which all the most advanced tech will come first and has more work opportunities. In conclusion, living in my hometown in the future not only can help me approach my dream and work in those company, but also can let me have more life quality and great medical care which are the future trends. 
These are how I would describe my future life ten years from now and how current trends would affect me.
	
* Bank of England to start unlimited bond purchases to stabilise market
* left : The Bank of England will not sell British government bonds into a dysfunctional market, but a sharp repricing that reflects changing fundamentals would be not be a reason to pause the programme.
* Center : Only stated out what did the bank do and it's cause so far.
* Right : The dramatic move came at 11am, as the Pound continued to fall and as top bankers had been meeting the Chancellor as he sought to defuse the economic crisis which he has sparked.

# 2022-09-29 #
* I have learn how the world's finance system works, and it changes my vision of the world.
* Although I'm not mayjor in finance and some theories are a little difficult, I'm able to understand some of them.

# 2022-10-06 #
* Finally, we have a physical done on the 5th week, and I think it's pretty good to have onsite interaction with everyboy.
* Sadly, I'm a little tired due to lots of assignments and exams this week, and I'm not able to fully concentrate in this class.
* Knowledge about economy is quite important, it influences how we think about the world and how we should manage our money.
* I'm starting realize how the things we believe can influence us.

# 2022-10-13 #
* I'm happy to meet my groupmate in person and have a great discussion and chat with them.
* The debate about how we have gain knowledge is quit impressive today, especially the part of whether we are gaining information or learning knowledge. 
* The TED talk about why exercise is important to our brain actually motivates me to start exercising every day.

# 2022-10-27 #
* the big group is a good idea, however, many people from the different group didn't join the google meet. I guess the didn't attend class because of the topic this week.
* this week we learn about depression, and it's a little bit difficult for me because there are some hard words but still it's a important lesson that we should all learn.

# 2022-11-3 #
* Continuation of the topic last class, we learn how to happiness/fulfilment and how to live a betterlife.
* The ted talk about two victim families is quite impressive, and it's all about forgiveness.
* Actually, I'm feeling a little blue this week, I felt a bit better after this lecture and I have found how to make my self better.

# 2022-11-10 #
* The presentations of super group is quite good this week, and there are a lot of professional material.
* It's interesting to learn about how the legal system work which I didn't know before this lecture.

# 2022-11-17 #
* I think my presentation is not good this week, cause there are some mistake information about Morroco.
* After watching the videos of this lecture, I think i may be a little addicted to social media.
* There are lots of exams next week, hope I can get through them.

# 2022-11-24 #
* Our group misunderstand the task last week, so one of our action is not availble in the presentation.
* I think I'm a little bit addicted to my phone, when I was bored in class, I took my phone up several times.

# 2022-12-01 #
* I'm quite tired this week because I have a lot of exams.
* Todays lecture is about Planetary boundaries and I think it's interesting.
* Professor's child is cute!

# 2022-12-08 #
* Most of the lecture this week is our presentation, and I think most of the people are doing well.
* The action of our group is to replace old equipment and I think it's quite feasible.
* Professor mentioned that using the Goshare system to carry out our action might be a great way, and we will think about it.

# 2022-12-15 #
* The debate this week is quite interesting.
* We will have to write diary every day this week.
* The weather is getting cold and colder, I think I should take out my jackets.

# 2022-12-22 #
* the course this class is great and the professor had gave us some suggestion on dressing
* I'm very sorry that I forgot to write diary everyday last week and I will do it this week.
* Successful and Productive
* I feel great today becasue i got a good grade on the quiz this morning.
* I will keep doing my best on the quiz tomorrow.
# 2022-12-23 #
* Successful and Productive
* After staying up last night, I got 99 on the quiz today and that feel great.
* I have already make a schedule about what I should study this weekend.
# 2022-12-24 #
* Unuccessful and Unproductive
* I can't debug my openGL assignment today and that feels terrible.
* I will try to wake up early and start my day earlier tomorrow.
# 2022-12-25 #
* Unsccessful and Productive
* Although I have study a lot today, it's Xmas today and I can only study at home.
* I have already make a schedule about what I should study this week and hope I can done it.
# 2022-12-26 #
* Successful and Productive
* I am able to answer all the question of the exam toady and I think I can get a good grade.
* I will keep on following the schedule.
# 2022-12-27 #
* Successful and Unproductive
* I don't have to do experiment today so I can have a great nap this afternoon.
* I will keep on following the schedule.
# 2022-12-28 #
* Unsccessful and Productive
* After hours of discussion with the TA of computer graphics, I still can't fix the bug of my assignment.
* I will start memorizing the Japanese words tomorrow.

# 2022-12-29 #
* I'm tired this week because I have a lot of exams.
* New year is coming, hope I won't have to study all weekend.
	
# 2023-01-05 #
* It's the last week of the semester, I'm happy that the winter vacation is coming.
* I have tons of exams this week, and I sad that I didn't do well on some of them.
* It is the final exam of this course, I have studied well and I got a good score.
* Hope I can get a good grade on this course although I only give presentation 4 times cause there isn't enough chance for everyone.
* Happy 2023 to everyone.

# 2023-01-07 #
* It's the final presentation today and I think everyone is doing great.
* I hope I can get a decent score on this course.
* Honestly, I think this course is quite cool and I have learned a lot of knowlegde that I won't learn by myself.