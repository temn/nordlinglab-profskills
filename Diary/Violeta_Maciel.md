This diary file is written by Violeta Maciel F94117047 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-23 #

* Nowadays, it's really important to know how to spot fake news.
* I agree that trust between people and leaders is very powerful.
* Fake news destroys trust.

## Homework ##

## Part 1 ##

How I think my life will be in 10 years from now:

In the year 2033, I find myself back in my hometown, Asuncion, Paraguay, after an eventful decade. Ten years ago, I graduated from NCKU University with a degree in Biomedical Engineering. The knowledge I gained during those years inspired me to pursue further studies.I wanted to learn more, so I decided to get a Master's degree in Genetic Molecular Engineering, somewhere in Europe, perhaps in Spain, Portugal, or Switzerland.

Now, in 2033, I'm not just a graduate but also someone deeply committed to making a difference in my field.When I returned to Paraguay, I realized that the field of Biomedical Engineering here needed some improvement. So, I started my own company.We focus on research and development in healthcare to bring positive changes to my country.

The journey from being a student to an entrepreneur has been a transformative experience.My passion for biomedical engineering fueled my desire to bridge the gap between the advancements in the field and its application in my homeland. 
The decision to establish my own company wasn't just about business but also about contributing to the growth and development of Paraguay's healthcare industry.

I also found myself taking on the role of an educator. Sharing knowledge and insights has become an essential part of my mission. I hold lectures and workshops, connecting with young minds in Paraguay.The goal is to inspire them to consider careers in biomedicine, thus paving the way for a brighter future for our nation. 
Watching these students absorb information and develop their own passion for the field is incredibly rewarding.

But it's not just my professional life that has evolved. My personal life has also seen significant changes over the last ten years. I married my boyfriend, and together, we've been building our life. We are planning to have children soon, and this marks an exciting new chapter in our journey. It's a reminder that while career success is important, so is building a loving family and nurturing relationships.

Throughout this journey, my family has remained a constant source of support and motivation. I haven't forgotten the sacrifices they made to invest in my education and dreams. Their unwavering belief in me gave me the courage to pursue my ambitions. Now, in 2033, I am committed to giving back to them. 
I want to support them financially, to ease the burden they carried for my sake. Repaying their love and trust is not just a duty; it's a heartfelt desire.

As I reflect on the last ten years, I see a life filled with purpose and fulfillment.My educational background, combined with my entrepreneurial spirit, has allowed me to contribute positively to my country's healthcare sector.The impact of my work extends beyond the confines of my business; it's about improving the lives of Paraguayan citizens and inspiring the next generation of scientists and engineers.

Looking ahead to the next ten years, I am filled with optimism. I know there are endless possibilities waiting to be explored.Every step I take is guided by my passion for biomedicine, my commitment to my family, and my love for my country. My journey continues, and I am excited to see where it will lead, knowing that I have the power to shape my destiny and make a meaningful difference in the world.

## Part 2 ##

* The article I chose was **"Nebraska mother sentenced to 2 years in prison for giving abortioon pills to pregnant daughter"**
* The liberal sources pay more attention to "assisting her daughter in taking pills to end her pregnancy" rather than the illegal act of abortion.
* The neutral coverage points out that Burgess was involved in "handling human remains," which wasn't emphasized by the liberal or conservative sources.
* The conservative perspective highlights "giving her daughter pills for an illegal abortion," focusing on the fact that it was against the law.
* Only the neutral and conservative sources provide information about Celeste Burgess' punishment or sentence.

# 2023-10-12 #

* I found it quite interesting to listen to my classmates as they shared their ideas about their future lives during the first hour.
* The topic of the class was money. Money is a widely accepted means of exchange and value storage that simplifies trade and economic transactions.
* The financial system revolves around the central bank's issuance and the subsequent dispersion of currency through the intricate banking network. 
* This intricate system, in turn, plays a vital role in enabling individuals to save, invest, and access credit. 
* Learning about money in this class was fascinating, however, there were moments when all the information felt like a lot to take in. It's like trying to explore a vast sea of financial ideas and concepts.
* I wish I had received better education in managing money, budgeting, and investing. It would have been so helpful in preparing for the real world.

# 2023-10-19 #

* I think all the watched videos were very interesting.
* Feeble nationalism results in violence and impoverishment.
* Fascism exclusively recognizes patriotism and negates other identities.
* The most potent form of knowledge is centralized information.
* Explored the distinctions between nationalism and fascism and the potential inadvertent connections between them.

## Homework ##

# Career #
* By being an international student in Taiwan, I have dedicated myself to pursuing a path in biomedical engineering. Embracing the narrative of a "career" has driven me to invest substantial time and effort into my university studies.
* This was a very hard week since it was full of mid-term exams.

# Therapy #

* This week I had an appointment with my psychologyst and after telling him all my concerns I somehow felt better. 
* Is funny to me because I feel like I pay someone to listen to my problems and tell me how to deal with them. 
* I do not think that monkeys will have a psychologist or ever even think about that idea, but its ok, it works for me.

# Money #
* There is going to be a "Coldplay" concert on 11/11 and I had an extra ticket so this week I sold it to one of my friends. 
* I was just thinking about Harari's youtube video about "Imagined realities" and his speech about money. Honestly, is funny to me the fact that I pais for a very expensive piece of paper that will allow me to go to a concert just to see other humans sing.


# 2023-10-26 #

* Honestly, today's class felt very personal for me.
* I found every video that we watched today very interensting and informative.
* Prioritizing the prevention of illnesses through healthy living and routine medical examinations is crucial, as opposed to addressing ailments after they manifest.
* The results of the anxiety survey from previous years got me thinking about students like us who are dealing with significant mental health challenges and what might be causing this to persist.
* After taking the class survey, I thought a lot about my own situation. Looking back I can say that I have improved my mental health a lot since I decided to beggin my psychiatric trantment and terapy. I really that I can improve more every day.

# 2023-11-02 #

* I was so shocked when today we had a look to the results from the survey we took last class. About a 60 to 70 percent of the class do not seek professional help for their anxiety.
* This particular quote from one of today's videos had a profound effect on me, and surprisingly, I strongly relate to it: "You can experience sadness and still be okay."
* I am not going to lie, as someone who tried to commit suicide in the past, I had a very hard time watching all these videos about depression, but I also felt in a safe place.
* Recognizing depression in a friend may involve noticing changes in their behavior, such as withdrawal, mood swings, or loss of interest in activities they used to enjoy.
* When talking to a depressed friend, offer support, lend a listening ear, and encourage them to seek professional help. IMPORTANT = AVOID giving unsolicited advice.
* To help a friend with depression, be there for them, offer to assist in finding professional help, and maintain open communication. Encourage self-care and be patient and understanding. Always do things from the heart, not only because you just feel you have to do it.
* Before trying to help or take care of someone else be sure to take care of yourself first.

# 2023-11-09 #

* On this lecture was the first time that I read my diary in front of everyone and I realized that I write a lot.
* It was the first time I have learned a little bit about the "living funerals" that people practice in South Korea and honestly, I was shocked to find out about such a practice.
* We watched some videos about how to choose happiness and fulfillment but I was very tired and did not pay so much attention.

# 2023-11-16 #
* I listened to our classmates presentation and I think that the presentation about healthy sleep was super interestiing and as a person with a very messed up sleep schedule I will take their advices into account.
* Since the presentations took a lot of time, we did not have time enough to go into the whole new lesson which was about the law but I think the topic is very important and look further to learn more about it in the next class.
* The professor gave us some video recommendations which I wish I could watch during this week, I have some midterms but I will still try.

# 2023-11-23 #
* It was very interesting to hear my classmates presentation about the different countries and their laws.
* So, in the last class, I spilled the beans about me and my friend causing disturbances during the last Chinese New Year in Taiwan and our encounter with the police. I couldn't help but laugh about it.
* We also talked about staying connected and free. Got me thinking how glued I am to my gadgets every day and how much they mess with me.

# 2023-11-30 #
* Every group presented their ideas and actions based on their topic, there were some that I found it really interesting and if they are actually doing it I would like to participate.
* My group presented ideas also based on our topic but it was actually kind of difficult to think some realistic actions ans the professor was not convinced so we have to think more.
* I really liked the chanllenge the professor gave us, taking our phone away during class was not a bad idea at all, I wish I was able to do it by myself but the reality is that my will is not that strong.

# 2023-12-07 #
* I became concerned when I realized our big group forgot to share our ideas on Google Sheets. It's time to act quickly and get it done.
* After a while, we finally had an hour for a lecture. I found the topic "Planetary Boundaries" really interesting.
* Before this lecture, I didn't know about the nine planetary boundaries. I like that I get to learn something new in almost every class.

# 2023-12-14 #
* We collaborated on our course project, incorporating an interview and delivering a presentation to our class.
* During the class, I participated in a brief debate, presenting the argument that our group's project held significant potential. 
  Our goal is to champion student empowerment by establishing a bulletin board where students can articulate their opinions, express concerns, and take meaningful action.
* We did not have much time to go through our class topic of the day but it was related to the third indstrial revolution and we got to see a bit of the video the professor prepared.

# 2023-12-21 #
* At the beggining of the class we started with the presentations as per usual and it was my turn to present.
* My little group presenation topix was abuot the so-called 4th industrial revolution, which it was a little hhard to explain since information is not very accurate.
* Its been a long time since I have heard about this topic, I feel like the last time was when I was in high school.
* This class was quiet interesting since we divided ourselves into small groups and did a little debate. Sadly, we did not have enough time to get to a concensus.

# 2023-12-22 Friday #
* Unproductive and unsuccessful.
* I woke up very sick and did not go to any of my classes, I slept al day and postponed all my pending tasks for tomorrow.
* I will try to take some medication so I can wake up better tomorrow and beggin with all my taks.

# 2023-12-23 Saturday #
* Unproductive and successful.
* I woke up feeling a little bit better and went to have lunch with a friend I have not seen in a while but after that I went back home and rest all day again.
* I prepared a tasks list for everything I need to get done tomorrow and promised myself I will do it.

# 2023-12-24 Sunday #
* Productive and successfull.
* I woke up early and cleaned my house and finished my circuits homework before lunch. After lunch I went to my friend house and started cooking for christmas eve, we had a good time.
* I will try to wake up early again and try to do as much as posible, waking up early really help me to feel productive.

# 2023-12-25 Monday #
* Unproductive and successfull. 
* I did not go to any of my classes because I started to feel sick again but I received a call from my family and we got to talk a lot and that made me very happy.
* I will try to take my medicine on time so I can start feeling better and start to get things done.

# 2023-12-26 Tuesday #
* Productive and successfull.
* Even though I did not got to class I used that time to study for my Anatomy final exam which is this coming Friday, I still have a lot to study though.
* I will wake up early and try to advance ass much as I can with the anatomy content.

# 2023-12-27 Wednesday #
* Productive and successfull.
* I did not go to class and woke up late becausse I still feel very sick, but after I woke up I finished my laboratory report and studied a lot for my anatomy exam.
* I need to the doctor again some I can get new medicines since the ones I am taking right now are clearly not working. If I start to feel better I willd o better in college.

## Homework ##
1. Maintain a positive mindset.
2. Prioritize and organize.
3. Practice time management. (I apply Pomodoro technique) 
4. Learn to say no.
5. Celebrate small wins.