This diary file is written by **Gerson Christian, C64138435,** in the course **Professional Skills for Engineering: The Third Industrial Revolution.**

# 2024-09-12 #

* It was a very fun and interactive lecture.
* We had a quiz today, and I got a very low score :")
* We had discussion about the future, how abundance will change the world.
* The teacher gave us a homework, Mr.Nordling task us to analyze a product which has grown in production number and how the price has decreased.

# 2024-09-19 #

* The first thing I learned from today lesson is about GIT.
* Then we had our time to present our homework from the previous week, although not all of the groups have the chance to show their work.
* From the presentation session, I learned a lot, how to give a good and proper presentation.
* Mr.Nordling gave us some tips on how to speaks infront of the crowd, that I think is very helpful
* Lastly we learned about the distruptive effect of exponential growth. 

# 2024-09-26 #

* This week presentations were very insteresting, we saw lots of statistics that shown how the world getting better in many ways. 
* During the class, we watched 3 videos about statistics and fake news.
* The TED interview video caught my attention, I am totally agree on what Amanpour stated in the video, when she said "that all sides have to be given an equal hearing, but not the equal treatment."
* Amanpour also suggested the idea of creating an algorithm capable of filtering out fake information. I think it�s a good idea, although it would be very challenging to implement. However, I also have concerns that if we become too dependent on this technology, it could lead to drawbacks, such as reducing our critical thinking skills or the possibility of the algorithm itself being manipulated.
* Mona Chalabi mentioned 3 questions to discover a bad statistics: 
<ol><li>Can you see uncertainty?</li> 
<li>Can I see myself in the data?</li> 
<li>How was the data collected?</li></ol>

## Task 1 ##
The five trends I expect will shape my future the most are:
<ol>
	<li>Advanced Education and Expertise</li> 
	<li>Business Digitization and Automation</li>
	<li>Technological Innovation in Business</li>
	<li>Globalization and Market Expansion</li>
	<li>Sustainability and Eco-Friendly Practices</li> 
</ol>

<p>10 years from now, at 31, I'll return to Indonesia after completing my master's degree abroad to continue the family business. My education and international experience will equip me with the expertise and knowledge to modernize and expand the business. I'll bring a broader global perspective and innovative solutions to ensure the company remains competitive in an ever-evolving market. The master's degree from abroad will provide specialized knowledge to introduce fresh ideas into the business. Global exposure will not only enhance my understanding of international markets but also enable me to build a network of contacts for new partnership opportunities and business growth. 
Integrating AI and automation will be a primary focus to optimize operations, increase efficiency, and enhance customer experience. From streamlining routine processes to leveraging AI-driven insights for better decision-making, these technologies will allow the business to operate more efficiently while scaling rapidly.
Sustainability will also be a critical trend I'll focus on. I plan to incorporate eco-friendly practices into the business, such as reducing our carbon footprint by adopting renewable energy sources or implementing sustainable supply chains to minimize waste. These practices will ensure the long-term viability of the business and build a positive brand image as an environmentally responsible company.
Globalization will present another key opportunity. With international experience and an expanded network, I'll be better positioned to explore global markets and expand the business beyond local boundaries. This will require a strong understanding of international trade laws, market dynamics, and the ability to navigate different cultural landscapes.
Lastly, staying at the forefront of technological innovation will be crucial. Whether it's leveraging big data for strategic insights, using IoT for real-time management, or developing a robust e-commerce platform, embracing the latest technological advancements will ensure the business thrives in a digital world. Together, these trends will help shape a sustainable, tech-driven future for the company.</P>

## Task 3 ##
<p>Summary of "Vietnam estimates damages of $3.31 billion from Typhoon Yagi"</p>
* **Center sources:** Report Vietnam's total damage estimate of $3.31 billion. The typhoon caused massive flooding, destroyed infrastructure, and impacted agriculture and industry, with 299 deaths and 34 missing.
* **Left-leaning sources:** Focus on the disaster’s effects on vulnerable rural communities, particularly due to landslides and flooding. The narrative highlights the urgent need for environmental and climate change action.
* **Right-leaning sources:** Emphasize economic concerns, particularly how the typhoon disrupted major industrial sectors and export activities, potentially impacting Vietnam's economic growth projections.

# 2024-10-03 #

* Due to the typhoon, the government has decided to cancel all work and class activities from October 1st to October 3rd.
* We were given assignment for the next meeting. The homework requires creating figures on the historical trends of purchasing power, central bank balance sheets, government debt, and various types of credit and debt, along with analyzing income and wealth distribution over time, drawing conclusions from the data, and presenting an explanation of what gives money its value based on credible sources.
* We need to submit a 2-minute video summarizing the findings of the presentation.

# 2024-10-17 #

* This week, we finally had an onsite class again after two weeks of disruptions due to the typhoon and Taiwan's National Holiday.
* In class, we first discussed the concept of money, what gives it value, the factors that affect it, and how it is created.
* We were also shown a trend of deposits and loans in Taiwan. It was fascinating to see how the amount of currency held by the public spikes every year around Chinese New Year.
* President Nixon was mentioned, stating that nothing supports the value of the dollar except the public’s trust in it.
* Additionally, we watched a video that illustrated how money works, how loans play an integral role in creating new money, and provided a clearer understanding of the financial system.

# 2024-10-24 #

* This week we covered 2 lessons, lesson 5 ("Why does extremism flourish?") and lesson 6 ("How to live healthy?").
* We watched a TED Talk from Christian Picciolini, an ex-member of a neo-Nazi movement. He shares how meeting his wife and becoming a father completely transformed his life and helped him leave the neo-Nazi movement. Reflecting on his journey, he discusses how a lack of purpose led him into a toxic path, and how understanding his own values ultimately guided him out. Picciolini's experience teaches that the roles we choose impact our future.
* In Wendy Suzuki's TED Talk about brain-enhancing effects of exercise. Exercise, not only improves physical health but also significantly boosts mood, focus, and memory. Suzuki’s research show how regular physical activity increases the brain's protective factors against depression, anxiety, and even neurodegenerative diseases like Alzheimer. This video underlines how incorporating exercise into daily life can have transformative effects on both mental and physical well-being.
* We also had an open discussion on the definition of knowledge and how it differs from information. Plato defined knowledge as “well-justified true belief,” suggesting that knowledge goes beyond just possessing information, it requires justification and truth. While information consists of raw data or facts, knowledge implies a deeper understanding, where beliefs are verified and supported by evidence. 

## Task 1 ##

* **God:** I believe that God has a perfect plan for us gives me peace, especially in uncertain times. My faith in God’s perfect plan provides reassurance and a sense of peace. I trust that every setback is part of a greater purpose, allowing me to approach the future without anxiety. This week, when faced with a challenge, I found comfort in knowing that God’s timing is always right.
* **Marriage:** I view marriage as a serious commitment, symbolizing my readiness to share my life with someone I deeply trust. For me, the ultimate goal of any relationship is marriage, which underscores the importance of thoughtful consideration in romantic connections. As for marriage, I have decided not to engage in any relationships at the moment because my current priority is education, not marriage. I feel it would be unwise to enter a relationship if I cannot fully prioritize it. This week, I reflected on the importance of focusing on my goals and ensuring that I am in the right mindset before pursuing anything serious.
* **Career:**  I believe that success is built on the foundation of consistency. This week, I focused on maintaining consistency in my studies and projects, recognizing that many people start strong but few finish the journey. My commitment to consistency is helping me stay on track toward my career goals. This mindset not only helps me maintain motivation but also reinforces the idea that small, steady steps can lead to significant progress over time.

## Task 2 ##

<p> For Indonesia’s future, President Joko Widodo has outlined a vision focused on continued digital and economic transformation, green energy, and regional resilience. As stated in the National Long-Term Development Plan, his goal is to position Indonesia as a competitive global economy, achieving industrial independence and technological innovation while advancing environmental sustainability. This includes developing digital infrastructure, encouraging green investments, and preparing for a post-fossil fuel economy. Widodo's plan reflects a future where Indonesia is a robust, sustainable, and technologically advanced society.</P>
<p> He emphasizes nurturing Indonesia’s young talent to support sectors like e-commerce, finance, and technology while attracting foreign investment in eco-friendly and high-tech industries. the government plans to promote regional equity, ensuring that all provinces benefit from national growth and digital advancement, reducing the urban-rural divide.</P>

# 2024-11-07 #

* This week, we dove into a set of TED Talks focused on heavy issues like depression and suicide. One of the talks touched on the silent struggles people often face, which made me reflect on how we often hear tragic stories of suicide and how common it is for people to carry those burdens without anyone knowing.
* The lecture left me feeling inspired to be more available for friends who might be facing similar struggles. I realized that being there to listen could make a difference if they ever need someone to talk to. However, It's also important to keep emotional boundaries in mind. Taking on too many of others’ worries can weigh on us, so knowing when to step back is important. If things ever get too intense, I’ll encourage my friends to reach out for professional support.

## Recognizing and Supporting Someone in Early Stages of Depression: ##

<ol>
	<li>Notice Changes: Pay attention to any shifts in mood, behavior, or energy, like sudden sadness, or withdrawal from social activities.</li> 
	<li>Start a Conversation: Find a calm, private moment to ask gentle questions like, "How have you been feeling?" or "Is there anything on your mind?"</li>
	<li>Listen Without Judgment: Be supportive and understanding. Avoid giving advice or saying things like "just cheer up". Simply listen and show you care.</li>
	<li>Encourage Self-Care and Help: Gently suggest talking to a counselor or therapist.</li>
	<li>Follow Up: Check in with them regularly, letting them know you’re there and that they’re not alone in their journey.</li> 
</ol>

# 2024-11-14 #

* This week’s topic, "How to Choose Happiness/Fulfillment?" examined the results of a class survey, revealing significant levels of anxiety among students over the past week. This brought attention to the mental health challenges many face. 
* we had group presentations on identifying signs of depression in friends and offering ways to help. The presentations also involved analyzing claims about mental health through hypothesis-based thinking, encouraging both empathy and critical analysis.
* The session reminded me of the importance of creating safe spaces for open conversations with friends while setting emotional boundaries. Providing support is essential, but recognizing when professional help is needed is just as important.
* In class, we watched a TEDx video by Petter Johansson, where he demonstrated an experiment manipulating people’s choices without their awareness. Participants were shown two pictures, asked to pick one, and then unknowingly presented with the picture they did not choose. Surprisingly, many defended the reasons for their “choice,” even though it was manipulated. This highlighted how we rationalize decisions, even when they don't align with our original intentions.

# 2024-11-21 #

* This week’s topic, "How to Stay Connected and Free?", focused on the challenges of balancing freedom and responsibility in our digital lives. We started with Jaron Lanier's TED Talk, How We Need to Remake the Internet, which examined the flaws in the foundation of modern digital culture. Lanier highlighted the "globally tragic, astoundingly ridiculous mistake" made by companies like Google and Facebook, creating business models that exploit personal data and attention for profit. He proposed designing systems that prioritize human well-being over algorithmic manipulation. The talk was thought-provoking, emphasizing the need to shift the internet's goals toward fostering genuine connections, privacy, and autonomy.
* We engaged in an open discussion about how to a consensus on a minimal yet sufficient set of enforceable laws to safeguard:
<ol>
	<li>Freedom of speech</li>
	<li>Privacy</li>
	<li>Equal access</li>
	<li>Easy sharing</li>
	<li>Prevention of fake news, brainwashing, manipulation, and addiction</li>
</ol>
## The Consensus Rules: ##

<ol>
	<li>Universal Digital Rights: Everyone has the right to access the internet free of discrimination, censorship, and surveillance.</li>
	<li>Data Ownership and Privacy: Individuals own their personal data and must give explicit consent for its use.</li>
	<li>Transparent Algorithms: Platforms must disclose how their algorithms work and allow users to customize their feed preferences.</li>
	<li>Net Neutrality Enforcement: Equal access must be protected, prohibiting providers from favoring or throttling specific content.</li>
</ol>

## Process To Reach The Consensus: ##

<ol>
<li>Brainstorming: Each group member shared ideas about positive aspects to preserve and negative behaviors to avoid in digital spaces.</li>
<li>Prioritization: We listed key values (e.g., freedom, privacy, equality) and harmful issues (e.g., fake news, manipulation) and ranked their importance.</li>
<li>Research and Debate: Members debated the feasibility of proposed rules.</li>
<li>Drafting Rules: We collaboratively drafted rules that aligned with the prioritized values while addressing the main challenges.</li>
<li>Voting: We voted on the final rules to ensure every voice was considered.</li>
</ol>

# 2024-11-28 #

* This week’s topic, "How to Make Sense of Events," offered an insightful informations of how we perceive and interpret occurrences around us.
* The session began with presentations of final group project ideas, where we voted on whether each proposal was a good or bad idea. It was an interactive way to reflect on critical thinking and decision-making processes, helping us evaluate different perspectives.
* Next, we analyzed historical graphics showing the growth in city sizes through various eras. One intriguing insight was how the recovery time after each "dark age" increased with each subsequent era. This led to the fascinating and somewhat unsettling question: How long might the next dark age last? It was a reminder of how historical trends can inform our understanding of the present and future.
* The session highlighted the importance of looking at different perspectives and questioning what the media tells us. It showed me how valuable critical thinking is in finding the truth. I was also inspired by the teenager’s courage, proving that anyone can make a difference by standing up for what’s right.
* Later, we watched a YouTube video about the riots in Amsterdam initiated by Maccabi supporters, where civilians were harmed. The video revealed how big media often omits key details, leaving the truth uncovered. Surprisingly, the incident was reported accurately by a courageous young teenage reporter.

# 2024-12-05 #

* This week’s topic, "Planetary Boundaries," offered a deep dive into understanding the limits of Earth's ecosystems and how our actions impact them.
* The session began with a group discussion where the class was divided into three groups. Each group analyzed news stories to explore the perspectives from which they were presented. My group examined the Korean martial law through two different newspapers, one from China and one from the USA.
* Next, we were shown an illustration about planetary boundaries and learned that by 2023, **six boundaries** have already been crossed: **novel entities, climate change, biosphere integrity, land-system change, freshwater use, and biogeochemical flows**. This alarming revelation sparked discussions about how human activities are straining the planet's capacity to sustain life.
* We also watched a TEDx talk by Johan Rockström, "Human Growth Has Strained the Earth's Resources." Rockström emphasized how scientific advancements can help us recognize and address these issues. His research identifies nine planetary boundaries that can guide us in safeguarding our planet's ecosystems. One key takeaway was the realization that our growth and consumption patterns must be restructured to operate within these boundaries, ensuring sustainability for future generations.
* Lastly, we were assigned tasks for our final project. These included preparing a **WBS (Work Breakdown Structure)**, **PERT (Program Evaluation Review Technique)**, and **Gantt** chart for our presentation. Additionally, we were asked to reflect on how our proposed actions can help bring society back within planetary boundaries or fulfill social foundations.

# 2024-12-12 #

* This week’s activities focused on planetary boundaries, particularly through a group debate on the actions proposed for our final project. Each team presented their action plans, explaining how they would support or maintain a specific planetary boundary. Following the presentations, we were having a Q&A session that allowed representatives to clarify their strategies. The session ended with a vote to determine the most compelling and impactful idea.
* We also watched a segment of **Jeremy Rifkin’s documentary The Third Industrial Revolution: A Radical New Sharing Economy**. The video discussed the global economic crisis and proposed a shift toward a collaborative and sustainable economic model. It highlighted the importance of collective effort for systemic change, the integration of digital innovation and renewable energy into future economies, and the prioritization of sustainability to align economic growth with ecological needs.
* **To summarize:** during the group debate, I learned how different strategies can help protect the planet, focusing on practical solutions and clear communication. The session also showed how interconnected planetary boundaries are and how collaboration is key to creating real change. Jeremy Rifkin’s documentary emphasized the need for collective effort, technology, and sustainability to reshape our economic systems, giving useful insights for our final project.

# 2024-12-19 #

* This week’s topic focused on "How to Succeed." Professor Nordling divided us into three groups for a debate session: the Capitalists, the Workers, and the Environmentalists. The first discussion revolved around "Why does inequality exist?" During the debate, the Workers presented the stronger arguments. They emphasized that inequality stems from greed, providing examples such as CEOs maintaining their salaries even during tough business conditions while cutting workers' pay. This perspective resonated strongly with many participants.
* After the debate, Professor Nordling introduced us to some important models for personal and professional development. Here are some models that I remember: The **7 Habits of Highly Effective People** by Stephen Covey outlines habits like being proactive, beginning with the end in mind, and putting first things first. These principles focus on personal responsibility, goal-oriented actions, and prioritization to achieve success. **Ikigai**, a Japanese concept meaning "a reason for being," represents the intersection of what you love, what you are good at, what the world needs, and what you can be paid for. This philosophy helps individuals align their passions with their life’s purpose. **Radical Candor** by Kim Scott encourages leaders to care personally while challenging directly. It is about building trust and fostering open communication to achieve better results without being overly aggressive.
* We were also given an assignment to make a daily journal for a week, where we need to label each day as either successful/unsuccessful and productive/unproductive, analyze why we felt that way, and state one change to be more successful or productive the following day.

# Daily Diary Task (20-25 December 2024) #
## Friday (20/12/24) ##

* Productive/Successful
* I felt productive because I went on a solo trip to the Fo Guang Shan Temple in Kaohsiung. I spent the entire day exploring the greatness of the temple, learning a lot about its history and Buddhist beliefs. I felt successful because visiting this temple was one of my bucket list goals, and I managed to take some good pictures to :) .
* The solo trip was fulfilling and well-executed. Continue exploring but ensure well rest to avoid fatigue.

## Saturday (21/12/24) ##

* Productive/Unsuccessful
* I visited Sun Moon Lake with my friends and planned to stay overnight. The journey was productive as we left Tainan at 2 a.m., arrived at Sun Moon Lake by 9 a.m., rented bikes to explore the lake, visited Wenwu Temple, and enjoyed a cruise to Ita Thao. However, the day felt unsuccessful because we overslept after checking in at our hotel, missing opportunities to explore areas like the pagoda. We also couldn't find any restaurant that opened (bcs of overslept), so we went to Seven-Eleven.
* Lack of detailed scheduling caused missed opportunities. Create a detailed itinerary to maximize the day.

## Sunday (22/12/24) ##

* Unproductive/Successful
* Oversleeping until 10 a.m. disrupted our original plan to wake up at 8 a.m. and visit the aboriginal theme park earlier. However, we eventually made it there, enjoyed all the rides, and had a great time. Despite the late start, the trip was still a success.
* Be more disciplined with wake-up times to accomplish more.

## Monday (23/12/24) ##

* Unproductive/Successful
* The day began with a fun and free OIA Christmas party where we had lunch (+ its free) and exchanged gifts. Afterward, I went to the gym for 1.5 hours. However, I planned to do homework but ended up taking a long nap. Later, I had dinner with friends, and only then did I remember I will be having my Chinese exam for the next day. Although I managed to stay up late and cover all the subjects, the day felt less productive.
* Lack of preparation led to last-minute studying. Prepare for exams earlier.

## Tuesday (24/12/24) ##

* Productive/Successful
* The day started with a Chinese exam, which I felt went well. After class, I went to the gym for 1.5 hours, then went shopping for gifts at T.S.mall, and ended the day with a hotpot dinner with friends.
* Stay consistent.

## Wednesday (25/12/24) ##

* Unproductive/Successful
* Merry Christmas! I attended a morning class but skipped the afternoon one. Later, I joined the Christmas ceremony at the church, which was incredibly joyful. We celebrated together with singing, games, and sharing happiness, which made the day feel successful.
* Prioritize class attendance more.

# 5 Rules for Success and Productivity

<ol>
	<li>Plan Your Day</li>
	<li>Wake Up Early and Consistently</li>
	<li>Prepare in Advance</li>
	<li>Balance Work and Fun</li>
	<li>Focus on Priorities</li>
</ol>

# 2024-12-26 #

* This week class, Professor Nordling asked us to dress as if we were attending a job interview. Each of us shared our 5 Rules of Success in front of the class. After the presentations, we voted on whose outfit was the best or most suitable for a job interview. Professor Nordling also provided feedback on our appearances, emphasizing the importance of first impressions and professionalism in specific occasions.
* Next, Professor Nordling demonstrated how to tie a tie, surprisingly big portion of the class didn't know hot to tie it properly(including me). It was fascinating to learn that there are several styles to tie a tie. This activity made me realize the significance of dressing appropriately for different occasions and how much appearance contributes to a person's overall presentation.
* Finally, we received the criteria for our final project video presentation, which will take place during the final week on January 8th, 2025.

# Daily Diary Task (27 December-1 January 2024) #
## Friday (27/12/24) ##

* Unproductive/Successful
* I felt unproductive because I planned to do a visa extension in the morning but overslept until 1 p.m. Thankfully, I still managed to complete the visa extension in the afternoon. Later, I attended a Christmas and end-of-year party with my Fellowship group. It was super fun! We had dinner together (+ it was free), and played exciting games.
* Oversleeping delayed plans. Go to sleep earlier to start the day on time.

## Saturday (28/12/24) ##

* Productive/Successful
* In the morning, I went on a Kenting trip with the Enrichment Club. This trip was very worth it, as it only cost 300 NTD/person, and the rest was sponsored by the club. The journey took 2-3 hours, followed by a fresh seafood lunch where I ate a lot of sashimi. Later, we spent the afternoon at the beach, playing games like volleyball, dodgeball, and Red Light-Green Light (from the TV show Squid Game). The most memorable moment was enjoying the beautiful scenery while watching the sunset.
* Wear slippers next time to the beach.

## Sunday (29/12/24) ##
* Unproductive/Unsuccessful
* I spent the entire day in my dorm resting up after the trip. It felt unproductive and unsuccessful as I did not accomplish anything meaningful.
* Go to sleep early after having fun at the beach to recover better.

## Monday (30/12/24) ##
* Unproductive/Successful
* Monday felt like a very normal day. With no classes, I woke up late, had brunch at an Indonesian restaurant (super delicious & unhealthy), and then got a haircut at a local barbershop. Later, I worked on some homework and went to the gym.
* Avoid wearing cargo pants while biking, I ripped my pants.

## Tuesday (31/12/24) ##
* Productive/Successful
* I attended my Chinese class in the morning, then went straight to Kaohsiung with friends to celebrate the New Year. It was super fun with lots of street food and a free concert. Although I didn’t recognize the singers, their songs were pretty good. The countdown was thrilling, and the fireworks show was amazing.
* Book a hotel next time for New Year’s Eve celebrations to avoid being stuck without a place to stay.

## Wednesday (01/01/25) ##
* Unproductive/Successful
* It was a holiday, so I overslept until 2 p.m. The rest of the day was quite plain, just relaxing and recovering from the previous night’s celebrations.
* Stay productive even during holidays.

# 2025-01-02 #

* This week, the class focused on the theme of "Making a Difference." We explored ways individuals can create meaningful impacts in their personal and professional lives. Professor Nordling encouraged us to reflect on our aspirations and how we can contribute to society.
* As part of this session, we were given a diary assignment: to write our own obituary and identify two individuals who have achieved something similar to what we envision for ourselves. Here’s my submission:
* **Obituary:** Visionary Engineer and Compassionate Leader
* **Two Inspirational Figures:**

<ol>
	<li>Elon Musk, Achievements: Revolutionized the transportation, energy, and space industries through Tesla, SpaceX, and other ventures.</li>
	<li>William Soeryadjaya (Founder of Astra International, Indonesia), Achievements: Built one of Indonesia's largest diversified conglomerates while maintaining a commitment to philanthropy and supporting education.</li>
</ol>

# 2025-01-08 #

* Today was the final session for this course, and it was a culmination of everything we learned. We began with the final exam, which was conducted online through Google Forms. The questions tested our understanding of the course material and our ability to apply the concepts practically.
* After the exam, each group presented their final project through a pre-recorded video. Each presentation was limited to three minutes, providing a concise and impactful overview of our projects. This marked the conclusion of the course, leaving us with valuable lessons and memories.

# Course Objectives #

## "A World of Facts, Challenges, and Ideas" ##

1. Know the current world demographics; **Suggestion:** Dedicate 15 minutes daily to review updated demographic data from sources like the UN or World Bank and summarize key points.
2. Ability to use demographics to explain engineering needs; **Suggestion:** Identify one engineering problem each week and relate it to demographic data during group discussions or journal entries.
3. Understand planetary boundaries and the current state; **Suggestion:** Research one case study weekly and write a short reflection on how economic systems could address the issue.
4. Understand how the current economic system fails to distribute resources; **Suggestion:** Conduct a simple simulation or role-playing exercise in a group setting to demonstrate the unequal distribution of resources and reflect on the experience.
5. Be familiar with future visions and their implications, such as artificial intelligence; **Suggestion:** Write a brief fictional scenario weekly imagining how a specific AI innovation could change everyday life in the next 10 years.



## "Personal Health, Happiness, and Society" ##

6. Understand how data and engineering enable a healthier life; **Suggestion:** Analyze one case where data-driven engineering improved health outcomes and summarize findings.
7. Know that social relationships give a 50% increased likelihood of survival; **Suggestion:** Initiate one new social activity weekly (example, inviting someone for coffee or joining a new group) and reflect on how it affects your mood and energy.
8. Be familiar with depression and mental health issues; **Suggestion:** Research one fact about mental health weekly and consider how you could support someone facing these challenges.
9. Know the optimal algorithm for finding a partner for life; **Suggestion:** Conduct a mock exercise where you define your "criteria" and simulate applying a strategy to understand its practical use in decision-making.

## "Professional Skills to Success" ##

10. Develop a custom of questioning claims to avoid “fake news”; **Suggestion:** Practice fact-checking one news article daily using reliable sources like fact-checking websites or academic references.
11. Be able to do basic analysis and interpretation of time-series data; **Suggestion:** Use a free software tool like Google Sheets or Python libraries to analyze a dataset of your choice (example, temperature changes or sales trends) and write a brief summary of your findings.
12. Experience collaborative and problem-based learning; **Suggestion:** Join a study group or work on a small collaborative project weekly to solve a practical problem.
13. Understand that professional success depends on social skills; **Suggestion:** Role-play weekly scenarios with peers to practice and improve social skills critical for the workplace.
14. Know that the culture of the workplace affects performance; **Suggestion:** Research one case of workplace culture and note how it affects productivity and employee satisfaction.