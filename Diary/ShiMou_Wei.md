This diary file is written by ShiMou_Wei in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lesson shows me the basic idea of this course
* The lesson teaches me about different aspects of industry
* The questions showed in the test was hard for me 
* The teamwork is great and we had a chance to get to know each other

# 2024-09-19 #

* The concept of git is quite fun 
* The suggestions given by teacher were great and useful
* I like the topic of the Ted talk
* The question,"The world is getting better or worse",is pretty profound and interesting

# 2024-09-26 #

* I learnt about the thoughts of other students of wheather the world is getting better or worse
* Teacher talked about the fake news and showed us three videos
* I like the third video the most because it's interesting 
* We can visualize the statistics to make it easier to understand

# Homework : What you think your future will look like and why it will be so 10 years from now # 

* Ten years from now,I will likely live in a modern, sustainable urban environment or even a smart city that prioritizes eco-friendly infrastructure and technology. 
* Depending on my career path,I might work in advanced engineering sectors such as renewable energy, robotics, autonomous vehicles, aerospace, or advanced manufacturing. 
* My job could involve designing and developing sustainable systems, optimizing mechanical processes, or working with automation to drive efficiency in industries. 
* I may also collaborate with cross-functional teams worldwide, thanks to increased global connectivity and remote work opportunities.
* First, as climate change concerns grow, mechanical engineers will be increasingly focused on developing eco-friendly technologies. In 10 years, I could be working on projects that involve renewable energy systems, like solar, wind, or tidal power, or improving energy efficiency in manufacturing. Sustainable design and circular economy principles will likely be at the heart of my work. I might be involved in creating systems that use fewer resources, reduce emissions, or recycle materials more effectively. This emphasis on sustainability will affect the projects I work on, as more businesses prioritize environmental responsibility.
* Second, Industry 4.0, which integrates automation, data exchange, and smart technology in manufacturing, will heavily influence in the next ten years. Robotics, the Internet of Things (IoT), and artificial intelligence (AI) are transforming how products are designed, produced, and maintained.
* Third, the healthcare industry is rapidly evolving with advancements in medical devices, robotics, and personalized medicine. 
* Fourth, focusing on the sustainability in the foo industry, the food industry is experiencing a significant shift toward sustainability and resource efficiency.
* Fifth, as more people prioritize physical and mental health, the demand for wellness-focused products and technologies will increase.
* To conclusion, my work will bridge multiple disciplines and industries, creating systems and devices that improve health, promote sustainability, and optimize production processes. These trends will make my role more interdisciplinary, pushing myself to continually adapt and innovate to meet the needs of a rapidly changing world.

# 2024-10-3

* We took a day off since there was a typhoon.

# 2024-10-10

* National day holiday

# 2024-10-17

* Teacher asked us what money is
* What makes money valuable 
* I don't think NT dollars can be replaced by virtual currency since it was too risky. 

# 2024-10-24

# 2024-10-31
* We took a day off since there was a typhoon. 


# 2024-11-7
* Teacher talked about how to deal with our daily depression. 
* I talked to a classmate and shared each other thoughts about what depression we had been dealing with these days. 
* The lecture today was fun and interesting. 

# 2024-11-14
* The lecture today showed us a video about choice blindness, and the context was interesting. 
* People often believe they are fully aware of their choices and the reasons behind them. However, research indicates that individuals can be unaware of their own decisions and may even justify choices they didn't actually make.
* Johansson presents experiments where participants are asked to choose between two options, such as faces or tastes. Unbeknownst to them, their selections are sometimes swapped, yet many fail to notice the change and proceed to justify the altered choice.
* Understanding choice blindness has significant implications for areas like marketing, politics, and personal relationships, where individuals' stated preferences can be manipulated without their awareness.

# 2024-11-21
* The sweden is planning to phase out ICE vehicle by 2050. 
* What are the advantages of switching our ICE vehicel? some classmates mentioned about Environmetal benefits and cost savings. 
* I think Taiwan should not shut down nuclear power plant, since there are no energy sources that are more efficient than nuclear power plant. 

# 2024-11-28
* Reaching consensus is essential in various settings—such as teamwork, governance, and interpersonal relationships—because it fosters collaboration, inclusivity, and long-term effectiveness.
* Here are five points on how to effectively reach consensus: 
* 1. Encourage Open Communication: Create an environment where all participants feel comfortable sharing their perspectives without fear of judgment or rejection. Listening actively and ensuring everyone’s voice is heard fosters trust and collaboration.
* 2. Clarify Goals and Priorities:Clearly define the common objective or problem that needs resolution. Establish shared priorities to keep the discussion focused and aligned, ensuring everyone works toward a mutual outcome.
* 3. Explore Multiple Options: Brainstorm and consider various solutions to the issue at hand. Discussing the pros and cons of each option helps the group identify solutions that best satisfy everyone's concerns.
* 4. Seek Compromise and Flexibility:Encourage participants to remain flexible and willing to adapt their viewpoints. Highlight areas of agreement and explore compromises that integrate different perspectives.
* 5. Use Facilitated Decision-Making: Employ structured techniques like consensus-building tools, voting, or ranking options to make progress. A skilled facilitator can guide discussions, manage conflicts, and ensure the group stays on track.
# 2024-12-5

# 2024-12-26
* Successful + Productive
* I studied in the morning and finish the homework in time. 
* I went to cram school to help the kids do finish their hw. 
# 2024-12-27
* Unsuccessful + Unproductive
* I went to class to do the final exam in the morning but I was not confident about the result.
* In the afternoon, I went to cram school to help the kids finish their hw till 8 p.m. 
* I went to gym with my friends at night but the process was not efficient due to my tiredness.
# 2024-12-28
* Successful + Unproductive
* I went to tutor in the moring.
* I played video games in the afternoon and I felt regret after playing too much of it.
* I completed the google form which is required for our final presentation.
* I went to the gym to work out.
# 2024-12-29
* Unsuccessful + Productive
* To complete the tasks from final presentation, I interview my friend who is majoring civil engineering.
* I went to a photo studio and had my headshots developed.
* I hung out with my friends and take pictures that are for graduation in the afternoon .
# 2024-12-30
* Successful + Unproductive
* I discussed with my senior aboout a project and did some experiments. I learnt a lot from him but I felt sleepy during the experiment.
* I went to cram school and help the kids finish their homework. 
* Since there is a presentation which should be presented on next week, I discussed about it with my group members. 
