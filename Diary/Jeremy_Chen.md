# 2022-09-08 #
* This is the first class of this semester
* Not much content are covered, but i guess it is becasue the first class are mostly about introduction
* I like how the class is more about us interacting with each other than pure lecture.


# 2022-09-15 #
* I enjoy watching other presenting becasue I can learn from them and recognize my disadvantage.
* Professor pointed out several stuff that we should be careful when putting an image on our slides.


# 2022-09-22 #
* Today I got the chance to present in front of the class.
* I am not satisfied with my performance, because I think I am in a hurry and missed some points that I intended to cover originally.
* Our group should spend more time on making sure the presentation follows the template.
* I realized I will have to improve my ability of understanding the video in a short mount of time.
* There are fact checking websites that might come in handy when we are doing researches.
* Everyone of use should be able to pinpoint the false statistics, and never be someone gulliable that trust everything the news told us.

# 2022-09-22 homework #
***Q1: In your Diary for this week, write a 400-600 word summary of what you think your future will look like and why it will be so 10 years from now. Start by defining where you will live and what work you will do. Then describe the five current trends of change that you think will affect you the most and their effect on your life 10 years from now.***   
  
In the ten year future, I believe that the world will become a more convenient world, with people having better access to technology. I might will be working as an engineer that is rather than improving artificial intelligence, an engineer who tries to put artificial intelligence in to our daily life. I hope that in ten year, I will be living in Canada, because it is a place that I wish to live at for the rest of my life because it is not only safe, but full of natural sites that I enjoyed a lot.  
The five trends that are I believe will affect the most:

1. The first trend is the ambiguous boundary between countries Nowadays, the boundaries between country are not as evident as before. We can travel not only faster, but at a cheaper price. Thus, people are traveling a lot for both work and entertainment reasons. So, in the future, we will be thinking about the world as a whole, rather than a universe that is made up of countries. 
2. Robots are coming into our life; things are getting more and more automated. I think in the future, we will have robots or machines to help us do mundane chores. Some service will no longer be provided by human, but by robots. Thus, for these reasons, we will have more stores, shops that open 24-7, they will be operative as long as the machines are working. 
3. The automated cars. I think it will affect me the most. Because with those self-driving cars driving for us, we will save a great amount of time, we will no longer need to stay focused when in a traffic jam. The possibility of a car accident will also decrease greatly because of no human factor involving in driving. So we will be able to travel faster and more efficiently, meanwhile we can manipulate the time traveling to better use. 
4. The world will be taken over by massive companies, most of the jobs available will be provided by the same few companies. We will have to chose one of them to work in for the rest of our lives. So in the future, I hope I will be able to get into an international company as early as possible, so that I can obtain a position before others. 
5. A more ubiquitous use of 3D printer. Things that we need in our life might be printed at home when we needed. We no longer need to go shopping for our daily need. What it takes is just a click on our devices, and the product will came out of the printer, precisely with the dimensions we need. 


***Q2: group presentation on fake news***  
   
***Q3: Select one news story published on https://ground.news/ and read all the different sources. Summarise the different views in the sources in bullet point format in your diary.***  

The article I chose is about the recent migration issue. Recently, 5 migrant buses arrive in NYC as Mayor Adams' tent city construction begins.   

* some talks about the dirtiness that it will bring to the city
* some mentioned the concern that the crime rates might increase
* some of them talked about the concerns of local citizens
* few mentioned the price that cost to build such tent city
* few talks about the benifit of those tents such as putting them in a single area to better manage them.
* none of them mentioned the political reasons
* none of them mentioned any other politican's point of view


# 2022-09-29 #

* money has to be stable, if it flucutates too much i.e. bitcoin, it might not qualify for being a currency
* Today we learned about how putting your money is an unwise choice
* Countries are usinng debt as a method to force the national back to print money for them
* I learned about the power that the dominate currency has.
* I think it is a extremely complicated system that we will not able to understand fully in a 3 hour class.


# 2022-10-06 #

* We should check our data and make sure it is reasonable before making it into graph
* The mark of the data should be more reasonable, which means that it should not be too detailed or too loose
* Borrowing money  from your own country is safer
* Printing money will cause inflation, it is something like an indirect tax on you (the money you put in bank just evaporate)

Describe three fictional stories, e.g. currency, nation, god, marriage, and career,that you believe in and how they have affected your life at some point during the last week. (You must give a tangible example of how the story affected you within the last week.)

1. Seeing the movie about a marriage, the husband and wife are constantly having conflicts on the tiny little problems. This make me feel that I will have to be more patient, and be more tolerable, when having a relationship with others. Or else, some day, anything that my intimate other did will look irritating to me. So I went back and talk to my girl friend with more patience than before.
2. I have read a short story about an the end of the earth, which got suggested to me because of the recenty earthquake. I realized that faceing the destruction of the nature, human has no power and a low possibility to win. So this week, I did my recycles properly, to contribute to saving the earth.
3. After watching the movie "intestellar" I feel that as an individual, we have minuscule power, but human are meant to be working together. Thus, last week, I reall forced myself to be more actively discussing with my group members because I believe that coworking with others will achieve more than three individuals.


# 2022-10-13 #
* We are having a brand new discussion on the new topic.
* I feel like I still have a lot to learn about how to live a healthy life.


# 2022-10-27 #
* I did not participated in this class because of being stressful recently, thus not feeling like to listen to things related to depression in case I start to have those kind of thoughts. 


# 2022-11-03 #
* Today's class is about how we should help others out when in a depression
* Most of us agreed that the best way to help is by accompaning them rather than forcing them to talk
* We all agreed that when things are getting out of control, we should let their intimate others to help, we are not capable of taking the full resposibility
* I learned that we should not pamper their unreasonableness or the boundaries between some who is there for support and the one who is responsible for all others' feelings will be vague.
* I feel like we should have sometime in class to discuss or topic



# 2022-11-10 #
* Today we have viewed other's presentations
* I really like how others deliver their topic
* The discussion on the construction time is really interesting, because it is a topic that is related to our life
* I found that if we spend some time observing the surrounding, we will find a lot os things that can be improved
* it is only when everyone are willing to improve, our life will be better 



# 2022-11-17 #
* I felt that different attitude among the group members toward their works lead to different results
* one of our groupmate tell us that he have dropped the class a day before the class
* we did not know what to do at fisrt becuase he let us know pretty late
* both me and the other teammate is busy preparing for our midterm, we can not find time to do his part of presentation for him.
* I think it will be better if the group mate let us know earlier so we will have time to take action
* The recent election made me see the problems of this traditional approach
* lots of candidates march on the street and scream with microphone and speakers
* insanely annoying
* this week in the class we discussed the reduction of co2 
* we have also cover the reduction of noise in the class


# 2022-11-24 #
* I think the instructions for this week's presentations is not clear
* Our group spent 30 minutes discussion what the quesiton acutally want us to do
* I feel that there is too many restritions on the topic
* it is hard to find something that can 100% be executed and see the results 
* Some groups are just talking about the concepts, but not the actions
* two videos in class 
* i feel a little anxious not haing my phone with me

# 2022-12-01 #
* I do realize some of the groups are just not filling the spreadsheet with thorough thoughts
* professor's kid is having fun haha
* Our group did not have the chance to present but I think its fine
* Some of our group members did not participate because of their exams


# 2022-12-08 #
* This time all of them participate
* The presenter of our group somehow got missing, yet he did upload all the things
* the interview had spent us a lot of time
* hard to find someone willing to be interview and post on youtube
* unable to participate in the second half of the class because have to go and meet a professor from another department for some requests

# 2022-12-15 #
* leave of absence today
Diary homework:

### 22-12-15 Thu ###
Unsuccessful

Did not got enough sleep yesterday thus I felt drowsy the whole day

I should go to bed on time and get up earlier to finish the job instead of doing it late in the night
	
### 22-12-16 Fri ###

Successful

Know the holiday will be coming, I have finished all the things I planned so that I can enjoy my holiday

I should keep this momentum in weekdays
	
### 22-12-17 Sat ###

Successful

I do not have pressure doing or finishing things today, I somehow ended up finishing early

I feel this king of atmosphere indeed boost my efficiency

### 22-12-18 Sun ###

Semi-Succussful

Regarding I finished lots of job yesterday, I have gotten a little bit lazy and thinking of laying on the bed for the whole day

I should not procrastinate because the more I did not finish during the weekends, I will have to do during the week days

### 22-12-19 Mon ###

Unsuccessful

Today I have went to work, when I got back home from work, it is already 7pm, I am left exhausted and frozen, thus I did not did much and went to bed earily

I am trying to look for a way to let me not felt exhausted after work

### 22-12-20 Tue ###

Semi-Successful

Having a final on Wednesday, I got home at the same time from work as yesterday, but I knew I have to prepare for the test, thus I force myself awake for the whole night

I should prepare for the test earlier, I should just have toi review rather than studying all the materials in the weekend

### 22-12-21 Wed ###

Successful

Finshed up the presentation for the group and rehersaled multiple times to make sure I will finish all the works tomorrow

efficient because of listening to music that is calm and nice


# 2022-12-22 #
* Today I presented the for our group
* did the presentation in 4 minutes and mentioned everything I'd like to mention
* Forgot to dress for the class



# 2022-12-29 #
* we read our diary
* there is an interesting survey on who have the Instagram account or have a Facebook account we need to stand up. and 95% of people are standing up.


# 2023-01-05 #
* today is the last day of class, i really enjoy the class this semester
* i like how we work together and how teacher give individual advice to each group

# Bonus Points #
1. Know the current world demographics

   Go travelling around the world
   
2. Ability to use demographics to explain engineering needs

   Think critically about the issues we observed in our daily life, then further think what is the relation with the environment
   
3. Understand planetary boundaries and the current state

   Go and draw a planetary boundary graph on the current status of NCKU so it will be easier to identify and learn the concept
   
4. Understand how the current economic system fail to distribute resources

   to look for research papers on the topic and Ted talks about environmental issues
   
5. Be familiar with future visions and their implications, such as artiEicial intelligence

   See how AI has been implimented in our daily life such as the creation of photos
   
6. Understand how data and engineering enable a healthier life

   Ask how data scientists use the data they collected to make life diffferent both mentally and physically
   
7. Know that social relationships gives a 50% increased likelihood of survival

   To look thorough the history of human civlization and see the difference between living alone and as a group
   
8. Be familiar with depression and mental health issues

   go to talk with an advisor about my issue
   
9. Know the optimal algorithm for Einding a partner for life

   Just go and try, as you try you will know what fits you what does not
   
10. Develop a custom of questioning claims to avoid “fake news”

    go and look for fake new websites to see if I can tell which part is fake
    
11. Be able to do basic analysis and interpretation of time series data

    To review the presnetations professor used in class and see how professor interpret time series data
    
12. Experience of collaborative and problem based learning

    To go and look for experince to work with a team such as clubs, competitions
    
13. Understand that professional success depends on social skills

    Go and talk to successful engineers about how they think about scocial skills and how they benefit from it
    
14. Know that the culture of the work place affect performance

    During my intership, go and ask others how they work with people from different countries