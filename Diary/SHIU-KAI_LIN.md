This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #

* I lack the knowledge of the world facts(only get 40 points in the exam).
* There are to many documents in this course,and we can get all of them through four ways(just below)
* 1.GIT repository for sharing text files and code 
* 2.Google drive for sharing presentations and other material 
* 3.Nordling Lab – ProfSkills web page 
* 4.Google sheets for signing up and reviewing scores)
* Because prof don't release slides before class, so I have to pay attention and take some important notes in class unless to miss important ideas.
* 
# 2022-09-15 #

* Git is a cool digital tool I never used before
* It is a great idea to comment on where your data come from in your ppt.
* The presentation of my peers are really amazing! I should look forward to them.

# 2022-09-22 #

* I think after ten years,I will still live at Taiwan and doing like an R&D about ME or maybe a team leader of an engineering teamI, the work I will do probably is to develope some brandnew vehicles, from design to finally make it, and this bussiness can truely help many people.
* I am now cultivating my mindset,learning methods,selfmanagement skill,liberal arts,and healthy.
* Start from mindset,I want to train my mind in all kinds of way, meaning I hope myself can have an openmind to face challange.
* I can have enough courage to face myown unperfectness, also face any bad things in life. Taste these feelings,and progress gradually with an active heart.
* Second thing is learning methods, We learn things to solve many problems in our ownlife, and cultivate this skill can make myself treat many things in a better way.
* The third thing is selfmanagement. I hope I can treat my time wisely,and always know what is the reasons I live so hard, also help me focus my time and will on important things.
* Fourth is liberal arts ,skill is really important for us to survive in this world, But I wish myself notonly be a technical talents. 
* I hope I am capable of having different kinds of skills to solve more complea problem or think some good idea and insight.
* The last thing is experience.“Nothing compares with the journey on your own feet” ,it is  important to feel this huge world by our action,and I think this experience will be engraved in my heart.

# 2022-09-29 #

* The tooic of todav is verv interesting ! I wish that I can understand more about monev.
* Finally finish the first round representation, hope I can do well on next time and cultivate my presenting skill by the chance in this semester.
* Look forward to meet in person next week!

# 2022-10-09 #

* The fiction l want to talk about is from a famous book called 「the secret」
* ,l read this book when l was little ,even couldn’t understand its meaning ,
* but a long time after, l heard a podcast is talking about law of attraction and is talking about the power of it.
* Then I go back and review this book.And get the courage back to fight back self doubt.
* This is a good way to face any challenge,even it looks impossible ,l can believe in myself and find any possible way to achieve it.

* How to spot depression?
* First we need to know that not everyone express depression in the same way.
* 1.seem more sad or tearful 
* 2.talk about feeling guilty, empty, or worthless more often than usual
* 3.seem less interested in spending time together or communicate less frequently than they normally would
* 4.get upset easily or seem unusually irritable
* 5.have less energy than usual, move slowly, or seem generally listless
* 6.have less interest in their appearance than usual or neglect basic hygiene, such as showering and brushing their teeth
* 7.have trouble sleeping or sleep much more than usual
* 8.are less about their usual activities and interests
* 9.experience forgetfulness more often or have trouble concentrating or deciding on things
* 10.eat more or less than usual
* 11.talk about death or suicide
* How to help?
* 1.Start a conversation
* 2.Help them find support
* 3.Learn about depression on your own
* The Claims 
* Conversation should be asking questions to get more information instead of assuming you understand what they mean.
* Testable hypothesis
* We can test whether it is a good way to have conversation by imagine how we feel, if someone chat with you in this manner.

# 2022-11-17 #

* Amazed by Professor's high EQ reaction when he knew most of us didn't watch the video he asked us to watch.
* I really like the word he told us on class "It is  ok to be overwelmed by all the task you have to do and feel frustrated that can't do them all perfectly
* ,just keey trying and improving yourself,that's enough."
* I am glad to be asked to answer the question on class,because of this experience,I got more courage and desire to use English to convey my idea.

# 2022-11-24 #

* Having no cellphone with me isn't a big deal to me. And it makes me more concerntrate on class and work.
* Group project from other classmates are all pretty great, making me think more about how to reduce any kind of pollution in my daily life.
* Although doing the project is sometimes annoying when the next midterm exam is comming, to be honest, it truely let me care about sustainabilty more than before.

# 2022-12-07 #
* I finish three graphs(PERT,GANTT,Structure) for the presentation this week,and I think it is really fun to draw them.For the reason that they are absolutely strong tools to help specify the goals.
* Videos and slides offered by this class are treasures,I will read it and use the tool I learned from this lecture on my life.

# 2022-12-15 #

* Course today is greater than before I think,because I always want to get more lecture from proffesor,rather than plenty of annoying activities having in class.
* It is interesting to know the concept of "monetary system is built from human,if you want to become rich,there are a lot of methods you have to implement on the system."
* The book from steven covey is really nice and worth to read many times.I must borrow it from NCKU library in winter vacation.

# 2022-12-23 #
* A. Unsuccessful and unproductive
* B. I feel unsuccessful because I spend all night to finish up  my task but only done few parts of them.
* C. I will try out some ways to increase my productivity and ask myself don't think too much.