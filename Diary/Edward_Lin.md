E14096245 林子耘(Edward_Lin)
# 2023-09-14 #
This is my first day in the class,and I think this lecture is fun and provoking.
Teacher talked about the world we will meet in our life,letting us to think what we should do before things coming.

# 2023-09-21 #
HW1:In the future,we will live in a hige technology world.Thanks to the advanced technology,we are now having driveless car on the road.Thus the car accident rate
is hitting the new low.I am grateful to see this,because Tainan has the highest accident rate in Taiwan.Second,we are now using the clear power,so the environment 
won't get polluted,and the polar bear will have place to live.In addition, the world temperature will decrease,so I won't be hot to death.Third,we now use eletric car in
replace of fossil fuel car.Thanks to the eletric car,street has less noise and air pollution.We can breathe the cleanest air,when we are walking on the street.To conclusion,
I'm excited to see the new world coming.

# 2023-10-5 #
Typhoon day off!

# 2023-10-7 #
In today's class,i have learned the knowledge of money.Teacher talked about the history of money,and introduce where the money come from today.
Also,we watch some video talking about money,but i thought it is boring.This lecture is not intersting to me.I think maybe i am not working now.
All of my money comes from my mother.

# 2023-10-19 #
The first offline lecture is awesome.
The presentation temas this week is really great.
Now I know more economic situations from different countries.
First story: Forrest Gump. The first movie that I would like to describe is Forrest Gump, a movie based on the novel of the same name about a slow-witted man that never thinks of himself as disadvantaged. 
Thanks to his mother’s support and his undying passion, leads him on an extraordinary journey of life.
This movie inspired me that whenever I feel tired about college, I get to remember that I'm very proud to be where I am right now and this keeps me wanting to finish it.
Second story:The hummingbird project. Moving on to the next movie, the hummingbird project, is about two friends that are in a high stakes game of big money trading, where winning is measured in milliseconds.
They dream of building a straight fiber optic cable connecting cities that would make them millionaires.
This movie taught me about finishing what I've started until it's complete, no matter how long it takes and whether we win or lose.
Third story:13 going on 30. The last film I would like to talk about is 13 going on 30,
it is a movie about a kid that wishes so badly to be popular and once she reached that point, he was opened up to new points of view in life that would later make her a wise person.
This movie is really relatable in my life as I have so many unchecked wishes but along the way, sometimes it's nice to appreciate what we have and dont always adhere to future plans.

# 2023-10-26 #
In today's lecture we learned how to keep us a healthy life.The most interesting thing for me  is the Ted-talk by Wendy Suzuki.She said that it is simple to keep our brain's health,enhance the volume of brain
and make our response time faster.All we need is exercise.We should normally work out three times a week ,and ensure we exercise more than 30mins every single time.Knowing this thing ,it is important for me ,because
i am a student that need a lot of attention on my work and abundant memories to handle all of my test.Fortunately, i had the habit of working out for almost one and a-half year .I am on the right way of achiving my
brain's health.In another way,i think it is important to encourage your parent to do more exercise,because the elder are prone to Alzheimer's disease and depression.More exercise they do less proportion of getting 
the disease.Prevention is better than cure is the thing we all knew. Not to mention the disease that are not curable. 

# 2023-11-02 #
The most important thing in everyone's life is to live meaningfully not to chase happiness.Otherwise you will feel boring if you only do something happy.
The most important thing is being a good listener when you meet someone depressed not to be a teacher.Just listening not to give any suggestion is the most useful way to calm them down.
How to live meaningfully is a worthy thinking thing.It worths us to look for the answer for our lifetime.

# 2023-11-09 #
Creativity as an alternative to death is very real and true.
It is not always the best way to let things free, although it is really convenient.It may make some trouble to people at the end.
People need time to learn how to communicate with other people. It also costs time to make a deep connection with others.
Nothing worth having comes easy.

# 2023-11-16 #
We listen to many group's presentation.It is intersting for me to have a chance to communicate with other classmates.We work together to get one thing done.
In my group,we were talking about how to use renewable energy or renewable source.I have found that people can use empty oyster shell to protect the coast line.
Because Taiwan is a country that is nearby the sea,we are prone to be affected by the Seawater backflow.If we take this way to make our coast line better,the effect will reduce.

# 2023-12-07 #
I'm sorry for forgetting to write the diary,but i remember this time.First,i want to apologize to my teammate.Although she didn't tell me to do the presentation,she still finished the whole presentation.
Second,we listen to some presentation this week.But this week's topic was not my favored,i didn't pay much attention to it.However,i still put myself to the discussion part.I think this class is not like 
the original course in ME .It is much more like a English conversation class , and the topic is about things that we may meet in the future.

# 2023-12-14 #
In this week, i made a presentation with my teammates.We made a video talking about how the plastic bag pollute our environment,and how can we use reusable bag to protect the Earth.
It's important that everyone should bring a reusable , or we may make a plastic trash ,when we buy something from the vendors. Also, in this week's group work, i start to learn something about
boundary.I choose biodiversity to fit our topic.Because if we produce too much plastic pollution to our environment, the pollution will go through the food chain and eventually make damages to
humanbeings. This is because the dioxin which is called the century toxic can not easily be break down.

# 2023-12-21 #
This week we talk about how to success.As a student,i am afraid to fail.I think this is why we become so nervous or stressful when encountering the challenge.But there is a old saying goes: failure is the 
mother of success, we should not afraid of failure, instead we need to accept.Keeping this in mind, we can accumulate a lot of experience,and we are not prone to be hurt by the failure.In this semester , i am
now preparing my master test.I am very afraid if i can't pass the test.But after this class, i release my stress, and find a new way to deal with it.I am now thinking that if i can't pass the test this year. So what?
i am still alive. i still have another chance.
