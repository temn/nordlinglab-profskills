This diary file is written by Alan Chen E14101105 in the course Professional skills for engineering the third industrial revolution.

2024-01-04
Course: In this lecture, we have to share our idea about a challenge we want to tackle. Our group tried to consult a 
professionalto help us with the project. Moreover, the instructor also allowed us to debate why our challenge 
is the most relevant among others, and did a poll to collect our opinions on these challenges.

2023-12-28
course: In this lecture, each group presents their idea about a problem they want to solve. Our group tried to find a 
professional to give us some advice to complete the project. In addition, the instructor also gave us a chance to
argue why our problem is the most important among others, and conducted a survey to gather our feedback on these problems.

2023-12-21
course:In this lecture, I have learned that results of two different voting system, one anonymous, and another is non-anonymous 
doesn't  have substantial difference. Anonynomus voting comnceal the identity of voter, enable he or she to express their notion
more freely. I think that anonynomous system is more preferrable to improve our disscussion enviroment.

2023-12-14
course:In this lecture, all group present their idea about some issue they intend to solve. Our group tried to find 
proffesional to give some recommendation to finish the project. Besides, instructor also give us an opportunity to debate 
why our issue is the best among ohters, and hava a poll to collect our opinion toward these issue.

2023-12-07
course:In this lecture, profrssor mix small group into bigger group, and we have disscussed the topic of our presentation. 
Besides, I have presented about freedom of press in each country, and find the correlation about how media report and 
the enviroment for media. In the topic about Gaza, most country report it neutrally, and there is no significant
bias.


2023-11-30
course:In this lecture, we spent most of time on presentation as we did like two weeks ago.Besides,
I learned about how to reach the consensus. It is an important skill when we cooperate with teamates 
and colleague in the future. How to conclude the conclusion is major difficulties when we confront conflicting opinions. 

2023-11-23
course:In this lecture, many group introduce some regulations about how other country confront traffic noise
and how law enforcement be implement.Besides, the presentation in the clips talking about the relation between 
people and mobile device and how adolescent develop relationship with other people. In the last of class , 
I discuss with teammate how to keep connected and free.


2023-11-16
course:In this lecture, we make our big presentation about how to decrease traffic noise. Our group members consider that substitute 
combustion engine ehicle to evs will decrease the noise.Besides,other group also present about the health or how to make product sustainable.
I think that listening people's presentation can improve an ability to understand key point quickly.


2023-11-09
course:In this lecture,depression doesn't have notable symptoms in a few cases, and it will makes it difficult to diagnose.Besides,the 
TED talk present on the lecture tell us that forgiveness is a difficult task for people who suffer from some tragedy and difficulties, but 
forgiveness can heal both yourself and people who harm you.


2023-11-02
course:In this lecture,I learned about mental health illness such as depression, anxiety and suicide.Besides,we also cooperate with other team
to combined our powerpoint and dicuss our viewpoint about how to keep us healthy.Nowadays,metal health has pretty impact to our daily life.We
must take care of our friends and family member if he or she suffuer from these modern civilized illness.

2023-10-26
course:In this lecture,I understand the definition of knowledge,well-justified true belief.Besides,the presenter in the TED talk
consider that our health insurance system must transfer from sick-care system to health-system.The second presenter tell us that
exercise is necessay to keep us health and could protect your brain from incurable diseases.


2023-10-19
course:In this lecture，I understand what is the difference between nationalism and fascism,and why extremism may become more influential 
in our daily life.Since some radical comments or activity will spread quickly through social media,and tech company utilize algorithm to
make user be stricked in echo chamber.As a reusult,people won't have opportunity to exchange their idea with people who have 
different opinion.


2023-10-12
course:In this lecture,I understand properties and definition of money,which we use everyday and most people pursue.
Beside,some financial concept such as leverage、inflation、cetral bank or loan which are common but unfamiliar terms also introduced in class.
This lecture construct basic concept of finance and make us understand the relation between money and daily life.



2023-09-21
course:The lecture taught us how fake news and information influence and spread, 
and how they can have a huge impact on politics or other parts of our life. 
Besides, I learned how to cite references by ISO standard correctly.

Q:In your Diary for this week, write a 400-600 word summary of
what you think your future will look like and why it will be so 10
years from now. Start by defining where you will live and what
work you will do. Then describe the five current trends of change
that you think will affect you the most and their effect on your
life 10 years from now.

A:I will live in Tainan, Taiwan, where I am currently studying mechanical engineering at National Cheng Kung University (NCKU). 
I will work as an engineer in a company that specializes in designing and integrating innovative machines and devices. 
I will enjoy the challenge and satisfaction of solving complex problems and creating useful products.
The five current trends of change that I think will affect me the most and their effect on my life 10 years from now are:

•	Automation: Automation is the use of machines, software, or systems to perform tasks that normally require human labor, such as manufacturing, 
transportation, and service. It can increase efficiency, quality, and safety, as well as reduce costs, errors, and waste. For example,
automation can enable smart factories, self-driving cars, and robotic assistants. 
Automation will affect my life by enhancing my skills and knowledge as a mechanical engineer. 
It will also create new opportunities and challenges for me in the field of automation technology.

•	Biotechnology: Biotechnology is the application of biological processes, organisms, or systems to produce or modify products or services for human use.
It can involve fields such as genetics, bioengineering, biomedicine, and bioinformatics. For example, biotechnology can enable gene editing, tissue 
engineering, personalized medicine, and synthetic biology. Biotechnology will affect my life by improving my health and well-being as a human being. 
It will also raise ethical and moral issues that I will have to consider and address.

•	Cybersecurity: Cybersecurity is the protection of computer systems, networks, and data from unauthorized access, use, or damage. 
It can involve techniques such as encryption, authentication, firewall, and antivirus. For example, cybersecurity can prevent cyberattacks,
data breaches, identity theft, and ransomware. Cybersecurity will affect my life by ensuring my privacy and security as a digital citizen. 
It will also require me to be more vigilant and responsible for my online activities and behaviors.

•	Education: Education is the process of acquiring knowledge, skills, values, and attitudes through formal or informal learning. 
It can be delivered by various methods, such as classroom teaching, online learning, experiential learning, and lifelong learning. 
For example, education can provide academic degrees, professional certifications, personal development, 
and social interaction. Education will affect my life by expanding my horizons and potentials as a learner. 
It will also enable me to access more resources and opportunities for my career and personal growth.

•	Decentralization: Decentralization is the process of distributing power and resources away from a central authority or organization. 
It can be seen in many domains, such as politics, economics, media, and technology. For example, blockchain technology enables peer-to-peer 
transactions without intermediaries1, while social media platforms allow users to create and share their own content without censorship2.
Decentralization will affect my life by giving me more choices and control over my data3, money4, and identity. 
It will also challenge me to be more responsible and accountable for my actions and decisions.
These are some of the trends that I think will shape my future in 2033. Of course, there may be other factors that I have not considered or predicted. 
Therefore, I will try to keep myself updated and prepared for whatever challenges or opportunities that may arise in the next decade.


