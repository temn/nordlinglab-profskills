This diary file is written by Ivan Strahija N16128410 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-07 #

* The first lecture was great.
* After taking the quiz I was inspired to learn more about current state of the world.
* I don't have any other particular memory from the first class since it was held 2 weeks ago.

# 2023-09-14 #

* The second lecture was good.
* Other students were presenting their groups' homework which was interesting.
* Using GIT for keeping a course diary seems cool.

# 2023-09-21 #

* The third lecture was good. I found the TED talk about spotting fake data very interesting.

* I am hoping that in 10 years time I will work in R&D team of one of the world leading companies in the field of renewable and sustainable everyday use devices. As there is not a country where I would like to live for a really long period of my life it is hard to predict where I could be living in 10 years time. Hence I can only say I see myself living in a coastal region. For some unknown reason the first country that comes to mind right now is Morocco. Five currents trends of change that will affect me the most will probably be remote work, sustainable design, renewable energy sources, mental health awarness and AI. With remote work opportuinities there will be almost no need for pernament place of residence which really suits the lifestyle I intend to have in the future (digital nomad). As I plan to work in the field of renewable energy sources and sustainable design, trends in that industry will greatly define my work and life in general. I will have to think creatively and develop innovative solutions that reduce resource consumption and environmental impact while maintaining functionality of the product. What is more, engineers will be tasked with implementing environmentally friendly (green) materials which will have to be as good as commonly used ones and that will shift my focus on finding the materials that satisfy customer needs and sustainable enivornmental guidelines. Mental health awareness will be significant for me due to high-pressure work environment in engineering field in general. Long working hours, the need for precision, innovation, and problem-solving are the key aspects of an engineering position so it will be crucial for me to be aware of the consequences it would have on my life. AI will be vastly involved into everyday tasks of almost every human being, but crucially, it will help automate repetitive tasks, analyze massive datasets, and even suggest innovative solutions to complex problems which will allow humans to concentrate more on the emotional side of their lives and, as mentioned before, help people focus on their own mental health. All in all, 10 years from now I expect my life to be more challenging and more rewarding than now since I will be more responsible for the consequences of my actions. Even if none of my predictions turn out to be correct I still think I will be more than happy with my life and the future state of the world. 


* NEWS STORY: https://ground.news/article/croatian-police-detain-9-soccer-fans-over-violence-in-greece-that-killed-1-last-month
* 'RIGHT' sources focus more on the legislative part of the story. Also, in this case they provide more detail about similar incidents that happened before.
* 'LEFT' sources mention and provide the amateur recording of the incident more often.
* 'CENTER' sources are just mentioning the amateur recording of the incident but not providing the footage.

* All in all, a lot of this articles are just copied and don't provide much information about the latest events regarding the incident.

# 2023-10-12 #

* The fourth lecture topic is very interesting to me. I would like to get more into the finance world and sometimes I regret I didn't choose Economics as my major.
* I found Prof. Werner's explanation of the current banking system insightful. Sometimes the information we get from banks can be misleading and in this video Prof. Werner explains how banks actually work.
* I learned about 5 charactheristics of money: fungible, durable, portable, stable, recognizable.

# 2023-10-19 #

* I found the video 'My descent into America's neo-Nazi movement & how I got out' very inspiring. It is not hard to get under the bad influence when you are at your worst, but strong personality is required to get you back on the right path. The speaker shared his life story which was very inspiring and he had many different twists in his life. What is important is that he is doing well now and he is actually helping others who got the similar problems as he did before.
* Also it was interesting to learn about the economies of other countries when our colleagues were presenting their homework. After that I was curious to explore the same dataset about many other countries.
* After today's class I realized I should fouces more onto objective reality and try to ignore human-made concepts of virutal reality.

# 2023-10-26 #

* I found the data collected in researches about stress and anxiety very interesting. Also I think that is a good idea to do a similar survey among NCKU students and data that was presented about NCKU students seems more positive. 
* I think that we should have more discussions about philosophical topics such as knowledge. It would be interesting to hear the general opinion about moral and existential questions.
* In the near future I could imagine the whole world switching to the subscribtion based health care service since this business model is very popular in general right now.

# 2023-11-02 #

* It was though to discuss depression and suicide in the class since these topics are very sensitive. Although I didn't have any close friends that were feeling depressed or suicidal I feel that the percentage of depressed people in our age group is worringly high.
* Hearing stories from people that were depressed but now are doing great can be aspiring and I think it's a good way of showing others that are having similar thoughts or problems that there is hope.
* I feel motivated to do our final group project since we will have the opportuinity to interact with even more people.

# 2023-11-09 #

* I can relate with the research that showed how almost 50% of the newly hired employees quit during first 2 years of their first job mostly due to personal and social reasons. Since I started my last student job at fairly respected company in Croatia last summer, I have already witnessed 3 recently hired employees quit mostly because of social issues. Even though the company cluture is very inclusive and tries to iniciate friendly events where employees can socialize, sometimes is hard to fit in such environment, espeically when there is a strong and coherent friend group already formed and new employees feel like they are not a part of it.
* The research that showed how we instinctively make decisions and then rationalize them shows how easy it is to think we really know what we prefer when it comes to making simple, non-life changing decisions. When it comes to moral decisions I still think I would reason before making the decision. 

# 2023-11-16 #

* It was nice to hear presentation and ideas from the other groups. It is nice to work on finding a applicable solution for a real world problem.
* Our group should probably try to focus on solutions that can actually be implemented.
* I am looking forward to the next group project presentations and hoping other groups make progress with their ideas.

# 2023-11-23 #

* This week's homework presentations were more interesting because of the comments and questions after the presentation. Ususally not a lot of people listen to the presentations but I feel like when there is a disscusion students are more interested and engaged.
* In my opinion, living authentically is crucial, valuing real experiences over a digital persona. Online social networks should serve as tools to enhance life, not become its centerpiece, allowing us to connect meaningfully while preserving the richness of offline moments.

# 2023-11-30 #

* This week groups were presenting their action ideas and it was positive to hear that we can influence the life of induviduals and society as a whole with some of the presented ideas.
* One of the students still did not find who his group members are even though they made a presentation about action ideas which I found quite funny.
* During the lesson we left our phones on the teacher's desk and I was expecting to be reaching out for my phone much more often, but I only did it few times (4). It is interesting to me how we often don't feel safe if we go out without our phone and state this as a reason for always bringing the phone with us.

# 2023-12-07 #

* This is the first time I forgot to update my diary for this course. Although I don't feel anxious about it, I find it quite interesting how I thought about it and actually just by thinking about it during the week I thought I actually did it.
* I remember we talked about planetary boundaries and it left we quite suprised that the current state of the world is so over the proposed limitations. We have to realize that we are living in the dangerous period and if we don't try to adapt we maybe not survive.
* Although I don't want the human race to extinct, I find it quite intersting to think about what would happen on Earth after that. 

# 2023-12-14 #

* Hearing the presented actions from other groups made me realize that not only our group will have a problem with actually implementing this ideas and changing the life of the locals.
* I think that allowing students to choose freely (no available topics) would make us think more about the things that we actually want to change in the world and then students could be more motivated to go through with their actions.
* The video we started to watch at the end of the class was very intriguing and made me want to hear and know more about the topic.

# 2023-12-21 #

* 2023_12_22 Friday:  
 	A: Successful and productive  
	B: I felt both successful and productive becasue I finished 2 projects today that I needed to submit until the end of the week. Besides that I had enough time to hang out and enjoy the nice weather.  
	C: To be more productive tommorow I will try to wake up earlier than today, at least that should not be hard.  

* 2023_12_23 Saturday:  
 	A: Successful and unproductive  
	B: I felt successuful from the morning since I achieved my goal of waking up earlier today. Even though I woke up early, I spent most of the day doing nothing so I feel unproductive.  
	C: To be more productive tommorow I will set some specific goals and checkpoints for my assignments so that I can track my progress easier.  

* 2023_12_24 Sunday:  
	A: Successful and productive.  
	B: I felt more productive than on Saturday since I set some goals for my projects and successfully accomplished them.  
	C: To keep the level of productivity for tommorow I have set myself some new goals, although I plan to be less productive tommorow since it's Christmas.  
	
* 2023_12_25 Monday:  
	A: Successful and unproductive.  
	B: I felt successful since my day was great and I enyjoyed spending it with my friends. I was not productive since I didn't even do the small tasks I planned to do. I don't mind since today should be a holiday.  
	C: To be more productive tommorow I'll set some bigger goals and try to finish tasks I should've done today.  

* 2023_12_26 Tuesday:  
	A: Unsuccessful and productive.  
	B: Today I felt totally unsuccessful since my friends told me I did wrong tasks for homework. Apparently there is a similar undergraduate course to the one I take and on File Managing course website it was not really obvious that one folder is for graduate and the other folder is for undergraduate students. I'll have to do this again tommorow, properly.  
	C: To be more successful tommorow I'll check requirements twice before I start working on my assignments.  
	
* 2023_12_27 Wednesday:  
	A: Successful and unproductive.  
	B: I finished my homework so I felt successful. Even though I have exam tommorow I found no motivation to study for it. I hope it goes well.  
	C: To be more productive in general I'll relax and enjoy my last days on exchange with my friends. I probably did enough to pass all my courses here and I don't really care about the grades I get since they are not relevant for my studies.  
