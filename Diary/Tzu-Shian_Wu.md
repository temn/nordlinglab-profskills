This diary file is written by Tzu-Shian Wu E14101236 in the course Professional skills for engineering the third industrial revolution.

## 2024-09-12 
* The first class briefly introduced the teaching format.
* The teacher speaks at a slower pace, which makes it easier to understand English, but it takes some time to get used to the accent.
* One of the group member I was first assigned to isn't taking the course, so now there are only two of us left in the group, which makes me a bit worried.

## 2024-09-19 
* It introduced perspectives on conflict,making me realize that conflicts can be constructive.
* Introduced useful tools such as the GIT system and Markdown.
* During the group presentation segment, the teacher called on my group.The presentation was my responsibility, while my partner was in charge of delivering it.
She appeared so nervous that she couldn't complete the entire report.

## 2024-09-26 
* Today's class covered detecting fake news, statistics and objectivity, and proper citation methods. I learned useful strategies like checking sources, cross-referencing, and verifying story withdrawals. The group assignment to identify fake images in news stories was challenging but insightful.
* The session deepened my understanding of how data can be misleading and highlighted the importance of academic integrity. The lecturer's engaging style made complex topics enjoyable and easy to follow.
* One classmate creatively used cartoon characters to explain statistical data, filling the classroom with laughter.
On the other hand, one of my group members seemed particularly nervous during their presentation and even forgot their lines at one point. We joked privately that we should practice more next time. These little incidents added a touch of levity to the otherwise serious classroom atmosphere and made me appreciate the value of teamwork even more.

## 2024-10-03 
* This week, we had three days off due to the typhoon, but the wind and rain in Tainan weren’t too severe. As a result, everyone in Tainan was thrilled about the unexpected break. We spent the time indoors playing board games, enjoying delicious snacks, and watching entertaining shows on Netflix together.
* However, the bad news is that my hometown is in Kaohsiung, and this devastating typhoon lingered there for an extremely long time, causing significant damage. Thankfully, my house and family are safe and unharmed.
* Because of the typhoon, we didn’t have any classes this week, making it a relaxing yet slightly regretful week.

## 2024-10-10 
* This National Day holiday has been such a joyful and much-needed break from the usual routine.  
* With no classes or assignments demanding my attention, I finally had the opportunity to relax and spend meaningful time with family and friends.  
* To make the most of this special day, we decided to host a cozy gathering at home. The dining table was filled with delicious homemade dishes, and the house buzzed with laughter and heartfelt conversations as we shared stories about everything from funny childhood memories to recent life updates.  
* In the afternoon, I joined a few friends and headed to a nearby park to watch a local National Day parade.  
* The streets were lined with spectators, all excited to celebrate together. The parade itself was a feast for the eyes, with vibrant performances, traditional dances, marching bands, and patriotic songs that filled the air with energy.  
* Seeing everyone wave flags and cheer in unison created a sense of unity and pride that was incredibly moving. It felt so special to be part of a community coming together to celebrate our nation’s heritage and achievements.  
* Later in the evening, we gathered in the living room to watch the fireworks display on TV.  
* While it might not have been as grand as experiencing the show live, the colorful bursts lighting up the night sky still managed to captivate us. Each explosion of light seemed to carry its own story, symbolizing celebration, hope, and progress.  
* The fireworks gave us the perfect moment to reflect on how far our country has come and to feel grateful for the peace and freedom we enjoy.  
* This holiday wasn’t just about taking a break—it was about reconnecting with loved ones, cherishing simple joys, and celebrating everything that makes this day meaningful.  
* As the day came to a close, I felt deeply refreshed and inspired, ready to face the coming days with a positive mindset and a heart full of gratitude for the people and opportunities around me.  


## 2024-10-17 
* Today's lecture covered the fundamentals of money, the financial system, and income distribution. We discussed what gives money its value—trust, scarcity, and government backing—and examined how financial systems operate. Group tasks included analyzing data on purchasing power and income distribution, making abstract concepts more concrete.
* This session deepened my understanding of the financial world and its societal impacts. I found the discussion on income inequality particularly meaningful, as it revealed systemic issues that influence opportunities and social mobility. It also highlighted the importance of critically analyzing data, as statistics can support different narratives depending on their presentation.
* One classmate impressed everyone with a creative and clear visual presentation of income trends. Meanwhile, our group faced technical issues during our turn, leading to some improvised explanations. It was a valuable reminder to stay flexible and prepared.

## 2024-10-24 
* Today's class explored the topic of why extremism arises. We discussed how fictional narratives, such as nationalism or religion, shape societies and foster the development of extremism. 
* This class made me realize the power of narratives and how they can deeply influence people's beliefs and behaviors. Even in modern societies, we still rely on some myths to function, which led me to rethink the systems we take for granted. Additionally, the discussion highlighted how extremism thrives in environments of fear and misinformation, emphasizing the importance of education and critical thinking in curbing these trends.
* During the group activity, one classmate shared an example of how social media algorithms amplify extreme viewpoints, sparking a lively discussion. 
* Our group didn't have to present this time, which gave us more time to discuss and refine our ideas, providing a valuable preparation opportunity.

## 2024-10-31 
* Today's professional skills course left a deep impression on me, especially the discussions on "living healthy" and the stress management experiment. 
* I learned about the benefits of regular exercise for the brain and how to reduce anxiety through breathing techniques. These insights are not only scientifically sound but also highly practical, directly applicable to daily life.
* One particularly nerve-wracking moment in class was when the instructor randomly selected students to share their diary entries. I silently prayed not to be called on because I hadn’t prepared enough. 
* As other students began sharing their stories, I found their experiences fascinating, especially one classmate who mentioned trying meditation due to class-induced stress. It made me want to give it a try as well.
* I noticed how easily I could be influenced by others' nervous energy during our group presentation. While my teammate was presenting, I was just sitting in the audience, but I felt my heart racing and my palms sweating. This made me realize that my stress management skills still have plenty of room for improvement.
* The final stress management demonstration was incredibly enlightening. The instructor asked for volunteers to perform math calculations while their heart rate variability was measured, then used rhythmic breathing to reduce stress. Although I didn’t participate, I observed closely and noticed how breathing indeed stabilized emotions significantly. Next time I feel nervous, I’ll definitely try this method.

## 2024-11-07  
* Today, we had a group discussion with other teams about our previous homework and prepared for a presentation. The main focus was on how to support friends experiencing depression. Simple yet meaningful actions, like listening to them and being there for them, were highlighted. This topic deeply resonated with me because I often feel uncertain about how to help people going through tough times.  
* The professor reminded us to be mindful of our mental health when supporting others, as taking on too much of their emotional burden can negatively impact us. This struck a chord with me, as I’ve realized how easily I let others’ problems affect my own emotional state.  
* We also watched a video that shared insights from individuals who had attempted suicide but regretted their actions at the last moment. This was a heavy moment for me. It underlined how crucial it is to provide support and empathy to those struggling. However, I also felt a pang of self-doubt—can I truly be the kind of friend who makes a difference? What if I make a mistake or fail to provide the support they need?  
* Despite these doubts, I felt encouraged to learn more and do better. Today’s class made me realize how much impact small gestures of kindness can have, even if it sometimes feels overwhelming.  

## 2024-11-14  
* Today’s class left me with mixed emotions. The anxiety survey results revealed that students this semester are experiencing higher levels of anxiety compared to previous semesters. This didn’t surprise me, as I’ve been feeling the weight of academic stress and future uncertainties myself. Seeing that I’m not alone in this struggle was both comforting and concerning.  
* Some groups shared helpful tips for supporting friends with depression. Listening and simply being there for them were emphasized again, which reassured me that even small actions can make a difference. Onboarding tips were also discussed, such as building relationships outside of work and collaborating with others. I found these insights valuable, as I hope to apply them to improve my own interpersonal skills.  
* Lastly, we discussed rules for a fulfilling life, such as practicing gratitude. This struck a personal chord because I realized I’ve been so focused on challenges lately that I’ve neglected the small blessings around me. Reflecting on these discussions made me feel both hopeful and motivated to shift my mindset.  

## 2024-11-21  
* Today’s lesson opened my eyes to the challenges of the digital era. We watched Jaron Lanier’s TED Talk, which discussed the need to rebuild the internet. Learning how companies like Facebook and Google exploit user data for profit left me feeling both angry and powerless. It’s frustrating to realize how much control these platforms have over our lives while we remain largely unaware of their deeper impacts.  
* The professor explained how Facebook’s business model revolves around user data and targeted advertising. This made me think about how a more ethical internet could be designed. Although the idea of change feels daunting, it also gave me hope that our generation could play a part in creating a fairer digital world.  
* During the class, we worked on developing consensus rules for a better internet. While the discussions were intense, I learned a lot about balancing freedom and responsibility. By the end, I felt a sense of accomplishment, knowing that collective efforts could lead to meaningful change.  

## 2024-11-28  
* Today’s class was both fascinating and exhausting. We reviewed group proposals, voted on them, and discussed the lifecycle of civilizations. It was interesting to analyze how societies recover from dark ages, but the sheer amount of content left me feeling overwhelmed.  
* One highlight was exploring the recovery process after societal collapse. It made me wonder: if our modern civilization faced a similar crisis, would we have the resilience to rebuild? These questions lingered in my mind, sparking a mix of curiosity and concern.  
* Toward the end, the professor asked us to reflect on how often we reached for our phones during class. I felt a bit embarrassed because I realized I had been distracted at times. This was a wake-up call to improve my focus, especially in such knowledge-intensive lessons.  

## 2024-12-05   
* Today’s class was eye-opening yet unsettling. We learned about planetary boundaries, and I was shocked to discover that six out of nine boundaries have already been crossed. The concept of “novel entities,” including human-made materials like plastics and pollutants, was particularly alarming. It made me think about the visible impact of pollution in our daily lives.  
* We watched videos by Johan Rockström and Kate Raworth. Rockström’s discussion on tipping points and the Earth’s limited capacity to buffer change felt urgent, while Raworth’s call for an economy that prioritizes thriving within ecological limits rather than endless growth resonated with me deeply. While these topics were sobering, they also reignited my determination to contribute to environmental sustainability.  
* Today’s class left me both anxious and inspired. The challenges we face are immense, but I felt a spark of hope that collective action can still make a difference.  

## 2024-12-12 
* This week, I officially took a leave of absence to represent National Cheng Kung University in the University Basketball Association (UBA) Women’s Basketball Tournament.  
* While it meant I couldn’t attend my regular classes, the experience I gained from participating in the games was absolutely invaluable.  
* This particular match was against Tainan University, a team that was considered relatively weaker than ours. Because of this, we went into the game with a calm and confident mindset, allowing us to play with a lot of freedom and creativity.  
* We fully embraced the strategies and techniques we had spent countless hours refining during practice, and seeing them executed so smoothly on the court was deeply rewarding.  
* It was one of those moments where all the effort, sweat, and sacrifices we’d poured into training felt completely worthwhile.  
* Our performance during the game was truly something to remember.  
* We moved as a single, cohesive unit, executing plays with seamless precision. Scoring points came effortlessly because our coordination was spot on, but what impressed me even more was how hard everyone worked on defense.  
* The energy on the court was electric, and every player gave their all, not just for themselves but for the team as a whole.  
* Basketball has this amazing way of bringing people together.  
* It’s not just about individual skills or talent; it’s about trust, communication, and lifting each other up.  
* That shared sense of purpose, the way everyone rallied together to achieve a common goal, was incredibly empowering.  
* I think this is what makes teamwork so magical—it connects people on a deeper level and magnifies their collective strength.  
* It reminded me why I love this sport so much and how it’s a metaphor for life itself: no one can succeed alone, but together, we can achieve greatness.  

## 2024-12-19 ##  
* Today’s debate was one of the most engaging sessions we’ve had. I joined the environmentalist group, and our discussion on inequality revealed diverse perspectives. I personally believe inequality stems from an uneven distribution of power, such as in gender dynamics. It felt empowering to voice my thoughts, though hearing opposing views challenged me to reflect deeply.  
* We also began a new topic on success and self-development. Learning about concepts like “Ikigai” reignited my passion for discovering my purpose. The idea of aligning what I love, what I’m good at, what I can be paid for, and what the world needs feels like a guiding light for my future.  
* The professor’s advice to “pick a boss, not a job” resonated strongly, reminding me that leadership plays a huge role in career satisfaction. I left class feeling inspired and determined to apply these lessons to my life.  

## 2024-12-26  
* Thursday’s class turned out to be quite an experience. I didn’t realize we had to “dress to impress,” so when the teacher gave us time to go home and change, I rushed back and put on the nicest outfit I could find. I don’t have many elegant clothes, but I tried my best, and getting two votes for my look was such a nice surprise! It wasn’t perfect, but knowing I did the best I could made me really happy.  
* In class, my group (Group 4) had to present, but we did not discuss who would speak, so we missed the opportunity to report. What stood out for me personally was sharing my own five success factors during my individual report. I talked about making to-do lists, avoiding procrastination, making more friends, staying positive and optimistic, and believing in myself. These are things I genuinely try to practice in my life, so it felt good to share them and reflect on why they matter to me.  
* The discussions during class were thought-provoking, but I couldn’t help but notice some perspectives that I didn’t fully agree with. For example, a lot of people seemed to define success in really idealistic terms, like always achieving big goals or being happy all the time. Personally, I think success is much more about the small steps and daily wins—finishing a task, reaching out to someone new, or even just trying your best in tough situations.  
* One point that surprised me was how often “always be happy” came up as a rule for success. I completely disagree with that. Life isn’t about being happy all the time—it’s about embracing all your emotions, the highs and the lows. Sadness, frustration, and even moments of doubt are just as important as joy because they shape who we are and help us grow. It’s okay to feel bad sometimes; it doesn’t mean you’re doing something wrong.  
* I’ve always loved the idea that emotional wounds need care just like physical ones. If you ignore a cut, it can get worse, but if you clean it and let it heal, it gets better with time. Emotions are the same—you have to acknowledge and process them, even if it’s uncomfortable. Ignoring how you feel doesn’t make it go away, and addressing those feelings can make you stronger in the long run.  
* By the end of the day, I felt proud of what I shared and how I handled everything, even though I wasn’t completely prepared. This class reminded me that success isn’t about perfection—it’s about growth, effort, and learning to appreciate every step along the way.

## 2024-12-27  
* Exam season is right around the corner, but my study efficiency was through the roof today! I managed to organize my notes perfectly, and reviewing them felt so smooth and productive. Everything is clicking into place, and I couldn’t be happier.  
* During class, I was super focused. The professor gave us a pop quiz, and guess what? I aced it with a perfect score! It felt amazing to see my hard work pay off like that.  
* My mood is fantastic today. Sure, the pressure of exams is there, but I feel like I’m on top of everything. Confidence level: sky-high!  

## 2024-12-28  
* The stress of exams is getting to me. I’ve been studying nonstop, and I’m starting to feel so drained. There’s just so much to cover, and it feels like the pile of notes never ends. Am I ever going to finish this? 
* I couldn’t keep myself awake during class today and accidentally dozed off. It was so embarrassing when the professor asked a question, and I was jolted awake. Luckily, I wasn’t the one he called on. Whew!  
* My mood is definitely down. The exhaustion combined with the sheer amount of work is really taking a toll. I hope I can power through this rough patch.  

## 2024-12-29  
* My review sessions went well today. I think I’ve gotten the hang of most of the material, and it feels like I’m finally on solid ground. Every time I practice a question, I think, “Oh, I know this!” It’s such a confidence boost!  
* However, during class, I felt like my brain wasn’t absorbing as much as usual. Maybe I stayed up too late last night, and now my focus is suffering. I need to rest better.  
* Overall, my mood is decent. There’s still a bit of pressure, but knowing I’m on track with my preparations helps me stay calm and motivated.  

## 2024-12-30  
* Panic is starting to creep in. The exam date is closing in fast, and my notes are still not fully organized. Time feels like it’s slipping through my fingers, and the stress is real.  
* In class today, I struggled to understand the professor’s explanations. The content felt so complex, and I couldn’t wrap my head around it. It made me even more anxious about the exams.  
* My mood? Definitely leaning toward frazzled. All I can think about is how much I still need to get done.  

## 2024-12-31  
* Today’s study session was decent. While I didn’t accomplish everything I wanted to, I made steady progress. At least it wasn’t a completely wasted day!  
* I was late to class, though, and it was so embarrassing. The professor gave me a look when I walked in, and I just wanted to disappear.  
* My mood is so-so. With the new year almost here, I hope I can reset my energy and focus on doing my best in the coming days.  

## 2025-01-01  
* Happy New Year! Surprisingly, I had a super productive day of studying. Maybe it’s the “new year, new me” energy, but I was in the zone and checked off so much from my to-do list.  
* During class, I paid close attention and even noted down some golden tips from the professor. I can tell these will come in handy when I’m reviewing later.  
* My mood is great today. 2025 is off to a strong start, and I’m feeling optimistic. Let’s make this year count!  

## 2025-01-02  
* The weight of exams is really hitting me. I pushed through my study sessions, but it was hard to stay focused. Everything feels like such a slog right now.  
* In class, I couldn’t seem to retain much. My brain felt like it was in a fog, and I struggled to keep up with the lecture. It’s frustrating when your body doesn’t cooperate with your plans.  
* My mood is on edge. I’m trying to stay positive, but the looming deadlines are making it tough.  

## 2025-01-03  
* Studying was slow but steady today. While I didn’t cover as much as I hoped, I’m reminding myself that progress, no matter how small, is still progress.  
* Unfortunately, the professor’s lecture today was really challenging to follow. By the end of class, I was still feeling lost, which doesn’t help my confidence.  
* My mood is neutral. Not great, not terrible—just hanging in there.  

## 2025-01-04  
* My throat felt scratchy and uncomfortable today, and I suspected I might be coming down with something. I went to see a doctor in the afternoon, and they prescribed some medicine, saying it was probably just a mild cold.  
* I didn’t get much studying done because of how tired I felt. Falling behind on my review schedule is making me nervous.  
* My mood is a bit gloomy. I really hope I can bounce back quickly because I can’t afford to lose more time.  

## 2025-01-05  
* Things took a turn for the worse. My throat became so swollen and painful that I had to go back to the hospital. Turns out my tonsils are inflamed and even developing abscesses. Great, just what I needed right before exams.  
* Despite feeling awful, I managed to push through a bit of studying, but the progress was minimal. The stress of falling behind is piling up.  
* My mood is terrible. Being sick and overwhelmed is a miserable combination.  

## 2025-01-06  
* The pain in my throat is unbearable. It feels like swallowing razor blades, and even breathing hurts. Studying is out of the question because I can’t focus on anything but the discomfort.  
* In class, I was completely out of it. My body feels too weak, and I can’t process anything the professor is saying.  
* My mood is at an all-time low. I’m physically and mentally drained, and I don’t know how I’ll get through this.  

## 2025-01-07  
* The medicine from the hospital is finally starting to work, and my throat feels a bit better. But the side effects are brutal—my stomach is bloated, my head is spinning, and I feel nauseous. It’s like trading one problem for another.  
* I tried to study today, but the side effects made it so hard to concentrate. It feels like I’m just going through the motions.  
* My mood is flat. I’m relieved my throat is improving, but everything else feels like a struggle.  

## 2025-01-08  
* Things escalated, and I ended up in the ER because the side effects were too severe. They gave me an injection and prescribed additional medication. Now I’m juggling two different sets of pills, and it’s exhausting.  
* I had to take a sick day from class because I felt too weak to even get out of bed. Most of the day was spent resting, with barely any studying done.  
* My mood is pretty low. Being stuck in this cycle of illness and stress is disheartening. I just want to feel normal again.  

## 2025-01-09 ##
* Exam day is almost here, and thankfully, I’ve completed most of my review. The relief of being prepared is starting to outweigh the stress.  
* In class, the professor covered some key points that I’m already familiar with, which boosted my confidence. It feels great to see all the studying paying off.  
* My mood is decent. After everything that’s happened, I’m glad to be ending this phase on a positive note. Now it’s time to give it my all!  
## TASK

* A. A World of Facts, Challenges, and Ideas

1.	Know the current world demographics
I’ve gotten pretty good at finding population data from reliable sources like the UN and World Bank. It’s fascinating to see how much demographics shape the world we live in, and it makes me think more about global challenges.

2.	Ability to use demographics to explain engineering needs
Connecting population trends with things like urban planning or infrastructure was a new skill I picked up. It’s amazing how data can show exactly what kind of solutions people need—it’s made me think more like an engineer.
3.	Understand planetary boundaries and the current state
Learning about climate change, biodiversity loss, and other environmental limits really opened my eyes. It’s a little scary but also super motivating to understand the science behind what’s happening to our planet.

4.	Understand how the current economic system fails to distribute resources
Through real-world examples like water shortages, I started seeing how economics plays a huge role in resource inequality. It’s frustrating, but now I can think critically about possible solutions.

5.	Be familiar with future visions and their implications, such as artificial intelligence
Exploring AI’s impact on industries and society was eye-opening. It’s not just about the tech but also the ethical and social implications, which I found super interesting to reflect on.

* B. Personal Health, Happiness, and Society

6.	Understand how data and engineering enable a healthier life
Looking into technologies like fitness trackers and telemedicine made me appreciate how much engineering and data improve our everyday health. It’s amazing how innovation shapes better lifestyles.

7.	Know that social relationships give a 50% increased likelihood of survival
I had no idea relationships could have such a huge impact on health! Learning about this made me focus more on building and maintaining meaningful connections in my own life.

8.	Be familiar with depression and mental health issues
I thought I knew a lot about mental health, but diving deeper through workshops and readings taught me so much more. I feel more empathetic and better equipped to support others now.

9.	Know the optimal algorithm for finding a partner for life
This was such an interesting topic! Learning about the science and strategies for finding the right partner gave me a fresh perspective on relationships and what actually matters.

* C. Professional Skills to Success

10.	Develop a custom of questioning claims to avoid "fake news"
Fact-checking news stories on platforms like Snopes has become a habit, and it’s made me much more critical and cautious about the information I consume.

11.	Be able to do basic analysis and interpretation of time series data
I started using Excel and Python to analyze data. It was intimidating at first, but after some practice, I feel much more confident working with numbers and patterns.

12.	Experience collaborative and problem-based learning
Working in groups to solve real-world problems taught me so much about teamwork. It’s not just about solving the problem but also learning how to collaborate effectively with others.

13.	Understand that professional success depends on social skills
Social skills are so important! Networking events and group discussions helped me practice communicating and connecting with others, which I know will be useful for my career.

14.	Know that the culture of the workplace affects performance
Studying workplace culture made me realize how much it affects productivity and employee happiness. I’ll definitely pay attention to this when choosing where I want to work in the future.

* Summary

This course has taught me so much about the world, myself, and how to navigate the future. I feel more informed, confident, and ready to tackle challenges with the knowledge and skills I’ve gained.
