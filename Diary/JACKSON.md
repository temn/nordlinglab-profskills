# 2024.09.12 #
•	In the first lecture, I understood the rules of this lecture and how to complete the weekly tasks.
•	I had the opportunity to work in a group and talk with other members, which was a great step forward for me.

# 2024.09.19 #
## Today's presentation is "Will The Exponential Growth Lead to a World of Abundance?" ##
The idea of exponential growth leading to a world of abundance is a fascinating and complex topic. Exponential growth, particularly in technology, has the potential to create significant advancements and efficiencies that could lead to a more abundant world. For example, technologies like artificial intelligence, renewable energy, and biotechnology are advancing rapidly and could solve many of the worlds pressing problems.
However, there are also challenges to consider. Exponential growth in a finite world can lead to resource depletion and environmental degradation if not managed sustainably. The key is to decouple economic growth from resource consumption and environmental impact. This means finding ways to grow economically without increasing the use of finite resources or causing harm to the environment.
# 2024.09.26#
## •	Today's presentation is " Is the World Getting Better or Worse? "##
Improvement Across Various Metrics:
     Homicide, War, Poverty, Pollution: Pinker notes that data shows improvements in these areas over the past 30 years.
Health and Wealth: There have been significant advancements in global health and wealth, contributing to overall better living conditions.
Progress is Not Inevitable:
Continuous Effort Required: Pinker emphasizes that progress requires ongoing effort and problem-solving. It is not a guaranteed or automatic process.
Approach to Global Challenges:
Problem-Solving Mindset: Issues like climate change and nuclear war should be viewed as problems to be solved rather than inevitable disasters
## my future ten years from now ##
Career Opportunities: With advancements in technology and renewable energy, there could be new and exciting career paths available.
Health and Well-being: Continued improvements in healthcare could mean a healthier lifestyle and longer life expectancy.

# 2024.10.17 #
# Money Creation in the Modern Economy by Ryland Thomas #
   Loans create deposits: When banks make loans, they create new deposits in the borrowers bank account, effectively creating new money.
   Misconceptions: Contrary to popular belief, banks do not simply act as intermediaries lending out deposits that savers place with them.
   Monetary policy: The central bank influences the amount of money in the economy through interest rates and quantitative easing.
# 2. Richard Werner: Todays Source of Money Creation #
   Credit creation: Banks create money out of nothing through the process of credit creation.
   Centralized vs. decentralized banking: Centralized decision-making by central banks and big banks can lead to asset bubbles and crises, while decentralized banking supports productive investment and growth.
   Digital currency concerns: Replacing bank credit creation with central bank-controlled digital currency could lead to excessive control and an Orwellian dystopia.
   Solution: Maintaining public money in local community banks and ensuring credit is used for productive purposes.
# 3. How the Economic Machine Works by Ray Dalio #
   Economic forces: The economy is driven by three main forces: productivity growth, the short-term debt cycle, and the long-term debt cycle.
   Credit and debt: Credit allows spending and incomes to rise faster than productivity, but also creates debt burdens that can lead to deleveraging.
   Deleveraging: Reducing debt burdens involves cutting spending, reducing debt, redistributing wealth, and printing money.
   Economic cycles: The interaction of these forces creates economic cycles, including recessions and recoveries  

# 2024-10-24 #
Thats fascinating! Imagined realities can indeed have a profound impact on our lives, shaping our beliefs, behaviors, and even our health.

Regarding the TED Talk you mentioned, its true that exercise has numerous benefits for the brain. Regular physical activity can help prevent Alzheimers disease and promote the growth of new brain cells in the hippocampus, which is crucial for memory and learning.
  
Three fictional stories
The Memory Garden: In a small town, there was a garden known for its magical flowers. Legend had it that anyone who spent time tending to the garden would never lose their memories. An elderly woman named Clara, who was beginning to forget things, decided to visit the garden every day. As she cared for the flowers, she found her mind becoming sharper and her memories clearer. The garden, it seemed, had a way of nurturing not just plants, but also the minds of those who loved it.
The Imaginary Friend: A young boy named Leo had an imaginary friend named Max. Max was always there to encourage Leo, especially when he felt down or scared. As Leo grew older, he realized that Max was more than just a figment of his imagination. Max had helped him develop resilience and confidence. When Leo faced challenges in his adult life, he remembered Maxs words of encouragement, which gave him the strength to overcome obstacles.
The Exercise Experiment: In a futuristic world, scientists discovered a way to enhance brain function through a special exercise program. Participants in the program reported not only improved physical health but also heightened creativity and problem-solving abilities. One participant, a writer named Emma, found that her writers block disappeared, and she began to produce her best work. The program became a global phenomenon, changing the way people viewed the connection between physical and mental health.
	
# 2024-11-07 #
Handling depression can be challenging, but there are several strategies that might help. Here are some tips:

Create rewarding goals: Setting small, achievable goals can provide a sense of accomplishment and purpose.
Find fulfilling activities: Engaging in activities you enjoy can help combat feelings of apathy and self-criticism1.
Exercise: Physical activity can reduce symptoms of depression and improve energy levels.
Socialize: Spending time with supportive friends and family can provide emotional support.
Practice mindfulness: Techniques like meditation and mindfulness can help manage negative thoughts.
Seek professional help: Therapists and counselors can provide strategies and support tailored to your needs.
Regarding Rebeca Hwangs talk, she emphasizes the power of embracing diverse identities. Her experiences highlight how a multicultural background can be a unique strength in a globalized world. By embracing our complex identities, we can foster a more inclusive and connected society.

# 2024-11-14 #
Identify Your Values: Understanding what truly matters to you can help align your actions with your core beliefs, leading to greater satisfaction.
Practice Gratitude: Regularly acknowledging the positive aspects of your life can shift your focus from whats lacking to whats abundant.
Engage in Activities You Love: Spend time doing things that bring you joy and fulfillment. This can be anything from hobbies to spending time with loved ones.
Cultivate Positive Relationships: Surround yourself with people who uplift and support you. Positive social connections are crucial for happiness.
Set Meaningful Goals: Having a sense of purpose and working towards goals that are important to you can provide a sense of direction and fulfillment

# 2024-11-21 #
## "Strawman - The Nature of The Cage": ##
Strawman Theory:
This is a concept often discussed in conspiracy theories and alternative legal frameworks. It refers to the idea that there are two "selves": one is the physical person (the "flesh and blood" person), and the other is the "legal person" or "strawman," a legal entity created by the state for purposes of legal and financial obligations. Some believe that the legal person (strawman) is a separate entity used to trap individuals in legal systems, and that if they can separate from it, they can escape debts and taxes.
Legal Personhood:
The idea of legal personhood is essential in understanding the "strawman" theory. This concept holds that certain non-human entities (such as corporations, trusts, or even governments) can have rights and duties under the law. In the context of the strawman theory, the "legal person" created by the state (often referred to as an artificial entity) is seen as something distinct from the actual human being.
Sovereignty:
This concept deals with the ultimate authority or control over a territory and its laws. In some legal theories related to the strawman argument, individuals seek "sovereignty" over themselves, claiming to be free from the control of the state or legal systems that are tied to their strawman.
Contract Law:
The strawman theory sometimes invokes contract law in an effort to explain how individuals are presumed to have entered into legal agreements (such as social contracts, birth certificates, or government registrations) without their full consent. This may tie into the idea that all people have unwittingly agreed to certain legal frameworks or obligations through registration or legal documents.
Tax Law and Governmental Control:
Related to the idea of the strawman, tax laws and government regulations often become a focal point for discussions on legal personhood and sovereignty. The claim is that the legal person (strawman) is used by governments to impose taxes and other legal obligations on individuals.
Natural Law vs. Positive Law:
Natural law is the philosophy that there are certain universal moral laws or principles inherent in nature or human reason, while positive law refers to laws created and enforced by governments. The documentary might contrast these concepts as it explores the ideas behind the "strawman" and sovereignty theories.
Legal System Overview
# Taiwan:#
Civil Law System: Taiwan follows a civil law system rooted in the Constitution of the Republic of China (ROC) and influenced by German and Japanese legal traditions. In this system, laws are primarily codified, meaning that statutes and codes are the main sources of law.
Legal Framework: The legal framework in Taiwan places a strong emphasis on the rights of individuals (natural persons) and entities (legal persons, such as corporations). The Civil Code and the Constitution provide the foundation for individual rights and obligations, and the country also has a robust set of laws governing contracts, property, and torts.

# United States:#
Common Law System: The United States follows a common law system, which is based on legal precedents established by court decisions. While statutory laws exist (such as the U.S. Constitution and various federal and state laws), much of U.S. law has been shaped by judicial decisions over time.
Legal Framework: In the U.S., the Constitution provides a fundamental set of rights for individuals, but the legal landscape is also influenced by case law, especially in areas like contracts, property rights, and tort law. The U.S. legal system emphasizes individual freedoms but has a more complex relationship with corporations and the concept of legal personhood.

# 2024-11-28 #
General Diary
Professor began the class with some mini experiment "Addiction of phone":
•Turn your phone to mute
•Put it on the table in front of the class
•Count how many times you looked for your phone during
class

•Pick it up after three hours at the end of the class
## 1177 BC: The Year Civilization Collapsed by Eric Cline, PhD ##
# Arguments #         
1.	Complex Globalized World-System in the Late Bronze Age:
The Mediterranean region from 1500 BC to 1200 BC was interconnected culturally, politically, and economically.
Trade routes linked major civilizations such as Egypt, Mycenae, the Hittites, Mesopotamia, and Canaan.
This internationalism created dependencies between regions, fostering a cosmopolitan world but also vulnerabilities.
2.	Collapse of Civilization:
The collapse around 1177 BC affected a wide area, with empires like the Hittites and kingdoms in Canaan and Mycenaean Greece falling apart.
The decline led to a "Dark Age," marked by loss of writing systems, reduced urbanization, and cultural stagnation for centuries.
3.	Multiple Contributing Factors:
Cline emphasizes a "perfect storm" of factors leading to the collapse:
Invasions: The arrival of the Sea Peoples disrupted societies and trade networks.
Natural Disasters: Earthquakes, droughts, and famines destabilized populations and economies.
Internal Strife: Political instability and revolts weakened internal structures. 
Over-reliance on Trade: The collapse of one region’s economy or political structure disrupted the interconnected system.
4.	Historical Lessons:
The interconnectedness of the Bronze Age civilizations made them vulnerable to cascading failures—a cautionary tale for today’s globalized world.
Similar factors such as climate change, resource depletion, and reliance on trade are present in contemporary society.
# Recommendations: #
1.	Learning from the Past:
o	Recognize the dangers of over-dependence on globalized systems and prepare for potential cascading failures in interconnected networks.
2.	Resilience and Adaptation:
o	Build resilient systems to withstand environmental, economic, and political shocks.
o	Develop localized solutions and reduce over-reliance on external trade or resources.
3.	Climate Awareness:
o	Address environmental challenges proactively, as climate change was a significant factor in the Bronze Age collapse.
4.	Preparedness for Complexity:
o	Foster robust and adaptive political and economic systems to handle the complexity and potential fragility of globalized societies.

# 2024-12-05 #
Johan Rockström’s TED talk, “Let the environment guide our development,” discusses the concept of planetary boundaries. These boundaries are nine critical processes that regulate the stability and resilience of Earth’s ecosystems. Rockström emphasizes that human activities have already crossed three of these boundaries: climate change, biodiversity loss, and the nitrogen cycle1.
He argues that understanding and respecting these boundaries can help us develop sustainably and protect our planet. The talk highlights the importance of integrating scientific knowledge into our development strategies to ensure a safe operating space for humanity1 
1.	Planetary Boundaries Framework:
o	Identifies nine critical Earth system processes that regulate the stability and resilience of the planet.
o	Crossing these boundaries increases the risk of destabilizing the Earth system.
2.	The Nine Planetary Boundaries:
o	Climate change
o	Biodiversity loss
o	Biogeochemical flows (nitrogen and phosphorus cycles)
o	Ocean acidification
o	Land-system change
o	Freshwater use
o	Atmospheric aerosol loading
o	Introduction of novel entities (e.g., chemicals, plastics)
o	Stratospheric ozone depletion
3.	Current Status:
o	Rockström highlights that humanity has already transgressed some boundaries, such as climate change, biodiversity loss, and biogeochemical flows.
o	These breaches are driving us into a zone of increased uncertainty and risk for global stability.
4.	Interconnectedness of Ecosystems:
o	The Earth operates as a tightly interwoven system; disruption in one area can have cascading effects on others.
5.	Science as a Guide:
o	Advances in technology and environmental science provide tools to monitor these boundaries and adapt behavior to avoid irreversible harm.
6.	Call to Action:
o	Rockström emphasizes the need for a paradigm shift: development must work with environmental limits rather than against them.
o	Sustainable development practices can ensure the well-being of future generations while maintaining Earth's critical life-support systems.


# 2024-12-12 #
# General Diary #
Jeremy Rifkin’s documentary, “The Third Industrial Revolution: A Radical New Sharing Economy,” addresses the pressing issues facing the global economy, such as resource depletion, declining productivity, and rising inequality. Rifkin proposes a new economic model that leverages digital technology, renewable energy, and a sharing economy to create a more sustainable and equitable future.

In the video, Rifkin outlines how these technological advancements can transform our economic systems, emphasizing the importance of collaboration and innovation. If you’re interested in exploring these ideas further, watching the documentary will provide a comprehensive understanding of his vision for the future.




##024-12-19###
#General Diary#
•	The class began with a short presentation from each group, and I was presenting a topic regarding, “How to forecast technological progress?”.
•	In the class, we had some debate between the student from capitalist view, workers view, and environmentalist regarding “why is the 3rd world industrial revolution?”.
•	The teacher also gave us the short explanation of the framework of how to succeed.
Individual Task - Daily Diary (Friday, 12/20 - Wednesday, 12/25)

#Friday#
•	Productive/Successful
•	I felt productive because I managed to wake up at 8 AM, have breakfast, call my mom back in Indonesia to catch up, and go shopping for outdoor gear and logistics to prepare for hiking Mount Hehuanshan in Taichung. I felt successful because I completed almost everything on my to-do list, except exercising, but I still considered the day a success overall.
•	Winter makes me lazy to get out of bed because it’s so cold, and that keeps me from going out for a morning walk or run. I’ll try to push myself more tomorrow!

##Saturday
•	Unproductive/Successful
•	I felt unproductive because I decided to take things slow before my 3 AM hiking trip to Mount Hehuanshan. I spent most of the day resting and gathering energy. But I felt successful because, by the evening, I felt calm and ready for the winter hike.
•	Next time, I’ll use my rest time more wisely, like reading a book instead of scrolling through social media.

Sunday
•	Unproductive/Successful
•	I felt unproductive because hiking took up almost the entire day, and while it was fulfilling and helped me check off a bucket list goal for 2024, I didn’t have time to work on homework or projects due Tuesday. I felt successful because I finally reached the peak of Mount Hehuanshan despite the freezing cold. Even though I couldn’t see the galaxy, constellations, or sunrise, I felt proud. On the way home, I even stopped by a mosque in Taichung to rest and pray.
•	Honestly, I didn’t take away any lessons-learned from Sunday, but I felt deeply alive—and maybe that’s enough.

Monday
•	Unproductive/Unsuccessful
•	I felt unproductive because I woke up at 12 PM, still exhausted from hiking. I was late to class, and even after attending, I was bombarded with new group tasks and assignments. I felt unsuccessful because I didn’t have the energy to do anything meaningful.
•	Tomorrow, I’ll wake up at 9 AM and start my day with a morning walk and pre-reading my class materials.

Tuesday
•	Productive/Unsuccessful
•	I felt productive because I finished my biomedical materials project and made progress on my group work for Virtual World class. But I still felt unsuccessful because I had spare time and didn’t use it for exercise or to work on my thesis.
•	Tomorrow, I’ll dedicate time to make meaningful progress on my thesis.

Wednesday
•	Productive/Successful
•	I felt productive because I managed to do some revisions on my thesis draft. I also felt successful because I reviewed my Chinese before class and joined a fun Christmas game with classmates during Chinese class.
•	Tomorrow, I want to focus on completing my thesis prospectus more thoroughly.


2024-12-26
General Diary
•	There were several group presented their course project progress
•	Each of us was presenting the step or key to success based on our preferences while wearing formal attire that the professor had told us last week. Professor also gave us some feedback on our outfit and emphasized how important it is to apply it on an everyday basis even if it is a rare occasion in the uni class.
•	Professor also demonstrated how to tie a tie and there were 85 ways to tie a tie based on the wikipedia source.
Daily Diary
Thursday (12/26)
•	Successful/unproductive
•	I am unproductive bcs i get up after 9 AM because the winter season makes me feel a bit lazy and I forgot to list up my to-do list before I sleep. That makes me feel “nothing to do” for that day. However, I was happy bcs I attended an exchange gift party with my fellas and doing some mini night-picnic. The party ended at 1 AM midnight so I expected that I wouldn't be awake in the morning, sadly.
•	I think, biggest lesson for me is to always do some to-do-listing before sleeping and keep the gadget away from my bed while placing useful items near bed, such as a diary and tumbler.
Friday (12/27)
•	Productive/Successful
•	Feels unproductive definitely. After last night party, I woke up at 11 AM 🙁but luckily,I have no class on Friday so I feel like i have a bit free in the morning. And I decided to warm up my body at my dorm balcony after I get up from bed. After that, I had to prepare myself to go to immigration office to extend my visiting period in Taiwan had a short trip to Tainan Fine Art Museum with my friend. I feel like I was unsuccessful cuz at that time I should have had to summarize my thesis Materials project, but not even finished a half of that.
•	I think on the last 3 days, my sleep routine was so messed up and I feel like my 6 AM morning routine should be rearranged again. Avoid drinking caffeine, relax the body and gotta back running routine

Saturday (12/28)
•	Productive/Successful
•	I was almost finished with my final project in one of my courses that I took. Almost every hour, I was doing my work in front of my laptop. I also did some house chores like washing all my clothes. I feel successful bcs all my work that I should have finished was done. I also feel successful because I run at night for almost 20 minutes. I also prepared to go to Hsinchu city to hike Jiali Shan with my Filipinos Friends. I knew that I would mess up my sleeping routine, but guess what? I ticked all my bucket list before my 2024 end, to hike another mountain in Taiwan after I failed to go to Qilai Mountain last week bcs of the bad weather.
•	Note that I was not consistent in my running routine, so for upcoming days and for 2025 the resolution is to give at least 20 minutes doing jogging and running routine, Engaging some activities that reach my fat-burning zone heart beat! Hopefully

Sunday (12/29)
•	Productive/Successful
•	I feel productive and successful, cuz This day was so EXHAUSTED yet FULL OF EXCITEMENT! I was starting my day at 3 AM bcs I had to go to Hsinchu and hike JialiShan in the morning with my Filipino friends. It took a total of 8 hours to go up and down from the mountain surrounded by Japanese Cedar Forest and it was so impressive. I feel successful cuz I got a clear view of the peak of the mountain and it was so sunny! No dense fog and I can see the other peak mountain!
•	For me, I should increase my nature hike routine. Giving me 1 day without a signal on my phone was so refreshing for me. Next year, more mountains to go!
Monday (12/30)
•	Unproductive/unsuccessful
•	Feels unproductive cuz I just arrived in Tainan from Hsinchu at 3 AM in the morning, and I had a class at 1 PM, so I just woke up at 11.30 AM. Nothing to do cuz my leg was so stiff. Feels unsuccessful cuz I don’t do anything.
•	Note for this day is: Do your best at everything! 😀Limit your screen time!
Tuesday (12/31)
•	Unproductive/unsuccessful
•	Because my class that day was dismissed since my professor had to go to Japan, it was my time to get a full holiday and watched new year countdown in Kaohsiung. Not really feeling unsuccessful because it was just an ordinary day and again, I still messed up my sleep routine, sadly.
•	My note for upcoming days is Arrange and give more space to have a good duration for sleep!
Wednesday (1/1)
•	Unproductive/unsuccessful
•	Unproductive cuz I still cannot control my sleep duration back to normal since I ended my activity at 2 AM. I was expecting that my body could prevent me from getting up in the morning. I also felt unsuccessful bcs I had nothing to do, no listing what I should do in a day. But, finally I did my house chores to wash my clothes.
•	Still progressing to try to arrange and give attention to my sleep routine and doing some exercises at least once in a day!




