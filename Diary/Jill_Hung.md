
# 2022-09-15 #

* It's not easy for my brain to address a bunch of information in english for straight 3 hours.

* Hope I could keep up with the class next time.

* But the coordinate of conflict view is quite interesting, and knowing how the concept affects our world and society is also surprising.

* The classmates share some cool information about the topic sentence this week.

* I'm inspired a lot.

# 2022-09-22 #

* Part 1:

* I have a plan for studying master’s degree in Germany after graduating from NCKU. 

* I assumed that I may feel frustrated at first since everything there is so different, but it won’t take me a long time to get used to the place.

* After 2-4 years for education, I graduate from the university, and start my career as an engineer there.

* It is confusing for me sometimes after working for a period cause I keep imagine what my life will be if I stay in Taiwan, instead of going abroad.

* But I soon get rid of the thought due to the hustle life. Few years pass, I turn to 32 years old. 

* I climb up to a higher position of the company, and the poverty I own allows me to buy a house and settle down there. 

* It seems that I’ll be able to have a comfortable lifestyle if I keep living in Germany, but the desire for going back to Taiwan and spend time with my family hesitates me.

* Therefore, 5 current trends of change that will affect me are:

* 1.The trend of economic:

* it seems that the price of the neccessities and food rapidly grows up day by day.

* 2.The shift in political power:

* China may launch missiles to Taiwan someday because of the transfer of political power.

* 3.Workplace shift:

* It's hard to get a job recently.

* Many tech companies are laying off workers.

* 4.Threaten from nature:

* Climate changes will definitely destroy where we live in the future.

* 5.Rapid improvement of technology:

* I,assumed to be an engineer in the future, may be replaced by advanced robots someday.

* Part 2:

* The article I choose from is called ’’Water in asteroid dust offers clue to life on Earth’’. 

* The article states that Japanese space probe found a drop of water at an asteroid distancing from Earth for 300 million kilometers.

* Scientists said this discovery offers new support for the theory that life on Earth was seeded from outer space.

* There are 9 people involving in the vote, for 3 leaning left (disagree), 2 leaning right (agree), and 4 staying at center.

# 2022-09-29 #

* This was my first time to record a presentation.

* It was kinda awkward at first cause there's actually no one in front of me while I was expressing.

* Soon I got used to it and felt more relieved, but still hoping to finish the recording asap.

* Also, the topic of the week is "money".

* We were asked to answer a form of questions about finance before the class started.

* There were lots of question that I have no clue how to reply.

* After the class, I knew better for what the money is and how the financial system works fundamentally.

* I hope that next time we will get to learn more financial knowledge.

# 2022-10-06 #

* The topic of this week is about "extremism".

* I'm personally interested in the theme cause I've watched a lot of videos about Nazi and Fascisim, or so on.

* But I was moved by the story of Picciolini and impressed by his courage to speak out what he had been through.

* What I learnt from Picciolini is that even you have gone to a wrong way, it is never too late to realize your own mistakes and starts making compensations. 

# 2022-10-13 #

* 1.It's pretty interesting to know politisians from other countries and their policies.

* 2.Also, I've never thought about the definition of knowledge.

* Being asked to do so makes me confused at first and don't know where to start.

* 3.The fact that exercise can reduce the percentages of getting nervous system diseases surprises me.

* But I think that I should do some research and look for some paper to read to verify it.

# 2022-10-27 #

* The topic of this week is about depression.

* It’s  quite related to our life, since there were some incident happening recently.

* In this rapidly changing society, people are more likely to encounter mental problems.

* I’m convinced that searching for help if we have melancholy tendencies is especially important.

# 2022-11-3 #

* Glad to know that people my age have the same anxiety, such as graduating from university.

* I was shocked by the first video showing that we're actually don't know ourselves at all.

* I respect both of the fathers in second video for how they got through all th tragedies and had courage to tell the story.

# 2022-11-10 #

* It's a great cooperation with the members of course project cause I think there's good division of work.

* We haven't give our presentation yet, but I hope it will go well.

* I'm pretty related to the probelms of contruction since I just live near contruction sites.

* And I have felt annoyed for last few month.

* It was especially intolerable when they had laid the foundation.

* It felt like these were earthquakes and I kept being waken up at 8am every morning for that severe shaking.

# 2022-11-17 #

* It's hard to make a consensus even with rules, since there's always someone try to ignore the rules.

* I'll say that the differences of rights and obligations between natives and foreigners are generally reasonable, bacause what you gat equals to what you are taken.

# 2022-11-24 #

* I still believe reported the noise preblem to related units could be a way to solve the current situation.

* Each of us is just a small citizen in this city, but as long as we are united and speak for ourselves, there's still a chance to change the city.

* This is a democratic society, to win the votes, I believe that the government should listen to our voices.

* It's kinda sad that our thoughts were totally denined by the professor.

* We humbly accepted the professor's opinion and we will try hard next time.

# 2022-12-01 #

* I've never compared the differences between newspaper from two different countries reporting the same incident.

* And it's quite interesting to know how these media affecting the readers unknowingly with their own views.

# 2022-12-08 #

* The group's debate was interesting.

* Hope our project can successfully conduct!

# 2022-12-15 #

* Unseccessful and unproductive

* Always feel exhauted when it's Thursday because there are too many classes to take(from 8 a.m.).

* And I slept whole night and do nothing.

* I should spend more time on studying tomorrow.

# 2022-12-16 #

* Successful and productive

* In order to catch up my reading progress, I got up early and fulfill my studying list for today.

* Hope I can keep in the same pace in studying tomorrow.

# 2022-12-17 #

* Unseccessful and unproductive

* I got up early in order to take the photos of graduation, and it was cold and rainy.

* After having lunch, I slept the whole afternoon cause it was too tired for me in the morning.

* Therefore, I didn't fulfill up my studying list today.

* I need to be more productive for tomorrow

# 2022-12-18 #

* Seccessful and productive

* I forced myself to get up in such cold day, and I was very proud of myself.

* I finished all the reading plan in order to watch the final of World Cup.

* I feel encouraged by the winning of Messi.

# 2022-12-19 #

* Unseccessful but productive

* I did lots of exercises for the final exam on Tuesday.

* I was not confident for the exam tomorrow.

* Wish me luck.

# 2022-12-20 #

* Unseccessful and unproductive

* I failed the exam but wasn't frustrated cause it was predictable.(Accepting the failure will make you happier)

* I was sick today so I did nothing for the whole day.

* Hope I can get my energy back tomorrow.

# 2022-12-21 #

* Unseccessful and unproductive

* It turned out that I actually got Covid-19.

* I did nothing but lay on the bed all day.

# 2022-12-22 #

* I didn't show up last Thursday because of Covid-19.

* At the begining, it was chilling since the only thing I should do is taking the rest.

* As time went by, I felt extremely uncomfortable for the sickness.

* And at the same time, I just realized that I need to study for the following final weeks.

* However, I could do nothing but lying on the bed and feeling the pain.

* It turned out pretty lonely and boring for staying in the room 7/24.

* I'm feeling much better now, and I hope Covid-19 will never come back.

# 2022-12-29 #

* Jill Hung, daughter of Jeff Hung and Janet Wu, was born in Tainan at 2001.

* She didn't contribute much to society, but she did help others sometimes within her ability.

* She didn't have many friends, but all her friends cared about her (including her two cats).

* She tried to live every day without regrets, but there were always bad times in life.

* She liked Netfilx and comics even when she gets old.

* She can finally go back to the ones she loves.

* 

* I don't have many goals in life, but I hope that I will die as an interesting person.

* I want to keep my obituary short, because it's way more simple and "powerful".

* I don't want my death to be seen as a sad thing, because we'll all die someday in the future.

* I'm not sure where I'll be in the future so I didn't describe too much about my future, for example, where I'll be or what do I do.

* The only thing I know is that I will definitely have two cats (or more) in my life.

* It's pretty fun to write my obituary, and I learn a lot from it.

* This is a chance to think about " who I am " and " what kind of person I want to be ".

# 2023-1-5 #

* This is our first class of 2023.

* I did a pretty great job for the exam.

* I felt successful, and I was proud of myself.

* And the first video about dating app is quite interesting.

* It gave us another aspects when using dating app if you try to find your true love on it.

* The classes of this semester teached me a lot.

* I was exposed to a lot of different topics that I hadn't thought of before.

* I wouldn't say that I'm interested in all of them, but some did arouse my attention and make me think a lot.

* It was tiring to have a weekly discussion for the presentation with teammates after the class.

* But after getting more and more used to it, I started enjoying in the discussion.

* I'm glad that all my team members are responsible and friendly people.

* Everyone is willing to share his/her opinions, making the discussion efficient

* We successfully finished all the task with no fight, and I'm really thankful to them.

* Due to the class, I will care mor about the thing happening around the world.

* The most important thing I learned from the class, I would say, is examining an incident from more aspects.

* Don't ever believe something from only one statement, cause no one is 100% correct, and every statement is biased, even it seems "neutral".

* I'm not sure if I become a better person after this semester, but I hope I will make it someday in the future.

* Finally, thanks to all the people I worked with in this semester, and of course, thanks to the professor and TA for all the efforts they put on the class.

* Good luck!