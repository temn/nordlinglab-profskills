This diary file is written by **Deasy Natalia, E64138411,** in the course **Professional Skills for Engineering: The Third Industrial Revolution.**

# 2024-09-12 #
The first lecture was unexpected. It covered a wide area of knowledge. I am really inspired by the things going on in the industrial area. I have never thought about it before and had no desire to get to know it,but this course opened my mind.


# 2024-09-19 #
I had a little struggle because I actually needed more time to process the data given. Unfortunately, I couldn't keep up with the pace and needed to take my own time to analyze the charts. The lecture was so informative, as it covered uncommon topics.


# 2024-09-26 #
We had discussions about the topic, **"Is the world getting better?"** and linked it with the statements that Steve Pinker made in his TED talk video we watched last week. I always thought that the world many years ago was much better than now, but it surprised me how the data and statistics showed us that the world is actually getting better :)  

Mona Chalabi mentioned three questions to uncover bad statistics in her TED talk, which are:  
a. Can you see uncertainty?  
b. Can I see myself in the data?  
c. How was the data collected?

**Task 1: How my future will look and what are the five trends that will affect my future in 10 years**

I'm currently a third-year civil engineering student, and I aim to pursue a career in the construction and property management industries. Ten years from now, I’ll be 31. Oh, my Dear Lord! It sounds crazy somehow. Based on the goals I set when I was 17, I believe I will have achieved most of them by that age. My future work will likely involve building infrastructure like buildings, highways, flyovers, bridges, dams, toll roads, and other related projects. While structural engineering is my main interest, I also have a passion for transportation infrastructure, and I hope to work in both areas. Looking back, I realize my siblings and I share the dream of starting a construction company, as they are both studying architecture. This means I may be involved in various types of construction work, including system management, which I'm learning about now.  

As for where I’ll settle down, I’m still undecided. I want to experience living in different countries first, as I’ve always been curious about life abroad, and I plan to explore that while navigating life. However, when I reach my peak working years, I plan to return to Indonesia to contribute to the country's development, while still engaging in international construction projects that allow me to travel. Eventually, after my career, I’d like to slow down and enjoy life fully, reconnecting with family, friends, and nature. So I think you will find me in a small town with green fields and a lake, farming and gardening, living my best life.

Here are five key trends in the construction and civil engineering industry that could significantly impact me as a civil engineer and owner of a construction company:

**A. Sustainability and Green Construction**
As a construction company owner, I may need to integrate green building practices, which could increase operational costs initially but enhance my competitiveness.

**B. Digitalization and BIM (Building Information Modeling)**
As my company adopts these technologies, project efficiency and accuracy could significantly improve, reducing errors and delays. However, keeping up with rapid technological advancements may require constant investment in staff training and technology updates.

**C. Automation and Robotics**
This could lead to faster and more efficient construction processes, reducing labor costs but also requiring an investment in high-tech equipment. The use of robotics may reduce the need for manual labor but demand new skills from my workforce.

**D. Smart Cities and IoT Integration** 
As smart city projects grow, my company may be involved in more complex and tech-driven construction work. I will likely collaborate with IT professionals and smart technology providers to create infrastructures that communicate and adapt in real time.

**E. Modular and Prefabrication Construction**
My company may need to adopt modular and prefabrication techniques to stay competitive. This could require changes in my business model and supply chain management, as a significant portion of the work may shift from on-site to factory production. This trend could lead to faster project turnaround times, opening opportunities for scaling my business to handle more projects simultaneously.

**Task 2: Summary of 'Thousands bid farewell to Tokyo zoo pandas before return to China' with three different perspectives**
**A. Left Perspective** 
The left-leaning perspective focuses on the emotional connection between the fans and the panda couple, Ri Ri and Shin Shin. It emphasizes the fans' dedication, with over 2,000 people, some camping out overnight, gathering to say their goodbyes. The article highlights how these pandas served as a source of comfort for many. It also provides information about their return to China for medical treatment due to aging-related conditions. The piece acknowledges the goodwill gesture of China in sending pandas abroad and touches on the endangered status of pandas, placing the animals in a broader environmental context.

**B. Center Perspective**
The centrist view offers a more detailed emotional and human-interest angle, featuring personal stories of individual fans who were deeply moved by the pandas' departure. It discusses the emotional significance the pandas had during difficult times in Japan, such as the Tohoku earthquake and COVID-19, suggesting that the pandas offered a form of solace during national hardships. The article also discusses the practical reasons for the pandas' return to China, focusing on their health. It includes quotes from zoo officials and fans, balancing sentiment with factual information about the pandas' impact on Japan.

**C. Right Perspective**  
The right-leaning perspective, while similar in basic content to the left-leaning version, presents the event more matter-of-factly, without diving deeply into the emotional stories of individual fans. It highlights the logistics of the pandas' departure, focusing on the medical reasons for their return to China and China’s ownership of pandas. The article notes that pandas are sent abroad as symbols of goodwill and mentions the importance of pandas to both China and the international community. The endangered status of pandas and conservation efforts are also addressed, but the emotional depth is less pronounced compared to the center and left perspectives.


# 2024-10-03 #
1. This week's topic covers money, currency, and their value. I’ve gained a proper understanding of them and finally grasp how they function.  
2. By studying this topic, I learned about the history of money around the world.  
3. I’ve come to recognize the influence of money and the forces behind it. How recession affected one country and how it could be avoided.  
4. For further knowledge, we were given the assignment to evaluate the purchasing power of the currency, debt, money consumption, and income and wealth distribution of my country over the last 100 years.


# 2024-10-17 #
This week our class back to normal again after online class during typhoon and national holiday. We began our class with reading some student's diary, it was motivating and inspiring for sure, we also learned about how to write the diary with the right markdown syntax, so it can be read well. After that we got to discuss the topic about money in class, it was very informative, that i won't know any of it if i didn't come to the class that day. it was also quiet inspiring on how we should see money and it's value. Not only that, we got deeper into the topic by watching some videos from the experts that gave a lot of insights about money, it's power, it's value, and who's behind it. Quiet shocking, honestly speaking. I have never want to know and think about it, but somehow, it was something that i should understand to have a better relationship with money and manage it well. Here are some things that i learned about money:

**1. What gives money it's value?**
ans: First of all government designated an official currency and printed it to make a piece of paper called money legal. Most currency was linked to valuable commodities like gold and the amount of it circulation depended on a government's gold or silver reserves. But the value of money can also be determined by the government policy.
**2. How money was produced?**
ans: Money produced by loans. when a citizen apply loans to bank (demand) then bank make sure that they can lend the amount of money the citizen's asked for (supply). In fact there are millions of people who borrowed money from banks and that made banks printed more money.


# 2024-10-24 #
This week, we learned about extremism. Extremism refers to intense beliefs, attitudes, or behaviors that fall far outside what society considers acceptable. It can sometimes lead individuals or groups to support or even commit acts of violence to achieve their goals, although not all forms of extremism involve violence. We watched Christian Picciolini, a former member of America’s neo-Nazi movement, who shared the reasons behind his decision to join the organization and how he eventually left it. He described how he was radicalized and, ultimately, how he escaped the movement. I found it interesting that Picciolini said the leaders of the organization targeted vulnerable young people who felt marginalized. From this, I understand that people who are broken-hearted or feel isolated are more likely to become extremists, as the saying goes, "hurt people hurt people." By understanding the reasons behind extremism, we can confront these actions in a compassionate and empathetic way.

# Task 1 #
three fictional stories that i believe in and how they have affected me over the past week:

**1. God**
I believe in Jesus Christ, my Lord and Savior, who created me. I believe that He didn’t want heaven without us, so He brought heaven down. Jesus has a huge impact on my life. as I claimed that I'm His, everyday I'm trying to become more like Him by obeying His words. However it's so easy to fall short, last week I was drowned by worldly values and understanding. I noticed that my actions didn't reflect the fruit of the Holy Spirit which is love, joy, peace, patience, kindness, goodness, faithfulness, gentleness, and self-control. This realization showed me that I need to draw closer to Him because I find my best self in Him. More precious than silver or gold, Jesus is the treasure I hold.

**2. Marriage**
When God looked at Adam and said 'It is not good that man should be alone; I will make him a helper comparable to him' I believe that one of my purpose was to become a wife and bring glory to His name through my marriage life. I believe in dating for marriage although I'm not dating yet, I've come to understand my role in marriage through God's word. The bible teaches that I should fully submit to my husband, just as it says 'For the husband is the head of the wife as Christ is the head of the church, his body, and is himself its Savior' I will find delight by submitting to my husband as I know that he was first submitted to Christ Jesus. By this belief, everyday I'm learning to understand the qualities that I'll need and how to fulfill my role as a woman. Over the past week I have been learning by doing. 

**3. Career**
I’ve always pursued things I enjoy, even though I lack the privilege and freedom to easily get whatever I want. God has helped me to reach the career path that I've always desired which is in Civil Engineering. I thank God that He heard prayers and equipped me with everything I need to reach my career path. With this belief, I have no worry or fear when facing trials or problems as I know God will walk me through it. Last week I talked with some friends about this awesome university called MIT. Since junior high school, studying at MIT has been one of my dreams and is now called goal. As I near my bachelor’s degree graduation, I realize I needed to start preparing myself to meet their requirements. Part of me screamed that I wouldn't be able to make it, but looking back, I wouldn't have made it here if it was not Jesus as He works all things together for good. I have peace in this, whether it's MIT or the other university, I know it will always be the best thing for me. I just need to show up and do my best, God will handle the rest.

# Task 2 #
Indonesia is striving toward its ultimate goal of becoming a sovereign, advanced, fair, and prosperous nation by its centennial in 2045, a milestone known as "Indonesia Emas 2045" (Golden Indonesia 2045), marking 100 years of the country’s independence. This vision was formulated by the Ministry of National Development Planning and was officially launched by Indonesian President Joko Widodo on May 9, 2019. Former President Widodo remains optimistic that by 2045, Indonesia will rank as the world’s fourth or fifth largest economy. By this time, Indonesia will also benefit from a demographic bonus, with 70% of its population in the productive age range (15-64 years), while the remaining 30% will be in the unproductive age groups (under 14 years and over 65 years) during the period from 2020 to 2045. The Indonesian government has set various strategic visions to achieve the goals of Indonesia Emas 2045. At that time, I will have significant work opportunities as a civil engineer, contributing to the country’s progress by continuing infrastructure development.


# 2024-10-31 #
We don't have onsite class today due to the typhoone, but we were assigned with new groups fot the final project and lectures 8-12 group.


# 2024-11-07 #
This week we talked about how to handle depression, not only that we also learned how to react, respond, and treat someone with depression rightly. I feel everyone needs comfort most of the times, we all need that 'special one' whose close enough to know what's going on in our life. but most of teenagers even adults don't consider their parents as that 'special one', that way they look for it in other people. whhen they don't find that 'special one' they tend to feel lonely and alone. i think most people fall into depression because of this. they look for comfort in the wrong places and persons where none of it cannot meet their expectations. for me personally, i'm blessed to find Jesus, He is my 'special one', He is the only one, He is everything i need. many times i cannot find words i want to say to explain my situation, with Jesus, i don't need words to explain. He knows it better than i do. i can just simply say 'Lord, i don't know, but You know. I give it to You, show up and make Your glory known, Lord'. my relationship with Jesus is the treasure that i found. i can fully rely on Him, i'm His beloved child. and i wish everyone will experience Jesus in their life too :(


# 2024-11-14 #
Today, we continued class by listening to group presentations about recognizing signs of depression in friends and ways to support them. The session was practical and insightful, reminding me to be more attentive to my friends and check in regularly to stay connected with what’s happening in their lives and mine. 
We also reviewed insights from Prof. Korte, highlighting six key areas to master with the support of others. I noted areas I should work on personally to prepare myself to grow into a successful engineer. Finally, we were given three minutes to discuss three concrete rules for a happy, fulfilling life. Here they are:

**1. Make sure it’s something you truly want**  
Before making any decision, take a moment to check and ask yourself, “Do I really want to do this?” If the answer is yes, be ready to face the worst part that may come with it. if you are ready for it, it shows that you really want it and will actually go for it.
**2. Know your ‘why’**  
Even when decisions feel easy, it’s essential to understand your reasons. Knowing why you’re doing something makes it easier to stay motivated and follow through.
**3. Choose what excites you**  
When faced with choices, I always go for the option that excites me, not only that i tend to choose the one that scares me that makes my heart skip a beat (except when it comes to food—gotta protect my taste buds and avoid regrettable bites /iykyk). this way, i know that i will choose it over and over.


# 2024-11-21 #
In today’s lecture, we watched an engaging video about algorithms and their applications, such as in big data systems used for insurance and loan credibility. The presenter criticized how these algorithms rely on historical data, perpetuating outdated trends and biases. One example was a company where women were less likely to be promoted, and this bias persisted even after the CEO retired because the algorithm factored gender into success predictions. While this highlights a serious issue, I believe the data we collect is invaluable for promoting fairness overall. However, it’s essential to regularly review and refine algorithms to ensure ethical and accurate outcomes. Additionally, with the growing volume of data, companies can focus on more recent information, reflecting current realities from the past 1–5 years.
Moreover, this week’s topic, "How to Stay Connected and Free?", explored balancing freedom and responsibility in the digital world. We began with Jaron Lanier's TED Talk, How We Need to Remake the Internet, which critiqued the flawed business models of companies like Google and Facebook that profit from personal data and attention. Lanier argued for systems that prioritize human well-being over algorithmic manipulation. The talk was thought-provoking, urging a shift toward fostering genuine connections, privacy, and autonomy online. We then discussed creating a minimal yet effective set of laws to safeguard key principles, including freedom of speech, privacy, equal access, easy sharing, and protections against fake news, manipulation, and addiction.

**Rules On How To Reach Concencus**
**1. Inclusive Participation**
Everyone affected by the decision should have the opportunity to participate and ensure that all voices and perspectives are heard and respected.
**2. Active Listening**
Listen to others without interruption and seek to understand differing viewpoints before responding or counter-arguing.
**3. Focus On Common Goal**
Emphasize shared values and objectives over individual preference and frame discussions around the collective best outcome.
**4. Commitment For Collaboration**
Approach the process with a cooperative mindset, rather than a competitive one and Strive to build trust and work towards mutual benefit.


# 2024-11-28 #
We started with a discussion on last week’s topic about balancing conflicting goals like freedom of speech and combating misinformation. Today’s lecture focused on final group projects, where teams shared their objectives. I’m excited to see how these projects evolve. We also conducted an experiment by handing in our phones for the duration of the class.  
I rewatched the remaining lecture content, which included a video on recent conflicts in the Netherlands between Israelis and Dutch individuals. Initial reports claimed Israelis were attacked, but the video revealed the opposite, highlighting how rushed or biased reporting can distort events.
This week’s theme, "How to Make Sense of Events," explored how we interpret the world. We began with a voting exercise on group project ideas to foster critical thinking. We then analyzed historical graphics showing city growth after "dark ages," noting that recovery times have increased over the centuries. 
This led to the unsettling question of how long it would take to recover from a potential future dark age. The session emphasized the importance of learning from history and questioning media narratives. A video on the Amsterdam riots exposed how mainstream media often omits key details, but a young reporter uncovered the truth. 
This reminded us that anyone can make a difference by standing up for transparency and truth.


# 2024-12-05 #
In today’s lecture, we presented group findings on how newspapers from different countries reported the South Korean President’s failed attempt to impose martial law. The session also focused on planetary boundaries and climate change, highlighting the risks of rising temperatures, which could lead to a 38% GDP drop by 2050 if unchecked. 
We discussed shrinking heat sinks, overshoots, and the importance of mitigating severe weather impacts. Johan Rockström’s TEDx talk emphasized using science to address ecological limits and reshape consumption patterns for sustainability. Lastly, we organized our final project tasks and reflected on actions to stay within planetary boundaries and meet societal needs.


# 2024-12-12 #
Each group presented detailed action plans using tools like the Work Breakdown Structure (WBS), PERT chart, and GANTT chart to outline their strategies for addressing specific planetary boundaries. These presentations included clear timelines, task breakdowns, and milestones, offering a structured approach to tackling environmental challenges. Each group’s representative provided a summary of their plan, followed by a Q&A session. 
This interactive segment allowed peers and instructors to provide constructive feedback, raise questions, and suggest improvements, fostering collaborative refinement of the proposed strategies. We also explored Jeremy Rifkin’s documentary, The Third Industrial Revolution: A Radical New Sharing Economy, which provided an in-depth look at how global economic systems can be reimagined to address current crises. 
Rifkin proposed a transformative shift toward collaborative economies powered by renewable energy and digital innovation. The film emphasized the importance of sustainability, equality, and systemic change to build a resilient, future-ready society. It highlighted how interconnected systems—such as energy grids, transportation networks, and communication platforms—can be redesigned to align with ecological and economic needs. 
This week underscored the importance of actionable and collaborative approaches to addressing planetary boundaries. Through the group presentations, I gained insights into structuring complex projects effectively and the role of teamwork in developing viable solutions. The Q&A sessions demonstrated how diverse perspectives can enhance the quality and feasibility of proposed actions. Rifkin’s documentary reinforced the urgency of collective action, technological integration, and sustainability in redefining economic growth while maintaining ecological balance. 
These lessons are pivotal for shaping strategies in our final project and for broader applications in creating systemic environmental change.


# 2024-12-19 #

Five Rules for Success and Productivity
1. Plan night before: By organizing your day beforehand, you reduce decision fatigue and increase efficiency. Planning helps you allocate time effectively, ensuring critical tasks are addressed first.
2. Stick to your plan: The effectiveness of a plan depends on execution. Staying dedicated to your schedule prevents procrastination and builds habits of reliability. Flexibility may be necessary occasionally, but commitment is key to achieving long-term success.
3. Be realistic: Overloading your day with excessive tasks can lead to frustration and burnout. Realistic planning ensures you focus on quality over quantity, maintaining steady progress without sacrificing mental and physical health.
4. Work hard and play harder: Productivity thrives when there’s a balance between work and relaxation. By rewarding yourself with leisure activities, you recharge your energy and motivation, preventing burnout and fostering creativity.
5. Journal: Have a chit-chat with yourself will helps you evaluate your achievements, identify areas for improvement, and keep a record of thoughts and ideas. 

**Daily Diary Task**

**Friday - 12/20/2024**
* A. Productive and Unsuccessful
* B. I felt productive because i got to complete my to do almost everything I need to do today except progressing my pavement analysis' final project because of overslept which makes me feel unsuccessful.
* C. I need to be more strict to myself.

**Saturday - 12/21/2024*
* A. Productive and Successful
* B. I finished my Civil Engineering Management System's final project the entire day, making me feel sucessful and a little less stressful that day.
* C. I need to keep it up.

**Sunday - 12/22/2024**
* A. Productive and Successful
* B. I woke up early today because me and my cicis planned to have lunch near Anping together and we need to take the 2 PM train to Kaohsiung for church. I spent most of the day in Kaohsiung, enjoy the sunday service and shared love with people there. It feels like home. Then I got to prepare the exchange gift for the OIA's Christmas Party more properly.
* C. I need to go out and meet more people.

**Monday - 12/23/2024**
* A. Unproductive and Successful
* B. I woke up and attended the OIA's Christmas party on time today. I enjoyed celebrating Christmas with people there. Yet it feels unproductive because I spent the entire day on my bed since I don't have any class or group meeting. But I managed to study for my Chinese quiz later on.
* C. It's better to do something I love than just staying on the bed.

**Tuesday - 12/24/2024**
* A. Productive and Unsuccessful
* B. I completed my to-do list, everything I need to do that day, but there's no single activity that I did for my ownself.
* C. I need to spend some time alone to connect with myself.

**Wednesday - 12/25/2024**
* A. Productive and Unsuccessful
* B. It's a big day for me today, I want to celebrate Jesus, yet I got many things to accomplish, but I came unprepared for it. Even though I managed to complete every task, i feel tired and drained, today was a hard day for me. but I thank God for His companion, I wouldn't make it today if it was not Him. 
* C. I need to rest.

# 2024-01-02 #
Orbituary: Someone who's passionate in road and building structure designs.
Name: Deasy Natalia Marena Br Napitupulu
Date of Birth: 2 December 2003

A visionary in sustainable architecture and infrastructure. Known for their unwavering passion for road and building structure designs, Deasy dedicated their life to transforming the built environment into a harmonious, eco-conscious, and innovative space.
Throughout their distinguished career, Deasy spearheaded groundbreaking projects that seamlessly integrated functionality, aesthetics, and sustainability. They were instrumental in championing green building standards, advocating for environmentally responsible practices in construction, and contributing to revolutionary designs that minimized environmental impact while maximizing utility and longevity.
Deasy's work often drew inspiration from their belief in a future where buildings and roads not only serve society's needs but also contribute to the planet's health. They were a key proponent of the "circular construction" movement, striving for 100% recyclable buildings and infrastructure that left no trace of climate impact throughout their lifecycle.
Among their proudest accomplishments was their comparative analysis of Taiwan's EEWH green building certification system with the world's most rigorous sustainable building standards. This work not only highlighted areas for improvement but also provided a roadmap for achieving global environmental benchmarks.
Outside of their professional achievements, Deasy was a beloved mentor, inspiring countless young architects and engineers to embrace innovation and sustainability in their work. Their legacy lives on in the roads we travel, the buildings we inhabit, and the sustainable practices they championed.
Deasy’s passion for creating a greener world will forever resonate in the hearts of those who knew them.

Lin Hsin-Jung:
A Taiwanese architect and environmentalist who has played a significant role in advancing Taiwan's EEWH green building certification system, making it a benchmark for sustainable practices in Asia.

# 2024-01-08 #
We started by taking the final exam administered through Google Forms, which evaluated our grasp of the material and practical application skills. Following this, groups showcased their final projects via three-minute pre-recorded videos, delivering concise yet impactful presentations that marked the end of our journey together. 

1. Know the current world demographics; **Suggestion:** invest 15 minutes daily reviewing current statistics from trusted sources like the UN and World Bank, synthesizing the key findings.
2. Ability to use demographics to explain engineering needs; **Suggestion:** identifying one engineering challenge weekly and exploring its demographic connections through group discussions or written reflections.
3. Understand planetary boundaries and the current state; **Suggestion:** conducting weekly case study research, reflecting on how economic frameworks could address environmental challenges.
4. Understand how the current economic system fails to distribute resources; **Suggestion:** group simulations and role-playing exercises, followed by thoughtful reflection on the experience.
5. Be familiar with future visions and their implications, such as artificial intelligence; **Suggestion:** writing weekly fictional scenarios about AI's potential impact on everyday life in the coming decade.
6. Understand how data and engineering enable a healthier life; **Suggestion:** analyzing real cases where data-driven solutions improved health outcomes.
7. Know that social relationships give a 50% increased likelihood of survival; **Suggestion:** initiating one new social activity weekly, like coffee meetings or group memberships, and tracking mood changes.
8. Be familiar with depression and mental health issues; **Suggestion:** weekly research on mental health facts and strategize support approaches.
9. Know the optimal algorithm for finding a partner for life; **Suggestion:** practicing partner selection strategies through structured decision-making exercises.
10. Develop a custom of questioning claims to avoid “fake news”; **Suggestion:** verifying one news article daily using credible sources and academic references.
11. Be able to do basic analysis and interpretation of time-series data; **Suggestion:** Master time-series analysis using tools like Google Sheets or Python to examine practical datasets and summarize findings.
12. Experience collaborative and problem-based learning; **Suggestion:** Practice collaborative problem-solving through weekly group projects or study sessions.
13. Understand that professional success depends on social skills; **Suggestion:** Strengthen workplace social skills through weekly role-playing scenarios with peers.
14. Know that the culture of the workplace affects performance; **Suggestion:** studying cases that demonstrate effects on productivity and employee satisfaction.