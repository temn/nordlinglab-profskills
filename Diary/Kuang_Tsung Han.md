This diary file is written by Kuang-Tsung Han E34131219 in the course Professional skills for engineering the third industrial revolution.

# 2024.09.12
* ### In the first lecture, I understood the rules of this lecture and how to complete the weekly tasks.
* ###  I had the opportunity to work in a group and talk with other members, which was a great step forward for me.
# 2024.09.19
* ###   Today's presentation is "Will The Exponential Growth Lead to  a World of Abundance?" Every group has its own point of view, from Electric Vehicles, Solar Plants, SSDs and Food Production, which gave me different perspectives to consider regarding the opposite of this question.
* ###  Does conflict always mean bad things? Not necessarily. When conflict arises, it means that people's expectations are not consistent, so knowing how to negotiate with others becomes very important.
# 2024.09.26
* ### Today's presentation is " Is the World Getting Better or Worse? "
* ## the three thing metioned in Mona's video
  1. ### Can you see uncertainly?
  2. ### Can I see myself inthe data?
  3. ### How is the data collected?
* ## my future ten years from now
    #### In 2034, what will the world really look like? Will AI replace all labor-intensive jobs, even in creative fields like art and content writing? Will humans be able to land on the Moon and Mars, or even live there? Will semiconductors reach the atomic limit, or perhaps break through it? Will a global war break out and cause devastating damage? I don't know, and neither does anyone else. Currently, I am majoring in Chemical Engineering at National Cheng-Kung University, trying to build a solid foundation for my future career. My aspiration is driven by my enthusiasm for aerospace.

    #### Ten years from now, I envision myself as a successful aerospace engineer, combining my passion for technology and aviation. I will be living in a city known for its aerospace industry, perhaps in America. My home will reflect my interests, filled with engineering books and a guitar for leisure. As an aerospace engineer, I see myself working for an aerospace company, contributing to the design, development, and testing of advanced aircraft and spacecraft. I’ll collaborate with multidisciplinary teams to create innovative solutions that enhance flight safety, efficiency, and sustainability. I aim to take on leadership roles, mentoring new engineers and promoting a culture of creativity within my team.

    #### Several trends will significantly influence my life and career over the next decade. One key trend is the rise of Artificial Intelligence (AI), which will transform the aerospace industry. AI is truly a double-edged sword; while it can optimize design processes and flight paths, enabling engineers to make data-driven decisions, it can also be misused by those with bad intentions. For instance, AI tools can analyze aircraft performance data and predict maintenance needs, reducing downtime and improving safety. Its powerful capabilities extend across many fields, but the ethical implications cannot be ignored.

    #### Geopolitical tensions and regional conflicts will also impact me, particularly as a citizen of the ROC. Anti-war initiatives and peace, which are included in the SDGs, should always remain top priorities. Unfortunately, some politicians neglect these ideals. Additionally, collaboration will be essential throughout my career. The aerospace industry thrives on partnerships, both within teams and across organizations; no project can be completed without teamwork. I expect to engage with international firms and research institutions on large-scale projects that require diverse expertise, fostering innovation and unique solutions to engineering challenges.

    #### Maintaining curiosity, passion, and concentration will also be critical in my evolving field. I plan to dedicate time to continuous learning to stay motivated and focused on my goals. As the saying goes, "Never gonna give you up, never gonna let you down." Keep pursuing your aspirations; with perseverance, you will become unstoppable, and your flame of passion will never be extinguished. My curiosity will drive me to explore new ideas, while my passion for aerospace will inspire excellence in my work.
    
* ## *News:Hezbollah confirms its leader Hassan Nasrallah was killed in an Israeli airstrike*
    1. Alabama Live (Center-Right Site): Its news includes comments from the Israeli army, Lebanon’s Hezbollah group, and the Palestinian militant group Hamas. All the content provides coverage that confirms the truth and explores further reasons, making it easy to understand why these events occurred without introducing significant bias.
 2. CBS News (Lean-Left Site): This outlet mentions the prior actions taken by Israel and statements from other countries, highlighting what Israel has done in Lebanon.
 3. News18 India (Lean-Right Site): This source only states that the Israeli lethal strike in Lebanon has caused many deaths, without detailing why this matter occurred.
    
    * source:https://ground.news/article/hezbollah-confirms-its-leader-hassan-nasrallah-was-killed-in-an-israeli-airstrike_10cc84(last visit 2024.09.29)
## 2024.10.17
* ### What is money? Who gives it value? And how does it work? Those questions were central to today's course, and here’s what I learned:
    1. **The property of money**  
       Money must have certain characteristics to be useful in an economy. These include durability, divisibility, portability, and recognizability. Without these properties, it would be difficult to use a substance as money.

    2. **The function of money**  
       Money serves several key functions in society: it acts as a medium of exchange, a unit of account, a store of value, and a standard of deferred payment. These functions make it essential for daily transactions and long-term economic planning.

    3. **What is the standard for something to become money?**  
       To become recognized as money, a commodity must meet the following criteria:
         - **Equal to same value:** It must be universally accepted for its equivalent value in goods and services.
         - **Countable unit:** It must be easily divisible into smaller units to facilitate transactions of various sizes.
         - **A medium of exchange:** It must be readily accepted as payment for goods and services.
         - **Portable:** It must be easy to carry and use in various contexts, from small purchases to large transactions.

    4. **The relation between debt, credit, and money**  
       Debt and credit are fundamental aspects of how money circulates. Debt refers to money that is owed, while credit is money that can be borrowed or extended for future repayment. These concepts are essential to the functioning of modern economies, where borrowing and lending help facilitate investment and growth.

    5. **How the government works in financial markets**  
       Governments influence financial markets through regulations, monetary policy (such as controlling interest rates), and fiscal policy (such as taxation and government spending). Their role is to ensure economic stability, manage inflation, and promote sustainable growth.

    6. **Property inequality**  
       The course also touched on the issue of property inequality and how wealth is distributed unevenly across different groups in society. Understanding the role of money in perpetuating or reducing inequality is crucial to addressing social and economic disparities.

## 2024.10.24
* ### I forgot to write today? >.<  
   - It was one of those days where I was caught up in various tasks and didn't take a moment to reflect or jot down anything. I felt like the day passed in a blur, and I missed the opportunity to capture my thoughts. It’s a reminder that, even on busy days, taking a few minutes to write can help me stay grounded and reflect on what I’ve learned and experienced.

### **2024.10.31**
* Today was a productive day. We worked on brainstorming ideas for our group project and explored how to approach the topic creatively. It was challenging to get everyone aligned, but we eventually agreed on a direction. Collaboration is tough but rewarding when done right. Despite some initial friction, we managed to build on each other's strengths. The diversity of ideas in the group helped create a rich discussion. I felt proud of how we overcame our differences to come up with a clear plan. It was a good reminder that working together often leads to better outcomes than working alone.

### **2024.11.07**
* We discussed mental health awareness in class and how to address the stigma surrounding it. A guest speaker shared their personal journey with depression, and it was both moving and eye-opening. It made me realize how important it is to create safe spaces for conversations about mental health. The speaker's vulnerability was inspiring, and it challenged me to rethink how I approach mental health conversations. I realized that the stigma still exists, even in academic settings, and that everyone could benefit from being more open and supportive. I left class thinking about how I can make a difference in my own community by fostering understanding and compassion for those struggling with mental health issues.

### **2024.11.14**
* We had a group discussion about our previous homework and prepared for an upcoming presentation. I also learned simple ways to support friends experiencing depression, like listening without judgment and just being there for them. At the same time, I was reminded of the importance of self-care when supporting others. It’s all about balance. While it's crucial to be there for people, I also realized how easy it is to neglect my own mental health while focusing on others. We explored different strategies for supporting loved ones and the importance of setting boundaries. This class made me reflect on the value of empathy and the role of active listening. It also helped me understand that it’s okay to seek help when I need it, as self-care is essential to avoid burnout.

### **2024.11.21**
* **How to help people in depression:** Today, some groups presented guides on supporting friends dealing with depression. Listening, being present, and avoiding unsolicited advice were the key takeaways. I also reflected on how important it is to foster empathy and understanding within communities. Depression is often misunderstood, and the more we educate ourselves, the better we can help others. The presentations made me realize that sometimes, just sitting in silence with someone and showing you care can be the most supportive action. The session emphasized the significance of being mindful of our words and actions, as they can either uplift or inadvertently harm someone already struggling.

### **2024.11.28**
* We started learning about planetary boundaries, which was a completely new concept for me. I was shocked to find out that six out of nine boundaries have already been crossed. It’s concerning because these boundaries keep our planet in balance, and crossing them could lead to irreversible damage. I’m beginning to realize the urgency of these environmental issues. As we explored topics like climate change, biodiversity loss, and land use, I felt overwhelmed by the scale of the challenges ahead. It’s unsettling to think that our actions have already pushed the Earth past certain tipping points. This lesson made me reflect on the long-term impact of our daily choices and the importance of collective action to preserve the planet for future generations.

### **2024.12.05**
* We analyzed news from different countries, linking the sentiment of the news to global press freedom scores. It was fascinating to see how perspectives vary based on who benefits from the story. We also delved deeper into planetary boundaries, focusing on "novel entities" like plastics and pollutants. These human-made substances are pushing ecosystems to their limits, which is terrifying but motivates me to think about solutions. The correlation between press freedom and the quality of news made me more aware of the influence media has on shaping public opinion and policy. As for planetary boundaries, I learned that even small pollutants have a large impact when accumulated over time. It was eye-opening to see how interconnected environmental issues are with societal systems like media, politics, and consumerism.

### **2024.12.12**
* Each group presented their action plans, including detailed structures like PERT and GANTT charts. It was great to see how different groups approached solutions for planetary boundaries. I learned a lot during the Q&A sessions, where we debated ideas and gave constructive feedback. We also watched Jeremy Rifkin’s documentary about the "Third Industrial Revolution," which highlighted the importance of transitioning to sustainable and equitable economic systems. The discussion made me realize how urgent the need for systemic change is. Jeremy Rifkin’s ideas about renewable energy and the sharing economy left me thinking about how we can move away from profit-driven growth and towards a more inclusive and sustainable future. It was inspiring to see how technology and innovation can drive positive change when paired with a clear sense of purpose.

### **2024.12.19**
* I feel like the semester is wrapping up strongly. Today, I reviewed everything we’ve learned about planetary boundaries and reflected on how interconnected these systems are. The challenge ahead is daunting, but it’s also inspiring to think about how small actions can lead to big changes. My biggest takeaway is that collaboration and awareness are key to solving global problems. I’ve been thinking a lot about the impact I want to have in the world, and I feel motivated to take action. Whether it’s making sustainable choices in my daily life or getting involved in community initiatives, every effort counts. This semester has challenged me to think critically about the environment and my role in shaping a more sustainable future. As we move forward, it’s clear that awareness, education, and collective action will be essential in addressing the environmental challenges ahead.

# A weeklong task
## 24/12/20
   - **A.** Unsuccessful and unproductive
   - **B.** Why? I planned to go home and was delayed on the train due to someone jumping onto the tracks, which wasted a lot of my time. How furious I was! Even when I got home, it was already 00:45.... The delay made me feel like I had lost precious time, especially since I had planned to be more productive at home. It was frustrating to watch the clock tick away as I waited for the train to move again. 
   - **C.** Notice and make more buffer time for traffic. This experience taught me that unexpected events can derail even the best-laid plans, so it’s important to allow extra time for possible delays.

## 24/12/21
   - **A.** Successful and unproductive
   - **B.** Why? I stayed with my friends and family, nothing done today, but full of enjoyable memories. Although I didn’t complete any tasks, spending quality time with loved ones was refreshing. Sometimes, taking a break from work and being present with the people around you is just as valuable as being productive.
   - **C.** Regular exercise is still important, even when I'm free. I realized that it’s easy to let my routine slip when I’m having fun, but incorporating exercise into my day helps me maintain my energy and balance.

## 24/12/22
   - **A.** Successful and productive
   - **B.** Why? I stayed with my friends and family, but I also became active and did regular exercise today. It was a productive day because I found a balance between relaxation and self-care. I made time for my health while enjoying the company of those I care about.
   - **C.** Do tasks that I haven't done yet. Despite the fun day, I managed to complete tasks I’d been putting off, which gave me a sense of accomplishment.

## 24/12/23
   - **A.** Unsuccessful and unproductive
   - **B.** Why? I slept over due to lack of sleep in the last two days. I felt so bad to work on. I woke up tired and sluggish, which made it hard to focus on anything. The exhaustion made it tough to get through the day, and I felt like I wasted valuable time.
   - **C.** Go to sleep early. I realized that getting enough rest is essential for productivity and well-being. I need to prioritize sleep to avoid feeling drained and behind.

## 24/12/24
   - **A.** Unsuccessful and productive
   - **B.** Why? I finished homework but still used too much time to deal with it. Despite completing the tasks, I spent more time than I should have. I got caught up in perfectionism, trying to make everything flawless. This made me feel like I wasn’t being efficient.
   - **C.** Break time into parts, and also break down homework to make it more efficient. By dividing my time and tasks into manageable parts, I can stay focused and avoid wasting time on unnecessary details.

## 24/12/25
   - **A.** Successful and productive
   - **B.** Why? I used my time more efficiently. I made a conscious effort to stay on track with my tasks, and it paid off. I was able to accomplish more while still enjoying the holiday with friends and family.
   - **C.** Keep it up. This day reinforced the importance of managing my time wisely. I plan to maintain this productive momentum going forward.

## 24/12/26
   - **A.** Successful and productive
   - **B.** Why? I finished a project for my Chinese course on time. I was proud of myself for completing the task ahead of the deadline. It felt satisfying to check something off my list, especially since I had been worried about finishing it.
   - **C.** Nope. There wasn’t anything else I felt I needed to change. It was a good day overall.

## 24/12/27
   - **A.** Unsuccessful and unproductive
   - **B.** Why? Although I had a to-do list, I only got a few things done by the end of the day. I realized that I hadn’t prioritized the most important tasks, which led to me feeling unproductive. The day got away from me, and I ended up feeling like I hadn’t achieved much.
   - **C.** Try to make a priority to-do list instead of just a regular to-do list. By focusing on what truly matters first, I’ll be more likely to accomplish my goals.

## 24/12/28
   - **A.** Successful and productive
   - **B.** Why? After using a priority list, I got more things done than yesterday. The list helped me stay focused on the most important tasks and gave me a sense of accomplishment by the end of the day.
   - **C.** Nope. I didn't feel the need to change anything. The priority list method worked well, and I felt efficient.

## 24/12/29
   - **A.** Successful and productive
   - **B.** Why? I went to exercise with my friends and prepared for the physics test. It was a day that balanced social time, physical activity, and studying, which made me feel well-rounded and accomplished.
   - **C.** Nope. I felt good about how I managed my time and tasks.

## 24/12/30
   - **A.** Unsuccessful and unproductive
   - **B.** Why? I slept over because of lack of sleep in the last two days. I felt so bad to work on. The lack of rest caught up with me, and it made it hard to be productive. I didn’t get much done and regretted not prioritizing sleep earlier.
   - **C.** Go to sleep early. I learned that rest is crucial, and I need to plan ahead to make sure I’m not sacrificing sleep for productivity.

## 24/12/31
   - **A.** Unsuccessful and productive
   - **B.** Why? I finished homework but still used too much time to deal with it. I was happy to finish my tasks, but I spent a lot of time perfecting them. In the end, I realized that I could have completed the same work in less time if I’d been more focused.
   - **C.** Break time into parts and also break down homework to make it more efficient. If I divide my tasks more effectively, I’ll save time and avoid overthinking.

## 24/01/01
   - **A.** Successful and productive
   - **B.** Why? I tried to solve oscillation problems and succeeded. I felt accomplished when I was able to work through challenging problems and learn new concepts. It gave me confidence for the upcoming semester.
   - **C.** Just hold on. I know that if I keep pushing through, I will continue to improve and get better at problem-solving.

## 24/01/02
   - **A.** Successful and unproductive
   - **B.** Why? I planned to study Calculus 3, but ended up only finishing one part of it. Although I made progress, I didn’t get as much done as I had hoped. I felt like I could have focused better.
   - **C.** Calculus should take more time to study. I need to set aside more focused time for this subject to ensure I master it properly.


# 2025-01-08

Today, I took a reflective moment to write my obituary. It felt strange but also inspiring to imagine the impact I’d like to have in my lifetime.

## My Obituary:
   "A dedicated advocate for education and social empowerment, I devoted their life to uplifting others, creating equitable opportunities, and fostering meaningful change in communities. Through their work in education and leadership, they inspired a generation of young people to realize their potential and make their voices heard. Their legacy is reflected in the countless individuals who were empowered to achieve greatness under their guidance."

To envision this future, I looked to two figures whose accomplishments resonate deeply with me: 
   1. **Nelson Mandela**: Mandela's leadership, resilience, and compassion allowed him to overcome unimaginable adversity. His ability to unite a divided nation through forgiveness and focus on long-term change is a powerful reminder of how patience and vision can drive systemic transformation.
   2. **Malala Yousafzai**: Malala’s courage to advocate for girls’ education, despite immense personal risk, is awe-inspiring. Her story highlights the power of youth activism and the profound impact one person can have when they use their voice fearlessly for a just cause.

## Empowering Students:
As a university student, I’m particularly inspired by actions taken by peers, educators, and institutions to amplify student voices and create opportunities for meaningful engagement:
   - **Student-Led Change**: At my university, student organizations have played pivotal roles in shaping policies on mental health, diversity, and sustainability. For instance, a student-run initiative recently campaigned successfully for increased mental health resources on campus.
   - **Empowering Faculty**: Some professors take extra steps to ensure students feel heard. A professor in my department encourages open discussions on societal issues and incorporates student feedback into curriculum design, showing how teaching can foster both empowerment and collaboration.
   - **Institutional Support**: Universities globally, like Stanford, have launched innovation labs and incubators where students are encouraged to turn ideas into tangible projects. These initiatives empower students by giving them the tools and platforms to lead.

## Takeaway:
As I reflect on these examples, I realize that empowerment isn’t just about large-scale change; it’s about fostering environments where individuals feel supported to take action. This can happen in a classroom, a club meeting, or even through one-on-one mentorship.

"Empowering others starts with believing in their potential and providing them with the opportunities to thrive. As students, we can be both recipients and drivers of that change."

   