This diary file is written by Martin Kubak N16128428 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-07
- The initial lecture was good. 
- I am looking forward the teamwork and making new friends and contacts.

# 2023-09-14
- The first lecture was good. 
- Sadly I could not present my presentation.
- The topic of exponetial growth was interesting yet I am not convinced that the society will develop in the way it was presented with abundance. 

# 2023-09-21
- The second lecture was a little borring because I am not that interested in TED talks.
- The bitbucket and the pull requests are a horribly designed from the user-friendliness perspective. 
- Last attempt (before the deadline on Sunday) was not saved.
- This one will hopefully be ok.
- My group had a great presentation with Ivan as a presenter.

# Diary task - lecture 2023-09-21
In ten years, I can imagine myself living in Sweden, a country renowned for its stunning landscapes, innovation, and quality of life. My professional journey could in a best-case scenario lead me to work as an automotive engineer at Koenigsegg or aerospace engineer at Saab AB, two esteemed companies at the forefront of Sweden's automotive and aerospace industries. 
As an engineer in Sweden in a top-notch company I mentioned, would fill my days with exciting challenges and opportunities for growth. I could contribute to cutting-edge projects, pushing the boundaries of automotive technology or aerospace advancements. My work would involve designing and developing innovative solutions. Just as exiting would hopefully be my salary which would allow me to more than sustain myself in rather expensive country such as Sweden. 
Technological Advancements is one of the current trends of change, and the one I am most excited about as an engineer. Rapid tech innovations will transform our daily routines. The seamless integration of AI in various aspects of life, from smart homes to automated transportation, streamlining tasks and enhancing convenience will make our lives easier and will give us more time for more fun activities. As an engineer the advancements with propulsion and material technology will allow us to start exploring space or at least the neighbouring celestial bodies. 
Climate Change Mitigation is another current trend mostly put into the practise in Europe. Hopefully in following years the rest of the world will follow, and increased awareness of climate change will lead to greener practices. Energy-efficient homes, electric vehicles, and sustainable consumption will be the norm, impacting our lifestyle choices. Even though if I connect this to my future life in Sweden, making the winters a bit warmer there would not be a disaster. Sadly, for most of the worlds fauna and flora it is a different case.  
Remote Work Revolution and flexible arrangements would offer us greater work-life balance. We will have increased freedom to choose where to live and how to spend our days. 
Healthcare Evolution, advancements in medicine, personalized treatments, and health monitoring will enhance our survivability chances even when facing life threatening conditions either from accidents or diseases.
Global Connectivity will broaden our horizons and the interactions with people from all around our world will offer us new opportunities regarding work, hobbies or doing fun activities. 
Embracing the change, we can look forward to a future of improved well-being, enhanced quality of life, and boundless possibilities.

# 2023-10-12

- This lecture was interesting because to learn the basics on finance is important for everybody, even engineers.
- The explaination how the banks and money work was really nice.
- I will do the presentation on this subject to learn more about the problematics.

# 2023-10-19

- My previous save from yesterday has not been saved again. My biggest enemy is this bitbucket apparently. 
- I have successfully did the presentation about economic topis. It was interesting yet in some cases maybe too long and went deeper than our undestanding as engineers. 
- Some of the presentations of other colleagues were also interesting, yet it was crystal clear that most of us did not understand what we were talking about in some cases. Which is completely ok, considering we are mostly engineers. Therefore it was nice from the teacher to make the explanations. 
- The lecture itself was also good and enriching. The video about a guy who helped others after successfully getting rid of shackels of extremism was really nice. I believe that for Taiwanese students this lecture was way more eye-opening that for me and my frieds from Europe since we have faced and learned about these things way more. 

# 2023-10-26

- Today's presentations were interesting since I could hear about futures of different countries and fellow students with different backgrounds and from different cultures. Somr were closer some were a bit further away from my perceived future. Also both of my friends did their presentation which was nice since their future were more European-centric. 
- The new topic about health was nice and welcomed change after extremism and economy.
- The TED talk from the girl was quite good even though I am still not fan of them. This one was at least bit more on a topic I like. 
- As said probably some Greek philosopher of old in a healthy body resides the healthy mind which is something I believe in. 
- Sadly in here I cannot do the sports I did at home because of heat, big city and lack of nice forests nearby. When I spend too much time on assignments it makes me a bit depressed which makes it harder to think as was explained in the class. 

# 2023-11-02

- Mixing the group and the beginning of the class was bit problematic. I could be biased but I do not think local students are used to teamwork in a way we are in Europe, also various level on English within our group played some part. Therefore it was a bit harder at the start. In the end I presented our join presentation which was cool. 

- The new people from my new group seem cool. It will be fun to work with them. 

- Depression is a problematic topic, some people are more prone to it while others are not. Luckily I have never suffered from it or been close to. But I know it's effects, what it can do to people, like it did to my friend. 

- The video from Ted talk was sad. But these things happen more than we realize. The reasons for that vary a lot I believe. For some it could be the work load of NCKU students to put into the diary some joke which hits maybe too close to heart. 

- I agree that it is really important to support and try to understand the person who suffers from depression. It could save their life. 

# 2023-11-09

- My colleagues have interesting presentations about the depression. Also along with story the professor told us I understood how hard it can be for somepeople. My friend suffered from it during his exchange so I had some initial knowledge. What i find personally interesting that from all of the people who are engineers no one openly siffers from depression despide the pressure. I am used to joking about all of the topics, depression included.
- The Tedtalk video i found interesting. Despite that this part of psychology is not really that catching. 
- I find interesting that many Taiwanese students have no work background. In Europe its far different since we know and value the work experience.
- Back to the video, sometimes we can be easily manipulated if we do not care much about the decision. If I should choose between 2 similar pictures and someone gave me the different one, i would not care enough to spot it far less to argue about it.

# 2023-11-16


- The other groups had interesting presentations. For example the presentation about the effect of sleep on study was interesting and I should think about it myself.
- Our group presentation on reduce traffic noise to reduce stress was not perfect but i think we did a good job. Next time we should focus implementable solutions which are possible to use in a real life.
- Looking forward for another lecture since this one about the law was not really interesting, after all I choosed to be an engineer not a layer.

# 2023-11-23

- I managed to do a nice presentation about a CO2 emissions laws in Taiwan and EU. I was suprised that some Taiwanese students asked questions on their own without the 30 seconds time to think about the question. It ment that someone was listening to my presentation.
- My colleague Ivan also did a nice presentation about the laws regarding noise in Taiwan and Croatia.
- The topic of how millenials are addicted to phones was interesting. For many people myself included it has become a problem from time to time. Yet in other occasions the benefits of being connected to family, partner, friends or colleagues is worth it.

# 2023-11-30

- This week was about presenting ideas for actions of the big groups and how they can influence the life of induviduals or society as a whole. As usual some ideas were better than others and I hope that mine was above that average. 
- As a group I think we did a better job than the last time. Other groups also seemed to improve a bit. 
- It was interesting to see how much we rely on our phones. I mean it makes sence since we have acces to so many things there from cards to pay through staying in contact with other people to finding informations and securing services like for example tickets for train or taxi. Being in foreigh country with languague I cannot speak having acces to my phone is vital. At home it would not be as neccesary. 

# 2023-12-07

- The new group mixing offers a good opportunity to meet new people. Some of the people I met during the class were good at team work.
- The topic of lecture being the planetary boundaries is interesting. Due to the climate change people in general should be interested in that topic to reduce the climate change causes or mitigate its damage.
- I new from before the concept of planetary boundaties but it helped me to refresh some knowledge.

# 2023-12-14

- Most of the time of the class was spend on big group projects and voting on whoes idea is the best. Some groups had a nice ideas which could help some people or enviroment in general. 
- The voting was a bit confusing at first which is maybe the reason why so many people changed their votes everytime.
- Our group topic is really hard to implement. Noise is not more outside  of our grasp than lets say use reusable plastic bags or sleep more. We will try to ask the city of Tainan to get some noise data.

# 2023-12-21

- # 2023-12-22 Friday
- A. Sucessful and productive
- B. I was succesful and productive since I managed to get up quicly and go swimming and to school afterwards. I was still sad because of the shooting in the city where I study which affected a lot of my friends and family. It was a big tragedy.
- C. I will study for finals.

- # 2023-12-23 Saturday
- A. Sucessful and productive
- B. I studied for my oral test in Chinese and final in Theory of Combustion and Turboengine principles which are very hard exams.
- C. I will do the same as on Saturday since the finals are coming despite the chrismass.

- # 2023-12-24 Sunday
- A. Sucessful and productive
- B. I studied for finals in Theory of Combustion and Turboengine principles.
- C. Tomorrow I will do the homework for Theory of Combustion and study Chinese. 

- # 2023-12-25 Monday
- A. Sucessful and unproductive
- B. I studied for finals in Theory of Combustion and tryied to do the homework but failed. It was too hard.
- C. Tomorrow I will do the homework for Theory of Combustion and study. 

- # 2023-12-26 Tuesday
- A. Sucessful and unproductive
- B. I studied for finals in Chinese and tryied to do the homework but failed again.
- C. Tomorrow I will finish the homework for Theory of Combustion and study. 

- # 2023-12-27 Wednesday
- A. Sucessful and productive
- B. I studied for oral test in Chinese and did the oral test. Result was quite good. Homework is finished.
- C. Tomorrow I will study for finals in Turbo Engine princpiples.

- # 2023-12-28 Thursday
- A. Sucessful but not productive
- B. I failed to study but managed to do some sport.
- C. Tomorrow I will study for finals in Turbo Engine princpiples finally

- # 2023-12-29 Friday
- A. Sucessful and productive
- B. I studied for Turbo Engine princpiples but its very hard.
- C. Tomorrow I will study it again.

- # 2023-12-30 Saturday
- A. Sucessful and productive
- B. I continued to study and created nice paper with how to calculate some tasks.
- C. I will try to celebrate in between studying tomorrom.

- # 2023-12-31 Sunday
- A. Sucessful and not productive
- B. I celebrated new year but not studied.
- C. Tomorrow I will try to study again.

- # 2024-01-01 Monday
- A. Sucessful and productive
- B. I studied for finals in Theory of Combustion which is the next day.
- C. Tomorrow I will write the final.

- # 2024-01-02 Tuesday
- A. Sucessful and productive
- B. I think i managed to write the final good.
- C. Tomorrow I will study for final in Chinese and write the test.

- # 2024-01-03 Wednesday
- A. Sucessful and productive
- B. I wrote the final and i am pround of myself.
- C. Tomorrow I will write the final from Professional skills for engineering the third industrial revolution.

- # 2023-12-28 Thursday
- A. I will be sucessful but productive hopefully. 
- B. ...
- C. Tomorrow very final exam from Turbo Engine Principles. 
