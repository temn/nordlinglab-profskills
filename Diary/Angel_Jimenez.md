# 2021-09-30

* The word is getting better presentation
* How to do a properly citation
* Solving groups problem
* No one is perfect, everyone makes mistakes
* Favio drinkig terere
* I have problem creating my diary
* How to detect fake news
# 2021-10-07

* topic: financial system
* Only trust in yourself
* Favio drinking terere and sleeping
* Rene dislike 'Ted Talks'
* I do know nothing about financial concepts
* Middle class people produce the money
* Printing more money does not help
* I finally created my diary
# 2021-10-10

* I still do not about financial system.
* I was depressed during the class so I went to the gym.
* My friends always come to my house for this class.
* Next time I will pay more atention.
* My country's government sucks.
# 2021-10-14

* The presentation about white supremacy was really interesting, I believe in my country we do not have this kind of groups 
* The video about the immigration crisis was realy emotive but i left with questions like, why does the parents decide to bring their children to cross the frontier if they know the conditions are so harsh?
* Now we can record our presentantion and post it on youtube for the homework, I think it is better that way.
* I went to decathlon with my friends and we met a taiwanese who can speak spanish, it was really cool.
* The professor asked who felt depressed during the week and almost everyone raised their hands.
* I really enjoyed my weekend!
# 2021-11-07

* Depression 
* The way I deal with depression is exercising, going to the gym, but sometimes I dont want to get up and keep fighting
* My family is my biggest motivation that keeps me on track
* Being far away from home is really hard, but life is not easy anyway 
* I hope everyone can fulfill their goal and be successful
# 2021-11-28

* Group presentation: pollution situation in Taiwan 
* Whalter presented about the noise limit in Taiwan and Paraguay 
* As I expected, the situation in Paraguay is pretty bad, there are helpless law to minimize this pollution 
* This week passed so fast, I did a lot of fun things, playing rugby and going to the gym make me happy 
* I am playing the freshman rugby cup and in this year I am the number 10, tackling other players relieves my stress 
* Why life is so hard or is just us making it to be hard? Hope I do not broke my nose again.
# 2021-12-12

* Group presentation: news from different sources
* Class topic was planetary bounderies, it was really cool.
* I thought the climate change situation was worse than how it really is rn
* Rene propuose to kill the elderly people to survive when where is not any food left 
* I support Rene's idea, but I do not want my parents to die
* It was a cold weekend
* I am kinda sad tho, beacuse Nicaragua broke relations with Taiwan and I have many Nicaraguan friends 
* You can do whatever you want when you have money. China bought Nicaragua (El presi de Nicargua es bien corructo) me la pela 
* NCKU is the best university in Taiwan
# 2021-12-19

* MERRY XMAS AND HAPPY ENDING OF SEMESTER!
* This weekend was a relaxed one, before the storm there is the calm they said, or how was it hahaha
* Being honest I am on vacation mood already, hate why in this part of the world we are studyng while in my country is vacation already
* New favorite music: Infinity by Olamide
* Today I went to watch 'Spiderman 沒辦法回家', it was not that bad hahaha
* Yesterday we went to a mexican restaurant with some friends, it was a really good nigth. One of my friends was Dj'ing so we ask him to play some of our fav songs 
* Sorry Rene, I could not make it today hahaha, how was the baleadas? 
* We are going to play the rugby freshman cup final match, getting ready to tackle some newbies jajaja
* I just remember one of the taiwanese player said that his teammates were scared of us (foreigner players) lol
-------------------------------------------------------------------------------------
# 2022-01-01 Saturday
* I cannot say that today was successful because I got up late, however, I did some productive things like cleaning my room and packing to move out
* Thanks guys for helping me today, going back to dorms after all. Cheers for it anyway!
# 2022-01-02 Sunday
* I woke up before my alarm and I did my morning workout routine, I finished packing my things to move out also. However, I feel like this is not a successful day anyway.
* Hopefully this year I will take better decisions in my life. 'Make sure you know the people you are spending or you will spend your time with' 
# 2022-01-03 Monday 
* I really forgot to write the diary today so I could say that yesterday was an unsucessful and unproductive day. I was busy moving out, and I cleaned out my new place and I fell asleep.
# 2022-01-04 Tuesday 
* Today I woke up early and I went to the gym with my friend, consequently I met the girls that yesterday while I was working out (I though) were looking at me, however, they were looking at the maquine I was using hahaha, anyway, they are really cute and friendly, anyway, now I am really inspired to go back to exercise tomorrow.
* I still do not know when a day is successful or not, but I can say today was a productive day. Happy and high! Just two weeks more to go.
# 2022-01-05 Wednesday 
* What a dream I had yesterday. I woke up before my alarm rang and then I wait until it ring, so I can turn it off. I took a shower to get ready for my day, after that I went to my fav but unfearly expensive brunch store to eat breakfast, while I was eating I lost track of time and I was late to my class, kinda sad the last day of that class; once finished, I ran to the gym to do my daily exercise routine (leg day), after I have done with the exercise it was already lunch time, so I bought some pasta, after I got to my house, I took a 3 hour nap, now I am studying thermo to pass the final (hopefully)
* Productive day, but still unsuccessful

# 2022-02-05 Wednesday 
* 1) World demographics are changing as time passes; the global population is rapidly increasing, which means that numbers and situations will change. To keep up with these changes, we must pay attention to international news to receive accurate information and avoid being duped by fake news. 

2) We can determine what needs and/or opportunities exist in terms of engineering based on a country's demography. For example, Paraguay is a country whose main source of income is agriculture and livestock, for which many people studied agricultural engineering, so many that there are now many professionals without a job opportunity, but seeing as Paraguay has two large hydroelectric plants for energy production without few professionals, we can determine what needs and/or opportunities exist in terms of engineering. One of the reasons I opted to major in energy engineering is because of this. 

3) The greatest way to learn and be aware of what planetary boundaries are and their status is to read fresh documents and scientific articles. 

4) By analyzing and comparing the situations of various countries, we can see inefficiencies in the distribution of resources. Looking at news from various countries is a good way to understand how the current economic situation failed in the distribution of these resources. As an example, while people in Canada can acquire basic essentials, Venezuela is experiencing a crisis. 

5) Right now, there is a lot of development in AI; in fact, progress is so tremendous that the automation of various machines is only around the corner, with many people losing their jobs as a result. Knowing that the future is the definition of development, we must always grow and enhance our skills in order to be prepared for an unknown future. 

6) We rely on data to ensure that anything we do is safe; take, for example, the food we consume; before we eat it or buy it, we check for information; here, engineering is used to improve the food quality as well as the creation of the resources we need to create that meal. 

7) Human beings live in a society where those who have more contacts have more opportunities for progress, therefore more opportunities for a better quality of life. 

8) Depression is one of the most common problems today, many of us have already had this problem, including me. The best thing in this case is to seek professional help and approach our loved ones to be able to cope with this problem, and always respect and be kind to others. 

9) Thanks to the ted talks of last class I discovered that there are algorithms to find the best partner, even so I am not very aware of the subject, researching on the subject would be my best option to understand and familiarize myself with this subject. 

10) Not believing everything before being sure that the news is real, the best way to avoid falling for fake news is to look at various resources and compare them. 

11) Personally, taking this course help me a lot to understand how to analyze data, however I still have a lot to learn, searching more information about it will be the best way for me to improve in this area. 

12) Working in a group is the best way to find our weaknesses and be able to improve them. In my case, I have to listen more to others and take more initiative. Participating in group activities can help me a lot. 

13) To be professionally successful we need to rely on our skills to socialize since our contacts and opportunities depend on that. 

14)  so bbasically when we work in an environment that everyone works it will influence others to work as well