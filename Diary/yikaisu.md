September 28, 2023

Today is my 21st birthday. It was pretty much the same as usual, but I still had a lot of fun.
Having a meal with friends and chatting at home will make you happy


September 30, 2023

Today is the Moon Festival. This is the week I am most looking forward to all year. It has my birthday and the Moon Festival.
I like eating meat very much, so I go home and eat meat with my family at this time of year. This year, I invited many friends back, which is a very unforgettable experience.


October 1, 2023 

Today is a beautiful Sunday, marking the beginning of a new month in my diary. In the morning,
I went to a café with friends and enjoyed a delicious brunch. In the afternoon,
I spent some time reading a good book, completely unwinding. The evening was spent with family, 
sharing a delightful dinner and expressing gratitude for this wonderful day.


October 5, 2023 

This week has flown by! Today was a workday, and I navigated through a busy day. After work, I had dinner with my friends, 
engaging in casual conversations to unwind. Back at home in the evening, I continued reading my favorite novel, concluding the day on a relaxing note.


October 12, 2023 

This week has been quite hectic, with work pressure mounting. Nevertheless, amidst the busyness, I managed to carve out some time for exercise to maintain physical well-being. 
Tonight, I opted for a leisurely activity, watching a movie with friends to unwind.


October 15, 2023 

The week concluded with a family gathering, spending quality time with relatives. 
The week was fulfilling, and I look forward to the start of a new one.


October 16, 2023 

Today marked the beginning of a busy week. Morning classes kept me on my toes, but the highlight was a group project discussion in the afternoon. 
Collaborating with classmates sparked some creative ideas, and I'm looking forward to bringing them to life.


October 20, 2023 

The week continued with a mix of classes, study sessions, and a bit of socializing. Friday night brought a much-needed break – a movie night with friends.
It's amazing how a good film and some laughter can lift the weight of assignments and exams.


October 23, 2023 

Midterms are approaching, and the library has become my second home. Despite the stress, I managed to take short breaks to recharge.
Tonight, I attended a campus event, enjoying live music and connecting with fellow students. It was a refreshing change of pace.


October 25, 2023 

 I spent the morning in the library, followed by a workout session in the afternoon. 
It's essential to keep both the mind and body in good shape during intense study periods.


October 28, 2023 

The weekend finally arrived, and with it came a mix of emotions. Balancing relaxation and catching up on assignments is an ongoing challenge. 
In the evening, I joined a study group, turning work into a collaborative effort. Supportive friends make the academic journey more manageable.


October 31, 2023 

Halloween brought a welcome break from the academic routine. Dressed in a makeshift costume, I attended a campus Halloween party with friends.
It was a night of fun, laughter, and forgetting about deadlines for a while. A much-needed escape before diving back into the last leg of the semester.


November 3, 2023 

The month kicked off with a heavy workload. Morning lectures, followed by an afternoon lab, left little room for breaks.
The looming thought of upcoming exams started to cast a shadow over my day.


November 5, 2023 

Weekends are supposed to be a respite, but not during exam season. Spent the day in the library, poring over notes and textbooks.
The pressure is building, and it's hard to escape the constant countdown to the first set of exams.


November 8, 2023 

Midterms are upon us, and the stress is palpable. Late-night study sessions have become the norm, and coffee feels more like a survival necessity than a choice.
The library is a maze of books, papers, and stressed-out students.


November 10, 2023 

Managed to squeeze in a short break to attend a group study session. Collective stress seems to fill the room, but there's a shared determination to conquer the upcoming exams.
The camaraderie provides a small respite from the pressure.


November 13, 2023 

The first round of exams has begun. Anxiety is at its peak, and it's a struggle to recall all the information under time constraints.
The library is eerily quiet, the tension in the air palpable.


November 15, 2023 

Midterm week is halfway through, but the pressure persists. The mental fatigue is starting to set in, and every moment feels like a race against the clock.
Late-night study sessions have become a solitary ritual, with the hope that hard work will pay off in the end.


November 16, 2023 

Exams are finally over! The relief is palpable. Spent the morning revisiting notes, but the weight has lifted. After the last exam,
treated myself to a quiet lunch – a small celebration for surviving the intense weeks of studying.


November 20, 2023 

The post-exam fatigue is real. Despite the exhaustion, caught up with friends over a casual dinner. 
The conversation shifted from textbooks to plans for the upcoming break. A well-deserved respite is on the horizon.


November 28, 2023 

Returning to campus after the break feels like a reset. Caught up on rest, met with professors to discuss the exam results,
and even had time for a leisurely stroll around the campus. Ready to face the final stretch of the semester.


November 30, 2023 

As the month comes to a close, there's a renewed sense of focus. Classes resume, and projects are on the horizon, but the memory of the intense exam period serves as a reminder of resilience.
The brief break was a necessary pause, and now it's time to finish the semester strong.


December 1, 2023 

The calm after the storm didn't last long. December begins, and with it, the relentless cycle of exams resumes.
Spent the day reviewing materials for the upcoming tests in the automatic control courses. The library is my second home again.


December 5, 2023 

Another day, another exam. 
The mechanical intricacies of mechanical Design and fluid dynamics dominate my thoughts. The coffee intake is steadily increasing.


December 8, 2023 

The grind continues. Today's exam focused on mechanical design principles. The pressure is mounting as the days pass. Even lunch breaks are spent revising equations and formulas.
The struggle is real, but so is the determination.


December 10, 2023 

Weekends no longer signify rest. Instead, they bring double the workload. A day filled with automatic control and automatic control leaves me mentally drained.
The library lights flicker as the night progresses, a silent witness to countless equations being solved.


December 15, 2023 

The middle of the month feels like the climax of the semester. Today encapsulates the essence of a mechanical engineering student’s life – testing two-wheelers.
In the examination room, everyone was praying that their robots could run according to the program. Fortunately, the robots in our group were very stable and got the highest score in the class.

December 16, 2023 

The countdown to the end of the semester is on, but the pressure is mounting. Spent the day buried in textbooks, reviewing notes,
and preparing for the upcoming final exams. The library becomes a silent witness to the collective hustle and bustle.

December 20, 2023 

Midnight oil has become my constant companion. Exam preparations are reaching their peak, and every moment is crucial. The stress is tangible, but the goal is clear – get all pass.

December 24, 2023 

Christmas Eve arrives, bringing a welcome break from the relentless studying. Managed to find a balance between reviewing materials and enjoying some festive treats with friends.
The holiday spirit provides a brief respite.

December 28, 2023 

The library is eerily quiet as most students are away for the holidays. A chance to focus intensively on individual study sessions.
The looming finals are both a challenge and an opportunity to showcase months of hard work.

December 31, 2023 

New Year's Eve is here, and despite the impending exams, a decision is made to take a break.
Invited friends for a barbecue celebration to bid farewell to the year. Grilled meats, laughter, and shared resolutions offer a temporary escape from the academic grind.

January 1, 2024 

The clock strikes midnight, marking the beginning of a new year. Despite the looming exams, the decision to celebrate with friends was worth it.
The sense of camaraderie and the brief break provide a fresh perspective and renewed energy to tackle the final stretch of exams.

happy new years!!!!

