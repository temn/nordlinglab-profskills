# 2024/09/19
* I think I can a lot fron this course because it is quite different from other courses.
* I was shocked by the result of the exam and realized how ignorant I am about the world.
* I believe I can learn a lot about how the world works.

# 2024/09/26
## Future Living Location and Career Goals
* I plan to live in a scenic and sustainability-conscious region, such as Northern Europe or the West Coast of the United States.
* Career-wise, I aim to become a materials engineer, focusing on researching energy materials related to sustainable development, especially battery technology.
## Trend 1: Sustainability and Climate Action
* The increasing global emphasis on sustainability and the demand for clean energy are driving innovations in the field of materials engineering.
* I will focus on developing materials that improve energy storage efficiency, contributing to advancements in green energy technologies.
## Trend 2: Advancements in Energy Storage Technology
* Over the next decade, battery technology, such as solid-state batteries, will see significant progress, leading to new material demands.
* I will dedicate myself to researching next-generation energy storage solutions while continuously improving my knowledge, especially in nanotechnology and chemical engineering.
## Trend 3: Digitalization of Research and Development
* Artificial intelligence and machine learning are transforming how research is conducted in materials science, making the process more efficient and faster.
* I will integrate digital tools into my research process to innovate and develop new materials at a quicker pace.
## Trend 4: Global Collaboration and Innovation
* The trend of global collaboration in scientific research is promoting cross-border partnerships, and I will participate in international projects to solve global sustainability challenges.
* These collaborations will help me propose solutions to both local and global issues in the field of materials engineering.
## Trend 5: Growth of the Electric Vehicle Market
* As the electric vehicle market rapidly expands, the demand for efficient and sustainable batteries will rise.
* I will focus on developing new battery materials that improve energy efficiency and reduce environmental impact, contributing to a greener future.

# 2024/10/17
## Video 1 
* Banks create money through loans, not deposits.
* The central bank’s monetary policy sets limits on bank lending.
* Quantitative easing (QE) affects bank deposits and asset values.
* Central bank money consists of notes and reserves.
* Broad money includes all bank deposits of households and companies.
* Loans create additional broad money.
* Monetary policy determines loan rates and affects lending.
* QE increases reserves held by banks but does not directly stimulate lending.
* QE affects the economy through increased bank deposits and asset values.
## Video 2 
* The economy works like a simple machine driven by productivity growth, short-term debt cycles, and long-term debt cycles.
* Credit is key in driving economic growth or creating economic cycles.
* Deleveraging can be successful if handled well, involving spending cuts, debt reduction, wealth redistribution, and money printing.
* Transactions are the building blocks of the economic machine, driven by human nature.
* Credit is the most important and volatile part of the economy, creating cycles of borrowing and spending.
* Short-term debt cycles involve credit-fueled expansions and contractions controlled by the central bank.
* Long-term debt cycles involve rising debt burdens, leading to deleveraging, which can be either painful or successful depending on how it's managed.
* Deleveraging includes cutting spending, reducing debt, redistributing wealth, and printing money to maintain economic and social stability.
* Balancing income growth with debt growth is crucial in achieving successful deleveraging.
## Video 3 
* Professor Richard Werner explains how banks create money out of nothing through credit creation.
* There are three theories of banking: financial intermediation theory, fractional reserve theory, and credit creation theory.
* The credit creation theory, where banks create money out of nothing, is the correct theory.
* Banks do not take deposits or lend money; they create money through the process of credit creation.
* Central banks monitor bank credit creation and guide it to prevent banking crises.
* Banks can create credit for financial transactions, consumption, or business investment.
* Centralized decision-making in banking, as seen in the Soviet Union and the ECB, has led to failures and crises.
* Decentralized decision-making, as seen in China and Germany with community banks, has been successful.
* The solution is to keep public money in the hands of local community banks for decentralized decision-making.

# 2024/10/24
* In today’s lecture, we discussed extremism and racial profiling. One of the videos introduced neo-Nazism, featuring a young American boy who became radicalized when his parents failed to provide him with the proper attention he needed. Although his parents were immigrants themselves, he quickly turned against foreigners and became violent for racial reasons.
* This reminded me of something I heard long ago: "Your weakness is what you care about the most." His lack of care and attention made him vulnerable to manipulation, but thankfully, the right love and guidance helped him find his way back.
* A classmate then mentioned the issue between Taiwan and China, suggesting that perhaps reducing hatred could lead to a better future.
* Next, we discussed assumptions, hypotheses, and the process of rejecting or accepting these hypotheses, which taught me the principle that truth becomes clearer through debate. I also learned that a single falsification can reject a hypothesis, while countless confirmations are needed to establish one as correct.
* Humanity still has limited understanding of the universe, but in modern society, we can continue exploring it with the power of technology and AI, as long as we’re willing to do so.

# 2024/11/7
## How to Spot Depression
1. Behavioral Changes:
* Withdrawal: Loss of interest in hobbies or avoiding social interactions.
2. Emotional Signs:
* Sadness: Persistent sadness, irritability, or feelings of hopelessness.
3. Physical Symptoms:
* Sleep and Appetite Changes: Insomnia, oversleeping, or drastic eating habits.
4. Decline in Performance:
* Work/School Struggles: Missed deadlines or reduced performance.
## How to Help
1. Start a Conversation:
* Show care, listen without judgment, and encourage them to open up.
2. Encourage Professional Help:
* Suggest therapy and offer to assist in finding resources.
3.Promote Positive Activities:
* Plan uplifting activities together, such as walks or sharing dark chocolate.
4. Follow Up Regularly:
* Check in often to show continued support.

## Key Claims
* Social withdrawal and sadness are signs of depression.
* Changes in sleep or appetite indicate possible mental health issues.
* Positive activities can help improve mood.
* Professional help is essential for recovery.
* Regular check-ins provide emotional support.

## Hypothesis: Eating dark chocolate daily reduces stress in individuals with mild to moderate stress.
## Testing Steps
1. Participants: Recruit individuals with mild to moderate stress.
2. Groups:
* Experimental Group: Eat 40g of dark chocolate (70% cacao) daily for two weeks.
* Control Group: Avoid chocolate during the same period.
3. Measurements:
* Use a stress questionnaire (e.g., Perceived Stress Scale) before and after the experiment.
4. Analysis:
* Compare pre- and post-test scores between groups using statistical tests (e.g., t-tests).

# 2024/11/14
## Key Areas for New Hires
* Gained understanding of six essential areas for new hires to achieve a smooth transition.
* Learned how to quickly adapt to new roles and contribute effectively to team goals.
* Acquired skills to navigate challenges and develop key competencies required in the workplace.

# 2024/11/21
I was absent due to folliculitis, as I was worried it might worsen into tetanus.

# 2024/11/28
I was absent due to taking sick leave for a follow-up medical appointment.

# 2024/12/5
## Global News and Press Freedom
* Groups presented how news sentiment relates to each country's global press freedom score.
* We discussed media perspectives and identified which stakeholders benefit from specific narratives.

## Planetary Boundaries
* Learned about the nine planetary boundaries, six of which have already been crossed.
* The most concerning is "novel entities" like plastics, chemicals, and pollutants that disrupt ecosystems.

## Johan Rockström’s Video
* Watched “Human Growth Has Strained the Earth's Resources” by Johan Rockström.
* Learned about "tipping points" and Earth's limited capacity to buffer environmental stress.

## Kate Raworth’s Video
* Watched “A Healthy Economy Should Be Designed to Thrive, Not Grow” by Kate Raworth.
* Introduced to the "doughnut economy," advocating for well-being within ecological limits.

# 2024/12/12
* This week, we focused on planetary boundaries through a group debate on action plans for our final project. Each team presented strategies to support specific boundaries, followed by a Q&A session where representatives clarified their approaches. The session concluded with a vote to select the most impactful proposal.
* We also watched a segment of Jeremy Rifkin’s The Third Industrial Revolution, which emphasized the shift toward a sustainable, collaborative economy. The documentary highlighted the role of digital innovation, renewable energy, and collective action in driving systemic change and aligning economic growth with ecological sustainability.


# 1. Know the current world demographics
## Interactive Data Exploration
* World Population Map: Utilize online interactive map tools to allow students to explore data such as world population distribution, density, and growth rate. Examples include Google Earth, Worldometer, etc. Students can click on different countries or regions to view detailed information and compare population characteristics across different areas.
* Population Pyramid: Introduce the concept of population pyramids and use online tools or software to allow students to create and analyze population pyramids for different countries. An example is PopulationPyramid.net. Students can compare the differences in population structure, such as the degree of aging and birth rate, between different countries or regions, and discuss their impact on society and economy.
* Simulation Games: Use simulation games to have students role-play as policymakers, formulating policies based on population data. For example:

1. Resource allocation: Simulate the allocation of limited resources among different population groups, prompting students to consider the fairness and efficiency of resource allocation.
2. Urban planning: Simulate urban development, allowing students to plan housing, transportation, and public facilities based on population growth trends. Encourage them to consider the impact of factors such as population density and age structure on urban planning.
3. Environmental challenges: Simulate the impact of population growth on the environment, such as carbon emissions and resource consumption. This will help students explore the relationship between population and the environment and consider solutions to environmental problems.

## Discussion of Issues and Critical Thinking: Case Analysis
* Have students analyze population issues in specific countries or regions. For example:

1. Japan's aging population: Explore Japan's policies and measures in response to its aging population, as well as their effectiveness and challenges.
2. India's population explosion: Examine the causes and impact of India's rapid population growth, as well as the measures taken by the government.
3. Population migration in Africa: Discuss the causes and impact of population migration in Africa, and how the international community should provide assistance.

# 2. Ability to use demograpjics to explain engineering needs
## Real-world case studies:
* Find engineering cases relevant to students' lives: For example, analyze the relationship between the local population age structure and public transportation facilities, the relationship between population density and green space planning, and the differences in the needs of different ethnic groups for technology products.
* Invite engineers to share their experiences: Invite engineers from different fields to share how they use demographic data to make decisions. For example, how civil engineers design bridges based on population density, how software engineers design app interfaces based on user age groups, etc.
* Conduct field trips: Take students to visit engineering-related facilities, such as sewage treatment plants, power plants, construction sites, etc., to let them observe how engineering meets the needs of different demographic groups

## Interactive data exploration:
* Use online interactive data platforms: Use platforms such as Gapminder and Datawrapper to allow students to explore population data from different countries or regions and observe the relationship between data and engineering needs.
* Data visualization competition: Divide students into groups and let them use different visualization methods to present the same population data, and explain their impact on engineering needs.
* Role-playing: Let students play different roles (e.g., urban planners, product designers, environmental engineers, etc.) and propose solutions or design products based on given population data.

## Critical thinking training:
* Analyze the bias behind the data: Guide students to think about the potential biases in the data collection process and how these biases might affect engineering decisions.
* Debate the interpretation of data: For example, debate topics such as "Does population aging mean that more nursing homes need to be built?" to encourage students to think about problems from different perspectives.
* Write data analysis reports: Have students conduct research on specific engineering issues, collect and analyze relevant population data, and write reports to make recommendations.

## Interdisciplinary integration:
* Combine sociology, economics, and other disciplines: Let students understand the relationship between demography and other disciplines, such as the relationship between population migration and economic development, and the relationship between cultural differences and engineering design.
* Explore the impact of population changes on the environment: For example, the demand for water resources due to population growth, the impact of urbanization on air quality, etc., to guide students to think about the sustainable development of engineering.

# 3. Understand planetary boundaries and the current state
## Introduce the Concept:
* Start with the "why": Begin by discussing the interconnectedness of Earth's systems and the potential consequences of exceeding planetary boundaries. Use compelling visuals like the "planetary boundaries" diagram to illustrate the concept.
* Define key terms: Explain terms like "thresholds," "tipping points," and "resilience" in an accessible way. Provide real-world examples to help students grasp the concepts.
* Historical context: Briefly discuss the history of human impact on the planet, leading to the need for understanding planetary boundaries.

## Explore the Nine Boundaries:
* Interactive exploration: Use online resources, videos, and simulations to delve into each of the nine planetary boundaries (climate change, biosphere integrity, land-system change, freshwater use, biogeochemical flows, ocean acidification, atmospheric aerosol loading, stratospheric ozone depletion, and novel entities).   
* Case studies: Analyze specific examples of how human activities are impacting each boundary. For example, deforestation in the Amazon (land-system change), plastic pollution in the ocean (novel entities), or fertilizer runoff causing algal blooms (biogeochemical flows).
* Guest speakers: Invite scientists or experts working in fields related to the planetary boundaries to share their insights and experiences.

## Current State and Future Implications:
* Data visualization: Utilize graphs, charts, and maps to present the current state of each planetary boundary. Encourage students to analyze trends and identify areas of concern.
* Debates and discussions: Organize debates on controversial topics related to planetary boundaries, such as the role of technology in mitigating environmental impact or the ethics of geoengineering.
* Future scenarios: Engage students in envisioning potential future scenarios based on different pathways of human development and their impact on planetary boundaries.

## Encourage Action and Solutions:
* Solution-oriented projects: Have students research and propose solutions to address challenges related to specific planetary boundaries.
* Community engagement: Encourage students to participate in local initiatives related to sustainability and environmental protection.
* Creative expression: Use art, writing, or multimedia projects to express understanding of planetary boundaries and inspire action.

## Tools and Resources:
* Stockholm Resilience Centre: This website provides comprehensive information on planetary boundaries, including research publications, data visualizations, and educational materials.
* World Wildlife Fund (WWF): WWF offers resources on various environmental issues, including climate change, biodiversity loss, and sustainable consumption.   
* Intergovernmental Panel on Climate Change (IPCC) reports: These reports provide the latest scientific assessments on climate change and its impacts. 

# 4. Understand how the current economic system fail to distribute resoureces
## Historical context
* Briefly discuss the evolution of economic systems and how they have shaped resource distribution throughout history.

## Case studies
* Analyze specific examples of how the current system has failed to distribute resources effectively, such as the 2008 financial crisis, the Flint water crisis, or the global food crisis.

## Tools and Resources:
* The World Inequality Database: This database provides comprehensive data on global wealth and income inequality.
* Oxfam reports: Oxfam publishes reports on various issues related to poverty, inequality, and social justice. 
* The Doughnut Economics Action Lab: This organization promotes the concept of "doughnut economics," which aims to create a socially just and ecologically safe space for humanity.

# 5. Be familiar with future visions and their complications, such as AI
## Focus on AI and its Potential Complications
* The basics of AI: Provide a basic understanding of what AI is, how it works, and its different types (e.g., machine learning, deep learning).
* Potential benefits: Discuss the potential benefits of AI across various domains, such as healthcare, education, transportation, and environmental sustainability.
* Potential complications: This is the core of the topic. Explore the potential negative consequences of AI, including:

1. Job displacement: Discuss the potential for AI to automate jobs and the impact this could have on employment and the economy.
2. Bias and discrimination: Analyze how biases in data can lead to AI systems that perpetuate or even amplify existing social inequalities.
3. Privacy concerns: Examine the potential for AI to be used for surveillance and data collection, raising concerns about privacy and individual freedoms.
4. Ethical dilemmas: Discuss the ethical challenges posed by AI, such as autonomous weapons systems, the use of AI in decision-making that affects human lives, and the potential for AI to surpass human intelligence.
5. Existential risks: Explore the long-term risks associated with advanced AI, including the possibility of uncontrolled superintelligence and its potential impact on humanity.

## Guest speakers
* Invite AI researchers, ethicists, and policymakers to share their insights and perspectives on the future of AI and its potential complications.

# 6. Understand how data and engineering enable a healthier life
## understand the importance of data
* Data collection and analysis: Discuss how data is collected from various sources (patient records, wearables, clinical trials) and how it is analyzed to identify patterns, trends, and insights.
* Personalized medicine: Explain how data analysis enables personalized medicine, tailoring treatments and interventions to individual patients based on their unique characteristics.   
* Predictive analytics: Discuss how data can be used to predict health risks and outcomes, allowing for early interventions and preventive measures.
* Public health surveillance: Explain how data analysis helps track and monitor disease outbreaks, identify public health threats, and inform public health interventions.

## real example
* Case studies: Analyze specific examples of how data and engineering are being used to improve health outcomes, such as:

1. Using AI to analyze medical images for faster and more accurate diagnoses.   
2. Developing wearable sensors to monitor vital signs and detect early signs of health problems.   
3. Using CRISPR technology for gene editing to treat genetic diseases.   
4. Creating 3D-printed prosthetics that are customized to individual patients.   
5. Guest speakers: Invite healthcare professionals, data scientists, and engineers to share their experiences and insights on how data and engineering are transforming healthcare.
* Ethical considerations: Discuss the ethical implications of using data and engineering in healthcare, such as data privacy, algorithmic bias, and access to technology.

# Know that social relationships gives a 50%increased likelihood of survival
## Introduce Interviews with Depression Survivors
* Connect to Research Findings: Explain how depression affects physical and mental health, and even increases mortality risk. Emphasize the importance of social relationships in depression recovery, echoing the research findings of "social relationships increasing survival rate."
* Interview Content: Refer to the interview design suggestions mentioned earlier and guide interviewees to share their experiences, such as:
* What kind of interpersonal interactions or support helped them when their depression was most severe?

1. How did they rebuild or maintain social relationships?
2. What advice do they have for others on interpersonal relationships and mental health?
3. Diverse Cases: Select interviewees with different backgrounds and recovery journeys to showcase the multifaceted nature and importance of interpersonal relationships.

## Supplementary Resources
* Related Books and Articles: Provide books or articles that explore interpersonal relationships and mental health, such as "Lost Connections," "Mindset," etc.
* Support Group Information: Provide information on depression support groups or mental health counseling for learners who need help.