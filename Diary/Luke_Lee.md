This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Luke Lee C14114716 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-21 #

Depending on where I will get my master's degree and what I will be majoring in, I might still be living in Taiwan or maybe move to America.
For my major, I am decideing that whether I will keep on studying in the department of mathematics, or maybe go study anything related to it, probably something related to programming.
Although I haven't decide which particular job will I be having in the future, but I believe that artificial intelligence will matter a lot in my job.

The five current trends that will affect me the most is:

1. Artificial intellegence
With the rising of artificial intelligence, although many jobs may be replaced because of the high performance of it, but it can also assist us on more complex things for example surgical operation.
I believe that in those ten years, artificial intellegence may be improved so that better decisions can be made will preforming surgery, and that is not the only thing that it can assist us, many jobs can be improved with the assistance of artificial intellengence.
And because of I will most likely to be working with artificial intellegence in the next ten years, so what it can do after ten years will effect what my job will be.

2. International relations
One of the most important thing that Taiwan needs to consider about is the relation between China, I believe that they really should just leave us alone, but it is difficult to predict what their president is thinking, and whether or not the war will happen, it will certainly affect where I will be living after ten years.

3. House price
Although I am not familar with the price in the US, but the house price in Taiwan is certainly growing that most of us can't afford it, and depending on whether or not the government will do anything about it, I might move to America because of the houses are more affordable.

4. The government
Both of the political parties are kind of corrupted, and it will certainly effect both of the two trends above, for Taiwan to have a better living environment, political parties that are different from the current two biggest parties must be able to stand out.

5. The environment
The latest biggest news about the green environment is Japan nuclear wastewater, although I personally think that there is no major damage that it may cause to the environment, but if other countries follow up, the damages may accumulate and cause irreparable harm to the environment, and after ten years these damages may effect on where I will be living and my health issue effected by it.

# news story #
Australian journalist back home after 3 years behind bars in China
* Australian journalist who had been detained on national security charges was released after 3 years, no immediate comment from the judiciary in China, and this may be a real progress on both sides relationship.
* Australian journalist who had been detained on national security charges was released after 3 years while Beijing said Cheng was “deported.”
* Mostly same as the above article (showing both sides statements)
* China's statement on the journalist was being deported.

# 2023-10-12 #

* Money does not depend on a countries' gold reserve but purely based on the trust of the people.
* Makes me wonder how does the currency value updates all the time.
* Will there be a organization that checks if a country is reporting a accurate currency value? Because it seems easy to control the currency value in some closed country.
* A lot of vocabs in economics which are really confusing.
* When will the next ecnomic depression come due to every price is increasing so fast.

# 2023-10-19 #

* I can't attend to today's course so I just watched the video the professor gave.
* I believe that the reason why fascism and other extremism may flourish in either today's or pass society is that the people who are into this are much more manipulative.
* Before fascism effects so much people is that the country is trying to brainwashing them so that the country can be more united.
* Now extremism flourish is because the countries has become more free then ever before so that people in those country can focus on other issue which since they are maipulative so that they don't have the correct mindset to understand that other people's argument on those topics which are extreme may be wrong, therefore more and more of those people become extremist.

# 2023-10-26 #

* In today's lecture we talked about anxiety, although i personally doesn't suffer from it, but i do agree that we should have breaks or meet someone to talk about it.
* Other than mental health, the improvement of our physical health is also discussed, and one thing is that weither we should have sickcare or healthcare, although i think that healthcare should be what is provided, but since we are all to "busy" to care for our healththe sickcare we currently have is the better option here.

# 2023-11-2 #

* Although I personally haven't experienced depression, but I do feel like there are more and more cases of depression and this is what we need to be considered about.
* How to spot and help a depressed friend: I think that depression is caused by prolonged stress or frustration, and it can be spotted by observing whether or not your friend have been acting unusual or not interested in everything, and the best we can do is to stay with him and take about what they have gone through.
* Hypothesis: talking to someone may help from depression

# 2023-11-9 #

* Today we talked about decisions, and there was a discussion after the first video was played, which the teacher have a point on regret on decision is because of the pain they felt on the decision, and the other one have a point on it is because the comparision we have and they felt that the other option is better than the one they've chose, and I do agree on the other one's point, because of comparison, we can have a unrealistic thought that "what if I chose the other option and it was better", and without comparision, there is only envious of what others have. This is what I think decision is, is to compare the options we have currently and the information we have on them, and regret is just that we missed an important information so that we feel that this decision is worst than the other one, or that we hope that the other option is better than this one to complain about.
* For the first video we watched, I do not think that it shows how people make all decisions, it only shows when people are making decisions that are not so important or decisions they haven't think through what will they react, when given a short period of time to decide on something, people tends to choose what their instincts told them to choose, and it is sometimes wrong because they often miss some information about it, and how the people reacted in the video they don't know what they are choosing, and without the comparision they don't know why their instincts told them to choose it, and that is why they confused it as what they have chosen, and this is different from when people are making important decisions, with irrational decisions not included, people can usually make the best decisions, and the rest is the informations they left out.

# 2023-11-16 #

* I forgot to scan the QR code today but i did attend to class, today is mainly on presentations of each large groups, there were a lot of intresting topics which one of them how sleep quality affects how you learn, and i think this is really important because in college we tend to just don't care about how many hours we slept, and it is something that we should really care about.

# 2023-11-23 #

* In the second video in today's lecture, it's talking about how millenials are addicted to phones because of how they have been taught, and i think that it is important to change and give awareness to them in order to make them free and stay connected, which is why I believe that their should be a law to add classes to the school system and how effective it is is of how the claases are designed which needs professionals, and parents should also be taught how to raise their kids since jow they grow up will change who they are, and it doesn't matter whether or not that sometimes it might not work or someone should be punish it they didn't follow the law since if they don't stay connected and free it doesn't really harm anyone except for themselves and we are just trying to make them not suffer when they grow up and guve advices and awareness on how to.

# 2023-11-30 #

* Today is group presentation again, many groups have interesting actions that can be carried out.

# 2023-12-07 #

* Perspective of different news is definitely an important topic, and it is also important for us readers to know which side is this news on and how much information in this news should we believe.
* In order for us to survive longer on this Earth, we should care more about it, and there are nessesary resources that we should let others know that are diminish and should be taken notice of.


# 2023-12-21 #

* successful, unproductive
* i did attend to class but basically just sat there and do nothing, also the club tonight was cancled so i just lay in my bed doing nothing, but since i did go to class which i'm feeling successful but unproductive.
* there's probably nothing i can do tomorrow except for the table tennis class since tommorow is kind of busy, but maybe i'll read books for my final exam.

# 2023-12-22 #

* successful, productive
* there's a table tennis class for me today which i feel like i learned very much, but i have to get back to taipei so i don't really have the time to do anything else but i still feel that today is pretty productive and successful.
* i should read something for my final so i brought my calculus text book back to make me more productive.

# 2023-12-23 #

* successful, unproductive
* i ate bbq today which is great, but for the rest of my time i stull didnt do anything.
* i'll probably finish my homework tommorow which feels productive to me.

# 2023-12-24 #

* successful, productive
* i'm almost done with my homework which is due tommorrow, so i thinm i'm quite productive.
* yeah sunday is almost over so i only got i week for the final, better start working.

# 2023-12-25 #

* successful, productive
* I've summitted my final project which I'm feeling productive.
* There's still so much more to read...

# 2023-12-26 #

* successful, productive
* There's not really anything special today just more reading to do.

# 2023-12-27 #

* successful, productive
* It was the same as yesterday.

# 2023-12-28 #

* successful, productive
* I did finish the chapter of what I planned to finish so I'm it's very productive for me.
* Although I finish the chapter i needed to finish, but there is so much more to study so I need to hurry.

# 2023-12-29 #

* successful, unproductive
* I planned what to do for the last week before the final put I didn't finish so I should be more productive.
* I should review one topic a day and time is running out.

# 2023-12-30 #

* successful, productive
* Although I only finished half of what I've planned but I finally manage to figure out some questions that have been bothering me.
* I better start catching up with the schedule.

# 2023-12-31 #

* successful, productive
* Today was quite productive, I did finished what I've planned to do, also tommorrow is a brand new year! But unfourtunatlly I have to stay at Tainan to study.

# 2024-01-01 #

* successful, productive
* Tomorrow is the first two finals which I'm pretty confident about but I'm still feeling nervous.

# 2024-01-02 #

* successful, productive
* I've finished two exams which all went pretty well, but tommorow is the ine that I'm feeling most uncertain, wish that everything went well.

# 2024-01-03 #

* successful, productive
* I'm not sure that wether or not I will pass, but tommorw is the last one, once I'm finished I can finally go home!
