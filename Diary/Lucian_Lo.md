# 2022-09-08

* Today is the first class of this course, looking foward to the rest of the course.
* Professor told is how to cite the reference in ppts.
* Instead of making a whole slide about references, just simply cite the source next to the image.
* Condom is actually an interesting topic to think about.
* Don't know why but one of my teammate is not responding to my messages about team presentation.
* Don't know why but I feel kinda shy to let everybody read my diary.

# 2022-09-15

* Shout out to my teammate Dylan, who taught me how to use bitbucket.
* Had automatic control courses today.
* I finished watching the movie called "let the bullets fly".
* Seems like one of my teammate vanished. I guess Dylan and I can only rely on ourselves.
* Professor introduced about Linud and GIT in today's course, which is both kinda new to me.
* There are 5 major innovation platform :blockchain, genome sequencing, energy storage, robotics, and artificial intelligence.

# 2022-09-22

* Too sad that only three teams can present their work per week.
* It's important that we all know how to distinguish fake news and stats.

* The follwing paragraph is the summary of what I think about my future will look like and why it will be so 10 years from now:
*	I think I will be a pilot in EVA AIR 10 years from now, most likely to live in Taipei. I think there are five current trends of change that will affect me the most. The first change is definitely climate change. Climate change have already made huge influence to our life and living environment. We can tell that by the frequency of natural disasters and the impact of a rain. Undoubtedly, natural disasters are happening quite frequently around the globe, and a single rain can cause cities to flood. The effect of climate change can only be worse 10 years from now.  The second change is auto-drive cars. Personally speaking, I think auto-drive technology will be the trend 10 years later, since it can greatly reduce the accident rate, provide more safety to people. However, there will be a lot of professional drivers losing their job, which will increase the rate of unemployment and cause a lot of trouble. The third change is the thriving VR technology and metaverse. It is the VR technology that make metaverse possible. Just like the movie “Ready Player One”, metaverse will truly change our life style. There is no need to go outside while everything can be done on metaverse. I can imagine that 10 years later, people start their day by putting on a VR glasses and dive right into the metaverse. The forth change is artificial intelligence. Artificial intelligence is already changing our current daily life in all kinds of aspects. For example, AI can help doctors to analyze and diagnose certain illness and cancer. In fact, AI can not only make progress in science or mathematical area, but also create their own piece of artwork, such as the AI called DALL-E, which is capable of creating an image based on a sentence you provide. It is predictable that AI can have better performance than human being in most field 10 years from now. The fifth change is 3D printing. As the 3D printing technology continues to develop, we can even print houses. Nowadays, scientists and engineers are putting their effort into printing human organs, which is likely to happen in the next 10 years. By the time human can print organs, we can all be immortals, since we can just print and replace all the organs in our body whenever they’re out of function. Moreover, people can even improve organs by increasing its durance and enhancing its functionality. In conclusion, there’re both advantages and drawbacks while our technology continues to progress. It is left to our current generation to deal with those problem and improve our living environment.

* The news I pick is about "Drinking two to three cups of coffee a day linked with longer lifespan", here are the three different perspectives from Evening Standards, Daily Mirror, and Sky News.
* Evening standards: Compared with no coffee drinking, this was associated with a 14 per cent, 27 per cent and 11 per cent lower likelihood of death for decaffeinated, ground and instant preparations, respectively.
* Daily Mirror: Brits who drink two to three cups of coffee each day could be lengthening their lifespan and the habit should be considered part of a healthy lifestyle, according to new research.
* Sky News: The study's findings applied to ground, instant and decaffeinated versions of the drink and researchers suggested coffee consumption should be considered part of a healthy lifestyle.

# 2022-09-29

* Some of the classmate's vision for the next 10 years are kinda different to me.
* Professor asked us to fill out a questionnaire about money and economy in the world, somehow I think it is quite hard for me to answer some of the question.
* The deposit part is kinda confusing to me.
* The video that professor during the lecture is also kinda hard to understand.
* Although ecnomy is hard to me, I'm actually kinda interested to this topic.
* In fact, economy is one of my possible choice of college majors before.
* Personally speaking, I think learning economy is learning how human will behave and react in certain situation instead of just learning numbers and statistics.
* The stats for the task today is so hard to find on internet, especially for the data long time ago.

# 2022-10-06

* One of the topics today is facism, which I've never heard before.
* we get to know a lot of different countries’ economy situation in today’s course.
* It is important that we know the axis of each graph and make sure that the unit is correct.
* Use exponentials to make the graph more readable, especially for minor changes between datas.
* Today is the first in-person class for this course  since the semester start. The learning experience is totally different compare to online lecture.
* The video that professor showed us today is quite inspiring. It tells us that no matter what you had been through before, it's always not too late to make a change. We were all childish before, we may say a lot of bad stuff to people, and we may have done something terrible and irreversible to others. However, no matter what we have done to others before, as long as we apologize to the victim, learn from our mistakes, and never do it again, it's not too late to become a good person. We must all learn how to forgive others, because we are not god and we all make mistakes from time to time. Moreover, we must learn how to forgive ourselves. Time is consistent, forgive youself in the past, and make yourself in the future a better person. 

# 2022-10-13

* Today's course is about anxiety and healhty life, which is an important task for every single individual.
* I got 2 exams next week, so it will be a tough weekend...
* Still processing how Plato defines knowledge, never heard of this definition before in my life.
* Hope the video about depression won't be too deep and depressing for me.
* Seems like we need to exercise more to stimulate our brain in order to study more efficiently/

# 2022-10-20
# * The lecture today will be moved to January 7th.

# 2022-10-27

* There lecture last week is moved to January 7.
* Which means that my winter vacation will be delayed for one day.
* Still two more mid-term exams to go for next week.
* It is important that we all be aware of our mental health.
* I'm depressed because of the exams, but I think I can still handle it.
* It's almost November, and the weather in Tainan is still a little bit too hot for me.
* I think by changing the way you think, it can help with depression.
* Thinking outside the box sometimes can let you escape from the depression loop and start thinking in a fresh way.

# 2022-11-03

* Today's course is about helping others with depression.
* It's actually kinda hard to deal with this type of issue.
* Sometimes you will also be affected by their negative thoughts or mindset.
* You have to be mentally strong enough in order to help others
* The tragedy happened in Korea during Halloween was horrifying.
* The air quality is horrible recently.
* The fact that I'm not familiar with most of my course group members makes the project a lot harder.
* Got no exams next week. HOORAY!

# 2022-11-10

* Only a few groups got the opportunity to do their presentation.
* Some of the topics are quite interesting to me during the class presentation.
* The air quality is still horrible for this week.
* It's indeed correct that we should contain more than one journal in the presentation to make it not that narrow.
* I'm super excited for Dwight Howard's performance in T1 league.

# 2022-11-17

* The world population have reached 8 billion recently, which is kinda horrifying. 
* I didn't know that noise can actually cause significant damage on health at first.
* But it actually make sence since I can't concentrate while there're a lot of noise around me.
* And I will be annoyed by those noise, which is definitly not a good thing to my mental health.
* Tons of exams are coming next week, tough weekend again.

# 2022-11-24

* The professor ask us to put our phone in front of the class today.
* It reminds me of my high school daily life when teachers usually asked us to put our cellphone in front of the classroom.
* However, this time I got my ipad with me, so basically there's no difference.
* Four exams in a week is a huge burden for me. 
* There was an earthquake yesterday, hope everybody is fine.
* Seems like there won't be a winter this year since it's still kinda hot recently.

# 2022-12-01

* One of my group members somehow cannot attend our discussion...., tough works for the rest of us again.
* Professor asks us to present our 3 action ideas.
* Although there's only three ideas for each group to present, the time isn't enough to let every group introduce their ideas.
* Automatic control project is kinda hard to me, hope I can finish it before deadline.
* Spend hours debugging the code and turn out that our MPU6050 is broken, what a waste of time.....
* Too sad that im not a soccor fan, or football, whatever... 

# 2022-12-08

* Today is the 3rd course project presentation.
* It's actually quite hard to complete the course project without all of the group members help.
* Our automatic control course TA helped us a lot with our project.
* There was a small earthquake happened few days ago, i bet most people didn't even notice.
* The weather is getting cold recently, i can finally enjoy my winter now.

# 2022-12-15(THURSDAY)

* Facts tought today: The third industrial revolution started in 1969 and the fourth industrial revolution started in 2011.
* successful and productive
* I prepared my TOEFL test.
* I prepared the paper airplane contest for tomorrow.
* Most of the test flies went out pretty good.

# 2022-12-16(FRIDAY)

* unsuccessful and productive
* The airplane contest results were terrible.
* Still preparing for my TOEFL test.
* I played basketball with my friends in the evening.
* I hope I can do well on the TOEFL test tomorrow.
 
# 2022-12-17(SATURDAY)

* unsuccessful and half-productive
* I woke up at 7:30 am to take my test. It's quite hard for me to wake up such early during cold weather.
* My score didn't meet my expectation.
* After having lunch, I was too exausted due to the test and fell asleep while studying elctronics.
* I went to the gym with my friends after the nap for two hours. 
* Gotta sleep earlier to be prepared for the studying tomorrow.

# 2022-12-18(SUNDAY)

* unsuccessful and productive
* I thought I could finish studying for the mechanics of composite material  for today.
* But I struggled on some of the parts and therefore couldn't finish it.
* Studied machine design for the rest of the night. 
* Spend more time on the parts that I'm not familiar with tomorrow. 

# 2022-12-19(MONDAY)

* successful and productive
* I finished studying mechanics of composite material today.
* Spend most of the night studying machine design.
* Since tomorrow's class starts at 8am, I slept early tonight.
* Got four classes tomorrow in the morning, better go to sleep early in order to be prepared for it.

# 2022-12-20
# * I took bereavement leave today.

# 2022-12-21
# * I took bereavement leave today.

# 2022-12-22

* successful and unproductive
* I felt successful because I nailed the matlab test this morning.
* I felt unproductive because the lack of efficiency while studying today.
* I will be playing basketball tomorrow evening and hope it will motivate my mind afterwards.

# 2022-12-23(FRIDAY)

* successful and productive
* I felt successful because I played pretty well during the basketball game with my friends in the evening.
* I felt productive because I finished my automatic control project this afternoon.
* I will be fresh mentally after the exercise this evening and be prepared for studying.

# 2022-12-24(SATURDAY)

* successful and productive
* I felt successful because I understood most of the topic of machine design.
* I felt productive because I spent the whole day studying.
* I hope I can keep up the good work.

# 2022-12-25(SUNDAY)

* successful and half-productive
* I felt successful because I have done preparing for the two exams tomorrow.
* I felt half-productive because I was kinda exhausted after back to back hardcore studying.
* I will sleep early tonight for the two exams tomorrow.

# 2022-12-26(MONDAY)

* successful and unproductive
* I felt successful because I finished two final exams today, and I think I did fairly on both of it.
* I felt unproductive because I spent the whole afternoon sleeping and hanging out with my friends.
* I will study more tomorrow in order to catch up the progress.

# 2022-12-27(TUESDAY)

* unsuccessful and productive
* I felt unsuccessful because fluid machenics is hard to me.
* I felt productive because I spent the whole afternoon studying fluid machenics.
* I can ask my classmates for help on the parts that I'm not familiar with.

# 2022-12-28(WEDNESDAY)

* unsuccessful and productive
* I felt unsuccessful because I can't fully understand the topic of fluid mechancis.
* I felt productive because I spent the whole afternoon studying it.
* Seems like I still need to work on it for few more days.

