# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-17 #

* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation 

# 2024-09-26 #
* three ways to evaluate whether a data chart is accurate:

* 1. Check for Proper Labeling and Scale
Examine the labels on the axes, the units of measurement, and the scale used. Inaccurate or misleading labels, such as inconsistent intervals or unclear units, can distort the data's representation. The scale should be proportional and consistent throughout the chart to ensure that comparisons are fair and valid.

* 2. Assess Data Source and Integrity
Look into the source of the data used in the chart. Verify whether the data comes from a reputable, reliable source. Additionally, ensure that the data used represents the subject fairly—watch for selective data presentation, cherry-picking, or omitting relevant data points that might skew the interpretation.

* 3. Examine Visual Representation and Design Choices
Consider the design choices made, such as the type of chart used (bar, line, pie chart, etc.) and whether it is appropriate for the data. Also, check for visual manipulation tactics, like truncated axes or exaggerated proportions, that can mislead viewers by overemphasizing certain trends or minimizing others.

* Here are five current trends that could significantly affect me in the next few years, shaping various aspects of your life, career, and well-being:

* 1 Artificial Intelligence and Automation
* AI and automation are revolutionizing nearly every industry. From self-driving cars to AI-powered customer service bots,the automation of tasks once performed by humans is increasing. Depending on your field, this could mean a shift in job opportunities and skill requirements. If you're in a profession that's highly repetitive or routine-based, 
it's possible that aspects of your job could be automated. On the flip side, AI is also creating new roles in machine learning, data analysis, and human-AI collaboration. Staying up-to-date with technological advances and developing skills in areas that complement AI, like creativity, critical thinking, and emotional intelligence, could give you a competitive edge.

* 2 Remote Work and the Gig Economy
* The rise of remote work, accelerated by the COVID-19 pandemic, is here to stay for many industries. Companies are increasingly adopting hybrid or fully remote work models, which means you might have more flexibility in where and how you work in the future. This shift could allow you to live in more affordable areas or travel while working. Additionally, the gig economy is expanding, 
with more people opting for freelance, contract, or part-time work. Platforms like Upwork, Fiverr, and Uber make it easier to find work outside of traditional employment. This trend may give you more control over your work-life balance but also requires you to develop self-discipline, time management, and financial planning skills.

* 3. Climate Change and Sustainability
* Climate change is an issue that’s increasingly shaping the policies of governments, businesses, and individuals. Whether through stricter environmental regulations, a rise in green jobs, or shifts in consumer preferences towards sustainable products, the pressure to adapt to a more eco-friendly way of living and working is growing. This trend might impact the choices you make about transportation,
energy consumption, or even the career path you pursue. Sustainability-focused industries like renewable energy, electric vehicles, and environmental consulting are expected to grow, offering new opportunities. However, climate change may also lead to challenges, such as increased living costs in certain regions due to natural disasters or resource scarcity.

* 4. Health and Wellness Prioritization
In recent years, there’s been a surge in interest in mental and physical well-being, from meditation apps to wearable health tech. This trend isn’t just about physical fitness but also about maintaining mental health in increasingly stressful times. Companies are focusing more on employee wellness programs, mental health support, and work-life balance initiatives.
You might find yourself placing more importance on holistic well-being, making lifestyle changes like incorporating regular exercise, mindfulness, and nutrition into your daily routine. Advances in personalized healthcare, such as DNA-based diets or customized fitness plans, may also become more accessible, shaping how you take care of your health.

* 5. Digital Transformation and Cybersecurity
The world is becoming more digitally interconnected, with everything from finance to healthcare being digitized. This trend means you will likely rely more heavily on digital tools and platforms, whether for work, personal life, or education. With this increased connectivity comes a heightened risk of cyberattacks and data breaches. Protecting your online presence, understanding digital privacy, and staying aware of cybersecurity risks will become essential. 
You may also find that tech skills—such as digital literacy, coding, or cybersecurity knowledge—become necessary in your career or daily life.

# 2024-10-03 #
*Today's class is canceled because of typhoon
*I may study at home.
# 2024-10-17 #
*Today's lesson is about financial.
*We talk about what is money and understand the operation of bank.
*We also discuss about where bitcon can replace TWD, and my answer is maybe in the future it will happen.
*This class help me manage my money someday in the future.
# 2024-10-24 #
*At the begining of the class, we listened to student present on the economic development of their respective countries (like Taiwan and Indonesia).
*We watch videos talking abou why does extremism flourish.
*I learned someting from the video:
*1.People believe in stories. We are easily influenced by money, religion, and politics, and thus make wrong judgments.
*2.People will be afraid of someting we don't familiar with, so we need to talk to each other.
*3.We must learn to look at things rationally and not be influenced by extreme remarks。
*Also, I leared about the difference of knowledge and information: Knowledge is someing that we believe it is true.
*How to live healthy
*1.	Maintain a Balanced Diet
*2. Stay Physically Active
*3. Prioritize Sleep
*4. Manage Stress
# 2024-11-07 #
*In today's class we watched a lecture on depression.
*We create a huge group
*I think the most important thing to help depressed patients is to make them feel needed and distract them.
*I will try my best to help depressed people.
*I should cherish life more.
# 2024-11-14 #
*At the beginning of the class, we listen to the presentation and learn about how to help depression friends.
*It is also important to set boundaries, practice self-care while helping others.
*Cute animals may be a good help, which can make people feel good and comfy.
*Also, I learn about renewable energy during the debate between my classmates about nuclear energy and windmill.
*We watch the talk about do you truly understand the reason behind your actions.
*We rationalize our decisions after the fact instead of truly understanding them.
# 2024-11-21 #
*The class talk 
*about rules and freedom:
*If you are alone in the world, then no rules are needed. If it is very crowded, then the survival requires rules.
*Rules are needed to define where one persons freedom ends and another persons start.
*It is hard to reach a consensus.
# 2024-11-28 #
*In today's lesson, we listened to presentations on final projects.
*I am particularly interested in the plastic bag reuse project, which aims to help reduce pollution.
*However, I don't think the project is implementable, as it can be troublesome for people to donate used plastic bags. As a user of plastic bags, I also worry about what the previous user might have put in the bag, perhaps their dinner or something smelly.
*Additionally, the professor collected our cell phones during this class, and I miss my phone very much. Without it, I cannot take notes.
# 2024-12-05 #
*In today's class, we listened to group reports on news from various countries.
*Through these reports, I learned that even when the same issue affects multiple countries, there can be different or even opposing viewpoints.
*To navigate this complexity, we should read more, stay informed about international events, learn to think critically, analyze information, and develop our own opinions.

# 2024-12-12 #
*In today's lesson, we discussed private ways to reduce carbon dioxide emissions.
*To my surprise, one of the most effective strategies is to have one fewer child.
*From our final presentation, I learned that using a Work Breakdown Structure (WBS) is an effective approach to achieving our goals.
*However, I previously misunderstood that the WBS should include results, not actions.
*Additionally, we had a discussion about our final project.
*During this discussion, we identified many details that we had previously overlooked.
# How I feel everyday #
2024/12/19(Thursday)
•	unsuccessful and productive
•	I feel dizzy in morning classes, so my learning efficiency is pretty low. But I stayed up late studying and reviewing a lot of course content.
•	Sleepe earlier or drink some coffee.
2024/12/20(Friday)
•	successful and unproductive
•	I have been tired all week, so I take a rest today, also I did well in today's calculus exam.
•	Get enough sleep and make plan.
2024/12/21(Saturday)
•	successful and productive
•	I sleep for the whole day and write code in the evening. A good rest is needed.
•	Get up early.
2024/12/22(Sunday)
•	successufl and productive
•	I prepared for the English final prasentation and studied Physics. DDL is the best motivation for me to work.
•	Go to library.
2024/12/23(Monday)
•	unsuccessful and prouctive
•	I wrote the Chinese final essay and studied caculus. It is five a.m. now, there is less than five hours to sleep.
•	Don't eat too full before studying.
2024/12/24(Tuesday)
•	unsuccessful and productive
•	I pass the physics midterm exam and studied calculus. There is still many things to study, it is four a.m. now, I am dying.
•	Take a nap when I am tired.
2024/12/25(Wednesday)
•	unsuccessful and productive
•	I went to the library and studied in calculus for 6 hours, also I prepare for the presentation tomorrow. There is still much things to do.
•	Go to sleep earlier and be relax.
# Five rules make me more successful and productive #
*1. Plan Your Time Effectively
*2. Focus on One Task at a Time
*3. Commit to Continuous Learning
*4. Maintain Healthy Habits
*5. Reflect and Improve