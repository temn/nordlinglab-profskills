This diary file is written by Jim Wu H44136027 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-19 #

* First lescture of this class
* It's the first time i know about git repository 
* I think it's quite complicated to use Bitbucket 
* The class today taught us about renewable energy
* The increase of the technology made the price decline over the years
* Some tips are given to improve our presentation 
* The homework is to find the evidence about the cliam we choose in "Is our world getting better or worse?"
* First time writing a diary (still need some time)

# 2024-09-26 #

# What did I learned today #

* Three things that we should remind us to avoid bad statistics:  (1)Seeing the uncertaincy  (2)Can I see myself in the data?  (3)How was the data collected?
* The data results might be influenced by many aspects such as its showing form, unit of stastistics and how many parameters were involved in. Also, many data which show the average numbers to mislead people belong to bad statistics. 
* We can use data to break stereotypes of people and observe some change in the world.

# Task 1 #

I'm a freshman of business management. 
After graduate from university,
I think I'll stay in Tainan and start my own life here instead my hometown New Taipei city.
As for the job,
I'm not have a certain idea yet.
I'm think about working in big company or working in the bank.
But I'm sure there're 5 current trends of change that might affect me in the future.

# 1.The Rise of AI #

AI is having huge increase these years,
they might be able to do accounting and manage in the future.
People like accountant and manager might be replace by AI.
The structure of the labor market will be change.
AI it's something we should care about in the future. 

# 2.Global Inflation #

Global inflation change the value of the currency around the world.
We can buy less thing than what we could buy in the past with same amount of money.
People's purchase intention might get lower in the future.
Economic structure will be change as will.
As a business management student,
This is very important for me to know more about this.

# 3.Global Cooperation #

Global cooperation is very important during this time.
Industrial products will be separate to many parts and given to many country.
Some will engage in production while others do brand management.
And expanding oversea market will be another target for big company besides local market.
Taiwan is a small country,
so oversea market is something very important.
If I want to get a better job after I'm entering the workplace.
Not only did I have to have management skill but also be good at different languages.
This skills really matters with everyone.

# 4.Growing Tense International  #

This is not a very good time around the world.
The war between Ukraine and Russia is still on going.
America and China's relationship is getting worse.
And China might want to invade Taiwan in future years.
The bad relationship between the nations is really affecting everyone's life.
especially the new generation young man like me.
The market is affected as well.
And these problem won't be solve easily in the near future.
After graduating from university, It's important for me to focus on international situation.

# 5.Global Warming Issue #

Global warming is getting serious in recent years.
The Earth is getting hotter and natural disaster like typhoon is much more stronger than it used to be.
Some technology is evaluate to be more effeciency and more environment-friendly.
This is a potential market which will grow faster in the future.
As a business management student,
This market it's something I can engage in for my future career.

# Task 3 #

* Left (Inquirer): A SpaceX craft has launched to rescue two stranded astronauts. The astronauts stuck in the international space station(ISS) due to Boeing's Starliner has some problem. Nasa and spaceX collaberate a plan with each other to save them.
* Center (BBC News): A SpaceX craft has launched for crew-9 mission and rescue two astronauts. Boeing's Starliner has some problem, so NASA decided not to bring them home on the Starliner. They will stay in ISS and join Crew-9 later.
* Right (India TV News): SpaceX launched a rescue mission for the two stuck astronauts, they will return in late February. They are stucked since Boeing's Starliner has some problem.

* Alought They're different media, they still has almost same report. I think SpaceX craft lauching is the event which didn't related to politics or other sensitive issue. It's more about technology and space explore. But it might still be different in other media besides these three.

# 2024-10-03 #

* The typhoon is coming in, so we didn't have a class today.
* Today's homework is about financial problem and the value of money.
* We switch to new group, but we didn't met yet.

# 2024-10-17 #

* Today learned about lots of financial stuff, including the value of the money and the system of economy works.
* Today's homework is making figures about financial situation in our country.
* The groupmate this week need to use English to communicate, and I think it's quiet a challange to me.
* The groupmate choosed Indonesia to make figures instead of Taiwan, this really surpises me.

# 2024-10-24 #

* Three ways to live healthy

* 1.Regular Health Check-ups and Monitoring
The video highlights the use of technologies like DNA testing and miniaturized sensors that can help detect disease risks early, promoting the implementation of preventive measures and reducing long-term illness occurrences.

* 2.Maintain Regular Exercise
Research has shown that regular exercise helps improve cardiovascular health, reduce stress, strengthen the immune system, and protect brain health. These benefits contribute to the prevention of chronic diseases, as mentioned by experts in the video, supporting a healthier lifestyle in the long term.

* 3.Balanced Diet and Proper Nutrition
A healthy diet is an essential part of disease prevention. Consuming sufficient vegetables, fruits, whole grains, and lean meats can reduce the risk of chronic illnesses. The video emphasizes that a prevention-focused healthcare model encourages healthy lifestyle choices, thereby decreasing the occurrence of long-term diseases.

# 2024-10-31 #

* We got typhoon……again.
* We're going to make our final report with our groupmates of 5 to 6
* I got lots of exam next week, and I gonna study hard.

# 2024-11-07 #

* Last week was the third typhoon day-off this semester. This is the first time I feel too many day-off.
* Today's course is about depression and the cases of some people who tried to commit suicide. The video about the police officer who deals with the cuicided cases on the Golden Gate Bridge let me think a lot.
* If I have a friend who has depression or trys to suicide, I think the first thing is to accompany him or her and to have a talk.
* The second thing is to help him or her seek a professional therapist who is much more exprienced.
* The last thing I think the most important thing is to invite him or her to join my life and show hoe beautiful the world is.

# 2024-11-21 #

* In today's lecture, we watched a very interesting video about algorithms and how they are used
* The presenter pointed out, how big data works and what it is applied in (e.g., insurance systems, credibility for loans, ...)
* Throughout the presentation, a major criticism was towards the fact that we only base these algorithms on the past, which makes it difficult to get rid of old, differently influenced "trends"
* A given example was a company, where women were much less promoted than men, which didn't change after the CEO retired, because the algorithm used the sex as a factor to distinguish the success potential of people
* While she mentioned a very crucial and dangerous point here, I am still convinced that the data we are gathering is a precious good, making many things more fair overall
* But of course, the information contained in the data must be used thoroughly and constantly revised from a purely human position, to validate the correct "behaviour" of used algorithms
* Ultimately, the amount of data is cnstantly growing. This gives enterprises the possibility, to use only the more recent data, to give a better representation of how things actually are in the past ~1-5 years


# Rules on how to reach consensus #

* Everybody's opinion counts --> Listen to it carefully
* Be condifent in your own point of view, but value everybody else's
* Stay objective and don't go / speak against a certain group of people
* Respect other people's boundaries. Don't get personal

# 2024-11-28 #

* We've watched the video about currency debasement might infect the world greatest country's rise and fall.
* We also talked about news, which can be said in every way and made us away from the truth.
* Following the original reporter of the incident can make us get closer to the truth. Following the middle person who uses the original reporters’ images may give us the untrue story.
* News with anonymous sources should not be believed because that anonymous source maker could be planning to manipulate people’s minds or just want to make a profit using false information.
* When everyone is a reporter, no one is.
* So when we watch the news, we should make sure the resourses it was from and the one who talked about it are close to the truth, not just believe in every thing we heard and thought that was what really happen. 

# 2024-12-05 #
* Green water is the water that exists in plants.
* 18% of GDP lost because climate change
* Just dropping fossil fuel is not enough, we have to recover nature.
* If Russia gets Ukraine, they get more natural resources.
* North Africa is not always desert. I is possible to plant trees and turn part of it green.
* Show the progress of planting trees to show people hope and how to move on, instead of showing unimportant news.
* One of the reasons why people don't care about climate change is that some people think:  “I focus on what is in front of me, and hope others take care of the big picture.”

# 2024-12-12 #
* 40% of people earn less than 2 USD per day.
* Climate change changes the water cycle .
* The 6th extinction on earth may be coming.
* Ice melting changes the sea current.

# 2024-12-19 #
* Building nuclear power plants and using nuclear power is more expensive than before. 
* Using solar and wind power is cheaper than before.
* We need friends that we can talk to in order to discover ourselves.
* A. Successful and unproductive
* B. I felt successful because I didn't fail to do too many things today. I felt unproductive because I was feeling tired after class and only studied for a few times.
* C. I will use nasal spray during daytime to breath better thus avoid tiredness.

# 2024-12-20 #
* A. Successful and unproductive
* B. I felt successful because I did a good job in my club'sChristmas party performance. I felt unproductive because I didn't study much.
* C. I will make the schedule for upcoming work.

# 2024-12-21 #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I didn't finish my project. I felt unproductive because I didn't participate in my group's meeting much.
* C. I will try to do my work more efficiently.

# 2024-12-22 #
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I didn't study much about my calculus. I felt unproductive because I slept too much and didnothing.
* C. I will sleep earlier so that I can have better sleep quality instead of having a long bad quality sleep.

# 2024-12-23 #

* A. Successful and productive
* B. I felt successful because I done with my project before wednesday. I felt productive because I didn't waste my time on something else.
* C. I will be more focus on my work.

# 2024-12-24 #

* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I was late and couldn't understand my economics in class. I felt unproductive because I slept too much.
* C. I should wake up more early.

# 2024-12-25 #
* A. Successful and unproductive
* B. I felt successful because I did a good job on my final exam of movie class. I felt unproductive because I skipped my class to study calculus and still couldn't understand.
* C. I should find the way to study my calculus more efficiently.

# 2024-12-26 #

* A.
* B.
* C.

# 2024-12-27 #

* A. Successful and unproductive
* B. I felt successful because I did a little bit better in my caculus test, I felt unproductive because I didn't study much after class.
* C. I should study more for my exam in next two week.

# 2024-12-28 #

* A. Successful and productive
* B. I felt successful because I had a good time in my club's field trip, I felt productive because I learned a lot about railway's knowledge in my field trip.
* C. I will have to learned more in something I'm interested in.

# 2024-12-29 #

* A. Unccessful and unproductive
* B. I felt unsuccessful because I can't understand my economic for the test next week, I felt unproductive because I didn't do much thing today.
* C. I should plan for my days more.

# 2024-12-30 #

* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I failed my management online final exam even I tried hard, I felt unproductive because I didn't finished my accounting homework untiled last minute.
* C. I need to take care of my study more than other things.

# 2024-12-31 #

* A. Successful and unproductive
* B. I felt successful because I did better in my economic test than last time, I felt unproductive because I didn't have friends to spend for new years.
* C. I will looking for more new friends in the future.

# 2025-01-01 #