# 2024-09-12 #

1.the teacher introduce himself

2.get to know each other

3.have a discussion about will the exponential growth of food bring abundance to the  world


# 2024-09-19 #

1.discuss the issue of conflict

2.the teacher told us how to improve the presentation          

3.watch a TED talk


# 2024-09-26 #

1.3 ways to spot a bad statistic

Do you see uncertainty in the data

Do you see yourself in the data

How was the data collected


2.In 2034, I envision myself living in a vibrant, eco-friendly city where sustainability is integrated into daily life. My home will feature smart technology designed to enhance energy efficiency, and I'll be surrounded by green spaces that promote community interaction.

Professionally, I anticipate working in the renewable energy sector as a project manager or consultant. My role will involve leading initiatives that focus on sustainable practices, collaborating with diverse teams to tackle urgent environmental challenges. I see technology playing a crucial role in my work, particularly through advancements in artificial intelligence and automation, which will streamline processes and foster innovation.

Several trends will shape my life: the focus on sustainability will drive corporate accountability; the rise of remote work will offer flexibility and work-life balance; health and wellness will become integral to workplace culture; and global connectivity will enable collaboration across borders.

Ultimately, I believe these changes will provide opportunities for personal and professional growth, allowing me to contribute positively to society while thriving in an interconnected world. My future holds the promise of meaningful work and a fulfilling life committed to sustainability and community well-being.



# 2024-10-17 #
1.Learning about the value of the money and what money is.

2.There a lot of things I can learn about investment.

3.I always have an interest in Bitcoin, I am glad professor discussed the issue with us.


# 2024-10-24 #

1.(a)Grades: The belief that grades reflect my intelligence and potential affected me last week when I stayed up late studying for an exam. Even though I know grades are just letters, I felt they define my academic success and future opportunities, so I prioritized studying over sleep.

(b)Career: The story of a career path guided my decision to stay at home studying instead of going out with friends. I believe a clear career path will lead to success after graduation, so I focused on building connections, even if it meant missing out on social time.

(c)Independence: The story of independence defines my college experience, pushing me to manage on my own. Last week, I was sick, but instead of calling my parents for help, I managed alone, buying medicine and cooking for myself. I believed that being self-reliant shows maturity and capability.

2.A future envisioned by Taiwanese politicians, like Tsai Ing-wen, emphasizes digital innovation, environmental sustainability, and democratic resilience. Tsai’s administration has focused on positioning Taiwan as a tech hub, investing in green energy, semiconductor leadership, and internet infrastructure to drive economic growth and global relevance. The future she envisions is also about social inclusivity, with support for progressive policies like marriage equality, aiming to make Taiwan a model of freedom and equality in Asia. This path emphasizes clean energy independence, economic strength in tech and AI, and strong ties with democratic allies to protect Taiwan’s autonomy and prosperity.


# 2024-11-07 #


I really like the lesson today because the teacher let us create a supergroup to develop a new topic to report. This not only gave me a sense of excitement to perform a report in a limited time, but also provided me with a chance to make more friends due to the rare opportunity to talk to unfamiliar classmates in class. It is often hard to make new friends, especially when everyone tends to stick to their usual social circles. However, today’s activity encouraged us to step out of our comfort zones and communicate with others who we don’t usually interact with.  

Additionally, the process of brainstorming with the group was both fun and challenging. We had to quickly come up with a creative idea, divide tasks, and ensure everyone’s participation. This kind of teamwork required good communication and collaboration, which I found very meaningful. It taught me how to express my ideas more clearly and how to listen to others’ opinions.  

Furthermore, presenting our report to the class was a rewarding experience. Even though it was slightly nerve-wracking to speak in front of everyone, it helped me build my confidence. The feedback we received from the teacher and classmates was also very helpful, as it gave us valuable insights into how we can improve in the future.  

Overall, today’s lesson was a memorable and enriching experience. It reminded me of the importance of teamwork, effective communication, and stepping out of my comfort zone. I hope we can have more activities like this in the future because they not only help us learn but also make the class environment more engaging and dynamic.


# 2024-11-14 

After watching TED talk choice blindness experiment video, I was truly amazed by how unaware people can be about their own decisions. The way participants confidently justified their choices, even after the photos were secretly swapped, left me stunned. When I first realized the switch had happened, I was shocked—it’s incredible how easily our perception can be manipulated. This experiment highlights how fragile our self-awareness can be, making me question how often I might unknowingly rationalize choices that aren’t truly my own. It’s a fascinating yet unsettling reminder of the limits of our consciousness.


# 2024-11-21#

In today's lecture, we explored the role of algorithms and big data in systems like insurance and loan credibility assessments. A key concern was highlighted: algorithms often rely on historical data, perpetuating past biases. For example, a company’s algorithm continued to disadvantage women in promotions even after leadership changes, as it factored gender into success predictions. While such biases pose risks, the presenter emphasized that data remains a valuable resource for fostering fairness if used responsibly. Ensuring algorithms behave ethically requires regular human oversight and updates. Moreover, leveraging recent data (from the past 1-5 years) can better reflect current trends, reducing outdated biases.


# 2024-11-28 #


The Importance of Consensus
Consensus is crucial in decision-making, especially when making laws, as it ensures that all parties involved feel heard and respected. It fosters unity, minimizes conflict, and promotes social harmony. In the context of lawmaking, reaching consensus helps to create laws that are widely accepted and supported, which is essential for their successful implementation and enforcement. Laws developed through consensus are less likely to face widespread opposition and more likely to be viewed as legitimate by the public.


Rules on How to Reach Consensus

1.	Open Communication: All stakeholders must have the opportunity to voice their concerns and suggestions. Open dialogue ensures transparency and prevents misunderstandings.

2.	Active Listening: Participants should listen attentively to each other’s viewpoints and concerns without interrupting. This builds trust and understanding.

3.	Mutual Respect: Respecting diverse opinions and finding common ground is essential to achieving a fair consensus.

4.	Compromise: Parties should be willing to make concessions where necessary to reach a mutually acceptable solution.

5.	Focus on Common Interests: Emphasize shared goals or values, such as justice and fairness, rather than individual agendas.


Process to Reach Consensus

1.	Problem Definition: Clearly identify the issue at hand and establish common objectives.

2.	Information Gathering: Collect relevant data, expert opinions, and public input to inform the discussion.

3.	Deliberation: Allow all stakeholders to discuss their views and propose solutions.

4.	Negotiation: Parties should negotiate to resolve differences and propose mutually agreeable solutions.

5.	Final Agreement: Once a general agreement is reached, the solution should be documented and formalized into law.



# 2024-12-05 #

**Diary Entry: Navigating Planetary Boundaries with WBS and PERT**

Today was a sobering reminder of the precarious state of our planet. Six of the nine **Planetary Boundaries**—novel entities, climate change, biosphere integrity, land-system change, freshwater use, and biogeochemical flows—have already been crossed. This alarming reality occupied much of my thoughts as I worked on a sustainability project for my environmental science course. 

We’re using **Work Breakdown Structure (WBS)** to organize our project into manageable sections. First, we divided the task into major deliverables: researching boundaries, assessing local impacts, proposing solutions, and designing an outreach plan. For instance, under "biosphere integrity," we identified subtasks like analyzing biodiversity loss data, exploring reforestation initiatives, and evaluating species conservation strategies. Seeing these overwhelming global challenges broken into smaller tasks gave me a sense of focus and direction.

To ensure efficient progress, we applied the **Program Evaluation Review Technique (PERT)**. By estimating optimistic, most likely, and pessimistic timeframes for each subtask, we could better plan our schedule. For example, researching the impact of biogeochemical flows (like nitrogen and phosphorus cycles) on agriculture was estimated to take 3–5 days. Using PERT, we calculated the expected time and adjusted our timeline to account for potential delays. This method helped us stay realistic and prepared for setbacks.

One challenge we’re grappling with is **novel entities**—synthetic chemicals and materials humans have created that disrupt ecosystems. Microplastics, for example, were a focus today. I couldn’t help but think of the toothpaste and cosmetics I use daily, which might contribute to this problem. This personal connection brought home how deeply entangled we all are in these boundary breaches.

I reflected on **land-system change**, especially urbanization and deforestation, as I walked past a nearby construction site. The transformation of forests into concrete is no longer a distant issue—it’s happening in my community. Using limited **freshwater resources** wisely also feels urgent, as I noticed my own water consumption while doing laundry.

Climate change looms over all these boundaries, amplifying the risks. The **biosphere’s fragility** feels overwhelming, but organizing our project gave me hope. With structured approaches like WBS and PERT, we’re not powerless. Solutions might start small, but they’re critical steps in restoring balance within these boundaries.

Tonight, I hope our generation can rewrite the story—one project at a time.



# 2024-12-12 #

This week, I reflected on the interconnected challenges of **income inequality**, **climate change**, and **planetary boundaries**. Climate change disrupts the **water cycle**, causing extreme weather, while **ice melting** alters sea currents, destabilizing ecosystems. The threat of a **6th extinction** highlights humanity's impact on biodiversity. These issues remind us that crossing planetary boundaries has dire consequences, often disproportionately affecting vulnerable communities. Addressing these challenges requires global cooperation, sustainable solutions, and a commitment to equity. Protecting our planet is no longer optional—it’s a necessity for future generations.


# 2024-12-19  #
*  unsuccessful and productive

* In the morning, I was productive, completing a research paper ahead of schedule.

* However, the afternoon was less successful. I spent hours trying to organize my notes for another project but ended up distracted and unfocused. 

* improving tips: learn ways to organize my notes


# 2024-12-20  #
* successful and unproductive

* I went to outlet with my friens, we took lots of photos and had a happy night, but we did not study.

* improving tips:try to balance time.


# 2024-12-21  #
* successful and unproductive

* I spend so much time on tutor , teaching my students how to slove math questions,so i did not have enough time to study.

* improving tips:try to balance time.


# 2024-12-22  #
* successful and unproductive

* I  went to X'mas market with mt friend, we bought things and took lots of photos.We did not study but really happy.

* improving tips:try to study more.


# 2024-12-23  #
* successful and unproductive

* I went home and cooked for my family, but I spend too much time on it , causing lack of studying.

* improving tips: learning to use small time to study.

# 2024-12-24  #
* unsuccessful and unproductive

* My friend had a bad mood, evevn if we went to KNOWLEDGE to study atuomatic control, I spend so much time comforting him, so I did not study too much.

* improving tips : cheer him up.

# 2024-12-25  #
* successful and productive

* It is X'mas, my friend and I had a big meal and we studied fluid mechanisms together.

# 2024-12-26  #
* unsuccessful and productive

* I only got one vote of the presentation of today which made we feel unsuccessful.

* I did all the homework on willeyplus , it was hard , so finishing it let me get a sense of achievement.

* improving tips: wear a nice suit next time.

# 2024-12-27  #
* unsuccessful and productive

* In the quiz of fluid mechanics, i did not get full points while my friends all got 10 out of 10.

* I watched the first epiosde of squid game, it was so exciting.

* I also prepared the final exam of machine design, and took los of notes.

* improving tips: study harder

# 2024-12-28  #
* unsuccessful and unproductive

* I am disapoointed today because my student still did not understand the content I taught her.

* I did not study much.

* improving tips: finding another way to teach my student.

# 2024-12-29  #
* successful and productive

* I went to library at 10 am , so I study a lot. 

* I made lots of notes about electronics.

# 2024-12-30  #

* successful and productive

* We did the experiment of automatic control, it was hard but we got a improvement.

* I did well on the test today.


# 2024-12-31  #

* successful and productive

* It was the last day of 2024, I went to Knowledge after school and studied till 11pm.

* I hang out with my friends to celebrate New Year's Eve at 11:30, we decided to go to the city hall and enjoyed the fireworks.


# 2025 1-1  #

* successful and productive 

* I really do not want to study in the first day of this year , but I have to due to the tough exam next week. I studied with my friends the whole day. The only fun part of today was going to Focus to have 二鍋 because it is the first meal of 2025.

# 2025 1-2  

**Diary Entry: My Obituary**  
 
Today, I imagined my future legacy and wrote an obituary that reflects the impact I hope to achieve:  
*"Hsiao Chung-Yen, a dedicated mechanical engineer, used skills to innovate sustainable technologies that improved daily life and reduced environmental harm. Known for the practical solutions and teamwork, contributed to creating more efficient and eco-friendly machines. "*  

Two figures who inspire me are **Isambard Kingdom Brunel** and **James Dyson**.  

- **Brunel** revolutionized engineering with his bold designs, such as bridges and ships, by pushing boundaries and staying persistent despite challenges.  
- **Dyson** combined creativity and engineering to solve practical problems, creating innovations like bagless vacuum cleaners. His perseverance and focus on improving existing designs are qualities I admire.  

As a student, I relate to their curiosity and problem-solving mindset. From them, I learn the importance of persistence, creativity, and teamwork to leave a meaningful mark, even starting from ordinary beginnings.