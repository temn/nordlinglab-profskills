This is Hugo Diary.

# 2023-09-24 #
* HOMEWORK, MY FUTURE's VISION
* I believe that by year 2035 we will be a multiplanetary species, so many things will be different, a large number of changes and unexpected events will take place.
* I think it is quite hard to completely describe how our lives will be in the futuree.
* However, there are some things are more likely to happen. The technologies we have right now and the ones that will be developed later, will allow us to have life in Mars, a number of enough people to even have colonies there, we might have a moon base and other unbelievable things.

* I believe scientists will develop an amazing genetic engineering, humans's lifespans will increase amazingly, and people will have the ability to adapt to new and rough environments very quickly.
* Although great things are more likely to happen for humanity, not everything will be colorful, I think Earth's population will decrease and life in Mars and hopefully in Earth will be hanging on by a thread.
* I am more likely to live in Earth by 2030, I do not want to stay in one coiuntry, i would like to have houses and apartments in Taiwan, Canada, Argentina and my country El Salvador.
* If I work hard enough to achieve my goals, I will be working on trying to make life possible in other planets and, if there is life in Mars already, I will try to improve lifestyle conditions.
* During the next 10 years, I will be leading a rocket's company that will also create satellites and control the first Latin America space station. I will also have another company that develops green energy so we can sostein life in Earth for a longer time.

* The trend I am more afraif of is AI, I believe this can bring good things to humanity, but I am afraid those who are working on it, lose the capability to control these or that a crazy guy uses it to create something that can put humans lives in danger. 
* Connection between our brains and computer scares me too, since this technology can cause chaos in the wrong hands. War is another trend that scares me and others as well, how come we have the option to destroy ourselves, we are the same species, we should all look for the great of humanity and not focus on interests of a group of people.
* Drones designed to kill is another trend that out is putting our lives in danger, what if AI does not run the way we want it to work, it becomes smarter than us like in the movie M3gan.
* Deep fake technology brings me a lot of concern, it is so good that soon we will have security problems when trying to identify ourselves when going to the bank, buying or selling things etc.

# 2023-10-04 #
* Unfortunately we did not have class this day due to the typhoon, we have missed 2 lectures in total, the other time was due to Holiday.
* Still, I watched some videos suggested by the professor and I watched the video about Elon musk again since he is one of the people I follow the most and I thought that video was really interesting, It raises awarness of how the future will be if we do not take action to solve those problems that put humanity existence at risk.
* I wish we did not miss this class because I really wanted to present this time
* I hope the following weeks topics are interesting as the one we discussed last week


# 2023-10-04 #
* I have been working on the ppt for this week about "Fake News" but unfortunately we will not have class due to the coming Typhoon.
* Since I will not be able to share what I prepared for this presentation with the class, I want to briefly talk about the content of it.
* I want to mention 2 of most interesting fake images we found. The first one shows a Chinese warship very close to a Taiwanese frigate, supposedly checking on Heping Peace Power Plant in Hualien. This photo was posted by the People's Liberation Army, PLA, but then Taiwanese autorities denied such photo.
* Accordiding to Taiwanese Senior Photographers and Principles of photography this photo requires an ultra Long Focal Length lens, the photographer must retreat to a long distance behind the 3 objects, but the warship does not have such a deep belly and the object's sidelines are too clean. Also, according to Taiwanese Earth Science experts, the proportion of the height of the mountain and the height on the surface of the ship is quite huge which is not reasonable. Moreover, according to the Taiwanese military officers, in the first photo that was posted, the taiwanese warship does not have a hull number, however on the second photo posted by the PLA, the warship has the number 935.
* The other fake image is one I believe most of us have seen, the selfie of the pilot in the sky, it is really funny since at first sight looks fake but still some people thought it is real. In order to open the window you need to pull it inwards and then pull it back, it needs a lot of pressure to open it. As we know, with increasing altitute, the pressure lowers, that is why the plane's canin i9s pressurized to counteract the lower pressured oxygen so that people can breath.
* If we look at the right engine, it is not running and this would be an emergency, I believe the pilot would not have enoughtime and calm to take a selfie. Also, if we look at his glasses, we can see parts of the airport and not clouds. If you guys wish to see the pictures and all the references you can check the google drive for the presentations of week 3, gruop 11, from this year, not last year.
* I wonder, how will these 2 weeks of no class affect our coming weeks, I hope it does not bring problems to us.
* Hopefully, next week small problem will be as interesting as the one from this year so I can present it in front of the class

# 2023-10-12 #
* I will not be able to attend this class since I have an appointment at the hospital to see the doctor. However, I will ask my teammates about what content was covered today
* I plan to read some articles about it as well and then check what the small problem for next week is and hopefully I can present next week.

# 2023-10-19 #
* We started this week's class by reading some diaries and then by presenting the homework from last week. After seeing my classmates presenting their homework I believe we should devide the presentations along the semester, for example froup 1,2,3,4,5 will present this week and five more groups next week so we all have the oportunity to present and get that score.
* Something important the professor mentioned during the presentations is that we need to put the units on the y and x axis when putting graphs so we can actually read it, I will have it in mind next time.
* Wikipedia is not a trusted website.
* I have headache during the class, I should take a pill.
* It so sadly I cannot upload photos here so I can show my classmates sleeping during class.
* Nothing that I can relate to fictional stories happened to me this week
* The president of El Salvador, Nayib Bukele, believes my country will be way more safer than what it is now. He has worked to erradicate violence and corruptiion during these 4 years and make El Salvador the safest country in the Latin America region, I want to mention it was the most dangerous country in the region few years ago. It is so safe now that Miss Universe 2023 will be hold there at the end of this year. He also believes El Salvador will finally get out of debt and the poverty rate will be super low

# 2023-10-29 #
* I do feel stressed and anxious, not only this week but every week, I basically have exams every week and they are not easy at all. Balancing my studies, work, family, mental health and physical health is not that easy. 
* The good thing is that every day is one step more to graduating from college.
* Yes, when I am stressed or having anxiety and panic attacks it is very hard to think straight and complete tasks.
* I read an article about how to stay healthy and I found out that 75% of our brain is water, by drinking enough water we can boost our intestines peristalsis, that is the automatic wave-like movement of the muscle that line our gastrointestinal tract. Water can also prevent brain malfunction and headache.
* We also need to consume less sugar, since high amounts of this can lead us to gain weightr and have snoring and sleep apnea. It is better to consume sugar less than 5% of the whole day calories.
* We must get enough sleep. Good sleep improves our brain performance, mood, and health. Not getting enough quality sleep regularly raises the risk of many diseases and disorders. These can range from heart disease and stroke to obesity and dementia.
* When we sleep, the brain totally changes function, it becomes almost like a kidney, removing waste from the system. Everything from blood vessels to the immune system uses sleep as a time for repair.
* I can tell that sleeping not enough and sleeping late can really affect your heart, and it can be worse if you are under stress, that is why my heart condition got worse during my first 2 years of university. So I decided to change my sleeping habbit and lifestyle. 

# 2023-11-04 #
* I have a friend who had depression, at first, I did not know he was depressed, I thought the amount of stress he had was due to the school pressure, we were both struggling with our classes since it is not easy at all.
* I started to notice he skipped many classes and that he would not reply to my messages or phone calls. I tried to contact him though other friends and I was told the same. I was worried about him so I went to his dormitory and tried to find his room number, it took my a while but I found him. I did not blame him for not asnwering, I just asked him how was he doing and then he agree to talk with me.
* After listening to him and letting him know I understood him since I have experienced the same feeling, I suggested him to face the problems and try to find a way to solve them even the result is not what we wanted.
* He changed his plans in order to get away from that depression and now he is doing really good.
* WAYS TO SPOT SOMEONE WITH DEPRESSION:
* 1) Appetite and weight changes.
* 2) Looking excessively tired and not wanting to go out.
* 3) Less optimistic and motivated than others.
* 4) Unable to concentrate

* WAYS TO HELP SOMEONE WITH DEPRESSION:
* 1) Let them know you care and are there to listen.
* 2) Do not blame, do not judge them.
* 3) Once you are able to have a conversation with them ask questions like: When did you begin feeling like this? How can I best support you right now? Have you thought about getting some help?
* 4) Have a good energy when extending loose invitations to activities
* 5) Stay in touch with them even after they are doing fine.
# 2023-11-26 #
* I have been so busy and it never ends, I wish this semester ends soon.
* I watched the video about Millennials in the workplace, and Simon Sinek mentioned and I like the fact he remarks we have a generation that has access to an addictive numbing to chemical dopamine through social media.
* I think one of the issues with social media, in particularly Instagram, people look like they have a much better life than they really do, people are posting pictures or stories of when they are really happy, they even modify those pictures to look better.
* In fact, those people seem are really happy, they are actually not that happy, some of them are really depressed, sad or mad, some of the happiest people, actually some of the saddest people in reality.
* I think we should not make social media an "essential" thing in our lives.

# 2023-12-05 #
* I have been really stressed these days, more than usual.
* In the video " How to choose your news", by Damon Brown, the controversial topic of some governments misleading the public with social media cooperation, made me really consider the verity of the news I found for the small group task for this week.
* I do believe it is really hard to believe whether what we see on the news is true or not since using social media to spread misinformation or lies allows governments to control narratives, manipulate public opinion and maintain a certain image, this way they can shape the way people perceive events, policies and even the government itself.
* Very hard days are coming and I hope I can pass all my classes :) .

# 2023-12-10 #
* I think planetary boundaries are like rules we need to follow to maintain balance in our planet.
* I think the fact of crossing all these 9 boundaries is very scary. Crossing the land use change boundary worries me a lot since the creation of new cities and infrastructure is something that growing really fast and it might never stop since our population keeps increasing and we need to support them.
* Deforestation brings so many serious consequences. Reforestation has been a solution but I think is not that good since by planting more trees, we also need to care about the carbon-storage capacity, and replacing lost forests is quite impossible so we gotta protect the ones we still have.
* I would like part of my career to be about doing something to reduce greenhouse gas emissions, like developing electric and hybrid-electric aircrafts.

# 2023-12-22 #
* Before starding this daily dairy, I want to clarify that I am super likely to be super stressed these coming days until I finally finish all my exams
* Successful and productive
* I felt successful because I attended one of my last class of the semester and submitted my last homework.
* I felt productive because I kept preparing my final exams and I went to work
* I will try to read more tomorrow

# 2023-12-23 #
* Successful and unproductive
* I felt successful because I went to Taichung to visit my sister and friends for Christmas dinner
* I felt unproductive because I could no study for my finals since I left everything in Tainan
* I will try to read more tomorrow after I go back to Tainan
# 2023-12-24 #
* I felt unsuccessful and unproductive
* I felt unsuccessful because I came late and very tired so I did not have enough energy to study 
* I felt unproductive because I did not prepare all the material for my Chinese final exam tomorrow
* I will let my body rest enough
# 2023-12-25 #
* I felt successful and productive
* I felt successful because I finished my first final exam and it went much better than what I expected
* I felt productive because I continued preparing the rest of exams and went to work
* I will let my body and brain rest enough
# 2023-12-26 #
* I felt successful and productive
* I felt successful because I finished another class and productive because I still prepare for my 3 final exams next Tuesday and went to work
* I will have breakfast with my girlfriend before my final exam
# 2023-12-27 #
* I felt successful and productive since I had my second final exam and it went really good, I keep preparing for my other finals and I went to work
* I will watch a movie tonight to release some of my stress
# 2023-12-28 #
* Things that help me ve productive:
* 1)Avoid multitasking
* 2)Limit distractions( phone, social media and youtube)
* 3)Take a break
* 4)Have a clean desk where I can study and do homework
* 5)Take a cold shower in the morning

