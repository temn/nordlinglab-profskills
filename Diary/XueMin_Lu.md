
This diary file is written by XueMin Lu E14116037 in the course Professional skills for engineering the third industrial revolution
# 2024-09-12 #
1. The teacher introduces himself.  
2. Students get to know one another.  
3. A discussion takes place on whether the exponential growth of food production will lead to abundance for humanity.
# 2024-09-19 #
This is the second class of Professional Skills for Engineering of Third Industrial Revolution.
In this class,the professor shows us what are *Conflict views & Harmony views. After showing us the differences between them,we discuss the conflicts in our daily life. By knowing the knowledges and doing the discussion,I think I can do better on cooperating and communicating with other people. And then,some groups do a short presentation about the relation between productions and prices. It makes me realize how important the technology progression is,and I also learn how to make a better ppt. I think this class is really interesting.
# 2024-09-26 #
Three Ways to Spot a Bad Statistic
Uncertainty in Data: Are there clear margins of error or limitations mentioned? If not, the data might not be reliable.
Relevance to You: Does the data apply to your situation or experience? If not, it might be misleading.
Data Collection: Was the data gathered in a trustworthy way? Poor methods can lead to bad statistics.
My Vision for 2034
In 2034, I hope to live in a green and eco-friendly city where sustainability is part of everyday life. My home will use smart technology to save energy, and I’ll enjoy parks and green spaces where people connect.
At work, I see myself as a project manager or consultant in renewable energy, leading projects to help protect the environment. I imagine using advanced technology, like artificial intelligence, to make work easier and more efficient.
Some trends will shape my life:

Sustainability will make businesses more responsible.
Remote work will give me flexibility.
Health and wellness will be a priority in workplaces.
Global connectivity will allow me to collaborate with people around the world.
I believe this future will let me grow personally and professionally, while making a positive impact on society.

# 2024/10/17 #
I recently had an eye-opening experience that changed the way I think about money and its true value. It’s fascinating how much more there is to learn about personal finance and the role money plays in our lives. The more I delve into the subject, the more I realize how important it is to understand financial concepts, especially when it comes to securing my future and making smart decisions. 
One aspect that really excites me is investing. I’ve always known it’s a powerful tool for growing wealth, but I never fully understood how it works or how to get started. Now, I’m starting to see it as an incredible opportunity, and I’m eager to learn more about the strategies, risks, and rewards involved. There’s so much to explore, from stocks and bonds to real estate and other investment options, and I can’t wait to dive deeper into the topic.
Today in class, the professor introduced us to Bitcoin, which has always been a subject of curiosity for me. I’ve heard so much about it over the years—both positive and negative—but I never really grasped the details of how it functions as a digital currency or why it’s considered revolutionary. Listening to the lecture helped me understand its underlying technology, blockchain, and how it fits into the broader world of finance and investing. It’s fascinating to think about how digital currencies like Bitcoin could shape the future of money. 
This whole experience has sparked a desire to learn more about personal finance, investing, and emerging technologies like cryptocurrency. It feels like a whole new world has opened up for me, and I’m excited to see where this journey will take me.
# 2024/10/24 #
Grades: I stayed up late studying for an exam because I thought my grades reflect my intelligence and future success. Even though I know grades aren’t everything, they still feel important to me.
Career: I skipped going out with friends to focus on studying and networking. I believe having a clear career path is the key to success after graduation.
Independence: When I got sick last week, I didn’t call my parents for help. Instead, I took care of myself because I think being independent shows maturity.
Taiwan’s Future
Taiwan’s leaders, like Tsai Ing-wen, see the future as one of innovation and sustainability. They aim to make Taiwan a leader in technology, green energy, and semiconductors, while also supporting equality and democracy. This vision includes:
lean energy and reduced reliance on imports.
A strong tech-driven economy.
Inclusive policies, like marriage equality.
Partnerships with democratic allies to protect Taiwan’s independence.

# 2024/11/07 #

Today, the teacher let us form supergroups to create and report on a new topic. I really enjoyed it because it made class more exciting and gave me a chance to meet and talk to new classmates. Making friends isn’t always easy, so I appreciated this opportunity!

# 2024/11/14 #
After watching the TED Talk about the choice blindness experiment, I found myself utterly amazed—and a little unsettled—by how unaware people can be of their own decisions. The experiment involved participants being asked to choose between two photos, only to have their selected photo secretly swapped with the other option. What really shocked me was how confidently the participants explained the reasons for their choices, even though they were unknowingly rationalizing a decision they hadn't actually made. It was almost as if their minds seamlessly filled in the gaps without realizing something was amiss. 
This experiment left a deep impression on me because it highlighted just how easily our perceptions and judgments can be manipulated. It’s incredible to think about how our brains, which we rely on to make sense of the world, can be so easily tricked into believing something false. It made me reflect on the strength—and the limits—of our self-awareness. If these participants were unaware of such an obvious change, how many times might we justify decisions or beliefs that aren’t entirely our own without realizing it?
The implications of this experiment are both fascinating and unsettling. It made me wonder: how often do I rationalize choices that weren’t truly mine? For example, could I have been influenced by subtle cues, societal expectations, or peer pressure without even knowing it? How much of what I think and believe is genuinely "me," and how much has been subtly shaped by external factors? These questions have been lingering in my mind since watching the talk, and they’ve pushed me to examine my decisions more critically.
Beyond the personal implications, this experiment also raises larger questions about human behavior and decision-making in general. It challenges the assumption that we are always conscious, rational agents who know exactly why we make the choices we do. Instead, it suggests that our sense of agency might be more fragile than we think. This insight could have profound implications for areas like psychology, marketing, and even politics, where understanding how people make decisions can shape strategies and outcomes.
Overall, the choice blindness experiment was a powerful reminder of just how complex—and sometimes unreliable—our minds can be. It has inspired me to think more deeply about my own choices, question my assumptions, and stay open to the possibility that I might not always be as self-aware as I believe. While it’s a bit unsettling to acknowledge the fragility of self-awareness, it’s also an opportunity to grow and better understand the workings of the human mind.
# 2024-11-21 #
In today's discussion, we delved into how algorithms and big data are utilized in systems like credit scoring, job recruitment, and insurance assessments. One significant issue that emerged was the persistence of bias in algorithmic decision-making. Algorithms, often trained on historical data, can unintentionally reinforce outdated or unfair practices. For instance, a hiring algorithm might favor candidates from a specific background due to biased patterns in past recruitment data, perpetuating inequalities rather than addressing them.  

Despite these challenges, the potential of data to drive fairness was also highlighted. By carefully designing and updating algorithms, we can use data to promote more equitable outcomes. This requires human oversight to identify and address biases, as well as the integration of recent, representative data that reflects current societal norms. Ultimately, ensuring algorithms serve the common good demands transparency, accountability, and a commitment to ethical practices.
# 2024/11/28 #

Consensus plays a vital role in decision-making, particularly in creating laws, as it ensures that all voices are acknowledged and respected. By fostering inclusivity, consensus promotes unity, reduces conflict, and strengthens social cohesion. In lawmaking, achieving consensus leads to the development of regulations that are broadly supported, enhancing their effectiveness and legitimacy. Laws formed through consensus are less likely to encounter resistance and more likely to be embraced by the public.  
**Principles for Achieving Consensus**  
1. **Transparent Communication**  
   All stakeholders should have the opportunity to express their concerns and suggestions openly. Transparent dialogue reduces misunderstandings and fosters trust.  

2. **Active Listening**  
   Participants must genuinely listen to each other’s perspectives, avoiding interruptions. This approach builds understanding and mutual respect.  

3. **Respect for Diverse Opinions**  
   Acknowledging differing viewpoints and striving to find common ground is key to achieving a balanced consensus.  

4. **Willingness to Compromise**  
   Reaching consensus often requires flexibility and a readiness to make concessions for the greater good.  

5. **Emphasis on Shared Goals**  
   Focusing on mutual objectives, such as fairness and equity, rather than individual interests, helps guide discussions constructively.  

---

**Steps to Build Consensus**  

1. **Define the Problem**  
   Clearly outline the issue and establish shared goals to set a foundation for discussions.  

2. **Gather Relevant Information**  
   Collect data, expert opinions, and input from the public to inform decision-making.  

3. **Facilitate Deliberation**  
   Provide a platform for stakeholders to share their views and propose solutions in an inclusive manner.  

4. **Negotiate Solutions**  
   Resolve differences through constructive negotiation, aiming for mutually beneficial outcomes.  

5. **Formalize the Agreement**  
   Once a solution is agreed upon, document the terms and integrate them into actionable laws or policies.  

By adhering to these principles and steps, consensus can be effectively achieved, ensuring that decisions are both equitable and sustainable.
# 2024-12-05 #
Navigating Planetary Boundaries with WBS and PERT.The concept of planetary boundaries reminds us that Earth has limits—thresholds we shouldn’t cross if we want to maintain a stable and habitable planet. Addressing global challenges like climate change or resource depletion often feels overwhelming, but tools like WBS (Work Breakdown Structure) and PERT (Program Evaluation and Review Technique) can offer a practical way forward.
WBS helps us break down large, complex problems into smaller, manageable tasks. For example, tackling climate change could be divided into main areas like renewable energy adoption, reforestation, and energy efficiency. Each of these could then be split into even smaller steps, such as building solar farms, planting trees, or upgrading public transportation systems. By organizing tasks hierarchically, WBS makes it easier to see how everything connects and where to start.
On the other hand, PERT helps us plan and manage time effectively. It considers optimistic, pessimistic, and most likely timelines for completing tasks, which is invaluable when dealing with uncertainty. For example, transitioning a region to renewable energy might involve a best-case scenario of five years, but setbacks could stretch it to ten. PERT ensures we account for these variations and create flexible, realistic schedules.
By combining these tools, we can transform planetary challenges into structured projects with clear goals and timelines. It’s a reminder that even the most daunting problems can be solved step by step, as long as we approach them with careful planning and determination.
# 2024-12-12 #
This week, I spent a lot of time reflecting on the intricate and alarming links between income inequality, climate change, and the limits of our planet’s capacity to sustain life as we know it. These issues are deeply interconnected, creating a web of challenges that amplify one another and demand immediate attention. Climate change, for instance, is disrupting the delicate balance of the water cycle, causing more extreme weather events such as devastating floods, prolonged droughts, and powerful hurricanes. These changes not only threaten human lives and livelihoods but also stress agricultural systems, water supplies, and infrastructure, making it even harder for vulnerable populations to recover and adapt.
As ice caps and glaciers melt at an unprecedented rate, the resulting rise in sea levels is altering sea currents and putting entire ecosystems at risk. Coral reefs, which serve as critical habitats for marine life and protect coastal communities from storm surges, are bleaching and dying due to warmer waters. These cascading effects extend beyond environmental damage, impacting global food chains and economies. The looming risk of a sixth mass extinction is a chilling reminder of the profound and often irreversible impact humanity has had on biodiversity. Species extinction rates are accelerating as natural habitats are destroyed, and ecosystems are unable to adapt quickly enough to human-induced changes. 
What makes these crises even more heartbreaking is the disproportionate burden they place on the most vulnerable communities. Low-income and marginalized populations, often with the fewest resources to cope, bear the brunt of the consequences. Whether it’s rising sea levels swallowing island nations, droughts decimating crops in already impoverished regions, or extreme heat waves threatening lives in urban slums, the inequity is glaring. Income inequality exacerbates these challenges, as wealthier nations and individuals contribute the most to climate change yet have the resources to shield themselves from its worst effects. Meanwhile, those who contribute the least are left to struggle with the fallout, trapped in cycles of poverty and environmental degradation.
These interconnected crises underscore the severe consequences of exceeding the Earth’s planetary boundaries. The concept of planetary boundaries highlights the limits within which humanity can safely operate without causing irreversible harm to the environment. Yet, we are already exceeding several of these boundaries, such as biodiversity loss, carbon emissions, and land-system change. This reckless overreach threatens the stability of the very systems that sustain life on Earth. It’s a sobering reminder that the planet’s resources are finite, and continuing on our current path will lead to devastating outcomes for all of us.
Addressing these challenges requires an unprecedented level of global collaboration, innovative thinking, and a firm commitment to equity and justice. Governments, businesses, and individuals must work together to implement sustainable solutions that balance economic growth with environmental preservation. This includes transitioning to renewable energy, adopting sustainable agricultural practices, protecting and restoring ecosystems, and investing in green technologies. But beyond technological solutions, we must also address the root causes of inequality and ensure that the voices of vulnerable communities are included in decision-making processes. Climate justice must be at the heart of our efforts, recognizing that safeguarding the planet means protecting everyone, especially those who are most at risk.
The responsibility to act has never been more urgent. Safeguarding our planet is no longer a choice or a distant ideal—it is an immediate and critical responsibility to ensure a livable future for generations to come. Every action we take today, no matter how small, contributes to the larger goal of preserving the Earth’s delicate balance. By acknowledging the interconnectedness of these crises and working collectively to address them, we have a chance to build a more equitable, sustainable, and resilient world. It’s a daunting challenge, but it’s also an opportunity to reimagine our relationship with the planet and with each other.
# 2024-12-19 #
**Productive but Unsuccessful**  
In the morning, I was productive, completing my research paper ahead of schedule. However, the afternoon didn't go as well. I spent several hours organizing my notes for another project but got distracted and lost focus.  
**Improvement Tip:** Find better ways to organize my notes.

# 2024-12-20 
**Successful but Unproductive**  
I went to the outlet with my friends, had a great time, took lots of photos, but didn’t manage to study.  
**Improvement Tip:** Try to balance my time better.

# 2024-12-21 #
**Successful but Unproductive**  
I spent most of my time playing my cellphone and basketball , leaving me with little time for my own studies.  
**Improvement Tip:** Learn to balance study time with other entertainment.

# 2024-12-22 #
**Successful but Unproductive**  
I went to a Christmas market with a friend. We bought some things, took photos, and had a wonderful time. However, I didn’t study.  
**Improvement Tip:** Focus more on studying.

# 2024-12-23 #
**Successful but Unproductive**  
I went home and cooked for my girlfriend , but spent so much time doing it that I didn’t get enough study time in, at the same time,I also sleep late.  
**Improvement Tip:** Learn to study during smaller gaps in the day.

# 2024-12-24 #
**Unsuccessful and Unproductive**  
My friend was in a bad mood, and even though we went to Knowledge to study automatic control, I ended up spending most of the time comforting him, which left me little time for studying.  
**Improvement Tip:** Try to cheer my friend up faster so I can focus on my studies.

# 2024-12-25 #
**Successful and Productive**  
It was Christmas, and my friend and I had a big meal together and studied fluid mechanics.  

# 2024-12-26 #
**Unsuccessful but Productive**  
I managed to complete all my homework on WileyPlus, and even though it was tough, finishing it gave me a sense of achievement, but I still spent a lot of time on it.  
**Improvement Tip:** Do the homework early.

# 2024-12-27 
**Unsuccessful but Productive**  
I felt that I didn’t perform well on the fluid mechanics exam that day, but I went to the library in the afternoon and studied for a long time. This gave me a great sense of accomplishment and helped me forget the unhappiness from earlier in the day.

# 2024-12-28 #
**Unsuccessful and Unproductive**  
Today, my girlfriend and I wanted to go to a café to study, but since we woke up late, most places were already full. We spent a lot of time just looking for a spot, and it took us nearly two hours to find a suitable place. It was a huge waste of time, and as a result, we ended up studying much less.
**Improvement Tip:** Try to get up early.

# 2024-12-29
**Successful and Productive**  
I went to the library at 10 a.m. and studied a lot, making extensive notes on fluid mechanism.  

# 2024-12-30 #
**Successful and Productive**  
We conducted an experiment in automatic control, which was challenging, but we made progress. I also did well on my test today.  

# 2024-12-31 #
**Successful and Productive**  
On the last day of 2024, I went to Knowledge after school and studied until 11 p.m. Later, I joined my friends to celebrate New Year's Eve by going to City Hall and enjoying the fireworks,we also play the fairy wand.

# 2025-01-01 #
**Successful and Productive**
Despite not wanting to study on the first day of the new year, I pushed myself to prepare for the tough exams ahead. I spent the entire day studying with my friends.
# 2025-01-02 #
In my obituary, I would like to be remembered as someone who combined passion, knowledge, and compassion to make a lasting impact on the world. I dream of contributing significantly to solving complex problems, whether through education, innovation, or helping others find their potential. I hope to leave behind a legacy of kindness, resilience, and meaningful achievements that inspire others to strive for their best.

**Two Inspirational Figures**  

1. **Albert Einstein**  
   Albert Einstein inspires me not only because of his groundbreaking contributions to science but also because of his ability to think beyond conventional boundaries. His curiosity and persistence allowed him to challenge existing ideas and discover new ones. Despite facing setbacks in his early career, his dedication to his work led to revolutionary discoveries, such as the theory of relativity. I admire how he combined intellectual brilliance with humility and a desire to use knowledge for the betterment of humanity.  

2. **Mother Teresa**  
   Mother Teresa is another person I deeply admire. Her life was dedicated to serving others, especially the poor and marginalized, through selflessness and compassion. She reminds me that no matter how small our actions may seem, they can have a profound impact on someone else’s life. Her unwavering commitment to helping others inspires me to prioritize empathy and kindness in everything I do.  

By looking up to figures like Einstein and Mother Teresa, I aim to balance intellectual growth with a strong sense of social responsibility. They remind me that success is not just about personal achievement but about making the world a better place.
# 2025-01-14 #
**Ability to use denpmographics to explain engineering need **
In this class, we extended what we learned in the previous session and connected it to our knowledge, such as the relationship between demographics and engineering needs. This helped me understand that engineering is not limited to mechanics or thermodynamics as we often think, but is deeply interconnected with society as a whole. It also made me reflect on whether my past perspectives were too shallow and if there are deeper meanings behind these concepts.
**Know the current world demographics**
After attending this class, I gained a deeper understanding of the current world demographics and reflected on many issues related to the world and its population. These are topics I hadn’t explored in other classes, which made it very interesting. The professor’s engaging explanations and interactive Q&A sessions helped me stay focused on the lesson. While learning, I also gained a broader understanding of the world.
**Understand plantary boundaries an the current state**
After attending this class, I learned that the nine planetary boundaries represent different aspects of Earth’s systems. I also understood how these boundaries can impact the planet’s stability. This has made me pay closer attention to the Earth’s current changes and conditions.
**Understand how the current economic system fail to distribute resourses**
After attending this class, I realized that the current economic system and resources in our society are not as straightforward as I had previously thought; there are many aspects that require attention. At the same time, I also came to understand the connection between mechanical engineering and these systems.