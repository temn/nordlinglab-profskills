
This diary file is written by Ethan yang E14102096 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-07 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* I don't think exponential growth applies to food production.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2023-09-14 #

* We are having our first presentation ,I've learned a lot from the other's speech.
* The word type have to be corrected ,each type of word has its way to use.
* We have to add the reference under every charts or figures.
* The TED talk speaker's topic is quite attractive.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2023-09-21 #
* This week lecture is about fake news.
* learn how to spot a bad statistic. (3 ways: 1. Do you see uncertainty in the data? 2. Do you see yourself in the data? 3. How was the data collected?)
* I feel the second ted speaker talk too fast, so its hard to understand what she saying. but in the end I got the point.
* My team did not got a chance to present this week.

# 2023-10-12 #
* Professor talked about financial system today, and introduce some interesting facts about deposits and loans.
* Gold is often one of the assets that is considered to be the ultimate safe haven, which value usually doesn't fluctuate dramatically.
* If one bank's deposit-to-loan ratio is greater than 1, which implies that the bank has loaned out every cent of its deposit.
* It is the danger zone because it has no reserves to pay customers for demand deposits.
* It's interesting to learn that whenever Chinese's new year has come, the amount of money government held increased drastically
  due to the Chinese's envelope custom.
* I also learned a tricky word "fungible", which is something may be replaced by another equal part or quantity.
* Money is the typical instance that is fungible.
* The purchasing power of each currency has droped exponentially for the last hundred year.
* The inflation of currency has become so severe that I can't help wonder what can we afford to buy
  with a thousand dollars in the next or two decads to come, perhapes not even a drink.
* If the government keep letting the inflation sabotages our currency purchasing power, then we must publish a new currency.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.
* Every politician tells that we must need to fix this gap, and makes promises to their voters.

# 2023-10-19 #
* today we saw some TED talks about facism, I only had a rough idea on how it was nowadays and these videos gave me a insight
* many of the members of a facism groups are marginalized people that wanted to belong to a group, and not because of the cause
* with potholes even a good person can do wrong decisions when manipulated by others
* it's interesting to see how someone can become so ignorant about others until they see other people's perspective

# 2023-10-26 #
* today's class was really relatable since I have many problems with anxiety
* Alan watkins gave a perfect explaination on how our body is direct related to our behaviour
* It was already in my knowledge that exercise have many benefits to our mental health
* the last ted talk that we watched was quite boring

# 2023-11-02 #
* today we had our first super group presentation, I panicked a bit on how many members we had and how we would organize everything but in the end we did alright
* depression is a very heavy topic to discuss, we watch many stories about people that had experienced it in different perspectives
* sometimes just listening to what a people want to say might mean much more than you can imagine
* after listening all these stories, it left a bad taste in my mouth, but I also agree it's a topic that must be discussed
  our new projects are hard
  
# 2023-11-09 #
* some people shared their experiences with depression and anxiety, to be honest it was a pretty relatable moment
* anxiety has a really heavy impact on college students, I got curious on how english speakers would have to do if they wanted pychologist support
* find purpose on life was already something I was aware, and I still chasing for one until this day, but it really is not a easy task
* we sit with our big group together today, we discuss about our big project next week

# 2023-11-16 #
* today we have many big presentations of many topics
* My group presented about reducing co2 emissions,I was a bit worry in the beginning because I thought it was a hard topic but in the end everything went ok
* Some facts presented were already part of my knowledge but not in depth, so it was interesting to listening in more detail
* This last week was a hell of exams and projects

# 2023-11-23 #
* I couldn't go to physical class today so I lost a part of the experience, professor propose a experiment related to our dependency of smartphones
* the groups discuss many ideas, I didn't had any expectations but some ideas were actually feasible and realistic
* regarding to noise problems from constructions in taiwan, it's indeed a problem and the authorities usually neglect these cases
* some groups also had the same ideas as my group like the use of EVs, it was interesting to see different perspectives on the same solution
* today's class was really relatable since I have many problems with anxiety
* the last ted talk that we watched was quite boring

# 2023-11-30 #
* The professor encourages us to form a more concrete action idea.
* In order to be concrete, we should include where, who, when, what, how, etc.
* I'm particularly interested in the presentation about sleep, cause I really want adequate sleep.
* The more chaotic the event is, the less you should follow it every few minutes.
* Verify the truth before spreading on social media.
* We have more freedom on the flow of information, but with that freedom comes responsibilities.
* The speaker suggests that multiple factors combined led to the destruction of the mediterranean societies around 1177BC, and the cities couldn't survive when one is destroyed because they are connected, or globalised. In nowadays world, the situation could be even worse should similar events happen, and I think it's important to strengthen the relationships between countries. It may be difficult for a single country to solve the problem, but together, human can have a better chance surviving.After watching the video, I think I will have to ask my boss to give me gold and silver rather than money as salary and build an undergroung vault to store them.

# 2023-12-07 #

* I've learned from the lecture about the planet boundary. It's quite surprising to me that except for issues frequently heard like climate change the nitrogen and phosphorous are overly consumed by us as well.
* I am not optimistic about what the world is gonna look like many years later if we don't make some changes. I really don't understand why this isn't an issue that people really care. The news I've read recently are mainly about the Isarel-Hamas conflicts. The wars initiated by stupid people are really distracting us.
* In the TED Talk about planetary boundary, the idea of threshold is interesting to me. We don't experience a very serious consequence now, but if we go beyond the threshold, there would be deadly consequences for us.

# 2023-12-14 #
* I've learned some more precise knowledges on how to plan things, including WBS, PERT, Gantt chart.
* The professor encourged us to focus on outcome. Indeed, if one don't even have an idea about what the outcome is going to be, it'd be impossible to finish a thing.
* After learning from lectures on issues regarding planetary boundary, I've understood that our planet is sicker than I'd previously thought. What we can do to solve this problem, based on scientific evidences, is surprising and kind of weird. If we have less children, can human survive? (In Taiwan, for example, most family nowadays have only one kid, if they reduce the number to zero, then there won't be next generation!)

# 2023-12-21 #
* The week's lecture about how to become successful is inspiring. I wish I could have learned this earlier.
* I like the chart of important/not important, urgent/not urgent, it's a good way to manage and I've started using it. I also combine it with the WBS to determine how much time each task would consume.
* ** 2023-12-21 **not successful, not productive: I fell behind of my schedule and even dosed off while I was studying. I've tried to implement the strategies to manage time learned in this course to plan tomorrow's schedule.
* ** 2023-12-22 ** successful, not productive: I've passed the final exam of my swimming course.(In the beginning of the semester, I don't know how to swim, and now I can swim 400m.) However, I went home today and chatted with my parents all evening, which casued a totally delay of my schedule. I will stick to my schedule tomorrow.
* ** 2023-12-23 ** not successful, not productive: I didn't do much things today. The schedule is just not working.
* ** 2023-12-24 ** not successful, not productive: It appears that the environment at home is too relaxing for me. I'm not very productive being here.
* ** 2023-12-25 ** successful, productive: I've finished the final project of another GE course. Because Tuesday is the deadline, I'm very productive.
* ** 2023-12-26 ** not successful, not productive: My tummy isn't feeling well. My studying performance was affected as well.
* ** 2023-12-27 ** not successful, productive: Nothing gives me senses of achievement today, but I'm still productive.

# 2023-12-28 #
* How to tie a tie: Windsor knot and half windsor(suitable for informal occasions) are simple and useful.
* The idea that we can randomly choose who to be our leader randomly is interesting, and I like it.
* However, I think there's a problem. It is that not everyone want to be in the parliament.
* The idea of verticle farms work may fine in cities in developed countries, but actually, a lot of people living in poor countries may not be able to access this kind of technology, and they are people who are truly threatened by hunger.

# Homework:Summary of what I think my future would be like after 10 years #

   Ten years from now, I envision a future that is shaped by several key factors, including my place of residence, my professional pursuits, and the prominent trends that will significantly impact my life. While it's essential to acknowledge that the future is uncertain and subject to change, I can speculate on how these aspects might evolve based on current trends and my aspirations.


   First,in the next decade, I expect to live in a more interconnected and sustainable world. The concept of smart cities will have advanced, incorporating cutting-edge technologies to enhance urban living. I envision residing in a well-designed, eco-friendly apartment or home within one of these smart cities. This urban environment will prioritize green spaces, efficient public transportation, and renewable energy sources, contributing to a cleaner and more sustainable lifestyle.


   Second,as for my professional life, I anticipate that the workforce will experience significant transformation. Automation and artificial intelligence (AI) will continue to advance, affecting a wide range of industries. To stay relevant and adapt to these changes, I will have transitioned into a role that combines creativity, problem-solving, and human interaction areas that are less susceptible to automation. Whether it's in a creative field, education, or a leadership role, my work will revolve around tasks that require emotional intelligence and innovation.


   Third,remote work will remain a prominent trend, further accelerated by the experiences of the COVID-19 pandemic. However, the nature of remote work will evolve, with more companies adopting hybrid models. Advanced virtual collaboration tools and augmented reality will enable seamless remote teamwork. I'll continue to work from my home office, collaborating with colleagues and partners from around the world, thanks to enhanced digital communication technologies.


   fourth,environmental sustainability will be a defining factor in my future. Climate change will continue to drive global efforts to reduce carbon emissions, leading to more sustainable practices in various aspects of life. I'll strive to minimize my carbon footprint by using electric vehicles, consuming locally sourced and plant-based foods, and reducing single-use plastic. Sustainable living will become not just a trend but a necessity for our planet's well-being.


   Fifth,the importance of health and well-being will be more apparent than ever. Advances in healthcare technology, personalized medicine, and data-driven health management will enable proactive health monitoring. I'll regularly use wearable devices that track my health metrics and provide personalized recommendations for diet, exercise, and mental well-being. Telemedicine will continue to grow, making healthcare more accessible and convenient.

   Overall, my future will be characterized by adaptability and resilience in the face of rapid technological advancements and global challenges. I'll embrace sustainable living practices, leverage digital tools for work and collaboration, prioritize my health and well-being, and continue to explore opportunities for personal growth and development. While the specifics of my future may vary, these overarching trends and values will guide my life in the coming decade, shaping a more interconnected, sustainable, and fulfilling existence.