This diary file is written by Jethro Salvador H34105040 in the course Professional Skills For Engineering The Third Industrial Revolution.

# 2023-09-07 #
* It's the first lecture today. I've never had class in Tze-Chiang Campus before.
* I love english based class :).
* Quiz and group workshop at the first meeting?

# 2023-09-14 #
* Its the second lecture today, the weather is nice today.
* I can't find my third group member, maybe he/she dropped the class.
* Several groups presented today using last week's topic.
* This class also discusses economic growth from various points of view. At first I thought this was just a class related to innovation in industry!

# 2023-09-21 #
* Today was the third lecture, our group didn't get the chance to present again today, maybe next time.
* This is my first time using git. The class began with the professor's instructions to create a diary.
* I'm still confused in the pull request section, maybe I'll read the readme file later.
* As usual, another presentation.
* Today I watch TED talk lecture about how to discover fake news
* I'm impressed in how the goalkeepers of Gates Foundation collect data. Even if the data is not accurate, it's still incredible.

Homework
Talking about the future always sends me shivers as it has a lot of outcomes and possibilities, which I am very curious at. 10 years from now, I will I will be a better and more mature version than I am now. I have been thinking about two different outcomes. First one is staying in Taiwan and working in a big company as a supply chain analyst or maybe a risk analyst. I hope that I can secure my accomodation and transportation along with overcoming my language barrier, as these really important for me living in a foreign country. After 3 or 5 years of work experience, maybe I’ll go back to my home country Indonesia and develop there, maybe start looking for new job opportunities while starting my own startup. Of course, returning home I should bring additional knowledge for me to use, Chinese language, which these days is really useful in my country as there are a lot of foreign companies starting to enter other countries. Hoping as time goes by, I will be having a balanced life with a lot of knowledge I can use to my benefits and also for me to be able to do things I love, while able to make a living to support myself and the ones I love. What I need for these, will be consistency in learning and gaining experience. As for right now, focusing on my studies in my bachelor's degree comes first. After graduating, there are two options I can choose, pursuing Master's degree or exploring several places looking for works and gaining experiences related to my study and also for my daily life. I am also looking forward to finding internships in my department, doing useful things beneficial for myself. As for now, I have started trying to find experience, I have worked two times since I arrived in Taiwan, one in a factory setting up components and the other in a restaurant, which gives me additional experience for my daily life. Also, I get to meet new people and learned more in language. I also like to learn more in my leisure time, like learning how to make money in stock market. I like to calculate the true valuation of a company, especially in Indonesian company, because the opportunity for growth is still very large. Hopefully, in 10 years, I can achieve all of my goals without any difficulties. Even if there are difficulties, I will take them as a challenge for me to improve myself, so I can be a better person both in life and work aspects. I still have a lot of time, and I wanted to use these times so I can prepare for me to in my best form in life.

# 2023-10-12 #
* Today was the third lecture, finally got the chance to present.
* I found out that photo of earth taken by NASA in 2012 was fake.
* Got new teammate
* Talking bout money makes me think only one thing, it can't buy happiness, but I'm sure that money increase our level of happiness
* Learn about how the financial system works.
* I got to know what cause money to increase and what its value and functions.
* I found out that it is not government who print money.

# 2023-10-19 #
* I saw Crezh's, Indonesia's and Thailand's economic growth and information from the other groups.
* I learnt that we have to think of the long-term, dont just see something from the short-term.
* I got to watch and understand how the extremist and fascist got influenced and how they think.
* 3 fictional stories : 1. Taiwan's currency is declining because the new policy. 2. The marriage of prince Charles shocks the nation. 3. China deploy a missile to one of the important site of taiwan causing decline in both currency.

# 2023-10-26 #
* Lots of group present today, and I'm confused about what a fictional story really is...
* We watched 2 ted talks about our health, but the first one is what drew my attention the most.
* It was about how exercise would really improve our brain function.
* The need to exercise was something that I feel like I should be doing, but didn't.
* I guess it's just cool to see how it would also help prevent brain diseases.
* It was also funny and akward when the professor made us stand up to follow the actions in the video.

# 2023-11-02 #
* I think that today was pretty interesting, we got to join with other groups to create a supergroup.
* It was really great, we could share our thoughts and combine our presentation into one.
* After that, we were put into different group, sometimes it was really hard for me to adapt into a new group
* I think that the class overall was pretty interesting, but I'm not excited because we have to do a final with a new group again? I think it was a little bit too much.
* Too many projects that were introduced at once.... maybe this is the most tiring general education class hahaha

# 2023-11-09 #
* Found out that some of our classmates had ever dealt with severe depression and anxiety.
* Most of NCKU courses only teach us technical skills, not professional skills
* I thought experience is the most important thing in order to deal with new things, but the best way to learn is from our co-worker
* Next week we will make our first presentation in the final project group
* My group's topic for the course project is "Increase quality sleep to improve learning". I like the topic

# 2023-11-16 #
* Today is our first final project presentation
* Funny to know that we are the only group that present together
* Most of the class was only about presentation
* The professor taught us how to put reference on a scientific researh article

# 2023-11-23 #
* Today my teammate got a chance to present
* Most of the topics is about the goal to reach net zero emissions
* Learn about how countries deal with their pollution control and their regulations of ot
* Funny to hear that the law in Taiwan is somehow wierd, just like wahat the professor told us about we couldn't stop the construction workers even when they are so noisy at night
* Another classmate said that they are reported to the police just because they made footstep at their own place at night
* I personally don't think my country handle pollution well, and the regulation is not implemented at all.

# 2023-11-30 #
* Today we made presentation to our action ideas of the final project.
* I was searching for my phone once, after the first break, it's just not usual for me not having it in my pocket.
* Watched last year course project about using own water container, it actually saved our money.

# 2023-12-07 #
* The professor told us about google's new powerful AI called Gemini that performs better than ChatGPT, I personally don't think this AI can help me on my course lecture due to the GPT is not helping me up till now. But nowadays big tech companies really are working hard on their AI. It's pretty terrifying due to AI are more powerful than human since they never get tired.
* Today we were doing news analysis on our presentation, it's shocking that most of the global news is about war.
* The professor then let us create super groups to perform deeper analysis on a different point of view.
* I think every big country helped the country in war just to fulfill their political benefits.

# 2023-12-14#
* Today is the third final project presentation, there was so much to prepare.
* Every group is presentiong their revised action idea.
* I learned how to write a proper WBS by the professor comment on each group presentation.
* We have a debate, totally not prepared for that, pretty unexpected.
* Learned about third industrial revolution.
* World's GDP is hardly increasing, unemployments increase in every country. We are supposed to increase in quality, but why does this happen?

# 2023-12-21#
* Not successful and not productive, I woke up at 12.30 pm
* We were talking about the third industrial revolution
* For me, the Gordon Moore’s law diagram seems like a scatterplot diagram that is in statistical control.
* I don't think that 100% renewable energy can be achieved, it's just my personal opinion.
* Making poster for this course's final project

# 2023-12-22#
* Not successful and productive
* Not doing any assignments, hanging out with friend whole day

# 2023-12-23#
* Not successful and productive, 
* Doing assignments but it exceeded my timeline

# 2023-12-24#
* Successful and productive
* Finished my assignment and working part time at night.

# 2023-12-25#
* Not successful and productive
* Attending a lot of group meetings, but ended up playing games instead of doing my internship's work.

# 2023-12-26#
* Successful and productive
* Finished 2 final exam and learned one more subject for final.

# 2023-12-27#
* Not successful and not productive
* Playing games whole day

# Five rules of success#
* Take responsibility of your own word
* Dare to make speculation
* Start today, make today day one
* Build relations with anyone
* Help others

# 2023-12-28#
* Successful and productive.
* Today, the whole class were presenting their own five rules of success.
* We were also required to vote to other's outfit.
* I didn't dress as what required, it's too cold and I've to ride a motorcyle.
* For me, success is about making a lot of money.
* I like this course, it's somehow interesting.

# 2023-12-29#
* Successful and productive.
* Studying for finals and doing final projects.

# 2023-12-30#
* Successful and productive.
* Studying for finals and doing final projects.

# 2023-12-31#
* Unsuccessful and productive.
* Enjoying new year.

# 2024-12-021#
* Successful and productive.
* Happy new year guys.
* Studying for finals...

# 2024-12-02#
* Successful and productive.
* Have midterm in the afternoon and working at night.

# 2024-12-03#
* Successful and productive.
* Working on projects and finished just in time.



