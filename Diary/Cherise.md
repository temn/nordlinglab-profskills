## This diary file is written by Cherise (E34135019) in the course Professional skill for engineering the third industrial revolution
## 2024-09-12
- This class' interaction is excellent.
- This lesson shows me what I should prepare for the future.
- This class also have presentation which can help improve our presentation skill & teamwork.
## 2024-09-17
- Today's class shows me the difference of our world between 30 years ago and now.
- Presentation tips are given for those who makes mistake.
- I get to know about GIT.
## 2024-09-26
### Task 1 (diary)
- I get to know about sdg (Sustainable Development Goals)
- We get to see a TED talk about how to spot a bad statistic, how to seek truth in the era of fake truth and also how a good statistic looks like
- We get to learn how to cite articles and why should we cite it
- Ways to detect fake news are also being taught in this lecture
### Task 2 (3 questions that Mona told us to ask about bad statistic)
- 1) Do you see uncertainty in the data?
- 2) Do you see yourself in the data?
- 3) How was the data collected?
### Task 3 (write a 400-600 word summary of what you think your future will look like and why it will be so 10 years from now)
- In ten years, I can see myself possibly living in either Taiwan or Canada. Taiwan intrigues me because I’ve heard about a fantastic company where many alumni from my major work. I can picture myself thriving in that environment. But Canada holds a special place in my heart as my dream country, and I’m determined to explore job opportunities there. The idea of building a life in a place I’ve always admired is incredibly motivating.
I hope to be working in the medical field, particularly in developing medicines. This passion is one of the main reasons I chose my major; I’ve always dreamed of helping sick people and making a real difference in their lives. By the time I reach that ten-year mark, I hope to have carved out a fulfilling career that allows me to contribute positively to the health and well-being of others. Alongside my career goals, I also see myself becoming a more confident and independent person. Building self-assurance is crucial for both my personal and professional life, and I believe the experiences I’ll gather over the next decade will play a significant role in that growth.
Several trends are likely to shape my future as well. One of the most exciting changes will be technology and because of the advance technology, I am sure that I can get a quicker access to job opportunities. I truly believe that the job market will evolve to make finding employment easier and since technology is advancing so rapidly, I expect many systems will streamline the job search process, reducing some of the stress that often comes with it.
Next, I would like to say sustainability because I can see how sustainability will affect my life by improving my health through cleaner air and water and also I can see how almost everything will be eco-friendly which will ultimately influencing my lifestyle and financial decisions.
Artificial Intelligence (AI) is another area that fascinates me. With tools like ChatGPT already helping with everything from homework to brainstorming, I can only imagine how much smarter and more advanced AI will be in ten years. I see myself using these tools to assist with research and problem-solving, making my work more efficient and enjoyable.
The internet is also changing rapidly, and I expect it to become even more integrated into our lives. A decentralized internet—one that’s accessible from anywhere without relying on traditional devices—could provide incredible flexibility. Just imagine being able to connect and find information without the usual constraints; it would be a game changer.
Finally, the rise of autonomous vehicles is an exciting trend I think will transform commuting. In ten years, I believe these vehicles will be more affordable and widely used. This shift could make travel more efficient and stress-free, allowing me to use my time for work or relaxation instead of worrying about driving. I anticipate seeing less traffic congestion and fewer accidents, leading to safer, more enjoyable journeys.
## 2024-10-17
- I learned about the value of money
- I also get to know what M1A, M1B, and M2 stand for
- Bitcoin was also introduced in this lecture
## 2024-11-21
- Sadly, I missed this lecture because I felt sick.
## 2024-11-28
- The professor asked us to put our phone in front of the desk to see how addicted we are with our phone.
- The professor also introduced about the dark age to us.
## 2024-12-05
- This week we watched a TED talk by Johan Rockström, where he discussed the need to change our growth and consumption patterns to stay within these boundaries and protect Earth's ecosystems.
- The professor also gave us our final project
- We also get to explore the concept of "Planetary Bounderies"
## 2024-12-12
- At the beginning of the class, the professor asked us to give one example of something we can do to reduce CO2 emissions.
- We had a debate and a Q&A sessions which I find it very interesting.
- After the debate, a survey was given to us, and the professor also asked which group was the most interesting and would likely help us the most.
- We also watched a youtube video about the third industrial revolution.
## 2024-12-19
- This week topic is "how to succeed"
- I noticed that this week class discusses the concept of Ikigai, which emphasizes finding purpose and fulfillment in work.
- The professor also showed us a website to see the differences between how poor and rich people live.
- We had a debate in which the professor divided us into three groups: capitalist, worker, and environmentalist.
- We were given the task of writing down how we felt each day for a week.

## 2024-12-19 (Thursday)
1. Successful and productive
2. I was able to answer my calculus quiz and I met up with my friend, and we did some sports. After that, I continued studying for my quiz on Friday.
3. I will study earlier so that I can get enough sleep
## 2024-12-20 (Friday)
1. Unsuccessful and productive
2. I don't think I did well on my quiz and I completed everything I had planned for the day.
3. I will study harder so that I can do better on my next quizzes.
## 2024-12-21 (Saturday)
1. Unsuccessful and unproductive 
2. I did nothing today and slept all day
3. I will wake up earlier
## 2024-12-22 (Sunday)
1. Successful and unproductive 
2. I got to buy the things I want and I didn't do anything else
3. I will take a break to do my work
## 2024-12-23 (Monday)
1. Successful and unproductive 
2. I woke up early today and get to class but then I slept for hours after all my classes and ended up doing nothing
3. I will stop procrastinating
## 2024-12-24 (Tuesday)
1. Successful and productive
2. I did my laundry today after procastinating for so long and I didn't lay on bed and did a lot of things today
3. I will make plans for tomorrow so I will remain productive
## 2024-12-25 (Wednesday)
1. Unsuccessful and productive
2. I didn't get a good score on my pe final and I have start studying for my physics quiz which I will have on Friday
3. I will work out more

### 5 rules that I think make me more successful and productive
1. Study earlier and harder
2. Make plans for tomorrow 
3. Take a break from social media
4. Stop procrastinating 
5. Have enough sleep

## 2024-12-26
- We have to wear for success.
- The professor asked us to tell our 5 rules to be more successful and productive.
- We were given the same task as last week which is we have to write how we felt each day for a week.


### 2024-12-26 (Thursday)
- Unsuccessful and unproductive 
- I felt unsuccessful because I got the wrong time for my english project discussion. I thought we were going to discuss it at night but turned out, it was in the morning. I felt unproductive because I didn't think I had study enough for my phsics quiz on Friday.
- I will double check my schedule next time
### 2024-12-27 (Friday)
- Successful and unproductive 
- I felt successful because I think I did quite a great job on my physics quiz and also, there were a late Christmas event and it was quite a fun night. I felt unproductive because I didn't think I did anything done.
- Attending the Christmas event made me realize that I need to socialize more
### 2024-12-28 (Saturday)
- Unsuccessful and productive 
- Although I had a lot of fun hanging out with my friends, I still felt unsuccessful because I didn't focus on myself which made me felt really tired. I felt productive because I did all my to do list
- I will focus on myself
### 2024-12-29 (Sunday)
- Successful and productive
- I felt successful because I went to the museum today and I slept earlier than usual. I felt productive because I did some sports with my friends.
- I will get enough sleep
### 2024-12-30 (Monday)
- Unsuccessful and productive
- I felt unsuccessful because I woke up later than I planned to and I felt productive because I had studied for my final and also, I did all my to-do list
- I will sleep earlier so that I can wake up earlier
### 2024-12-31 (Tuesday)
- Unsuccessful and unproductive
- I felt unsuccessful and unproductive because I didn't get up from my bed and scroll through social media for almost all day
- I will take a break from social media
### 2025-01-01 (Wednesday)
- Unsuccessful and productive
- I felt productive because I did all my work today but I still fekt unsuccessful because I was overthink with almost all my decision
- I will stop overthinking