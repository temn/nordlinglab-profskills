# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-17 #

* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation 

# 2024-09-24 #

*Today’s presentation topic is “Is the world getting better or worse?”, our classmates present about climate change and changes in poverty rate.
*Today’s lecture is about the SDGS, we focus on the first few topics and spend some time discussing why the education system seems to have failed in these years.
*The video talks about how to spot bad statistics provides three questions for us to ask ourselves:
1. Can you see the uncertainty? 2.Can I see myself in the data? 3. How was the data collected?
*Homework
Ten years from now, I think I’ll still be living in Taiwan and I’m probably going to work at TSMC like most students graduate from mechanical engineering in Taiwan, but there is still some uncertainty due to the everchanging world.
The first thing I think will affect my future life the most is the relationship between Taiwan and China. If China takes over Taiwan in 10 years, the policies will change extremely, and it will no longer be a safe place for scholars to live. If this happens, I might find a new place to live and work at, most likely America. 
Secondly, the use of AI. Nowadays using AI is not a weird thing to do and I believe AI will become more and more common in 10 years. As AI become more mature, we will be able to use it in various fields and even for complex problems which A.I can’t solve today. As engineers, we often face complex problems, and AI will definitely change the ways we respond to them. 
What comes next is, the rapid improvement in astronautics. The development of recyclable rockets seems pretty good. Although it still somewhat optimism to think that we can colonize Mars or Moon within ten years, the trend is unstoppable. If we can’t do in 10 years, we will in 20 or even 30. Once we start developing these places the infrastructure becomes very important, and the students graduate from mechanical engineering are well-suited for this job. 
Another important trend is the metaverse, the boundary between reality and virtual world will become vaguer due to the conventional use of VR, AR and MR. A new economy system may form, based on virtual currency, which is totally different from now. Continuing from the last topic, there might be a chance we can control machines on MARS while we’re at Earth. It’s heartening that along with the development of metaverse plenty of new entertainment will be invented, but it’s also frustrating that these entertainments could be extremely expensive. 
Last but not least, climate change. Nowadays, politicians are implementing many policies to combat climate change, but it’s getting worse. We can see that in the near future, more and more different policies will be implemented, and they could become more extreme. In my opinion, these policies will heavily affect the technology sector.
In conclusion, I might be a semiconductor engineer working at TSMC and living in Taiwan, a civil engineer working on Mars but living in America, or even a successful businessman working from home in the virtual world.
