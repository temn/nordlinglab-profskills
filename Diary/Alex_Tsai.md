This diary file is written by Alex Tsai E14096499 in the course Professional skills for engineering the third industrial revolution.

---

# 2022-09-15 #

* Not until the first cooperation with my group realized I was a person with Conflict view.
* I am really inspired and puzzled by the exponential growth.
* I still can't understand wat is Git and how it works.*
* I am glad with the success of our presentation, since not everyone have a chance to do so. 
* 「Bad things happened quickly, but good things aren't build in a day」.
* I don't agree with the idea "People are getting happier" nowadays.
* Thanks to the develope of technology and spread of social media, those things placed more pressure on the people of my age. 
* We usually blame things terrible, such as politics, society, commodity price, salary...etc 
* It comes out a word call "emo" to this phenomenon.  
* We believed that even thought putting every effort, the world won't change, it will always being sucks.
* Related effects including the rage of music style change, civilization disease, and youth become pessimistic.
---

# 2022-09-22 #

* Summary of what I think my future will look like and why it will be so 10 years from now：
		When it is 2032, I am at age 31. At that time I think I already have a family, live in an apartment which locate in suburbs.
	I hope there will be a river infront of my balcony, parents, kids and dogs can all playing in a park aside the river. 
	It is a pleasant place for living, but also having convenient trafic ways to the downtown. 
		I haven't decide which job I am going to do, but I think at that moment, I should probably stay in a starup company for several years,
	or start a business on my own. These company have same characteristics, such as producing machine or robotst that can replace human labor works, 
	inspiring people to find out their purpose of living on this world, and finally keep proposing diverse education.
		The five current trends of change that will affect me, I think it will be strained international situation, low salary environment in Taiwan, 
	AI cover our life massively, new types of occupation born, and self conscious will be vigorously advocate. Since Russo-Ukrainian War haven't ended,
	a lot of question pop out like world energy supplies affecting our live. Low salary environment was a truth nowadays and I think it won't getting
	better only if government offer a powerful policy, but it seems to be hard. The quick change and invention of technology I think will affect me the
	most, AI will getting better use of our life, but humans rights group and ethics problem will correct it to a more suitible for humankind. The effect
	of quick change will also change our occupation, human labor works will be replaced by robots, their must be some new economics system appear to 
	satisfied these people. Due to the rely of social media, not only teenagers but all ages human spend more time or even fully surrounded by internet
	environment. At that time, our conscious will be easily controled by people with bad intentions. In order to prevent this situation happend,
		I believed some groups will stand out and call for publics notice. 
	Though there are many things that I can control, but I believe if we have a positive mind can conquer everything!
---

# 2022-09-29 #

* What gives money its value.
* Learn about how our financial system works
* Money is created by the bank when we are taking loans
* Bank has the power to create and print more money
* Extreme inflation can happen when more money is created if there is a low demand for the currency
---

# 2022-10-06 #

* Presentation Correction：
	1. If data increases or decreases exponentially, then multiply a log.
	2. Data related to industrial is often exponential, learn to think in this way.
	3. Make sure the range won't be out of data point.
	4. Log scale will let small changes become more obvious, easy for reading the details behind the lines.
* What does extremism flourish：
	1. Fascism-nationalism gather people together, loyalty to his/her country. People under Facism only identify their country and the identity of themselves, do everything whatever is good for country.
	2. The point that centralization rise up recently is the resources and data hold in few enterprise or people. It will cause them to be powerful enough to threat whole country. 
		Operating data centralized is more efficient than doing separately.
	3. Flawed Democracy didn't progress in these years, people tend not to vote for the candidate because the lose of confidence.
	
Task 1 Describe three fictional stories that you believe in and how they have affected your life at some point during the last week.

* Story 1 | A blindman always carries a lantern while walking in the dark. People ask him:"You can't see! Why do you always carries the lantern?" The blindman answer:"I carry the lantern not only can light up the road
		for other pedestrian, but also makes them easily to notice me, and they won't bump into me. Thus, I help others and protect myself at the same time." (Always learn to think of other is being a part of my life,
		not just my family and friends, but also every person I have met. Try the best to help others, because somehow they will return a favor while you are in need.)
* Story 2 | A lazy fisherman weaved his net only a size of table. One day he went to fishing with his net, but return without any fish. His neighber told him to weave a bigger net, so the fisherman make a large one within
		few days. He tried his new net and finally catch some fish! He was so excited, in order to get more fish so he decided to weave his net bigger and bigger! Eventually he made a giant net and go to fish happily.
		Unexpectedly, because the net was too big contain too much fish, so the fish pull the fisherman off the boat and he fell into the sea. (Nothing is the more the better, being greedy always lead to bad consequences.
		Besides money and power, I think how many things we can do within finite time are need to be considered. I usually tend to do many thing at the same time, but it oftren comes out every thing doing worse. Understand
		that I have only 24hours a day, and choose the most important thing to do. Don't be greedy)
* Story 3 | During the vaction, a capitalist came to a riverside trying to get some fish. He don't how to fishing so he ask a man who was also fishing by his side. The man teach the capitalist how to fish, he learned quickly and get a fish.
		The capitalist ask the man:"You are great at fishing! How about I invest you, and you can provide your techniques of fishing?" The man ask:"And then?" The capitalist said:"Then you can accumulate a considerable 
		capital to invest nore company!" The man ask:"And then?" The capitalist said:"When you have enough money, you can do whatever you want like I'm doing now! Sitting right here fishing." The man said:"But I am 
		doing what I want right now! How come I need to do those things I don't want for many years except for just do it?" (Believed that only if our career is successful we have ideal life is wrong, in fact, ideal life is 
		always been at our fingertips, but the pressure of work and the lack of work goals have been forcing us, making us ignore the process and all things around us. The secular definition of "success" prevents us from pursuing
		the life we want. It always reminds me to make decisions consciously, live a meaningful life, not the life that others expected you should be.)
---

# 2022-10-13 #

* Today we discuss about " What is knowledge? " My answer to it is," If a statement or thought of something can be accept by most of the people, then we can assume it is knowledge." 
	I believed there is no absolute answer in the world, because what we thought to be knowledge are created by humans, and it will change if we don't believe it or the values of whole society ddoesn't admit that.
* Engineer usually apply the simpliest rules to get the approximate answer, and this is why Newton's physics laws are more common than Einstein's theory of relativity in real life.
* In colledge, we gain information not knowledge. 
* It is a big problem that we accept things without thinking.
* The brain-changing benefits of exercise (zh) (13:04) https://www.ted.com/talks/wendy_suzuki_the_brain_changing_benefits_of_exercise
* What if we paid doctors to keep people healthy? (zh) (10:41) https://www.ted.com/talks/matthias_mullenbeck_what_if_we_paid_doctors_to_keep_people_healthy
---

# 2022-10-27 #

* Today's topic is depression. There are several suicide incident recently.
* I have lots of exam last week, four exam and two quiz in a single week!
* I am glad that I was so positive or maybe I will do some bad things to myself.
* We should care more about the condition of our classmates, friends and relatives.
* Care and companion is the way to heal those in depression.
---

# 2022-11-3 #

* Anxiety survey：
	1. The purpose of the survey is to let us know when facing depressed, I'm not alone.
	2. nothing get better by suffering alone, so go seek help !
	3. speak to student union about the unfairness of exam will be a practical solution.
	4. feel good : push a little boundaries but not too much to cause stress; do nothing will develope stress or feel guilty.
	5. short term affects our decision. We think exam to be more serious because it is urgent 
	6. people can only consider about 3 things once a time.
* mental illness has grown fast in last 20 years.
* speak to multiple counsellor, find the most suitable one.
* put limit on how much we do to support others.
* Definition of professional skill: the skill of work with other.
---

# 2022-11-10 #

* this week we have to deal with the course project, our group spend a lot of time on this project, but one of the group members didn’t give any effort. 
  That’s not good at all-I think! Unfortunately, after finishing the course project, we found out that our topic is same as another group which wasn’t 
  be allowed. When we found out, it was been Wednesday midnight! Damn, I said. Due to the mistake we did, our group need to make another slides of the 
  other topic which should be done in this week.
* 11/10, in class we watched the course project presentations made by some groups (group 1/2/3/11) which is a good chance to learn how other groups dealt 
  the slides, searching information they need ,dealing the material they got.
---

# 2022-11-17 #

* I was taking a personal leave to visit my grandma at the hospital today.
---

# 2022-11-24 #

* experient during the class：put our phone infront of the class, count how many times you look for the phone.
  result： I didn't look for my phone once time, because I know that there's no emergency message or calls might be in class time. I try to focus more in lecture and it is not a hard thing.
* the solution and action for the presentation is what we can do right now.
* our willing to do recycle depend on cost of the material. for example, gold is the most recycled material in the world, because it is expensive so everyone will recycle it as possible.
* maybe we can do a inventory to find out what is the most effective behavior that create the most CO2 in school.
---

# 2022-12-1 #

* In order to prepare the midterm of automatic control, I skip the class this week. I'm awfully sorry for professor and all the student.
---

# 2022-12-8 #

* the most influent impact to CO2 emissiion is having a child. Glad that we all student doing good on this.
* the another influent impact that we involved is having a mortocycle. 
* Though we knew change our mortocycle into a electical one is good to our environment, but the price of it is unaffordable for us, it cost too much.
* Action：Promoting reusable cups
	1. substitute with a biodegradable material.
	2. how much CO2 are emitted while making one cups?
	3. is the plastic cups durable？
	4. do a one week research.
	5. why plastic but not glass or metal(steel)？
	6. interview the cleaning factory, check out how they clean those cups.
	7. did they use green-energy？
---

# 2022-12-15 #
* The professor started by introducing us to how voting matters.
* computers doesnt really start third industrial revolution.
* roll out of electric vehicle is twice than combustion vehicle and the price of electric vehicle will be cheaper.
* I learnt that giving out electricity is not that easy and it requires a lot of planning and effort.
* Energy storage is the future.
* Learnt about peaker power plant that is on stanby when we need more power on peak times during the day.
* learnt that batteries have a lot of variation.
* Learnt about inequality that there is a poor distribution in money hence inequality exists.
---

# 2022-12-16 #
* Successful and unproductive
* i Felt successful because i passed my exams but i dont really feel productive as i didnt do anything.
* I will try to find more materials for my final presentation to be more productive.
---

# 2022-12-17 #
* Successful and Productive
* I felt successful because i catch up with a friend that i havent met in a long time and productive at the same time as i started to work on my finals presentation.
* I will try to continue my finals presentation and hopefully do up to half of it.
---

# 2022-12-18 #
* Unsuccessful and Unproductive
* I felt both unsuccessful and unproductive because i was lying in my bed all day as the weather is freezing cold and i was lazy to do anything
* I will try to take an early hot bath when i wake up to build up more energy to start my day.
---

# 2022-12-19 #
* Successful and unproductive
* I felt successful because i actually woke up early and take a hot bath as i mentioned yesterday but then i didnt do anything major today.
* I will try to be consistent in waking up early and maybe buy a few books since my finals is mostly about presentations and i have done almost everthing.
---

# 2022-12-20 #
* Unsuccessful and unproductive
* I felt unsuccessful because i failed my exam and spent the whole day sulking
* I will start revising for the finals so that i can pass the course
---

# 2022-12-21 #
* Successful and unproductive
* I still managed to find the topics to study and arranged it in a timetable but then i didnt do much rather than that
* I will start to follow the timetable i made

* 5 rules to be successful and Productive
* Set a timer: Because i am a person who oftenly loses track of time it will be helpful to have a timer to check your progress.
* Planning: It is very hard to get a thing to go on when there is no goal, hence planning it will help create the motivation to do it and be more productive.
* Have breaks: If we as a human keep on working 24/7 we would burn ourselves out and will lead to more problems.
* Don't multitask: It is really hard to do it as you are splitting your focus and eventually will finish the task longer.
* Don't be afraid of mistakes: we will not start as we are afraid it will go wrong, hence just do it, mistakes are normal.
---

# 2022-12-22 #
* Today's presentation about the course is very good and on time
* The professor made some interesting point about dressing to success
* I will try to improve how i dress to be better in the work place
* Successful and Productive
* I start to pick back up on my tasks and did some work
* I will keep on following the timetable i made
---

# 2022-12-23 #
* Successful and productive.
* Today I plan to go home, to my hometown Chiayi.
* I have a nice time watching Netflix and eating midnight snack with my family.
---

# 2022-12-24 #
* Successful and productive.
* Today is Christmas eve, and tomorrow is my father's birthday! 
* We have a great meal with my father's old friend. 
---

# 2022-12-25 #
* Successful and productive.
* I have a good sleep though the night.
* Heading back to Tainan, I enjoy the talks between me and my dad, we seldom have time like this.
* I spend a lot of time tiding up my space and our dorm, it makes my happy after seeing everything just alright.
* I post a post which is about my performance experience in jazz club, it was a prescious memory.
---

# 2022-12-26 #
* Successful and unproductive.
* In order to wake up early to study exam, I started to study in 6:30.
* I have a good spirit and I do great on the exam.
* I finish three final exam in a day, it was a longterm, but I conqure it.
* I do alot of task this day, and eventually end up with a great period of celebration meal with my band.
---

# 2022-12-27 #
* Successful and unproductive.
* I have noo class today, so I plan to study something about my guitar gigs, I feel great because it has been a long time that I had never touch things related to guitar.
* In the evening, I tried to find a place finishing my work, but library was terrible because I felt sleepy in that environment.
* Thought I didin't finish all my jobs, but I do push some progress, so I felt successful.
---

# 2022-12-28 #
* Unsuccessful and unproductive.
* i think i wiil screw up all of the important thing in my live.
* i feel so numb and stressful in this moment.
* i just want to get rid of this moment.
* Everything will be losing control.
---

# 2022-12-29 #
* The reason that we do the successful diary is to tell us, people on social media often shows bright side.
* Social media keep telling us we are bad, and not good, depressed and become not feeling successful.
* We are human, we can not be successful and positive everyday.
* Think about why we student take the course ?
---