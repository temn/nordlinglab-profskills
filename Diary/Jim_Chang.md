
This diary file is written by Jim Chang E84101159 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #

* The second course was great.
* After the course I realize the importance of English speech.
* It is important to respect the original author during the presentation,and always put on the sourse of the information or image.

# 2022-09-22 #

* I learned a lot this week.
* Note the English format in the report.
* We can tell if it is fake news with the skill of statistic.
* I thought I have globalview; however, world is always changing, and I should keep up with the latest information. 
*
In the future, 10 years from now, as a hydraulic engineer and environmental protection worker,
I will work and research on ecology and engineering. With the demand of developing green energy
and the awareness of environmental protection, the ecological environment in Taiwan is becoming 
better and better. We have successfully utilized the latest technologies to assist in our work
including: Internet of Things, Metaverse and Artificial Intelligence, Advanced Materials
And Advanced Medical. With their help, human beings have reached unprecedented levels of efficiency 
and have made our working lives longer, but I think that's a good thing. In this era there is no
more manual driving, the era of fully automated driving has come to make a significant decline in 
car accidents, but at the same time has led to many drivers losing their jobs. In fact, many 
traditional jobs have been replaced by robots, and low-skilled jobs have been replaced by robots, 
for example, in some service industries, but other jobs have also been added.
After all, all jobs are becoming more professional. The combination of the Internet of Things and the
original universe has brought people closer together and made shopping more convenient, but at the same
time we have also become indifferent and paranoid. Social lawmaking may have gradually failed to keep up 
with technological advances, and there are still many potential problems that need to be solved. 
The breakthroughs in advanced materials have directly led to a wave of revolution in the industry 
as a whole, and all engineering has become more potential and business opportunities in the future,
allowing more people to work in various fields. Advanced medical care has given me the confidence to 
build a better future without worrying about the future.
 I see a very bright future now, and although there are still many problems, it gives me more confidence 
 and courage to face them.



* Restaurant fire kills 17 in northeastern China 

* There are different descriptions of the characteristics of the locations
* Some references to other unrelated events
* Some pointed out other detail information

# 2022-09-29 #
* I'm happy to learn about finance this week ,and it is interesting.
* Bank is so much important than Ｉ thought. Many is totally controlby the bank, and it is scary.
* There are many things that I am confused about the theary.I will try to figure them out.


# 2022-10-06 #
* I believe that traditionally, men must be more assertive, so I took the initiative to arrange a date with my girlfriend.
* I was taught that work must contribute to the public, so I take speculative lightly.When friends are talking about stocks.
* I've been educated to support my Taiwanese players when I'm watching a game, even if I think the opponent is better looking.

# 2022-10-13 #
* This course was very inspiring for me, especially the benefit of exercise for the brain.
* I was always afraid of brain disease, it made me afraid that I would not be me anymore, and now I will do more exercise.

# 2022-10-27 #
* The topic this week is about depression and suicide.I have a friend who had the depression,and now she is recovered.
* I always treat her as usual like she didn't has the disease,and stay kind and open.I think it's a good way to get along with deppressed people.

# 2022-11-3 #
* I took a leave of absence from this week's class, I attended the school festival rehearsal and it was so hot outside, I was more interested in attending class.

# 2022-11-10 #
* today we talk about law
* I watch the video it talk about wahat is legalness what is common law and law about  photagraphy.
# 2022-11-17 #
* This week's lesson mentioned the problem of inattention due to excessive use of cell phones, 
* which reminded me that when I go out with my girlfriend she always puts her cell phone in her 
* backpack and only takes it out at certain times, which I think is a very smart approach.

# 2022-11-24 #

* This weekend finally to go home, from the beginning of the school year until now have not been home, I am so looking forward to.
* I feel as if I really have a cell phone addiction, I'm bad......


# 2022-12-01 #

* This week we discussed some of the actions we proposed and I think ours needs more adjustment to meet the demand.

# 2022-12-08#
* We had a debate today and I think our actions need more refinement.

# 2022-12-15 #
* successful and unproductive.
* Today was the orchestra's performance, and although I didn't study much, we did a great job.
* I have to rest tomorrow to prepare for the weekend classes.

# 2022-12-16 #
* successful and unproductive.
* Today's class was good, but I went to see a movie and practiced my instrument this afternoon, and didn't read much.
* Tomorrow I'm going to the library to study

# 2022-12-17 #
* unsuccessful and unproductive.
* I overslept today, which made me feel very bad and not in the mood to study.
* I have to go to class all day tomorrow and I'm really tired from weekend classes.

# 2022-12-18 #
* unsuccessful and unproductive.
* Today, I went to class all day and came back to the dorm late at night, I was so tired and upset.
* I want to take a break tomorrow and hope that everything will be better.」

# 2022-12-19 #
* unsuccessful and unproductive.
* I lost sleep last night, and I didn't have any energy in class today, and I didn't do well on the exam.
* I think it would be more efficient to find students to study together.

# 2022-12-20 #
* unsuccessful and unproductive.
* I had insomnia again last night and was dizzy all day.
* I should make my bed comfortable
 
# 2022-12-21 #
* unsuccessful and unproductive.
* I am so tired that my biological clock is broken
* I should learn about effective sleep methods


# 2022-12-22 #
* successful and productive.
* Today's learning situation is good and also learned how to tie a tie
* I should go to the library tomorrow to study

# 2022-12-29 #
* oh no I forgrt the diary
* succesful and productive.
* I did my presentation well today.
* Tomorrow I want to get up early and study before going to class.

# 2022-12-30 #
* unsuccessful and unproductive.
* I overslept today
* I went to the library to finish my homework.
* I should have set one more alarm clock.

# 2022-12-31 #
* successful and productive.
* Today is the last day of the year
* My classmates and I went to climb Meiling
* My feet are sore but I see a beautiful landscape
* Then we went to Guanziling
* I thought it was a good way to end the year with a hot spring.

# 2023-1-1 #
* unsuccessful and unproductive.
* Finals are coming up but my feet still hurt
* This seriously affects my reading efficiency
* I feel a bit regretful to go hiking

# 2023-1-1 #
* successful and productive.
* Today's studying efficiency is quite good
* But I'm still not making enough progress
* Maybe I should work out more in the new year

# 2023-1-2 #
* successful and productive
* I stayed in the dormitory today and study all day.
* I hope I can do well in tomorrow's exam

# 2023-1-3 #
* unsuccessful and unproductive.
* I did not do well on the exam
* I want to eat junk food to heal my body and mind
* This is unhealthy, I should find new ways to relieve stress

# 2023-1-5 #
* Today I watched a video about marriage, and I think it's very important 
* to know what is mentioned in the video, even though I already have a girlfriend. 
* I think it's better to get to know someone better than to reject them, because getting 
* to know someone better can always give us a lot of benefits and insights. Not to mention
* the fact that it is not easy to be loved.