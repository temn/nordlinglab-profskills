# 2024/09/19

At the start of today’s class, the teacher introduced the "Harmony View" and "Conflict View." I usually take the peaceful side, especially with strangers or casual friends. I tend to follow their ideas due to social anxiety. However, with family, close friends, or my partner, I’m more likely to stand firm on my opinions.
For example, if I want McDonald's and my boyfriend wants KFC, I might suggest we each get our own takeout to avoid conflict. In group conflicts, I try to find a compromise where both sides get part of what they want.
The teacher also talked about energy. With climate change, transitioning to renewable energy like solar and wind is crucial. These technologies are becoming cheaper, reducing carbon emissions and improving air quality. Clean energy will drive future growth, and we should actively engage in this shift.
Finally, the teacher emphasized keeping PPT simple and clear to highlight key points. I’ll apply this in my next presentation.

# 2024/09/26

3ways to spot a bad statistics

* Can you see uncertainty？
* Can I see myself in data？
* How was the data collected？

## Task

1.

In ten years, I hope my life will be filled with happiness and good health. I dream of having my own house and car, getting married to the person I love, and raising children together. I hope to have a stable job with enough income to live comfortably, and to occasionally travel abroad to explore the world. These are the motivations behind my hard work, and I believe that if I keep working hard over the next ten years, these dreams will come true. Additionally, I think the country’s economy may improve, which will bring me more opportunities and choices. I may choose to stay in Taiwan and pursue a career in the aviation or engineering industries. If the opportunity arises, I would also like to work in Europe or the United States.

Of course, there are a few current trends that might impact my life. First, the development of technology, especially in my field of work, will become increasingly important, and I will need to continuously learn new skills to stay competitive. Environmental trends will also influence my choices, as I may pay more attention to a sustainable, low-carbon lifestyle. Changes in the global economy will affect my income and quality of life; if the economy grows, I will have more financial freedom. With globalization, my workplace might become more international, which will positively impact my career development. Lastly, increased awareness of health will remind me to maintain a healthy lifestyle, allowing me to have the energy to achieve my dreams.

3.

* Ground News: Scientists at the University of Southampton are testing a new propulsion system that could use metals found on comets and moons to propel spacecraft indefinitely, potentially revolutionizing deep space travel
* Sky News: The new rocket thruster could enable humans to travel deeper into space, moving beyond current limitations and transforming science fiction into reality
* ITV News: The propulsion system could make long-term space exploration feasible by creating endless energy for rockets
* Phys.org: Deep space exploration might soon be more achievable with the development of this innovative rocket technology
* Wales Online: The technology could allow for space travel over vast distances, previously only imagined in science fiction
* Irish News: The breakthrough could mean humans embarking on never-ending space journeys
* University of Southampton: The propulsion system is being hailed as a potential game-changer for space travel, pushing the boundaries of human exploration

# 2024/10/17

Today professor discussed the financial system, focusing on the role of gold as a safe haven asset, the risks of high loan to deposit ratios, and how Chinese New Year traditions impact cash circulation. 
I found it insightful to learn about the concept of "fungibility" and how inflation continues to erode currency purchasing power. 
The growing wealth gap since the 1980 also raised important concerns. This lecture made me reflect on how inflation could shape our financial future and the need for effective economic policies.

# 2024/10/24

## Video Topic: What if we paid doctors to keep people healthy?
Claim:

The current healthcare system focuses primarily on post-illness treatment rather than disease prevention, which leads to unnecessary surgeries and an increase in chronic disease rates.

Testable Hypothesis: 

If the healthcare system shifts to a model that prioritizes maintaining patients' health, unnecessary surgeries and chronic disease rates can be significantly reduced.

Supporting Evidence:

* The video mentions the application of technology, such as DNA testing and miniaturized sensor technology, which can help detect health issues early, allowing individuals to take preventive health measures.
* Changing the basis for medical subsidies to link doctors' income to the health outcomes of their patients, rather than the number of surgeries performed, encourages doctors to adopt preventive measures, reducing unnecessary procedures and the incidence of chronic illness.

## Three rules for how to live healthy

1.Regular Health Check-ups and Monitoring

The video highlights the use of technologies like DNA testing and miniaturized sensors that can help detect disease risks early, promoting the implementation of preventive measures and reducing long-term illness occurrences.

2.Maintain Regular Exercise

Research has shown that regular exercise helps improve cardiovascular health, reduce stress, strengthen the immune system, and protect brain health. These benefits contribute to the prevention of chronic diseases, as mentioned by experts in the video, supporting a healthier lifestyle in the long term.

3.Balanced Diet and Proper Nutrition

A healthy diet is an essential part of disease prevention. Consuming sufficient vegetables, fruits, whole grains, and lean meats can reduce the risk of chronic illnesses. The video emphasizes that a prevention-focused healthcare model encourages healthy lifestyle choices, thereby decreasing the occurrence of long-term diseases.

# 2024/11/07

This class made me deeply aware of the significance of mental health issues. After watching the TED Talks, I learned to observe and listen more attentively, understanding that I don’t always need to offer comforting words; sometimes, simple presence and understanding are powerful enough.
I also recognized the importance of self-care, as continuously absorbing others’ negative emotions can impact me, and it is wise to seek professional support when necessary.Overall, this experience reminded me to care for others while also learning to protect and take care of myself.

# 2024/11/28
At the beginning of today’s class, the teacher asked us to put our phones on the podium, which meant no phones for the next three hours. It was so hard for me! Since my English is not very good, I often couldn’t understand what the teacher was saying, and without my phone, I couldn’t use any translation tools to help me. This made me feel really helpless.

During the group project presentations, one group’s idea impressed me a lot. They designed a watch alarm that you can wear on your wrist to wake you up easily. I think this idea is super practical, especially for people who struggle to get out of bed in the morning.

However, what stresses me out the most is having to give presentations. I really don’t want to present! Speaking in English in front of everyone is way too stressful for me. Just the thought of presenting makes my palms sweat, and I even feel like I can’t stand up sometimes. I really hope this semester can pass smoothly, and it would be the best if I didn’t have to present at all. That would be the luckiest thing for me!

# 2024/12/05

This class introduced planetary boundaries, and what struck me most was the topic of novel entities. Human-made materials, like plastics, do not decompose naturally, causing ecosystems to struggle to adapt.

Plastics break down into microplastics, which enter oceans, soil, and even our food. While technology brings convenience, we must recognize its environmental risks and avoid ignoring the consequences of innovation.

This class reminded me that humanity and nature are interconnected, and we must focus on reducing the environmental impact of new materials.

# 2024/12/12

This week’s class focused on the topic of planetary boundaries. Through group debates, we discussed various action plans and explored ways to protect our planet. Each group proposed different strategies, exchanged ideas during the Q&A session, and finally voted to select the most impactful solution.

This process made me realize that when we collaborate and discuss toward a common goal, we can often generate more concrete and meaningful solutions.

## Friday - 12/20/2024

A. Productive and Normal Day
B. It was a regular workday. Nothing special, just completed my normal job tasks.
C. Continue maintaining steady progress at work.

## Saturday - 12/21/2024

A. Productive and Successful
B. I completed packing my luggage for returning home and worked a regular shift at my job. Felt good to check off the tasks I set for myself.
C. Keep this productive mindset going forward.

## Sunday - 12/22/2024

A. Productive and Successful
B. It was a holiday. I successfully bought snacks for the team-building event and organized my clothes as planned.
C. Plan ahead to make good use of my holidays in the future.

## Monday - 12/23/2024

A. Relaxed and Productive
B. Slept in and felt well-rested. Left in the afternoon to attend the team-building event.
C. Aim to balance relaxation and productivity better.

## Tuesday - 12/24/2024

A. Productive and Successful
B. After finishing work, I completed all my laundry, dried and folded it as planned. This made me feel accomplished.
C. Keep staying on top of chores after work.

## Wednesday - 12/25/2024

A. Productive and Successful
B. Cleaned my room in the morning, worked my usual shift in the evening, and double-checked my luggage after work. Everything was done on time.
C. Build a habit of double-checking tasks to ensure nothing is missed.

## Thursday - 12/26/2024

A. Productive but Slightly Disrupted
B. Left for the airport at 9 a.m. by train and managed my time perfectly. However, my flight was delayed, which caused some inconvenience.
C. Be prepared for unexpected delays when traveling in the future.

## Saturday - 12/27/2024

A. Productive and Relaxing
B. Traveled to Ipoh today. Although the journey took 4-5 hours, traffic wasn’t too bad. In the evening, attended my brother’s pre-wedding party, which was a lot of fun.
C. Be mindful of planning travel breaks to avoid fatigue.

## Sunday - 12/28/2024

A. Joyful and Memorable
B. Today was my brother’s wedding day! We started early to witness the bride-picking ceremony, and it was so heartwarming. The weather in Malaysia was hot, but the joy of the day made it bearable. In the evening, we attended the wedding banquet, and everyone dressed up beautifully. It was a very happy day.
C. Cherish more family moments like this in the future.

## Monday - 12/29/2024

A. Productive but Exhausting
B. Packed up in the morning and began our journey back home. It was the last day of Malaysia’s public holiday, so traffic was heavy, and we only got back home in the evening. After dinner at a mamak stall, I packed my luggage for tomorrow’s trip.
C. Consider starting trips earlier to avoid peak traffic.

## Tuesday - 12/30/2024

A. Slightly Disrupted but Exciting
B. Left for the airport at midnight with my boyfriend and best friend. We flew back to Taiwan, though the flight was delayed by 20-30 minutes. After landing in Kaohsiung, we rushed to Tainan to prepare for our trip to Taipei later at night.
C. Allow more buffer time for unexpected delays during travel.

## Wednesday - 12/31/2024

A. Exhausting but Thrilling
B. Took the bus to Taipei at 2 a.m., arriving after 4.5 hours. Although we were tired, we started our “special forces” travel day, exploring Taipei nonstop. In the evening, we went to the Taipei 101 area for the countdown and fireworks. The crowd was massive, but the experience was worth it.
C. Plan rest breaks better for intense travel days like this.

## Thursday - 1/1/2025

A. Memorable but Challenging
B. After seeing the fireworks, we returned home at around 2-3 a.m., but woke up at 4:30 a.m. to head to Yilan to see the first sunrise of the year. Unfortunately, it was cloudy. We then went to Jiufen and Shifen, enjoying food, sightseeing, and even releasing sky lanterns despite the drizzle. In the evening, we decided to take the high-speed rail back to Tainan. It was crowded, and we had to stand initially, but eventually got seats. Almost missed our stop in Tainan, but luckily my friend reminded us just in time!
C. Always double-check schedules and prepare for unexpected situations, like crowded trains or tight timing.

# 2025/1/2

Since I started studying abroad, I’ve been striving to grow and improve myself. Throughout this journey, I have learned the importance of independence, resilience, and self-discipline. Balancing work, studies, and personal development hasn’t been easy, but I’ve come to realize that every challenge is an opportunity to become stronger. My ultimate goal is to contribute positively to the world while staying true to who I am.

1. Chen Shu-chu

Chen Shu-chu is a vegetable seller from Taiwan who donated over NT$10 million to help those in need. Despite her humble background, she persistently worked hard and lived frugally to support others. Her success comes from her selflessness and commitment to helping others.

2. Dato' Azhari

Dato' Azhari, a renowned Malaysian diver, has contributed significantly to marine conservation. He leads efforts to clean up the ocean and restore coral reefs. His achievements are driven by his love for nature and consistent action.

These individuals inspire me to stay focused on my goals and contribute positively to society.