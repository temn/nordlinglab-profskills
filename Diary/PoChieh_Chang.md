
This diary file is written by PoChieh Chang E14116859 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-19 #

* This week we learned about the founder of linux. 
* I learned about some formating techniques in presentations, such as using the correct frame for the ppt and understading the importance of understanding the units in a graph
* We watched a TedTalk on wheather or not the world is deteriorating over time, the lecture stated that if we also compare the worst of today to the best of the past we will always be disappointed
* If we look into data we will see that nowadays there are less wars, less homicide, less work load, more democracy.
* Progress is a fact of human history it may be slow and inconspicuous but it is undeniable.

# 2024-09-26 #

* Fake news
* Some times being neutral is to be a accomplice
* Previously fake news was saying that all parties in conflicts were equal
* Danger of more unverified news on social media
* Social media news may cause tunnel vision due to so much information and choice
* Clickbait causes fake news
* News quality may suffer from amateur journalists
* Russian financed propaganda

* Statistics
* 1. How to spot bad polls
* can you see uncertainty In polls
* People may lie
* Some groups may not be realized

* 2. Visualization is important
* Can you see yourself represented in data

* 3. How was data collected
* Definition of the question is important
* May be trolls
* Sample size may be too small
* Is the sampler biased

* News Story: Hong Kong Plans to Cut Tax on Liquor in Bid to Revive Nightlife
* Left: 
* Hong Kong is expected to cut taxes on alcohol in an attempt to revive its flagging tourism industry
* Hong Kong also faces competition from other big east Asian cities, notably in Japan, where the weak yen is attracting visitors, Bloomberg reported, while mainland China’s economic slowdown is reducing domestic tourism.

* Right:
* Reeling under the declining number of tourists since the Covid-19 pandemic, Hong Kong is planning to make spirits cheaper in its bid to regain its edge as a premier destination for nightlife, dining and shopping.
*  Hong Kong is also facing tough challenges from metropolises from mainland China, Singapore and Japanese cities, which have seen a tourism boom with the yen weakening considerably.

* center: 
* At the same time, the city is under increasing pressure to regain its relevance as a travel and shopping hub as it faces rising competition from metropolises in mainland China, as well as Singapore and Japan, where there is a travel boom thanks to the weak yen.

* conclusion 
* not a lot of difference in narrative for this story between left, right, and center leaning sources. 

* My future,
The future is uncertain and shrouded in mystery. But if I were to make a guess, I would say ten years from now I would be living in America. I would probably be an engineer, however if I’m fortunate I will be retired and will be free to do whatever I want. Regardless I will probably have a cat as company and play video games for entertainment. I have no high hopes for my future besides being able to afford and live a simple life.
I believe this will likely be my future in ten years because it includes nothing highly unachievable. 
The present is the path to the future, I am currently pursuing a degree in mechanical engineering. This will likely lead to a job in engineering. 
The next thing that will probably have a substantial impact on my future life is my habit of investing extra money in the stock market, though the amount I invest is not major, over a ten-year period it will likely make my life slightly easier down the line. 
As a 21-year-old student I spend most of my time studying with some minor investments, so there are currently no other trends that could have a huge impact on my future. 

# 2024-10-17 #
* This class we learned about finances and how money works and what gives it its value, how it came into existance and what affects it value.
* We watched a video on how financial aspects like loans and printing new currency works.
* It is found that more mmoney circulates during Chinese New Year

# 2024-10-24
* This class we watched some videos about hate speech, benefits of exercise, and helth care 

* For hate speech we watched a Ted Talk about a man from a immigrant family which joined the neo nazi movement when he was 14 years-old. He believes not knowing his
place in life led him to join the movement. He said when he 19 he met a woman and had a child which made he rethink his life. He decided to open a music store which 
sold 75% white power music. However, later down the line he decided to stop selling the racist music because he connected with some black and gay custmoers. Unfortunally 
when he got rid of the racist music the store couldn't support itself, which resulted in him hitting an all time low in life. Thankfully, he got a job at IBM for installing 
computers. Later on in his corrected life he started to face his past by confronting some of the previous people he hurt, and even went on to help others connect with 
some of the people they hate.

* Students of the class also shared some hate stories between China and Taiwan.

* A video sharing a research on exercise and effects on the brain showed that after exercising your mood instantly gets better and better attention span.

* Another video also shared a story about going to the dentist and got recommended the most expensive surgery treatment. After research he found out that most doctors
recommend the more expensive treatment methods due to profit. Therefore the speaker believes that we should shift our focus towards preventative healthcare and not trearment
based healthcare.

* How to live healthier: 
1. Eat less fried food, fried food can lead to higher rates of type 2 diabeties and heart diseases.  
2. Eat more fruits and vegetables, helps protect against cancer, diabetes and heart diseases.
3. Eat less sugar, excess amount of sugar leads to tooth decay, high blood pressure and diabeties...


