This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-17 #

* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation 

This diary file is written by Chunyu Chen E24134281 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-19

* This is my first lesson and everything is in a mess. 
* As I miss a lesson, I am unfamiliare with the class rules.
* From others presentation, I've realized the importance of the layout.
* From the Ted talk, the unexpected death rate is dropping, people is earning more money, the pollution is getting worse.
* The rappid development brings crisis and opportunity to humanity. 
* I am going to work with guys who seem responsible. I hope we can present well next week.

# 2024-09-26

### 3 ways to spot a bad news

* See uncertainty
* See myself in data
* How was the data collected

### Summarise the different views in "Earth will have a temporary ‘mini moon’ for 2 months. Here’s what’s happening"	

*  "2024 PT5" is introduced as a mini moon linked to India's Mahabharata, with a diameter of ten meters, set to exit orbit in 53 days.
* Highlights that the asteroid is about 10 meters wide and was first spotted by astronomers in August, emphasizing its orbit duration of roughly 57 days.
* Specifies that the mini moon will orbit Earth for nearly two months, returning to the Arjuna asteroid belt, and mentions it won’t be visible to the naked eye.
* Mentions the size of "2024 PT5" as roughly 33 feet and reiterates the orbit duration of 57 days before its departure on November 25.
* Refers to the asteroid as "Loki," indicating it will act as a temporary mini moon before being captured by the Sun's gravity on November 25.
* Restates the size of the asteroid as a school bus-sized object, reinforcing that it will become a mini moon for about two months starting September 29.

### What I think my future will look like

* As a freshman major in Electrical Engineering in NCKU, I have imagined about my future for many times.  Ten years later, after graduating from the college and the graduate school, I might be working in the company in the Electronics industry, like ADR(TSM) and MTK. Where I will be busy working, but receive high payment. At that time, I will rent a house near the company and live on my own. In the room, there might be a big screen for me to watch videos and all kinds of snacks are pilled up, where I can enjoy the leisure time and relax myself.

* To fulfil my dream, I need ten years or even more time. Above all, I need to graduate from college successfully and apply to a good graduate school. Without a doubt, a solid foundation of professional knowledge is the fundamental condition for future employment. Then, I need to accumulate work experience for more than two years, which is the time for me to be familiar with the industry. After a long experience, I can finally realized my long-cherished dream.

* However, in this era that technology develops rapidly, things may change dramatically in the next decade. Varies of facts may effect my future. 
* Firstly, my schoolwork. As professional knowledge becomes more and more difficult, I will face greater challenges. In other words, I am not sure whether I can master in this field or not.
* Secondly, as competition intensifies, can Taiwan maintain its advantage in the semiconductor industry is still in doubt.
* Thirdly, with the rapid development of AI technology, human’s jobs are likely to be replaced. The only solution to survive in this highly competitive environment is to be creative and be at the forefront of the trend.
* Fourthly, the international situation. Since Ukraine and Russia started a war two years ago, and war continues in the Middle East, food prices are rising, shipping costs are rising, and prices of industrial raw materials are rising. The whole world is effected. Obviously, Taiwan's industries are affected inevitably.
* Last but not least, as relations between China and Taiwan are increasingly tense, we, as Taiwanese are at the center of the storm. No one can predict what will happen to the land we love so much.

* All in all, I hope the war will end as soon as possible and the world will remain peaceful. I hope Taiwan can be stronger and stronger. And I hope I can be the one I expected ten years later.


# 2024-10-03

* Today's class is canceled because of typhoon
* I will still do some research about fiancial

# 2024-10-17
* Today's lesson is about financial.
* We talk about what is money and understand the operation of bank.
* We also discuss about where bitcon can replace TWD, and my answer is no.
* I think this class can help me manage my money someday in the future.

# 2024-10-24
* At the begining of the class, we listened to student present on the economic development of their respective countries (like Taiwan and Indonesia).
* We watch videos talking abou why does extremism flourish.
* I learned someting from the video:
* 1. People believe in stories. Whether we realize it or not, narratives shape our perceptions and reactions to the world around us. We are easily influenced by money, religion, and politics, which can lead us to make judgments that may not necessarily reflect reality. These narratives often create a lens through which we view events and people, sometimes distorting the truth. It’s important, then, to critically examine the stories we encounter and understand their impact on our decision-making process.
* 2. Ppeople often find themselves afraid of what they do not understand or are unfamiliar with. This fear can be paralyzing and lead to misunderstandings or conflicts. To combat this, we need to foster open dialogues, encouraging each other to share our thoughts and experiences. By engaging in conversations, we can demystify the unknown and build a sense of community and trust. When we take the time to listen to one another, we can bridge the gaps in our knowledge and reduce fear of the unfamiliar.
* 3. We must learn to look at things rationally and not be influenced by extreme remarks or sensationalism. In today’s fast-paced world, it’s all too easy to get swept up in powerful emotions or the loudest voices. However, a rational approach involves taking a step back, assessing the evidence, and considering multiple perspectives before arriving at a conclusion. This practice of critical thinking will not only lead to better decision-making but also promote a more balanced understanding of complex issues.
* 4. I have learned about the difference between knowledge and information: Knowledge is something that we believe to be true, often acquired through experience, education, or critical reflection. Conversely, information can be merely data or facts presented without context or understanding. It’s essential to differentiate between the two, as knowledge has the power to inform our beliefs and shape our actions, while information without critical thinking can lead to misunderstanding and misinformation. In a world rich with data, cultivating knowledge through thoughtful consideration and dialogue is vital for personal growth and informed decision-making.

#### How to live healthy
* Go to sleep before twelve aclock everyday and sleep more than 7 hours.
* Eat healthy diet include meat or tofu (protein), vegitable and few rice.
* Drink enough water and drink warm water.

# 2024-11-07
* In today's class, we watched a compelling lecture on depression that provided deep insights into this challenging mental health issue. The speaker emphasized that, more than anything else, one of the most vital aspects of supporting individuals who are struggling with depression is to help them feel valued and needed. It's crucial for them to experience a sense of purpose, as this can significantly impact their outlook on life.
* Furthermore, the lecture highlighted the importance of distraction as a therapeutic tool. Engaging in activities, pursuing hobbies, or simply enjoying moments with friends can help lift the heavy veil of sadness that often accompanies depression. I found it inspiring to think about how small, thoughtful gestures can make such a difference in someone's life, reminding them they are not alone in their struggles.
* While I am eager to help those who are dealing with depression, I also realize the importance of setting boundaries for myself. As much as I want to provide support, I must prioritize my own mental and emotional well-being. It's essential to remember that I'm not a doctor or a trained therapist, so while I can offer compassion and understanding, I must also recognize my limits. 
* This understanding has led me to reflect on the value of my own life. I should cherish every moment and the connections I have with others. Life is precious, and being aware of this can inspire me to live more fully while also being there for those in need. Ultimately, it's a balance of offering support while nurturing my own mental health, allowing me to be a better friend and ally to those around me.

# 2024-11-14
* At the beginning of the class, we listened to a presentation on how to support friends with depression.  
* Fluffy pets can be a great source of comfort, making people feel good and relaxed.  
* It is also important to set boundaries and practice self-care while helping others.  
* Additionally, I learned about renewable energy during a debate with my classmates about nuclear energy versus wind energy.  
* We watched a talk that encouraged us to reflect on whether we truly understand the reasons behind our actions.  
* The talk emphasized that we often rationalize our decisions after the fact instead of genuinely understanding them.  

# 2024-11-21
* I havr learned someting about rules and freedom:
* If you are alone in the world, then no rules are needed. If it is very crowded, then the survival requires rules.
* Rules are needed to define where one persons freedom ends and another persons start.
* It is hard to reach a consensus.

### Claims and Arguments in the video
* Social software platforms like Facebook utilize intricate punishment and reward systems to manipulate user behavior, encouraging engagement through likes, shares, and comments.
* We are often treated as products by social software companies, with our data and interactions being sold to advertisers for profit, raising ethical concerns about privacy.
* The era of unquestioningly trusting big data and algorithms should come to an end, as blind faith can lead to a loss of autonomy and conformity.
* Algorithms are not neutral; they can perpetuate existing biases found in the data they process, shaping public opinion and societal norms in unintended ways.
* The focus on user engagement without considering the qualitative aspects or potential harms can lead to ethical oversights, prioritizing profit over user well-being.
* There are significant profits to be made from exploiting algorithmic biases, creating a cycle where companies benefit financially from reinforcing narrow viewpoints or societal divisions.
* Users must remain critical and aware of the systems governing their interactions, advocating for transparency and accountability in social software.
* Promoting greater ethical standards in the design and implementation of social software can lead to a more equitable digital landscape that respects individual rights and fosters meaningful connections.

### rules to protect freedom
* Legally, we need to include the spread of false news and the sale of personal information.
* We can set up an independent agency to identify false news and promote education to enable people to have independent thinking and judgment.

# 2024-11-28
* In today's lesson, we listened to presentations on final projects. 
* I am particularly interested in the plastic bag reuse project, which aims to help reduce pollution. 
* However, I don't think the project is implementable, as it can be troublesome for people to donate used plastic bags. As a user of plastic bags, I also worry about what the previous user might have put in the bag, perhaps their dinner or something smelly. 
* Additionally, the professor collected our cell phones during this class, and I miss my phone very much. Without it, I cannot take notes.

# 2024-12-05
* In today's class, we listened to group reports on news from various countries. 
* I learned that even when the same issue affects multiple countries, there can be different or even opposing viewpoints. This variation can depend on cultural, historical, and political contexts. Engaging with global news helps to expand our horizons and understand the complexities of international relations. Realizing that news is often not black and white is crucial. 
* To navigate this complexity, we should focus on developing critical thinking skills. This involves questioning sources, identifying bias, and understanding the motivations behind the information presented. Consistently reading and following reputable news sources is vital. Being informed enables us to form well-rounded opinions and contribute meaningfully to discussions about global events. 
* Additionally, learning to analyze the information we receive, looking for corroborating evidence, and recognizing differing viewpoints helps us grasp the full picture of any situation. 
* Ultimately, it’s essential to develop our own opinions based on thorough research and analysis, rather than accepting information at face value. This empowers us to engage thoughtfully in discussions and understand the nuances of international topics. 

# 2024-12-12
* In today's lesson, we discussed private ways to reduce carbon dioxide emissions. 
* To my surprise, one of the most effective strategies is to have one fewer child.
* From our final presentation, I learned that using a Work Breakdown Structure (WBS) is an effective approach to achieving our goals. 
* However, I previously misunderstood that the WBS should include results, not actions.
* Additionally, we had a discussion about our final project. 
* During this discussion, we identified many details that we had previously overlooked.

# How I feel everyday

### 2024/12/19(Thursday)
* unsuccessful and productive
* I feel dizzy in morning classes, so my learning efficiency is low. But I stayed up late studying and reviewing a lot of course content.
* Sleepe earlier or drink some coffee.

### 2024/12/20(Friday)
* successful and unproductive
* I have been tired all week, so I take a rest today, also I did well in today's calculus exam.
* Get enough sleep and make plan.

### 2024/12/21(Saturday)
* successful and productive
* I sleep for the whole day and write code in the evening. A good rest is needed.
* Get up early.
### 2024/12/22(Sunday)
* successufl and productive
* I prepared for the English final prasentation and studied Physics. DDL is the best motivation for me to work.
* Go to library.
### 2024/12/23(Monday)
* unsuccessful and prouctive
* I wrote the Chinese final essay and studied caculus. It is five a.m. now, there is less than five hours to sleep. 
* Don't eat too full before studying.
### 2024/12/24(Tuesday)
* unsuccessful and productive
* I pass the physics midterm exam and studied calculus. There is still many things to study, it is four a.m. now, I am dying.
* Take a nap when I am tired.
### 2024/12/25(Wednesday)
* unsuccessful and productive
* I went to the library and studied in calculus for 6 hours, also I prepare for the presentation tomorrow. There is still much things to do.
* Go to sleep earlier and be relax.
### Five rules make me more successful and productive
* Get enough sleep.
* GO to Library to study instead of the dormitory.
* Take a nap when I'm really tired.
* Don't eat too much before studiying.
* Do tasks before DDL.

### 2024/12/26(Thursday)
* successful and productive
* I made two presentations today and prepared for the calculus exam tomorrow. I am going to sleep earlier.
* Be relax before an important exam or report

### 2024/12/27(Friday)
* successful and unproductive
* I go to the club  and watched a movie in the evening. I am doing something I enjoy instead of schoolwork.
* Get up early and start to prepare for the final exam earlier.

### 2024/12/28(Saturday)
* unsuccessful and unproductive
* I slept until noon and then went out to eat delicious food with my friends. I didn’t study at all today.
* Make a todo list.

### 2024/12/29(Sunday)
* unsuccessufl and productive
* I stayed in the dormitory and studied for the whole day, it's a productive day. But it is now 2 a.m., I am staying up late again.
* Go to sleep earlier.

### 2024/12/30(Monday)
* successful and unprouctive
* I don't have class today, so I get up late and miss the launch time, restaurants are resting. I discuss the group work with my friends in the evening.
* Have three meals each day.

### 2024/12/31(Tuesday)
* successful and productive
* I finish one final exam today. Also, I celebrated New Year's Eve with my friends, counting down together and enjoying a wonderful night.
* Every day is a new beginning, keep a happy mood.

### 2025/1/01(Wednesday)
* successful and productive
* This is the first day of the new year, I am energetic to prepare for final exam. And I have a big meal in the evening.
* Foods make people happy.

# 2025/1/02
Tobey Chen, a passionate IC engineer and traveler, passed away peacefully at the age of 99, leaving behind a legacy of inspiration, resilience, and adventure. Throughout her life, she was known for her unwavering work ethic and determination, traits that propelled her to remarkable success in her career as an IC engineer. With her sharp intellect and innovative spirit, she made significant contributions to the field, earning both wealth and recognition that few could match.

By the age of 40, after establishing herself as a leading figure in the industry, Tobey decided to retire comfortably, a choice she made to reclaim her time and pursue her long-held passions. This moment of transition marked the beginning of a new chapter in her life, one that was characterized by a deep-seated urge to explore the world around her.

Post-retirement, Tobey devoted herself to her love for travel. Over the years, she visited more than 50 countries, from the majestic landscapes of Patagonia to the bustling streets of Tokyo. Her journeys took her to hundreds of cities, where she immersed herself in diverse cultures, tasting local cuisines, participating in traditional festivals, and engaging with the people she met along the way. Each trip added a new layer to her understanding of the world, enriching her life with unforgettable memories and stories she cherished for years.

Tobey Chen will be remembered as someone who worked hard, lived fully, and embraced life on her own terms. Her friends and admirers carry forward her spirit, celebrating her legacy of courage, curiosity, and compassion. She taught those around her the importance of pursuing one’s passions and the joys of exploration, reminding everyone that life is meant to be lived to the fullest. As they reflect on her remarkable journey, they find solace in knowing that Tobey left an indelible mark on the world and in the hearts of everyone she encountered.

# Course feedback
#### a.First thing come to mind
* It is a great class that allows me to explore the world and learn a lot about economics, renewable energy,  physical and mental health and so on.

#### b.Five things I like most
* I like to watch TED videos as I can learn some professional things.
* I like to listen to the presentations of my classmates, which motivates me to do a better job.
* I like to listen to the discussion between students and the teacher. As we are all majoring in different subjects,  we have different opinions.  And I can feel that some of my classmates have a much wider vision and experience than I do.
* I liked the final project because we were doing something to help others.
* I like the form that we discuss the final project in which we have a representative on the stage and the rest of us ask questions, it gives me lots of ideas.

#### c.Five things I dislike most
* I didn't like the final exam since I didn't do well.
* We don't have a fixed group member, so I need to learn how to communicate with others.
* We need to prepare presentations and write a diary every week; that's a lot of work.
* I need to use English in class, and even while I am working with my group members, it is a little bit hard for me.
* I'm getting a low grade because I seldom do the presentation, so sad!

#### d.One sentence I will tell other students
* You can learn a lot and practice English while taking the course.

#### e.Lcture needs improvement
* The lecture about the fake news. Maybe we can add a game to distwinguish wheather it is a fake news or not.

# Course objectives
* 1.Instruct students to gather information about population from various websites and conduct a comparative analysis. They should compare the population structure of different continents, such as Europe and Asia, and make a presentation.
* 2.Let students take a product of a company as an example to analyze the impact of demographics on the functional design and sales of this product.
* 3.Let students make a simple modle about planetary boundaries and the current state on the computer.
* 4.Organize a Model United Nations and let students serve as representatives of various countries to discuss and propose economic policies to address resource inequality.
* 5.Host a guest lecture by an AI or futurism expert to discuss emerging technologies and their societal implications.
* 6.Let students searcch more information on the Internet and write a report about it.
* 7.Organize a hackathon where students develop simple engineering solutions or apps promoting health and well-being.
* 8.Wach a movie about depression or other mental health issue.
* 9.Have students work in groups to interview parents and other elders with marriage experience and compare with the optimal algorithm.
* 10.Hold a fake news competition to let students distinguish fake news.
* 11.Assign homwork about basic analysis and find tutorial videos online so students can study themselves and complete it independently.
* 12.Divide the students into small groups to address real-world problems and present their solutions to the class.
* 13.Arrange alumni to share their stories about their success and social skills.
* 14.Organize visits to various companies to help students engage with employees and learn about the influence of different corporate cultures.