This diary file is written by Ada Grundström P56118414 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #

* I think collaborative and problem based learning will work well for this kind of course.
* The exam didn't really make sense to me and contained mostly trivia questions. I'm curious how the course will teach about those subjects.
* I felt that the first lecture didn't really teach me anything yet but maybe it's because all the introduction and stuff.

# 2022-09-15 #

* GIT is a new thing for me in this form. Seems quite convenient but I haven't yet understood how is it better compared to i.e. drive.
* It might take a while before fully being able to make good use of all the GIT features.
* I noticed that we didn't have time for over half of the presentations.
    * Could we be divided into two or three groups for the presentations so that everyone would have time to present if that was the purpose?
* The list of videos seems interesting. I hope I can find time to look some of them.

# 2022-09-22 #

* I didn't know that we were supposed to add the diary entry already on Sunday. Hopefully it doesn't matter that I was late last week.
* Fake news and fact checking is an interesting and very important topic to learn.
* The videos were great.

Homework:

_In your Diary for this week, write a 400-600 word summary of what you think your future will look like and why it will be so 10 years from now. Start by defining where you will live and what work you will do. Then describe the five current trends of change that you think will affect you the most and their effect on your life 10 years from now._


In 10 years from now I will be living in one of the largest cities in Finland in a rowhouse apartment with my family. I work four days a week in a modern company as a project manager in the field of sustainable development and enjoy my job.

Climate crisis has shown significant effects all over the world. In the North it’s not that much about heat and drought but around the world so many places have become uninhabitable that the migration of people to more habitable areas is enormous. That has also affected the cultural diversity in my city, and, for example, many customer service jobs require more wide-ranging language skills. At least, humanity finally starts to realise the threats of climate crisis. Sustainability has become a requirement, in some cases even a priority, in working life basically also offering me my job. At the same time, I also must fear for the future of my children in a rapidly changing world.

Valuing the well-being and slow-paced life has increased. Even though I’m working as a project manager, I have four days working week and work doesn’t follow me to home. When I go to the office, I usually take a bike and my employer offers free breakfast to all who travel by bike or foot. Due to having shorter working week and a good working environment I have more time and energy to be with my family and friends and my life is enjoyable.

Due to acceleration of digital transformation and several social distancing periods caused by a couple of epidemics/pandemics remote work is already more a norm than an exception. I also work approximately two days a week from home office which gives me more time to do other things when cutting the time of traveling to work.

My family doesn’t have a car because we live in a city where public transport works very well. Sometimes when we want to travel further away, we use the shared car of our rowhouse’s residents. That saves us a lot of money to use in other purposes. Other shared items have also become more popular. We have a tool storeroom in one end of the rowhouse where anyone of the residents can borrow for example a hammer drill for renovating. Almost all the vehicles are also electrical, and you can rarely see a combustion engine car anymore. So is the shared car in our rowhouse. 



_Select one news story published on https://ground.news/  and read all the different sources. Summarise the different views in the sources in bullet point format in your diary._

* I chose Arizona abortion ruling will set women back 'more than a century' (https://ground.news/article/arizona-abortion-ruling-will-set-women-back-more-than-a-century)
* There are 12 sources that have mostly been categorized to “lean left”, “center” and “right”.
* It is hard to tell the subtle differences in texts since the language is very complex and advanced with many words I don’t know.
* Also, all of the articles directly quote the same speech quite much, so it doesn’t give room for that many different interpretations.
* It seems like the right leaning sources quote mostly just Karine Jean-Pierre and not others whereas the left leaning sources have also other quotes from different people.

# 2022-10-03 #

* Ughh, forgot to write the diary before Sunday night... :(
* Anyway, I find the topic of the last lecture was interesting
* Still I had difficulties to concentrate, though. Three hour lecture is just too much.
* The videos were certainly very informative but I go along with the comment of someone in the lecture that even though understanding most of the words, I couldn't catch the idea what that one man was saying on the video.
* Economics is difficult and complicated.

# 2022-10-06 #


diary:

* Sooo, I got to present the group work presentation in front of the class this week because apparently the sound was too quiet in the video :--)
* It was quite okay otherwise, but I hadn’t really prepared to present it live so it wasn’t as compact presentation as it could have been.
* It also made me a bit mad that I had skipped some other important things the night before just to film the video and then we didn’t even watch it, but I had to present it live instead.
* I don’t find that grilling someone on the stage about other group members’ work is very nice but on the other hand I understand that that kind of situations are very likely to happen in the working life so I’m not as indignant as I could be.
	* I can only imagine how some shyer people are terrified of having to be grilled there in front of the class some day. I would also have been still a couple of years ago.
* I’d like to actually learn how money and finance work and these two previous lectures have been a good peek into it but way too little to understand anything in any deeper level.
* The video of the guy who had left the neo-Nazi movement was very interesting and I could find many familiar ways of thinking from his speech, i.e. the will to understand other people’s ways of thinking and gradually starting to fill in the potholes in there.
* I also very much agree and have noticed that narrow-mindedness, racism, and movements related to them often spring from fear of unfamiliar things, lack of knowledge and a desire to belong to something.


homework task:

* I don’t quite understand the first task of this week - fictional story?? Let's try to still say something.
* Currency naturally affects my life quite often since being here in Taiwan and my money and bank being in Finland. Every time I need to withdraw some cash I have to pay quite big sums for the banks to do the currency exchange so whenever I need some more cash I try to take as much at a time as I dare so the one-time payment is distributed to a bigger sum of money.
* I don’t really believe in any gods but religion still affects my life in different ways. Last weekend I met a volunteer of a temple and she told me a lot about the local Taiwanese religion.
* Career is something many students have in mind while for example sacrificing a good night sleep for schoolwork. I have tried to distance myself a little bit from the career-centred thinking and have for example taken more time for my university studies, but in the society where we are living, it is quite difficult.

# 2022-10-30 #

* Apparently my Oct 6th diary still hasn’t been merged here from the branch that it created so I’ll just put it here again.
* This week’s topic was depression which was a bit rough but very interesting and important at the same time.
* I was glad that this topic was handled in an online lecture. It would have been harder in class even though discussion would have been nice.
* I think it’s extremely important to talk about different mental conditions to help people understand better what other people might be going through and to gain knowledge on how to connect with them.
* It was nice and interesting that the videos were from both directions, the suicidal and the helper.
* I'm wondering how the group project will turn out. Seemed to be quite weird but could turn interesting if we find a good topic.

# 2022-11-10 #

* I realized that I again forgot to write the diary last week... The Sunday deadline always causes me trouble.
* This week I couldn't attend the class because I was presenting my home university at Study Abroad Fair.
* Considering professional skills it was probably more beneficial to be there, though, and it was also fun.
* I can't watch the documentary from the class now since I don't have headphones, but I'll try to remember to watch it later.
* Law in general is not that interesting topic for me but I've had some group projects in Finland handling law topics (related to environment mostly), and when you really start to get into the law text and understand it, it might be quite interesting to get to know the very little details of the regulations.
* I hope we can find a good action for the group project so that it would also be nice to actually carry it out.

# 2022-11-24 #

* Again forgot to write last week. It seems to be every other week that I remember.
* My weekend almost starts from Thursday and continues to late Sunday / mid Monday so it's difficult to remember to write the diary.
* This time the lecture was almost all about the course project second presentations.
* I had to leave a bit early so we had to swich my presentation to be earlier than originally supposed.
* I think this time I did quite good work with the presentation. It didn't feel as awqkward to watch my own video in the class as before.
* This time I also almost even enjoyed answering the questions because I'm quite familiar with the meat/vegetarian topic.
* I hope many people learned that especially red meat is very high emission food and should be eaten less to help preventing climate change and many other environmental problems.
* I was quite surprised that so many people seemed not to know about the emissions of meat production compared to plant-based food.

# 2022-12-01 #

* Victory! Two weeks streak of remembering to write the diary! XD
* This week's lecture made me frustrated again. I had already had several relatively good weeks with this course but now I was just not enjoying at all.
* It didn't really feel worthful to be sitting in the class.
* Every week the number of people on the lecture gets less and less. Tbh I'm considering also skipping some weeks now.
* I didn't understand the instructions of the group task in the class so I just stayed silent.
* Planetary boundaries is probably a nice topic but maybe we could have had a bit more time to handle that, idk.
* It feels every time that we're enormoysly short of time and I don't like that feeling.
* Also we didn't have time to go through even close to all the groups' action ideas since we used so much time for the first two groups.
* The unclearness of the instructions just makes me pissed. Every time there are some problems to understand the tasks.

# 2022-12-08 #

* Another victory with remembering to write the diary! 3 weeks!
* I didn't go to the class this week because I was hiking Yushan.
* I read the lecture slides and it was a bit hard to understand what some of the slides were about.
* There were quite many slides from previous years, apparently to show some examples what had been done earlier.
* The voting graphs were interesting.
* I'm not sure if I understood them right, but in case yes, the effect of electoral procedure on the outcome of an election is surprisingly big!
* I'm starting to get tired of these weekly presentations, especially the ones handling the course project.
* Although, it's probably the only way to make people do at least something, when there's a threat of having to present or at least answer questions in front of the class regarding the topic.
* I'm a bit worried how our course project will turn out.
* I kind of want to do it well because sustainability topics are close to my heart but I also don't want to use too much time for that since my time in Taiwan is running out soon. :/
* I want to use my time for experiencing new stuff and being with people. Studying I can do back home too.

# 2022-12-15 #

* the inequality growth graph was interesting to see
* I'm still a bit sceptical about the sufficiency of materials for changing to renewable power generation fast enough...
* I mean, sure there are enough materials in the ground but getting them for use with a reasonable amount of pollution is quite a challenge.
* But cool if it will happen. Hoping for that.

Diary homework:

### 22-12-15 Thu ###

A) Successful and productive

B) I had a super busy day with a lot of things to do and 5 hours of lectures and somehow managed to do pretty much everything. It was a bit tiring but also felt very productive to be on the go or working all the time. Altough, in the evening when I found out that our choir rehearsal was cancelled, I was very happy to get a couple of hours just for myself to calm down, relax and enjoy the nighttime Tainan.

C) I will wake up earlier to start the day more efficient (and because of a lecture XD)
	
### 22-12-16 Fri ###

A) Unsuccessful and unproductive

B) I used totally over four hours of the day trying to understand one little analysis type but couldn't because the course material nor the internet provided me a proper example on how to do it. Even asked help many times from a group mate but eventually ended up giving up for the day and trying again tomorrow with better luck and concentration. Also a group work was causing me gray hair with other people not wanting to take responsibility and not even listening what others were talking. Ugh.

C) Going to take it easier and try to not to feel bad for asking for help.
	
### 22-12-17 Sat ###

A) Successful and unproductive

B) Unproductiveness was somewhat planned which made the day pretty much successful. I wanted to take it easier and succeeded.

C) Going to sleep longer to reduce my sleep deprivation --> make me feel better.
	
### 22-12-18 Sun ###

A) Successful and productive

B) Pretty much reached all my goals for this day. The day included a lot of procrastination but this time it didn't have that bad effect because the tasks weren't too many. I had a nice long dinner with a friend and finally decided how and when to cut my hair. A big success point comes fro remembering to write this diary.

C) I'll try to go to bed earlier to be able to wake up before noon tomorrow :D

### 22-12-19 Mon ###

A) Unsuccessful and unproductive

B) Had a low mood pretty much for the whole day. Had a final presentation and it went well but the professor had some strange critics mood and just gave negative feedback for pretty much all the groups so it made me feel weird.

C) Stop procrastinating going to hairdresser.

### 22-12-20 Tue ###

A) Successful and productive

B) Not school-wise though. Cut my hair, skipped first hour of lecture because of that but was just happy about it. Managed to do shopping for ISA christmas event for tomorrow.

C) Finally do Chinese homework (before the event!) so that I don't need to stress about it.

### 22-12-21 Wed ###

A) Successful and productive

B) Did Chinese homework and organized a christmas event.

C) Concentrate a bit more in school things once in a while.
