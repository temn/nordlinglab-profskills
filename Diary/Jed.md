# 2023-10-19 #
      *Today is my first day of the course. I was not albe to select this courses after teacher sign the paper. And then there comes the typhoon, then the moon festival vacation. 
	  The issue teacher talked about today is very interesting. Tt is about the communism which i consider it evil since i was young. My mom always say the political figure i support
	  use the way communism use to control the teenagers now. After watching the video teacher showed us, I suddenlly realize how it works. Communism really shows the side you want to 
	  see. It is a beautiful fantacy, full of our ugly desire. 
# 2023-10-26 #

      *Today's lectures covered various topics, ranging from the flaws in the current healthcare system to the transformative idea of shifting from sick-care to health-care. The proposal of paying doctors to keep people healthy is intriguing, aiming to align incentives with the goal of maintaining good health. This would require a significant overhaul of the entire healthcare system.
Another video emphasized the brain-changing benefits of exercise, highlighting its immediate and long-lasting effects on mood, attention, and memory. Regular exercise is recommended for mental health, especially as mental disorder cases, including climate anxiety, are on the rise.

# 2023-11-02 #

      *The new approach to presenting homework was surprising, but we managed to adapt. A series of videos delved into the topic of depression, emphasizing the genetic and chemical factors involved. Breaking the silence around depression, building a support network, and understanding how to connect with depressed friends were key takeaways.
       A poignant video discussed the bridge between suicide and life, emphasizing the importance of listening and being present for someone struggling with suicidal thoughts. It stressed that regret often follows the moment of contemplating suicide.

# 2023-11-09 #

     *Today's discussions began with reflections on depression and the necessity of self-care. The focus then shifted to professional skills for future work life, emphasizing the significance of building good relationships, collaboration, and going beyond one's responsibilities.
     The lecture concluded with insights into living a rich and fulfilled life, emphasizing flexibility in decision-making, the importance of forgiveness, and prioritizing a meaningful life over the pursuit of happiness.

# 2023-11-16 #

    *The presentations from other groups proved interesting, providing basic knowledge on various topics. The one on sleep was particularly engaging, emphasizing the importance of adequate sleep for everyone.

# 2023-11-23 #

    *After a classmate shared her diary, the professor praised her honesty, highlighting the importance of acceptance as a prerequisite for change. Videos discussed the challenges millennials face in the workplace, including issues related to social media addiction and the need for patience and genuine human connections.
    A thought-provoking video proposed the need to remake the internet, addressing issues of behavior modification through ads and the consequences of social punishment and reward.

# 2023-11-30 #

    *The professor encouraged concrete action ideas, emphasizing the importance of including details such as where, who, when, and how. A video discussed the importance of critical thinking in choosing news sources, advising to find the original source and verify information before sharing on social media.
     Another video explored the factors leading to the destruction of Mediterranean societies around 1177 BC, emphasizing the interconnectedness of globalized systems. The potential consequences for today's world underscored the importance of strengthening international relationships.

# 2023-12-07 #

     *The lecture on planetary boundaries revealed surprising information about the overconsumption of nitrogen and phosphorus, adding to concerns about the planet's health. The TED Talk emphasized the concept of thresholds and the consequences of surpassing them, calling for responsible actions to avoid deadly outcomes.

# 2023-12-14 #

    *Lectures provided precise knowledge on planning techniques, including WBS, PERT, and Gantt charts. The focus on outcomes and the realization of the planet's challenges led to reflections on potential solutions, including the surprising idea that having fewer children might contribute to human survival.

# 2023-12-21 #

     *The lecture on success offered inspiring insights, with practical tips on time management using charts and the importance of focusing on the outcome. Personal reflections on successful and less successful days were noted, highlighting the challenges of maintaining a productive schedule.

# 2023-12-28 #

     *Practical tips on tying ties, the idea of randomly choosing leaders, and concerns about vertical farms were discussed. The entry concluded with five rules for success, emphasizing the importance of making plans, forming habits, eliminating distractions, enjoying the process, and acknowledging the potential pain in achieving success.