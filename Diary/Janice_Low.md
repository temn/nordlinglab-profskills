This diary file is written by Janice Low B24095913 of the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* This is my first lecture. 
* I am inspired by the videos about fake news.
* I think we are living in a world full of fake news and information.
* The video "3 ways to spot a bad statistic" inpired me to think and identify before believing.
* It is essential to identify the source of the news.
* The source and ways are important when we do researches, prevent creating fake news.

# 2021-10-07 #

* It is fun to learn so much facts about money.
* The course today is a bit hard to understand for me.
* The teacher asked us "how to get millions of ntd legally" when he started the topic, I first thought the answer would be owning a bank, but eventually it is not.
* I started to think of the wealth gap problem today after the lesson. 

# 2021-10-17 #

* I am so into this topic.
* Speaking of fascism, I immediately think of China now a days. 
* It is horrifying to see people being kind of brain washed and have faith in their government blindly.
* As a Hong Konger, I am afraid that Hong Kong is gradually becoming a place filled with Chinese communist fascism.
* For me, the most unexceptable thing of fascism is is destroys people's free mind and culture.

# 2021-10-21 #

* It was shocking to know doing excersise can help growthing brain cells.
* When speaking of how to live healthily, I seldom connect it to the mental health part. 
* Matthias Müllenbeck's speech was inspiring.
* It is an interesting point of view of changing our existing health care systems from “sick care” to true “health care”.
* Although I don't think the technology now a days can offer people all over the world to moniter their health by AI, it is a facinating idea.
* Prevention is always better then cure, people's life expectancy can be longer if the technology and health care systems are mature enough.

# 2021-10-28 #

* I was moved by the video "The Bridge Between Sucide and Life".
* It really takes huge courage to live with depression, I was impressed by the mother mentioned in the video.
* However, though I agree with the idea of do not suffer from your depression in silence, I don't think everyone can be brave like the speaker.
* As one of the classmate being depressed without seeking for professional aids, I feels hard to open up my feelings to a stranger, even I known he/she is to help.
* Sometimes, the process of telling or reminding the wounded past is like getting through everything again, and it hurts.
* It is essential but difficult to seek for help and admit something is going wrong.

# 2021-11-04 #

* Big thanks to the ones shared their experiences in depression.
* Emily Esfahani Smith pointed out that chasing happiness can make people unhappy in her talk, which is so sarcastic but true.
* I have been reflecting on my life after the lesson.
* It is a fun process to think what makes me happy in my life and what is the purpose of it.

# 2021-11-11 #

* It was fun to work with groupmates from different countries, knowing the eco-friendly systems from other places.
* Duting the project, we found that the Taiwan government actually did a lot in compensating local Co2 emmisions.
* It was mid-term, so it is hard to find time to do more researches and dig deep into the topic.
* I was not feeling fine for 2 of my groupmates giving no ideas when we were having the onlinemeeting, they didn't even speak until we call their names and devided their parts. 

# 2021-11-25 #

* The video about smartphone addiction is inspiring.
* I think I am quite addicted to smartphones and computers too.
* It is the first time I have the physical class for this lesson.
* I still prefer online lessons though the physical lesson allows us to have group discussions in class.

# 2021-12-02 #

* I couldn't go to the class physically this time.
* I was shocked when professor told everyone in the classroom to hand in their phones.
* I have imagined what if I am in the classroom, how many times would I look at my phone, and the answer should be more than 20 times.
* Every group's ideas are great, but not Implementable for students, as well as my group's.
* It would be great if some companies would be able to know some of our ideas and implement them.

# 2021-12-09 #

* I was absent this week.
* Hope to be able to attend the class physically next time.

# 2021-12-16 #

* The ideas presented by the groups were fascinating.
* The ones I remembered the most should be stop burning paper money and the noise from the church.
* These ideas contains the crush between traditional thinkings and modern concerns.
* There was a rearrangement among groups and I changed to group8.

# 2021-12-16 #

* I had my injection so I was absent again this week.
* Our group has decided to divide our presentation chances evenly, so each of us would have a chance to present.
* The one presents will finish the powerpoint alone, I don't really think it is a good idea, but it is convenient.

# 2021-12-30 #

* I felt sick these 2 days so I finish this diary late.
* The groups seemed not having really much progress.
* It was nice to know this is not the last presentation as I just presented 2 times.

# 2022-01-06 #

* It is the end of the term, and I have been busy studying.
* I really like the idea of investing ladies mentioned in the video played in the lesson.
* I did the presentation this week, this is my first physical presentation in this class.
* I was kind of nervous but it shall be fine.

# 2022-01-13 #

* I studied the wrong material and did a bad job in the. exam.
* I am a bit sad because I joined the course in the third week, so I never met this test before.
* The talks are inspiring again this week, I like the one about fixing a broken heart.
* It is really hard to do so, but I think the idea of keeping a note in the phone is an easy and useful way.

# Course objectives #

# “A world of facts, challenges, and ideas” #

1.	Know the current world demographics: Check out the world’s population regularly on the website of United Nations or World Bank links given in our exam paper.

2.	Ability to use demographics to explain engineering needs: We can interview people, especially the ones living in a crowded place, if they encounter any problem. The most common ones should be noise problem and not enough public area. Thus, we can use the knowledge of engineering to deal with them.

3.	Understand planetary boundaries and the current state: Keep ourselves update to the planetary boundaries through reliable sources like NASA, and relate to the current problems / questions human are facing, eg: global warming and if there are other planets human can move to. 

4.	Understand how the current economic system fail to distribute resources:  The current economic system is unfair, at the side of the wealthy ones, leading to the widening wealth gap. We can know more about the situation of poverty according to the link of World Bank given in the exam, relate it to the situation of the people living in the poor countries. 

5.	Be familiar with future visions and their implications, such as artificial intelligence : Technology has been improving and our life is already filled with it. There are new technological inventions every year, we can keep an open mind to the new technology and keep an eye on those information, eg: the International Robot Exhibition held twice a year.

# “Personal health, happiness, and society” #

6.	Understand how data and engineering enable a healthier life: We can know about the importance of sports and healthy living through data, and equipment that can help with these are brought out by engineering. When purchasing those equipment or encountering new data, we can reflect on their relationships. 

7.	Know that social relationships gives a 50% increased likelihood of survival: Friends and family can accompany us to walk through ups and downs, so never hide your emotions to them when you are feeling depressed, it may be able to save your life someday.

8.	Be familiar with depression and mental health issues: Don’t be afraid to depression and mental health issues, seek for professional help when you are in need, and stay with your friends if you spotted they are in need.

9.	Know the optimal algorithm for finding a partner for life: Evaluate on the people you encountered throughout your life and think what kind of person you actually want. Never get into a relationship simply because you want to have a boyfriend / girlfriend. 

# “Professional skills to success” #

10.	Develop a custom of questioning claims to avoid “fake news”: There are so many fake news on the internet. Check if the information is given by a reliable source before using don’t 100% trust the information without thinking if it is rational, so you can avoid becoming one of the people spreading fake news. 

11.	Be able to do basic analysis and interpretation of time series data: Instead of using a single data, do researches and analysis, so you can know more about the information you are using rather than just a graph / data.

12.	Experience of collaborative and problem based learning: Brainstorm and communicate with your classmates about the problems, you can always learn from others’ insight.

13.	Understand that professional success depends on social skills: Having good social skills help you to gain good interpersonal relationships, and the people may offer you help when you are in need, and it is one of the key to success. So never be harsh to everyone, and work on social contact.

14.	Know that the culture of the work place affect performance: If you want a higher performance in work, learn from the successful people and keep yourself competitiveness. You can compare yourself with your partners to boost your determination. 


