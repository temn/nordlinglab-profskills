This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Michael Chen E14085090 in the course Professional skills for engineering the third industrial revolution.

#2023-09-07#

* Getting to know the class, pretty intresting course.
* This course was suggested to me by multiple friends saying that it is intresting and quite calming, which is exactly what i feel.
* Was shocked to see that we are doing the final exam mock test on the first day of class, I got 40 something for the first try, thankfully higher than a kitten.
* Expect this to be a chill and fun course throughout this semester.

#2023-09-14#

* First Lecture was fun and intresting, didnt get to present this time
* With some kind of luck I was lucky enough to find a teammate that is from the same country as me.
* Although we are only a group of 2 but I believe it is quite enough already.


#2023-09-21#
 
* Honestly I hate bitbucket, its so unefficient to use.
* Maybe it is because I'm not used to this system but my god is it annoying to use.
* As for the ecpectation for 10 years in the future, I would expect myslef to be in a stable job, hopefully in a hight tech company in taiwan.
* Since I'm on my fifth year now I hope I could finally pass my fluid dynamic course and quickly start making some real money.
* But by seeing how the world is going now, I would say within 10 years human resources would be less needed and would mostly be totally automated.
* Even by now most companies have minimalized the number of workers by replacing them with automated robots which are much more efficient and could work 24/7 without rest
* But the good thing for taking mechanical engineering for me is that till now we can see that engineers and etc. are still in high demand which is my absolute hope.
* With the advanced technology we have now and as it improves within 10 years we could only hope it for the best, since most countries are more intrested in creating weapons of max distruction.
* It's either we might destroy ourselves by then or we would probably be half dead by then.{ Hopefully not but seems to be highly likely }

#2023-09-28#

* No class today
* Still have to go to work tho at the evening, so technically didnt really have a holiday feel to it
* Really getting tired this days since school started there are more customers coming into the restaurant.

#2023-10-05#

* Still there is no class today, to be honest there wasnt really any need to have a holiday since the worst of the worst of the typhoon was at night.
* But I'm not complaining for some free holidays.
* Finally could get some rest today, no work, no class, just perfect.

#2023-10-12#

* Who doesnt LOVE money, I know I do.
* And when it comes to the standard between most nations where they set the US dollars as an international facing currency is just a joke
* The US has agree on maintaining the price of dollar to the gold reserves they have, yet lets be serious, the amount of dollars that have been printed recently
* Surely have exceeded the total amount they have, especially how they are not transparent on their countings
* yes they do have a vault in fort knox, but even if its empty I doubt they would really announce such a thing.

#2023-10-19#

* The new team was quite intresting, met a guy that came here as a transfer student if I remeber right he was from the chezh republic.

#2023-10-26#

* to be honest I would most definitely just pay when I get sick
* I am lucky enough to have great immune system in my opinion
* ever since I was a kid I only got sick once or twice every year, pretty amazing I know
* Yet now that Im studying in Taiwan I have to quite abit for the health insurance nonsense that I have barely used throughout my life here.
* It just made me feel like Im being robbed.

#2023-11-02#

* Was supposed to be my turn to present today for my group, yet there was a sudden change of plan since we combined groups together to combine all the points we've given together
* Although some how I easily found my new team mate after the presentation since apparently she was just right beside me.
* Depression is honestly the most terrible thing I've experienced.
* There are also a few friends I know that have gone through depression.
* And all we can do is to stay with them and being logical might not be the best option, most people just need someone to listen and to understand and comfort them.
* By telling them what the reality is and such are just a bad idea.

#2023-11-09#

* 

#2023-11-16#

* Our group have decided our topic which is to make products sustainable, my part with my friend is about bioplastic.
* We found out that many bioplastic could be reused and are actually being commonly used now a days which is in 3D printing.
* PLA is very affordable and could be reused by just simple heat. 

#2023-11-23#

* 

#2023-11-30#

* Our group presented about our plan to create a second hand book stall in our department, where students can donate their old books
* so that other students or professors or anyone that is intrested with the book could borrow it without needing to spend money.
* Although it is true that since it is the end of the semester so for us to proceed with this plan and get 10 people to thank us is not that achieveable.
* There are many intresting presentation that day and I also found out that the video the professor played for us for the example is actually on of my friend from the indonesian group
* And the people they found to make the videos are mostly people from the indonesian group as well, just a funny thing to find out.

#2023-12-07#

* I was suppose to present today but because I forgot to complete the whole question on picking a history event so I didnt get the chance.
* But I did become the leader of the big group to answer the questions.
* Also found my new teammate which he said he's from Peru, and we chatted abit about both our countries and how we are treated by taiwanese here.
* The topic this week is pretty intresting, we as humans have been excessively taking from our planet.
* Although there is one segment were humans have actually repaired there mistake is on the ozone layer reperation program, whcih is signed in montreal.


#2023-12-14#

* 

#2023-12-21#

#2023-12-28#

#2024-01-04#