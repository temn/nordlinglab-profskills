# 2023-09-14 #
* My first lecture of the class because i just sign up this course three days ago.
* I learned about conflicts this week.
* I am highly passionate about win-win, win-lose, and lose-lose ideas.

# 2023-09-21 #
* HW1: Summary of what I think my future would be like after 10 years and five current trends of change
* With that being said, 10 years from now I will be 30 years old and I have imagined myself being a father, living happily with my 2 children. Besides,I will have a job in a machine company like making the spare part of car, motorcycle, technology, or another stuff.
* I think the five trends nowadays.
* The first one is electric car. There are more and more companies have been developing electric cars, and there are more and more E-cars on the street now. I believe E-car will be spread all around the world, and there will be no gas car in 2032. And there will be less carbon footprint ad less noise pollution then. 
* The second one is AR technique. In virtual reality, we can do a lot of things that we can’t do in the real world, and people use that to experience things that they can rarely do. And I think in 2033, VR technique will be more advanced, which can let human enjoy all the activities and games in it. *
* The third one is green energy. All the countries are doing their best to develop green energy, hoping to reduce to carbon discharge. And the Earth will be even more green and more healthy in 2033. 
* The fourth one is star-link program. It’ll make the whole human can be enjoying the internet everywhere. I think there will be a more advanced technology that can help human being to interact with the others more swiftly and more effectively. 
* Also, I believe we are doing progress on space research and Mars Landing Program. And we will learn more about the origin of the universe, the destiny of the earth, and the way to live on Mars, so human can emigrate if the Earth is over-populated. 
* Of all the trends listed above, what I expect the most is VR technology. There are all lot of things that we never get a chance to experience because of either cost or danger, we can try them all in a small gadget without any safety or budget problem. We can’t foresee the world in 2033, we only can predict it. I believe the technology will advance, compared to 2023, and we can all enjoy the advantage that the technology gives us. Also, any kinds of pollution won’t bother us any more. In 2033, I’ll be in my 30. I hope the five things listed above can make my life better than now.


* Select one news story published on https://ground.news/ and read all the different sources. Summarise the different views in the sources in bullet point format in your diary.

* I chose U.S. Privately Warned Russia Against Nuclear Weapons (https://ground.news/article/us-has-sent-private-warnings-to-russia-against-using-a-nuclear-weapon).
* There are 9 of Total News Sources, and 3 Leaning Left side and another 4 are choosing center, and the last 2 become Untracked bias.
* In the article is written that United States already warning the Kremlin for what kind of consequences if they still use nuclear weapon in their conflict with Ukraine.
* and from a Anonymous said that White House has publicly purpose about the consequences to building a concern at Russian leaders.
* Because the article word are so hard, I still confuse what kind of stuff that they want to showing to us（the reader).
* basically what the 9 article write are the same things, but if the 3 leaning the left side they will add more stuff thatcorner one of the parties.

# 2023-10-5 #
Typhoon day off!

# 2023-10-12 #
* The lecture is mainly about the financial system which is something unfamiliar to me.
* The financial system facilitates savings, investment and access to credit for people.
* Maybe that's why we look immediately to the dollar when we want to know how the economy's doing.
* Central bank's objectives: ensure a stable value of money, guide other banks, foster economy.
* These recessions can lead to economic contraction, unemployment and widespread financial problems.
* I could totally feel the decreasing purchasing power of the New Taiwan Dollar. Things are getting more and more expensive these days.

# 2023-10-19 #
* This lecture is about why extremism flourish.
* The TED talk videos were great and helpful to fully understand what is fascism and how does it affect our mind. Those vedios really useful to let me handle the unfamiliar concept like fascism.
* Fascism and nationalism both highlights one's dedication to his country.
* In a sense, fascism sounds more like a religious belief, rather than just a political ideology.
* Potholes will affect our mental health over time, so we should not neglect or overlook them.
* Video1:Why Facism is so temptinVideo2:My descent into America's neo-Nazi movement & how I got out
* Video3: Imagined reality-Yuval Noah Harari

# 2023-10-26 #
* Today's class has been one of my favorites so far.
* In the first Ted Talk the idea was raised of changing the focus of the healthcare system, which currently focuses on treating diseases, to one that focuses on keeping people healthy. This approach can save money by avoiding costly procedures and unnecessary treatments.
* After the Industrial Revolution, mass production makes it easier to make people want what one produces, rather than produce what people want.
* Understanding a term and having to explain it in other words are, in my opinion, very different.
* Knowledge is a "well-justified true belief."

# 2023-11-02 #
* This lecture is about depression.
* Although I was aware of the topic of the day, it was very shocking for me to see the videos of the talks presented in class, since lately I am very easily moved, especially with topics that involve mental health.
* I know from first-hand experience just how hard it is to have a friend who's depressed and suicidal.
* I learned how to talk with people who have depression. We do not have treat them as a patient, instead just communicate with them like interact with our normal friends. An important point is listen to them carefully and express sincerely, then they will feel more free to talk to.
* Do not minimize any problem that may overwhelm another person and avoid judging, especially those who struggle with suicidal thoughts, and provide them with a safe space to express themselves, avoiding giving simplistic advice or quick solutions, since depression is a complex condition.

# 2023-11-09 #
* At times depression may not have any symptoms at all, that's what makes it a difficult illness to diagnose.
* I have realized that I have so much to say about it, but it is something that I do not dare to talk to anyone about unless it is someone I trust very much.
* We often have difficulty proving ourselves right about ourselves because of the subjectivity of our own thoughts and beliefs.
* I do not think I am able to forgive anyone that killed my be loved ones. I think at least it would take me a lot of time to do that.
* A meaningful life is something I wish for, more than anything.
* The vedio provides a totally different opinion which is we should not persue happiness. Finding out things we can dedicate to and people you treasure mutually are important steps to build meaningful life. Also don not forget we can change the way when we are telling our life story. It will give new perspective to regard happening.

# 2023-11-16 #
* The presentation from other groups are interesting. I've gained some basic knowledges on these topics.
* After the presentation, there wasn't much time for class. Professor did not teach much this lecture.

# 2023-11-23 #
* After the presentation, there wasn't much time for class. Professor did not teach much this lecture.
* I noticed that I misunderstood what professor ment about the big group's project. I thought the topic for presentation was only for two week, but it is actually the topic for the semseter project.
* I liked the topic that was covered in the first video we saw in class better, since it is a shame for me to see how people are slaves to technology and especially talking about social networks (I must admit that I am not very far from that reality and I hate it).
* Although technology advances, it brings with it many benefits for humanity, but I cannot help but imagine being in a world where people feel more present, outside of the virtual world, where we know how to appreciate in detail the things that surround us, leading a healthier routine and leading us to happiness, less worries.
* I will continue to keep in mind the comment I made after this video. (Why did you copy what other student wrote in her diary?) 

# 2023-11-30 #
* The professor encourages us to form a more concrete action idea.
* In order to be concrete, we should include where, who, when, what, how, etc.
* I'm particularly interested in the presentation about sleep, cause I really want adequate sleep.
* A month 'till New Years! I can't think of where to go this year, though.
* The fireworks show at Taipei 101 has been in my bucket list for a long while, but I'm not sure I want to go to Taipei just for said event.
* In any case, I hope it'll be fun times this year too.

# 2023-12-07 #
* This lecture is about the planetary boundaries
* Planetary boundaries are the limits to the impacts of human activities on the Earth system.
* in order for us to survive longer on this Earth, we should care more about it, and there are nessesary resources that we should let others know that are diminish and should be taken notice of.
* I've learned from the lecture about the planet boundary. It's quite surprising to me that except for issues frequently heard like climate change the nitrogen and phosphorous are overly consumed by us as well.
* For the supergroup project, I hope that the implementation of the action would be convenient and effective in changing people's life. Let's see if everything will go well.
# 2023-12-14 #
* this week's presentation was, needless to say, very hurried and rushed.
* I had to represent my group and presented our planning on the project. It was tiresome because I spent a long time finishing the powerpoint. I actually learned that you really need like minded people with motivation and optimism to carry out the project well. If not, then you would be the only one doing all the hard work. It is important to have people that would help you in a team, so we need to be choose and find the right team members.
* The professor discussed variations in the results of different voting systems on the same topic during the class. The outcomes revealed differences in the number of votes between anonymous and non-anonymous voting, although these differences were not substantial. It can be inferred that anonymous voting allows individuals to conceal their identity, enabling them to express their opinions more freely. This attitude may arise from the challenge of establishing accountability in anonymous situations, as it is difficult to trace the origin of statements. Simultaneously, with the increasing freedom of speech, anonymous expressions tend to be more extreme, potentially causing harm to others.
# 2023-12-21 #
* The week's lecture about how to become successful is inspiring. I wish I could have learned this earlier.
* I like the chart of important/not important, urgent/not urgent, it's a good way to manage and I've started using it. I also combine it with the WBS to determine how much time each task would consume.
* The presentation of the concept of Ikigai led me to reflect deeply, reaching the conclusion that it is really difficult to maintain a balance between all the aforementioned areas, but it would really be to achieve our general well-being.
* Lately I have realized how difficult it is to receive good feedback in an assertive and constructive way. This causes a stagnation in the personal development of someone in a certain social environment.
# 2023-12-22 #
* unsuccessful and unproductive
* In the afternoon, I went to the laboratory, and the experimental data did not meet expectations. Moreover, the instruments seemed to be malfunctioning. I spent a lot of time rechecking and calculating averages, only to eventually confirm that the instruments were faulty.

# 2023-12-23 #
* successful, unproductive
* i ate bbq today which is great, but for the rest of my time i stull didnt do anything.
* i'll probably finish my homework tommorow which feels productive to me.
# 2023-12-24 #
* Yet another Unproductive/Somewhat Successful day.
* Went to church in the morning, but I felt a little bit out of it for some reason.
* Had a hotpot night with friends. The weather's really quite chilly today, and it's the perfect time for hotpot.
# 2023-12-25 #
* not successful, not productive
* It appears that the environment at home is too relaxing for me. I'm not very productive being here.
# 2023-12-26 #
* successful and productive
* Tomorrow, I also have two final exams. In the evening, I reviewed my materials diligently, and I believe I can achieve good results. Additionally, there is a final presentation foProductive and successfull.
* I felt productive because I was able to spend a lot of time studying for my final anatomy exam. I felt successful because I did well on my PE and English final exam, so I will have time next week to prepare an important presentation.
* I will continue trying to take advantage of the time left to prepare for my exams, celebrating my small advances and also giving importance to my rest to be more effective in my preparation for the upcoming exams. my general education course next week. Even though it's only ten minutes, we are preparing very seriously. Most of the team members are highly involved, and I feel the positive atmosphere is conducive to success.
# 2023-12-27 #
* Productive and successfull.
* I felt productive because I was able to spend a lot of time studying for my final anatomy exam. I felt successful because I did well on my PE and English final exam, so I will have time next week to prepare an important presentation.
* I will continue trying to take advantage of the time left to prepare for my exams, celebrating my small advances and also giving importance to my rest to be more effective in my preparation for the upcoming exams.
# 2023-12-28 #
* How to tie a tie: Windsor knot and half windsor(suitable for informal occasions) are simple and useful.
* The idea that we can randomly choose who to be our leader randomly is interesting, and I like it.
* However, I think there's a problem. It is that not everyone want to be in the parliament.
* The idea of verticle farms work may fine in cities in developed countries, but actually, a lot of people living in poor countries may not be able to access this kind of technology, and they are people who are truly threatened by hunger.
# 2023-12-29 #
* Productive and somewhat successful.
* Started my day early, and got quite a lot of things done today.
# 2023-12-30 #
* Unproductive and unsuccessful.
* Feeling a bit burned out, and did nothing of use today.
# 2023-12-31 #
* Unproductive but quite successful.
* Spent the day with close friends, ate hotpot and watched the annual new year's eve concert.