This diary file is written by Jonathan (E14105311) in the course Professional Skills For Engineering The Third Industrial Revolution.

# 2022-09-08 #
* Its the first lecture today, bet it will be fun.
* Was relieved to see other English based course, cause my Chinese isn't that good.
* We will be working in groups of 3, I found that interesting.

# 2022-09-15 #

* Its the second lecture today, it was nice.
* There are several groups that presented today using last week's topic.
* There really are a lot of people in this lecture!

# 2022-09-22 #

* Today was the third lecture, we started with feedbacks and how the professor reacts to them.
* Our group didn't get the chance to present again today, maybe next time.
* We learned a lot about 17 SDGs, topic talks, and many other topics.

Talking about the future always sends me shivers as it has a lot of outcomes and possibilities, which I am very curious at. 10 years from now, I will fully be an adult. I have been thinking about two different outcomes. First one is staying in Taiwan and working in a mechanical-related company, a great one if possible. I hope that I can secure my accomodation and transportation along with overcoming my language barrier, as these really important for me living in a foreign country. Second, is for me to return home to my home country, Indonesia and try to work there. Of course, returning home I should bring additional knowledge for me to use, Chinese language, which these days is really useful in my country as there are a lot of foreign companies starting to enter other countries. Hoping as time goes by, I will be having a balanced life with a lot of knowledge I can use to my benefits and also for me to be able to do things I love, while able to make a living to support myself and the ones I love. What I need for these, will be consistency in learning and gaining experience. As for right now, focusing on my studies in my bachelor's degree comes first. After graduating, there are two options I can choose, pursuing Master's degree or exploring several places looking for works and gaining experiences related to Mechanical and also for my daily life. I am also looking forward to finding internships in my department, doing useful things beneficial for myself. As for now, I have started trying to find experience, I have worked two times since I arrived in Taiwan, one in a factory setting up components and the other in a restaurant, which gives me additional experience for my daily life. Also, I get to meet new people and learned more in language. I also like to learn more in my leisure time, like learning how to make 3D object using software. Sometimes watching videos of how machine works. Hopefully, in 10 years, I can achieve all of my goals without any difficulties. Even if there are difficulties, I will take them as a challenge for me to improve myself, so I can be a better person both in life and work aspects. I still have a lot of time, and I wanted to use these times so I can prepare for me to in my best form in life.

Summary From Ground News: Fiona sweeps away houses, knocks out power in eastern Canada

* Left view: After battering the Caribbean, hurricane Fiona was downgraded to a storm and it made landfall on Canada's east coast early Saturday morning bringing down electricity pylons and damaging homes.
* Center view: Hurricane Fiona transformed into a post-tropical cyclone late Friday, but meteorologists warned it could still bring hurricane-strength wind and heavy rain.
* Right view: Powerful storm Fiona slammed into eastern Canada on Saturday with hurricane-force winds, blowing over trees and powerlines and leaving hundreds of thousands of homes and businesses without electricity. 

# 2022-09-29 #

* Fourth lecture! The professor announced that we will have offline class next week.
* The presentations were changed into video type for this week, live presentation next week. 
* The professor mainly talked about financial condition and how the financial system works. Interesting!

# 2022-10-06 #

* First physical class!
* Learnt about how to think in long-term
* Caught something about fascist and extremist

# 2022-10-13 #

* Listened to several presentations of week 5
* Knowledge is a well-justified true belief
* Learned the benefits of exercise from Wendy Suzuki, even demonstrated a bit.
* Saw some health care system from Matthias M�llenbeck.

# 2022-10-27 #

* Online class for this week. no class last week.
* We got assigned to a new group and were given explanation about the projects we need to do.
* Saw a video about depression from TED Talks.

# 2022-11-03 #

* I forgot to write my diary for last week's class cause I keep procrastinating.
* We won't have any homework for the 3-person group next week, professor assigned us for the 5-person group course project instead.

# 2022-11-10 #

* Again, I forgot to write my diary for last week's class, that was really bad haha.
* We won't have any homework for the 3-person group next week, professor assigned us for the 5-person group course project instead.
* We are having the same weekly homework plus the course project.

# 2022-11-17 #

* I forgot to do my diary again for the third time, I really have to change this.
* We won't be doing small groups' assignment for this week, we'll be doing the course project instead.
* We listened the professor talking about reduction of CO2 and noise pollution, also about destructions caused by technology.

# 2022-11-24 #

* Again, I don't know how many times I forgot to write the diary, wow haha.
* The profeessor asked us to keep our phone in front to see how we will do without our phone for the next 3 hours.
* We saw a lot of interesting campaign and organzation ideas from all the groups.

# 2022-12-01 #

* I don't even know how many times I have missed writing my diary, hoped this will be the last time.
* Recently,, the weather has been really weird, the temperature has been changing up and down for a lot of times.
* We'll be doing course project for next week, we even get to change the 3 person group!

# 2022-12-08 #

* Finally I remembered to write my diary on time.
* I was going to attend the class last time, but I suddenly got an important call from home back in my country.
* I tried to read the ppt but I don't really understand what does it cover.

# 2022-12-15 #

* I was unable to attend the class because I have a rehearsal for the Overseas Night.
* I saw the ppt and the professor talked about money equality, energy storage, electricity, voting.
* I am satisfied that I remembered to write my diary for the second time in a row. Improvements I guess.

# 2022-12-16 (Friday) #

* Successful and Productive
* Succesful because of the satisfaction of my performance in the Overseas Night, Productive because I finished my long homework in a time shorter than I anticipated.
* Will try to practice more for my next  performance.

# 2022-12-17 (Saturday) #

* Succesful and Unproductive
* Succesful because of the satisfaction of my performance in the Music Performance for a community, Unproductive because that's the only thing I did after sleeping the whole day tucked in because of the low temperature rainy weather.
* Will try to find data for my final report and learn for my exam.

# 2022-12-18 (Sunday) #

* Unsuccessful and Productive
* Unsuccesful because I didn't achieve anything special in this extremely cold weather, Productive because I did some of my important homeworks and learnt for the test.
* Will finish learning for my test and start practising for the next Angklung performance I got around the first week of January.

# 2022-12-19 (Monday) #

* Successful and Unproductive
* Succesful because I manageed to do my exam and I think the result would not be that bad, unpoductive because I didn't do anything anymore for the rest of the day.
* Will start listening to the music for my performance in January.

# 2022-12-20 (Tuseday) #

* Successful and Productive
* Successful because I finished the music practice fluently, and productive because I finished some list of things I am supposed to do.
* Will try sleeping early to make up for my decreased sleeping hours.

# 2022-12-21 (Wednesday) #

* Successful and Productive
* Successful because I did the last course for the week that weight a lot and I can finally breathe free, productive because I continued my homeworks and finished the majority of them.
* Will try finding items I need to buy for upcoming events.

# 2022-12-22 (Thursday) #

* Successful and Productive
* Successful because I did find the things I need to buy, Productive because I did almost all of my homeworks.
* Will start finding data and making the required things for my final report.

# 2022-12-23 (Friday) #

* Successful and Unproductive
* Successful because I our Indonesian Organization successfully organized our Christmas Dinner, unpoductive because I didn't do anything useful for the rest of the day.
* Will try to wake up early and start doing my final report.
# 2022-12-24 (Saturday) #

* Unsuccessful and Unproductive
* Unsuccessful and Unproductive because I woke up late and didn't do anything for the day.
* Try to finish at least half of my final report.

# 2022-12-25 (Sunday) #

* Successful and Productive
* Successful and Productive because I woke up and stayed in my department building for almost 8 hours and eventually finished my final report.
* Sleep earlier to wake up for final report presentation.

# 2022-12-26 (Monday) #

* Successful and Productive
* Successful because I did well on the final presentation, Productive because I managed to finish my other presentation in a span of 3 hours.
* Sleep earlier to gain strength for the presentation and to study for Wednesday's test.

# 2022-12-27 (Tuesday) #

* Successful and Productive
* Successful because I did a great job presenting, Productive because I finsished studying and also practiced for angklung performance.
* Have a good and enough sleep to prepare fixing my bicycle and class for the whole day, including a test.

# 2022-12-28 (Wednesday) #

* Successful and Productive
* Successful because I think I did well enough for the test just now, Productive because I did the majority of my homework.
* Start learning and prepare for a test tomorrow.