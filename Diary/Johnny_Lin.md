This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-17 #

* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation

# 2024-09-26 #
* I almost fell asleep on the class
* My phone is out of battery, so I pay more attention to the class
* I learn that when dealing with numbers, you should always be skeptical 
* How to spot a bad statistic? uncertainty, Sampling method, who do the sruvey?

In 10 years, I envision a future where I live in a bustling city, an emerging global city that has embraced technology and sustainability.
The choice of location reflects the growing trend of cities becoming hubs of innovation, particularly those with thriving tech ecosystems and access to nature, which would allow for a healthy work-life balance. 
These cities tend to be more forward-thinking in terms of environmental consciousness, social progress, and infrastructure, making them ideal for a future where personal and professional growth are intertwined.                                                                                                                                                      
I used to grow in a city which named Pingtung.It a small city compare to some international cities like Kaoshung or Taipei, but it still a convienient city to live.Although my hometown was not smmiliar to other industrial cities
like Pittsbugh or Detroit,but when it comes to rush hour, the traffic becomes heavy.And the exhaust from the cars and motorbikes makes the air look gray like an industrial city.
I envision 10 years later, the traffic will be greatly improved, the AI signs help driver navigate in this city and the teconologies are able to optimize route you take and make public transport a lot easier to use.


As for my work, I imagine myself continuing to evolve as a key player in the tech industry, likely working in artificial intelligence, 
data science, or a related field. The world in 10 years will have become even more digitally driven, with AI and automation playing integral roles in nearly every sector. 
The field of AI will likely expand into more ethical, creative, and human-centered applications. I would like to work in an environment where technology is used not only for profit but for solving global problems such as climate change, healthcare inequality, or education access.
Given the trajectory of AI today, I foresee a shift towards building systems that can better understand human emotions, creativity, and even ethics. Working on projects that bridge the gap between humans and machines will not only be intellectually stimulating but also immensely impactful in shaping society’s future. 
This could involve leading or collaborating in AI research focused on enhancing human capabilities—through wearable tech, personalized learning platforms, or mental health tools powered by AI.
Remote work will likely still be prevalent, even more than today, thanks to the ongoing refinement of virtual collaboration tools and digital infrastructure. 
This flexibility could allow me to occasionally travel or live in different countries while staying deeply connected to my professional responsibilities.
Additionally, I see myself perhaps splitting time between a primary city home and a rural retreat, possibly in a country that values sustainable living, which aligns with my desire for balance between high-paced work and personal well-being.


On a personal level, in 10 years, I hope to be more grounded, balancing professional ambitions with a deep commitment to personal growth and relationships. Health and fitness will continue to be priorities, likely supported by advancements in personalized health technology—such as AI-driven fitness plans or nutrition monitoring tools.
Family life may evolve to include children or, alternatively, a strong community of close friends and colleagues who share my interests and values.
Socially, I envision the world becoming more interconnected through technology, with virtual reality and AI enabling people to form deep connections across physical distances.
However, with that, I believe there will also be a counter-movement—an emphasis on mindfulness, nature, and real-world human experiences as antidotes to the digital saturation.
In this future, my role will likely extend beyond just my work, contributing to societal shifts toward more sustainable living, inclusivity, and well-being. 
I may engage in projects that merge technology with humanitarian efforts, whether through mentoring, public speaking, or contributing to global initiatives aimed at leveraging AI for good.
Ultimately, my future 10 years from now will be shaped by a combination of technology, innovation, and a deeper understanding of how these tools can be used to improve both individual lives and society at large.

# 2024-10-26 #
A fair Hebrew wife named Susanna bathes privately in her locked and walled garden. Two elders, having previously said goodbye to each other, bump into each other again when they spy on her bathing. 
The two men realize they both lust for Susanna. When she makes her way back to her house, they accost her, demanding she have sexual intercourse with them. 
When she refuses, they have her arrested, claiming that the reason she sent her maids away was to be alone as she was having intercourse with a young man under a tree.
She refuses to be blackmailed and is arrested and about to be put to death for adultery when the young Daniel interrupts the proceedings, shouting that the elders should be interrogated to prevent the death of an innocent.
After being separated, the two men are cross-examined about details of what they saw but contradicted about the tree under which Susanna supposedly met her lover.The first says they were under a mastic tree, and Daniel says that an angel stands ready to cut him in two. The second says they were under an evergreen oak tree , 
and Daniel says that an angel stands ready to saw  him in two.The great difference in size between a mastic and an oak makes the elders' lie plain to all the observers. The false accusers are put to death, and virtue triumphs.


* This story gives me a thought about how to make a fair verdict.The one who make a judement can't just listen to the opinion of what most people say, he needs consider all other aspect and find strong proof to make a judgement.

# 2024-11-12 #
* because of the typhoon, there is no class on last week.(again lol)
* I think the final project may be very difficult to finish on time due to a lot of reasons.


# 2024-11-20 #
* I somehow find the course on this week is pretty useful since I used to have sleep disoder.
* I feel I've been approved since a lot of people vote that the academic stress occupies a huge part of the total of mental stress, and it comes from the difficult of the courses are not proportionate.
* And according to the poll result that I'm convinced NCKU is terrible. I should've never apply to this college. But it is what it is, and I'm encouraged by my friends that I should apply to other college for
my master degree.


# 2024-11-30 #
* I'm very busy for the midterm so I will just skip to the conclusion. All empires will crumble, no exceptions. What we can learn from Rome is the wrong policy and dictatorship can lead to destruction.


# 2024-12-7 #
this week I learn about planetary boundary. To be honest I do not care this topic since I concern more about my academic preformance and getting a job than enviromental protection.


# 2024-12-19 #
* productive and successful
* I fixed my sleep schedule and I finish my goal of my study plan.

# 2024-12-20 #
* unproductive and successful
* I got good grades on automatic control, and I win 5 ranked games in a row.But I skipped the class then played games for the whole day.

# 2024-12-21 #
* unproductive and unsuccessful
* I won 6 ranked games and lost 6 ranked games, and my elo(ranked score) stayed the same, which I wasted 7 hours on it.Then I spent the rest of the day on casual games.

# 2024-12-22 #
* productive and unsuccessful
* I played OSU with my friend and I got dented by him,although I got a full combo on a new map.And I successfully trobuleshoot the problem on my DC motor and arduino module.

# 2024-12-23 #
* productive and successful
* I finished my study on automatic control and fluid dynamics, then I hosted an anime watching party with my friend.

# 2024-12-24 #
* unproductive and unsuccessful
* I encountered some setbacks on some courses when I studied, and I watched a hololive vtuber playing elden ring for 8 hours.

# 2024-12-25 #
* productive and successful
* The presentation is smoother than I expected, and I successfully studied for 3 hours without checking twitter and playing mobile games.

# 2024-12-26 #
A.productive and successful


B.I finished my study on complex anaylsis.


c.I can study other subjects.

# 2024-12-27 #
A.unproductive and successful

B.I did a little bit study but wasted a lot of time on check twitter and facebook.

C.maybe I should delete my SNS.

# 2024-12-28 #
A.productive and successful

B.I finished my study on composite material.

C.I'll use my time more efficiently so I can have more time to play.

# 2024-12-29 #
A. unproductive and unsuccessful

B.I slept untill noon, then watch shirakami fubuki and sakura miko gamebling for 8 hours.

C.I should finish my study first, then go to do some leisure activities.

# 2024-12-30 #
A. unproductive and unsuccessful

B.I watched amane kanata playing elden ring for 9 hours and then went watching some apex legend streams.

C.I should buried my PC in the backyard or locked in a safe.

# 2024-12-31 #
A.productive and unsuccessful

B.I did poorly on midterm exam, so I need to work harder on final exam, and I celebrate new year's eve with my friends.

C.I need to expidite the studying progess since the final exam is imminent.

# 2025-01-01 #
A.productive and successful

B.I finished some of my homework then watched band dream it's my go for 5 hours.

C.I need to spend more time on studying if I want to get good grades on final, especially on machine disign.

# 2025-01-14 #
actually I feel I learn a lot from this coruse,aside from other major course like fluid mechanics and engineering math, those other stuff like sleeping, stress relief and connection between people.I hope next year professor can 
add some lecture like how to improve your performance on work interview and how to improve relationship between yourself and colleagues.Also I think the task of this course is way too diffcult, I hope to topic selection can be 
more flexible, which means if the major derection is determined it's ok to us but we can select the topic we want by ourself.