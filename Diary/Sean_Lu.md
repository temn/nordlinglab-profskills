This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Sean Lu E14093043 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #
* This is the first lesson.
* First time meeting my crew but there`s only two of us.
* We did a quiz about trends in the whole world.
* It was a bit difficult for me. I hope it will be easier to answer these questions after the course.

# 2022-09-15 #
* Listen to some presentations about production growth and decreasing in prices.
* A new team member has joined.

# 2022-09-21 #
* We review the comments about the class that we wrote on the google form
* We heard some students share their diaries.
* Learn some skills about making a powerpoint.

*  I think that in ten years, I will have successfully completed my current degree and a master's degree from the Institute of Materials Science and Technology, and then I will go to work in a large technology company. Not sure which company I am going to yet, but it will be relate to what I learned at school. Although I live the life of a normal office worker, I may sit in a similar job every day, but I will also get a good salary for my hard work at work. I think I will move back to Taipei after I finish my studies. It may be renting an apartment or continuing to live with my family, but I most hope to have a happy time with my own wife and children everyday.
*  I feel that living in a world that is changing all the time, there are many changes that will affect me more or less whether it is me now or me ten years from now. I think the more widespread use of electricity resources to replace petroleum fuel resources will be the first thing to affect me. Everyone knows that petroleum fuels are running out, so the government began to formulate relevant regulations. In a few years, the cars and motorcycles on the road will be electric vehicles instead of traditional oil vehicles. There is also the vigorous development of the Internet. With the change of the times, the network we need to use when processing information will be larger and faster. Although everyone is still living in the 5G era and 5G signals are not yet popular, but I believe that we will soon face the 6G generation. There will also be a big shift in manufacturing, now there are more and more lightweight composite materials or more sophisticated manufacturing techniques like 3D printing, from small precision parts to big things like houses all can be made in this way, with more advanced materials. Also, although the current medical skills are already very powerful, I think there will be more substantial progress in ten years, and the number of people who live to be over 100 will also increase greatly. Smart home appliances will gradually become popular in life. Using the remote control of the mobile phone, you can turn on or off the air conditioner first, and you can automatically close the window by speaking the command, etc.
* The topic I chose is "Drinking two to three cups of coffee a day linked to a longer life"

# 2022-09-29 #
* Economics is a lot harder than I thought.
* Forgot to write my diary and filled out the feedback form...
* This three hours class is a little too much for me.
* New members are ready for the next presentation.

# 2022-10-06 #
* Listened to some live presentations about economies of Indonesia, Finland, and Singapore.
* Feeling a bit tired during the class.
* I prefer online classes a lot more.
* Learned that the government sometimes proposes policies that appear to be beneficial to the people but are actually more taxed behind the scenes.
* Learned the pros and cons of countries borrowing money from the central bank and from foreign countries.
* Talked about why people at our age won't vote. 
* I used to hate Korean when watching baseball games because they all use some dirty tricks to score points, but i find that sometimes major league players do some tricks to score, so the idea of hating Korean fades away.
* I'm not a religous people so whenever I'm in danger or in trouble I won't mention god.
* I believe that China will launch a war against us in the future so I am always ready to surrender.

# 2022-10-13 #
* At today's lecture we did a short exercise that is good for the body and the brain.
* Listened to some presentations and the one that I am most impressed is about "Crazy Friday".
* Heard some students share their views on Taiwan's traditional customs.

# 2022-10-27 #
* We have online class today.
* Made a presentation during the class but there's only two members working including me.
* Learned some ways to live healthier and have a better life.
* Bad habits such as smoking and drinking too much will cause damage to our body.

# 2022-11-03 #
* Unfortunately, I forgot to write my diary before sunday.
* I was so tired that I kept falling asleep during class.
* We discussed with nearby classmates about how to help a friend who is in depression.
* Sometimes we shouldn't overestimate ourselves because that can put us in a lot of stress.

# 2022-11-10 #
* We listened to some course projects.
* It's actually preety fun to gain knowledge about different topics through presentations.
* Watched a video about law.

# 2022-11-17 #
* I forgot to write the diary...

# 2022-11-24 #
* We have to hand in our phones this week.
* To be honest, I really miss my cellphone during the class.
* Listen to some group projects and every presenter did their best to answer all the questions on stage.

# 2022-12-01 #
* I just forgot to write my diary again...
* The presentations and other tasks from different courses are a little too overwhelming.
* Hope to take a week off.

# 2022-12-08 #
* We did a course project presentation today.
* We had a debate about the actions each group has chosen.
* At first I felt really nervous talking on stage and answering all the questions from the audience.

# 22-12-15 Thu #
* A. Successful and productive
* B. I worked hard for the test today and had a nice meal with my friends.
* C. Guess I'll study more tomorrow.

# 22-12-16 Fri #
* A. Successful and unproductive
* B. I slept until afternoon and went to class, after that I went shopping.
* C. I'll get up earlier.

# 22-12-17 Sat #
* A. Successful and productive
* B. I went to have a delicious brunch with my friends, and in the afternoon I discussed the course project for almost 3.5 hours.
* C. Just keep it going.

# 22-12-18 Sun #
* A. Successful and productive
* B. I slept until I woke up naturally, and discussed the final reports of another course with my classmates at night.
* C. Tomorrow is weekday and I think I should get up earlier.

# 22-12-19 Mon #
* A. Successful and productive
* B. I presented the final presentation of another course and prepare for the final exam.
* C. I will keep studying for all the exams.

# 22-12-20 Tue #
* A. Unsuccessful and unproductive
* B. I took a pill last night due to my running nose. Unfortunately, I overslept and missed the class in the morning.
* C. I will try to wake up on time although it's cozy staying in bed at such temperature.

# 22-12-21 Wed #
* A. Unsuccessful and productive
* B. There's a final tomorrow so I basically spent the whole day studying without having a meal.
* C. I want to meet some friends after the exam is finished.

# 22-12-22 Thu #
* A. Unseccessful and productive 
* B. There's another test tomorrow so I'm studying almost for the whole day again. I was too tired so I skipped the class.
* C. I will try to sleep earlier and eat three meals a day.

# 2022-12-29 #
* This is the last time for this class this year.
* After New Years Eve with a few friends, I went home and study.
* I'm in great depression because my girl just left me before Christmas.
* Breaking up and final are overwhelming for me.
* The best thing I've learnt is to enjoy my life and to not always stay in the comfort zone.

# 2023-01-05 #
* I guess this is the last time writing the diary.
* We had a exam today but unfortunately I didn't prepare for it and got a bad score.
* Happy winter vacation and happy Chinese New Year to everybody.