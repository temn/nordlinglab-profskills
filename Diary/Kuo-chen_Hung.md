# 2021-10-03 #

* This is my third lesson's diary, due to didn't figure out how to use GIT's diary, i didn't compose any diary in the past two class.
* Now i am going to summerize what i get in these weeks: the important of group works, how to face conflicts in group.
* Two topic we should do for presentation: Is the world getting better or not, and figure out whether the news in front our eyes is real or not.
* Our first group work for the second week's presentation, i thought, is not good enough. Everyone should prepare stuffs before group meeting.  


# 2021-10-10 #

* Now is the end of the birthday of my country, and here i celebrate with writing the diary.
* Economic and finantial problems are always pain in my brain. I need time to study them. the survey in class really knock me out.
* It seems that english version of NCKU is really scaring, one of my teammates seemed to withdraw the class.


# 2021-10-17 #

* The presentation in Wednesday is my first report, seemed everything is fine.
* i finally understood with some economic words, like credit or debt, because i needed to make the report.
* I found that PPP, Perchsing our purity is not used to determine single country perchasing power, is used to compare two country.
* I'm glad to join the physical class, hope to meet everyone, seems to be a lot of foreigner.

# 2021-10-24 #

* I forgot to write diary, it's 11:40 pm, i'm rushing for it.
* I have misunderstand the meaning of the task last week. What we need is a fiction story, incedent that affected me, not the fictional novel.
* I figure out fiction stuffs are those human can create, but animals can't.
* I found that Taiwanese leading party is itself fiction. Always let citizens hate China to make them forget wrong policies they made.

# 2021-11-04 #

* I forgot to compose the diary on time this time, so i'm doing now.
* I used to suffer the depression due to my high school classmates didn't like me.
* Finally i found that the problem is my personality.It is not that good, and i fixed it in university.
* Nowaday, i make more friends than before. i am glad for myself.

# 2021-11-14 #

* I found that this class is going to be more interesting, because it was my first time to have the project about climate change.
* After searching the data, i figure out that the main murderer of the climate is caused by CO2.
* A lot datas show the successful examples for fighting against climate change were happened in Europe.
* In the view of previous point, i think that countries which are not highly developped can not do against-climate change.
* i think it is ver hard for Taiwan to stop increasing greenhouse gas, because we are still use fire-plant to get electricity.

# 2021-11-28 #

* Now is coming to 23:19. Fluid mechanics really knock me off.
* I think the physical class is amazing, lots of foreigner student showed up in the same time.
* The new classroom is fantastic.
* After getting together to discuss the problem, i found that i still need to improve my english listening.
* I have no idea about virtual world, but i think it's tendency of world developing.

# 2021-12-05 #
* Now is coming to 23:34. Thanks for Rene message, or i will forget to write the diary.
* After these two physical class, i can think in system about pollution and climate problems.
* I find that everything we want to deal with protecting environment mostly go with political processes.
* One of our teammate was disappear in course project nr. 8 last week, i think "she" should told us if she have problem.
* If someone don't have basic attitude, i suggest that he or she should not take class with project.
* Finally, i took professor's advice and change my glasses. Now i feel my eyes are good.

# 2021-12-25 SAT #
* I totally forgot to write the diary everyday. Thanks for Dave mention mw about that.
* Today i have the kickboxing match, so sad that my opponent is fear about fighting me, he quit. And i played nothing.
* My rugby team have matches today in Taipei, hope them can win.
* It seems that i have to find somwthing to wear in formal situations.

# 2021-12-27 MON #

* I forgot to write the diary again.
* My rugby team got 2nd in Taipei and 100 thouand money for encourgement.
* I am sad that i don't have that ability to join this match, because food and hotel is excellent in the matches.
* Our team had 12:22 lose to the Army school, but i knew that they have done their best.

# 2021-12-28 TUS #

* I am a sixth year buchler, grades are important to me.
* If my Fluid mechanics fell, my life and univerity's life fell together.
* Totally, i got 75 in my second mid term, really save my ass, because my first time only 10.
* I am loyal to my rugby team, i am proud to be a player, thus sacrifice a lot on my homework.
* I will not spoil here. I will success.

# 2021-12-29 WED #

* So sad that i could not do something for my big team, too busy.
* Sometimes i thought about that this class is special, a class doesn't teach any tough mechanicl stuffs, so lucky.
* I always confuse that whether i fit to ME or not, because i am skillful in language, music and PE, BUT NOT M.E..
* Too tired, but always need to back to rugby field, this is my last duty.

# 2022-01-03 MON #

* So sad that i forgot to write my diary.
* the new year eve i drank a lot of Achohol.
* Unforturnately, our rugby team have team celebration on JAN. first, need toplay rugby matches from 12 pm to 5 pm.
* My body just like torn apart when having the big meal at night.
* Too much pain and too much acohol,......unrealistic

* Now is coming to 23:34. Thanks for Rene message, or i will forget to write the diary.
* After these two physical class, i can think in system about pollution and climate problems.
* I find that everything we want to deal with protecting environment mostly go with political processes.
* One of our teammate was disappear in course project nr.8 last week, i think "she" should told us if she have problem.
* If someone don't have basic attitude, i suggest that he or she should not take class with project.
* Finally, i take professor advice and change my broken glasses. Really feel good with my eyes.

# 2022-01-20 #

* This is my last time to compose the diary.
* I hope that professor will not fail me in this class, i need this class' score for graduating from the school.
* This is my 6th year in the university. Beg for passing this class.
* I am so sad that i always forgot to compose the diary, too much homework needed to do......
* Here I congratulate professor for having a child and wish you have a wonderful family life.

# Course objective's suggestion for learning the skill required to fullfill the objective #

* 1.know the current world demographics:
  we can obtain the data from wiki but the number is an estimation. we still need to add and reduce demographics 
  by recent new data from each countries' government, like corona, war event data.
* 2.Ability to use demographics to explain engineering needs:
  I think we need to explore what kind of problem will produce if demographics grow up, like climate change
  , food shortage..., and then we can know  to use engineering method like green energy using, making efficient
  food producing machine ...etc to cure the future problem.
* 3.Understand planetary boundaries and the current state:
  We need to search the data about planetary boundaries, because most of us are not familiar with it, and then search 
  the data about recent climate change and environmental chemical pollution, using plantary boundaries to explain them.
* 4.unerstand how the current economic system fail to distribute resourses:
   i think first we neet to figure out politics and capitalism problem, beacuse allocating the resource always related 
   to  politics, and capitalism always cause rich and poor gap.
* 5.Be familiar with future visions and their implications such as artificial intelligence:
   I think if we want to understand the future, we must first understand the research direction of technology companies, 
   and military development. The progress of society comes from changes in the use of commodities, 
   and most of the technologies used in commodities come from military research and development.
* 6.Unerstand how data and engineering enable a healthier life:
  First, we need to figure out waht kind of environment and daily life can lead a healthy life.Then we can have 
  motivation to review the data and engineering method to fix our lifestyle.
  Like buring paper money is not good for health, and maybe we can come up with a new burning machine that will not 
  produce bad particals.
* 7.know that social relationships gives a 50% increased likelihood of survival:
  I think we can observe two different kinds of people and record, one is good at social, and the other is morein their 
  own world.
* 8.Be familiar with depression and health issues 
  If we want to familiar with psychological issues, we can directly contact and care for friends with depression, but 
  don't try to correct them, jut listening more.
* 9.Know the optimal algorithm for finding a partner for life:
  I think we need to study who we like first and then make ourselves attractive. If we are not attractive, the algorithm
  will be the worst.
* 10.Develop a custom of questioning claims to avoid '' fake news''
  I think we need to cultivate a spirit of skepticism. For a piece of news, we must inestigate the origin of people 
  and things in its content.
* 11.Be able to do basic analysis and interpretaion of time series data:
  we can try to do with data with trend chart, x-axis is the time line,and y-axis is the number of things we want to 
  analysis, like demographics up and down. Then we can try to interpret why it go in this trend.
* 12.Experience of collaborative and problem based learning:
  I think the method is making ourselves join the project which needs the teamwork. Projects have main problem and need 
  teamworks.
* 13.Understand that professional success depends on social skills:
  I think we still need to do with a project like making a robot or making a hydrualic research. Project need teamwork
  and same time testing our social skills. If conversation success, i project will be easier. 
* 14.Know that the culture  of the workplace affect performance:
  I think we can try to do some part-time job to realize that whether the the culture will affect to performance.