This diary file is written by Celine F54095051 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #
* Second lecture of the class!
* I did a presentation today and received some insights from the professor.
* Well my hypothesis could be wrong, but let's see.

# 2022-09-22 #
* Third lecture of the class!
* Today I was lat for the lecture because I attented a workshop, so I missed out the first half of the lesson.
* In the second half, professor talked about the impact of fake news.
* I think fake news are terrible, people can believe the wrong sources and it would ended up affecting the society in a bad way.
* In my opinion, it is similar to propaganda. 
* We were assigned of a task, that is to describe how we think our future will look like in ten years from now. I think ten years from now, I will be having a family of own, living in Taiwan or around Indonesia. By then, I would be an Engineer with MBA degree, who works in a renowned fashion brand company to develop and market sustainable fashion technology. The reason why I can envision myself doing that in the future is because it is my current goal. As an individual who loves fashion and technology, nothing comes better with both of them integrated in my practice. I would love to work with passion, but also making big bucks at the same time. By then, I think fashion trend would be more and more focused on sustainability. The world will implement generative design for most of the industries, not just the fashion industry. Our world might have less and less resources, thus we need to work on maximizing every single part of the resources we have. Remember how textile dyes contribute a big part in water pollution? Well, not anymore since bio-based textile dyes were invented. Other than that, automobiles will be using environmental-friendly choice energies, solar energy will be popular at that time and contribute a huge part in powering the world. Most people use solar-powered cars, including me, because they are low cost and a better choice for the environment.  Oh, people don't really use paper anymore, it is out of trend already. Everything is made digital, paperwork is more efficient with less waste, we no longer cut those trees for paper production. On the other hand, I see that a lot of human resources are replaced by artificial intelligence resources, especially in restaurant, most of the staffs are robots. A lot of people lost their job due to the development of technology; it is like a double-edged sword. Oh, the virtual bank has also been a trending phenomenon in finance-related technology. There are fewer people who use cash, or to exact almost no one will use cash anymore. All we need to do is just a tap or scan, and transaction is done! But since the transaction is virtual, it feels unreal. The transaction deals are getting bigger from time to time, more deals are made within one click. It pushes people to buy even more than what they needed, so is that actually a good thing? We might ended up using more resources than saved resources from the development of technology. 
* The article I have chosen was 'Blinken warns Europe against getting bullied by Putin over energy'. 
* After reading all the sources available on Ground News, I am actually quite confused because each article gave me a different vibe. 
* The Kyiv Independent, New York Times, and The Washington Post are categorized as lean left. Most of the contents are talking from social economy views and how Europe's alliance should unify to resist the threat. The Washington Post was considered mixed factuality, while the other two were high factuality.
* I found out that there were two almost exact articles categorized differently. The one from News24 was considered center and high fractuality, while the Times of India one was considered lean right with mixed factuality. But when I opened those news. the content were very similar, almost like a copy. So, can we trust the Ground News?
* All the articles in center were categorized high factuality. I can notice that there is a difference between the center and the other two fractions. Center gave me the vibe that they just report the news, of what is happening. Not making much considerably subjective comments. 
* While for lean right, there was only one article in this category, which was Times of India. The content emphasized on the determination of Europe to get rid of Russia's chokehold. 

# 2022-09-29 #
* Fourth lecture of the class!
* Today's lecture was talking about something I like, money.
* For me, money is an unit of value to convert and simplify the worth of an object or service. 
* There were videos about the concept of money, but it was quite confusing. 
* I understand what they are talking about but at the same time I don't understand the meaning behind it.
* Well, in my opinion, money are invented due to civilization and they are controlled by the richest group of people who have the authorities. 
* Banks make money from loans. They create based on our request.

# 2022-10-06 #
* Fifth lecture of the class!
* Today we discussed about the purchasing power of Indonesia, which is my country. The product's price doen't really decrease, it increases a little bit instead. But the Rupiah currency worths more compared to two years ago. So, the increment of purchasing power might be due to the GDP increase, not recicropal of CPI.
* Professor asked us whether it is better for government to take a loan from other countries or their own central bank. If the money is massive, borrowing to other countries would be a better choice, while for small amount, it is better to borrow from own central bank. But, when the central bank prints out more money, then value of the country's money will decrease. So, that is something that must be paid attention to.
* Story 1: Wash the white clothes on Monday and put them on the stone heap; wash the color clothes on Tuesday and put them on the clothesline to dry; don't walk barehead in the hot sun; cook pumpkin fritters in very hot sweet oil; soak your little cloths right after you take them off;this is how you sweep a whole house; this is how you sweep a yard; this is how to love a man, and if this doesn't work there are other ways, and if they don't work don't feel too bad about giving up;this is how to make ends meet; always squeeze bread to make sure it's fresh; but what if the baker won't let me  feel the bread ?; you mean to say that after all you are really going to be the kind of woman who the baker won't let near the bread? (This story is relatable to me, as my family often tell me how do I need to act and behave like a girl. Some of the advices are great, but sometimes they are just too much. Why do girls have more restrictions?)
* Story 2: Long, long ago there lived at the foot of the mountain a poor farmer and his aged, widowed mother. They owned a bit of land which supplied them with food, and they were humble, peaceful, and happy. Shining was governed by a despotic leader who though a warrior, had a great and cowardly shrinking from anything suggestive of failing health and strength. This caused him to send out a cruel proclamation. The entire province was given strict orders to immediately put to death all aged people. Those were barbarous days, and the custom of abandoning old people to die was not uncommon. The poor farmer loved his aged mother with tender reverence, and the order filled his heart with sorrow. But no one ever thought twice about obeying the mandate of the governor, so with many deep and hopeless sighs, the youth prepared for what at that time was considered the kindest mode of death. (The mandate is like the society's demand, while I am the youth. Nowadays, pursuing education and career often leads to difficult decisions. I want to be near my family, but there is not much good opportunities around my hometown, so I need to sacrifice my time with my family to be faraway from my hometown to pursue education in another country.)
* Story 3: Remember the night out on the lawn, knee deep in snow, chins pointed at the sky as the wind whirled down all that whiteness? It seemed that the world had been turned upside down, and we were looking into an enormous feild of Queen Anne's lace. Later, headlights off, our car was the first to ride through the newly fallen snow. The world outside the car looked solarized. You remember it differently. You remember that the cold settled in stages, that small curve of light was shaved from the moon night after night, until you were no longer surprised the sky was black, that the chipmunk ran to hide in the dark, not simply to a door that led to its escape. Our visitors told the same stories people always tell. One night, giving me a lesson in story telling, you said, "Any life will seem dramatic if you omit mention of most of it." (I really like this story, especially the last sentence. People often leave out a huge part of the story in order to spice it up because it leaves the space to imagine for the listeners. That is why, listening to same story from different perspectives would let us have a better grasp of the truth.)

# 2022-10-13 #
* According to Plato, knowledge is a well-justified true belief. 
* I think it's a short yet beautifully described statement. 

# 2022-10-27 #
* We formed a supergroup and had to do a sudden presentation, but we weren't chosen.
* Other groups shared about healthy ways of living.
* Seems like a lot of group projects next time. 

# 2022-11-03 #
* Today we talked about depression. When professor asked us where is the line of us distancing a severe depressed person, I actually thought of wanting to help them without any limits. But then I realized that I have a bad habit in prioritizing my loved ones need than mine, and I think it might not be healthy.
* I think to live a healthy and fulfilling life is to know our capabilities and don't overwork ourselves just to meet the expectation of others.

# 2022-11-10 #
* Today we skipped diary reading.
* We didn't record a presentation video for the course project, fortunately we can do a live presentation.
* Class ended sooner than usual, yay.

# 2022-11-17 #
* First half was group presentation.
* Our group was chosen, but our presenter didn't show up.
* Next week is the course project group presentation.

# 2022-11-24 #
* Course group presentations today, I think many of us didn't understand about the tasks.
* Some groups didn't really talk about what kind of actions would they execute.
* Professor told us to come up with 3 actions related to our first course project topic.
* Today we watched two videos. 
* Next week we have to sort news from different sources. 
* Didn't look for my phone today. 

# 2022-12-01 #
* Small group presentations today.
* Next course project requires us to interview.
* It is getting harder in my opinion.

# 2022-12-08 #
* Today we had a debate and I represented my team.
* Realized we should reconsider some of our choices. 
* Hopefully we can come up with a better platform other than Facebook.

# 2022-12-15 #
* New small groups again.
* We were told to write a diary everyday from now on.

# 2022-12-16 #
* Today I skipped a morning class because I wasn't feeling well.
* It was a lazy and relaxing day.
* I spent the rest of the day hanging out, it felt nice taking a break.

# 2022-12-17 #
* Today is so cold.
* It was raining the whole day, I had to stay inside.
* Eating hotpot felt nice and warm.

# 2022-12-18 #
* I woke up at 7 AM to practice dancing, it was windy and cold outside.
* Today is productive, I did a lot of things. 
* Part of it is because I am making up for the guilt of not doing much in the past few days.

# 2022-12-19 #
* Today I should have been learning some new experiments, but the person who was supposed to teach me forgot about our schedule.
* So, I didn't do much that day, just doing my homework.
* Boring day.

# 2022-12-20 #
* Morning class at 8 AM, took some efforts to wake up.
* Got my water supply exam back, bad scores. 
* Not happy. 

# 2022-12-21 #
* Nothing much for today.
* Just a normal usual day.

# 2022-12-22 #
* Today we had a uniform hotport party at our department.
* I wore my highschool uniform, it has been a long time since I wore one.
* The party was fun.

# 2022-12-23 #
* Last night was too tiring, so I skipped the morning class.
* I went to the lab to do some experiments, the result turned out well.
* Had a christmas dinner with my friends, the day ended well.

# 2022-12-24 #
* Today is a busy day. 
* I had to teach from morning to afternoon.
* Then I had to practice dancing after the class.
* Then I went to stroll during the night, not bad.

# 2022-12-25 #
* I stayed in bed until 10 AM, then I went to teach.
* Dyed my hair today, still trying to get used to the new colour.
* Had japanese food for christmas dinner, it was yummy.

# 2022-12-26 #
* Today is a tiring day.
* Other than lunch and dinner, all I did was doing experiments.

# 2022-12-27 #
* I feel like I am running out of time to prepare for my finals.
* Went to classes and did experiments for half of the day.

# 2022-12-28 #
* Still went to laboratory to do some experiments. 
* Made some mistakes, feeling down.
* I hope winter break comes soon.

# 2022-12-29 #
* Went to laboratory in the morning to finish my experiment.
* Done with it and felt great.

# 2022-12-30 #
* Did nothing much today.
* An unproductive day.

# 2022-12-31 #
* Did some homeworks and studies, felt a bit productive.
* New year eve! Went out to get supper and strolled while counting down to 2023.

# 2023-01-01 #
* New year but can't celebrate because have to study.
* Getting unwell during the night.

# 2023-01-02 #
* Tested positive for COVID-19 in the morning.
* What a way to start 2023.
* Feeling unlucky and impossible to be productive.

# 2023-01-03 #
* My body isn't feeling well.
* I still can't be productive although I have many exams coming up.
* Tried my best to study.

# 2023-01-04 #
* Feeling slightly better, but my throat hurts.
* Had an exam today, glad I still did alright.
* Feeling unmotivated.