12345642This diary file is written by Maxwell Yuan E14116809 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lecture was pretty good.
* I was really inspired and puzzled by the exponential growth.
* I had a lot fun thinking and discussing with my peers!
* This class is different!
# 2024-09-19 #
* The 2nd lecture was a little overwhelming because the temperature inside the classroom was extremely high.
* I thought everything in the world is going downhill before I saw the TEDtalk video.
* Perhaps it's only the morals of human beings that are declining at a fast rate.
* Other than that, we living good, init?
* Oh yeah, I also presented in class and it was quite terrifying.
# 2024-09-26 #
Ten years from now, I see myself living in a city that fosters innovation and technological advancement, possibly in the United States, where opportunities in mechanical engineering and data science thrive. Given my academic path and spiritual journey, I will likely be working in a field that merges engineering with sustainable solutions or advanced technology, perhaps within renewable energy or automation. My role might involve designing or optimizing systems that reduce environmental impact.

The first trend that will profoundly shape my future is the rapid advancement of artificial intelligence and automation. As someone pursuing data science, I will need to stay at the forefront of these developments. AI will revolutionize engineering by creating smarter systems and automating tasks that currently require manual input, giving me the chance to focus on more complex and creative aspects of problem-solving. This will also align with my goal of becoming a data scientist, where AI will likely enhance data analytics and predictive modeling.

The second trend is climate change and the shift towards sustainability. Engineering is moving toward greener solutions, and I expect this trend to dominate the next decade. By 2034, I will likely be part of teams working on developing sustainable technologies. Whether through energy-efficient systems, renewable energy innovations, or materials engineering that reduces environmental footprints. This aligns with my values, as I feel a personal responsibility to care for God's creation.

The third trend is globalization and the increasing need for cross-cultural collaboration. As a mechanical engineer with international aspirations, especially having a connection with the world, I foresee working on global projects, collaborating with diverse teams to solve complex engineering challenges. This will require adaptability and cultural sensitivity, values that my faith continues to foster in me as I seek to love and serve others.

The fourth trend is the growing demand for personalized education and skill development. With technology rapidly evolving, the traditional education system is shifting toward lifelong learning, and I will likely need to continually update my skills. Pursuing certifications, will remain essential to staying competitive in my field.

The fifth trend is the digitalization of daily life, from smart cities to digital currencies. By 2034, this digital transformation will have deepened, influencing how we live, work, and interact. My expertise in mechanical engineering, combined with data science skills, will help me contribute to designing these future systems, ensuring they are both efficient and ethical.

In conclusion, my future will be shaped by these trends, blending my professional ambitions in engineering and data science with my personal faith, as I seek to serve and make a positive impact in this world. I am confident that with God's guidance, my life will reflect the balance of technology, sustainability, and purpose.

-News from different sources-
* The left focuses on De Croo’s demands for "concrete steps" and victim recognition, presenting it as a direct critique during Pope Francis’ visit.
* The center highlights King Philippe's similar strong criticism, emphasizing the urgent need for atonement and tangible actions.
* The right includes both De Croo’s and Philippe's critiques but also underscores Francis' acknowledgment and commitment to shame and forgiveness, adding a historical perspective about the Church's practices.

# 2024-10-17 #
* Today we talked about money and economics, it was really interesting how money and credit are used to trade for services, goods and financial assets. 
* Money is something that interests a lot of people, but few of them know the meaning of money. Many of them will say money is evil, but it's not the money that is evil, but people.
* We talked about how it would be like if NTDs are cahnged into bitcoins, and the outcome of it would be disastrous. Because money is printed every day, but bitcoins are not.

# 2024-10-24 #
* I thought that the depiction of Inferno (or Hell) in Dante's Divine Comedy is real and probably true. But later I found out that the Bible never really said what Hell would look like. All we can tell is that it's a seperation from God.
* I thought that Jay Gatsby in the novel "The Great Gatsby" is a real person in the US history. And it changed my view on him because I thought the novel is just talking about a specific person, but instead it's talking about all the rich during that time in America.
* I thought that George Washington actually cut off a cherry tree. But I found out that historians have found no evidence that this incident actually occurred. It led me to think whether there are more fiction stories about other US presidents.
* Lai Ching-te: 
To our young adults and those in the prime of life, next year, the minimum wage will once again be raised, and the number of rent-subsidized housing units will be increased. We will expand investment in society and provide more support across life, work, housing, and health, and support for the young and old. Raising a family is hard work, and the government has a responsibility to help lighten the load.
To our senior citizens all around Taiwan, next year, Taiwan will become a "super-aged society." In advance, we will launch our Long-term Care 3.0 Plan and gradually implement the 888 Program for the prevention and treatment of chronic diseases.
We will also establish a NT$10 billion fund for new cancer drugs and advance the Healthy Taiwan Cultivation Plan. We will build a stronger social safety net and provide enhanced care for the disadvantaged. And we will bring mental health support to people of all ages, including the young and middle-aged, to truly achieve care for all people of all ages throughout the whole of our society.
* Three ways to live healthier: 
1. Sleep 7-8 hours a day on a regular sleep-wake cycle. Sleep is vital to brain health, including cognitive function. "Sleeping on average 7-8 hours each day is related to better brain and physical health in older people.", "The sleep-wake cycle is influenced by many factors. A regular sleep-wake schedule is related to better sleep and better brain health." - Global Council on Brain Health (GCBH);
2. Don't smoke. Smoking related to 16 types of cancer, 5 cardiovascular diseases and 15 other diseases as well;
3. Don't drink. Drinking caan lead to interference with the brain's communication pathways, heart damage, liver and pancreas problems, and cancers.

# 2024-11-07 #
* Today we delivered our presentation on how to live a healthier life.
* I went on stage to talk about what I wrote in my diary, it was fun and the professor ask me some interesting questions.
* We will be presenting a topic about depression, which can be hightly relatable to anyone.

# 2024-11-14 #
* Today, we talked about depression, and there are two groups who presented their findings. They were both extraordinary!
* Depressed people are not always down or frowning or unhappy, sometimes they can be our friends who smile frequently.
* I learned how to approach depressed people with love and kindness. As it is said in the Bible, in 1 Corinthians 13:4-7, "Love is patient, love is kind. It does not envy, it does not boast, it is not proud. It does not dishonor others, it is not self-seeking, it is not easily angered, it keeps no record of wrongs. Love does not delight in evil but rejoices with the truth. It always protects, always trusts, always hopes, always perseveres."

# 2024-11-21 #
* Today, we engaged in an in-depth discussion about the themes and profound ideas presented in the videos, which prompted a variety of unique interpretations and diverse opinions among the participants. The atmosphere in the room felt somewhat disconnected, as I observed a noticeable gap in understanding and communication between the students and the professor, which seemed to hinder a truly cohesive conversation. This experience reminded me of the sobering words from Scripture: “There is no one righteous, not even one; there is no one who understands; there is no one who seeks God.” This verse felt especially relevant as I reflected on the nature of humanity and its constant struggle to comprehend spiritual truths.
* Despite the efforts made to bridge the gap, it became clear to me that there is often a barrier between intellectual exploration and the deeper, spiritual wisdom that comes only through a genuine connection with God. This realization filled me with a sense of longing for a greater understanding, not only for myself but also for those around me who seemed to be searching for answers without realizing the true source of all wisdom.
* The discussion left me with a heavy heart but also a renewed determination to seek God more fervently in my own life, as I felt increasingly convinced that without Him, true understanding and righteousness are impossible to achieve.

# 2024-11-28 #
* Today, we viewed a thought-provoking and deeply impactful video that examined the dangers and widespread effects of false reporting and the pervasive presence of fake news in today’s media landscape. The content was both disturbing and eye-opening, as it specifically highlighted how anti-semitism has been utilized as a tool in propaganda, spreading hatred and division through manipulated narratives. Witnessing the depth of deception presented in the video was a shocking experience for me, as it revealed the extent to which truth can be twisted and weaponized to serve harmful agendas.
* As I processed the implications of what we watched, I couldn’t help but draw connections to the visions and prophecies described in the Book of Revelation. The more I read and meditate on these biblical passages, the clearer and more vivid they seem to become, as if the events unfolding in our world are echoing the words of Scripture in an unmistakable way.
* This experience left me with a profound sense of urgency to remain vigilant and discerning in the face of such widespread deceit. It also deepened my conviction that turning to God and His Word is the only way to navigate a world where lies and half-truths so often prevail.

# 2024-12-05 #
* Today felt like a day filled with unique and extraordinary accomplishments that left me with a sense of awe and gratitude. As the day progressed, I couldn’t help but dream about a future where I could escape the hustle and chaos of modern life to live on a peaceful farm surrounded by the beauty of nature. I envisioned a life where dogs and other animals roamed freely across vast, open fields, creating an atmosphere of pure joy and serenity that felt incredibly fulfilling.
* This vision of simplicity and harmony contrasted starkly with my observations of how people in the world are so tirelessly seeking love, yet often fail to look toward God, who is the ultimate source of all love and goodness. It is perplexing to me that so many choose to search for fulfillment in fleeting things, while God, who is love itself, patiently waits for them to turn to Him and experience a love that is pure, eternal, and unmatched by anything else.
* These thoughts left me reflecting on my own journey of faith and my growing desire to share the truth of God’s love with others. I feel deeply compelled to help people understand that by seeking God, they can find not only love but also peace, joy, and purpose beyond anything the world could offer.

# 2024-12-12 #
* In today's class, all the groups actively presented and discussed their final project plans, along with the specific next steps they intend to take for execution; however, it became apparent that some groups seemed uncertain about the direction they should take to move their projects forward effectively. This uncertainty highlights the need for additional time and effort to reflect on and clarify what each group truly wants to achieve, ensuring their chosen direction aligns more accurately with the goals of their project and the article they are working on.
* Within our group, while the discussion revealed some differing opinions and perspectives on the approach to take, this diversity of ideas provides an opportunity to integrate everyone's thoughts and identify the best possible direction for our article. By embracing and combining these varied viewpoints, we can create a more comprehensive and well-rounded plan that benefits from the strengths of each member's contributions.
* By fostering collaboration and maintaining an open dialogue, our group can ensure that every member feels heard and valued, which will not only help us determine the most effective direction for our article but also enable our teamwork to function more cohesively and efficiently as we progress through the project.

# 2024-12-19 Thu #
* successful and unproductive
* Because I successfully prepare for my test of fluid dynamics and unproductive because I knew there were still a lot of time wasted
* Try to take fewer naps during the day, and drink more coffee.

# 2024-12-20 Fri #
* unsuccessful and unproductive
* Because I didn't successfully finish my test and made a stupid error; unproductive because I didn't fully use my time on the important things
* Do the problems quicker and check if there is any error in my answer

# 2024-12-21 Sat #
* successful and productive.
* Because I woke up early and did my job.

# 2024-12-22 Sun #
* successful and unproductive.
* I finished my tasks but felt there was more I could do.

# 2024-12-23 Mon #
* successful and unproductive.
* I successfully plucked up my courage to try something new; unproductive because I sleep too many naps today.

# 2024-12-24 Tue #
* successful and productive.
* I did my work and did my best.

# 2024-12-25 Wed #
* unsuccessful and unproductive
* unsucessful because I didn't memorize my script for my presentation; unproductive because I was too busy thinking about the unimportant stuff

# 2024-12-26 Thu #
* successful and productive
* Because I woke up early and focused on my tasks. I spent more time on what mattered and avoided distractions.
* Keep focusing on the important tasks first, and avoid checking my phone too often.

# 2024-12-27 Fri #
* unsuccessful and unproductive
* Because I didn’t manage my time well, and I spent too long on less important tasks.
* Use a timer for work sessions and take breaks only after completing key tasks. Prioritize things that need to be done the most.

# 2024-12-28 Sat #
* successful and productive
* I worked efficiently and finished tasks on time. I also studied for my upcoming tests without procrastinating.
* Continue waking up early and setting clear goals for the day. Stay focused and limit distractions like social media.

# 2024-12-29 Sun #
* successful and unproductive
* I completed my tasks, but I feel like I could’ve done more, especially in terms of quality and not just quantity.
* Aim for better quality work by focusing on fewer tasks with more detail. Plan my day with a bit more structure to leave room for deep focus.

# 2024-12-30 Mon #
* successful and productive
* I focused on my studies and avoided unnecessary breaks. I even had a chance to work on my personal projects.
* Use a checklist to stay on track and remind myself of the small tasks that can add up to something big. Keep distractions to a minimum.

# 20224-12-31 Tue #
* unsuccessful and unproductive
* I was distracted with too many things, and didn’t accomplish my goals. I also wasted time thinking about trivial matters.
* Take a moment to review my priorities and set clear goals before starting the day. Keep focused on one task at a time.

# 2025-01-01 Wed #
* successful and productive
* I finished a big chunk of work and made significant progress on my project. I took fewer naps today and felt more energized.
* Continue managing my energy by pacing myself throughout the day. Take short breaks but stay focused on completing high-priority tasks.

# Five ways to be more productive #
* Pray and ask God for strength and power.
* Turn off notifications and regulate app usage.
* Take a 15 minute power nap.
* Plan your day and make a to-do list.
* Ask your friends' study progress.

# 2025-01-09 #
* Obituary Reflection: In my obituary, I would like it to highlight my contributions to advancing sustainable engineering solutions and fostering innovative education. The two individuals I admire and would like to emulate are Elon Musk for his relentless pursuit of technological innovation and Mother Teresa for her selfless commitment to improving humanity. Their achievements were enabled by their perseverance, visionary thinking, and deep commitment to their values.
* Action Plan: I am currently working on implementing the selected solution for my Nordling Lab project. I aim to address the problem effectively by gathering evidence, quantifying its impact, and proposing a practical solution. This week, I will focus on making a draft video to present the solution.
* Progress Summary: My tasks for the week include finalizing the problem analysis, crafting a detailed script for the video, and ensuring it aligns with the provided structure. By staying organized and leveraging feedback, I aim to produce a meaningful and impactful 3-minute presentation by the deadline.