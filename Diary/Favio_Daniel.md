# 2021-02-16#
* introduction to the course
* we made groups based on numbers which was an interesting way of grouping
* We watched a video of elon musk and I learned that the cost of things will go down by a large scale in the future

# 2021-02-23#
* we watched a ted talk
* i learned that overtime the cost of solar panel decreased and the cost of nuclear energy went up, so the solar energy is way cheaper now
* the gap between presentations are so long, which takes time of other people that have to do their presentation

# 2021-09-30#
* I was very confused about the group thing
* liked the first ted talk, the second was not very useful
* more people presented today
# 2021-10-07
* when using an image for presentation, you have to write the date you visited the website
* the first break was funny
* realised that I know little about financial system
* learned that money's value comes from our belief in it
* didn't get time to watch the videos so we're watching on our own.
* poor people are the one's making the money for society
# 2021-10-21
* Talk about exercise health benefits. 
* No matter what time you exercise, you'll get the benefits anyway
* learned how the emotions play an important roll in education and can make you learn slower or faster.
* got confused about the grading system.
* we did an interesting experiment, and realised many people are stressed.
* fasting is not for everybody, depends on the individual.
* poor people are the one's making the money for society.
# 2021-10-14
* New things will be to start recording the presentations, which is a good idea so will let more people have the opportunity to present.
* i think it will be on youtube
* we create imaginary things that lives among us
* nationalism creates hate between humans
# 2021-10-28
* realised that it is easy to talk to people with depression, but the thing is that we  often feel responsible for their depression, so instead of talking to them we just try not to.
* happy people are often not as happy as we might thing, on the contrary, they could be masking up their emotions to deal with reality.
* "people can be sad and okay at the same time". Really liked that phrase.
* All it takes is to listen to what somebody has to say, you don't really need to say anything, just listen. 
* Also took the time to watch another ted talk from the survivor of the golden gate, the one mentioned by Kevin Briggs and I think is worth watching to understand more about the whole story.
# 2021-11-25
* I completely forgot to write on my diary for like three weeks because my mind was very occupied with all the midterms and reports to be done each week, and now I'm changing my strategy and will start to write the diary in class.
* Today I woke up late and could not attend the first class of the day, one about animal human history. 
* I do not know if only happens to me but I always receive the mails and moodle stuff so late, one day I received a mail from moodle to submit a report that was due for 2 days before.
* That is one the reasons why I came late to the class.
* Rene had to tell me that today we have physical class.
* I am worried about my grade on this course because it will have a huge impact on my final gpa and I need around 75 gpa to be able to transfer to another department.
* I have a problem with reading, that makes me have a hard time understanding when reading. For example, I have to ask my classmate what are we doing for this class because the tasks are vert confusing.
* I feel like the more sophisticated the words are, the more I get lost.
* I did not expect the class to be this cool, looks very futuristic and the screens helps with the seats and everything just looks cooler.
* Professor just said that we will start with presentations again and I think would help me develop public speaking skills, because I am not good at it.
* We watched a ted talk about the internet, about passions and supernatural models. 
* Ads became better.
* VR glasses are gonna get cheaper in the future, but our incomes might go down so It could be at the same price.
* Even those who come in the last place get a medal and it devaluates the value from it.
* we're good at showing people that our lives are perfect when in reality our lifes are shit. 
* living in the instragram and facebook world are making us feel worse.
* instagram is probably going to make youngsters feel discouraged and sad out of significant social applications, 
* instagram is one of the most harming web-based media in the world
* If you wake up and the first thing you do is use your phone before even say hi to your partner you clearly have a serious problem with addiction to social media.
* We care more about the year than the lifetime.
* I feel like since we started to use social media on a daily basis things are just getting worse and worse, because we became very dependent. Some people get really angry when they're not on their phone.
* I remember my grandma saying that I don't have to talk to her before she had her first cup of coffee in the morning and that is what an addict would say.
* When you remove the temptation it gets easier to not fall into the bad habit.
* Let the phone charging in another room because if you wake up in the middle of the night you won't be checking your phone.
* Also smart phones makes your night difficult because it interferes with your circadian rythm.
* We made group discussion and it was very funny.
* We called our group chess table because we had a chess board in our table. 
* I have never written this much but I feel like my grade is gonna go down, so I'm writting this long. Sorry
# 2021-12-09
* I wrote the last week diary but can't see it here
* finals are coming and honestly I am not prepared for it.
* just want this semester to end already
*　Ｂａｓｉｃａｌｌｙ　ｗｈａｔ　ｉ　ｓａｉｄ　ｉｎ　ｔｈｅ　ｏｔｈｅｒ　ｄｉａｒｙ　ｗａｓ　ｔｈａｔ　ｉ　ｆｉｎｄ　ｔｈｅ　ｉｎｓｔｒｕｃｔｉｏｎｓ　ｏｆ　ｔｈｅ　ｔａｓｋｓ　ｓｏ　ｄｉｆｆｉｃｕｌｔ　ｔｏ　ｕｎｄｅｒｓｔａｎｄ．
＊　ｍａｙｂｅ　ｉｔ＇ｓ　ｊｕｓｔ　ｍｅ　ｂｕｔ　ｉｄｋ．
# 2021-12-16
* Today we did a votacion (no me acuerdo como se decia en ingles), and I feel like we should really focus on the pdf thing.
* because the school will be a better place with pdf instead of the heavy and sometimes useless books.
*　to be honest, most of the books we buy are of no help, only works for solving the problems that the professor gives.
* like, this edition has new problems and shit.
* so changing to pdf would mean that; first, our backs will be saved because of the heavy weights, and our pockets too.
* the university has to provide pdf for everyone, but of course that's not gonna happen :(
* Tomorrow is my applied mechanics test, hope to do it better than the last two exams.
* Hopefully this time my diary will stay and NOT disappear like last time >:( xdd
# 2021-12-23
* today i didnt go to class because I was feeling so bad due to the lack of sleep, but fortunately my friend told me what we have to do.
* I have not been productive today, because I was too tired to make decisions.
* so I had to take a full 5 hour of sleep to get back in track and now I'm finishing a project and studying for tomorrow's exam.
* unfortunately it is gonna ruin my sleep schedule but that's okay if I get to pass the class.
* So today was not a successful day
# 2021-12-24
* not a productive day
# 2021-12-25
* not a productive day and forgot to write the diary
# 2021-12-26
* today was a productive day, woke up early and finished my tasks so early that I could go out and enjoy.

<<<<<<< HEAD
# 2021-12-02
* We did a little experiment in class today.
* professor basically took our phones
* I don't feel like I want to get my phone back during the class time.
* We are gonna save at least 10 people, we're superheroes.
* Closing the church could be a good idea :'D jk
* Construction noice is worse.
* The government have to pay us because every night we have to hear old people sing bible stuff xd
* Today I'm gonna write a bit shorter than the last time
=======
# 2021-12-30
* unsuccessful because i failed to write the diary again
* also because I didnt do any school homework 
# 2021-12-31
* last day of the year yay!
* successful but not productive
* because I didnt do any homework but I got a great workout, that for me is very important
# 2022.1.1
* unproductive because I only watched the spiderman series
* successful cause I worked out
>>>>>>> 3e724ad633b242adbfb71ab11271897cb714b8ac
# 2022.1.2
* unsuccessful because didn't do much
# 2022.01.03
* today's my birthday lol
* very productive cause I've finished an annoying essay
# 2022.01.04
* today is gonna be the day that they're gonna throw it back tooyouuuuuuuuu
* today was very productive, woke up early and did everything by 12 pm
* next week the 5 days i got shit to do and its so stressing
* to be honest, this semester was awful. made bad decisions in choosing classes and didn't have time to drop it because I was focusing on the harder courses.
* I'm concerned about my grades, cause this semester's grades are important for me to transfer to another department
*　Because I also chose the wrong department lol, but I guess is how life works and yeah.
* I'm excited for winter break cause then I could go to the mountains and feel alive again.
# 2022.01.05
* not a productive day cause I couldn't sleep last night so woke up with bad temper today
* winter is coming!!!
# 06-01-2022
* for some reason this class 的 grades makes me feel so uncomfortable, because it's very unsure and I need a good gpa in order to transfer
* i dont know if we are still doing the weekly thing
* i have a final next friday and my life depends on that test
# 2022-01-20
* Love's warm softness appears to be a thing far eliminated from the cruel truth of science. However, the two do meet, regardless of whether in lab tests for flooding chemicals or in severe chambers where MRI scanners uproariously thud and friend into cerebrums that light at looks at their perfect partners.
* With regards to contemplating love, artists, savants, and surprisingly secondary school young men looking groggily at young ladies two lines over have a huge early advantage on science. 
* In any case, the field is gamely dashing to get up.
* Richard Schwartz, academic partner of psychiatry at Harvard Medical School (HMS) and a specialist to McLean and Massachusetts General (MGH) clinics, says it's never been demonstrated that affection makes you truly debilitated.
* however it raises levels of cortisol, a pressure chemical that has been displayed to smother safe function.
* Love additionally turns on the synapse dopamine, which is known to animate the cerebrum's pleasure focuses.
* The oxytocin helps concrete security, raises safe capacity, and starts to present the medical advantages found in wedded couples, who will more often than not live longer, have fewer strokes and respiratory failures, be less discouraged, and have higher endurance rates from a significant medical procedure and malignant growth.
* Love and friendship have left permanent imprints on Schwartz and Olds. However they have separate vocations, they're isolated together, working from discrete workplaces across the corridor from one another in their masterful Cambridge home. Each has an expert practice and freely prepares psychiatry understudies, yet they've additionally worked together on two books about depression and one on marriage. Their own association has kept going 39 years, and they brought up two kids.
# 2022-01-20 (attendance score)
* 1) Know the current demographics: a feasible way to know the current demographics would be to read more news because by simply reading the news you'll be able to understand more of the world demographics. Then I’d say to read by yourself about different countries and learn how they are doing.
* 2)Depending on how is the situation in a specific location, engineers could use their knowledge of demographics to understand what kind of projects they could develop there; let’s set Paraguay as an example, we have two dams that provide energy to the whole country, prior to that engineers had to do their research and then realized that the project was feasible.
* 3)First of all, I’d read books about it because that is the easiest way to get an understanding of it. Talking with people that know about it, go to seminars. Also, youtube’s got very good and easy-to-understand materials.
* 4)Take Venezuela for example, a rich country that had very poor resource distribution throughout its history, and now since the currency got devalued people cannot afford to buy the basic necessities. My family is always filled with new information because they read too much and teach us how things work. I have some friends from Venezuela and I could ask them how their situation really is because sometimes the news is not enough for understanding and for me personally I think that talking to the people who actually live in the specific location is the more accurate thing to do.
* 5)In the company I used to work in prior to coming to Taiwan, we always were involved in seminars that teach you about the future and the impact of artificial intelligence. Also, I have a cousin that works for google and would be able to give me the necessary information for me to understand the topic very well.
* 6)Take the food industry as an example, I have watched many documentaries about it and I plan to research more about it, it has been developing for centuries now and we know more strategies to eat cleaner and get rid of diseases. Also, I recently learned that in order to build a floor in a building you have to consider the dead weight of the furniture and the variable weight that would be people.
* 7)It’s well known that relationships give more chances of survival. I have been living in Taiwan for 2 years and I would not have made it if it wasn’t for the people, especially the Latinos in the university, because they guided me and did not make me feel alone. 
* 8)My brother had depression for a couple of years now, at first I did not believe it until I researched about it. Reading articles really helped me understand what he was going through and also talking to people and listening to their experiences, since it is a very common thing among people, sadly.
* 9)There are really good books out there for learning about this topic, and also watching that ted talk made me comprehend more of how to actually find a partner for life. Ask my parents for advice because they have already experienced this and could give me a really good point of view on things.
* 10)Have more critical thinking and skepticism because believing everything you read or hear from others could have a negative impact on our lives. And personally, I don't like to fall into those traps so getting informed and reading from different sources is a pretty good idea.
* 11)Professional skills class is a good method of learning how to really analyze and understand data, other ways would be taking an online course about it, there are plenty of them and at a very affordable price or sometimes even for free on youtube.
* 12)At the place I used to work we were always put in different groups to do different tasks and honestly, I believe that the leader has to have the ability to choose who they wanna work with, of course, most of the time you have no choice but just think about it if you work with lazy people you have to do most of the work because they won't and we all get the same wage.
* 13)Well, I’ve learned that through my uncle, he is the owner of a currency change business and he got there because of his social skills, because he was not talented at speaking Portuguese which is the main language of his customers but had the connections to make it work, he got pretty far in his business because of how social he was and how good he can communicate with the people. Also here in Taiwan, I got my motorcycle for a very cheap price because of how I interacted with the buyer.
* 14)I also learned it from a personal experience, the work environment was not very organized and my mom told me that the environment really affects you on a psychological level, for example, when you study at home and it is messy it feels like you cannot concentrate, so by organizing everything and get rid of as many distractions as possible would contribute to a better workplace and productivity.

