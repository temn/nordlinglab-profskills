# 2019-09-12

*    The first week attending the class.
*    Conflicts are not always bad, rather we can know more about each other better.

# 2019-09-19

*    the world today in general it is more peaceful than ever.
*    The negative media reports are certainly the reason why most people have a gloomy view of our world.

# 2019-09-26

*    Proper referencing which is a skill that should be second nature.
*    A majority of fake news is a deliberate act of misinformation and is not bad journalism.
*    Hoax can lead to riot

# 2019-10-03

*    Banks are just financial intermediaries, they don't take deposits or money but borrow from the public.
*    Most engineers not taught about economics when it plays a big factor when it comes to funding for their projects etc.  

# 2019-10-17

*    Facism is what happens when people try to ignore the complications and to make life too easy for themselves.

# 2019-10-24

*    Only a healthy engineer can be a good engineer.
*    Exercise helps to increase supply of oxygen to the brain, hence leading to the increase in neurons produced.
*    Regularly working out increases our attention span.

# 2019-10-31

*    Depression is not about being sad.
*    Depression is the cause of most of the diseases and illness and also suicide.
*    When dealing with people with depression, don't argue, try to listen also encourage them to seek professional help.

# 2019-11-07

*	Simulating our own death make us reevaluate our purpose on life.
*	Persuing happiness is ironically make people less happy.
*	instead we should acknowldge the fact that sometimes life is painful to be happy.

# 2019-11-14

*	Today topic is about law, I don't know much about law, to be honest. All of the information and unfamiliar terms are a bit overwhelming to me
*	The police in this video are very rude and don't respect human rights. They supposed to be public servant.
*	"We are slaves who live inside a prison and only a few of us have realized it.", I find this quote to be mindblowing.

# 2019-11-21

*	Technology we wanted could destroy ourselves. They could manipulate our behaviour.
*	as I used the Internet through smartphones, I was easily sympathized with the biased response of public opinion to the press article.
*	The millenial generations are not actually self entitled. They are just raised with bad parenting and large amount of social media usage.

# 2019-11-28

*	By getting your news from a variety of sources, you can not only find the truth but also the bias, lies, and corruption that drives certain media. 
*	Many sites twist information piece by piece so that they can make you conform to their beliefs.
*	Research is great but not too many people have the time for that.	
*	greed has always been the fall of countries seen to be as wealthy.

# 2019-12-05
*   the planet earth is the most important wealth of human beings. we should protect our home.
*   we have 9 planetary boundaries and some of thrm has been exceeded.
*   i can't really differentiate what she mean between grow and thrive.
*   it's important to see the world as a whole, andn not focused on pursuing the profit

# 2019-12-19
*	2019-12-19
	a. successfull and productive
	b. just finished delivering a presentation.
	c. try to wake up early
*	2019-12-20
	a. unsuccessful and unproductive
	b. did wake up early, but can't focus in the class.
	c. write a to-do list
*	2019-12-21
	a. unsuccessful and productive
	b. got all-day long shift in my part-time work, too tired to study after back from work
	c. try meditating
*	2019-12-22
	a. successful and productive
	b. go to the church, back to work
	c. excercise before doing a task
*	2019-12-23
	a. successful and productive
	b. hit the gym, discussing the final presentation, and then prepare for the final test
	c.
*	2019-12-24
	a. successful and productive
	b. feeling energetic all day, celebrating the christmas eve
	c. 
*	2019-12-25
	a. unsuccessful and productive
	b. feeling lazy, skip class, and use the time to do homework and take a nap
	c.
	
# 2019-12-26
*	Today, each group have to present about various topics

# 2020-01-02
*	Making things go viral in this digital era is surprisingly easy. Any ridiculous, random, thing you can come up, might go viral when done right.
*	We get to present our solution of problem
* 	Doing a brainstorming session. Most voted ideas are going to be presented for final project.