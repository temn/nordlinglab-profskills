# 2024-09-12 #

* sadly I wasn't there in the first Week
* Elias Neumaier luckily told me about this course

# 2024-09-19 #

* The conflict behaviour was very interesting for me and I'm supprised why I never heared about it before.
* I learned about GIT and the impact Linus Torvalds had on the world.
* Listening to different presentations about abundance was quite interesting.
* I learned how to improve a presentation and to make sure that the collected data really is the reason for a cause.
* I understoof Wright's law 
* I know now to allways write a diary after the lesson

# 2024-09-26 #

* In ten years, I will be 30years old and I see myself living in the south of Germany, in the Alps, with my girlfriend. Our dream is to have a small farm, where my girlfriend can follow her passion for horses, and we can enjoy the mountains. Living here means we could do activities like hiking, mountainbiking and skiing, while also staying close to my family and where I grew up. By then, I hope to have finished my Master’s degree in Renewable Energy in Würzburg, and maybe even a Ph. D in a Similar topic. I'll be working for a small start-up focused on photovoltaics, mostly working online so I can travel around in a van doing road-trips while staying connected to my job.


There are a few trends that will shape my future:

* **Climate change**: As someone studying renewable energy, I want to help find sustainable solutions, and the climate crisis will definitely influence my career choices. But it also makes me think about tough personal decisions, like whether it’s morally acceptable to still have kids in a world that might be dealing with serious climate issues.
* **Technology development**: Advances in artificial intelligence (AI) and renewable energy, like photovoltaics, will change the way we live and work. AI is likely to make remote work even easier and help us in creating more effective solutions. But there is also the danger of being replaced by AI.
* **Political instability and conflict**: The conflict between Russia and Ukraine shows how easily the world’s stability can be shaken. Plus, the rise of right-wing parties in Europe is concerning, as it could mean trouble for some of my good friends from Eritrea and Ethiopia who might face discrimination or even deportation.
* The **stock market**: I’ve already invested a fair amount in stocks, and how those investments perform will play a role in what I can do later on. If things go well, it would mean more freedom to travel and help fund our farm. But I know the stock market can be unpredictable, so I need to be ready for ups and downs.
* **The decisions my friends make**: This will have a big impact on my life. Staying close to my friends is important to me, and where they decide to live will influence where I want to settle. Whether they stay in Germany or move abroad, I want to be nearby and keep those relationships strong.

# Fake news on Facebook #

Trump vs. Harris: The Truth Behind the Lies

During the first Trump-Harris debate, a viral post by user Insurrection Barbie claimed that Kamala Harris lied 17 times in just the first 10 minutes, quoting: "CNN's official fact-checker says Kamala lied 17 times: ‘It would be easier to count truths.’"

This post sparked a heated discussion, with many in the comments backing Trump and accepting the claim at face value. Others were more skeptical and started digging into different sources.

The Truth?
According to CNN's official fact-check report, the reality was quite different. The analysis showed that Trump lied over 33 times throughout the debate, while Kamala Harris lied just once. However, CNN also emphasized that both sides made half-true claims or statements that needed more context.



# News story from various sources #

The different views from various sources on the aid worker's death in Gaza include:


* **Reuters, U.S. News and Barrons:** Describes the incident as a case of mistaken identity, according to Hamas-run government officials.
* **Jerusalem Post:** Reports the shooting as a tragic mistake by Palestinian gunmen.
* **The Times of Israel:** Highlights that Palestinian gunmen killed the aid worker in a volley of bullets.
* **Breitbart:** Presents the event as Hamas gunmen intentionally murdering an American aid worker.

* The perspectives vary from describing it as a mistaken identity to emphasizing deliberate killing.

# 2024-10-17 #

* First, we read some diaries from different students about where they see themselves in 10 years. It was interesting hearing other people's goals and dreams.
* The USD Dollar was backing other currencies, and gold backed the USD Dollar. President Richard Nixon changed this.
* Development of money (deposits, loans, M1A, M2, M1B, etc.), how it fluctuates every new year, and how democracy and dictatorship (trust) influence the loan/deposit ratio.

* Five desirable properties of money and how Bitcoin fails to fulfill all five: Fungible, Durable, Portable, Recognizable, Stable


VIDEO: Ray Dalio

* Explaining transactions:
* Lender and borrower create credit/debt.
* Low interest rates -> many borrowers.
* Higher income -> creditworthy + higher spending -> growth of economy.
* Productivity + short debt cycles + long debt cycles.
* Currency is created by giving out loans.


VIDEO: Richard Werner

* Decentralized banks and decision-making often lead to higher success (Germany, China).
* Centralized banks often create cycles.
* Power to the local people.
* Credit is creating the money -> issuing loans.
* Consumers -> inflation.
* Financial assets (houses) -> asset bubbles.
* Companies -> more machines, construction, or renovation.

# 2024-10-24 #

* First, we read some diaries of two students.
* After that, we listened to a presentation about the financial situation of Indonesia.
* student shared his story about the hatred between China and Taiwan, which was really interesting to hear.
* it raised the question: Why do people hate each other, and what are the reasons for war?
* In the end, we watched videos from Yuval Noah Harari, Wendy Suzuki, and Mühlenbeck about the fictional stories of humans, the benefits of exercise on your brain, and the difference between a sick-care system and a health-care system.
* We also discussed what knowledge is: Plato: “well-justified true belief.”



**VIDEO: My Descent into America’s Neo-Nazi Movement & How I Got Out**

* 14-year-old lost kid got recruited by the first skinhead of America.
* Became the leader of this Neo-Nazi group.
* Singer of racist songs.
* Hated everyone who was different without evidence.
* Got identity and purpose back because of his wife and child.
* Lost everything because of his Nazi views
* Opened a white supremacist record store with racist and normal music.
* Contact with other groups (gay, Black, etc.) led him out of the Nazi group; an old Black security guard forgave him and told him to forgive himself.

**Key findings of the story:**

* Potholes (privilege, trauma, neglect, etc.) nudge people in the wrong direction if unresolved.
* Disconnection is the reason for becoming a hateful Neo-Nazi.
* Fear turns into hatred.
* Draw people in, not push them away, and argue to change them.
* Lost children are the best targets for recruitment.



# Theee Fictional Stories #

* **Money:** I went to the Ching-yeh restaurant and noticed I only had 25 NTD. I was already at the counter and had to pay 45 NTD. Nevertheless, I was allowed to eat and pay later.
* **God:** Last week, my friends and I did a temple run. We visited several temples in Tainan, learned something new about the religion, and got predictions for our lives.
* **Studies:** I’m currently looking for a topic for my bachelor’s thesis. It takes a lot of time to find the right topic with a suitable professor and a lot of good literature.


# Future of my Country #

* In my country, the standard of living will have dropped because of another financial crisis. The government is still striving to fulfill its climate change goals.
* There will be more immigrants because of the climate and war.
* The Euro has dropped by 200%.
* The public transportation system was improved, and green vehicles are available everywhere.
* Politics is a topic of great tension, and the right-wing party has gained significant success.


# 2024-11-07 #

* Listened to other people’s diary and how they imagine their country in the future.
* We created a supergroup and combined all of our presentations into a hopefully interesting one.
* Talked about the difference between profitable and non-profit organizations.
* Listened to a very interesting story


# 2024-11-14 #

* presentation about the final project


# 2024-11-21 #

* sadly missed it

# 2024-11-28 #

* We listend to two diaries about different ...
* multiple Groups presented their action ideas about their final project. It will be interesting to see theses ideas in progress.
* The thought of Lifecycle of civilisations is frightening as we are about to face a fourth dark age soon in the future.

# What really happend in Amsterdam # 

* After a football match the fans of an israeli football club attacked citizens by random. Even though there were multiple eyewidnesses and videos about this incidence, REUTERS was unclear about the exact event. This lead to many well known and trusted News medias presenting the incidence as an attack from citizens on the israeli fans. Some even compared it to the Krystallnacht commeting it as a hunt on jews. This is a really scary thought as  many people probably still think it to be an act of racism by the citizens of Amsterdam.

# How to choose your news #

* This video highlights the danger of the concept that everybody can post what ever they want on the internet. It shows when everybody is a reporter, nobody is. The video also talks about the negative sideeffects of a 24h coverage of the news. Every news media wants to be * the firs one to publish the latest news but sometimes forgets to think of the consequences. Only by factchecking it by your self and comparing it to different media you can be sure that what you read is correct. Even though the example of Amsterdams attack tells otherwise.

# 2024-12-05 #


* There is no correlation between press freedom and the presentation of news positively or negatively.
* After three group presentations, three big groups with one group leader and secretary discussed together about one story and the different presentation by news media in different countries. This was interesting to hear, but the group dynamic for this task was even more interesting and showed different work ethics.
* **Planetary boundaries**
* The effects climate change will have on nature, animals, and humans will be enormous. For example, the cost caused by climate events will lead to a huge economic loss of 18% of the GDP until 2050. For 1,000 years, the temperature was very stable (around 14 degrees), enabling civilizations to rise and economies to thrive. Now that we leave this comfortable temperature range, scientists are getting really worried because several CO2 sinks are turning into CO2 sources. One example is the Amazon rainforest, known as the Earth’s lung. This is really worrying because the Earth is very good at absorbing CO2—around 31% of the emitted CO2—but it is slowly losing this ability.
* Another CO2 sink is the ocean, with an absorbance of 19%. A new problem that occurs is a sudden temperature rise, which can have unknown and extreme consequences. The problem is that scientists don’t know why this is happening, and because of that, we can’t look for any solutions apart from fighting climate change.
* With all the events occurring, the danger of crossing tipping points arises. There are 16 very important tipping points, of which we have already crossed 5 by exceeding the 1.5-degree range. And we will probably cross the 1.5 degrees within the next 5 years.
* Things that will help us take the necessary steps are the planetary boundaries. They show the boundaries of the planet we shouldn’t cross; otherwise, the end of the Earth is coming closer. The problem is that we are crossing nearly every planetary boundary, which will cause huge problems in the long run. So we need to find solutions quickly, or the end of our planet will come sooner than we think.

# 2024-12-19 #
# Diary #

* **2024-12-19:**
* A. Successful and productive 
* B. I felt successful because I achieved a good work life balance by studying and meeting friends in the evening for 3h of pool. I felt productive because I didnt waste to much time on my phone and studied quite a lot.
* C. I will wake up early so that I can achieve all my goals for the next day.


* **2024-12-20:**
* A. Successful and productive
* B. studied from 10AM to 5PM. After that I met some friends for Mahjong and went to a birthday party until 6AM.
* C. I dont expect the next day to be that great after partying so I try limit my expectations for the next day.


* **2024-12-21:**
* A. Unsuccessful and unproductive
* B. Woke up very late and had to take a train to Hualien really soon. Didnt achieve to study as much as I wanted on the train.
* C. Wake up early so I can enjoy fhe following biketrip.


* **2024-12-22:**
* A. Successful and productive
* B. We biked 102km from Hualien to Yuli and saw the beautiful landscape of Taiwan. Didnt waste any time on my phone.
* C. I try to set up a clear time plan so we wont miss the train back to Tainan without being stressed.


* **2024-12-23:**
* A. Successful and productive
* B. Biked 93km to Taitung and just caught the train to Tainan. Used the time on the train productivly by catching up with the diary.
* C.I try to sleep enough so that I have enough energy for the next day.


* **2024-12-24:**
* A. Successful and unproductive
* B. Spend most of my time looking for christmas presents and celebrated christmas with all my friends until 4AM.
* C. I will try to do a powernap in the afternoon so I can achieve all the things i need to do.


* **2024-12-25:**
* A. Unsuccessful and unprductive
* B. Worked on tasks that still have time to be completed rather than tackeling the pressing problems because I dont want to do them. Was tired the whole day. 
* C. Go to bed early and make a plan to have a productive morning.


# five rules that make me more successful and productive
* 1) Create a specific plan for the next day with explisit time frames for each task
* 2) Meet with friends so I have something to look forward to 
* 3) Limit the time on my phone. Its just a waste of time
* 4) Sleep enough
* 5) If something takes less than 5 minutes to complete just do them. Otherwise they will bother you fot the rest of the day.


# 2024-12-26 #

* **2024-12-26 (Thursday)**
* A. Unsuccessful and Productive
* B. Stressed out because of a big essay deadline. Worked a lot on the paper and got closer to finising it of it done.
* C. Wake up early with a clear to do list and stay of the phone in the morning.

* **2024-12-27 (Friday)**
* A. Successfull and Productive
* B. Worked the whole day on the essay and finished it 5min before the deadline.
* C. Make plans to use the day productively even though all deadlines are over.

* **2024-12-28 (Saturday)**
* A. Unsuccessfull and unproductive
* B. Bought several christmas gifts and souvenirs for my family back home. Wasted a lot of time on my phone.
* C. Meet friends a plan the time I have left in Taiwan.

* **2024-12-29 (Sunday)**
* A. Unsuccessfull and unproductive 
* B. Continued to work on the presents and met with friends in the evening. Still wasted to much time on my phone.
* C. Restrict the amount of time spent on my phone.

* **2024-12-30 (Monday)**
* A. Successfull and unproductive
* B. Donated Blood for medical research comparing caucasian and taiwanese blood. Finished all presents and did everything on my To-do list. Set up a new plastic bag station with 林怡成.
* C. Wake up early to have a productive morning.

* **2024-12-31 (Tuesday)**
* A. successfull and unproductive 
* B. Went to Taipei at 11AM to celebrate New Years. Met all my friends there and had a lot of fun until 6AM
* C. Take enough powernaps so that i dont fall a sleep.
 
* **2024-01-01 (Wednesday)**
* A. Successfull and productive 
* B. Arrived in Tainan at 10AM and slept until 13PM. Did a lot of small things I wanted to do a long time ago. Met with some friends in the evening
* C. Go to bed early enough and start the day with some stretching.



# 2024-01-02 --- Orbituary # 

* **Frerik Schlief helped to pave the way to a sustainable world - dies at the age of 87**

* Greta Thunberg: At the age of just 15, she initiated "Fridays for Future," a global movement advocating for stronger environmental action. The movement gained significant popularity especially among students, providing them with a platform to demand a sustainable future. Thunberg even addressed the UN Climate Summit, delivering a speech that captured international attention.
*   --> She was not afraid of risking expulsion from school or standing alone in her beliefs. She remained consistent in her activism, continuing her protests despite widespread criticism and hostility. She also didnt back down from the task of speaking in front of the whole world at the age of 16 years demonstrating courage and determination.

* Daryl Chapin: A brilliant inventor of the 20th century, he made significant contributions to science and technology. Among his achievements, he worked with Bell Laboratories to develop the first modern silicon solar cell. This invention was both efficient and practical, laying the foundation for the commercial use of photovoltaic systems and providing a sustainable energy source. In 2008, he was inducted into the International Inventors Hall of Fame.
*   --> He recognized early on the potential of clean energy and remained dedicated to his passion for science. Through tireless effort, he contributed to improving solar technology. He also worked in a talented team with more brilliant people.