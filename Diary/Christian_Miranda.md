

# 2023-09-21 #


* My future in 10 years...

* I think that planning ahead for 10 years is a probably a huge task for a lot of people since most people don't even know what they'll be eating tomorow. However, its good to have an idea more or less how your life will turn out.
* In 2 years I should be graduating with my Bachelor's Degree in Energy Engineering. In between those two years, I plan to probably undertake some research in Fuel Cells since I took a course with my advisor and found it to be something I can delve into. With the third industrial revolution on its way it should be pivotal as an alternative to energy storage devices.
* After these 2 years, I am considering looking into some type of entrepreneurship with a focus on renewable energy. This, I hope to establish back home in Belize since there aren't many already established industries.
* In 4 years, after having completed my bachelors I hope to finish my Masters either in Taiwan, Canada or New Zealand.
* During the 5th year I think that will be it for studying as I don't intend to pursuing heavy research or delve that much more into academia as it would probably take too much time.
* I will get more serious in the job market as my qualifications should be enough to land a good job and I should more than knowledgable in my field. I don't intend on changing the world but rather make small important contributions. Like the creator of Linux, Linus Torvalds, sometimes its better to have people who don't gaze off into space but rather focus on the things already there.
* This 5th year I should also be married and perhaps in the process of having kids. I intend on living in New Zealand or Canada. Taiwan is too hot!
* From then on from the 5th to 10 years I intend of having my own business focusing on energy technologies with branches in Taiwan, New Zealand/Canada and Peru. Also I would like to work for a more well known and reputable company or industry.
* I love to share what I know so I would like to teach energy technolgies either at colleges back home or probably in discussions stemming from my business or the company where I work at.
* During the 6th-7th year things should be far different and perhaps there will be other things that get implemented so I will be looking into investment opportunities and also purchasing land in my country and Peru since land is always increasing in value.
* I will update this once something happens.

# 2023-10-15 #


* This week's lecture was indeed interesting when it comes to finances.
* I found it rather philosphical the concept of what gives money its value.
* As usual, the US always has to put a twist on things. I mean it makes more sense to have money be backed by gold and not by what we believe.
* Honestly, I think this is why we have so much "crap", for lack of a better word, arising when it comes to cryptocurrencies.
* All in all, we would probably be better off with a barter system.
* I missed a couple weeks of diary, oh no... But this week was good albeit a little stressfull. I mean, what else can a student at NCKU expect.

# 2023-10-21 #

*1) Currency: I once had a friend who, instead of pursuing happiness and peace of mind, pursued the quickest way to obtain money as he believed that all the other problems in life would be solved with money. I, on the other hand, don't care that much about money and instead focus on pursuing happiness and helping others first. I believe that being a good human being is what solves all of life's problems.

*Tangible example: I saw a story from my friend exclaiming how miserable he was despite chasing and obtaining what he had primarily sought. Conversely, I have received many at times a helping hand from people who in return have asked nothing from me.

*2)God: If you are of Christian belief, and I'd imagine that it goes for most religions as we are all the same just of different names and appearances, God is always portrayed as this omnipotent and omniscient being. This is true, and I would give scientists til the end of time to prove me wrong.

*Tangible example: Last week I smiled for no reason, despite being bombarded by a plethora of debts and issues. For some reason, a smile is the best weapon and shield one can carry in this life since we cannot control outcomes but just the way we react to its circumstances.


*3)Career: My friend originally wanted to become a doctor but later down the line realized that he could help people in many different ways.

*Tangible Example: I have started to appreciate my current major even more so and have realized that I can help people in more than one way.



*Story about a Politician

Shyne Barrow

From rapping about being a "Bad Boy" , no really , he did help write and sing the song "Bad Boyz" to now possibly leading a nation. What awaits our nation that has once been fooled by rhythmic drums, marching towards a brighter future? Perhaps the record scratching of a DJ with a repetitive loop analogous to that of a rap song. Indicating a constant loop of crime and violence covered up, as most corruption is in any third world country. New rap songs have a new featured artist, perhaps new crimes will include a new politician every time. Rappers rap about money without any account of it, Belize's finances too will be spent on booze, strippers etc. It makes for a better rap video. Unlike the shiny diamond grill, our country's future 

# 2023-10-26 #


* This week's lecture was very informative and spoke about very interesting topics.
* The professors question on whether we would pay to be healthy everyday or pay only when we get sick was a very philosophical question but can be argued either way. It really struck
* home with me because I recently went to the dentist after a really long time and I undoubtedly regretted it since I had to get a tooth filling. I guess theoretically prevention is better than a cure but I mean come on, who has the time
* and money to be that consistent.
* I'm all for exercise but I can't help but cringe thinking about this new-age exercising community setting bullshit standards for everyone. Exercise to be healthy, not to be better than anyone else.


# 2023-11-02 #


* This week's lecture spoke about mental health with topics such as depression, anxiety and suicide.
* This really struck home with me since my dad actually went through depression in his life. I was perhaps 8 at the time was able to 
  witness the dreadful effects from him losing his job, to not eating and pacing at odd hours of the night just worried. It ended up leading to him
  leaving my mom and us and going to stay with my grandparents who lived in a more rural area. Overall, he was able to overcome it but it was not an overnight process.
 * I believe that most people who speak about mental health are hypocrites and should not try to compare themselves to somebody's life as each person's struggle is unique.
 * We should play our part when we notice that someone is not right but accept that we are not superheros and ultimately cannot save people, rather only help them in the
   way they "WANT" to be helped.
  * As I've gotten older, I have gotten better at dealing with my mental health and overall I have learned to not worry about everything, everywhere all at once and just live one day 
    at a time. Like Dory says "Just keep swimming.. just keep swimming... swimming... swimming"
  
 
# 2023-11-09 #
  
* This week's lecture spoke about choosing happiness/fulfillment which was very thought provoking to say the least.
* The professor asked if we had taken any courses that emphasized on professional skills to which two students answered. I think the girl that spoke didn't really understand
  what he meant by professional skills since she mentioned a shop class as one. At the end of the day I think this is the thinking of most of my classmates, they just want what they need for a job
  everything else is just frill. Which makes me think that life must be really one sided for them and that it explains why some students jump off buildings as failure to pass means failing at life.
  What a load of bull***. Life is about making mistaked and finding what makes you not only happy but at peace. 
  What use is a high paying job if you loathe going to work and just do it for the money?
* I also think that a lot of classmates don't care about work relationships since they never bother with forming relationships with fellow classmates. Not to mention one with 
  a dark "老外" like me. Why? I don't know why. But I cherish every interaction I do get and from my experience working, have formed bonds that still help me to this day.
* All in all, we need to be careful with what we think happiness is and should remember that it is much more than material. 

# 2023-11-16 #
  
* I reached late today to class so I missed a few of the presentations but from the few that I could catch, I can tell that my classmates really put effort into their presentations this week.
* The group that spoke about sleep and its importance actually brought up some really important points that I really want to implement for upcoming quizzes and exams. Perhaps I had already known about them
  but just needed some reassurance. I will study early and do all assignments and not sudy past perhaps 10PM the night before so that I get a good rest before the exam. We'll see how that goes. They also had some good action plans that I hope to see implemented.
* The last group that spoke on pollution should perhaps have not been only biased about positives of recycling but perhaps have found information that says otherwise. This, I think, the professor addressed pretty well.
* There was also that group that spoke about noise pollution. I think they are right that electric vehicles would reduce noise pollution but not eliminate it because you still have those that adjust their mufflers to make a shit ton of noise.
  I believe the Taiwan government should impose heavy fines against those that illegally modify their motorcycles/ vehicles. Lock them up for a year or teo so they learn.
  
# 2023-11-23 #
  
* I wasn't able to make it today to class today so I don't really know what happened ':).
* After looking at some diaries I was able to gather a few ideas but I'm still quite lost.
* Hopefully next week will be better and I'll be able to get over this learning hurdle.

# 2023-11-30 #
  
* Today I presented on our new action ideas and had the chance to listen to what other groups had to offer.
* There was one about a coffee shop that I still don't really get. Do they have money to open a coffee shop before the semester ends?
* When it comes to education, I still agree that it can't change over night. I already shared my experience with a foreign teacher in my department and the discontent shared
  by my other local classmates. It was sad to say the least because the teacher eventually got discouraged and just turned to "chalk and talk" method of teaching. I always think its sad
  that a lot of students value more arriving at an exact answer rather than appreciating the process. For this reason I must admit that many of them are thinking the wrong way about engineering.

# 2023-12-7 #
  
* Today three groups presented on a news story from different perspective of several countries. I believe that anything that has to do with war will always be in favor of the country reporting.
  In addition it can also be more rightist since the government has a lot to do with what is published in newspapers.
* Today the professor also spoke about planetary boundaries and since I had already watched a documentary on it new more or less what he was going to say.
* I think one of the biggest issues is not identifying the threats to the planet but rather getting countries to take action.
# 2023-12-14 #
  
* Today I reached a little late again but overall was able to catch most of the presentations. One thing that stood out to me was the prospect of a coffee shop. Achieving something like that
  should be expensive. Shouldn't it? Anyhow I just hope the one that we all are assigned is feasible.
* Overall there were many things which were agreeable and disagreeable as shared by the other presenters.
* I really hope to finish the course before leaving home since I'm doing so on December 31st! WOOOOO! But I'll spend new years in the airport. :'( Anyhow let's gO!
>>>>>>> 24de5dc5aebb7dab5fc0fa484f3ef937924fee96
