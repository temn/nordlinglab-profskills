This diary file is written by Christian Deantana F74085319 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-07 #

* The first lecture was great.
* We took a quiz to asses our knowledge about the course.
* The topic exponential growth is covered in this lecture.
* A team of 3 was assembled and we are given a task to research about exponential growth and abundancy.
* Some videos are also shown regarding the topic of the 1st lecture

# 2022-09-14 #

* We learn about Git and in this case Bitbucket.
* We got to watch Steven Pinker Ted talks about is the world getting better or worse.
* We are given a task to research about the Ted talks topic and in this task, we all support Steven's idea.

# 2022-09-21 #

What I think my future will look like 10 years from now is working in a tech-company (probably foreign company in Taiwan) and is responsible in mantaining security or network
of the company. I hope before I am 30 years old, I can have my own accomodation, and transportation all paid and I can travel while work in a hybrid environment. To support these
dream, what I need to keep is consistency, which is a consistent behaviour in learning and wanting to improve and learn something new. Other than that, I am also taking an online
course outside the NCKU course, and I also try to find a new project related to security and networking. I hope by keeping this consistency, I can achieve my dream by 2032. In 
my spare time, I also like to watch some documentaries and also simple introduction videos about my field. At first, I found it really hard to mantain the consistency in learning because
I set the goal to intense and I ended up getting bored even though the topic is quite intriguing. So, later on I set a manageble goal and reward myself at the end of the week if
I manage to finsih my goal. And from there I increase my productivity little by little, and now I am getting more used to it and it becomes like one of my daily routine. In my 
opinion, the start is the hardest, because our body and mind have not get used to it, but later it will all be easier and on that moment you will feel like you can achieve anything
that you want to achive. While writing these diary, I also did some research about ways to achieve my goals and I found these 7 steps which are write down goals, set a deadline, work on our mindset,
develop our skillset, take the first step, continue to completion, and reward ourself. Making the most of our times is also one of the aspect that I think is important, and sometimes
I got easily distracted and this distraction can keep me unproductive and I found that one of the steps which is to set a deadline is really good to fight distraction. Last, seeing this 
in 10 years from noww, I hope that by doing those things, I can successfully achieved my goal which is to work in a field related to networking and security. 

* I got the chance to present my group opinions based on Steven Pinker : Is the world getting better or worse?
* Other groups cover different claims and it is quite intersting to hear.
* We got to learn about 17 Sustainable Development Goals (SDG)
* 3 ways to spot bad statistics are can you see uncertainty? can i see myself in data? how was the data is collected? (Mona Chalabi)
* Hans Rosling Ted Talks really interesting and I found out that we also did the same entry exam to asses our knowledge, the visualisation data is also easy to see
* "When facts are false, decision are wrong" - Olga Yurkova (StopFake)
* A single UK ticketholder has come forward to claim the £171m jackpot in Friday's EuroMillions draw.
* the ticket holder had the numbers 14, 15, 22, 35, 48, and the lucky stars (03 , 08).
* Only 16 people in the UK have won more than £100m.
* It is the 6th EuroMillions jackpot in Britain this year is the 3rd biggest ever win.
* The winner can decide whether to go public or not after their ticket is validated.

# 2022-09-28 #

* I found out that photo of earth taken by NASA in 2012 was fake.
* This week is the start of week 4 and the group member is being randomized again, so I have a new teammate.
* Learn about money and also financial system.
* I got to know what cause money to increase and what its value and functions.
* I found out that it is not government who print money.
* There is a connection between loan and the amount of money.

# 2022-10-6 #
* I saw Finland's, Indonesia's and Singapore's economic growth and information from the other groups.
* I also learnt that the UK Prime minister did something surprising with the UK's economic within weeks of him working.
* I learnt that we have to think of the long-term, dont just see something from the short-term.
* I got to watch and understand how the extremist and fascist got influenced and how they think.
* 3 fictional stories : 1. Taiwan's currency is declining because the new policy. 2. The marriage of prince Charles shocks the nation. 3. China deploy a missile to one of the important site of taiwan causing decline in both currency.

# 2022-10-27 #
* This week is an online course, and we are assigned to a new group.
* Other than that a super group is also created to work on the final project of this course.
* The lecture gave me an understanding about how to help with someone who is suffering depression.
* We got to watch TED Talk videos about depression, and it is a new knowledge to me

# 2022-11-3 #
* I forgot to write this week diary.
* I learn about "choice blindness" experience from watching the TED's video
* This week hw is to work on the final project and we are asked to go to the library to search for the evidence/reports

# 2022-11-17 #
* this week cover about reduction of co2 and noise polution.
* Apart from that we also see the destruction that could cause by technology
* For next week lecture, we have to work on for the final project group, not the small group.

# 2022-11-24 #
* We were asked to mute our phone and put it on the table in front.
* Through that experiment, I sometimes still keep looking messages through my laptop.
* A lot of the group talk about CO2 emissions.
* One of the action that i think interesting is to take a vegetarian meal once a week.
* Some of the team also think petition and campaign is one of the actions that can be done easily as it is feasible.
* we got to watch a video that state the similarity between rome and america.
* For next week lecture. we have to write 3 actions that is specific for the course group and also compare news with 6 different countries.

# 2022-12-8 #
* I got a chance to do a presentation and I got some feedbacks about how to style the ppt
* I got to know other teams ideas on the action for the final project
* we got to vote on the social foundations and planetry boundaries that we think is the most important
* We have to work on our action and present again 2 weeks later

# 2022-12-15 (Thursday) #
* We talk about inequalty and have a debate with the class divided into workers, environmentalist and capitalist.
* The debate is quite interesting.
* In the beginning of the class, we also need to fill in the group google form
* After talking about inequality, we also talk about wright law
* We have to write a diary everyday and work on course project for next week

# 2022-12-16 (Friday) #
* A. Successful and unproductive
* B. I felt succesful because I manage to progress on my OS homework, but I felt unproductive because after making a progress I slept and decide to continue it the next day.
* C. Set a goal to finish it then after it I can then do other things that I wanted to do in the first place.

# 2022-12-17 (Saturday) #
* A. Unsuccesful and unproductive
* B. I felt unsuccessful and unproductive because I didnt do my homework and achieve my goal to finish it.
* C. Do it as soon as i have time so stop procrastinating.

# 2022-12-18 (Sunday) #
* A. Successful and productive
* B. I felt successful and productive because I finsih my goal on my os homework and I also finish all my chores.
* C. I must keep up this good work and make it a habit in order to be a successful person

# 2022-12-19 (Monday) #
* A. Successful and productive
* B. I felt successful and productive because I manage to keep up with yesterday progress and spent quite some times doing it.
* C. I must keep up this good work and make it a habit in order to be a successful person

# 2022-12-20 (Tuesday) #
* A. unuccessful and unproductive
* B. I felt unsuccessful and unproductive because I didnt work on anything, and I watched all day long.
* C. Set a screen timer and try to be productive and dont procrastinate.

# 2022-12-21 (Wednesday) #
* A. unsuccesfull and productive
* B. I felt unsuccessful because I cant finish my homework 100%, but i felt productive because I spend my times learning
* C. Begin doing task as early as possible	

# 2022-12-22#
* We have to share our 5 rules to success and that day we have to wear what dress we would like to wear on interview.
* We got to hear every group short 4 minutes presentation about their idea.
* We hear 2 ted talks, the first one is about democracy and the second one is about bhutan as a carbon-neutral country.
* I really enjoy the 2nd ted talk because the prime minister of bhutan gave an amazing insight on bhutan and their program. Apart from that the delivery of the idea is also interesting.
* For next week presentation, it is the super group and we have to start implementing our ideas or action and ask for about 10 peoples to try our small action.

# 2022-12-23 (Friday) #
* A. Successful and unproductive
* B. I felt unproductive because I slept for 16 hours , but I managed to make my remaining hours productive by learning for my final term next week.
* C. I will try to sleep and wake up earlier and always set a schedule so that I have something to do that are already plan ahead.

# 2022-12-24 (Saturday) #
* A. Successful and productive
* B. I felt successful and productive because i managed to learn 2 chapters for final term next week and I almost finished the remaining chapters. I also get to wake up earlier to start the day.
* C. I have to keep up this good work and wake up early tomorrow and finish reading all the final's material.

# 2022-12-25 (Sunday) #
* A. Successful and unproductive
* B. Today I felt sucessful because I managed to work on my finals and i do something today, but i still felt unproductive, because I wake up late and didnt have the chance to start the day earlier.
* C. I will try to not snooze my alarm and get up earlier to be more productive.

# 2022-12-26 (Monday) #
* A. Successful and productive
* B. This diary and the diary on Tuesday 2022-12-25 is written on 2022-12-28. I felt successful and productive because i spend my time studying and manage to finish all the chapters.
* C. I will try to keep up this good work and keep being productive

# 2022-12-27 (Tuesday) #
* A. unuccessful and unproductive
* B. Today, I felt unsuccessful an unproductive because i havent set any other goal because I finished studying all the chapter yesterday. and what i done all day is watching .
* C. Immediately set plan ahead and give reward like watching, but dont be addicted with it.

# 2022-12-28 (Wednesday) #
* A. Successful and productive
* B. I learn a new skills and manage to finish my goal (progress on online-course that i bought).
* C. Let's keep up with the good work and try to maintain this productive and successful day.

# 2022-12-29 (Thursday) #
* I forgot to write the diary for last week lecture
* I write this on 2 january, Happy new year everyone!
* We heard all of the groups and some student and even professor gave us an advice on how to improve or tackle our problem
* The professor asks us to write our obituary, I think what i want to write in my obituary is "Christian Deantana haved lived happily throughout his life. He hopes that he has brought joy and kindness to everyone. Rest in Peace myself"

