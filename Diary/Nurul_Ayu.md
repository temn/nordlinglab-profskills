This diary file is written by Nurul Ayu E64138429 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-19 #

* The first lesson shows me how interactive the class was.
* The lesson is great where it shows us general knowledge about what world are going to be in the future (from international view).
* i am a bit surprised by the fact that the class always getting task every weekend.
* it also improve our network of local and other international students since we have to make a group presentation class every weekend.

# 2024-09-26 #

## Task 1: General Diary ##

* In this class meeting, the lecture focused on the explanation of 17 SDGs
* During the class, I presented my week 2 task with all the nervous feeling, and I got new input that I didn’t aware that there was one of my picture depicted sea level rising in Netherland and Belgium which was actually those 2 countries have a special case where their sea level have risen not also because the global warming, moreover because there was post-ice age isostatic rebound. 
* Therefore, the professor taught us to avoid using data or evidence that is related to sea level rising in some countries such as Southern Europe (Belgium and Netherland) that have already had a history for natural geological phenomena.
* This class we also watched 3 videos; talkshow from a known journalist–Christiane Amanpour, Mona–talk about how to spot bad statistics data, Hans Rosling–the best stats we would’ve ever seen talking about life expectancy across the globe as the time went by. 
* From those videos, the most concerned was about the fake news are always easily viral and distributed in any social media platforms, where AI is the one factor to distribute the “undetected” news easily and make the society mislead and misinformed about the news, especially when talking about humanitarian effort, political sector, elections, and so on.
* Regarding the statistics, the 3 main questions that are crucial to spot are: Can you see uncertainty? Can I see myself in the data? How was the data collected?

## Task 2: Create a Diary of what would my life be in the next 10 years from now ##

<p>Before diving into my dreams for the next 10 years, I’ve decided to break this period into three phases: the first 4 years, followed by 3 years, and another 3 years after that. These phases will reflect different priorities and milestones I aim to achieve along the way. In the first phase—the next 4 years—my primary goal is to graduate with a degree in Materials Engineering from my university, ITB in Indonesia. As the eldest child in my family, with parents who are no longer working, the pressure to find stable employment right after graduation weighs heavily on me. The first step after graduating is to secure a job with steady income, ensuring financial security for both myself and my family. My focus is to work for a company that offers not just a stable paycheck, but also a healthy work environment. A workplace that encourages growth, supports its employees, and ensures a good work-life balance is essential to me. One of my concerns, however, is how my career path might intersect with my plans for marriage and family. I’ll be looking for companies that uphold women’s rights and offer flexibility in this regard. Given my background in Materials Engineering, which has been centered around industrial operations (like metal applications, machine maintenance, corrosion prevention, etc.), my first work experience will likely be in the energy sector—either onshore or offshore in the oil and gas industry. I imagine this will take me outside of Java, and I’m prepared to commit to this path for at least the first 4 years. However, the possibility of pursuing a master’s degree is always at the back of my mind. Upon graduating, I plan to apply for both jobs and master’s programs simultaneously. Wherever and whenever the right opportunity comes—whether it's work or study—I’ll seize it, as long as it aligns with my interests and offers the best long-term options. When it comes to pursuing a master’s, I’m particularly drawn to studying abroad in a country with a comfortable learning environment, mild seasons, clean surroundings, and good walkability. One of my top choices is the Netherlands, primarily because of its climate and academic reputation. For the second phase—the following 3 years—I hope to get married. My ideal partner is someone who shares my love for exploration and travel, someone who cherishes their parents, offers new perspectives, and engages in deep, meaningful discussions. Above all, I seek someone who supports me and encourages growth in all aspects of life. So, my projection for the next 5 years—by 2029—is to be either married, pursuing a master’s degree, or working, or a combination of both. In the final phase—the last 3 years of my 10-year plan—I will let life unfold naturally. Whether that means continuing my career, pursuing further studies (even a PhD, perhaps), or embracing the role of a career-mom, I am open to whatever opportunities arise. I anticipate that by then, the global workplace will shift towards remote work as part of the ongoing "remote-work revolution," allowing more flexibility for women like me in male-dominated industries. I am optimistic that workplace diversity and inclusion will provide the support needed for women to excel. I also foresee that online learning will become an even stronger force in my life, as I am committed to being a lifelong learner, no matter where life takes me. The rise of the gig economy—where people generate income through side hustles and freelancing—will offer additional opportunities for flexibility and growth.
</p> 

## Task 3: Analyze News from Different Sources  ##

<p>Different Sources on: “SpaceX launches rescue mission for 2 NASA astronauts who are stuck in space until next year” 
</p>

<p>On the given website Ground News [link: https://ground.news]  three sources from about 5 hours ago were shown for this question. When analyzing the biases of these three news pieces (Left, Center, and Right), we can explore how each source frames the same event slightly differently. 
</p> 

<p>in the left leaning news, the why is the emphasis on the logistical and safety concerns surrounding the mission. It frames the story in a way that highlights technical issues and delays, like the Boeing spacecraft’s failure. While in the center leaning news, The focus is on the broader importance of the mission, particularly how significant this event is for SpaceX’s operations and advancements. On the right leaning news: The emphasis is on the action and success of the mission. It highlights SpaceX’s role and capabilities without mentioning safety concerns or Boeing’s issues.
</p> 

# 2024-10-03 #

* From the mail that i received, i'm required to study such as a economic stuff to learn all about money and financial condition based on the country we choose. 
* As an undergraduate materials engineer who has never any thought to search kind of this task, it was interesting yet no where should I search kind of this data and how to analyze. 
* Still not understand of what takeaways should I point out from those graph (based on the task)

# 2024-10-17 #

* In class, we discussed the concept of money, what gives it value, the factors that affect it, and how it is created.
* We also watched a video that explained how money functions, how loans are essential in generating new money, and offered a better insight into the financial system.
* It was interesting to observe how the amount of currency in public circulation of Taiwan surges annually around Chinese New Year.

# 2024-10-24 #

## General Diary ##

* Class started with a quick review about money through a diary reading session. We also had to fill out a survey on the financial system to test our knowledge about money and its value.
* After that, some groups presented on their country’s economic conditions, and surprisingly, the presenters were from Indonesia, just like me! What really shocked me was learning that Indonesia has a lower purchasing power. Plus, the inequality level has been basically stagnant from before the 2000s until 2020. The presenter explained that this gap is largely due to corruption among local politicians.
* After the presentations, we watched a video from a former member of the American Neo-Nazi Movement about “potholes.” I found this “underdog story” really engaging and inspiring, motivating me to be a better person day by day. And “potholes” was my new English vocab for the day!
* I also heard from a classmate who shared a story about how some of his family members were forced to separate due to an ideological conflict 50 years ago—between mainland China and Taiwan. Even though they share the same ancestry, the conflict split them apart, severing family ties. It made me sad to hear his story, but I felt grateful to witness the bond still existing between some families across the mainland and Taiwan.
* At the end of the class, we also learned about “imagined realities” and how they can influence our lives.
* In one of the TED Talk videos I watched, they talked about the brain-boosting benefits of exercise. The speaker mentioned that physical activity can help prevent Alzheimer’s and regenerate brain cells in a part of the brain called the hippocampus. This part of the brain is crucial for things like focus, decision-making, and happiness, and it helps us fight against depression. What I loved most about this video is how it instantly changed my lifestyle in the dorm. Now, I always make it a point to walk more and take the stairs instead of the elevator—LOL!
* We also discussed knowledge, and it turns out that knowledge is more than just information; it’s a set of beliefs that are well-justified and true.

## Individual Task 1: Fiction Imagined Reality Stories ##

### Success ###

<p> During Covid-19, I often saw many content creators my age sharing how they made their first billion at 19. Some even talked about buying a Porsche at 21! This made me and my family rethink what success really means.
But lately, my TikTok feed has changed. Now, there are more videos of people who love exploring mountains, nature, or going solo traveling. They dare to try extreme things to prove they’re still living as "humans," not just transactional tools in this material-driven world.
From this, I realized that money and material possessions are just illusions for some people. While money is important for survival, life should be more about happiness, equality, courage, and mutual respect. We need to help each other, not just chase after financial targets.
</p>

### Religion ###

<p> Growing up in a mostly Muslim country, wearing a hijab was never a big deal for me. But moving to Taiwan, where Muslims are in the minority, made me see my hijab in a whole new light. Just a few days ago, I went to a Muslim forum at NCKU, and I heard so many meaningful life lessons. One thing that really hit home was that every girl who wears a hijab faces challenges. For some, their families don’t support it, while others struggle with self-esteem or doubts about wearing it.
</p> 

<p> It’s important to realize that by putting on the hijab, I’m not saying I am Islam; I’m just a Muslim trying to follow my faith. I see the beauty in it and hope to improve myself through it. No one’s perfect, and we all make mistakes. The hijab doesn’t make you better; it’s a reminder to keep working on yourself—both personally and spiritually.
</p> 

<p> In this journey, I’m learning to embrace my identity and grow stronger in my beliefs, no matter where I am.
</p>

### Faith and God ###

<p> The first story I carry is "faith." Faith, for me, has always been a deeply personal journey, a source of peace and grounding. But here, in a country where Muslims are a minority, it sometimes feels like an unfamiliar language. Last Saturday at Muslim Prayer room, I went to find a quiet spot to pray on campus, and though I felt self-conscious, the practice reminded me of home. It’s strange; I’ve been doing this my whole life, yet in a new environment, it feels like rediscovering my faith from a different angle—one that deepens my understanding of what it truly means to be connected to something greater.
</p>

## Individual Task 2: Describe your future or the future offered by some famous named politician in your country! ##

<p> One of the notable politicians shaping Indonesia's future outlook is Anies Baswedan, the former Governor of Jakarta and a prominent figure in Indonesian politics. Anies has a vision for Indonesia that emphasizes inclusivity, education, and sustainable development.
</p> 

<p> <strong>Inclusive Governance</strong> : Anies wants a government that listens to everyone, especially marginalized groups. He believes in open dialogue and collaboration to ensure all voices are heard, focusing on social justice and reducing inequality.
</p> 

<p> <strong>Education Reform</strong> : Anies emphasizes the need to improve Indonesia's education system. He aims to make quality education accessible, enhance teacher training, and use technology to equip students for the future.
</p> 

<p> <strong>Cultural Preservation</strong> : He values Indonesia's rich cultural heritage and wants to promote and preserve it to strengthen national identity and unity.
</p>

<p> <strong>Sustainable Urban Development</strong> : As the former Governor of Jakarta, Anies focused on sustainable urban planning, emphasizing green spaces, efficient public transport, and eco-friendly policies for better living conditions.
</p>

# 2024-11-07 #

## General Diary ##

* From the last lecture, Bill Bernat emphasizes the importance of connecting with depressed friends in a genuine and natural way. He suggests that it's not necessary to adopt a sad voice when talking to them. Instead, he encourages being yourself and clearly stating what you can and cannot do to offer support.
* In the YouTube video "Don't suffer from your depression in silence" by Nikki Webber Allen, she shares her personal experience of living with anxiety and depression while holding the belief that she wasn't allowed to be depressed due to her accomplishments and societal stigma. 
* Nikki, shares her personal experience with depression and how she copes with it through self-care practices such as meditation, yoga, and a healthy diet. She also emphasizes the importance of seeking help and support from others, including therapy.

## Following aspects of depression ##

> <strong>How to recognise when a friend is depressed?</strong>
>
> It should be very hard to recognise the person who suffer from depression or even anxiety, but, i thought that sometimes, common sign of depression may look alike:
Loss of interest; noticeable lack of interest in things they used to enjoy—social activities, hobbies, or daily routines. Sleep and appetite changes; they may mention difficulty sleeping, sleeping too much. Feelings of hopelessness or worthlessness; feels guilt or feeling like a burden.


> <strong>What to say to a depressed friend?</strong>
>
> When talking to a friend you think might be depressed, probably, trying to be open, gentle, and non-judgmental is the best way to talk with them. Also, express your support: “You matter to me, and I’ll be here every step of the way.

> <strong>How to help a friend with depression?</strong>
>
> Maybe encourage healthy habits also stay positive and be patient.

# 2024-11-14 #

## General Diary ##
* Last class, we started with some review of anxiety survey form. Most of the students feel anxious when they think that their future might be frightening them. And I feel so, somehow.
* One of the thing that highlighted me in the class was short story from other student regarding energy power plant in Taiwan that disregarding the health of people especially in Taichung. 
* From the short debate, the professor also told us to fill some of our potholes with amazing unlimited opportunities through online course, some TED Talks, or try some amazing edu-tech start-up to learn new things.
* Do not limit ourselves only to listen from the professor, but also maximize the opportunities and  the network. 
* The class also shared about the other's presentation group regarding identifying some of depression and how to cope and offering some ways to help. 
* The class also gave me some other TED talks that discuss human nature from experiments conducted by one of the speakers in a TED talk (I forgot which one). The experiment showed us that, as a human nature, we can naturally rationalize the choices that have already been given to us, even if they were not our real choices.
* And we also have to watch some long documentary video regarding the legal system work for the pre-read materials in the next class meeting. 

# 2024-11-21 #

## General Diary ##

* Through a TED Talk video, Jaron Lanier, a computer scientist, argues that the internet needs a fundamental redesign. He highlights how the current system fosters manipulation, polarization, and inequality. 
* A YouTube video titled "Weapons of Math Destruction" by Cathy O'Neil speaker discusses the abuses and destructive consequences of algorithms in various domains such as education and employment. 
* O'Neil emphasizes that algorithms are not objective or fair, as they are created by individuals with their own biases and agendas.
* She advocates for more transparency, accountability, and audits of algorithms to ensure fairness and justice in their application.
* It's essential to be mindful of how the internet shapes my behavior and perspectives. I should strive to use the internet consciously, valuing authentic interactions and protecting my attention from manipulative systems.  
* I aim to make smarter choices online and take charge of my time, ultimately building a more thoughtful and productive future (hopefully).
* It surprised me when the professor talked about how Facebook got their revenue and became a profit company.
* My thinking was always, “how facebook sustains their business while users don't have to pay to use facebook or Instagram?” Turned out, It is predominantly built on targeted advertising which collects vast amounts of data from its users (demographic, behavioral data, interest and they selling our profile data to companies, politicians, 
* By offering detailed targeting and a massive audience, it has become important things for businesses worldwide looking to advertise efficiently. However, this model comes with ethical and regulatory challenges related to user privacy and data security such as the facebook scandal in some political elections. 

## How to Reach A Concencus with A Mini Example: ##

> <strong>Jakarta Trending: NGO Propose Law for School in Jakarta</strong>
>
> Students from Jakarta schools will have one designated day off per week to visit Bantar Gebang landfill. The aim is to educate students on waste segregation and create awareness of landfill management challenges. Schools will incorporate lessons on waste management, segregation techniques, and environmental impact.

> ### <strong>Step to Reach Concencus:</strong> ###
>
> #### Debate Initiation ####
>
> - The discussion began with the question: "Should schools in Jakarta implement a holiday dedicated to waste sorting activities at Bantar Gebang landfill?"
> - Participants included teachers, students, parents, and environmental activists.
>
> #### Diverse Opinions Presented ####
>
> - Supporters: Advocated that such an initiative would foster environmental responsibility among students and provide real-world lessons.
> - Critics: Raised concerns about logistics, safety at the landfill, and whether students would genuinely engage in waste sorting.
>
> #### Addressing Concerns ####
>
> - Safety concerns were addressed by suggesting strict supervision and protective gear for students during landfill visits.
> - For logistical issues, stakeholders agreed to start with a pilot program
> 
> #### Voting on Proposed Rules ####
>
> - Participants proposed several ideas, and each was voted 
>
> #### Finalization of The Rules ####
>
> - The group reached a consensus with some other concerns and diversing opinions.



# 2024-11-28 #

## General Diary ##

* Professor began the class with some mini experiment "Addiction of phone":to put all students' handphones in front of the class for 3 hours and prohibited us to our the handphone in the middle of the class. 
* We also got to present the course final project based on the actions that we’re planning to execute
* In the end of the class, the professor gave us a video regard Lifecycle of civilisations and the dark age throughout history
* We also got to watch “How to watch some news?” from TED talk
* One of the videos that prove many different views of broadcasters around the world has been investigated in one of the videos that we watched in the class that talked about the Maccabi Israelis Supporters clash in Amsterdam. 

# 2024-12-05 #

## General Diary ##

* This week’s topic, "Planetary Boundaries," provided an in-depth exploration of the limits of Earth's ecosystems and how human activities influence them.
* The session kicked off with a group discussion, where the class was divided into three teams. Each team analyzed news articles to evaluate the perspectives from which they were reported.
* We were then introduced to a visual representation of planetary boundaries and discovered that by 2023, six of these critical thresholds had already been surpassed: novel entities, climate change, biosphere integrity, land-system change, freshwater use, and biogeochemical flows. 
* Finally, we were tasked with final project preparations

# 2024-12-12#

## General Diary ##

* We had a short panelist QnA session between audience and panelist where I was representing my group regarding replacement of steel concrete
* I learned that collective actions, such as collaboration between governments, corporations, and communities, are essential to maintaining environmental balance and preventing permanent ecosystem damage

# 2024-12-19 #

## General Diary ##

* The class began with a short presentation from each group, and I was presenting a topic regarding, “How to forecast technological progress?”.
* In the class, we had some debate between the student from capitalist view, workers view, and environmentalist regarding “why is the 3rd world industrial revolution?”. 
* The teacher also gave us the short explanation of the framework of how to succeed. 

## Individual Task - Daily Diary (Friday, 12/20 - Wednesday, 12/25) ##

> #### Friday
>
> - Productive/Successful
> - I felt productive because I managed to wake up at 8 AM, have breakfast, call my mom back in Indonesia to catch up, and go shopping for outdoor gear and logistics to prepare for hiking Mount Hehuanshan in Taichung. I felt successful because I completed almost everything on my to-do list, except exercising, but I still considered the day a success overall.
> - Winter makes me lazy to get out of bed because it’s so cold, and that keeps me from going out for a morning walk or run. I’ll try to push myself more tomorrow!

> #### Saturday
>
> - Unproductive/Successful
> - I felt unproductive because I decided to take things slow before my 3 AM hiking trip to Mount Hehuanshan. I spent most of the day resting and gathering energy. But I felt successful because, by the evening, I felt calm and ready for the winter hike.
> - Next time, I’ll use my rest time more wisely, like reading a book instead of scrolling through social media.

> ### Sunday
>
> - Unproductive/Successful
> - I felt unproductive because hiking took up almost the entire day, and while it was fulfilling and helped me check off a bucket list goal for 2024, I didn’t have time to work on homework or projects due Tuesday. I felt successful because I finally reached the peak of Mount Hehuanshan despite the freezing cold. Even though I couldn’t see the galaxy, constellations, or sunrise, I felt proud. On the way home, I even stopped by a mosque in Taichung to rest and pray.
> - Honestly, I didn’t take away any lessons-learned from Sunday, but I felt deeply alive—and maybe that’s enough.

> ### Monday
>
> - Unproductive/Unsuccessful
> - I felt unproductive because I woke up at 12 PM, still exhausted from hiking. I was late to class, and even after attending, I was bombarded with new group tasks and assignments. I felt unsuccessful because I didn’t have the energy to do anything meaningful.
> - Tomorrow, I’ll wake up at 9 AM and start my day with a morning walk and pre-reading my class materials.

> ### Tuesday 
>
> - Productive/Unsuccessful
> - I felt productive because I finished my biomedical materials project and made progress on my group work for Virtual World class. But I still felt unsuccessful because I had spare time and didn’t use it for exercise or to work on my thesis.
> - Tomorrow, I’ll dedicate time to make meaningful progress on my thesis.

> ### Wednesday 
>
> - Productive/Successful
> - I felt productive because I managed to do some revisions on my thesis draft. I also felt successful because I reviewed my Chinese before class and joined a fun Christmas game with classmates during Chinese class.
> - Tomorrow, I want to focus on completing my thesis prospectus more thoroughly.

## 5 Rules for Success and More Productive ##

> Set Clear Goals: Always define your short-term and long-term goals clearly. Knowing what you're working towards keeps you focused and motivated.

> Stay Consistent: Small, consistent efforts every day are more effective than occasional bursts of hard work.

> Try to balance the whole week between productive, social time, and creative: Make a casual breakdown of how we can balance whole week between productivity, social time, and creativity

> Dopamine Detox: rebalancing your brain to appreciate and stay motivated by long-term, meaningful achievements instead of short bursts of pleasure (from social media for instance)

> Keep your spiritual strength for success: Strengthening our inner peace and resilience, gratitude and contentment, also ethics and integrity 

# 2024-12-26 #

## General Diary ##

* There were several group presented their course project progress
* Each of us was presenting the step or key to success based on our preferences while wearing formal attire that the professor had told us last week. Professor also gave us some feedback on our outfit and emphasized how important it is to apply it on an everyday basis even if it is a rare occasion  in the uni class. 
* Professor also demonstrated how to tie a tie and there were 85 ways to tie a tie based on the wikipedia source.

## Daily Diary ##

> #### Thursday (12/26)
>
> - Successful/unproductive 
> - I am unproductive bcs i get up after 9 AM because the winter season makes me feel a bit lazy and I forgot to list up my to-do list before I sleep. That makes me feel “nothing to do” for that day. However, I was happy bcs I attended an exchange gift party with my fellas and doing some mini night-picnic. The party ended at 1 AM midnight so I expected that I wouldn't be awake in the morning, sadly.
> - I think, biggest lesson for me is to always do some to-do-listing before sleeping and keep the gadget away from my bed while placing useful items near bed, such as a diary and tumbler. 

> #### Friday (12/27)
>
> - Productive/Successful
> - Feels unproductive definitely. After last night party, I woke up at 11 AM 🙁but luckily,I have no class on Friday so I feel like i have a bit free in the morning. And I decided to warm up my body at my dorm balcony after I get up from bed. After that, I had to prepare myself to go to immigration office to extend my visiting period in Taiwan had a short trip to Tainan Fine Art Museum with my friend. I feel like I was unsuccessful cuz at that time I should have had to summarize my thesis Materials project, but not even finished a half of that. 
> - I think on the last 3 days, my sleep routine was so messed up and I feel like my 6 AM morning routine should be rearranged again. Avoid drinking caffeine, relax the body and gotta back running routine 

> #### Saturday (12/28)
>
> - Productive/Successful
> - I was almost finished with my final project in one of my courses that I took. Almost every hour, I was doing my work in front of my laptop. I also did  some house chores like washing all my clothes. I feel successful bcs all my work that I should have finished was done. I also feel successful because I run at night for almost 20 minutes. I also prepared to go to Hsinchu city to hike Jiali Shan with my Filipinos Friends. I knew that I would mess up my sleeping routine, but guess what? I ticked all my bucket list before my 2024 end, to hike another mountain in Taiwan after I failed to go to Qilai Mountain last week bcs of the bad weather. 
> - Note that I was not consistent in my running routine, so for upcoming days and for 2025 the resolution is to give at least 20 minutes doing jogging and running routine, Engaging some activities that reach my fat-burning zone heart beat! Hopefully

> #### Sunday (12/29)
>
> - Productive/Successful
> - I feel productive and successful, cuz This day was so EXHAUSTED yet FULL OF EXCITEMENT! I was starting my day at 3 AM bcs I had to go to Hsinchu and hike JialiShan in the morning with my Filipino friends. It took a total of 8 hours to go up and down from the mountain surrounded by Japanese Cedar Forest and it was so impressive. I feel successful cuz I got a clear view of the peak of the mountain and it was so sunny! No dense fog and I can see the other peak mountain! 
> - For me, I should increase my nature hike routine. Giving me 1 day without a signal on my phone was so refreshing for me. Next year, more mountains to go! 

> #### Monday (12/30)
>
> - Unproductive/unsuccessful
> - Feels unproductive cuz I just arrived in Tainan from Hsinchu at 3 AM in the morning, and I had a class at 1 PM, so I just woke up at 11.30 AM. Nothing to do cuz my leg was so stiff. Feels unsuccessful cuz I don’t do anything.
> - Note for this day is: Do your best at everything! 😀Limit your screen time!

> #### Tuesday (12/31)
>
> - Unproductive/unsuccessful 
> - Because my class that day was dismissed since my professor had to go to Japan, it was my time to get a full holiday and watched new year countdown in Kaohsiung. Not really feeling unsuccessful because it was just an ordinary day and again, I still messed up my sleep routine, sadly.
> - My note for upcoming days is Arrange and give more space to have a good duration for sleep! 

> #### Wednesday (1/1)
>
> - Unproductive/unsuccessful 
> - Unproductive cuz I still cannot control my sleep duration back to normal since I ended my activity at 2 AM. I was expecting that my body could prevent me from getting up in the morning. I also felt unsuccessful bcs I had nothing to do, no listing what I should do in a day. But, finally I did my house chores to wash my clothes. 
> - Still progressing to try to arrange and give attention to my sleep routine and doing some exercises at least once in a day!

# 2024-01-02 #

## General Diary ##

* In this course, we watched several videos regarding "on ho we can make a difference". This brought us to watch what makes something goes viral?, invesment in women and girls, and some other videos. 
* One of the student asked, why we should invest in women, then the professor gave me a mic to answer that based on how my experience has affected me for this long.
* Also professor encouraged us to make a difference and how we can contribute to our society. 

# 2024-01-09 ##

## General Diary ##

* As the last lesson were talking about "How to make a difference?", hence, we were given a taskto make 2 obituary of our version and mention 2 inspirational figures that admired us:
* This week was the final session for this course, and we had a final exam. 
* After the exam, each group has to present their progress of the final project video and other students should give a feedback. 

> #### My Obituary
>
> - Aside from becoming a material engineer, I aspire to be a remarkable female traveler and hiker, exploring the beauty of Indonesia while leaving behind a meaningful legacy. I want to share knowledge and experiences with friends from different backgrounds and privileges, fostering connection and empowerment.
> - I also dream of being an active content creator on social media, leveraging the platform to influence algorithms positively and cultivate a healthier, more inspiring environment for personal growth and learning.

> #### 2 Persons that I admire:
>
> - *Zhafira Aqyla* is a content creator and scholar who has shared her educational journey and personal experiences through various platforms. She graduated with a Bachelor's in Human Sciences from Osaka University and a Master's in Learning Design, Innovation, and Technology from Harvard University.
> - *Anies Baswedan* is an Indonesian academic and politician who served as the Governor of Jakarta from 2017 to 2022. He was previously the Minister of Education and Culture from 2014 to 2016. Anies holds a PhD in political science from Northern Illinois University and is an alumnus of Gadjah Mada University

# Additional Task: Improvement for Course Attendance #

## A World of Facts, Challenges, and Ideas ##

1. Know the current world demographics;

* Dedicated to regurarly read or watch articles from trusted sources. Modify and try to adjust my algorithm tiktok, youtube, or instagram to that related current news form UN, World bank, politics, or other global trends. So that my daily suplement of social media could improve my general knowledge. 
* Attend public discussion that has been facilitated by a lot of good influencer that always speak up about global issues. This could improve my connections, critical thinking, and enrich my knowledge while I do my travelling in Jakarta or other metropolit city. 

2. Ability to use demographics to explain engineering needs;

* My 2025 resolution is travelling Indonesia and hike more mountain in Indonesia. While traveling or exploring different areas, note examples of infrastructure (e.g., transportation, housing) and think about how demographics influence their design. 
* Discuss these observations casually with friends or classmates to develop insights.

3. Understand planetary boundaries and the current state;

* Map examples of industries that cross or respect these boundaries and discuss their implications.
* As the final year student, and I had to impelement green material innovation in my green polymer lab, I should've make a background of my research related to planetary boundaries and the curent state, in order to help and tackle the society's problems. 
* Discuss with my professor and my labmates to initiate and innovate tech based on the planetary boundaries framework. 
* I want to watch documentaries movie more, such as Our Planet or Before the Flood during my free time.

4. Understand how the current economic system fails to distribute resources;

* I'm not really a game-player, but I've heard that playing a games is one of the platform where you can reflect and forecast how the technology could've developed in the near future. 
* Playing simcity, which simulate resource distribution challenges. 
* Read short articles or infographics on economic inequality while commuting or during downtime.

5. Be familiar with future visions and their implications, such as artificial intelligence;

* One day, learn one AI tool
* Watch entertaining YouTube channels like ColdFusion or Kurzgesagt to learn about AI in a fun, digestible way.
* Attend TED Talks offline or public discussion to be familiar with current future visions

## Personal Health, Happiness, and Society ##

1. Understand how data and engineering enable a healthier life; 

* Use health apps or wearables (like step trackers) to see how data influences your own well-being.
* Make a widget of screen time in my phone so I know when I should to limit the screen time
* Use google spreadsheet to set my monthly cashflow

2. Know that social relationships give a 50% increased likelihood of survival;

* Travel with friends to foster meaningful connections.
* Set up a casual dinner with my professor or other persons that I admire to motivate myself 
* Travel with parents and have a deep talk with them at least once a week
* Attending Networking session and activate my linkedin account to be mre active so I can connect with people from other sectors!

3. Be familiar with depression and mental health issues;

* Listen to podcasts or audiobooks about mental health during walks or workouts.
* Take a sabbatical leaving to pause some overwork activities, to manage and improve my inner peace.
* Improve my spirituality to attend muslim forum to fulfill my inner life. 

4. Know the optimal algorithm for finding a partner for life; 

* Reflect on your values and preferences while journaling during quiet moments.
* Try something new! Embrace the mistakes! Overcome the fear feeling! Develop myself, enrich skills as much as possible, then I can find the best partner that always accompany me a long the process that I've been through. 

## Professioanal Skills to Success ##

1. Develop a custom of questioning claims to avoid “fake news”;

* Use a fact-checking app to practice identifying fake news
* Dedicate a couple of times to read and compare the news from different publishers. 
* Discuss current news with friends and analyze together what seems credible.

2. Be able to do basic analysis and interpretation of time-series data;

* Try analyzing personal data, like tracking my travel expenses or screen hours, to make it fun.
* Enroll and try basic training course like python, matlab, excel from google or linkedin.  

3. Experience collaborative and problem-based learning; 

* In order to acomplish my final year project, I commit to myself to diverse my persepctive with some other labmates, professors, and my advisors. 
* Also, to analyze my ciritical and problem based skills, I have to ask my professor regularly about my feedbacks, improvements, so I can reflect myself of my learning experience.

4. Understand that professional success depends on social skills; 

* Practice social skills by attending social events, traveling with new people, try to wear formal attire, networking connect events
* Attending conference that free to attend for students while wearing a goog atire.

5. Know that the culture of the workplace affects performance;

* Watch videos or interviews with professionals discussing workplace culture in different industries.

# Last Course Diary #

<p>I just want to say thank you to Professor Nordling. I initially joined this course because my friend recommended it. My friend suggested that this course was incredibly useful and worth taking to gain a lot of general knowledge. During the second week of class, I was surprised to find out that we had to do homework almost every week, working in groups with Taiwanese classmates. It seemed overwhelming at first, but it turned out to be... I just want to say that I am incredibly grateful and thankful because, during my time as an exchange student at NCKU, I had the opportunity to join a class that allowed me to learn so much beyond engineering. From mental health, economics, and forecasting technology to even learning how to find the best partner in life—this course covered so many diverse topics. Even more than that, I had the chance to practice public speaking in English, something I rarely did before. This experience pushed me to embrace my weaknesses and encouraged me to be brave enough to step out of my comfort zone.<br>
Last but not least, I want to express my heartfelt gratitude to Professor Nordling and Teaching Assistant Andy for making this course so memorable and impactful in expanding my horizons!</p>

<p>Wishing you all the best always—SUKSES SELALU!!!<br>

Sincerely, 
**_Nurul Ayu_**