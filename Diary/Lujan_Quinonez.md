This diary file is written by Lujan Quinonez F94117063 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-24 #

## Homework ##

## Part 1 ##

What do I think my future will look like:

In 10 years, I see a future filled with excitement, growth, and purpose. My journey has taken me far from my home in Paraguay, and I find myself in Taiwan, a place that has captured my heart and soul. This beautiful island has become my second home, and I envision continuing to build my life here.

In the realm of my career as a biomedical engineer, I see myself as an active participant in my field. I have embarked on a quest to gather knowledge and experience from various laboratories, shaping my path and refining my true passion within the field. Taiwan, with its advanced research opportunities, still beckons me. I envision pursuing a postgraduate degree here, despite having other options abroad. My affection for Taiwan is deep-rooted, and it seems only fitting to further my education in this thriving environment.

As I look ahead, I am thrilled to see my partner by my side. We both share a passion for education and psychology, and together, we are exploring opportunities to advance our knowledge in these areas. Our shared dreams and aspirations are closely tied to Taiwan, where we see ourselves thriving personally and professionally.

When it comes to starting a family, I tread cautiously. Ten years from now, I still see myself as a work in progress, continuously learning about myself and my surroundings. I am committed to taking the time to become the kind of mother I aspire to be, nurturing and guiding my children with love and wisdom. This journey will take time, and I want to savor every moment of self-discovery and growth before taking on the responsibilities of parenthood.

Family remains a constant source of support and love in my life. While I may be far from home, I am determined to continue assisting my family back in Paraguay, even if it means from afar. Their well-being and happiness will always be close to my heart.

Five current trends of change that I believe will have a significant impact on my life in the next 10 years:

* **Healthy Relationship with My Partner:** The foundation of my happiness lies in my strong and nurturing relationship with my partner. Our bond continues to grow, providing me with unwavering support, love, and strength as I pursue my dreams.

* **Improved Relationship with the Taiwanese Community:** Over the months, I have worked on building connections and understanding with the local Taiwanese community. This has deepened my sense of belonging in this foreign land, making it easier for me to envision a long and fulfilling future here.

* **Embracing Ambition:** I have left behind complacency and embraced ambition. This newfound drive propels me to push my limits and strive for excellence in everything I do. I am now more determined than ever to achieve my goals.

* **Boosted Self-Esteem and Confidence:** My self-esteem and self-confidence have seen significant improvements. This newfound belief in myself fuels my determination to continue pursuing my dreams relentlessly. I am ready to tackle challenges head-on.

* **Building Healthy Relationships:** I have started shedding the need for approval from people who don't genuinely care about me. Instead, I am focusing on fostering meaningful and healthy relationships with individuals who contribute positively to my life. This shift in perspective ensures that I surround myself with those who uplift and inspire me.

I see my future as a bright tapestry of growth, learning, and love. Taiwan has become my second home, and I see myself continuing to thrive here professionally and personally. My journey with my partner, our shared ambitions, and my dedication to self-improvement will shape the next decade of my life. As I continue to build a life I am proud of, I remain committed to my family's well-being and cherish the profound relationships I have cultivated along the way. The trends I've embraced, from self-assuredness to meaningful connections, will serve as guiding lights on my path to a fulfilling and purposeful future.

## Part 2 ##

* **"Surgeons have successfully transplanted a pig's heart into a dying man in an experimental procedure".**
* The patient, Lawrence Faucette, had no other options due to heart failure and health issues.
* Faucette responded positively to the pig heart transplant, showing early signs of improvement.
* There is a significant shortage of human organs for transplant, leading to the exploration of xenotransplants using genetically modified pig organs.
* Special permission from the FDA was required for this procedure outside of a formal trial.
* This time, researchers have better tests to detect hidden viruses in the pig heart and have made some medication changes.
* Faucette's condition was not as critical as the prior patient who received a pig heart transplant.
* The success of this procedure is uncertain, and it is considered a single-patient "compassionate use" case.
* The FDA's approval for this second case does not necessarily indicate readiness for formal pig heart clinical trials.
* The pig heart used has undergone 10 genetic modifications to make it more compatible with the human immune system.

# 2023-10-12 #

* Money is a widely accepted medium of exchange in an economy that is used to purchase goods and services.
* What gives money value: general confidence and acceptance in the economy, stability and government support and the ability to exchange it for goods and services.
* The financial system involves the issuance of money by the central bank, which is distributed through the banking system.
* The financial system facilitates savings, investment and access to credit for people.
* Recessions caused by credit bubbles are economic crises that result from excess debt and market speculation.
* These recessions can lead to economic contraction, unemployment and widespread financial problems.

# 2023-10-19 #

* Today's class caught my attention more compared to the previous one. Before the class I didn't know Harari, and in fact, I found the first video in class quite interesting, where he explores why fascism is attractive and how data can be used to promote it, highlighting the importance of education to counteract this trend.
* My favorite talk so far was Picciolini's, where he explained how he joined the movement due to a lack of identity and purpose in his life at such a young age, and that, despite having been a leader in that group, he was not satisfied with the direction it was taking him, realizing the negativity and hatred in it, he made the brave decision to leave the group and has worked to combat extremism and promote tolerance and understanding. .
* By the last video, I didn't pay much attention and to be honest, I doubt I will watch the entire video, but in a way I will have to watch important parts of it to have a better idea of how I could write the assignment and prepare the presentation for the next week.

## Homework ##

* I finally understood the concept of “fictional stories” and one of the ones that influenced my life last week is “career”. As a foreign student in Taiwan, I am committed to a career in biomedical engineering. Belief in the “career” narrative led me to put significant time and effort into college. This influenced my time management, as I had to balance studying with other activities and personal responsibilities. I mean, ultimately, the belief in the "career" story led me to make specific decisions about my education and time organization over the past week.

* The former president of Paraguay, Mario Abdo Benítez, has focused his leadership on issues such as economic stability, the strengthening of democratic institutions and the development of the country in areas such as infrastructure and education (areas that to this day still don't receive the attention they deserve, which leads citizens to feel dissatisfied with each political candidate). His vision of the future for Paraguay seemed to be based on the consolidation of democracy and sustainable economic growth, without adopting the extremes of the options mentioned by Thiel.

# 2023-10-26 #

* Today's class has been one of my favorites so far (I know that the next one will also have a great impact on me due to the topic to be addressed).
* In the first Ted Talk the idea was raised of changing the focus of the healthcare system, which currently focuses on treating diseases, to one that focuses on keeping people healthy. This approach can save money by avoiding costly procedures and unnecessary treatments.
* It is important to prevent diseases through healthy lifestyles and regular check-ups rather than treating diseases once they occur, this could help us stay healthy for longer and enjoy a better quality of life.
* My favorite talk of the day was the second, where a neuroscience specialist took herself as a test agent. Throughout the talk she shows (not only the positive results that the test brought in her experience, but also in a general way) how exercise benefits the brain by improving mood, memory and protection against brain diseases, providing compelling reasons to incorporate regular exercise into our daily lives.
* I think she couldn't be more right, because I consider exercise as part of my life, and I am shocked by the way in which spending several days without exercising affects me, especially emotionally.
* The results presented from the anxiety survey from previous years really impacted me and I think that the current situation of the students has not had a noticeable change. What is worth highlighting is that I appreciate the university's initiative to offer more psychological help for students, showing that they are also interested in our mental health.
* Having also completed the survey left me thinking for a long time about my current situation, reconsidering resuming sessions with a professional, since I consider it important to maintain a general balance between our academic and social life with our physical and mental health.

# 2023-11-02 #

* Although I was aware of the topic of the day, it was very shocking for me to see the videos of the talks presented in class, since lately I am very easily moved, especially with topics that involve mental health.
* In Nikki Webber Allen's talk I felt largely identified with her, since above all the things we achieved and have to do, we try to avoid giving space to something negative that presents itself or we feel, so as not to stop it from affecting everything else, this is where we believe we can experience sadness and still feel okay, which is really complicated and over time it becomes a heavier and heavier burden, since we do not let it go, we prefer suffer in silence, running the risk of reaching the point where we do not feel able to endure any more, that it ends up exploding.
* I can't imagine finding myself in the same situation as Kevin in the second talk, however, despite having witnessed how people decided to end their stay here, he was the light of hope for others when they found themselves in the darkest place of their lives, encouraging us to be that light in the lives of others, showing that there is a path to recovery and a better life.
* I found myself in the last talk, and I must admit that despite making the effort to take good care of my mental health, I have not yet taken the step of following sessions with a professional. I have already found myself in a situation where I had to live in a depressive environment where I had to put my mental health in second place to try to save someone else's, which I do not regret at all because today I see that person is much better and also makes me happy, but it is always important that we take care of ourselves before anything else.
* Assisting a friend with depression can be a bit complicated, but through small actions we can have a positive impact on their daily life, so it is important not to leave them to the point where they cry for help.
* We must show empathy and listen to them in a compassionate way, maintaining the connection with them, showing that you care about them and are willing to support them.
* Do not minimize any problem that may overwhelm another person and avoid judging, especially those who struggle with suicidal thoughts, and provide them with a safe space to express themselves, avoiding giving simplistic advice or quick solutions, since depression is a complex condition.
* I would also like to add one of Bill's tips, which is that adding a little humor with sensitivity can be useful to relieve tension and connect in a closer way, most of the time the other person just needs us to do normal things with them and enjoy it together.

# 2023-11-09 #

* I could not understand the first topic addressed by the professor, because I was trying to recover after continuing the discussion about depression.
* I have realized that I have so much to say about it, but it is something that I do not dare to talk to anyone about unless it is someone I trust very much.
* We often have difficulty proving ourselves right about ourselves because of the subjectivity of our own thoughts and beliefs.
* Through experiments, Johansson demonstrates that when our choices are manipulated without our knowledge, we tend to explain them in a way that is not based on the actual outcome.
* Forgiveness can help people overcome tragedy and find inner peace.
* There is a difference between being happy and having meaning in life. The speaker argues that happiness is fleeting, while having purpose and meaning in life can provide a lasting sense of satisfaction, helping us live a fuller and more satisfying life.

# 2023-11-16 #

* I felt too tired during the entire first hour of class and part of the second, so I didn't pay much attention to my classmates' presentations.
* It is strange to me to feel that when it comes to working in a group with Taiwanese in a subject taught in Chinese they ask me little or nothing to do, however, when it comes to a subject in English, I feel like many do not give it its due importance, wanting to leave it until the last minute, it really bothers me.
* I think I've reached the point in the semester where everything makes me a little more tired, easily losing motivation to do things, that's why homework has been bothering me lately and patience is what I have the least now.
* While I was writing my diary I realized that the lecture for week 9 is not in the drive, but I remember that it was about laws.
* I admit that along with economics, laws are not very to my liking either, but I have had the opportunity to read some from my country, and for homework I will also have to do it again.

# 2023-11-23 #
* To be honest and even though the teacher congratulated me for my honesty, it was the day I least wanted to read my diary in class.
* I found it interesting to hear the laws of different countries during the presentations.
* I liked the topic that was covered in the first video we saw in class better, since it is a shame for me to see how people are slaves to technology and especially talking about social networks (I must admit that I am not very far from that reality and I hate it).
* Although technology advances, it brings with it many benefits for humanity, but I cannot help but imagine being in a world where people feel more present, outside of the virtual world, where we know how to appreciate in detail the things that surround us, leading a healthier routine and leading us to happiness, less worries.
* I will continue to keep in mind the comment I made after this video.

# 2023-11-30 #
* I found several of the actions presented by the different groups interesting and in fact I would like them to be implemented; on the other hand, many of them seemed very complex and abstract to me.
* I hope that when carrying out our actions, they are accessible to both Taiwanese and foreign students, I am sure that more than one will be interested in participating.
* I think that the teaching method implemented by the teacher in this class is good, as students we are not used to other teachers offering spaces for participation for us. 
* It really hit me when the teacher mentioned that an important part of being a teacher, in addition to teaching, is learning from students; this reignited the flame of my desire to be a teacher in the future, because together you can discover endless paths of knowledge and I find it fascinating.
* I supposedly saw the video that the teacher showed in class, but I don't remember anything. I think that after seeing that it was about news, I automatically related it to the media and spent the time remembering a book that I would love to read, called: Amusing Ourselves to death.

# 2023-12-07 #
* I must admit that I felt a little upset about my presentation, I understand that we are loaded with activities, homeworks and we must study for exams, but I dislike that many students are used to leaving everything until the last minute, reaching the point that it has to affect other groupmates for various class work.
* I noticed that the classmate who had copied what I had written in my diary marked attendance and left the class before I came to speak. I would have liked to talk to him (I think he doesn't know that I realized what he did), to be honest.
* On several occasions, I like the fact that we work in big groups to discuss mini problems. I like to listen to different points of view from my classmates and learn new things from them.
* Before the class, I was not immersed in the topic of planetary boundaries, I had no idea of how bad the situation of the planet is becoming due to our misuse of natural resources and other acts that are gradually deteriorating our environment.
* At first I was amused by the professor mentioning that humans are heading towards communal suicide, but in all he is right, and it is becoming more and more difficult to try to reverse the deterioration already done on the earth.

# 2023-12-14 #
* I was surprised by the number of classmates who were absent from class; on the other hand, many had a hard time staying until the end of class to be able to mark attendance.
* It was interesting to hear the actions presented by the groups, however, most of them are difficult to implement at this point (including mine).
* I'm a little discouraged by the fact that I don't see it possible to put our peer support group into action due to the little time left and I'm afraid that this will greatly affect my grade at the end of the semester.
* I am so sorry that there was no time to continue watching the video presented near the end of the class, because it really caught my attention and I fell back into the reality of climate change. I am afraid that our time on earth is decreasing, but I still have hope that it can slowly be reversed.

# 2023-12-21 #
* I was a little discouraged by the fact that I had taken the initiative to prepare our presentation for class, but my groupmates decided to skip it.
* I like debate spaces in class because I can listen to other people's ideas, getting to know a little more about how each person thinks and learning something new from them.
* The presentation of the concept of Ikigai led me to reflect deeply, reaching the conclusion that it is really difficult to maintain a balance between all the aforementioned areas, but it would really be to achieve our general well-being.
* Lately I have realized how difficult it is to receive good feedback in an assertive and constructive way. This causes a stagnation in the personal development of someone in a certain social environment.

# 2023-12-22 Fri #
* Productive and unsuccessful.
* I felt productive because I spent most of my day dedicating myself to university things. I felt unsuccessful because I could have done a lot more if it weren't for my distraction with social media.
* I will activate my time limit for using my social media, respecting them when I reach the limit for the day.

# 2023-12-23 Sat #
* Unproductive and successful.
* I felt unproductive because in the end I couldn't do anything about anything related to the university. I felt successfull because finally after a long time I was able to talk to mom and I was able to arrive in time to share Christmas Eve Eve with my friends from the north, after receiving the invitation at the last minute and some difficulties on the trip. I was so happy :')
* I will try to complete my pending tasks tomorrow.

# 2023-12-24 Sun #
* Unproductive and successfull.
* I felt unproductive because I didn't have space to start studying for my final exams. I felt successfull because I was able to finish my tasks for the day and I was able to share our family Christmas with my friends.
* Tomorrow I will start preparing for my final anatomy exam.

# 2023-12-25 Mon #
* Productive and successfull. 
* I felt productive because I was finally able to start studying for my finals. I felt successful because I was able to talk to my family to wish them a Merry Christmas, also because I advanced towards my anatomy final.
* Tomorrow I will use my cell phone less to better optimize my study schedule.

# 2023-12-26 Tue #
* Productive and unsuccessfull.
* I felt productive because I was studying for my finals. I felt unsuccessfull because I still have problems distracting myself with my cell phone.
* Again, I'll try to use my cell phone less to better optimize my study schedule, maybe changing my wallpaper will help me as a reminder that I should continue studying.

# 2023-12-27 Wed #
* Productive and successfull.
* I felt productive because I was able to spend a lot of time studying for my final anatomy exam. I felt successful because I did well on my PE and English final exam, so I will have time next week to prepare an important presentation.
* I will continue trying to take advantage of the time left to prepare for my exams, celebrating my small advances and also giving importance to my rest to be more effective in my preparation for the upcoming exams.

# Five rules # 
* Seeking perfection without fearing failure, accepting that failure is part of the journey can lead to growth.
* Breaking tasks into smaller, manageable steps to make them less daunting.
* Choosing delayed gratification over instant rewards for long-term benefits.
* Breaking out of narrow social circles. Expanding connections and networks beyond familiar groups can lead to unexpected opportunities.
* Being positive and giving credit for every small progress we make on the way to our goals.

# 2023-12-28 #
* I was really struck by the number of classmates who don't know how to tie a tie. I don't usually use one either, but I have to be prepared for everything.
* I didn't really dress like an interview, but I decided to take advantage of the opportunity to dress well and I enjoyed it. I would have liked the other classmates to follow the theme. Btw, in case of a real interview I will take into account the teacher's advice regarding clothing.
* To be honest, I didn't even have 5 clear rules to be successful, but based on my experience I formulated them.
* After seeing the photos of the teacher with his clothes for various occasions, I realized that there is one more class left and I am really going to miss it. I don't come across such good teachers every day at the university, especially because I am a foreigner and have Chinese as a barrier in my communication with others.
* I feel a little discouraged by the result of my group project, I really wanted to complete the task successfully.
* If I have ever heard about some type of farm of the future, I did not see it favorably since I imagined we were consuming something molecularly modified and that in the long run it would cause damage to our organisms. But if this project really meets every requirement to be approved and also helps the environment, it really wouldn't hurt to support it and make it known worldwide.

# 2024-01-04 #
* I don't understand the reason why the video of my group in class couldn't be played, the only thing I know is that I was sure that there were many things that we should change but the rest of the members decided to just leave it like that, I'm afraid that that potentially affect my final grade.
* During the class I was feeling nostalgic because I was aware that it was the last one and I was sure I was going to miss it a lot.
* When the teacher showed us the reason why he was teaching that class, it touched me a lot and I saw myself reflected in him, because I really try to build a better world for my future family, seeking every day to achieve a better version of myself for them.
* The last thing I had to add about my feelings in this last class I already mentioned in the farewell email and gratitude to the professor for his exceptional work as a teacher and guide for future generations. Just thank you for everything.