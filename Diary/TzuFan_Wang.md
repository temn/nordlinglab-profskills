# 2021-09-23 #

### Feedback about Conflict View
* In my opinion, I am sometimes in harmony view, and sometimes in conflict view. 
* It all depends on the confidence of how much did I know about the issue. 
* If I can handle the situation, I will be a positive person when facing conflicts. 
* Something like academic issues or something that I can make my own decisions, I will be fine. 
* However, there are still some problems that destroyed my mind. 
* For example, my girl friend got epilepsy last two weeks. That was the most serious conflict I’ve never met before. 
* When something out of control, It seems that I would change to a person in harmony view.

### Difference between Group and Team
* Group: Only be grouped together, but don’t know each other.
* Team: Work together smoothly and achieve same goals.

# 2021-09-30 #

### Feedback about "We are in The Era of Code"
* Professor told us that in nowadays world, we are strongly connected with Codes.
* In my opinion, I thought that this comment is correct.
* Not only in the domain of Computer Science, but also in the domain of Mechanical Engineering,we all need to learn the computer language! 
* this will be the world,s trend.

### Feedback of Steven Pinker's Claim
* I would like to share my own opinion about his claim.
* Obviously, in his presentation, he had told us that we are in the era that progresses in a good way now.
* However, there are still lots of issues that he hadn't discuss in his report.
* He had just talked about changes in "Human", but there a large amount of species that he hadn't discussed.
* On the other side of those pros to human, animals and plants are suffered from dangerous situations.
* We should make the conclusion in more cautious way.

# 2021-10-07 #

### Feedback about the Questionnaire
* I think that I am not actually good at "financial knowledge".
* In my opinion, money plays a significant role in the world.
* However, we actually touch this area only when we get our first job.
* Since that we don't have enough money to invest without salaries.
* We can only talk about investment, but cannot really earn a lot on it.

### Feedback about the Class
* Today I learn a lot of "Money".
* I think the most interesting part is the part discussing "What is money?".
* Money is fungible, durable, portable, recognizable and stable.
* I didn't really search the meaning of money before.
* Actually, it is quite interesting!

# 2021-10-14 #

### Feedback about the TED talk: Why fascism is so tempting
* The speaker mentioned the problems that fascism may cause, 
* and how such a new dictatorship will control our thoughts through technology and machine learning. 
* This sounds creepy and thought-provoking. 
* In my opinion, Fascism and Democracy, and the discussion of this topic seems to be far from my daily life. 
* In fact, this problem exists in our daily lives. 
* Under the premise that we strive to pursue rapid integration and analysis of data. 
* Correspondingly, will other social issues arise? 
* I think this is a problem that our generation needs to face.

### Feedback about the TED talk: My descent into America’s neo-Nazi movement

* When I heard of the speaker saying that 
* "You see, it's our disconnection from each other. Hatred is born of ignorance. Fear is its father, and isolation is its mother."
* Actually, I cried out in a silence.
* When we don't understand something, we tend to be afraid of it.
* If we keep ourselves from it, that fear grows, and sometimes, it turns into hatred.
* It was almost talking about my childhood.
* My father and mother was always working when I was a child.
* At that time, I hate the whole world.
* Hate those bulliers bullying me because of my fat body.
* However, I hadn't thought before that the problem is my bad personality that time.
* When I finally realized this, it was time for me to graduate from high school. 
* But time is gone, I can only tell myself now and in the future.
* "To understand each other more, so as not to cause hatred that shouldn't exist."

### Feedback about "Yuval Noah Harari on Imagined Realities"
* Fictional reality creates objective reality.
* In fact Lawmakers are lawbreakers.


# 2021-10-21 #

### Feedback about the TED talk: Being Brilliant Every Single Day

* A challenge affects physiology, emotion and feelings, then affects thinking.
* Finally it will behaviour and performance.
* To learn to control our performance, we need to learn to control our emotions at first.

### Feedback about the TED talk: What makes us get sick? look upstream
* I think the con concept of "Health Begins" is great.
* I think that training upstreamists is a good idea.
* the main thing that they are interested in is changing the sense of confidence, that "don't ask, don't tell" metric among clinicians.
* In my opinion, it really hepls a lot!

### Feedback about the TED talk: The brain changing benefits of exercise
* In my opinion, brain is the part that most difficult to learn of people.
* I have a friend she has a brain desease, but still cannot find the reason that cause her epilepsy.
* Thinking really changes people a lot, but it hurts people on the other hand.
* We still cannot learn everything of our brain at all.

# 2021-10-28 #

### Feedback about the Discussion
* Making discussion with others in English is difficult to me.
* Actually, I was only listening to others rather than speaking actively.
* English is undoubtedly an obstacle to me.
* Thas was the main reason why I chose this course.
* I want to improve my English skills!

# 2021-11-04 #
### Feedback about todays presentation
* It was a lot of methods to cure depressed people being mentioned today.
* However, in my opinion, I think the most important thing to cure people is not only to stay with or support them.
* “Emotions will infect“, when someone is being anxious or depressed, your emotions will undoubtedly get worse. 
* The claim of “Take care of yourself then others” is widely mentioned in papers and articles on websites. 
* This is not a new thing in nowadays world. In fact, everyone in this era suffered from different mental problems. 
* This is the main reason why this claim is said again and again. Because that so many people get depression when thinking of taking good care of others.

### Feedback about the TED talk: Do you really know why you do what you do
* "If there are no differences between a real choice and a manipulated choice, perhaps we make things up all the time."
* Speaker made a great conclusion of his presentation.
* In my opinion, I have naver thought these questions before.
* I have learned a lot from this talk!

# 2021-11-11#

### Feedback about our team
* My big group members are all so good.
* We had sent a lot of time on our presentation.
* From preparation to presentation, we had done a great job.
* Our topic is about green buildings.
* It's not a new thing nowadays.
* However, I had found that I was not actually known this part.
* Due to the work, we knew about strategies that Taiwan had done.
* It was a great experience to me.

### Feedback about the Presentation
* This week is all for presentations.
* It is interesting to hear others thinking to me.
* Every opinions sounds great and deep.
* I think that I still have a lot to learn.

# 2021-11-25#

### Feedback about Physical Class
* Today is the first day of the physical class.
* However, I was at Kaosiung with my parents in the hospital.
* Through the Google meet, I can berely heard of people's discussion.
* Hope that I can be at classroom next week!

# 2021-12-02#

### Feedback about Physical Presentation
* Today is the first time I go to the physical class.
* Physical presentation is very difficult to me.
* Teacher's question of our group's presentation is so hard for me.
* However, I love the feeling of answering those questions.
* Because that it is a good way to show that we made a well prepare ofthe presentation!

### Feedback about Face to Face Discussion
* My English is very poor, so I am very nervous in our discussion.
* Nevertheless, my teammates are all very kind.
* We have a very good time today!
* Talking about what can we do for improving this world.
* We think that setting garbage can all over the school is a good solution to a large amount of garbage.
* The detail will be put on the google drive!
* Hope that everyone can make some comments.

# 2021-12-09#

### Feedback about Discussions
* Today, we are talking about media and News of different countries.
* In my opinion, there are lot of different sounds all over the world.
* However, the most important thing is always the capacity for independent thinking.
* That is, no matter what we listened, we need to think ourselves.
* This is the most difficult but not ignorable thing in nowadays era.

### Feedback about Super Group Discussions
* After todays lesson we made a short discussion about our next week's presentation.
* I found out that if we are talking about something that was proposed by others, 
* it is difficult to know about what is the critical problem of th issue.
* For us, without living beside the church.
* It is difficult to think about what problem is the most annoying part that should be solved.

# 2021-12-16#

### Feedback of CO2 Emmision
* Nowadays, people are trying to use green energy for sustainable development.
* Even in Taiwan, it is a hot issue.
* This weekend, Taiwan will hold a referendum.
* The energy issue played an important part in this referendum.
* As far as I am concerned, I think that nuclear energy and natural gas must be used alternatively.
* Because we are actively developing the semiconductor industry,
* the lack of energy will cause fatal injuries.
* According to the repeated power outages of Taipower this year, 
* people have even greater doubts about wheater our energy is enough or not.
* Under conditions of extreme power shortage, 
* I am even thinking of increasing the coal ratio is more reliable than expanding the proportion of green energy.

### Feedback of Group1 Presentation
* Converting paper to PDF is actually a good idea.
* However, even though we have a large amount of open sources online nowadays.
* I still loves to write down everything I read on papers.
* This will extremely help me to pay more attentions on the thing I'm doing then.
* If every sources converts to PDF, I still have demands on using papers.
* I am sorry for that TT

# 2021-12-23 #

### Feedback about presentations
* Today, every groups had a great presentation.
* It was happy that my opinion had been confirmed by professor.
* The energy issue is very important nowadays.
* However, still I don’t think that green energy works in Taiwan.
* Weather and democracy effects a lot.
* Take referendum this year as example, governments can not do their proposal at anytime.
* Sometime it’s good, but sometime it results to uncertain future. 


# 2021-12-30 #

### Feedback about Others 5 Rules that make me Successful and Productive
* We discussed everyone’s tips for the pursuit of success this week.
* I think something special is that someone mentioned, "Don't trust anyone, no one "must" have to help you.
* I think independence is a very important step for success.
* In addition, I also think that early to bed and early to rise are unrealistic.
* Because for me, if I do half of a project, I just go to bed.
* I can't sleep at all, the same thing is in my head.
* So I am more accustomed to doing it when I’m tired, I’ll sleep when I’m tired, and continue when I’m full, which is more efficient.

### Feedback about Supergroup Task
* Everyone has prepared the final report this week,
* But I found that many groups have not started to move,
* On the one hand, I’m very thankful that once we started to move,
* On the other hand, our subject was rejected,
* Because our theme has little impact in fact,
* The professor hopes that we can proceed to other topics,
* This actually makes me feel very anxious,
* I hope everything goes well at the end of the term.

### 211230 Thu. 
*  A. Unsuccessful and unproductive.  
*  B. I felt unsuccessful because our topic about final project of this course has been rejected. I felt unproductive because I haven’t done my final project of the course “Data Mining”.
*  C. I will try to make a proper schedule tomorrow, and try to finish my project and go on my final exam. 

### 211231 Fri. 
*  A. Successful and productive. 
*  B. I felt successful because today is the final day of the year, and I am happy today. Happy is the most difficult thing in my university life, It is important to be happy at the last day of the year! I felt productive because I stick to my schedule today, and finished my final project of the course “Data Mining”, it is good for me. 
*  C. I think I can stick to the rule of sticking to the schedule. 

### 220101 Sat. 
*  A. Unsuccessful and unproductive 
*  B. I felt unsuccessful because I have a fight with my girlfriend. It was my fault, I should not have done that. I felt unproductive because I was effected by my emotion and have done anything today. 
*  C. I think I need to consider others emotion, and not to be urge anymore. As the old saying goes “Urge is Demon”. 

### 220102 Sun. 
*  A. Successful and unproductive 
*  B. I felt successful because I had apologized to my girlfriend. Apology is not that easy, however, I have done it. You are the best , Tzu-Fan Fan! I felt unproductive because I spent a lot of time without doing anything before my apology to my girlfriend.
*  C. Apology is an art, it heals every relationships between me and others.

### 220103 Mon.
*  A. Unsuccessful and unproductive
*  B. I felt unsuccessful without any reason. I felt unproductive still without any reason.
*  C.I am so negative today, hope that tomorrow everything will be okay at all.

### 220104 Tue.
*  A. Unsuccessful and unproductive. 
*  B. I felt successful because I didn’t follow my schedule today. I felt unproductive still with the same reason.
*  C. I should try to relax myself, and not to be anxious any time.

### 220105 Wed.
*  A. Successful and unproductive.
*  B. I felt successful because I have taught others today, it makes me feel successful when teaching others. I felt unproductive because I didn’t follow my schedule today..
*  C. Emotion effects strongly, be happy everyday!

## Notes for 5 Rules that make me Successful and Productive

* Don’t sleep: Get more time to do my task.
* Pet cats: Be more responsible and take good care of others.
* Take this course: Learn a lot from professor and discussion due the class.
* Learning from teaching others: When teaching others, I can learn something again and again.
* Try everything I could when I am only a student: We will have right to make mistakes.

# 2022-01-06 #

* It's almost the end of the semester! I have a lot of tests to take next week.
* I totally support gender equality, but I cannot agree with the notions of many other people regarding the same topic.
* By the way, I love my super group teammates.
* They helped me a lot.

# 2022-01-13 #

* Final class of course
* Today, I get 100 on the final exam.
* Please kindly give me A+!