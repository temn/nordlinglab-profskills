# 2023-09-07 #

* During the first lecture, I knew more about the syllabus and criterion of this course.
* The topic we're going to discuss about this semester are definitely necessary for students who work and live in this society for the upcoming 40 years.
* It's a little stressful for me to have lecture, do homework and discuss with group members complelely with English.   
However, I take it as a good chance to get acquainted with people from other departments or even other countries.  
I look forward to meet group memebers.  


# 2023-09-14 #
* About the team work:  
    * After the fisrt class last week, I sent email to the memebers in the group of mine, and they are all easy-going and diligent.    
    * We search online for the presentation sepatarely at first, put forward our prosal and then picked the most comprehensive one.
* Professor mentioned the column "Conflict view" of the spreadsheet of student information.   
I like to share different thoughts and ideas with others, because they bring inspirations.  
I'm glad that Andy and Timotius feel the same way.
* During the course of this week, several diagrams impressed me.    
The influence of important inventions is a little differnet as my previous thought.   
The impact level of computers and the internet are much lower, but the genome sequencing is much more greater.
* While the production goes up, it usually makes the cost goes down subsequently.    
Though it's more accessible to people, but does it really bring the happiness? I think it's not that easy and intuitive to answer, there're still tons of aspect should also be concerned.


# 2023-09-21 #
* It's the era of information explosion, we literally know that we should be skeptical when we get pieces of information or data.    
But actually, I didn't really learn the way to stay sober and smart on all the infos we've gotten.  
    * Although the speed of the speaker in the 1st video was a little fast that she lost me several times, I went home and watched again 'til I fully catched. I took it useful and provocative.
    * I like the 2nd video the most among the three ones.    
	The way he made the data into different diagram and demonstrated was interesting.

### HOMEWORK
>
> **HW1**   
>> According to my career development, 10 years from now, I would live in Hsinchu which is my hometown.    
I would like to work as a mechanical engineer in Hsinchu Science Park (HSP) or Industrial Technology Research Institute (ITRI).   
My work is about simulation and analysis, and maybe have a cooperation with the government or private enterprises.   
Maybe I spend most of my time for work when I start working, but I would adapt the work time for my health and family.   
Beside the work, I plan to marry before 30 years old and have a newborn after that.    
Having a kid will change my life a lot, it takes me more time and cost me much more than before because I have one more family member to take care.   
>
>>The 5 current trends of change that might influence me is:   
>> 1. **Inflation**. Although this situation has been a couple years, I think the prices will never stop hiking within 10 years, so it would still be a big issue for everyone in this world. If the raise of income doesn’t meet the inflation, then it would be difficult to have a surplus.    
>> 2. **The growth of artificial intelligence**. AI is no more tested, but actually used in industry. Most of the workers might be replaced by robot arms and automatic manufacturing, but engineers are the ones who command and control the machines. Engineers won’t be replaced by AI completely 10 years from now, but that’s the trend of the future.    
>> 3. **Energy crisis**. Lack of energy in Taiwan, especially electricity, is always an issue that political parties have argument on. Electricity is the foundation of daily life, and the main industry in Taiwan and the future technology rely on electricity as well. The requirement of energy won’t get less but the production can’t meet the growth of it.    
>> 4. **The tension between Taiwan and China and the wars**. No one can ensure China won’t invade within a decade and I can hardly believe that China would become a democratic country, either. The trade war is still active. As the war between Ukraine an Russia, dispute between countries will never end, which make the world turbulent.    
>> 5. **Intensified information explosion**. We get more and more infos from the internet through social media, news and other sources. It might tend to cause more ethnic conflict and weaken the ability to distinguish right from wrong.    

>**HW2**   
>> News: [African rhino numbers increase for first time in over a decade](https://ground.news/article/african-rhino-numbers-increase-for-first-time-in-over-a-decade_2b3f04)   
>
>> - In a surprising turn of events, rhino numbers in Africa increased last year, providing some much-needed good news for a species threatened by poaching.    
The International Union for Conservation of Nature reported a 5.6% increase in white rhino numbers, marking the first growth since 2012.   
>> - Despite the rise in poaching incidents, black rhino populations also saw a rise of nearly 5%.     
Conservation efforts, including the establishment of new populations that have grown in size, have contributed to this increase.     
While there were 551 rhinos killed in Africa in 2022, this number is significantly lower than the peak of 1,349 deaths in 2015.   
>> - The overall rhino population in Africa has seen a 5.2% increase, with nearly 23,300 rhinos roaming the continent at the end of last year.     
Although this positive development is a step in the right direction, rhino populations have been devastated over the years, dropping significantly from approximately 500,000 in the 20th century.


# 2023-10-15 #

* It took me plenty of time for the presentation.    
During the preparation, I really  learnt a lot and put skills taught in lecture into practice.
* I also found that the website of factcheck was kind of interesting.     
Though I didn't receive mcuh fake news on social media, it's still useful and I could learn some simple skills of verifying facts.
* The survey of finance is a little difficult to answer because I'm not really into economics then.   
I know the basic knowledge it's crucial as an adult in this society, I'm still lack of motivation to learn about it.   
Though not that accurate, several of my answers to the survey just somehow right, I had no idea how this happened hahaha.
* As my first lecture about money, finance and econ in college, I found it quite underatandable. Hope it could pique my interest:)
* This week marked the beginning of a new semester, and I found myself immersed in the world of mechanical engineering. From the introductory lectures to meeting fellow students, the excitement of diving into the intricacies of machinery and design became palpable. Navigating through the course syllabus, I look forward to the challenges and growth this semester promises.


# 2023-10-22 #
* The first video is about the difference of facsism and nationalism.
After watiching, I'd take fascism as a tiny branch of nationalism but lack self-awareness.     
The only one way to prevent ourselves to be controled, then we should be familiar to our own weakness, make sure the data are non-centralized and still be processed efficiently.   
* I watched a lot of videos about cult.    
Generally, when someone's lost, lonely, insecure and not cared about, they tend to go somewhere to find the feeling of being needed. 
The white supremacism and neo-Nazi mentioned in the second video are just like cults, the outsiders might consider them evil, unreasonable and ridiculous.     
However, it's hard for those who are brainwashed to realize that they're axtactly believers of the cult or the extremism.  
* As the second week unfolded, I encountered my first set of challenges. Balancing the workload of multiple courses and adapting to the rigorous demands of engineering courses presented initial hurdles. However, by seeking guidance from professors and collaborating with peers, I began to grasp the importance of teamwork and problem-solving within the field. This week taught me valuable lessons about resilience and the power of collective effort.

# 2023-10-29 #
* During the course this week, we discussed about the chioce of paying subscription everyday (or in a specific period) for staying healthy or paying when we're sick.   
Here's my choice. Suppose my income is sufficient, I’d choose pay regularly if it ensures I'm 100% healthy.    
Learnt from the experience of my grandmother, I know how illness impacts a person and the whole family. If I can choose, stay healthy is the best way to live in peace and security.
* Although in reality, we do pay national health insurance, doctor only get paid when we’re sick.    
It’s still a little different from the health subscription we discussed before.   
* Another video is about how excerise influence our health.
Exercise is an instant way to improve physical fuction and achieve longer attention, besides, exercise can protect brain and prevent diseases.
The lecturer said that doing exercise for 30-minute 3~4 times a week really helps a lot.   
Even just stand up from the chair, walk around in the office and climbing upstairs can provide a little change, too.   
In the end of the video, all of us stood up and did a little stretch, that was quite a cute scene hahaha!


# 2023-11-05#
* "Don't suffer from your depression in silence"
Patients with depression don’t need to hide.    
Sharing the experience is also a way to accept the illness, and help those who 
* "How to connect with depressed friends"
Depressed people still want to have connection with others but lack of ability to do it.   
Don’t expect yourself to cure the depression of your friend. Just talk to them friendly as usual.   
Connection is not necessary to every patient.
    * Don’ts: usual ways don’t really work on depressed ones. Don’t give advices and ways try to pull them out of the upset mood.   
    * Dos: do something unrelated to depression with your friend. Take them same as you. Make them know that they’re valuable and their lives are beautiful, too.    
* "The bridge between suicide and life"
The best ways to give solace to the ones suffers from depression are talking and listening to understand. 

# 2023-11-19#
* From the homework this week, I learnt something that I didn’t know before.    
For example, the effect of noise of the traffic and how the noise was made.   
Futhermore, it's the first time cooperated on the presentation with group memebers of the final project, we all did well on the presentation.    
* Though this week we didn’t have enough time for the new topic of the lecture, the presentation of other group were kinda interesting.     
I really look forward to the further cool discussion of those topic.
* The third week delved into practical applications, providing a hands-on experience that brought theoretical concepts to life. From working in the lab to engaging in group projects, I gained insights into the real-world implications of mechanical engineering. This week reinforced the significance of bridging theory with practice, fostering a deeper understanding of the subject matter and sparking a passion for the field.


# 2023-11-26#
* About the regulation in Taiwan, I found it hard to implement because the standard is ambiguous.    
It’s basically notice but no enforcement on the noise makers unless the one got reported for many times.    
Besides, I don’t think it’s common for Taiwanese to report the noise problem of neighbor.    
But from the presentation of group12, I found that the implement of noise control in Croatia is somehow the same of that in Taiwan. So maybe we could say that the noise is hard to be controlled.

* The second clip is about the addiction of using phones.    
      * Brain produces dopamine when receiving text message, gamblinga and having alcohol. Jsut like drinking and gambling, using phones is not bad but too much is bad.
      * Adolescence recently don’t know how to develop dee relationship.   
   We really need to teach them and maybe learn hard about atience, love, job which give fulfillment, feel the joy of life, building confidence.   
      * Real relationship is developed by real interaction with other people.
      * Remove the temptation it makes it easier to stay away from the things you’re addicted to.
      * I think the addiction is kind of mental illness, the root cause maybe be some lacking mentally.

# 2023-12-03
* video: How to choose Your News       
      * When big news comes out, we can absorb some information regularly but not always receiving the newest ones.    
      Let time to prove what the truth is.      
         If it’s available, try to get the first- handed information, it could be disorganized but that’s the strong point of it, because it’s not over-polished.       
         It’s hard for most of the people to do so, thus, reading or watching from multiple source is a nice way to gt close to the truth, too.     
      * Anonymous sources are not reliable. We can take them as somebody’s opinion but don’t trust them too much.     
      * Make it a habit to check the fact before spreading the information onto your social media.    
* I have several opinion about the presentation of group 2, whose topic was about actions to improve the education in Taiwan:          
      * Teaching method: I think lower the amount of student in each class may be a good beginning.      
        In my opinion, when Taiwanese students are in a class which has a lot of students, we tend to not asking questions and making discussion.       
        Maybe it’s the habit formed since young age that teachers wanted us to be quiet during classes.     
      * Student feedback doesn’t really improve the status quo of education. It's still a method that is less positive.


# 2023-12-17
* The presentation this week need an interview of someone whose job is about traffic noise measurement and management.    
We interviewed a dorm officer, who might have dealt with the complaint form student about traffic noise. But unfortunately, he didn't have any experience about this topic.    
Maybe we can have an interview with police of East Distric, Tainan, and ask some details about traffic noice management.    
* As the month concludes, I reflect on the strides made in my understanding of mechanical engineering. The combination of theoretical knowledge, collaborative efforts, and hands-on experience has significantly contributed to my growth. This week highlighted the importance of continuous learning and adaptation in the ever-evolving realm of engineering. Looking ahead, I am excited about the upcoming challenges and the opportunities they present for further development.


# 2023-12-31
* Today, I had a discussion with the professor from the Solid Mechanics group regarding the details of my project proposal.     
Winter break is starting next week, and I plan to dedicate this time to reading research papers and gathering relevant information.     
My goal is to complete the project proposal by the end of February and submit it to NSTC (National Science and Technology Center).      
If the proposal gets approved, I will commence my research from July to February of the following year and submit a report on the results.     
It's a challenging timeline, but I'm excited about the opportunity to delve into my research and contribute to the field of solid mechanics.      
The winter break will be a crucial period for laying the groundwork and familiarizing myself with the literature before diving into the project in earnest.      
I look forward to the journey ahead and the potential impact my research may have within the academic community.





