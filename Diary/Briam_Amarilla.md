This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-19 #

* The class talked about stuff like linux, git and bitbucket
* Some of the presentations where quite interesting, including Memory availability, food crops and cell phones.
* The constructive criticism was very helpful in order to have a clear view on how to present
* i learned quite a bit, including types of conflict management, how to use bitbucket and some other topics related.

# 2024-10-24 #
* Three rules to live healthy 
* Have a purpose that drives you: find joy and fulfillment in the stuff you do
* Maintain a social life: having friends and hanging out can improve our quality of life
* balance diet, sleep and exercise: this is more of a foundation for everything else you do in life

# 2024-11-07 #

* In this class we talked a lot about depression and sucidide which is a very important and delicate topic. 
* The teacher shared a story about his own experience dealing with someone who was in the verge of taking their life, and how he managed that situation and how it affected his life. I myself had this kind of experience and can relate to this topic very deeply. 
* The videos we watched really resonate with my experience and my opinions on how to deal with people with depression as I have been there too.
* I wanted to emphasize that all three videos talked about bridges in some way or another which I found really interesting, one of the videos made an analogy about making a bridge between ourselves and people with depression (or at least a bridge to a better understanding and connection). 
* Other video literally talked about the golden gate bridge and experiences with people that committed suicide there. 
* An analogy that I came up during the class is that life is like walking on the bridge, we all walk alongside but some people walk in the middle of the street and others walk near the edge.
* those who walk near the edge are constantly looking to the water and are just steps ahead of taking the leap, and we all see them but we don�t realize that they are in that position.
* we have to be careful on how we handle those situations just like the man talked about in the video, with what we say and how we act towards other people, we might be saving their lives

# 2024-12-26 #

* Productive and sucsuccessful 
* thursday i wen to the class and i haven't been to the previuous one so i didn't knew we had to "dress to impress" so when the teacher gave us time to go home and change i took the oportunit and went 
* straight back home and did my best to choose the most suitable clothes i had. I wasnt really super happy with it cause i wansn't prepared neither did i had elegant clothes but I managed to win when i wen to the front!
* Also that day I talked a lot cause I had to present for my group (group 7) and also the 5 rules and some things more. I was very... sad i would say, and perplexed that sooo many people said things I really don't agree with, and i need to clarify those things so i will do it here.
* The first thing and the teacher already adressed this but you dont need to be successful in order to try and achieve success everyday and tell people about it, success has many ways and shapes and forms, and maybe onde day it looks like winning an award, and other days might be cleaning your room, so be gentle with yourself.
* Something else that I found shocking is the amount of people that had on their rules for succes "always be happy" or some variation... and that struck me hard cause that is NOT AT ALL TRUE!.
* You shouldn't be trying to be happy all the time, not at all! life has its ups and downs and you have to embrace all of it, some days you will feel good, some days you might feel bad, but you shouldn't feel like its wrong to be sad or angry or lost or anything, its a very important part of life!
* And just to close my thought for thursday, there is this analogy that I love. Emotional Scars (like feeling bad, being sad, angry, having had an event that shook you, someone did something to you and you feel awful) are just as important as physical scars.
* When you have a cut or a wound, you take care of it, you clean it (if its bad you go to the doctor) you put some medicine on it, and by taking care of it and actually paying attention to that wound, it heals, you make it heal.
* The same applies for emotional wounds or scars. You cannot ignore the things that are hurting you or weighing on you or they will just get infected, you have to treat them the same as physical scars, take care of them, clean them, go to the doctor if needed, talk about them, feel them.
* This is way harder than physical wounds or scars because of how easy is to just try to ignore emotions and distract ourselves, but in the end if you dont deal with this things, they will never go away.
* So my advice is, FEEL EVERYTHING, completely, even if its uncomfortable, be happy too of course, but dont refrain yourself from being sad, it truly helps! 

# 2024-12-27 #

* Unproductive but successful 
* uff, this friday it took a lot of effort for me to get out of bed, i slept almost until 12, and it kinda made me worried because the night before i had slept really early, around 9 or 10, so I was definitely having a weird (hard) time this morning.
* When I eventually leaved my bed, i cleaned my room a bit and helped a friend with a music project he had, cause I kinda have a home studio (very very rudimentary i would say) in my room, so he came and we worked on his project for like 3 hours.
* After that another friend came and we worked on a couple of song of our own and we were recording and talking and just figuring things out, it was very fun but man, the patience needed to found the right sound, the right time, the right chord, it takes time.
* Overall it wasn't a bad day, although it started a bit heavy on me, it got better at the end. I didn't do much academically speaking but i enjoyed having done music for the whole day.

# 2024-12-28 #

* Productive and successful
* Saturday, I dont remember what time i woke up, but we got together with some friends to study and do homework in the library, we rented a room and stayed there until night time. It was very very good cause I got "emotional support".
* I didn't do much honestly, but I'm still happy about the results, cause I was very behind with eng math so what I did was i loaded up all my AI tools (I use AI a lot to help me organize my mind and life and it helps me a lot) I gave it every note and book I had and explained what I needed to do,
* and asked it to make me a step by step basically of what do I have to do to fully understand this chapters and how to study etc and so I started slow, reviewing concepts, and just following the guide, so although it seemed like I did "nothing"
* I was actually feeling way more confortable with the subject and least got a broad idea of how to approach all of it and how to break it down into smaller portions and not be overwhelmed with the sheer amount of information I need to consume in such a short amount of time.
* And although it is 100% my fault for not starting earlier and prioritizing some stuff versus other, I still have to take responsability and do the best with what I have, so im happy still about my saturday!

# 2024-12-29 #
* Productive and Successful
* Sunday I did the same, i went to the library to study, there's not much to say about it honestly, we again rented a room with friends and just studied there the whole day, I enjoy studying with friends quite a bit cause it gives me some  emotional support, seeing people studying motivates me.
* Also when we take breaks from studying like every hour or so, we laugh and joke and de-stress a little bit which is cool.

# 2024-12-30 #

* Unproductive and unsuccessful 
* Harsh day, I didn't have class that day cause I already handed my final and did a presentation and everything so i was free basically, but it backfired cause I didn't do anything, at some point decided I was just gonna play genshin impact so I stay in my room and that's what I did. Very proud! /s 

# 2024-12-31 #

* Productive and successful
* Tuesday I went to both my morning classes, I remember being in quite a good mood that morning, professors anounced the time for next week's final and everything, which I was waiting for eagerly cause I needed to have a concise deadline to be able to work properly.
* What I mean is, even tho I know next week is 100% gonna be the final, not having a time and place and everything kinda makes my mind go "it's not real, you have time, you are fine man", so it relaxes and procrastinate so I have to fight with my mind to be like, dude, you are 100% aware you have a final next week.
* Anyway after lunch we studied in the library while planning what we were gonna do at night, we ended up going to this mexican place and eating some burritos and nachos with my girlfriend and some friends, there was this band from the Philippines playing there and it was quite a good time.
* I have to admit i stayed up really late with said friends playing music and having a good time after midnight. Happy New year btw!

# 2025-01-01 #

* Unproductive but successful 
* First day of 2025, woke up reallyyyyyyyy late and also decided it to call it a day, ate McDonald's and played games, great way to start the year!

# Obituary #

# Briam Amarilla #

* Briam lived life to its fullest, embracing every facet of existence with an open heart and mind. He discovered profound spiritual truths and shared the goodness within him to make the world a better place. His actions left a lasting impact on countless lives.

* Through his art—whether in songs, books, or heartfelt journal entries—he captured the raw spectrum of human emotion: joy, pain, love, trauma, philosophical musings, and everything in between. His work resonated deeply with those who faced similar struggles, offering them solace, inspiration, or simply the comfort of knowing they weren’t alone. His creations moved people to grow, heal, and find greater meaning in their lives.

* The world, even if only by a small measure, is brighter because he existed.

# People that have achieve this or similar # 
* Bill Viola (1951–2024)

* Bill Viola, a pioneering video artist, passed away at 73 due to complications from Alzheimer's disease. Viola's work was heavily influenced by a near-drowning experience at age six, which he described as a transcendent and beautiful moment. 
* He strived to capture this essence in his art, focusing on fundamental human experiences such as birth, death, and consciousness. His notable works include Nantes Triptych, depicting birth, suspension in water, and death, and The Messenger, which faced censorship at Durham Cathedral. 
* Viola's career spanned significant exhibitions and retrospectives, including at Tate Modern and the Venice Biennale. His work often merged technological innovation with profound spiritual exploration, integrating elements of Christian iconography and Zen Buddhist philosophy. Despite mixed critical reception, Viola's impact on contemporary art and video installations remains profound.

# what enabled him to achieve this? # 

* 	Bill Viola’s success came from turning personal experiences, like his near-drowning as a child, into universal art exploring life, death, and spirituality. He embraced emerging video technology, combined it with deep philosophical influences like Zen Buddhism, and maintained a meticulous dedication to craft. 
* His wife and collaborator, Kira Perov, ensured his vision reached global audiences, while his resilience and focus on universal human themes cemented his legacy as a pioneering video artist.



# Topic 1: Know the Current World Demographics #
* From the class materials, I understand that knowing global demographics is essential for identifying engineering challenges, such as aging populations requiring accessible infrastructure or urbanization demanding sustainable city planning. These trends shape how engineers respond to societal needs.
* To get a better grasp of this topic, I could start by exploring one major demographic trend, like urbanization or population aging, using resources like UN or World Bank reports. I could then analyze how this trend impacts engineering needs—for example, designing water management systems for urban areas or assistive technology for the elderly. Reflecting on how these insights apply to past coursework or projects would further solidify my understanding and link the topic to practical applications.
# Topic 2: Use Demographics to Explain Engineering Needs #
* A great way to understand how demographics influence engineering is by playing city-building games on my phone. These games let me create and manage cities, and I can clearly see how certain trends affect others. For example, when my city’s population grows, I need to build more houses, police stations, schools, and electricity plants to keep everything running smoothly. If I don’t plan properly, things start to fall apart—power outages happen, traffic jams get worse, and people start leaving.
* By playing and experimenting with these games, I can see how population size and other factors shape engineering needs in a simple but powerful way. It’s a fun and interactive way to visualize how demographics affect infrastructure and get a deeper understanding of the topic.
# Topic 3: Understand Planetary Boundaries and the Current State #
* Planetary boundaries define the safe operating limits for humanity to avoid significant environmental damage. They include critical areas such as climate change, freshwater use, and biodiversity. Every action we take, no matter how small, has a connection to these boundaries. For example, how we travel, what we eat, and the resources we use daily all contribute to either staying within or exceeding these limits.
* To better understand this, I could label some of my daily activities and connect them to specific planetary boundaries. For instance, commuting by train or bus impacts climate change through CO₂ emissions, but switching to a bicycle reduces this impact. Similarly, the long showers I take or the bottled water I drink relate to the freshwater use boundary. By reducing water waste—like opting for shorter showers or carrying a reusable water bottle—I can positively influence this boundary. Taking small, deliberate actions for each boundary helps me visualize how my choices affect the planet and makes the concept of planetary boundaries more tangible and actionable in everyday life.
# Topic 4: Understand How the Current Economic System Fails to Distribute Resources #
* The economic system is vast and complex, covering ideas like inflation, loans, taxes, and resource allocation. It’s difficult to grasp these abstract concepts without experiencing them in some way. The teacher’s exercise of “losing” 20% of my cash to simulate inflation was a simple but powerful way to make an abstract idea tangible. I could feel what it meant to lose purchasing power and later appreciate how inflation affects individuals and economies alike.
* The teacher has also mentioned an idea for a class game to simulate an economy, and I completely agree that this would be the best way to make sense of such a massive topic. In this game, students could play different roles in a government, like being part of the central bank, the finance ministry, or other governing bodies. Each role would have responsibilities—such as setting interest rates, collecting taxes, or deciding how to spend public funds. Together, the class could manage a simulated nation, navigating challenges like inflation, debt, or uneven resource distribution. For example, the central bank could raise interest rates to curb inflation, while other groups debate funding healthcare versus infrastructure.
* This interactive approach would let us see how different parts of the system work together (or sometimes fail) to manage resources and wealth. It would also make the topic come alive by forcing us to confront real-world dilemmas, like choosing between short-term fixes and long-term sustainability. By seeing how our decisions lead to resource inequality or economic instability, we’d gain a much deeper and more practical understanding of the flaws in the current economic system.
# Topic 5: Be Familiar with Future Visions and Their Implications, Such as Artificial Intelligence #
* Understanding AI’s implications goes beyond its technical side—it’s also about ethics, history, and how it shapes society. A class I took at NCKU, Philosophy of Information, helped me realize how important it is to stop and think about the hidden dilemmas AI brings, like issues of data ownership or bias. AI may look like a fancy tool, but at its core, it’s just algorithms sorting data, and how we use it matters a lot.
* One thing that stood out in that class was how the teacher encouraged us to use AI responsibly. She let us use it for assignments as long as we explained how it helped, what prompts we gave, and why it improved the outcome. This approach taught me to see AI as a tool to refine my thoughts, not replace them.
* Personally, I’ve started training my own AI systems for specific subjects to help me study. Having one trained system per subject has made it easier to manage my classes and given me a much deeper understanding of the topics. I’d suggest others explore how AI tools can be integrated into their lives in ways that help them tremendously—whether it’s for organizing ideas, studying, or staying on top of tasks. It’s all about finding responsible and effective ways to use these tools.
# Topic 6: Understand How Data and Engineering Enable a Healthier Life #
* Data and engineering have become such a big part of improving health, both for individuals and on a larger scale. From fitness apps to medical devices, they help us understand our habits and make better choices for our well-being. It’s incredible how these tools can make something as personal as health feel more manageable.
* For me, one of the most impactful ways I’ve seen this is through a mood tracker app. I’ve been working on my mental health and needed to pay more attention to how I was feeling. A few weeks ago, I started using a mood tracker  app daily, and it’s been eye-opening. It doesn’t just track my mood but also connects it to the things I do each day, like exercising, hanging out with friends, or working late. Over time, I’ve started to see patterns—like how certain activities consistently make me feel better. It’s a small habit, but it’s made such a difference in how I understand and manage my mental health.
* I’d honestly recommend trying a mood tracker to anyone, even for just a couple of weeks. It’s a great example of how smart design and thoughtful engineering can make a huge impact on your life. 
# Topic 7: Know That Social Relationships Give a 50% Increased Likelihood of Survival #
* I find this topic fascinating because it ties so deeply to our history as humans. For thousands of years, we lived as nomadic tribes and hunter-gatherers, always connected to each other in small, tight-knit communities. It’s only recently, with how fast everything is changing, that we’ve started to live more isolated lives. Despite all the modern conveniences we have now, the core of what it means to be human hasn’t changed: we’re wired to connect.
* The idea that social relationships can increase survival by 50% makes so much sense when you think about how we’ve evolved. Staying together wasn’t just about emotional support; it was about survival—protecting each other, sharing resources, and solving problems as a group. Even now, when life feels overwhelming or disconnected, it’s connection with others that heals. Whether it’s a deep conversation with a friend, a laugh with family, or even small moments of kindness, it reminds us of who we are and grounds us in what really matters.
* To better understand this, I could spend a week focusing on intentional connections. For example, I might reach out to someone I haven’t spoken to in a while, make time to meet a friend in person, or even join a group activity. As I do this, I’d reflect on how these interactions make me feel and whether they change my stress levels or mood. Seeing firsthand how social bonds influence mental and physical well-being would give me a deeper appreciation for their importance.
# Topic 8: Be Familiar with Depression and Mental Health Issues #
* This topic is deeply personal for me because I’ve gone through periods of depression and have seen friends deal with it too. That’s why I felt it was so important to share some words with my classmates that day and encourage everyone to take time to process their feelings. It’s okay to feel bad, cry, or simply acknowledge how you’re feeling—it’s a necessary part of healing.
* A practical way to understand this better is by checking in with others. Ask a friend, “How are you?” and then follow up with, “How are you really?”—and then truly listen. You’ll often find that people are struggling but just need someone who cares to open up. Of course, not everyone will want to talk, and that’s okay too. It’s about showing you’re there for them if and when they’re ready. I would encourage everyone to try this because even small gestures of care can make a big difference.
# Topic 9: Know the Optimal Algorithm for Finding a Partner for Life #
A practical way to better understand this concept is to take some time to reflect on what truly matters to me in a relationship. I could look back on past experiences and think about what worked, what didn’t, and why. Another useful approach could be to intentionally focus on getting to know people deeply, taking the time to understand their values and priorities before rushing into judgments. This helps build a better sense of what aligns with my long-term happiness and allows me to approach relationships with more clarity and intention.
# Topic 10: Develop a Custom of Questioning Claims to Avoid Fake News #
* A practical way to better understand this concept is to choose one piece of information every day—a news article, social media post, or even something shared in conversation—and trace it back to its original source. Check if the source is reliable, look for supporting evidence, and see if there are other perspectives on the same topic. Doing this regularly helps build the habit of questioning claims and makes it easier to spot misinformation.
# Topic 11: Be Able to Do Basic Analysis and Interpretation of Time Series Data #
* Having used a mood tracker app, I realized how powerful it is to see data about myself and understand parts of my life better. Tracking my mood over time and seeing how it related to my daily activities made patterns so much clearer and helped me make positive changes.
* I’d recommend others try gathering data about themselves—whether it’s their mood, sleep, or habits—and see what the data reveals. It’s a simple way to gain insights about your own life while also practicing how to interpret time series data in a meaningful and personal way.
# Topic 12: Experience of Collaborative and Problem-Based Learning #
* Me and my friends have this running joke when we study together: “Between the three of us, we’re almost one full student” or “one full brain.” It’s funny, but it also reflects a real truth—collaborating with others often leads to better results. Combining different perspectives and strengths makes problem-solving faster and more creative.
* One idea I came up with to test this is a timed experiment. Pick a topic or project and work on it alone, timing how long it takes to come up with a plan or conclusion. Then, do the same with one other person, and finally with a group of three or four. Compare the results—how much time it took, how detailed the plan was, and the quality of ideas. My guess is that the group efforts will not only be faster but also result in better solutions. I haven’t tried this yet, but I’m excited to test it out because it seems like a fun way to really see the value of collaboration in action!
# Topic 13: Understand That Professional Success Depends on Social Skills #
* I firmly believe that social skills are the foundation of everything in life. They allow us to connect with people, whether it’s for work, friendship, or finding a partner. To really understand this, you need to experience connection for yourself.
* A simple but impactful way to do this is by starting conversations with people around you and listening carefully. For example, go to the second-hand bookstore near the subway and ask the lady working there about her favorite book or for a recommendation. It might sound a bit metaphysical, but there’s a kind of energy in connecting with others, in truly listening to their stories and noticing how it makes you feel.
* We should all give ourselves the chance to really hear other people and be present in those moments. It’s through these small interactions that we can see how social skills shape our connections and enrich every part of our lives.
# Topic 14: Know That the Culture of the Workplace Affects Performance #
* One of the best ways to truly understand this topic is to think back to courses you’ve taken in school or university. Maybe there was a subject you could have enjoyed, but a bad teacher made you dread it—or, on the flip side, how an incredible teacher made you fall in love with a subject like Mathematics. This shows how the environment, whether positive or negative, shapes emotions and behavior.
* The same idea applies to the workplace. A supportive and engaging culture brings out the best in people, while a toxic one can do the opposite. Reflecting on these experiences helps us see how much the environment matters in shaping both performance and attitude.
