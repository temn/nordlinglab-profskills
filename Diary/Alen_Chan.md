This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Alen Chan E14086127 in the course Professional skills for engineering the third industrial revolution.

# 2019-02-20 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* I don't think exponential growth applies to food production.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I had seen Steven Pinker's TED talk.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.

#2022-09-08#
* It is great to be back to get back to a english taught course.
* The oprofessor mention that group disscusion is very helpful for learning.
* It's nice to talk English in class.
* I think we should all be willing to talk more in class.
* We  will be presenting our progect the next week, so exciting!

#2022-09-15#
* It is so bad that I didn't present our peoject due to the lack of class time.
* It was a little messy in the class because it is the first time we using GIT and we are still learning how to use it.
* I think it would be better if we resort to the physical class, because it is a better way to practice our speech skill in English.
* Hope we have enough time to let all the group to present.

#2022-09-22#
*It’s year 2022 now, and I’m a college student.
* I think in 2032, I’ll will be in a company that doing the materials industry, and probably I won’t live and work in Taiwan, but in a foreign country, such as the USA or Australia.
*I think the five trends nowadays, the first one is electric car.
*There are more and more companies have been developing electric cars, and there are more and more E-cars on the street now.
*I believe E-car will be spread all around the world, and there will be no gas car in 2032.
*And there will be less carbon footprint ad less noise pollution then.
*The second one is AR technique.
* In virtual reality, we can do a lot of things that we can’t do in the real world, and people use that to experience things that they can rarely do.
*And I think in 2032, VR technique will be more advanced, which can let human enjoy all the activities and games in it.
* The third one is green energy. All the countries are doing their best to develop green energy, hoping to reduce to carbon discharge. 
*And the Earth will be even more green and more healthy in 2032.
*The fourth one is star-link program.
*It’ll make the whole human can be enjoying the internet everywhere. 
* I think there will be a more advanced technology that can help human being to interact with the others more swiftly and more effectively.
* Also, I believe we are doing progress on space research and Mars Landing Program. 
*And we will learn more about the origin of the universe, the destiny of the earth, and the way to live on Mars, so human can emigrate if the Earth is over-populated.
*Of all the trends listed above, what I expect the most is VR technology. 
* There are all lot of things that we never get a chance to experience because of either cost or danger, we can try them all in a small gadget without any safety or budget problem.
*We can’t foresee the world in 2032, we only can predict it.
* I believe the technology will advance, compared to 2022, and we can all enjoy the advantage that the technology gives us.
*Also, any kinds of pollution won’t bother us any more. In 2032, I’ll be in my 31. I hope the five things listed above can make my life better than now.

1.Fox News host Tucker Carlson says Biden should call a stop to the war before the nuclear conflict bursts out.--Daily Caller
2.Fox News host Tucker Carlson says Biden should call a stop to the war before the nuclear conflict bursts out.-- a video
3.Fox News host Tucker Carlson says Biden should call a stop to the war before the nuclear conflict bursts out.--grabie news
4.Tucker Carlson says "We are the closest we have been to nuclear conflict in the history,but all lot of statesmen don't want to stop this,they want Putin to get what he deserves.--Fox News
5.Tucker Carlson scolds Tony Blinken for keep upgrading the war and Victoria Nuland for the 20-year tragedy of Iraq. And they don't want to stop the war.--WND
6.Fox News host Tucker Carlson says Biden should call a stop to the war before the nuclear conflict bursts out.--red voice media

#2022-09-29#
*Send gratitude to my previous teammates, they're good.
*I got new new teammates, so cool!And I'm still group 9.
*I found that the erath photo shot by NASA is fake, so suprising!
*Finance is so important to our daily life, and we should learn more about it.
*I'm applying for my grad school recently, so stressful!

#2022-10-06#
*it's the first physical class, which is good, and it made me get more focus on the lecture.
*Economy is very important in our daily life, we should learn more about it.
*One of my teammate was on the stage to present, and i think she did it well!
*I got so many things to do recently, application, scholarship, visa...
*So happy that i'm back to Taipei to visit my family now!
1.I always believe that summer in Taiwan is unbearable for European.
	My teammate, who is from Finland, said it's not hot as she excepted!!
2.I thought that Wego high school(which is a bilinguo high school i studied) taught me nothing more than DEAD knowledge.
	After speaking English with my Finnish teammate, I found that Wego DID teach me a lot about English!
3.I watch a video says that Russia likes to rob the other's territory, and it made sense then.
	After the fact that russia want to make four provinces of Ukraine its territory, I'm mad about how Russia government mess this world up.
	
#2022-10-13#
*This weekend talks about Heallth. I recently re-realize the importance of health, so I will plan to do more sport in the future.
*Knowledge? What is knowledge? I always dislike this kind of blurred question, but got to say, it's vital.
*I've been busy working on grad school things, exchange things, OMG.
*I'll go to kaoshong to apply for the exchange things, which is so time_consuming.

#2022-10-27#
*Suiside is a serious issue, and fall is here, everone should care more about our mentality.
*I got three interview for grad school, so stressful.
*I have new teammates! So cool.
*I've book a flight ticket to Europe, and it was my first time doing this. Hope everything is fine.

#2022-11-03#
*So stressful this week.I got interview and exam to deal with.
*I have to go back to Taipei to apply visa, so bothering==.
*Two homework and two exam for the next week, my god.
*But it's good that I can go back to visit m family!

#2022-11-10#
*OMG, in course project, we accendetally select the same topic as group 3, so we have to change to C3 in the next presentation.
*Finally I have more free time this week, and the friday is school's birthday, so we take a day off.
*My computer's keyboard is broken, and I'm thinking abuut fixing it.
*I want to watch Black Panther.

#2022-11-17#
*So happy that I'm accepted to the grad school for department of material science!
*I've enroll to play basketball game, I need to exercise!
*REACHING CONSENSUS is a big topic, because we always need to cooperate with others. Although it's hard, we still have to learn to do it.
*I recently think our election is so irrational. Hope we can get rid off these bad tradition such as noisy truck, spreading rumors, etc.

#2022-11-24#
*During the class of 3 hours, i didn't think of my cellphone even once. Because if i am totally aware that it's not eith me, and if I have somrthing important, i still have my iPad.
*Finally the grad school thing is offically over. However, I'm a little nervous about the grad school life because I have no friend who is in material science with me.
*Soccer World Cup is finally here. Luckly that I'm not a gambler, or I'll lose a lot of money.
*I'm getting more and more excited about the exchange life! And I'm recently looking for where to have fun then.

#2022-12-01#
*I'm recently registering courses for my exchange program. I want to put all the classes into three days so I can enjoy longer vacation for every week.
*I'm informed that a professor from my exchange school will come to NCKU this month, and I decided to meet him?
*I deeply found the essence why professor want us to ask and answer questions. We lack thinking.
*It's less than two month for me to fly to Europe! So excited!

#2022-12-08#
*We had a interesting debate today! It's cool to listen to what others group's opinions.
*Finally, my visa the chezh has been approved! So lucky that it happoens so fast.
*My first time buying insurance, happens to buy a foreign insurance, cool.
*Finally it's getting cold, I almost feel that the earth is doomed.

#2022-12-15#
*successful and productive
*I review Phase Tranasformation for friday's class, and I ate a free lunch treated by my mentor.
*I was happy, and I sleep well.
*I practice English by attending class and chatting with classmate.

#2022-12-16#
*successful and semi-productive
*I took two online class, and I cooked lunch for myself. 
*I had a big meal for dinner, and after that, I Watch movie in the evening.
*However, due to the movie, I have little time to finish my homework.
*Need to work on my homework tomorrow.

#2022-12-17#
*successful and unproduvtive
*Someone is finally willing to rent my room! So I won't have to pay for a empty room during my exchange.
*A experiment of combustion theory took me nearly three hours, it's too tome-comsuming.
*I felt like i have less energy due to the cold weather, so I am not to dedicated too my studying.
*Gonna study more tomorrow!

#2022-12-18#
*successful and unproductive
*For my health, I walked for 45 minutes for a dinner, and I walk back.
*I want to sale my text books out, so I put them all on Facebook group.
*I watched World Cup Finale, because it happens once in four years, so I don't think it's the waste of time.
*I have to finish my combusion theory project tomorrow.

#2022-12-19#
*Successful and produvtive.
*I finished my report on combustion theory, finally.
*I ate a good hotpot for dinner, so i was in good mood.
*The weather is not so cold as desterday, so I feel more energy.
*Keep going!

#2022-12-20#
*unsuccessful and productive
*Tuesday is always my exhausting day.So many classes.
*I learnt so much today.
*But I'm not so happy foe this course shedule. Too tiring. I'm senior now!!
*I've done some preparation for city councilor, and I feel like this is so cool.
*Gonna prepare for final exam tomorrow!

#2022-12-21#
*successful and unproductive
*I finish another document for exchange. Finally. I'm done with this kind of role play game.
*I spent too much time for dinner, which left me less time to study.
*Fove tips to be productive and successful:
1. Plan your schedule beforehand.
2. Don't be too greedy. Have some time foe your recreation.
3. Tell somebody about your plan, so you will feel guilty when not completing them.
4. Give yourself some pressure. Moderate pressure can make you motivated.
5. Focus on your tasks, even when you are relaxing. That can make you stick to your schedule.


#2022-12-22#
*productive and successful.
*I submit the militirial document. Finally.
*I studied a lot today.
*It's getting cold. Hope I won't catch a cold.
*Yeah, formal outfit is getting important when I get old, because I'll face a lot of formal occasion in the future.
*Although the vacation and departure date is getting close, but I still have to deal with exams first, so I can't relieve now.

#2022-12-23#
*Successful and unproductive
*I visit the councilor today. He's so nice to talk a lot with us.
*I watched too much today. But I finish all I want to watch, so I can focus on studying tomorrow.
*Study for exam on the next week!

#2022-12-24#
*Successful and unproductive.
*My family visit Tainan for the whole day, so I show them my school, and ate dinner with them.
*Because their visiting, I read so less.
*But I accompanied them! That is what matters!
*Merry Christmas for y'all!
*Study for next, for real.

#2022-12-25#
*Successful and productive
*I bought my family their breakfast, although I have to wake up so early.
*I studied a lot for the exam.
*I have my parent take my stuff back home, so my room is more tidy now.
*I need to set alarms in order to wake up to do things.

#2022-12-26#
*productive and successful
*I studied a lot, and review the last week course.
*I was up-lifted bucause I can finally plan what to do during January.
*Prepare for other exams.

#2022-12-27#
*productive and unsuccessful
*Tuesday is always tiring.
*But I had a lot of classes today, which made me feel successful.
*Got to wake up early tomorrow.

#2022-12-28#
*unproductive and successful
*I finished the class optics, great.
*Optics costed all my daytime, which is a little waste of time.
*I interviewes a city counclior, what a cool experience!
*Seize the trivial time, and study!
*Five tips to be productive and successful:
1. Plan your schedule beforehand.
2. Don't be too greedy. Have some time foe your recreation.
3. Tell somebody about your plan, so you will feel guilty when not completing them.
4. Give yourself some pressure. Moderate pressure can make you motivated.
5. Focus on your tasks, even when you are relaxing. That can make you stick to your schedule.

#2022-12-29#
*I finally understand why professor want us to write diary every week. But I actually post some sad feeling on instegram.
*It's the last month before my exchange journey, and I'm recently buying something for it.
*It's sad that I still have to prepare for my exams on the weekend of 12/31. I wanna have fun.
*I'm worried I might have a bad grade from my professor's course, and lucky that it's not that bad.
obituary
*He is a good student, a good son, and a good friend.
*The best thing I learn from him is to enjoy your life, and always step out of your comfort zone.
*His gone is the loss of this country, amd the loss of the engineering industry.
*I hope his spirit can live among of us.
*Please don't be so sad, because due to my understanding of him, he won't be happy about that.

#2022-01-05#
*This semester is finally coming to an end! It's my last semester as a bachlor student in NCKU.
*Since I'll be in Europe for the next semester, so my period in department of mechanical engineering is over.
*I'm recently prepering for my stuff to bring to Czech, and I'm cleaning my room for the rental.
*Since I'll be back until July, I'd like to visit some place cool or try something special in Taiwan.
*Happy winter vacation y'all!