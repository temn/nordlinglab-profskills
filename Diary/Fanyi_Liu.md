This diary file is written by Fanyi Liu E14102224 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #
* It's been a long time since I attended a foreign teacher's class, and I found it very interesting.
* Hearing the wide-ranging content of this class makes me look forward to future lessons.
* However, the all-English teaching method is a bit challenging for me.

# 2024-09-19 #
* Seeing my classmates' presentations and the professor's questions and suggestions, I realized that speaking up and practicing expression is indeed the way to improve.
* I enjoy that the course includes many discussion parts, allowing me to interact with different people.
* The professor's slides contain a lot of charts, which help me grasp the content more quickly compared to just text.

# 2024-09-26 #
Carbon Market Introduction: The professor’s friend shared his work on the carbon market, which I found fascinating.
Diary Reading: A student was randomly chosen to read a diary entry, adding a personal touch to the class.
Group Presentations: Most groups agreed the world is improving, though some slides seemed to be from last semester and were not presented.
Video Viewing: We watched "How to Seek Truth in the Era of Fake News," "3 Ways to Spot a Bad Statistic," and Hans Rosling’s "The Best Stats You've Ever Seen." Mona Chalabi highlighted the importance of asking, “Is there uncertainty in the data? Do you see yourself in the data? How was the data collected?” Hans Rosling’s data visualization was particularly impressive.
Professor’s Guidance: The professor explained how to properly cite sources and identify fake news, which was practical and valuable.
Create a Diary of what would my life be in the next 10 years from now
Over the next decade, I am determined to master mechanical engineering skills with the goal of becoming an engineer at Porsche. This ambition drives my commitment to continuous learning, innovation, and excellence in the field. Alongside my career aspirations, I plan to use my free time to travel the world, discovering diverse cultures and gaining fresh perspectives that inspire creativity. Exploring new places will not only enrich my personal growth but also fuel my professional passion for designing cutting-edge technology. With dedication and curiosity, I aim to balance a fulfilling career with a life of adventure and discovery.

# 2024-10-17 #
Diary Sharing: Hearing others' 10-year aspirations was inspiring and thought-provoking.
Blue Marble Photo: I learned that the iconic Earth photo from Apollo 17 is actually a composite because they couldn't capture the whole planet in one shot.
Financial System Insights: The videos and the professor's explanation about money and credit creation were eye-opening.
Taiwan Dollar Fluctuations: It was surprising to learn how the Chinese New Year affects the circulation of currency, with people withdrawing new bills and redepositing them later.

# 2024-10-24 #
Indonesia’s Economy: A presenter highlighted the severe rise in Indonesia's debt during the 1998 crisis, which was eye-opening.
Neo-Nazi Experience: We watched a video about a man leaving America’s neo-Nazi movement, sparking a classmate to share his thoughts on whether Taiwanese resentment toward China is justified.
Professor’s Suggestion: The professor proposed that Taiwanese people make at least 10 Chinese friends to foster understanding and potentially prevent conflict.
Three Stories:
Nation: Concerns grew over Indonesia’s new leaders and mandatory militia training policies.
God: Balancing faith and effort inspired renewed motivation for a challenging task.
Marriage: A conversation about dating with marriage in mind prompted reflection on personal relationship goals.
Indonesia’s Future Vision: Former President Joko Widodo focused on economic growth through infrastructure, digital transformation, and green energy, balancing modernization with environmental responsibility.

# 2024-11-07 #
Group Discussion: We reviewed our previous homework with other groups and prepared for a presentation.
Supporting Friends with Depression: Simple ways include listening and being present, while also protecting our own mental health to avoid negative impacts.
Video Insights: Many suicide survivors regretted their decision during the attempt, highlighting the need for timely support.
Health Impacts of Depression: Depression can contribute to severe health issues, such as cancer.
Homework Reminder: I realized homework is due this week—I initially thought it was for the final project.

# 2024-11-14 #
Anxiety Survey: Most students, including me, expressed feeling anxious about an uncertain future.
Energy Power Plant in Taiwan: A student shared a story about health concerns caused by a power plant in Taichung, which stood out to me.
Maximizing Opportunities: The professor encouraged us to explore online courses, TED Talks, and edu-tech platforms to fill gaps in our knowledge and expand our networks beyond the classroom.
Depression: A group presentation discussed identifying and coping with depression, offering helpful ways to support others.
Human Nature Experiment: A TED Talk revealed how humans rationalize pre-determined choices, even if they weren’t real options.
Upcoming Documentary: We were assigned to watch a long documentary on the legal system as pre-reading for the next class.

# 2024-11-21 #
Jaron Lanier's TED Talk: Lanier argued that the internet needs a redesign, as its current structure fosters manipulation, polarization, and inequality.
"Weapons of Math Destruction": Cathy O'Neil discussed how biased algorithms can cause harm in areas like education and employment, advocating for transparency, accountability, and fairness.
Personal Reflection: I realized the importance of using the internet mindfully, focusing on authentic interactions and protecting my attention from manipulative systems to build a thoughtful future.
Facebook's Revenue Model: I was surprised to learn that Facebook's profit comes primarily from targeted advertising, using vast amounts of user data to offer precise targeting. While effective, this raises ethical concerns about privacy and data security, as seen in political election scandals.
Reaching Consensus Process:
Idea Collection: Participants brainstormed enforceable rules, ensuring all perspectives were included.
Categorization: Suggestions were grouped into themes like freedom of speech, privacy, and addiction prevention.
Discussion: Pros and cons of each rule were debated, focusing on practicality, enforceability, and fairness.
Voting: Rules were voted on to ensure majority support and inclusivity.
Refinement: Compromises merged similar ideas and adjusted contentious rules to align with diverse views.
Finalization: The refined list was reviewed and finalized to meet the group’s objectives.

# 2024-11-28 #
The professor started the class with a mini-experiment titled "Phone Addiction":
Mute your phone.
Place it on the table in front of the class.
Count how many times you reach for your phone during the session.
Retrieve it after three hours, at the end of the class.

# 2024-12-05 #
This week’s topic, *Planetary Boundaries*, delved into the limits of Earth's ecosystems and the impact of human activities. The session began with a group discussion, where three teams analyzed news articles to assess their perspectives. We explored a visual representation of planetary boundaries and learned that by 2023, six critical thresholds had been exceeded: novel entities, climate change, biosphere integrity, land-system change, freshwater use, and biogeochemical flows. The session concluded with preparations for our final projects.

# 2024-12-12 #
Jeremy Rifkin's documentary, The Third Industrial Revolution: A Radical New Sharing Economy, explores critical challenges in the global economy, including resource depletion, declining productivity, and growing inequality. Rifkin advocates for a new economic model driven by digital technology, renewable energy, and the sharing economy to build a sustainable and equitable future.
The documentary highlights how technological innovations can reshape economic systems, with a focus on collaboration and creativity. For a deeper insight into his transformative vision, the documentary is a must-watch.

# 2024-12-19 #
