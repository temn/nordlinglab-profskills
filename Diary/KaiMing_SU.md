
# 2024-09-12 #
* The first lesson is quiet attractive for me because its extensive topics
* This lesson is a little bit challengeful for me due to its all-english teaching

# 2024-09-17 #
* The theorem of conflict is useful which gives me a different view, helping me face cinflict next time
* Today, I learn about what is GIT 
* Some classmates' presentations offer me diverse kinds of exponential growth

# 2024-09-26#

In 10 years, I expect that I will live in a bustling city such as Taipei, Kaohsiung or even Tokyo. These cities are known for their advanced infrastructure and continuous development. These characters are ideal for a civil engineer. I will be working for a leading engineering firm, specializing in sustainable urban development. My role will involve designing and managing projects that focus on creating eco-friendly and resilient infrastructure to address the challenges posed by climate change and urbanization.

* Sustainable Development and Green Building: The push towards sustainability is one of the most significant trends in civil engineering. Governments and organizations worldwide are increasingly prioritizing green building practices to reduce carbon footprints and promote environmental stewardship. This trend will not only shape the projects I work on but also influence the skills and knowledge I need to stay relevant in the industry.

* Smart Cities and IoT Integration: The rise of smart cities, powered by the Internet of Things (IoT), is transforming urban living. Smart infrastructure, such as intelligent transportation systems, smart grids, will become standard. This trend will require me to collaborate closely with IT professionals and continuously update my technical skills to keep pace with technological advancements.

* Resilience and Disaster Management: With the increasing frequency of natural disasters due to climate change, there is a growing emphasis on building resilient infrastructure. My work will involve designing structures that can withstand extreme weather events and developing disaster management plans to ensure the safety and continuity of urban areas. This trend will make my role crucial in safeguarding communities and minimizing the impact of disasters on infrastructure.

* Urbanization and Population Growth: Rapid urbanization and population growth are driving the need for expanded and improved infrastructure. Cities are becoming more crowded, and the demand for housing, transportation, and public services is rising. In response, I will be working on large-scale urban development projects that aim to accommodate this growth while maintaining a high quality of life for residents. This trend will challenge me to find innovative solutions to optimize space and resources in densely populated areas.

* Advancements in Construction Technology: The construction industry is experiencing a technological revolution with the invention of 3D printing, and robotics. These technologies are improving the efficiency, accuracy, and safety of construction processes. Staying updated with the latest technological trends will be essential for my professional growth and success.

# 2024-10-17#
* I am shocked due to the random presentation at the first.
* Today's lesson is fresh for me because teacher is talking about some concept of economy, which I didn't think before.
* I understand the financial system deeply after today's lesson.

# 2024-10-24#
* At the beginning of the class，teacher let us think about what knowledge is. I straightly think that knowledge is the content from the test I read. But, after thinking for a while, I realize that it is not that simple. I think knowledge is something which affects me form the every decision I make to the way I face the difficulty.

* The first story is The Lion King. Every time when I encouter problem and I feel depressed, I always think hoe Simba grow after his father died. This story always gives me courage to stand up and face the problem.
* The second story is The Ugly Duckling. This fairy tale always reminds me that "don't judge a book by its cover". I think a person's or item's value is far more important than its look.
* Tha last story is Pinocchio.  This story tells me how importance of being honest. 

* My three rules for how to live healthy are get quality sleep, think postive, and have a regular life.
* Get quality sleep: Research shows that chronic sleep deprivation is linked to various health issues, including obesity, heart disease, and diabetes.
* Think postive: Mental health is key part of a person's health.
* Have A regular life: A regular life creates stability and predictability in daily routines, which helps regulate essential bodily functions.

# 2024-11-07#
* Last week was the third typhoon day-off this semester. This is the first time I feel too many day-off.
* Today's course is about depression and the cases of some people who tried to commit suicide. The video about the police officer who deals with the cuicided cases on the Golden Gate Bridge let me think a lot.
* If I have a friend who has depression or trys to suicide, I think the first thing is to accompany him or her and to have a talk.
* The second thing is to help him or her seek a professional therapist who is much more exprienced.
* The last thing I think the most important thing is to invite him or her to join my life and show how beautiful the world is.

# 2024-11-14#
* The debating between two classmates at the first of class is amazing. And the topic they discussed is close to our daily life. I think nuclear power is much more eco-friendly than thermal power generation, but how to store the waste of nuclear is hard for the technology now we have.

# 2024-11-21#
* The video talks about the Internet we have now and some companies are controling what users see on the Internet. The technology lets us get the information much more easily, but we also are controled by the contents more easiliy.

# 2024-11-28#
* Today teacher leads us to think about the truth of news. One of the video talks about which information is editted by someone who has certain faction.
* And the video says that even the news during a natural disaster or war has many versions, so it is hard for us to identify whether the news is true.
* Today's presentation form the classmates is interesting.
* I think the alarm on hand is quite useful for me because with it, I don't need to find where my phone or alarm is when I just wakw up.

# 2024-12-05#
* In Johan Rockström's TED Talk, "Let the Environment Guide Our Development," he emphasizes the importance of planetary boundaries to ensure sustainable development. Rockström advocates for balancing human progress with ecological limits and calls for bold actions from governments, businesses, and individuals to preserve the planet for future generations.
* In Kate Raworth's TED Talk, "A Healthy Economy Should Be Designed to Thrive, Not Grow," she advocates for Doughnut Economics, aiming for an economy that meets everyone's needs within planetary limits, promoting sustainability and equitable distribution.
* In today's class, some classmates share their researches of some countries' freedom of news points.

# 2024-12-12#
* Today, we had a debate for each other's topic, which is hard for me to use English to explain what I think and our topic about.
* Althogh it is diffcult time for me, I learned a lot during the class.

# 2024-12-19#
* Five Rules for Success and Productivity
* Plan Your Day in Advance: Create a detailed schedule with priorities clearly outlined to focus on what matters most.
* Eliminate Distractions: Turn off notifications and use tools like website blockers to maintain focus during work hours.
* Break Tasks into Smaller Steps: Divide large tasks into manageable pieces to reduce overwhelm and track progress easily.
* Balance Group and Solo Work: Leverage group discussions for insights but dedicate time for individual implementation.
* Reflect and Adjust Daily: Spend 5 minutes at the end of the day evaluating successes and challenges to improve continuously.

# 2024-12-20#
* A. Successful and Productive
* B. I felt successful because I completed all my planned tasks, including reviewing course notes and working on an assignment. I felt productive because I stayed focused, managed my time well, and avoided distractions.
* C. Tomorrow, I will start my day by prioritizing the most important tasks first to maintain momentum throughout the day.

# 2024-12-21#
* A. Unsuccessful and Unproductive
* B. I felt unsuccessful because I didn’t manage my time well and missed a key class discussion. I felt unproductive because I didn’t accomplish much of what I had planned and found myself distracted for most of the day.
* C. Tomorrow, I will limit distractions by turning off notifications and setting clear, time-blocked study periods

# 2024-12-22#
* A. Successful but Unproductive
* B. I felt successful because I participated in class and contributed to a group project. However, I felt unproductive because I didn’t make progress on my individual assignments and procrastinated a lot.
* C. Tomorrow, I will focus more on individual tasks before group work, using a timer to keep track of time spent on each task.
# 2024-12-23#
* A. Successful and Productive
* B. I felt successful because I finished a major portion of my project and received positive feedback from my professor. I felt productive because I stayed on task and completed the majority of my assignments ahead of schedule.
* C. Tomorrow, I will review my work before submitting to ensure there are no overlooked mistakes.

# 2024-12-24#
* A. Unsuccessful and Unproductive
* B. I felt unsuccessful because I didn’t participate much in the class and felt disengaged from the material. I felt unproductive because I didn’t make any significant progress on my assignments or preparation for exams.
* C. Tomorrow, I will review the course material in advance to ensure I’m prepared and actively engage in class.

# 2024-12-25#
* A. Successful but Unproductive
* B. I felt successful because I had a productive meeting with my study group, and we accomplished some key goals. However, I felt unproductive because I didn’t spend enough time on my personal study tasks and let others dominate the meeting.
* C. Tomorrow, I will set personal goals for group meetings and ensure I spend time independently working on my tasks.

# 2024-12-26#
* A. Unsuccessful but Productive
* B. I organized a lot of information, but kept getting distracted by irrelevant tasks, leaving the most important work unfinished.
* C. I will set clear priorities to ensure I tackle the most important tasks first and avoid distractions.

# 2024-12-27#
* A. Successful and Productive
* B. I followed my plan, completed all tasks, and even took time to learn new skills.
* C. I will continue maintaining this high level of efficiency by setting specific learning goals every morning.

# 2024-12-28#
* A. Unsuccessful and Unproductive
* B. I couldn't focus at all, kept thinking about unrelated things, and as a result, my work progress was almost stagnant.
* C. I will try meditation or relaxation techniques to improve my focus and set short-term goals to stay motivated.

# 2024-12-29#
* A. Successful but Unproductive
* B. I completed most of my tasks on time, but I spent too much time using my cellphine.
* C. I will set time limits to avoid getting bogged down in details and focus on finishing core tasks efficiently.

# 2024-12-30#
* A. Unsuccessful but Productive
* B. I made many plans but didn't execute them because I was distracted by smaller tasks.
* C. I will organize my to-do list the night before and allocate time for each task to prevent chaos.

# 2024-12-31#
* A. Successful but Unproductive
* B. I celebrated the new year with my friends, but I didn't do any tasks.
* C. I will catch up on my progress and make sure to complete the tasks I missed.

# 2025-01-02#
* A. Unsuccessful and Unproductive
* B. I got many small tasks done, but neglected the key tasks, which left me feeling unsatisfied.
* C. I will change my work strategy by tackling the most challenging tasks first and ensuring enough time is dedicated to complete them.

# 2025-01-02#
* Kai Ming, a dedicated and inspiring individual., they transformed small, practical goals into meaningful accomplishments. Whether through empowering others or contributing to their community, they left a legacy of perseverance and impact. Wu Pao-chun, a world-renowned baker, achieved global recognition through his dedication and innovation, while Andre Chiang, a Michelin-starred chef, elevated culinary arts with creativity and respect for tradition. Their focus, persistence, and ability to inspire are values I strive to emulate.

# 2025-01-03#
* Preparing answers for the exam questions was the most challenging part. Some questions' answer is hard to  find, making it tough to feel fully confident. Despite this, I focused on organizing my thoughts and prioritizing key points, hoping my efforts will pay off in the results.


1. Know the current world demographics
* Suggestion: Use websites like Gapminder to look at charts and graphs about population. Practice explaining what the data shows about people in different countries.

2. Ability to use demographics to explain engineering needs
* Suggestion: Look at examples where engineers built things based on population needs, like schools or roads. Try creating your own idea for a project, like designing a park for a busy city.

3. Understand planetary boundaries and the current state
* Suggestion: Learn about limits like how much pollution the Earth can handle. Watch videos or do a small project to see what happens if these limits are crossed.

4. Understand how the current economic system fails to distribute resources
* Suggestion: Look at examples where some people have too much and others have too little. Write a simple plan or story about how resources could be shared more fairly.

5. Be familiar with future visions and their implications (e.g., AI)
* Suggestion: Read about things like robots and artificial intelligence (AI). Think about how these could help or change the way we live. Share your ideas in a group discussion.

6. Understand how data and engineering enable a healthier life
* Suggestion: Use health apps or devices like a fitness tracker to collect data about your steps or sleep. Think about how engineers make these tools and how they help people.

7. Know that social relationships increase survival likelihood by 50%
* Suggestion: Spend time with friends or join a group activity. Reflect on how being around people makes you feel healthier and happier.

8. Be familiar with depression and mental health issues
* Suggestion: Watch videos or attend a talk about mental health. Learn simple ways to handle stress, like taking deep breaths or talking to someone you trust.

9. Know the optimal algorithm for finding a partner for life
* Suggestion: Learn about decision-making tips, like not rushing into a choice too early. Think about what is most important to you in a life partner.

10. Develop a custom of questioning claims to avoid "fake news"
* Suggestion: Before believing a story, check if it’s true by searching online or asking someone you trust. Practice spotting news that doesn’t seem right.

11. Be able to do basic analysis and interpretation of time-series data
* Suggestion: Use simple tools like Excel or Google Sheets to track and look at data over time, like daily temperatures or savings. Try making a chart to see patterns.

12. Experience collaborative and problem-based learning
* Suggestion: Work with classmates on a project where everyone solves a part of a problem. Practice sharing ideas and listening to others.

13. Understand that professional success depends on social skills
* Suggestion: Practice talking and working with others, like in group activities or mock interviews. Focus on being polite and clear.

14. Know that the culture of the workplace affects performance
* Suggestion: Learn about different workplaces. Think about what makes people happy and work well, like clear rules, teamwork, or fair bosses.







