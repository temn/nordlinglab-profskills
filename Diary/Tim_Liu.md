This diary file is written by Tim Liu E14096106 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #
* This course taught me to see things in a different light
* I am confused about what conflicts means in the questionnaire
* I feel novel about this course because I've never taken classes like this

# 2022-09-15 #
* 1.Today's class is great.
* 2.They did the presentation well. 
* 3.It's interesting to laern how to use professional platforms.
# 2022-09-22 #
* 	I was born in Taipei. I grew up and studied in there. Two years ago, I came to Tainan for university. The first thing that surprised me is the low price. Unlike Taipei, the money I spend on meals in Tainan is about 30% lower than in Taipei. Additionally, the low house price here is also beyond my expectation. The house price here is almost twice higher than Taipei’s. Therefore, there is a high probability that I reside in Tainan and work here.
* 	Since I major in Mechanical Engineering, it is very likely that I become an engineer. However, we are living in an era of rapid development. More and more innovative technologies come up. It is crucial that we keep up with the progress. In my opinion, the top five current trends of change can be listed as the followings.

* 1. Big data

* 2. Artificial Intelligence

* 3. Robots

* 4. Online shopping

* 5. The changes of our lifestyles due to Covid-19

* 	First of all, since big data came up, many things can be predicted and counted. Such as the way people buy things, what kinds of clothes sale more, and what kinds of videos or movies are the most popular. Big data is basically statistics, it works by collecting lots of data and turn the results in numbers so that it can be read easily.	Secondly, Artificial Intelligence grows up rapidly these days. The most famous example is AlphaGo. By collecting over one hundred million games, it soon enhanced its skill in playing chess in a short amount of time. Few years later, AlphaGo even defeated the top human player. This result indicates that artificial intelligence has infinite possibilities. It can learn a lot faster than human can and thus come up with ideas that we never think of. Thirdly, robots now can do many precise works. One of the examples is da Vinci Surgical System. It can perform surgery in an extremely precise way that can not be done by human hands. Making very small wounds compared to the traditional ways. Fourthly, since surfing the internet become so convenient nowadays, the way people buy things change a lot. More and more online shopping platforms come up. Almost everything can be bought online. This causes many hypermarkets lose money. I think few years later, the amounts of hypermarkets will become less. Lastly, it has been for about 2 years since the outbroke of Covid-19. Our lifestyles change a lot. One of the most obvious changes is that we need to put on masks when going outside. Sadly, the virus pandemic is not going to stop. Many new types of viruses came up. We need to get vaccinated more often to protect ourselves. In my opinion, the virus won’t disappear even after 10 years. It will just become one part of our lives.

* The article is "Heathrow Airport emergency after 2 planes collide into each other on the ground ".
* Each article offered very simular information.
* Some article mentioned how the planes collided
* Some mention about the casualties
# 2022-09-29 #
* Todays lecture is very informative and interesting.
* It's interesiting to understand how banks work.
* The task is challenging.
# 20222-10-06 #
* Today is the first physicak class.
* It's interesting to hear other students' presentation.
* I have learnt a lot about other countries' economic.
# 2022-10-13 #
* sleep at least 7~8 hours a day is important for health.
* I learnt about proving knowledge.
* Eating vegetables is good for body.
* Knowledge is well-justified true belief.
# 2022-10-27 #
* Today's topic is very depressive.
* We don't need to feel having a gap with the people who are in depression.
* The lecture let me know how to help with someone suffering depression.
# 2022-11-03 #
* I think it important to think optimistic.
* EXpressing feelings to friends is relaxing.
* Playing badminton with friends is a good way for me to release stress.

# 2022-11-10 #
* We saw many presentations today. Some are very interesting.
* It war pretty cold this morning, but turned to be hot in noon.
* Today's class taught me how to stay healthy
# 2022-11-17 #
* Today's class is very interesting.
* Presentation is great in the class.
* I learnt the importance of knowing laws about environment.
# 20222-11-24 #
* During this 3 hr, I thought of my cellphone only few times.
* I think the experment (take away cellphone) today is interesting.
* I didn't pay too much attention to today's presentation.
# 2022-12-01 #
* There are so many exams next week. I don't know how survive next week.
* I lost some money buying lottery tickets.
* I can't believe that Spain lost to Morocco in World Cup
# 2022-12-08 #
* Tainan is getting colder. Especially in the morning.
* We got a new group this week.
* It's almost the end of this semester, there are lots of exams. Hope I can pass all the subjects.
# 2022-12-22(Thu) #
* Every group need to give a live presentation this week. Unlike video presentations, I think live presentation is the better way.
* (A) I think today is a successful and an unproductive day. 
* (B) I had a final exam this morning. It's very hard but I still did it a lot better than in the first exam. So I think today is a successful day.
* We need to wear a formal dress this week, but I forgot to do that. I felt sorry about that.
* I have 2 fluid mechanic test and 1 final exam next week. I should start studyiing for the tests next week.
* However, I felt so tired after I finished electronic final exam this morning. So I just play computer games after this class. That's why I think today is an unproductive day.
* (C) To make sure that I study for tests next day, I made a list to remind me what I should finish on friday.
# 2022-12-23(Fri) #
* (A) I think today is a successful and productive day.
* (B) There is a fluid mechanics test this morning at 10A.M. I woke up at 7 to prepare for the test. Thankfully, the quiz is quit easy. I think I did pretty well on it.
* Unfortunately, There are still 2 fluid mechanics tests next week. So I started studying for the tests in the afternoon.
* Since I skipped for some classes, there are lots of things I need to study. To my surprise, I finished the schedule I made. So I think today is a successful and productive day.
* (C) I don't think that I need to change anything.
# 2022-12-24(Sat) #
* (A) I think today is not a successful and unproductive day.
* (B) I woke up at 11.30 A.M this morning. I should wake up earlier because I had 2 final exams next monday. What's worse, I didn't finish the schedule I made.
* I only finished about 50% on my schedule. That means I have to study more on sunday. I didn't perform well on the second exam of Mechanic Design.
* So I have to get a higher score this time. Hope I can complete ths tasks next day. 
* That is why I think today is an unccessful and unproductive day.
* (C) I don't think that I need to change anything.
# 202-12-25(Sun) #
* (A) I think today is a successful and porductive day.
* (B) Tomorrow I have 2 final exams. Alough today is Christmas,I stayed at home the whole day to prepare for the final exam.
* I guess that most of the students in ME have the same situation(studying on Christmas). It's my third Chirstmas in NCKU, and I still don't have a girl friend.
* I tink I'm well prepared for the exams tomorrow. Hope I can perform well tomorrow. That's why I think today is a successful and productive day.
* (C) I don't think that I need to change anything.
# 2022-12-26(Mon) #
* (A) I think today is an unsuccessful but produvctive day.
* (B) I finished 2 final exams this week. However, I didn't perform well on Mechanic Design exam. That's why I think today is an unsuccessful but produvctive day.
* Fortunately, there's another final exam(Composite material) which is very easy. I got a 90 on the exam. Hope I can still pass this class.
* Alough I just finished 2 exams today, there are still 2 fliud mechanic tests on wednesday and friday. I have to start studying right away.
* (C) I don't think that I need to change anything.
# 2022-12-27(Tue) #
* (A) I think today is a successful and produvctive day.
* (B) There's a fluid mechanics test tomorrow. I think fliud mechanics is probably one of the most difficult object in this semester.
* I studied for it until 4A.M next day. I think I'd better go to bed now, or I may not get up on time. Hope I can do well on the test.
* That's why I think today is a successful and produvctive day.
* (C) I don't think that I need to change anything.
# 2022-12-28(Wed) #
* (A) I think today is a successful but unproduvctive day.
* (B) I have a fluid test today. Thankfully, my effort yesterday is worthy. The test is extremely easy, almost everyone got a good score.
*　Ｉ got a perfect score on the test. However, I didn't study for the test on Firday after I went home. That's why I think today is a successful but unproduvctive day.
* (C) I don't think that I need to change anything.
# 2023-01-05 #
* This semester's class has finally ccame to the end.
* Through the whole course, I have learnt a lot of useful things, such as global warming, suicide issue, environment issue, and trash recycling.
* Unlike other courses in Mechnical Engineering, this class taught me more about thinking. There are many social issues that won't be mentioned in class. However, they're stll meaningful and worth thinking.
* Thank's for everyone in this course, it's the best course in this semester.