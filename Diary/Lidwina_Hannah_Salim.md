This diary file is written by Lidwina Hannah Salim H44105319 in the course Professional Skills for Engineering the Third Industrial Revolution.

# 2024-09-12 

* In the first class, we learned about the professor's background, the course structure, and the topics we'll cover in the coming weeks.
* We also had a chance to introduce ourselves to the people sitting nearby and had a brief discussion with them.
* There was a quiz on Google Forms that we needed to complete, and the professor mentioned we’ll take it again for the final exam.
* Overall, I really enjoyed the class because it was interactive and had plenty of discussions.

# 2024-09-19 

* The professor introduced the concepts of conflict views and conflict behavior, followed by a brief discussion with our classmates.
* We also learned how to use Git repositories and were guided on how to write our weekly diary entries.
* Some groups were randomly chosen to present their works, and the professor gave constructive feedback to help everyone improve for future presentations.
* After the presentations, the professor discussed disruptive innovation and exponential growth.
* We watched a video featuring Steven Pinker discussing whether the world is getting better or worse.

# 2024-09-26

* The professor's friend came to class and introduced his work on the carbon market, which I found interesting.
* The class started with a student randomly picked to read a diary.
* Groups presented their work, and all of them thought the world is getting better.
* Some of the PowerPoints seemed to be from last semester since no one came up to present them.
* We watched three videos: "How to Seek Truth in the Era of Fake News," "3 Ways to Spot a Bad Statistic," and "The Best Stats You've Ever Seen" by Hans Rosling.
* In her video, Mona Chalabi stresses the need to ask these key questions about statistics: "Is there uncertainty in the data? Do you see yourself in the data? How was the data collected?"
* I found the last video by Hans Rosling really impressive, especially how he visualized the data.
* The professor explained how to properly cite sources and differentiate fake news.

Ten years from now, I envision myself living in a different country, provided I get the opportunity to continue my career abroad after completing my master’s degree. I haven’t decided on a specific country yet, but I’m open to moving where the best opportunities arise. However, I can also see a strong possibility of returning to my home country, Indonesia, by then. This decision would largely depend on career growth, personal circumstances, and how well I’ve adapted to life abroad.

As for my career, I’m currently in a phase of exploration, discovering what I’m truly passionate about. By the time a decade has passed, I hope to have settled into a job I deeply enjoy, one that aligns with my values and provides a sense of fulfillment. Achieving a healthy work-life balance is also a key priority for me, as I aim for a career that allows time for personal development, hobbies, and social connections outside of work. While I’m currently more interested in the financial industry, I remain open to finding new passions and exploring other fields that may capture my attention along the way. I believe that by staying curious and adaptable, I will find a career path that resonates with me.

There are several key trends of change today that I believe will have a significant impact on my life over the next decade:

* Digital Transformation: Technology continues to evolve rapidly, influencing almost every industry, and the finance sector is no exception. More financial services are moving online, and to remain relevant, I will need to develop strong technical skills, such as data analysis, artificial intelligence, and automation. This trend might also offer more opportunities for remote work, giving me greater flexibility in where I choose to live.
* Sustainability and Climate Change: As businesses become more environmentally responsible, industries are shifting toward sustainability. In finance, green investments and ethical banking are becoming more prominent. If I remain in finance, this trend may shape the types of projects I work on, potentially focusing on initiatives that promote sustainability and positive global impact.
* Changing Nature of Work: Remote and hybrid working models are increasingly common, allowing for a more flexible approach to work. While this could improve my work-life balance, it may also blur the lines between my professional and personal life, requiring me to set clear boundaries to maintain a healthy balance.
* Growth of the Gig Economy: The rise of freelance and gig work is creating more opportunities for flexible, project-based roles. This trend could allow me to explore multiple interests and career paths, offering variety and autonomy, though it may also demand greater self-discipline and time management skills.
* Globalization: With companies expanding across borders, international collaboration is becoming the norm. I expect to work with diverse teams from around the world, which could influence both where I live and the skills I develop. Working in global teams will require cultural awareness and adaptability, regardless of whether I remain in finance or switch to another field.

These trends will shape my career and lifestyle over the next ten years, presenting both challenges and opportunities. By staying open to change and adapting to new developments, I hope to create a future that balances personal fulfillment with professional success.

News Title: Pope Francis suggests Israel's actions in Gaza and Lebanon are disproportionate and immoral

* abc News: Pope Francis is suggesting that Israel has used an “immoral” and disproportionate response in its attacks in Gaza and Lebanon
* My Mother Lode: Pope Francis suggesting that Israel's military domination has gone beyond the rules of war
* The World Without Fake News: Pope Francis critiques excessive force in conflicts, emphasizing proportionality in warfare

# 2024-10-3

* There is no class today because of a typhoon holiday.
* Fortunately, Tainan isn't impacted much by the typhoon.

# 2024-10-10

* There is no class again today because it is the Independence Day of Taiwan.

# 2024-10-17

* It’s nice to hear what other people write in their diaries about what they want to be in the next 10 years.
* I just learned that the "Blue Marble" photo by Apollo 17 is actually a composite because they couldn't get far enough to take a single photo of the entire Earth.
* The videos about the financial system and the professor's further explanation about money and the financial system were really insightful. 
* One of the key lessons was learning about credit creation.
* I also didn’t expect that the fluctuation of the Taiwan Dollar held by the public is influenced by the Chinese New Year. Every year, people withdraw new money and then put it back in the bank afterward.

# 2024-10-24

* One presenter talked about Indonesia’s economic condition, and I just realized how severe the country’s debt rise was during the 1998 crisis.
* We watched a video featuring a man sharing his experience with America’s neo-Nazi movement and how he managed to leave it behind.
* Someone in our class shared his own experience after watching the video and asked if Taiwanese people should really harbor so much resentment toward the Chinese.
* Our professor suggested that one solution to prevent China from attacking Taiwan is for every Taiwanese person to make at least 10 friends in China, so if a conflict arises, the people there might oppose it.

Three fictional stories:

* Nation: I had been hearing stories about the backgrounds of Indonesia’s new president and vice president even before the election, which made me worry about the country’s future. This week, I saw news about plans for mandatory militia training, which only added to my concerns about whether these policies are truly necessary.
* God: A few days ago, I was reminded that while I believe God has a plan, I still need to give my best effort. This belief motivated me to work harder on a challenging task, feeling that both faith and action are essential.
* Marriage: Talking to a friend who dates with marriage in mind made me rethink my own approach to relationships. It got me wondering if I should also view dating as a path toward marriage.

Future offered by politician in my country

* In Indonesia, former President Joko Widodo envisioned a future of economic growth fueled by infrastructure, digital transformation, and green energy. His vision included large-scale infrastructure projects, the development of a new capital, Nusantara, and attracting foreign investment to drive industrial growth and job creation. He also emphasized a shift toward sustainable energy and reducing carbon emissions, aiming to position Indonesia as a global leader in green development. This vision sought to balance economic progress with environmental responsibility, modernizing Indonesia’s economy for the digital age.

# 2024-10-31

* There is no class again today because typhoon holiday

# 2024-11-7

* We had a group discussion with other groups about our previous homework and prepared for a presentation.
* Simple ways to support friends who are experiencing depression include listening to them and being there for them.
* It’s important to pay attention to our own mental health when listening to their stories so that it doesn’t impact us negatively.
* From the video we watched in class, many people who attempted suicide regretted their decision by the time they made the attempt.
* Depression can lead to various health issues, for example cancer.
* At the end of class, I realized there’s homework due this week, I thought it was for our final project.

# 2024-11-14

* The anxiety survey results show that students this semester have higher levels of anxiety compared to previous semesters.
* Some groups shared guides on helping a depressed friend, highlighting the importance of listening and being there for them.
* We also learned key tips for onboarding experiences, such as the importance of non-work relationships, collaboration, and going beyond "the call of duty."
* Lastly, we discussed with other students rules for a happy and fulfilling life, like being grateful and doing what you love.

# 2024-11-21

* We watched Jaron Lanier's TED Talk about how we need to remake the internet, and from that video, I realized that the exploitation of user data by companies like Google and Facebook has created significant societal problems.
* I learned that current digital business models manipulate users and contribute to issues like misinformation and polarization.
* I also learned the importance of creating an internet where users are fairly compensated for their data and have greater control over their digital lives.
* The professor also explained that Facebook's main revenue and profit come from targeted advertising, highlighting the central role of user data in its business model.

# Consesus Rules
* Safeguard Freedom of Speech: Allow individuals to express their opinions freely while respecting boundaries of hate speech or violence.
* Ensure Privacy: Protect user data with strict data encryption and limit its access to unauthorized parties.
* Guarantee Equal Access: Provide affordable and universal internet access to bridge the digital divide.
* Enable Easy Sharing: Facilitate sharing of information and ideas while maintaining respect for intellectual property.
* Fact-Check for Fake News Prevention: Mandate transparent fact-checking and clear labeling of verified versus unverified content.
* Mitigate Manipulation and Brainwashing: RestrWe discussed the lifecycle of civilizations and the lengthy process of recovering from a dark age.ict the use of algorithms that promote extremist or misleading content to maximize engagement.
* Combat Addiction: Implement features like time-use reminders and parental controls to minimize overuse and addictive patterns.

# Reaching Consesus Process
* Idea Collection: Each participant shared their own set of enforceable rules during a brainstorming session, ensuring everyone's perspective was heard.
* Categorization: Suggestions were grouped into broader themes such as freedom of speech, privacy, and addiction prevention.
* Discussion: Participants debated the pros and cons of each proposed rule, focusing on practicality, enforceability, and alignment with the goal of maintaining a free yet fair internet.
* Voting: Participants voted on the rules to determine which had majority support, ensuring inclusivity and fairness.
* Refinement: Compromises were made where disagreements arose, merging similar ideas and adjusting contentious rules to suit all perspectives.
* Finalization: The group reviewed the refined list to confirm it met the objectives and finalized the consensus rules.

# 2024-11-28

* Some groups shared their action ideas for the final project, and after each presentation, we voted on their proposals.
* We discussed the lifecycle of civilizations and the process of recovering from a dark age.
* We also watched a YouTube video about the riots in Amsterdam involving Maccabi supporters, It revealed how media narratives often leave out crucial facts.
* At the end of class, we were allowed to pick up our phones, and the professor asked how many times we were looking for the phone during the session.

# 2024-12-05

* Some groups presented their work about news from different countries and how the sentiment of each news correlates with the global press freedom score of each country.
* We had a group discussion to analyze the perspectives of the news and who benefits from them.
* We moved on to a new topic: "planetary boundaries." It was my first time learning about this, and it’s concerning that six out of nine boundaries have already been crossed.
* The boundary that has been crossed the most is "novel entities," which refers to human-made substances like plastics, chemicals, and pollutants that disrupt ecosystems.
* We watched a video by Johan Rockström titled “Human Growth Has Strained the Earth's Resources,” where he discussed crossing tipping points and the Earth's limited buffering capacity.
* We also watched "A Healthy Economy Should Be Designed to Thrive, Not Grow" by Kate Raworth, where she emphasized the need for an economic system that focuses on thriving within ecological limits rather than pursuing endless growth.

# 2024-12-12

* Each group presented their action plan, including the Work Breakdown Structure (WBS), PERT chart, and GANTT chart, while explaining how their plan impacts one of the planetary boundaries.
* A representative from each group briefly explains their action plan to the class, followed by a Q&A session to gather input, discuss, and provide suggestions.
* After the presentations and discussions, the class votes for the most compelling action plan.
* The Q&A and debate sessions are valuable opportunities to learn from other groups, improve our plans, and contribute constructive feedback.
* We also watched the documentary "The Third Industrial Revolution: A Radical New Sharing Economy" by Jeremy Rifkin, where they explores solutions to global economic crises, emphasizing a shift toward new economic systems that prioritize sustainability and equality.

# 2024-12-19
* we had a debate in class between three different groups which we can chose freely which one we wanted to join (capitalist, workers, pr environmentalist)
* The main question revolves around inequality, and we heard answers from different groups, but for me personally it's because there is a group that has more power or considered to has more power (like in gender inequality)
* after that, we started to learned new topic which is "how to succeed" which I think it is a really important thing to learn for our self-development 
* we learned about "7 habits of highly effective people" by Steven Covey, the time-leadership matrix, radical delegation framework, and lastly "ikigai"
* I heard of "ikigai" before and I really like the concept of it because since few months ago my goal is to find my real passion and find a job that makes me feel like i am just playing, which is the intersection between what I live, what I am good at, what can be paid for, and what the world needs.
* Besides that, we also learned about four innovation cultures and Radical Candor by Kim Scott, which emphasizes the importance of criticism and praise to improvement
* another important takeaways from this class session is "pick a boss, dont pick a job" because boss is the biggest factor in our career success

5 Rules to Succeed and More Productive 
* Manage time wisely
* Increase your focus
* Be confident 
* Consistency 
* Avoid distractions 

# 2024-12-20
* unproductive/successful 
* I do all the things planned today which js attending class in the morning and then taking graduation photos with my friend from Kaoshiung and dinner together, and then I attended my French friend's birthday party, so the day actually felt fun and packed and, but I was trying to do something between my spare time but ended up doing it inefficiently 
* I need to practice "time blocking" and increase my focus so I can use my spare time more efficiently 

# 2024-12-21
* unproductive/successful
* my sister and her friend come from Taipei and it was super fun, we go to many places in Tainan and ate a lots of things, but because of it I don't have time to do anything productive 
* next time I need to control myself to divide the time between having fun and do the things I need to do 

# 2024-12-22
* unproductive/unsuccessful
* I dyed my hair for the first time today and it takes more than 6 hours and I did nothing during those hours, so I felt really unproductive even though I do some things after that but it just felt the day is unsuccessful because too many hours wasted
* I should be able to use my time more wisely

# 2024-12-23
* productive/successful
* I wake up early even though my class is on the afternoon because there are too many things to do, so I do all the things I need to do even it's not finished yet, and after that I had last dinner with my roommate because I am going to France soon
* I need to keep this up, but need to increase my pace

# 2024-12-24
* productive/successful 
* I had final exam at 7 am which is a sudden request by my teacher but there's nothing much I can do about the schedule, after that I was busy packing up all my things and check out from the dorm and heading away directly to Taipei and have christmas eve's dinner with my sister (it's a shame that I arrived in Taipei later that we though so there is not much option to choose)
* next time I need to do things earlier to spare some times for unexpected things

# 2024-12-25
* productive/unsuccessful
* I am busy packing up and preparing things for my departure and after my arrival all night that I lack of sleep and I am in rush even before my flight, so its feel a bit unsuccessful 
* I should be able to manage my time more wisely and do things earlier so I don't need to be in that rush

# 2024-12-26
* unproductive/successful
* I can't attend the class today because I am not in Taiwan anymore but I asked my friend about what they are talking about in the class
* I heard that our group got some feedback for the final project
* I spend most of the day in the flight because it's a long flight, so I can't do much in the plane and already plan to just rest since I don't have enough sleep theese days, and I will arrive in Paris in the morning and plan to start exploring the city as soon as I arrived, but I already plan when should I start sleeping to so I won't have a severe jetlag.
* I successfully manage my sleep time to avoid jetlag, and I met nice people in the flight and had nice conversation, so the day felt successful even though I didn't do many things there.
* I should be able to follow the things I have planned next time

# 2024-12-27
* productive/successful
* I explore the city once I arrive and meet new people so I am really happy but I also do some of my work in the library so it feels productive 
* I should keep manage my time like this

# 2024-12-28
* unproductive/succcesful
* I had too much fun and forget to to the things I need to work on
* next time I need to manage my time more efficiently 

# 2024-12-29
* unproductive/successful
* I enjoyed exploring the city again and only a little bit of my jobs so i dont feel productive today
* I should manage my time better so I can still doing the things I need to do while also having fun

# 2024-12-30
* productive/successful
* I visited many places and its feels so nice, after that I also managed to do some of my things even it's really tiring
* I should keep doing this

# 2024-12-31
* productive/successful 
* I woke up pretty late but still managed to visit some places before the new year's eve dinner that I planned to attend, but theres a place that just closed when I arrived, however the night is really great and spent with great people
* next time I should plan more accordingly and check the information such as closing hours in special days

# 2025-01-01
* unproductive/unsuccessful
* I woke up late today since I just arrived home so late and I don't feel really good, but I force myself to go outside and explore the city again, but just like before theres a place that just close when I arrive (I think I still not used to how its closed so early here)
* I should wake up and go outside earlier next time

# 2025-01-02
* productive/successful
* I woke up earlier today and start doing some of my tasks once I woke up, and after that I countinue to exploring the city and packing my things since I need to go to another city tomorrow
* I should keep up the this spirit everyday and use my time wisely

# Obituary

Lidwina Hannah Salim, a compassionate leader, innovative thinker, and advocate for positive change, passed away at the age of 85. Her life was defined by a relentless pursuit of excellence and a dedication to improving the lives of others. A trailblazer in her field, Lidwina combined business acumen with a deep sense of purpose, leaving behind a legacy of impactful initiatives that bridged economic success with social responsibility.

Her journey was one of resilience and determination. From her early days as a student navigating challenges to becoming a global leader, she embraced every opportunity with curiosity and an unwavering belief in the potential for growth and transformation. Her work not only shaped industries but also inspired a generation of future leaders to pursue meaningful success. Known for her humility, empathy, and ability to connect with people from all walks of life, Lidwina was a mentor to many and a source of inspiration to all who knew her.

Two individuals who reflect the legacy I hope to achieve are Melinda French Gates and Mother Teresa.

Melinda French Gates exemplifies the kind of leadership I admire. Her work through the Gates Foundation demonstrates a profound commitment to addressing global challenges, from poverty to education and healthcare. What enabled her to achieve such transformative impact was her ability to blend strategic thinking with a genuine compassion for humanity. Her collaborative approach and dedication to creating equitable opportunities for all are qualities I strive to embody.

Mother Teresa represents the selfless compassion and service that I aspire to bring to my work and life. Her unwavering dedication to helping the poorest and most vulnerable is a powerful reminder of the impact of small but meaningful actions. What enabled her to achieve such profound influence was her humility, relentless commitment, and ability to see the dignity in every person. Her life serves as a beacon of hope and a call to action for anyone who wishes to make the world a kinder, more just place.

May my life, like theirs, be remembered for making a difference—building bridges, fostering opportunities, and leaving the world better than I found it.

# 2024-01-09
* This is the last meeting of this class, and even though I cannot come to the class, I woke up early in the morning to attend the final exam at the same time as the students in Taiwan
* There is a mistake in our group video but we can change it after that

# Course Feedback
Write the first thing that comes to your mind when you think about this course.
Different kinds of topics, because I think the topics covered in this class vary a lot, and all are really useful.

Write the five things you liked the most about the course.
Various topics: I really like how it covered many topics, which are really useful and can be applied in our lives.
English-taught: This is the main reason I took this class because it’s easier to understand.
Interactive: The teacher often asks questions to the students, and we need to express our opinions, which is not very common in Taiwan’s education system.
Random groups: We get to know new people.
Group project: We can divide the work rather than doing it all by ourselves.

Write the five things that you disliked the most about this course.
I actually cannot think of five things, but what I disliked the most was having uncooperative group members.

Write one sentence that you would like to tell other students thinking about taking this course.
This class covers various topics, and you will get different groups every few weeks, which is nice because you get to know new people, but some group members might not be very cooperative.

Which lecture do you think needs the most improvement, and how would you improve it?
Maybe the lecture about planetary boundaries, because it was my first time hearing about it, so it would be helpful to give more explanations about each boundary in a way that’s easier to understand.

# "A World of Facts, Challenges, and Ideas"
# 1. Know the current world demographics
* Regularly read demographic reports from reliable sources like the United Nations or World Bank.
* Use interactive data visualization tools (e.g., Gapminder) to explore demographic trends.
* Participate in online courses on population studies or global trends.
# 2. Ability to use demographics to explain engineering needs
* Analyze case studies where engineering solutions were influenced by demographic trends, such as urban planning or renewable energy systems.
* Collaborate on projects that integrate demographic data into engineering design (e.g., building infrastructure for aging populations).
# 3. Understand planetary boundaries and the current state
* Study the Stockholm Resilience Centre’s framework on planetary boundaries.
* Attend webinars or conferences on sustainability and environmental challenges.
* Conduct experiments or simulations on resource consumption limits.
# 4. Understand how the current economic system fails to distribute resources
* Read articles or books on economic inequality (e.g., Capital in the Twenty-First Century by Thomas Piketty).
* Join discussions or debates on economic systems and resource allocation.
* Volunteer with organizations working to alleviate poverty or address resource scarcity.
# 5. Be familiar with future visions and their implications, such as artificial intelligence
* Follow thought leaders and industry experts on AI and emerging technologies on platforms like LinkedIn or Twitter.
* Take online courses or attend workshops on AI and its societal impacts.
* Participate in hackathons or innovation challenges related to future technologies.

# "Personal Health, Happiness, and Society"
# 6. Understand how data and engineering enable a healthier life
* Explore case studies where engineering solutions have improved healthcare, such as wearable devices or telemedicine.
* Learn to analyze health-related data using tools like Python or Excel.
* Collaborate on health-tech projects or attend health-focused innovation summits.
# 7. Know that social relationships give a 50% increased likelihood of survival
* Build a habit of engaging in meaningful conversations and maintaining connections with friends and family.
* Participate in community or social groups to strengthen your social network.
* Take courses or read materials on emotional intelligence and relationship management.
# 8. Be familiar with depression and mental health issues
* Enroll in workshops or courses on mental health awareness.
* Read mental health resources from reputable organizations like WHO or the American Psychological Association.
* Practice mindfulness or engage in activities like yoga to understand the impact of mental health on well-being.
# 9. Know the optimal algorithm for finding a partner for life
* Read about decision-making models, such as the 37% rule in dating or partner selection.
* Reflect on your values and priorities to understand what matters most in a relationship.
* Attend social events or join dating platforms to practice decision-making in partner selection.

# "Professional Skills to Success"
# 10. Develop a custom of questioning claims to avoid “fake news”
* Practice fact-checking information using trusted fact-checking websites like Snopes or PolitiFact.
* Take courses on media literacy and critical thinking.
* Use the CRAAP (Currency, Relevance, Authority, Accuracy, Purpose) test to evaluate sources.
# 11. Be able to do basic analysis and interpretation of time series data
* Learn to use statistical tools like Excel, R, or Python for time series analysis.
* Work on datasets from platforms like Kaggle to practice analysis.
* Attend workshops or webinars on data analytics and interpretation.
# 12. Experience collaborative and problem-based learning
* Join group projects or collaborative learning platforms like Slack or Trello.
* Participate in problem-solving competitions or hackathons.
* Work on real-world case studies with peers or colleagues.
# 13. Understand that professional success depends on social skills
* Take courses on soft skills like communication, teamwork, and leadership.
* Join networking events or professional organizations to practice social skills.
* Seek feedback from mentors or peers on interpersonal interactions.
# 14. Know that the culture of the workplace affects performance
* Observe and reflect on workplace cultures through internships or part-time jobs.
* Read about organizational behavior and culture-building practices.
* Engage in discussions with professionals from diverse industries to understand how culture impacts performance.