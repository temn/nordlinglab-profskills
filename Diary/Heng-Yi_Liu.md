This diary file is written by Heng-Yi Liu F64134043 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-19

1.Today is my first time attend in whole-English class in NCKU, so I have a little nervous because I am afraid of I can not understand what professor says.
  Fortunately, I understand at least half of the content in the class and learn some useful knowledge, such as: how to prevent conflicts and how to analyze data.

2.But I am still a little confused because I am not familiar with the teacher's lab website; therefore, I need to spend some time exploring the website.



# 2024-09-26

## What I learned Today
1.Three things that we should remind us to avoid bad statistics:
	(1)Seeing the uncertaincy
	(2)Can I see myself in the data?
	(3)How was the data collected?
2.The data results might be influenced by many aspects such as its showing form, unit of stastistics and how many parameters were involved in.
  Also, many data which show the average numbers to mislead people belong to bad statistics.
3.We can use data to break stereotypes of people and observe some change in the world.

## Task 1 question
As a freshman in the department of Geomatics, I think I will live in a highly developed city, maybe in Taiwan or other country. As for my future job, I think I might enter the government department, technonlogy indistry which is suveying GIS or navigation and map related industries.
Moreover, when it comes to the current trends that might affect me in the future, I mention 5 trends below:

### 1.The Rise Of AI
Due to the rise of AI, we can increase our efficiency in many aspects of works, such as collecting and analyzing data. In Geomatics field, many complex analysis and data comparison can be accelerated by using AI, so I can enhance my work efficiency when working or even creating.
### 2.The SDGs Goals
Since 2015, the the Sustainable Development Goals(SDGs) were have been pay a lot of attention around the world. Because of SDGs call for environment improvement and human well-being, I think no matter what kinds of corporation and institution should all abide by the rules when it operating. Therefore, these rules will affect the way I doing the jobs. 
### 3.Global cooperation
Responding to the era of globalization, many surveys need different country's people to participate in, for example: Communicating with Germany and Canada, which have extensive research on Geomatics, can increase the speed of industrial and academic progress. Thus, I might use the chance to communicate with foreigners in the next 10 years.
### 4.Increased Military Demand
In recent years, many conflicts have occurred around the world, so people have to use Geomatics to locate positions and analyze the terrian. Furthermore, I think the applications on this field will only increase instead of decrease.
### 5.The Crisis Of Global Warming
Because global warming is becoming more and more serious, during disaster relief, we will use technologies related to my major, which will allow us to conduct search and rescue operations faster.

## Task 3 question
I choose the news "Canada announces $10 million for humanitarian assistance in Lebanon" in https://ground.news/ , and after I compare to many news reports, I found that there are 3 different view points in the same news topic.

Left:Emphasis on calling for cease-fire and the words is more moderate and conservative.

Center:There are about as many donations as there are words calling for an cease-fire.

Right:Describe the horror of the attack more clearly than the donations.



# 2024-10-03

Unfortunately, today's class is canceled due to typhoon.



# 2024-10-10

We take a day off due to National Day of ROC.


# 2024-10-17

In today class, I learn many financial knowledge which is I seldom contact with.

Before this class, I have never think seriously about" what gives money its value?"; after this class, I am surprise because money's value is determined by loans!

Moreover, I learn the 5 desirable properties of money and also understand that why bitcoin cannot be Taiwan's currency in circulation.

Among today's class, I think the most interesting part is one figure demonstrates Taiwanese Deposits and loans, in currency held by the public has cyclical changes are actually related to the Chinese New Year!


# 2024-10-24

## Three fictional stories
1.People can use a signature to print money leagally, and it affects currency's value.

2.Taiwanese live their lives in compliance with countless lengthy laws, and once they break the law, they have to pay the price.

3.In the past two weeks, I have been racing against time to complete my homework and prepare for many exams.

## The future offered by some famous politicians in Taiwan
Due to the results of the 2024 presidential and congressional elections, there have been many chaos in the domestic political circle. Moreover, if they continue to create hatred, it will have negative impacts on both inside and outside to the country. So I think if the political chaos continues, Taiwan's future will be full of uncertainty.

## 3 rules for how to live healthy
### 1.Get moving: 
According to the U.S. Department of Health and Human Services, just 150 minutes of moderate-intensity aerobic activity a week, or 75 minutes of vigorous aerobic activity a week, can positively impact your health.

### 2.Eat more whole foods and less processed food: 
In general, whole foods are healthier for you and provide your body with more vitamins, minerals, and essential nutrients than processed foods which is contented more sugar, salt, fat and so on.

### 3.Have at least 7 or more hours of sleep each night: 
Sleep is a time for your body to repair cells and restore energy. Your brain also performs many essential functions while you're sleeping, like storing information, removing waste, and strengthening nerve cell connections.

# 2024-10-31

Unfortunately, today's class is canceled again due to typhoon.



# 2024-11-07
Today's class content is very diverse but serious. At first, I take part in the supergroup discussion with combining 4 groups of homework and choosing one claim to explain whether we accept the hypothesis.

Then, we watch many TED Talks about dispression and suicide. After watching these videos, I associate with I often hear from the tragic news of suicide. I believe that before they decide to do this, they have many struggles in their mind, but maybe they don't tell anyone thier situations so that no one can provide asscitances to them and stop the tragedy happened.

Moreover, I come up with the instructor of military training course mentioned that there are more suicide incidents rather than traffic accidents in NCKU. It demonstrates that quite a few students in NCKU have suffered from depression. Thus, after this class, I will pay more attention to whether the people around me are depressed and be a listener to provide them help.

However, because I am not an expert in this field, as the professor mentioned, if you accept too many negative emotions, you will be affected. Therefore, when there are too many negative emotions l have received, l will stay away temporarily and help them seek professional help.


# 2024-11-14

In today's class, I see the anxiety survey results about two weeks ago that we had done.
I'm not very surprise that most of us have several times anxiety situations during the end of October, because that period is close to the middle of the semester and many students may have midterm exams or reports which were needed to prepare for.

Then, I witness a great debate between two classmates about Taiwan's energy issue during the class.

Furthermore, l can feel that the presenters in today's lecture are prepared carefully and shared some of their own experiences to support their claims, so that we can easily understand the content of the report.

# 2024-11-21

In today's lecture, I see many groups discuss the laws of environment and demonstration issues.
Moreover, after watching some TED Talks videos, I learn that the negative impacts of technological advancement and the development of social media, and how people are unconsciously manipulated. 

## The consensus on a minimal and sufficient set of enforceable laws
### 1.Privacy Guarantee
•No data collection and sharing without consent.

•Build a data transparency mechanism to provide control over personal data.
### 2.Fight fake news
•Establish a fact-checking angency to verify public information.

•Provide a trusted public platform to identify real information.

# 2024-11-28

In today's lecture, I see many groups present their reports about final tasks. The contents are diverse so I learn a lot about the field I seldom contact with but close to our life, such as government policy responsibility and environmet issues. 

Also, I see many creative thought and simple but useful things that can improve our life. Among them, one group mention a device that can effectively wake up roommates without disturbing other people. As a person who is always woken up by roommate's alert clock, it's very partical to improve the  sleep quality for me if he wear that device.

# 2024-12-05

This speech recomended by professor helped me understand what fascism is and how it differs from nationalism. I also discovered that fascism is not as obvious as the bad guys in movies, but it uses beautiful images to attract us and make us lose our alertness.

In addition, although technological advancement brings convenience, it may also be used to centralize data and manipulate human emotions, further threatening democracy. The use should also be more careful to ensure that it serves human beings.

# 2024-12-12

Althought I didn't go to the class today because I got a severe cold, I watch the video " The Third Industrial Revolution: A Radical New Sharing Economy" which is put in the lecture's pdf file. After watching the video, I start to think of what problems can be solved by the Third Industrial Revolution?

In the past decades, we have developed many technology to improve our lives but also harm our environment, so how to handle those environmental problems? 

Automation and intelligence have become key words, and with the popularization of the Internet, information access and exchange have become more convenient, and businesses and individuals can quickly obtain the knowledge and resources they need. The most important thing is that the background of this revolution is closely related to the concept of sustainable development, as mentioned in the film, as long as we make good use of the things developed in the third industrial revolution, we will definitely achieve the goal of sustainable development of the earth.

# 2024-12-19
### 2024-12-19 Thu
A. Unsuccessful and unproductive.

B. I felt unsuccessful because I am dispointed to the result of a subject's midterm grades. I felt unproductive because I can't figure out why I have studied hard but still can't get good grades?

C. I will find out the problems and adjust the way I study to improve myself.

### 2024-12-20 Fri
A. Successful and unproductive.

B. I felt successful because I figured out some calculus parametric equations' problems. I felt unproductive because my cold is not completely gone yet, so I need to rest more instead of preparing for final reports and exams all the time.

C. I will adjust my diet and sleep to speed up recovery.

### 2024-12-21 Sat
A. Unsuccessful and unproductive.

B. I felt unsuccessful because I found some new concepts in physics weren't easy to understand. I felt unproductive because I still felt tired result from the flu. 

C. I will try to rest more on a cold day.

### 2024-12-22 Sun
A. Successful and productive.

B. I felt successful because figure out some concepts in thermodynamics. I felt productive because I felt better today so I study more today.

C. I will prevent myself to catch cold again by by exercising and dressing warmer.

### 2024-12-23 Mon
A. Successful and unproductive.

B. I felt successful because I finished the presentation of Taiwanese general course and received a lot of positive feedback. Some people also helped me point out where I can improve.
I felt unproductive because of the wet and cold weather.

C. I will be willing to accept other people's opinions.

### 2024-12-24 Tue
A. Unsuccessful and unproductive.

B. I felt successful because I tried many times but couldn't get the Arduino board to work properly. I felt unproductive because of the bad weather again.

C. I have to figure out the problem by trying again and again.

### 2024-12-25 Wed
A. Successful and productive.

B. I felt successful because I finished two courses's final exam today. I felt productive because of the good weather and having more free time today.

C. I will plan my free time properly.

# 2024-12-26
### 2024-12-26 Thu
A. Unsuccessful and unproductive.

B. I felt unsuccessful because I am dispointed to the result of a subject's midterm grades. I felt unproductive because I can't figure out why I have studied hard but still can't get good grades?

C. I will find out the problems and adjust the way I study to improve myself.

### 2024-12-27 Fri
A. Successful and unproductive.

B. I felt successful because I finished final presentation in English course and gained some positive comments. I felt unproductive because my cold is not completely gone yet, so I need to rest more instead of preparing for final reports and exams all the time.

C. I will adjust my diet and sleep to speed up recovery.

### 2024-12-28 Sat
A. Unsuccessful and unproductive.

B. I felt unsuccessful because I found some new concepts in physics weren't easy to understand. I felt unproductive because I still felt tired result from the cold. 

C. I will try to rest more on a cold day.

### 2024-12-29 Sun
A. Successful and productive.

B. I felt successful because figure out some concepts in thermodynamics. I felt productive because I felt better today so I study more today.

C. I will prevent myself to catch cold again by by exercising and dressing warmer.

### 2024-12-30 Mon
A. Successful and productive.

B. I felt successful because I finished the presentation of Taiwanese general course and received a lot of positive feedback. Some people also helped me point out where I can improve.
I felt productive because I finished many tasks today.

C. I will be willing to accept other people's opinions.

### 2024-12-31 Tue
A. Unsuccessful and unproductive.

B. I felt unsuccessful because I tried many times but couldn't get the Arduino board to work properly. I felt unproductive because it seems that I have done many things but just like did nothing.

C. I have to figure out the problem by trying again and again.

### 2025-01-01 Wed
A. Successful and unproductive.

B. I felt successful because today is the first day of 2025, and it is a new begining. I felt unproductive because I don't know why I felt very tired today.

C. I will sleep more to observe whether the fatigue can be removed.

# 2025-01-02
Many issues were discussed during today class, such as women's rights, bullying, and the 16-week or 18-week course schedule that is currently being discussed heatedly. 
In addition, the professor also said in class that he might offer a finance-related course in the future, integrating games and role-playing so that students can learn. If the professor offers this course in the future, I would like to try it, because finance knowledge is very practical.