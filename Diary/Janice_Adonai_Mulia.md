This diary file is written by Janice Adonai Mulia H44097061 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15#
* The second lecture was great. 
* It was less overwhelming compared to the first lecture. 
* I was able to get a better grasp of the contents. 
* Our group was not able to present, but it was nice to hear other people's opinion.

# 2022-09-22#
* I was first introduced to SDGs when I was in highschool
* I didn't think that there was any change until I saw the statistics that was shown in class 
* Now what do I think of the future, especially in the next 10 years?
* I honestly don’t know where to start, I think that there is a lot that has changed. 
* I used to think that one of the slowest impacts would be electric vehicles. 
* Although a lot of developed countries are now using electric vehicles, like Teslas and Gogoros, I thought it would still take a long time before they would reach places like my country. 
* But when I went back home, I see that some car companies starting to produce electric cars and encourage people to lean more toward cleaner energy. 
* I that with the inflation and the increasing price of fuel, it will be very likely that people will be using electric vehicles in the next 10 years. 
* This way, not only will be better for the environment, it would be cheaper for them to use in the long run. 

As I said, I don’t know what to expect in the future. No one would have expected that there would be a pandemic, let alone a war. 
But it did happen in the last 5 years, so I think anything could happen. 
But in a more general sense, I’m hoping that there will be a more diverse culture all around the world, and not just in work areas, but in public areas too. 
One of the biggest events that might occur not too long ago, was the injustice toward George Floyd, which started the “Black Lives Matter” movement.
Other than that was the “Stop Asian Hate” movement, I think this movement would hit closer to home. 
And now with social media being more powerful and impactful, I think that a lot of people are starting to see the injustice that occurs on a normal day. 
I think this way, people would be more accepting of other ethnicities. 

Unfortunately, I’m not too entirely sure how we would slow down climate change in the next 10 years. 
Although I do believe that there would be some improvements that would be done, better technologies, and innovations that would protect the ecosystem, I do think that it would be very slow. 
I think that it would be hard for a lot of people on board to stop something they can’t see. 
There is still a lot of litter dumped into the ocean. I think that to make a bigger impact, bigger manpower would be needed. 
Although I’m being pessimistic, I think that slow progress is better than none.

# 2022-09-29#
* In this lecture, I was not sure on what to expect. 
* We get to see some of the presentations done by some of the groups, which showed some interesting fake pictures. 
* Then the lecturer had us do a survey about money and where they get their value. 
* There are some that I'm aware of, but there are some that I either forget or did not know at all. 
* So, it was interesting to listen to his lecture this week

# 2022-10-06#
* This was the first time we had our lecture in class
* I think that we spent too much time too focused on the presentations
* there seem to be some trouble with the volume 
* I had a hard time thinking of stories that have affected me
* I could only think of a book called "Little Women" 
* It shows a series of short stories of 4 sisters, their struggles and disputes. 
* It inspired me to not be too bitter about what happened and to reconcile with people I have not connected with a long time.

# 2022-10-13#
* I think that this week's class was a little bit more interesting. 
* We watched 2 ted talks about our health, but the first one is what drew my attention the most. 
* It was about how exercise would really improve our brain function. 
* The need to exercise was something that I feel like I should be doing, but didn't.
* I guess it's just cool to see how it would also help prevent brain diseases.
* It was also funny when the professor made us stand up to follow the actions in the video.

# 2022-10-27#
* This week, we had our class online.
* I think that it was pretty interesting, we got to join with other groups
* The whole project that we did was pretty interesting and I think that it went well. 
* After that, we were put into different group, which I might not be too excited about, because I was already comfortable with my old group
* I think that the class overall was pretty interesting, but I do have to say that the bigger projects were a bit too much. 
* I think that there were too many projects that were introduced and that the professor was skimming through it too quickly for me to understand

# 2022-11-03#
* This week's lecture was back in the class. 
* We continued to talk about depression and how to help our friends with depression. 
* And we then watched another Ted talk. 
* Honestly, I think that the speaker was not able to explain the methods of the experiments well. 
* So it was hard for me to keep up with what was happening and what the result shows. 

# 2022-11-10#
* This week, our class is mainly about our big group presentation
* There are some things about our group's topic that we did not venture into.
* We'll pay a closer attention on those details for the next course project presentation.
* Then we watched a Ted talk that which I found pretty interesting.

# 2022-11-17#
* I did not present this week, but I get to see other people's presentation. 
* We watch a video on how to be connected and free, which was interesting. 
* We were then put into groups to discuss a way to create a law that would allow freedom of speech, privacy and other things without manipulation, fake news and other negative aspects
* we would be having a big group presentation next week.

# 2022-11-24#
* This week, the professor had our phones collected for an experiment 
* It did not bother me too much, but I unconsciously looked for my phones once or twice
* We also had the big group presentation, but my group did not come up this week because we already went last time 
* and after that, the professor played a video for us to watch, which I find interesting 

# 2022-12-01#
* This week, we had a discussion about newspapers from different countries and their biases 
* It slipped my mind to include the country of the topic itself
* other than that, we were sharing our 3 actions and whether they are feasible or not 
* I think my group needs to revise out actions and plans to do them 

# 2022-12-08#
* This week, we had the big group presentation
* since there were a lot of groups, we didn't get to present.
* but since our action similar to another group, we have to change our action which is more complicated for us

# 2022-12-15#
* I think that the class was pretty interesting
* I'm pretty clueless about the 4th industrial revolution, so there are times where I was lost during the lectures 
* the class was also really cold and I was feeling unwell, so I had a hard time focusing to the lecture by the third hour of the class

# 2022-12-16#
* Successful and Unproductive
* I had a lot of presentations this week and I had the last one today. I feel successful after I went through all of them. But I feel unproductive because I wanted to start editing an essay that's due soon, but my teammates were not cooperative 
* I'll be heading to a coffee shop tomorrow to edit the essay and hopefully I can finish it and start studying for my exams. 

# 2022-12-17#
* unsuccessful and productive
* I was planning on revising for my exams, but editing my group's essay is taking longer than I thought
* I'm hoping finish is before 3 tomorrow, and head to a cafe or the library to start revising

# 2022-12-18#
* successful and productive 
* I finished my group essay and got to revise for my exams 
* I'm hoping to get good rest tonight, so I can focus better tomorrow

# 2022-12-19#
* Unsuccessful and unproductive
* I didn't do anything that needs to get done, and I'm still procrastinating
* I think I need to make a checklist of what I need to get done and their urgnecy, so that I can prioritize the important ones first

# 2022-12-20# 
* Successful and Productive.
* I had 2 group discussion without any issue, I also finished all my tasks for today.
* I hope to also be productive tomorrow, and get through my tasks

# 2022-12-21#
* Unsuccessful and Unproductive.
* I didn't plan on what to do today, so I didn't really do what needed to get done.
* I need to think of what I need to do from the night before, so I won't waste time the next day.

# 2022-12-22# 
* I presented for our course project this week, and I'm glad that we could use the idea we came up with.
* I didn't really dress for success today, because it's getting cold now.
* Then the teacher professor showed us a ted talk on how should we live together. 
* it talks about the issues with democracy, which is pretty insightful

# 2022-12-29#
* We had the big group presentation this week 
* Our group didn't get to present, but the professor gave us advice to face the problem we have 

# 2023-01-05# 
* We had our exam today, I think it was too many questions for 25 minutes. I nearly didn't finish on time 
* We presented our progress this week, and it seemed like the professor had no problem with it too.
* We're almost done with the video, just need some minor editing 
* Other then that, the professor showed us TED Talk videos, which was interesting