This diary file is written by Justin Wu E24105088 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #

* I learnt what is a GIT thru some research (It's a Version Control System), and GIT doesn't have an offical abberviation
* I'm also considering an EV, but as for now they are still too expensive
* Notes on how to credit in PPT: credit the author and CC license under every image and source, and state the date on URL
* Thoughts on Steven Pinker's video: I agree with almost all of his points, but I found that suicide rate actually increased 30% between 2000–2018 in the US
* Would like to share an interesting video I came across with everyone weekly: https://youtu.be/_Y1-KlTEmwk

# 2022-09-22 #

* Regarding first group's presentation, I don't see allowing gay marriage as problem, but rather just a necessary need that society will adatap to on its way forward
* I now know there's 3 ways to spot bad statistic: Can you see uncertainty, Can I see myself in the data, and How was the data collected?
* 
* I selected "Boeing to pay $200 million to settle U.S. charges it misled investors about 737 MAX" as my news summary, different views are as followed:
* Left: Boeing's chief technial pilot should face crimial charges but didn't.
* Center: Reporting both Boeing and Muilenburg sides equally, showing both have their own fault.
* Right: Boeing shares are down 68%, but their best-selling plane will returned to the skies in November 2020.
* 
* I think the five trends that will take over in the next 10 years are foldable tech, glass interface devices, autonomous electric car, Bluetooth Auracast, and renewable energy.
* It is a morning of August, 2032, and I wake up to my alarm that's echoing through out my double room apartment. Spaces are at a premium right now, but thanks to VR and Metaverse advancement, you can still make a tiny space feel much much bigger. Technology and devices have filled up our lives, that includes my room's windows that are functioning as speakers right now.
* Glass is not only used as a structural element, but mainly it is acting as most of the interface material now. Corning had really incorporated this material in all aspect that requires a strong but transparent interface, from shop windows, bus signs, lamp posts, and information signs are all capable of providing relavent data to pedestrian.
* I pick up my Samsung Z Flip 15 Max Pro Cyberpunk edition, a 10 inch tablet that can fold down into a wearable bracelet. I leave the house without bringing anything else.
* It can act as my wallet, social security card, health ID, autonomous viechial fare, and everything I need at work. By the way, I am a product designer/ mechanism optimizer by trade.
* Just by speaking to my Flip bracelet, I order a driver-less electric taxi to take me to work. Around 86% of the cars on the road now are fully autonomous, but there are still people who prefer to drive themselves to their destination, or just enjoy the thrill of driving.
* All the autonomous cars are using their build-in 6G capabilities to talk to one another, and also to the entire city's traffic system; meaning vision-based driving systems are just as a backup because they can get all the real-time traffic information directly feed to their processor. Oh, the taxi is showing me that it is taking an alternative route because an emergency convoy is heading down this lane.
* After a 5 minutes commute, I arrive at my company, JustInTime Corporation. I pick out the integrated earbuds from my Flip and using the newest Bluetooth Auracast standard, the daily beirfing is feed into my ear automatcially without me having to select a device to connect.
* When I want to talk to my coworkers on another floor, I just need to speak out "Hey Bixby, connect me to Zoe in room 512", and it will directly link our buds together for a direct communication.
* Lastly, thanks to all the advancements in renewable energy, our company and society are releasing much much less CO2 into the atmosphere now. Even tho globlal warming is still a problem, we as a species have it under control now. What a wonderful world!

# 2022-10-06 #

* People are used to thinking in linear, but most industrial processes are exponential in nature
* Indonesian purchasing power has actually gone up, unlike other countries 
* GDP to Debt graphs can hide the exponential growth of debt, since GDP tends to grow exponential as well
* Weather borrowing from a foreign country's central bank is a good thing or not, depends on the amount being borrowed!

* Video note:
* Weak nationalism leads to violence and poverty 
* Fascism denies all other identities other than being a patriot
* Evil things are often quite beautiful in real life, the same goes for fascism 
* Centralized information is the most powerful 
* Don't let democracy become an emotional puppet show (Don't let them affect your emotions) 
* Engineers beware of who controls the data, find ways to control which few people can control the data
* Don't be manipulated by those who control the data
* Also worry about dictatorship who controls all the data, other than fascism 
* Finding identity, purpose, community, and where do I fit in
* Draw marginalized teens in using false paradise pictures
* " Was it my purpose to scortch the earth, or was it to make it into a better one?"
* "I was afraid to be judged, the same way I judged people"

# 2022-10-13 #
* Some Taiwanese don't eat beef before exam, I didn't know that
* I like the second group's advice on watching a news channel you agree with first, then watching an opposing view channel later to compare
* Knowledge: it's information that you internalized (my definition) 
* &#8220;well-justified true believe&#8221;
* I think many students in Taiwan are being taught information, but not knowledge. They just accept everything they are taught in class and do not turn it into a well-justified true belief. 
* Can you prove a claim? - ask the professor to discuss at later lectures
* Video claims:
* immediate benefits to your body, mood, and health, the powerful effects of exercise that can last for your life
* brief moment that can alter your whole life 
* exercise is the most transformative thing you can do to your body
* do cardiovascular workout for long-term effects 
* produce new brain cells in the hippocampus 
* 3 to 4 times, 30 minutes each, aerobic per week
* exercise can protect your brain from incurable diseases and change the trajectory of your life
* be paid to keep patients healthy, not charge when they got sick
* turning a sick-care system into a health-care system

# 2022-10-27 #
* I learnt there are really so many benefits when it comes to exercise, and hope I can turn it into a habbit of mine
* The class every doesn't give enough time for more than 5 groups to present, and it decreases the drive to complete PPTs.
* Suffered from depression ever since I was young, but after transferring into the faculty I really want, it reduces a lot to almost none.
* Talk to depressed people normally, don't get emotional if they don't respond the way you expected.

# 2022-11-03 #
* In our class, 86% of the students don't seek professional help for their anxiety 
* It's very unlikely that the first doctor you speak to will be the right fit for you, seek at least 3 doctors, according to our professor 
* To make yourself feel good, we each need a suitable challenge
* Exercise has been shown to have a positive effect during treatment of depression 
* I experienced losing a friend to suicide for 10 days. During that time, I was so numb to every stimulate, even slapping myself hard, and looking in the mirror, I saw a dead person and kept blaming myself. Thankfully she was rescued just in time and got better now, but it was truly the most painful memory in my lifetime. 
* Finding a “Work Group” in a company is important for insights and growth
* 6 areas new employees must master: People, Performance Proficiency, Politics, Language, Organizational Goals, History  
* Professional skills are soft skills like how to communicate with people, public speaking 
* 
* Video 1 - It's hard to prove someone is wrong about themselves 
* Real choices vs mapullated choice, prehaps we made them up
* Choice Blindness - new term
* Shift people's view and argue with themselves 
* Asking people “why?” can create an attitude in people that wasn't there before
* Allow people and yourself to take things back
* 
* Video 2 - A powerful and touching video, violence is a leant behavior 
* Meditation is a toolkit to reduce anger
* 
* Video 3 - Chasing happiness can make people unhappy 
* 4 pillars of meaningful life: belonging, purpose, transcendence, storytelling 
* You can edit and change the storytelling, even if constraint by the facts
* Sufi

# 2022-11-10 #

* Construction rules in Tainen, all week 7am ~ 10pm, and heavily biased towards the construction companies 

* The embodied carbon of the building takes 11% of the global energy-related carbon emission

* CLT buildings could reduce CO2 emission by 36% compared to concrete

*  They can even last longer than steel-concrete buildings in some circumstances, which is very surprising to me! 

# 2022-11-17 #

* Only 2 countries have plans to reduce the CO2 emission within 1.5 degrees C

* One country is actually carbon negative, which is Bhutan 

* Channel 52 (中天新聞台）being close down is perhaps a violation of Freedom of Speech?

* Video 1 notes: Technology could destroy us

* Technology should draw us away from mass suicide

* "It's much easier to lost trust than built trust"

* Video 2 notes: Millennial suffers from "Failed Parenting strategies"

* No phone policy

* Video 3 notes: "Weapons of Math destruction"

* AAA ratings are lies hidden in mathematics

* "Algorithm are opions emdded in codes"

* Widespread, Mysterious, Destructive. Three factors of bad algorithm.

* Rules are needed to define where one persons freedom ends and another persons start.

# 2022-11-24 #
* On how to reduce carbon emissions, Group 5 came up with actions such as "Implement carbon emissions inventory", "Increase carbon tax", "Replacing old or energy-intensive equipment"
* Carbon tax is to subsidize the carbon released from all parties, and we as individual people are indirectly paying for it already because we release so much less than corporations 
* The European Union plan to put an import tax on all Taiwanese products (or other countries) that don't reach the carbon-reduction target by 2030
* Gold is the most recycled material on Earth, simply due to how expensive it is, which means all other materials are too cheap to obtain?
* My steps towards reducing CO2 emission: bike to class, turn off lights and computer when I don't use them, use my own LED lamp than the dorm-provided fluorescent desk light, turn off the public washroom and bathroom lights before I go to sleep, eat locally and don't travel when unesscary
* "Fines are a good motivator" - from Group 9's presentation
* Solution to reduce noise problem, use earplugs! - Group 8
* How to make sense of events: today's lesson
* Video 1 notes: traditional media can also be sabotaged, including democratic countries
* Try not to follow real-time news, it could falsely be accusing innocent people
* Separate facts from opinions, and verify the sources
* Video 2 notes: monetary system causes many of its own problems
* No sign of inflation in ancient Rome!  Not until war arrived and they needed more money and started making more gold coins with cheaper metal such as copper
* War spending and Public works lead to chaos?
* "We no longer have money, we have currency"
* Gold always catches up to how money empty money we printed
* Those who are able to hold gold maintained their purchasing power, those who don't suffer
* "Hyperinflation", Rome forced people to work and sell at unlivable wages imposed under the penalty of death

# 2022-12-1 #
* Video of the week: https://youtu.be/RzkD_rTEBYs
* The weather is getting colder everyday, watch out for Seasonal Affective Disorder (SAD)

# 2022-12-8 #
* "Have one less child" is the most impactful action to reduce carbon footprint as individual, followed by "Live car free", "Avoid one transatlantic flight", "Buy green energy", "Buy more efficient car"
* Recycling plastic is a scam!  There is so far no effective way to recycle them

# 2022-12-15 #
* It matters how you vote!  The voting method can affect the outcome, a close-voting system is good to get people to express their true opinion, open-voting system to get them to stand behind their votes
* Why does inequality exists?  Use Gini Coefficient, a measure of satistical dispention to judge social wealth distrabution
* Taiwan's next industrial revoluation is called "Productivity 4.0"
* For Taiwan's electricity generation, nuclear generation is very constant
* Top 50 richest people in the world have seen the most wealth growth rate
* The middle class is losing out in wealth growth rate, "The Shrinking Middle class"
* Bottom 50% captured 2% of global wealth growth
* World inequlity index, earning $300,000 NTD/ month is the top 15% in the world
* You got to be able to save money to be rich, have reserve
* The monotary system will cause you to lose money over time to inflation if you don't invest
* 7 habits of hightly effective people y Steven Covey: 3 levels *Public victory - Interdependence (4. Think win-win, 5. Seek first to understand then to be understood, 6. Synergize)
* Independence
* Private victory - Dependence (1. Be proactive, 2. Begin with te end in mind, 3. Put first thigs first)
* The time-leadership matrix: Ugent, Not Urgent x Important, Not Important
* Radical candor by Kim Scott
* Johari window
* Feedback I-form


# Weekly Diary #
# 2022-12-22 (Thrusday) #

* Unsuccessful and unproductive
* Broke up last night, but thankfully I have friends to support me
* Didn't do projects for the Basic Design final fair, going to start tonight

# 2022-12-23 (Friday) #
* Unsuccessful and productive
* Finish the majority of my final projects, it's so late already
* Hope tomorrow's final fair goes well

# 2022-12-24 (Saturday) #
* Successful and productive
* It's the final fair today, did a good presentation in front of the judges
* Checked out classmates' designs as well, I find everyone has their own unique style and take on design
* Went out with classmates to celebrate we finish all the projects for the fair in time, tho I didn't sleep last night

# 2022-12-25 (Sunday) #
* Successful and productive
* It's Chrismas! Though I am doing final fair, but went out with family to eat dinner
* The second day of final fair, and I know so many people who got picked by the judges :O
* The top three designes from each grade are really eye-opening

# 2022-12-26 (Monday) #
* Successful and unproductive
* It is my mom's birthday today, happy birthday mom!
* Call up a couple of friends to celebrate it with her over sushi dinner

# 2022-12-27 (Tuesday) #
* Successful and unproductive
* Slept over the presentation for next semester's course, but I already know what I want to take
* Brought cream puff and Tiramisu for a girl and ate it with her
# 2022-12-28 (Wednesday) #
* Successful and productive
* For our "Design Method" class, we counted how many bikes are not locked and learnt an interesting fact about the school
* About 20% of the people do not lock their bikes, and varies by the location and time of day!
* Ate dinner with a long-time friend and had a deep face-to-face talk since forever, reminds me of the good old times from last term

# 2022-12-22 #
* Professor teaches us how to tie a Full Windsor tie today
* A tie should be a thumb width on top of belt, not over and not too short
* Video note - How do we share resources?
* The answer is democracy, but it has a massive discrepency
* Ideal vs Reality, time to fix this broken system
* Sortition: random selection, a microcosm of society
* Wisdom of crowds, diversity can trump ability
* Video 2 note - A promise to remain carbon neutral
* Bhutan is seen as the last Shamrla in the world, not the reality
* "Gross National Happiness is more important than GDP"
* Economic growth cannot be built on destorying the national values

5 rules for success
1: Have a clear goal in mind
2: Have confidence, in both posture and dressing and attitude
3: Keep trying, don't fear failure
4: Bring others up, not down
5: Have different support pillars, family, friends, partner, hobbies

# 2022-12-29 #
* Social media causes us to have unrealistic expectations about our own life compare to others
* So the professor wanted us to write diaries to keep track of what real life is like, not everyday is successful and productive
* Price decline due to learning, keep driving down the price of the product to make them more accessible
* Wright's learning curve, use it to your advantage
* Video 1 notes: "What makes something go viral?"
* What makes a video go viral? What were the people thinking when watching livestreaming?
* "Particiaping in the shared anticipation of somethig about to happen"
* People are increasingly using social media to talk to other people
* "This is us", "Connect with family", "Makes me laugh"
* "The Humble brag" from the "Guess your age and height from your outfit", popular amoug 55-ish women
* "Let's do this together", Challenge with your friends mindset
* For my Brawl Stars videos, "This is me proud", "Connect with other fans", "Make you laugh"
* Cultural cartography, what factors make people want to view your post
* Video 2 notes: "Invest in woman and girls"
* Organized the community of girls in Syria
* "Isirika" is a pragmetic way of life in Syria langurage
* Go to their sick neighbours home and harvest their crops for them
* Come together to contribute money to send other people's kids to school and oversea universities
* As she gain knowledge such as "Investment", "Return", "Doner and Recipients", she became less and less Isirika
* You are human together, see a human being first, value all contribusion equally
* 1. Have faith that we are ONE humanity and the world, no more walls and no other planets
* 2. Every ideas count
* 3. Those who has more have the privilages of giving more
* To solve the world's biggest problem, invest in woman and girls
* Video 3 notes: ""
* "Help" is the key word in obituary
* What do I want to be written in my obituary？
* I want to be remembered as "A cheerful friend-maker who brings people together to have a fantastic time"
* Video 4 notes: 
* "Education can be a tool for social justice"
* Our aim is to empower student to articulate their opinion
* Change the way we think about rebellion in our kids, that the education is working
* "Justice is what love looks like in public" - Dr. Comel West
* We can't be afraid of student's power, got to encourage them to express and pratice their own voices
* Why is the professor teaching this course?
* By empowering you to grasp your big dreams for the good of all of us, I hope to make a difference.

# 2022-1-5 #
* In almost all cultures, it is men that do not talk about metal health issue and women can talk about it more freely
* Also more men commits sucides than women
* Group Two's presentation on CLT and GLT building materials, their progress report is very thrual and comprehensive, clearly a lot of research and effects went into it
* They interviewed some industry leaders, but they were all too busy working and thought that the current market in Taiwan do not have enough demand for these building materials
* Group Six's progress report, they are experimenting with using reusable cups for a week and see how much money they save
* They are still working hard on their videos
* Their take on notes of how to make their videos become viral, they suggest asking the interviee of whether they have such rules in thier home country?
* Group Seven's progress report, they set up a facebook fan-page to educate people some basic sleeping knowledge
* They also conduct surveys of around 17 people, and post daily posts to educate people about the important and facts about sleep
* Group Nine's presentation, recycle more and use WBS chart 
* They find people are not willing to fill out long Google form answers
* On how to get viral, they want to copy the "Salt bae" movement and throw the trash and recycling stuff, hoping to create a trend
* Video One "How to stop swiping on dating apps and find the right person" notes:
* Zero date, one drink, one hour, to decide whether you want to have dinner with the person
* Source and get offline as quick as possible
* Video Two "The mathmatics of love" notes:
* Why I don't have a girlfriend paper, written by a mathmatian, Peter
* Only around 26% of the woman in UK fits his estaimate
* Human emotion isn't rational, but that doesn't mean that math can't be helpful
* Top three mathmeaticallly vierfiable tips for love
* How to win at online dating, how atractive you are doesn't equal to how popular you are
* Because you think one person is attractive, you are less likely to pursuit them, so more change with the people you find less attractive
* How do you decide when to settle down? Reject the first 37% of the serious relationships, then accept the one after
* "You are marginally better than the last 37% of the people I dated!"
* The best way to find the best partner in your life, decide what age you want to marry (ex 35 years old), then the first person surpass the previous baseline after 37%, you should just stick with him/ her
* In the wild, fish also follows this "Reject the first 37% rule and accpet the next one that comes along"
* When people choose their online photo, they tend to hide the things about themselves, but will often have the opposite effect
* How to avoid devorce, John Gottman observed one of the most important thing about 90%
* How positive they are in their conversations, more positive the less likely
* How much husband and wife infulance each other in the relationship
* Negativity threshold, the research shows the most sucessful couple have the lowest of this index
* Do not let anything go unnotice and have room for it to grow bigger, disguss everything with your partner

# 2022-1-7 #
* This is the final class!
* It is time for all groups to show the result of their project, produce a 1080p 3 minutes video
* The first group's video was very informative, but not going to go viral
* For our group video, we need to show data graph of our action's result
* The music is too loud and covers the people speaking
* Needs subtitles for interview response so they can understand what's being spoken
* Group 9's video has a very epic beginning clip, astronurts looking at trash mountain
* For education use case, we are allowed to use a certain percentage of the video, book, material without notifying the authurs
* Thank you professor, for the semester and this interesting and useful class.  Will recommand other people to take it!