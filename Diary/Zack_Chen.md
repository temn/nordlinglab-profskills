# 2022-09-15 #
   1.Today's class is awesome.
   2.Other groups' presentations are good. 
   3.It's interesting to know the new platform.
   4.It's interesting that we can post the idea on this platform.

# 2022-09-22 #
I imagine I will live in an apartment at Tainan for job after 10 years.
I work in the technological company as an engineer.

I think there are 5 trends affects me deeply.

First,the evolution of robots causes the opportunities of jobs are fewer and fewer.
Jobs replaced by a robot is not just some repeatedly work.
A part of these jobs even require the certain knowledge and techiniques.
For this reason,I must keep learing new things,cultivating extra expertise,or I may be "replaced" too.

Second,people are more aware of the conservation of the environment.
Eco-friendly products have become the mainstream.
I need to learn some relevant knowledge to reach the standard of eco-friendly during the production.
Many new laws to restrict pollution severely.
Even so,the environment is keeping getting worse.
The main reason for this phenomenon is the population is getting bigger and bigger.
It's hard to lower the consumption of resources. 

Third,more and more things can be done with electronic devices.
I often only take my phone when I go out.
Pay for daily necessities,open the door of my house,detect possible danger etc. 
the things above can be done by a phone.
We can save more time to do other things.

Forth,equality is more valued.
Every public utility must have gender neutral restrooms and complete path for disabled.
More people can doing themselves freely with sound assurance in law.
I am more willing to fight for rights.

The last,the rate of the people getting lifestyle disease is higher.
Cancer,diabetes,high pressure,uremia are more common.
I keep exercising three times a week no matter how busy I am.
I put a lot of effort into eating healthy at the same time.
At least two healthy diets a day is the goal I trying to reach.
I use notebook or private platform to record what happened and what I feel,and I often listen music when I am at home.
By doing these things,I can get some relief and reduce my stress accumulated from life or work.

I imagine the world in the future is more convenient and free,but also have more uncertain risk.
More disease,financial and environment problems heve great influence on our life.
I skould be careful of the change of myself,society and the world all the time. 

# 2022-09-29 #
Today's class is informative.
I know more about the finanal system after this class.
How the governmet controls the value of the money,how money is created,what are the types of the finanal problems etc. 
It's important for everyone to know these things because they have a lot to do with our life,society and the world.

# 2022-10-06 #
Today's class is insporing.
The story about the transformation of the person engaging in movement about racism has an deep inpression on me.
It's difficult changing one's mind by himself or herself.
I hope I can make appropriate judgement to myself.
By doing so,I can be a better person,and not be tricked by flittering words and sweet traps.
Clarifying the essence of myself is very important.

# 2022-10-09 #
1. I was believe in Santa Claus' existence when I was a little boy. I think the story of him(or her) has the meaning of selfless and fair love.I I often help classmates about their academic problem no matter who they are.
2. I am believe in reincation,my grandfather gives me many books about this,one of the stories in these book is leave a deep inpression on me.
A lady likes to eat the tounge of the pig for a long time,and she comes out pig's ears in the end of the story.This story makes me to eat less meat.
3. I am believe in that everything have its souls.One of the stories about Bao Zheng is "a black bowl" has griveance and the pedestrian finding it,and then Bao Zheng cracks this case.
So I have the habit that I pray for the things accidentally damaged by me.

# 2022-10-13 #
Today's class is informative.
I learned exercising is better than I thought.
Exercising can take some immediate effect to your brain and elevate abilities such as concentration,memory and reaction in the long term.
Furthermore,it can delay the aging od the brain.
I didn't know exercising has these advantages before.
Another thing I learned is the importance of health-care.
As the saying goes,"Prevention is than cure".
Learning how to lower the chance to get disease is more ideal and economic than improving the technique to cure from disease.
We should always check our bodies' conditions.

# 2022-10-27 #
Today’s class is useful for me.
Although I don’t have depression,I am sentimental sometimes.
After listening to these Ted’s talks,I know more about how to deal with my bad mood.
I hope I can be more optimistic in my life.

# 2022-11-03 #
Some thoughts for achieving happy and fulfilling life.
first,set goals.
These goals may be hard but they are not impractical.
You can set short-term and long-term goals.
When you make effort to realize them,if these goals come true or not,you will gain harvest.
Second,record what you did.
It's a good way to examine your life and it gives you a direction to change.
You can do it by different means - writing diaries or taking some photos/videos.
The last,be careful to your health.
Regular exercise,suffcient time to sleeping,eating healthy diet etc.
If you are not health,there are many things you can't do.

# 2022-11-10 #
Climate change is an important issue,but I once thought it is distant for me.
This thought is not right,it is close for all of us.
Everyone should take responsibility for our environment.
Maybe what a person can do is limited,but gathering everyone’s efforts,that can be a powerful force.
So not to be stingy to make a change for our planet.
I also learned some tips for arranging the content of ppt from today’s class.

# 2022-11-17 #
It’s nearly impossible to live alone. 
Many things can be easily done by team work,but unifying everyone’s opinion is always difficult.
It is important for us to find a method to reach consensus based on respecting others and the freedom of speech.
We should check what’s the essence of the problem to avoid redundant discussion,then everyone takes a brief description for solving the problem. 
Discussing the strengths and weaknesses of these opinions.
Finally,we integrating them to a  suitable method for the situation at the moment.