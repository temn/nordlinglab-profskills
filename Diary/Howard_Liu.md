# 2023-01-07
* The very last class, I was up all night editing the video.
* And trying to export it for the first hour of the class.
* I think we did ok, a decent job.
* still got some little thing to fix though.

# 2023-01-05
* Happy new year to everyone. Yeah ~~~
* I asked professor if he could tell us the final presentation will be in a video form earlier, well..
* Turns out he did! On the 7th lecture, but only stating it as "a 3min video", not somthing that could go viral.
* A presentation for 3 min is very differnt from a viral video, which require some effort planning how to tell our story.
* I really liked this course. Hope there's more chance to take professior's other courses in the future. 
* I hope everyone in the class to pass every course this semester, may everyone have a nice time over winter vacation, and may all be wealthy and well in the future. Good luck, people!

# 2022-12-29
* Oh my God, we need to almost start from zero for the final presentaion.
* We thought it's going to be a video presentation, we've already finished 50% of it.
* Professor could've told us a couple weeks prior, so we can use our extra time for that, now none of us has the time for making a video.
* We are all going to stay up all night doing that next Friday, feels awful.
* Ops, yeah yeah I forgot my obituary, as long as it's not "student who... "
* tbd

# 2022-12-22
* Oh no. I just learned that we should do a 7 days diary last week, should've catch up with classmates last week.
* Now I'll go back as far as I can remember and try to fill in last couple days.

---

#### Let's get working:
* ***2022-12-18***
	* Unsuccesful:
		- Why I felt this way:
			1. I didn't finished the things I planned today.
			2. I forgot to buy some fruits in the market this morning.
		- Things to do differently:
			1. Set an alarm when watching show in case I got too focus and forgot about time.
			2. Write down a shopping list and take it with me so I won't miss anything anymore.
* ***2022-12-19***
	* Successful:
		- Why I felt this way:
			1. I manageed to convince a classmate to joined my team for a project next semester.
			2. I am happy that everyone is happy with there choices, 
		- Things to do differently:
			1. No I couldn't think of anything that can be done better during the process.
			2. Maybe don't do this kind of stuff too frequently.
			3. Took me quite some time, but the satisfaction was worth the run.
* ***2022-12-20***
	* Successful:
		- Why I felt this way:
			1. It's Winter solstice on Thursday, so I went to Supermarket early and made sure there were tangyuan in stock.
		- Things to do differently:
			1. I will keep think ahead of time.
* ***2022-12-21***
	* Unsuccesful:
		- Why I felt this way:
			1. Did't finished the study/work progress I planed yesterday.
			2. I went to classroom this morning only to find the class was changed to online cus the professor got Covid.
		- Things to do differently:
			1. Spend less time enjoying my dinner when theres a moutain of work wating.
			2. Check Moodle more frequntly, maybe I'll notice the annoucement before going to class.
* ***2022-12-22***
	* Unsuccessful:
		- Why I felt this way:
			1. I planned to finish fluid mechanic homeworks before midnight but I fell asleep before that.
		- Things to do differently:
			1. I am going to wake up early and start my work early before going to class.
* ***2022-12-23***
	* Succesful:
		- Why I felt this way:
			1. It's a sunny day, and I got some solid sleep last night. Felt great.
		- Things to do differently:
			1. Maybe I should start planning my sleep as well.
* ***2022-12-24***
	* Unsuccessful:
		- Why I felt this way:
			1. I again didn't manage to finish all the worked planned today, something came out and interupted the schedule.
		- Things to do differently:
			1. I'll try and plan a bit more space in the plan.
* ***2022-12-25***
	* Succesful:
		- Why I felt this way:
			1. It's a sunny day and I had a greaet meal with some classmates.
		- Things to do differently:
			1. It's so great I'm a bit overwhelmed.
			2. I should probablly start to for meditate 20 minutes to cool down before studying.

---

# 2022-12-15
* I'm feeling really sick this week, so didn't go to class. 

# 2022-12-08
* Our group project has already started,
* If everything goes to paln we will have all the data we need the the next two weeks.
* Turns out it wasn't too difficult to convince people trying veggie diet.
* I feel lucky to have good team memers who finish their job on time and are willing to discuss openly. 

# 2022-12-01
* Today's task was interesting mostly because it gave us the opportunity to talk to different people in our class.
* It feels pretty cool to learn together like this.
* And we get to meet professor's son.

# 2022-11-24
* Another group present, seemed like a lot of works await.
* We've shift our goal to finding veggie.
* good luck to us

# 2022-11-24
* I looked for my phone twice, first being trying to look up some word in the dictionary.
* Second time for checking the clock cus I wanted to pee. 
* Today we went through a few presentations, didn't get to ours unfortunately.
* I think ours is pretty good.
* And a few videos we didn't have the time to finish.

# 2022-11-17
* I forgot to write diary again, it's 24 today.
* Midterm starts kicking in.
* this week we cover reduction of co2 and noise polution.
* I like how people can kind of discuss with the people on stage in class.

# 2022-11-10
* We watched a few presentations, I think ours is pretty good.
* Maybe just a few minor improvements.
* And a video after class dismissed.
* This week was so vanilla, no test, no drama, nothing sh8ty and an extra day off?
* It's the calm before the storm. I know not what the future hold.

# 2022-11-03
* It was quite intresting to hear that people would choose to actually leave the depressed friend(not sure they were joking or not). 
* It's understandable if they want to draw a line to protect themselve, but really? I felt sorry for the depressed one, they were probably not your friend.
* "Just mind me own businies, when the day end, I just close me eyes and sleep like a baby."
* And a interesting Ted talk about choises, maybe we weren't choosing afterall.

# 2022-10-27
* Today we have online class, it's a bit lackluster, professor is stiil in quarantine.
* The internet seemed pretty bad on the other side, making it difficult to hear what the professor was saying.
* We watched three videos about depression, I totally agree that we shouldn't take the matter lightly.
* Had a few friends and myself experienced depress state before, good company is really the key.
* Mind is a complicated thing, hopefully hamanity can understand it better in the near future.

# 2022-10-20
* No class todsay.
* Busy midterm weeks.

# 2022-10-13
* Knowledge is "Well-justified true belife"-Plato.
* Looks like the professor is leaving the country for next week.
* Today's Ted talk told us exercise is quite important for brain health.
* Seems like we need to exercise more to stimulate our brain in order to stay focus and study more efficiently.

# 2022-10-06
* The first physical class is amazing. 
* We get to know a lot of different countries’ economy situation in today’s course.
* Use exponentials to make the graph more readable, especially for minor changes between datas.
* The Ted talk this week was quite inspiring, having a person who personally been in a hate group spoke about how people are capable of changing. 

# 2022-09-29
* Actually, economy is one of my possible choice of college majors before.
* I think learning economy is learning how human will behave and react in certain situation instead of just learning numbers and statistics.
* the task's data long time ago is so hard to find on internet.

# 2022-09-22
* too bad only three teams can present their work per week.
* It's important that we all know how to distinguish fake news and stats
* What I imagine my future will look like 10 years from now:

---

In 2032, we live in a time when information has blown craters on the Earth. I live in Toronto, Ontario, Canada as a UX engineer. 
The slow rise of the metaverse gave my job more influence than ever. I now design UIs and sometimes work as a front-end engineer for my company's new VR experience. 
By a "new VR experience," I mean a partially decentralized world that people will be able to live inside. 
Since it is still in development, you can imagine it as a means to connect people's shops, personal spaces, games, or whatnot with the infrastructure and tools we build.

Although the unemployment rate is lower than ever, the number of homeless men seems to have increased as my salary has not. 
The World Happiness Report 2032 shows an almost-consistent drop in happiness levels (Cantril ladder) as the world population has reached 8.5 billion. 
I used to expect governments would set more laws for ruling the online world and restrict monopolies like Google and Amazon, but it has not happened yet. 
Nevertheless, governments worldwide have been encouraging people to install solar panels. Thanks to this action and other green movements, global warming has slowed down significantly. 
Ultimately, the world has not changed much other than some improvements, optimistically speaking.

---

# 2022-9-15
* Today we have a basic run-down of how the class would operate henceforward.
* There are so many people in this class & lots of people are still settling down.
* Hopefully previous experience from Sci-info class would help me in this course.

---

#### Here are some thoughts on Steven Pinker's ted talk
* Quite inspiring, will definitely share if I heard someone saying the world is ending, again.
* I think these few sentence summed up how we can help improve the world we live in pretty well.

	> ...in understanding our tribulation and woes, human nature is the problem, but human nature channeled by
	>enlightenment norms & institutions, is also the solution.	
	>Admittedly, it's not easy  to replicate my own data-driven epiphany with humanity at large.
	
* We need more individuals who knows:

	1. the importance of knowledge (in general) and 
	
	2. to-learn
	
* But one might refuse to learn thus never know how to learn and will never progress.
* Every problem will take some time to solve, just be patient. 

# 2022-9-08
* First class.

# Diary for Pro-Skill class.