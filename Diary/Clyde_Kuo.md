# Diary

This diary file is written by Clyde Kuo in the course Professional skills for engineering the third industrial revolution.

## 2024-09-13

- Unfortunately, I'm not there yet.

## 2024-09-20

### Things I learned

- Learned about conflicts, and I think that the introduction to different view of people (harmony, conflict) really helps. By using this framework, I can navigate conflicts more easily.

- The chart about different types of conflict behavior is something that no one had ever taught me before. I understand that problem solving involves should satisfy both party involved, and avoiding is the worst.

- I also learned more about Git and Bitbucket, which is quite similar to GitHub.

- When the professor is talking about disruptive technology, I thought of AI and it's potential to replace a lot of workers.

- Wright's Law is also something interesting, it's more complicated than Moore's Law.

- Pinker's view on the world being a better place is quite weird hearing about it at first, the world is such a horrible place, child death, wars, famine, disasters. But his explanation that the world is already getting better is something that I'll remember. I'll have a more optimistic view.

## 2024-09-27

### Things I learned

- I learned more about SDGs (Sustainable Development Goals) in the course, and it's really interesting to see that the world is really getting better. However, according to the predicted worst scenario, it is really bad. But it's really saddening to see that most of the goals by 2030 are still very far from reaching.

- I also learned about how to cite links correctly, the recommended way is by using APA format.

- In the video about how to seek truth, I find the following points mentioned in the video:

  - objectivity
  - sensationalism
  - social media is not fact checked

- However, I think that it's hard to implement the just-filter-the-crap algorithm, there's some technical difficulty. Also, it might be criticized as being _1984_-like.

- The three way to spot fake statistics Mona Chalabi mentioned is:
  - Can you see the uncertainty?
  - See myself in data.
  - How was the data collected.

### Tasks 1

As an 18-year-old chemistry major, I envision a future where I apply my knowledge to promote sustainability and environmental stewardship. In ten years, I see myself living in a city like Copenhagen or Vancouver, renowned for its green initiatives, where I will work as a chemical researcher or environmental chemist. My focus will be on developing eco-friendly materials and reducing industrial waste while collaborating with organizations and industries to combat climate change. I will also personally embrace sustainable living practices, such as minimizing my carbon footprint and participating in community efforts instead of relying on others for change.

#### Trends Shaping My Future

1. **Sustainability and Circular Economy:**  
   The shift towards a circular economy emphasizes reusing and regenerating resources rather than the traditional “take-make-dispose” model. I will be involved in developing biodegradable materials and chemical processes that convert waste into reusable resources.

2. **Green Energy Transition:**  
   The global move from fossil fuels to renewable energy sources like solar and wind will be crucial. My work will focus on enhancing energy storage solutions, researching efficient batteries and carbon capture technologies, ensuring these innovations are both scalable and cost-effective.

3. **Climate Change and Environmental Restoration:**  
   As climate change escalates, I anticipate that environmental restoration will become a priority. I aim to work on projects addressing soil and water remediation and creating chemical processes that clean up pollutants and restore ecosystems. This will drive me to specialize in environmental toxicology and green chemistry.

4. **Technological Integration in Chemistry:**  
   The advancement of AI, machine learning, and robotics will revolutionize research methodologies. I expect to utilize computational models for simulating chemical reactions, which will enhance efficiency and allow me to tackle complex environmental issues with greater precision.

5. **Global Collaboration and Policy Changes:**  
   The recognition of worldwide environmental challenges will foster international cooperation and stricter regulations. My work will require staying informed about these policies and collaborating with global teams to achieve sustainability goals.

By 2034, I foresee my career being deeply rooted in addressing these environmental challenges through chemistry, driven by the trends of sustainability, energy transitions, climate action, technological advancements, and international collaboration, ultimately contributing to a healthier planet.

## 2024-10-03

Sadly, there's a typhoon.

## 2024-10-10

We celebrated Taiwan's National Day and had a day off.

## 2024-10-17

### Things I learned

In today's lecture on the actual workings of money, I came across several key insights that challenged my previous understanding of monetary systems. These concepts helped me realize the central role of banks in shaping the economy and how monetary policies can influence financial stability and growth. Below is a summary of the major takeaways:

- **Loans Creating Money**: Banks create money by issuing loans, which increases the money supply. This process impacts interest rates, inflation, and economic growth. Unproductive credit, like subprime mortgages, can lead to financial crises, while responsible lending boosts the economy.

- **Transactions in the Economy**: The economy is made up of transactions where people use money or credit to buy goods, services, and assets, which drives economic activity.

- **Printing Money**: Central banks can print money as a tool to stimulate economic growth. While risky in some cases (e.g., Zimbabwe, post-WWI Germany), it can be useful when done correctly.

- **Deleveraging**: Reducing debt in an economy, or deleveraging, affects economic growth and stability, often linked to financial crises.

Overall, understanding these concepts sheds light on how monetary policies and lending influence the broader economy.

## 2024-10-24

### Things I learned

- Extremism often stems from dissatisfaction with aspects of society, like economic struggles. This frustration can lead people to unfairly target marginalized groups, such as immigrants.

- The way to counter radicalization is by recognizing that those we label as “them” are also living human.

- Hatred is born from ignorance and separation.

- If someone presents extremist views (like deporting all immigrants), don’t engage in direct debate. Instead, approach with empathy. Showing compassion can sometimes help them see the humanity in others and reduce radical thoughts.

- Humans have a unique ability to believe in fictions, which can be a weakness yet also a strength.

- Science operates on theories that are strongly supported by evidence, not “proven” in the absolute sense. It’s just that counterexamples haven’t been found yet.

- Regular physical activity is excellent for brain health and cognitive function.

### Task 1

The first story is **currency**. I recognize currency as a construct; these pieces of paper we call money are not inherently valuable, yet I can trade them for the goods and services I need to survive. Just last week, I dined at a pricey restaurant with my "student family," racking up a bill of about 400 NTD. I’m grateful to be a freshman and have them cover the tab; otherwise, I would’ve had to foot the bill myself.

The second story revolves around **nation**. While nations are often perceived as tangible entities, they are fundamentally fictional constructs that unite people but also create divisions. During a lecture last week, someone argued that Taiwanese individuals frequently discriminate against people from the PRC. They claimed we share a similar culture and should focus more on that rather than our political differences. Initially, I brushed it off as typical Chinese propaganda, but upon reflection, I realized how deeply I’ve been influenced by this fictional idea of nations, leading to my own irrational biases.

The third story I believe in is **success**. The definition of success is often tied to wealth, status, or achievements, but it's largely a narrative shaped by society. Recently, I saw a post on social media showcasing someone’s extravagant lifestyle and accomplishments. It made me question the idea that success equates to material wealth. I started a conversation with peers about what success means to each of us, realizing that it can also involve personal fulfillment, happiness, and making a positive impact.

### Task 2

Imagine a future where AI is steering society, liberating us from the relentless grind of work and **allowing us to finally pursue what we genuinely love**. In this world, AI takes care of all the heavy lifting, freeing us from the monotonous 9-to-5 shackles that have long dictated our lives. **Why should we sacrifice precious moments for colorful pieces of paper? Why do we feel the need to conform to a narrative of success that demands our time and energy, leaving us little for ourselves?** Instead, we embrace a life filled with creativity, exploration, and connection. Whether it’s painting, writing, or simply soaking in nature, the possibilities are boundless when robots manage the mundane.

In this liberated reality, the concept of money becomes absurd. Why stress about earning cash when everything we need is just... there? With AI ensuring that everyone has access to resources, we no longer have to sacrifice our time for those trivial pieces of paper. **It’s as if we wake up from a long, bizarre dream where we believed our worth was tied to how much we could earn or produce.** In this new paradigm, we come to understand that sharing and caring for one another is far more fulfilling than chasing after arbitrary goals set by society.

Education undergoes a radical transformation, too. Gone are the days of rigid curriculums designed to fit everyone into a one-size-fits-all box. Instead, we dive into **personalized learning experiences** tailored to our unique passions and curiosities. Want to delve into astrophysics or master culinary arts? The paths we take are determined by what ignites our enthusiasm, making education an exciting adventure rather than a chore. This newfound freedom allows us to craft lives brimming with joy and creativity, instead of merely surviving through obligatory tasks.

This future isn't just about new tech; it's about ditching the exhausting cycle we’ve been fed our whole lives—**the idea that work, status, and “success” define us**. Freed from these outdated rules, we actually get to live. Imagine spending your days learning, creating, or **just being present without the constant pressure to “make a living.”** We no longer bend our lives around narratives that force us to compete, stress, and scramble for things we don’t actually need.

Picture it: a world where AI has removed the need for us to obsess over survival. Money and status fade into the background as absurd relics of the past. And finally, instead of just surviving, we get to fill our lives with things that truly matter. It’s not just a utopia—it’s a future that cuts through the noise and nonsense to let us be who we actually want to be.

## 2024-10-31

There's sadly typhoons again, it's really bizarre to see them in November.

## 2024-11-07

### Things I learned

- In the lecture about depression, I learned that:

  - Depressed people are not defective; they just work in different ways.
  - Depression is often physical.
  - One thing that should definitely not be done is to ask depressed people to “just get over it.”
  - When talking to them, be empathetic and avoid trying to “cure” it.
  - Talking to them can be mentally straining, so setting boundaries is important.
  - Regular check-ins can be really powerful.
  - They can be sad and okay at the same time.

- **About suicide and depression:**
  - It's crucial to take any mention of suicide seriously and seek professional help immediately.
  - Depression is a complex mental health issue that requires understanding and support from loved ones.

## 2024-11-14

### Things I learned

In this lecture, I expected to focus only on depression, but I ended up learning a lot about green energy during a debate on nuclear power. I found out that the Taichung coal power plant produces a significant portion of Taiwan's electricity and that nuclear power is actually classified as green energy. It’s fascinating how nuclear power is often portrayed as dangerous rather than as a revolutionary energy source.

The government’s ongoing efforts to shut down nuclear power plants seem to stem more from fearmongering than from evidence-based concerns, which I find utterly ridiculous. People in Taichung are suffering from pollution caused by coal power, while functional nuclear power plants sit unused, gathering dust. I do understand the risks associated with nuclear power, especially after incidents like the Fukushima disaster, but dismissing nuclear energy entirely feels shortsighted. A single mishap could indeed devastate Taiwan, but proper safety measures could make it a much safer and cleaner alternative.

- We rationalize our decisions after the fact instead of truly understanding them. It's not self-knowledge but self-interpretation.

- **Key takeaway**: Knowing that you don’t fully know yourself is valuable.

- Non-work relationships are important if you want to stay at a company for a long time.

- Don’t be limited by what your professor provides.

- Use online resources like AI or YouTube to supplement your learning.

- Give your friends a hug when they’re feeling down.

- Don’t be afraid to stay at university longer if it helps consolidate your knowledge.

- Accept the feelings you experience—they’re valid and a part of growth.

## 2024-11-21

### Task - Misinformation and Moderation

The most important challenge in this discussion is dealing with the **subjective nature of definitions**, especially when it comes to concepts like **misinformation**. History is full of examples where ideas we now take for granted were once considered misinformation. Take **heliocentrism**: in the 1600s, the idea that the Earth revolved around the Sun was seen as dangerous misinformation. In fact, during **Galileo’s** time, advocating for this theory was not just controversial—it was deemed **heretical**. The **Catholic Church**, which had enormous power, condemned this idea, and spreading it was seen as a threat to **religious** and **social order**.

Another striking example of the subjective nature of misinformation comes from the field of **medicine**. In the 19th century, a **Hungarian physician** named **Ignaz Semmelweis** made a revolutionary discovery: **handwashing** could dramatically reduce the spread of infections in hospitals. Yet, many doctors at the time considered this idea to be unnecessary or even ridiculous. They resisted it, even ridiculed him, because it contradicted their established beliefs and practices. Despite mounting evidence supporting the practice, Semmelweis faced significant professional rejection and was eventually ostracized. It wasn’t until after his death, when others proved his theories correct, that handwashing became widely accepted in **medicine**. What seems like **basic knowledge** today was once fiercely opposed as misinformation.

These examples may sound like **common knowledge** now, but they illustrate a critical point: misinformation is **not a fixed concept**—it is shaped by the prevailing beliefs, ideologies, and attitudes of a particular time and place. What is considered **false or misleading information** today might have been seen as **truth** in the past, and vice versa. This makes defining and addressing misinformation incredibly tricky in any context.

Now, when we turn to today's **polarized political landscape**, the definition of misinformation becomes even more subjective. Take any controversial topic, and you’ll see it—what **Party A** considers **objective truth**, **Party B** might view as **misinformation**, and vice versa. The narratives and policies of each group are often seen as the absolute truth by their supporters. For instance, one group might believe that a particular policy is scientifically supported, while the other sees the same policy as a dangerous manipulation or a distortion of facts. This divide can lead to accusations of misinformation, manipulation, and brainwashing on both sides. The result is a cycle of misinformation and counter-misinformation, with each side asserting its own version of the truth.

This creates a paradox for **social platforms** trying to navigate this complex landscape. If they choose to ban content from one side—say, **Party A**—they will likely be accused of infringing on **freedom of speech** and silencing certain viewpoints. However, if no action is taken, and potentially harmful posts from Party A continue to spread, **Party B** and its supporters might accuse the platform of enabling the spread of dangerous misinformation. In other words, any decision to moderate or not to moderate can be viewed as biased, unjust, or even oppressive by one group or another. It's a **lose-lose situation** when trying to apply a one-size-fits-all approach.

One possible solution to this problem is **shadow banning**. Instead of outright deleting or removing posts, shadow banning reduces the visibility of content flagged for misinformation or harmfulness without notifying the creator. For example, if a post is deemed to be spreading false information, the platform could reduce its reach by making it less likely to appear in search results or recommendations. This would allow users to continue sharing their views without completely removing the content, and it would also limit the spread of potentially harmful or misleading information. However, shadow banning is controversial. Critics argue that it could be a form of censorship, and there are concerns about **transparency**—users would not know why their posts are being suppressed, leading to distrust and accusations of manipulation.

So, what’s the real solution? The answer lies in finding a **middle ground**; we all have to sacrifice some degrees of freedom to ensure the greater good—a balanced approach that protects freedom of speech while minimizing harm. **Moderation shouldn’t be an all-or-nothing choice**. Instead of resorting to shadow banning or outright content removal, platforms could take a more **nuanced approach** by combining several strategies.

For example, one option could be **flagging posts** that are deemed questionable, but rather than removing them outright, the platform could provide **disclaimers** or links to verified information. This way, users can still engage with the content, but with added context to help them make more informed decisions. This strategy ensures that **freedom of expression** remains intact while also promoting responsible consumption of information. **Transparency** is key here—platforms could disclose how and why they flag content, ensuring users understand the rationale behind moderation decisions and feel that the process is fair and justified.

Additionally, **user-generated context** can play a significant role. Allowing users to add context or clarifications to posts that might be misunderstood or misinterpreted could help prevent the spread of misinformation. For example, if a post contains potentially misleading or false information, users who are familiar with the subject can add comments or links to credible sources that provide more accurate perspectives. This not only encourages **collective responsibility** but also fosters a **community-driven approach** to information verification.

This kind of model has been seen in platforms like **X (formerly Twitter)**, where users can sometimes correct or flag misinformation, allowing for greater user engagement in content moderation. By integrating **peer-based corrections** and allowing additional context to be layered on top of flagged content, the platform can balance **freedom of speech** with **responsible content moderation**—empowering users to make better-informed decisions without completely stifling their ability to express diverse viewpoints.

Another approach, while it may sound cliché, is mentioned not due to a lack of creativity but because it actually works: **promoting digital literacy** and **critical thinking**. Instead of relying solely on automated algorithms or human moderators to sift through content, platforms could take a proactive role in educating their users. Offering resources that teach how to **spot misinformation**, **verify sources**, and **recognize biases** would empower users to make informed decisions independently.

**Customizable filters** and algorithms could also give users the power to control their own experience on the platform. This would allow them to adjust the way they interact with content, ensuring they get the level of moderation or context that suits them best. For instance, some users might prefer seeing content with added context or disclaimers, giving them extra information about the reliability of the source. Others might want to take a more hands-off approach and prefer to filter out flagged posts altogether, creating a cleaner, less cluttered feed.

By offering these **customizable options**, the platform can respect personal autonomy while still tackling misinformation in a way that aligns with individual preferences. This helps strike a balance between **freedom of expression** and **responsible content management**—without forcing users into a one-size-fits-all model.

In addition, **open algorithms** could also play a role in promoting **transparency**. If users know how the platform's algorithms are deciding what content to show, they can make more informed choices about how to engage with posts. **Open algorithms** allow users to understand the factors that influence the content they see. For example, if an algorithm is prioritizing content based on a certain bias or filter, users can better recognize why certain posts are pushed to the top of their feed, and adjust their interactions accordingly. This level of transparency not only builds trust with the user base but also gives them the power to shape their experience, making the platform feel less like an automated machine and more like a space where their preferences matter.

Ultimately, the goal is not to silence all controversial views but to create a platform that balances **freedom of speech** with **responsibility**. By allowing users to make informed decisions, providing context, and embracing transparency, platforms can empower users while protecting against the dangers of misinformation. The key is **moderation** that respects **autonomy** and promotes **informed participation**—without sacrificing one for the other.

### Things I learned

- After watching the video on Big Data, I found that people often blindly follow it for a few reasons: it is rooted in math, it sounds objective, and it gives the illusion of control and predictability in an unpredictable world. However, it isn't that at all. **It is simply opinion embedded in code instead of a completely neutral and unbiased source of truth.**

- **The power of "emission-free" electric cars fundamentally comes from the energy source used to charge their batteries**. While the cars themselves produce no emissions during operation, **their true environmental impact depends on how the electricity that powers them is generated**. If the electricity comes from renewable sources like solar, wind, or hydroelectric power, the vehicle can genuinely be considered "clean." However, if the electricity is generated from fossil fuels like coal or natural gas, the **emissions are simply shifted upstream to the power plants**. Furthermore, the production of batteries, which are essential to electric vehicles, involves mining materials like lithium, cobalt, and nickel—processes that can cause significant environmental damage. Thus, the power of electric cars being labeled "emission-free" relies heavily on the energy infrastructure supporting them and the sustainability of their manufacturing processes.

- Governments shouldn't just implement regulations without support to the companies.

## 2024-11-28

### Things I learned

After watching the video "The Fall of Empires: Rome vs. USA," the key takeaway for me was the harmful consequences of currency debasement, especially when used to fund wars and excessive public spending. When a government prints more money to cover its expenses without creating real value to back it up, the currency’s value inevitably decreases, leading to inflation. This not only reduces the purchasing power of the population but also erodes trust in the financial system over time.

Fiat money, which isn’t backed by physical assets like gold or silver, may provide governments with short-term flexibility, but it comes at a long-term cost. It essentially allows governments to control the economy by manipulating the money supply, but this often results in economic instability. Historically, nations and empires that relied on fiat currency, such as the Roman Empire and more recently Weimar Germany, experienced severe financial crises as their currency lost value.

Sound money, like gold and silver, has stood the test of time because of its intrinsic value and scarcity. Unlike fiat money, which can be printed endlessly, precious metals cannot be artificially created, making them resistant to inflation. Gold and silver have historically been trusted as reliable stores of value and safeguards against economic turmoil.

In conclusion, the lesson from history is clear: currency debasement may offer short-term solutions, but it undermines long-term economic stability. Sound money, like gold and silver, remains a more sustainable alternative, providing a foundation for trust and value that fiat money simply cannot replicate.

## 2024-12-05

### Things I learned

In this lecture, I learned about planetary boundaries, and to me, the most surprising one is novel entities. I never knew that new things might have unknown properties. Most of the stuff on Earth has already existed for millions, or even billions of years, and their interactions are well developed. Organisms have had time to recognize them, and evolution has had the opportunity to adapt to and develop ways to break them down. But with novel entities like plastic, we are introducing something completely new to the planet at a rapid pace, which can have unpredictable consequences on ecosystems and organisms.

What struck me most is how novel entities bypass the natural cycles that typically regulate materials in the environment. Plastics, for example, don’t break down easily and instead fragment into microplastics, which persist for centuries. These particles are now found everywhere, from deep ocean trenches to the food we eat, with impacts on health and ecosystems that we are only starting to understand. Unlike natural materials, novel entities often accumulate faster than the environment can process them, posing a growing risk.

This boundary highlights how human activities are creating materials that are not just harmful but also completely alien to Earth's natural systems. It serves as a reminder that while innovation has its benefits, it also comes with responsibilities we cannot ignore.

## 2024-12-12

### Things I learned

Today, the weirdest thing I learned is that having one less baby can actually be extremely beneficial to the environment as an individual. Studies show that reducing the number of children you have can significantly lower your carbon footprint—far more than lifestyle changes like driving less or eating a plant-based diet.

At first, this idea felt strange to me. Having children is such a personal and culturally significant choice. For many people, it’s tied to happiness, love, family legacy, or even societal expectations. To think of it in terms of environmental impact almost feels... cold. It makes children, who are usually seen as a joy and a blessing, seem like liabilities to the planet. That’s a tough idea to process.

Is it really ethical to place a carbon footprint tag on a children?

## 2024-12-19

### Self evaluation task

- 2024-12-19 (Thu)
  - productive, successful
  - I felt productive because we stayed after class to discuss the project we are implementing. I felt successful because we figured out how to design the Plastic Bag Sharing Station.
  - Don't forget to take a break!

- 2024-12-20 (Fri)
  - unproductive, unsuccessful
  - I felt unproductive and unsuccessful because I heard loud music and got scared so much that I collapsed. After that, I couldn't study anymore.
  - Remember to bring earplugs.

- 2024-12-21 (Sat)
  - unproductive, successful
  - I felt successful because I studied calculus all day. But I felt unproductive because I only finished a chapter.
  - Skip some questions instead of writing them all.

- 2024-12-22 (Sun)
  - unproductive, successful
  - Like yesterday, I studied calculus, but my efficiency was abysmal. I felt successful but unproductive.
  - Allocate even more time to study.

- 2024-12-23 (Mon)
  - productive, successful
  - I finished a chemistry experiment report, studied calculus, and completed a presentation. I feel productive and successful.

- 2024-12-24 (Tue)
  - productive, successful
  - I had a discussion with my team members about the chemistry final report tomorrow, and I feel that we are fully prepared. I feel productive and successful.
  - Don't panic during the presentation.

- 2024-12-25 (Wed)
  - productive, successful
  - I presented, and it was a success. I feel productive and successful.
  - Take a break tomorrow!

### 5 Rules to success

1. Focus on efficiency, not effort
2. Balance work and rest
3. Prepare presentations thoroughly
4. Prevent distractions
5. Ask classmates for help

## 2024-12-26

### Self evaluation task

- 2024-12-26 (Thu)
  - productive, unsuccessful
  - I presented in the course today, and it was a disaster. I stuttered a lot and ended up reading almost entirely from my script because I misinterpreted the instructions. I thought we were supposed to summarize the rules to success we learned specifically from last week’s diary. Seeing almost everyone else successfully present their 5 rules to success felt defeating.
  - When working on an assignment, always read the instructions carefully.

- 2024-12-27 (Fri)
  - unproductive, unsuccessful
  - I don’t have classes today and went home yesterday. At home, I ended up doing practically nothing.
  - Even when you’re at home, make time for studying.

- 2024-12-28 (Sat)
  - unproductive, successful
  - I attended a school activity about relationships and learned a lot. The teacher was very down-to-earth and shared some helpful tips. However, I didn’t work on my test preparations, which made me feel unproductive. I felt too tired after the activity.
  - Prioritize studying after activities to stay on track.

- 2024-12-29 (Sun)
  - unproductive, unsuccessful
  - I studied today, but I ended up procrastinating by playing games and doomscrolling. It feels unproductive because I didn’t accomplish much.
  - Try rewarding yourself with fun after completing your tasks.

- 2024-12-30 (Mon)
  - productive, successful
  - I studied calculus and physics today, which made me feel productive and accomplished. In the evening, I exercised and noticed a beautifully clear night sky. I even recognized Orion’s Belt among the stars.
  - Make exercising part of your daily routine again tomorrow.

- 2024-12-31 (Tue)
  - productive, successful
  - I felt productive today because I spoke to the dorm about setting up a sharing station, and they agreed to let us place it near the trash cans. I also studied calculus.
  - Don’t hesitate to ask for help or permission—you might be surprised by the support you receive.

- 2024-01-01 (Wed)
  - productive, unsuccessful
  - I struggled to study today because the main library was closed for the holiday. The only open study space was overcrowded, and I felt suffocated. My roommate was noisy too, gaming with his friends, but I couldn’t really blame him since it’s a holiday. Still, I managed to study calculus and focus on some math topics I enjoy.
  - Balance study and rest better tomorrow to recharge effectively.

## 2024-01-02

### Things I learned

#### Isirika video

- Isirika emphasizes our shared humanity, encouraging us to see people as human beings first, rather than labeling them based on circumstances such as disabilities, refugee status, or being immigrants.

- The concept advocates for unity and compassion by recognizing the humanity in everyone.

- While supporting women and girls is important, I believe assistance should come from an egalitarian viewpoint. Helping women should not mean prioritizing them above others but ensuring fairness and equality in the support provided.

#### University

- A classmate brought up the question of whether we attend university to pursue our own goals or to conform to society's expectations of success. At that moment, I realized I was a coward for following orders rather than doing what I truly wanted. I didn't raise my hand because I conformed to society's idea of success instead of pursuing my own interests.

- Instead of chasing credits and scores, I could focus on learning what truly excites me, like 3D modeling, programming, game development, or even category theory.

### My obituary

**In Loving Memory of Clyde**

Clyde's soul was a beautifully imperfect blend of contradictions, passions, and eccentricities. He lived with purpose, fully embracing life rather than merely getting by. At the age of 77, he passed away peacefully in his sleep, leaving behind a life as rich and nuanced as he was.

Born in Kaohsiung, Taiwan, Clyde was the kind of person you might not notice at first but, once you did, left an unforgettable impression. His insatiable curiosity and thirst for knowledge led him to explore a wide range of interests—from mathematics and programming to his dedication to chemistry, where he authored papers that made a small yet meaningful impact on the world. Beyond his academic pursuits, Clyde prioritized balance, making time for family, friends, and the moments that brought him joy and connection.

Clyde's legacy isn’t defined by grand gestures or hollow platitudes but by the quiet, lasting influence he had on those around him. He inspired thought, sparked laughter, and made people feel understood. While his absence will be deeply felt, his presence will endure through the lives he touched, the ideas he championed, and the genuine, humane way he lived.

Rest easy, Clyde. You mattered.

## 2024-01-09

### Things I learned

Today is the final lecture. Inspiring videos of people making real changes to the world rather than just surface-level improvements were all around us. I initially believed that everyone in this course was just interested in passing and wasn't paying attention. However, I had a change of heart after seeing the video.

In this course I think that I improved my English speaking and the ability to improvise whatever thing I want to say without preparation. I often hope that the professor won't pick me to answer the question, and I think that I can be more active.

I started thinking about why people didn't notice our action. We don't have a set time for a large event that draws people to this cause, I reasoned. We just passively place plastic bag sharing stations around the campus. We spent so much time yet saw so little change.

### Attendance score booster

1. **World Demographics**  
   Watch short educational videos or read articles from accessible sources like [Our World in Data](https://ourworldindata.org/) or [Population Reference Bureau](https://www.prb.org/).  

2. **Using Demographics to Explain Engineering Needs**  
   Join engineering-focused communities on Reddit, like [r/Engineering](https://www.reddit.com/r/Engineering/), to explore how demographic trends influence design.  

3. **Planetary Boundaries**  
   Follow environment-related subreddits like [r/Sustainability](https://www.reddit.com/r/Sustainability/) or [r/Environment](https://www.reddit.com/r/Environment/) to understand more about the environment, which also include planetary boundaries.  

4. **Current Economic System and Resource Distribution**  
   Follow discussions on economic systems in subreddits like [r/Economics](https://www.reddit.com/r/Economics/) or [r/AskEconomics](https://www.reddit.com/r/AskEconomics/) to understand more about the economy from a global perspective. Additionally, consider exploring subreddits like [r/BasicIncome](https://www.reddit.com/r/BasicIncome/) or [r/socialism](https://www.reddit.com/r/socialism/) to learn about alternative resource distribution models and their implications on society.  

5. **Future Visions and AI Implications**  
   Participate in AI and machine learning discussions on subreddits like [r/MachineLearning](https://www.reddit.com/r/MachineLearning/) or [r/Futurology](https://www.reddit.com/r/Futurology/) to gain insight into the future and know more about AI. Additionally, watch [CS50's Introduction to Artificial Intelligence with Python](https://youtu.be/gR8QvFmNuLE?si=sVQDeeUoLstwcX0d) on YouTube to know how to actually do AI programming and implementation. This will provide a practical understanding of AI concepts and applications in various industries.  

6. **Data and Engineering for Health**  
   Work on projects that require analyzing healthcare data to propose engineering solutions. Use tools like Python or R for data visualization and analysis of health trends.  

7. **Social Relationships and Survival**  
   Participate in social experiments or studies related to psychological well-being. Create small group projects or community service initiatives that emphasize building strong social connections.  

8. **Depression and Mental Health Issues**  
   Join mental health-focused communities like [r/mentalhealth](https://www.reddit.com/r/MentalHealth/) or [r/selfhelp](https://www.reddit.com/r/selfhelp/) for shared experiences and discussions.  

9. **Optimal Algorithm for Finding Life Partners**  
   Join subreddits like [r/Relationship_Advice](https://www.reddit.com/r/Relationship_Advice/) to understand more about ways to find a partner and potential red flags.  

10. **Questioning Claims to Avoid “Fake News”**  
   Use fact-checking platforms like [Snopes](https://www.snopes.com/) or [FactCheck.org](https://www.factcheck.org/) to verify the accuracy of information before sharing it with others. Additionally, consider diversifying your news sources to gain a more well-rounded perspective on current events.  

11. **Time Series Data Analysis**  
   Work on projects involving predictive modeling using time series data (e.g., stock market trends, climate data). Use platforms like [Google Colab](https://colab.research.google.com/) or [Jupyter Notebook](https://jupyter.org/) for coding and visualization.  

12. **Collaborative Problem-Based Learning**  
   Participate in interdisciplinary group projects that tackle real-world problems, such as sustainable development goals or technology implementation in developing regions. Ensure a balanced approach where every member contributes.  

13. **Social Skills for Professional Success**  
   Engage in role-playing exercises, networking events, or collaborative workshops designed to improve communication and interpersonal skills. Focus on team-building activities and leadership development.  

14. **Workplace Culture and Performance**  
   Read case studies and articles from sources like [Harvard Business Review](https://hbr.org/) or [LinkedIn Learning](https://www.linkedin.com/learning/) that discuss how culture impacts performance.  
