# 2022-09-15 #

* I have learned many useful skills and different viewpoints toward things through today's lecture.
* I think this course must be the one that amazed me.
* But I still can't get the main point of today's lecture, so I'll go through the slides again.

# 2022-09-22 #
I think my future would be a very carefree life, I will buy a house in a high-class apartment in Banqiao and live a carefree life with my girlfriend or wife. 
Ten years from now, I will be a digital integrated circuit designer in MEDIATEK, I love this job which is a high-paid position and my generous boss give us a lot of welfare.
For my girlfriend’s part, she may be a traditional Chinese medicine doctor and she is a workaholic for the sake of reaching our life objective “ financial freedom “. 
Therefore, she always get off duty late and I need to pick her up home, so we necessarily need a comfortable car.
I think there are not any change can affect my lifestyle severely, but I think changes must exist to alter the general situation, which may affect my situation. 
First of all, I think the decline of integrated circuit industry may affect me the most, nowadays every electrical product need a chip to make them work, if one day in the future the amount of chip requirement drop or the technology were being replaced by AI, I may lose my favorite job which can seriously upset my life.
Second, I think the pandemic change may affect my life as well, because I love to travel around the globe to eat many local food and to see many marvelous scene.
Third, I think the climate change also affect me a lot, owing to my allergic nose I am sensitive to be in an extreme hot or extreme cold environment, so the global warming may more or less affect my life.
The fourth and fifth may be the exchange rate and the stock market, I think most of us will do some investments to maximize our property, in this case, exchange rate and stock must be vital to us.
Take the America FED for instance, recently FED claim authorities will lift rate up to six times, which exactly made Taiwan stock market hit a record low, for me, I must change my lifeway to spend the bear market.
To sum up, I think it is too difficult to follow the blueprint of our life but I believe only when we get prepared will chances appear, so I will go all out to prepare myself waiting for the chance come.

# 2022-09-29 #

* This lecture made me know more about financial system and its rule.
* I'm looking forward to meet teacher and clssmates in person.
* Since homeworks in ME is getting harder, so I almost forget to review the lectuer slides.

# 2022-10-06 #

* The first physical class is amazing because I this course is my first course taht everyone communicate in English.
* Some of the classmates presented their presentation and diary.
* Watching TED is a good chance to learn English listening comprehension.

# 2022-10-13 #

* This lecture is amazing because classmates' presentations are so funny and I learn some new policy and future offered by polititians.
* This lecture teacher asked me to come up with a knowledge learned in school, I answered Newton's second law and we start to issue.
* In this course, I always can learn some new things by listening to some Ted talk and I would appreciate that teacher can discuss with the talks.

* Last week, the stock market in Taiwan dropped severely that made me confused with my concept of investing.
* I thought I can make a perfect decision of when to buy TSMC stock, but it turned out that i was totally wrong, I think maybe I need to consider some international factors.
* Last week, I went to outlet to buy some new clothes, at the beginning I think people would not buy much in outlet, however, everyone carried more than three bags which amazed me a lot.
  I am looking forward to earn much money to buy what I want.
  
# 2022-10-27 #

* This lecture made me know more about some knowledge of living a healthy life via classmates' presentations.
* Today's lecture is a little bit trivial because the survey was weird.
* Listening to Ted's talk is my favorite part of the class.

# 2022-11-02 #

* Today's lecture is very nice, I have learn many thing that should be prepared before graduate.
* This lecture made me know well about the difference those professional skills and technical skills I learn from NCKU.
* Today I'm a little bit sleepy because of many exams.

# 2022-11-10 #

* Today we enjoy many presentations with various topics and I love it.
* I think the this bigger prensentation is better than those weekly presentation owing to it completion.

# 2022-11-17 #

* Today I learned about how to reach a consensus with my group members.
* I think this lecture was very great because how to cooperate with others plays a vital role in our life.
* Today I was very sleepy because many exams and tasks has to be done.

# 2022-11-24 #

* Today's lecture is very useful and make me learn many things.
* I like those presentations becaues by listening to others presentations I can know others thought of an issue.
* I almost fall asleep because the light is dark.

# 2022-12-08 #

* Today's lecture was great and as the same many groups present their works.
* Last week I forgot to write the diary and Professor's son is cute though.
* My super group have some trouble reaching consensus about final project.

# 2022-12-16 #

* Successful and unproductive.
* Successfully present my final report but its too cold to be concentrated.
* Do not make excuss for laziness.

# 2022-12-17 #

* Unsuccessful and unproductive.
* Still cannot conquer the cold.
* Need more perserverance.

# 2022-12-18 #

* Successful and productive.
* I have overcome the temperature and start to be more energetic.
* Should maintain the perserverance and improve efficiency.

# 2022-12-19 #
 
* Unsuccessful and productive.
* Today my experiment was bad but I do many things efficiently.
* Don't be upset about some unsuccessful.

# 2022-12-20 #

* Successful and unproductive.
* Today I have 8 lectures, so I feel tired after dinner and fell asleep.
* I think I can go shower to make me feel energetic.

# 2022-12-21 #

* Successful and productive.
* Today I finish many homework and do some pratice about the skill of programming.
* I must keep doing things like this.

# 2022-12-22 #

* Successful and unproductive.
* Today I have to deal with my final project about auto control lecture, so I feel tired after dinner and fell asleep.
* I think I can go shower to make me feel energetic.

# 2022-12-23 #

* Successful and productive.
* Today I finish many homework and do some pratice about the skill of programming.
* I must keep doing things like this.

# 2022-12-24 #

* Successful but unproductive.
* Today is Chrismas Eve and I feel energetic and excited but I cannot concentrate on my subject.
* I must improve my self-control ability.

# 2022-12-25 #

* Successful and productive.
* Today is Chrismas and I almost finish my final project.
* I need to make my efficiency higher.

# 2022-12-26 #

* Successful but unproductive.
* Today I get two final exams and I feel nervous due to our final presentation is around the corner.
* I must get some rest to make me more energetic.

# 2022-12-27 #

* Unsuccessful and productive.
* Today I stay at lab for almost 8 hours and I meet with professor to discuss my personal project.
* I have to make my time more useful.\

# 2022-12-28 #

* Successful and productive.
* Today I finally finish the last presentation and I can focus on my final exams.
* I feel great and I need to study harder with my final exams.

# 2022-12-30 #

* Today's lecture is very awesome because all of the groups present their project.
* I am well-prepared for my presentation today but owing to limiting time I cannot present to everyone.
* I feel great after this lecture and teacher do make us think more about or action.



