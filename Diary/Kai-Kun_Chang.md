This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.
                                                                                                                            
This diary file is written by Kai-Kun Chang E14116841 in the course Professional skills for engineering the third industrial revolution.
                                                                                                                                        
# 2024-09-12 #
* The first lesson introduced us to the concept of the Third Industrial Revolution, emphasizing its transformative impact on society and technology. We explored how this era reshaped industries and 
* highlighted the evolving role of engineers in addressing modern challenges, from sustainability to innovation.
* During today's class, we also had an exam, which tested our understanding of the basic principles that will serve as the foundation for this course. Additionally, the class structure requires us to 
* engage in frequent teamwork, as we are tasked with preparing and delivering presentations with our teammates almost every week. These presentations provide an excellent opportunity to enhance our 
* collaboration, communication, and problem-solving skills while diving deeper into the course topics.
                                                                                                      
# 2024-09-19 #
* in the second class we watched a TED talk : "Is the world getting better or worse? A look at the numbers"
* the video really imspired me to reflect about the world I'm live 
* then we had some groups presented their report
* I found out that the professor is strict about the templates of presentation 
* teacher gave us some tips to improve our presentation 
                                                       
# 2024-09-26 #
* I think in the future I will be a engineer working on internal combustion engine or that kind of stuff in any possible country, since I love those "traditional" machines very much. 
* Though at that time the world might be an EV trending place, I still want to work on the field associated with vehicles which have internal combustion engines.
* The major reason is that I really love those things, and the other reason is that I think there must have some situations need the power or the capability of ICE.
* And at that time engineers who works on ICE might be really rare so I'm considering to dedicate into it.
* Then the five current trends of change which I think will affect me the most are 1:Artificial Intelligence and Automation, 2:Climate Change and Sustainability,
* 3:Advancements in Renewable Energy and Green Technologies, 4:Healthcare and Biotechnology Advancements, 5:Globalization and Cultural Exchange.
* Among above tends I think 1:Artificial Intelligence and Automation will affect many people, but as an engineer I might learn to use it as an useful tool or assistant.
* 2: Climate Change and Sustanbillity is a long-standing issue, and it may influence many industries in many aspect.
* Take automobile industry for examole, dut to climate change and the conscious of sustainbility rise up, there more and more environmental regulations, this is one of the reason why ICE maight decline and eletric motors rise up.
* So there might be a lot more oppotunities in EV industries than in the "traditional" automobile field.
* 3:Advance on Renewable Energy and Green Technologies, this trend is a little bit related to 2: Climate Cgange and Sustainbility, since the environment is getting worse and worse on earth.
* people are finding ways to protect or make it become worsen slower as much as possible, this lead to the falls on ICE, so that make it harder to survive.
* 4:Healthcare and Biotechnology Advancements, this will make us live longer and longer which I think is really good, since I thought it is priceless to be alive.
* 5:Globalization and Cultural Exchange, this will let us have much more chance to interact or cooperate with foreiners, so as may be can make some technologies devlope faster.
                                                                                                                                                                      
* Summarise the different views in the sources, focus on the news "Iran says a senior commander killed in an Israeli strike together with Hezbollah leader Nasrallah".
* I read three different sources, first one focuses on the broader profile of Abbas Nilforushan. 
* It reports that he was killed in an airstrike in Lebanon, alongside Hezbollah leader Hassan Nasrallah. It highlights Nilforushan's involvement in suppressing protests in Iran,
* especially following the death of Mahsa Amini, and his role in supporting Syria's Bashar Assad.
* Second one confirms Nilforushan�s death in an Israeli airstrike on Lebanon. The emphasis is on the fact that Israel carried out the strike targeting Hezbollah's leadership.
* It is a straightforward announcement with no further details, focusing on his position as deputy commander for operations in the Iranian Revolutionary Guards.
* Third one echoes the Iranian media, reporting that Nilforushan was killed in an Israeli strike on Beirut.
* The report also highlights his advisory role to Hezbollah in both military and diplomatic matters, showing his strategic importance in the region.

# 2024-10-03 #
* We took a day off due to the typhoon situation, which disrupted our usual class schedule. Despite this, my teammates and I stayed productive by working on a presentation video remotely.
* In the presentation, we studied and introduced several interesting topics related to money, currency, and finance. We explored the historical evolution of currency, the role of 
* financial systems in modern economies, and the ways these concepts impact our daily lives. Preparing the video required us to do some research, discuss various perspectives, and ensure 
* that our explanations were clear.

# 2024-10-10 #
* Double ten holidy

# 2024-10-17 #
* Today's class was packed with insights that deepened my understanding of economics. We explored critical topics such as money, currency creation, income distribution, and recessions. 
* Each concept felt like a vital piece of a complex puzzle, revealing how interconnected our economic systems are. 
* I felt like we had learnd many things about an really complex system, one concept at a time.
* We also examined recessions, which underscored the fragility of financial systems and the devastating effects they can have on communities. This made me appreciate the importance of sound economic policies.
* Finally, our conversation about credit bubbles introduced crucial lessons about the risks of excessive borrowing. Understanding these risks has given me a newfound appreciation for the need for regulation in financial markets.
* Overall, today's class was an enlightening experience that motivated me to further explore these important economic concepts and their implications for our lives.

# 2024-10-24 #
* I thought the story in the movie "A.I. Artificial Intelligence" may come true in the future, it alert me of not being too dependent on those AI tools. It may not become 
* completely like waht in the movie, but if we use it too much, it may make us lose some of the abilities that we suppose to have.
* At this moment of my life, I still don't believe that there is heaven or hell after we dead, I thought there might be just completely nothing after the end of our lives.
* So this makes me more appreciated about my life, and makes me more motivated on what I should do.
* Altough I don't believe in god or sth like that, but I'm afraid of ghost, for no reason. I think it might be a good thing to be fraid of ghost, since it may sometimes 
* stopping me to do some stupid or dangerous things.
* Terry Gou : picture a tech-driven, economically thriving Taiwan. His focus is on expanding high-tech industries, increasing trade with China for economic gain, and 
* streamlining government for efficiency. This could mean more tech jobs and growth opportunities, but also a stronger economic tie to China, with less emphasis on
* social welfare.
* 1:Stick to a regular sleep schedule.Consistent sleep improves heart health, reduces inflammation, and enhances cognitive function, according to studies in "Sleep Health".
* 2:Do appropriate exercises twice weekly. Research in "The American Journal of Preventive Medicine" shows that it reduces mortality risk and improves muscle and bone health.
* 3:Focus on whole foods like fruits, vegetables, lean proteins, and whole grains. Research published in "The BMJ" shows that diets rich in whole foods reduce the risk of heart disease, obesity, and type 2 diabetes. 

# 2024-10-31 #
* Typhoon day-off.

# 2024-11-07 #
* 1.How to recognize when a friend is depressed?
* common indicators:  
* Emotional changes:1,Persistent sadness, irritability, or mood swings.2,Expressions of hopelessness, worthlessness, or guilt.3,Loss of interest in activities they previously enjoyed.
* Behavioral changes:1,Withdrawing from social interactions and isolating themselves.2,Increased dependence on substances (alcohol, drugs).3,Reduced energy or excessive fatigue.
* Physical symptoms:1,Changes in appetite (eating too much or too little).2,Disturbed sleep patterns (insomnia or excessive sleeping).3,Unexplained aches or pains.

* 2.What to say to a depressed friend?
* prioritize empathy and non-judgmental support:  
* Express concern and care、Offer reassurance、Avoid dismissive comments
* Listen without judgment:I:Allow them to share their feelings without interrupting or offering unsolicited advice.II:Acknowledge their pain with phrases like, "That sounds really hard."

* 3.How to help a friend with depression?  
* Emotional support:Be patient and understanding; healing takes time.Check in regularly to show you care and that they’re not alone.Encourage them to express their feelings without fear of judgment.
* Practical support:Help with day-to-day tasks if they feel overwhelmed (e.g., cooking, errands).Suggest joining them in activities, such as a walk or watching a movie, to gently reintroduce joy.
* Encourage professional help:Suggest that they consult a therapist, counselor, or doctor. Offer to accompany them if they feel nervous about seeking help.
* Know your limits:Understand that you’re not a replacement for professional help. Support them, but encourage professional treatment when necessary.Take care of your own mental health to avoid burnout.
* ----------
* 1.How to Spot and Help a Depressed Friend?:
* Spotting Depression in a Friend:#Look for emotional signs: Persistent sadness, irritability, or hopelessness.#Notice behavioral changes: Withdrawal from social activities, decreased energy, or loss of interest in hobbies.
* #Identify physical symptoms: Changes in sleep patterns, appetite, or unexplained aches.
* Helping a Depressed Friend:
* #Start the conversation gently、#Offer emotional support:Validate their feelings with empathy.#Encourage professional help:Suggest therapy or counseling.
* #Provide practical assistance:Help with errands, offer to accompany them to appointments, or check in regularly.
* 2.Claims from the guide:Depression can impact anyone, often in unseen ways, which is why it’s essential to prioritize mental health and support one another.

# 2024-11-14 #
* Today, we had a meaningful discussion about depression that really resonated with me. One key insight was the realization that people suffering from depression don’t always show typical signs of sadness or withdrawal. 
* In fact, some individuals who seem happy and smile often may be struggling internally. This understanding challenged my previous assumptions and highlighted the complexity of mental health.
* We talked about how depression can manifest in various ways, which made me reflect on my interactions with friends. It reinforced the importance of being attentive and compassionate, as mental health issues 
* can be hidden behind a cheerful exterior. Our conversation also emphasized the need to treat those dealing with depression with care and kindness. Simply being present and offering support can make a 
* significant difference in someone’s life. I left class feeling inspired to be more mindful of my words and actions, fostering an environment where individuals feel safe to express their feelings.
* Overall, today’s discussion deepened my understanding of depression and motivated me to advocate for mental health awareness, helping to break down stigma and support those in need.

# 2024-11-21 #
* Today’s lecture explored the potential and pitfalls of algorithms and big data. We discussed how algorithms, while powerful, can unintentionally perpetuate biases, such as a company’s system that reinforced 
* gender inequality in promotions by relying on outdated trends. The lecture highlighted the need for constant ethical oversight to ensure these tools promote fairness rather than reinforce old injustices.
* Our discussion also touched on the importance of diverse perspectives in addressing these challenges and the value of balancing critical thinking with empathy. It reminded me of the responsibility to consider 
* the human impact of technological decisions while staying open to learning from others. 

# 2024-11-28 #
* In today’s class, we engaged in a fascinating experiment focused on phone addiction that truly opened my eyes to my own habits. We were asked to leave our phones at the front desk and track how often 
* we instinctively reached for them throughout the session. Initially, I felt a wave of anxiety at being separated from my device, but as the experiment progressed, I became more aware of just how often 
* I felt the urge to check my phone. This experience highlighted the pervasive nature of phone addiction and made me reflect on how technology can distract us from meaningful interactions and learning.
* In addition to the experiment, Professor also discussed historical dark ages, drawing parallels to our current situation regarding the climate crisis. He warned that ignoring the urgent issues surrounding 
* climate change could lead us into another dark age, where societal progress is stifled by environmental degradation. This perspective was both alarming and thought-provoking, prompting me to consider 
* the importance of collective action and responsibility in addressing climate issues. It reinforced my belief that we must prioritize sustainable practices and advocate for policies that protect our planet. 
* Overall, today’s class was a compelling reminder of the interconnectedness of technology, history, and social responsibility, encouraging me to be more mindful in both my personal habits and my approach 
* to global challenges.


# 2024-12-05 #
* In today’s lecture, we compared newspapers from different countries. We then worked in larger groups to analyze headlines and their political biases. Since we didn’t have many similar headlines, we focused on Korea.
* We also learned about planetary boundaries, particularly rising temperatures. If temperatures keep increasing, global GDP could drop by up to 38% by 2050. Currently, temperatures rise by 1.2°C per year and could reach 2.7°C without action. 
* This could cause a temporary temperature spike before cooling. We also discussed heat sinks like icebergs, which help regulate temperature. As they melt, they worsen trends like rising sea levels. While an overshoot is likely, we can still try to minimize the impact. 
* The priority is to address rising temperatures and other planetary boundaries to avoid extreme weather lasting 50-75 years.

# 2024-12-12 #
* In today’s class, some groups presented their final project plans. While many had developed innovative ideas, some teams were unsure about their next steps. This uncertainty highlighted the necessity 
* for deeper reflection to ensure that their projects align with their overall goals and objectives. It was a valuable reminder of the importance of clarity in our intentions and the need for continuous 
* reassessment as we progress.
* We also watched Jeremy Rifkin’s documentary, "The Third Industrial Revolution," which provided a compelling overview of the shift towards renewable energy and digital innovation as essential components 
* for creating a sustainable economy. The documentary emphasized the urgent need for systemic change to effectively address environmental challenges, illustrating how interconnected our economic practices 
* are with ecological sustainability.

# 2024-12-20 #
* successful + unproductive
* Because I did a great job on my fluid mechanics test 、 I sleept basically the rest of the day since I studied it till maybe 3 AM yesterday.
* Study the exams/tests earlier.

# 2024-12-21 #
* successful + unproductive
* I hung out with my friends this day evening, and that's happy.
* I only do 1 homework after I got up at noon.
* Get up earlier.

# 2024-12-22 #
* successful + unproductive
* I had a friends came to Tainan from Hsinchu, so I took him to some famous place in Tainan.
* Since I didn't do anything related to schoolwork.
* but I thought I'd try my best.

# 2024-12-23 #
* unsuccessful + productive
* Since the weather today is really bad so it's an unsuccessful day.
* but I finished many homework today, I thought it could be productive.
* find a way to make the weather better

# 2024-12-24 #
* unsuccessful + unproductive
* The weather was still a tragedy today, what's worse is that I got sick today, so did nothing...
* try hard not to get sick

# 2024-12-25 #
* successful + productive
* It's successful since today is Christmas, also I finish some homework in a short time, that's productive.
* keep it up!

# 2024-12-27 #
* successful + unproductive
* I celebrate Christmas with my friends and this makes me feel successful, but I only study a little for the up coming exams, this makes me fell unproductive.
* Try to find a better balance of celebrating Christmas and working on the exams.

# 2024-12-28 #
* successful + productive
* I work on a final project of a general course with my teamates for a whole afternoon, and we finish it productively.

# 2024-12-29 #
* unsuccessful + productive
* I study some of the final exams today, but still think it is not enough, and feel nervous for it.
* Try to relax but also take good care of progress.

# 2024-12-30 #
* successful + productive
* I got a good grade on the midterm exam of mechanical design which turn the paper back today, also I finish the final exam of experiment course.
* keep going!

# 2024-12-31 #
* successful + unproductive
* I celebrate the new year eve with my friends tonight, we gathering together, having some nice meals, and have a really happy new year eve.

# 2025-01-01 #
* unsuccessful + unproductive
* I slept till afternoon since me and my friends we haved fun till early morning, and I were so tired that can't make it to get up earlier.
* Remind myself don't forget to study after having fun.
                                                        
# 2025-01-09 #
* Obituary reflection :　I would like to write some things, thoughts, that I would like to let all my close friends, my families, my children... know.
* I would like to let them know that I were grateful for having them in my life, and also telling them to keep doing themselves, keep pursuing their own dreams,
* or some words like that. 
                          
* Action Plan: Preparing for an English Proficiency Test
* Step 1: Create a Study Plan, daily schedule: Dedicate at least 2 hours daily to practice. Monday-Friday: Focus on specific sections (e.g., reading on Monday, writing on Tuesday). Weekends: Take practice tests and review mistakes.
* Step 2: Practice Regularly. Reading: Read articles, and academic texts. Summarize key points. Writing: Write essays and practice writing under timed conditions. Use feedback to improve grammar and coherence.
* Listening: Listen to podcasts, TED Talks, or news in English. Practice note-taking. Speaking: Practice speaking with classmates or friends.
* Step 3: Take Mock Tests. Schedule full-length mock tests. Simulate real exam conditions, including timing and environment. Analyze mistakes and focus on weaker areas.
* Step 4: Monitor Progress. Keep a journal to track improvements in vocabulary, grammar, and test scores. Adjust study plan based on progress and areas of difficulty.
* Step 5: Prepare for the Test Day. Gather necessary documents (e.g., ID, registration confirmation). Sleep well the night before and eat a healthy meal on the day of the test.


