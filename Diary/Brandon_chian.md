# 2024/09/19
* At the start of the class, the teacher introduced the concepts of "Harmony View" and "Conflict View." In my opinion, small conflicts within a team’s ideas can actually help refine those ideas. The Conflict View allows us to expose both the pros and cons of an idea early on, making it easier to improve. However, if the conflict arises from personal differences or personality clashes, my suggestion would be to either leave or switch teams.
* As for GitHub, it was the first time I had heard about it during your class. After class, I took a quick look at the website and found that it functions similarly to a more advanced version of Discord. It includes features such as Version Control, Repositories (Repos), Collaboration, Community & Open Source, and more.
* When the teacher reviews our presentations, they often provide feedback. One point that stood out to me is that references placed at the end of a presentation are rarely noticed. If there are important charts or data, the source should be included directly below them, making it easier for others to immediately access detailed information. This also prevents people from forgetting about the reference when they can’t find it right away. Another suggestion was to simplify links by removing unnecessary parts. For example, instead of:
https://app.cwa.gov.tw/web/obsmap/typhoon.html#:~:text=
It’s better to shorten it to:
https://app.cwa.gov.tw/web/obsmap/typhoon.html.
# 2024/09/26
3ways to spot a bad statistics

* Can you see uncertainty？
* Can I see myself in data？
* How was the data collected？

 **Task 1**:

In ten years, I have a clear vision of my future, shaped by my current academic and career plans. As a student from Malaysia, I am currently studying and working abroad, aiming to secure a stable and competitive income in the coming years. Specifically, my goal is to start with a monthly salary of 60,000 TWD and, as I gain experience and improve my skills, to reach over 150,000 TWD per month within 8 to 10 years. This financial stability will give me more options for my future life.

I am pursuing a bachelor’s degree in mechanical engineering, and I expect my future job to be closely related to my field of study. This could be in Taiwan’s aviation or engineering industries, both of which have great growth potential and can provide a steady income. With the rapid pace of technological advancement, these industries will present new opportunities, and I will need to keep learning new skills to remain competitive.

* In the next ten years, five key trends will likely have a significant impact on my life. The first is technological advancements. Whether in mechanical engineering or aviation, innovation in fields like automation, artificial intelligence, and the Internet of Things will reshape how work is done. I will need to continually update my skills to adapt to these changes in the workplace.
* Secondly, global economic fluctuations will play a major role in shaping my career and financial prospects. Both Taiwan and Malaysia’s economies will be influenced by global markets, which in turn will affect my career progression and salary growth. I will need to build my wealth during favorable economic periods and be prepared for any downturns to ensure long-term financial security.
* Third, the rise of environmental awareness will affect both my work and personal life. As the world moves towards sustainability, the field of mechanical engineering will focus more on designing energy-efficient products and technologies. This will not only influence the direction of my career but also shape my personal choices, such as living in an energy-efficient home and adopting a more eco-friendly lifestyle.
* The fourth trend is the increasing flexibility of work arrangements. The future of work is likely to be more flexible, with remote work or hybrid models becoming more common. This flexibility will allow me to work from Taiwan while collaborating with global companies, giving me the chance to gain international experience while maintaining a balanced work-life schedule.
* Finally, a growing emphasis on personal health and well-being will be crucial. Over the next decade, I plan to prioritize my mental and physical health, ensuring that I stay fit and energetic to tackle the challenges of both work and personal life.

In summary, my goal for the next ten years is to remain professionally competitive and achieve stable career growth. By the time I reach around 50, I plan to return to Malaysia to enjoy a relaxed life, where I can grow vegetables, go diving, and perhaps open a small shop, living the life I’ve always dreamed of.


 **Task 3**:

-**Right Source (U.S. News)**:
  - Emphasizes that the EU tariffs on China-made EVs are aimed at protecting European EV manufacturers from unfair competition.
  - Suggests that the decision responds to the pressure of Chinese EVs flooding the European market, aiming to shield the local industry from potential harm.

 **Center Source (Reuters)**:
  - Focuses more on providing neutral reporting about the details of the upcoming vote, mentioning that the EU will vote on October 4 to finalize tariffs
  on China-made EVs.
  - The report mainly covers the event itself without diving deeply into the background or implications.

 **Left Source (Asharq Al-Awsat)**:
  - Highlights the potential impact of tariffs on EU-China trade relations, particularly the global market's response.
  - Stresses how China might retaliate and whether this move could escalate economic tensions between the EU and China.
These sources reflect different perspectives on the same event: the right-leaning source focuses on protecting local industry, the center gives a factual account, and the left emphasizes the broader global economic and political implications.

# 2024/10/17
 **Reflection: Money Creation in the Modern Economy**:
 
* Today, I learned how loans create deposits, revealing the process of money creation. Initially, I thought banks only lend from existing deposits, but this video explained that when a bank approves a loan, it simultaneously creates a new deposit in the borrower’s account. This means loans do not just transfer money but actually generate new money, increasing liquidity. It was interesting to see the role banks play in money supply, and I now understand the importance of regulatory policies in preventing excessive lending.

 **Reflection: How the Economic Machine Works**:
 
* I watched Ray Dalio’s video and gained insight into how credit drives the economy. I learned that credit stimulates demand and affects interest rates and growth. However, excessive credit use can lead to deleveraging, triggering a recession. Dalio explained that economic cycles are inevitable, and managing these cycles depends on effective government and central bank policies. This video helped me understand the complexities of the economy and the importance of sound policy management to avoid crises.

 **Reflection: Richard Werner: Today’s Source of Money Creation**:
 
* Richard Werner’s video gave me a deeper understanding of how money is created today. I learned that banks generate money primarily through credit, not just cash reserves. Without proper control, this system could cause asset bubbles and inflation. Werner proposed an interesting solution: focusing credit creation on productive investments to support growth while avoiding inflation. This provided me with new perspectives on economic policy and taught me how credit can be managed more effectively for stable development.

Conclusion
These three videos gave me valuable insights into money creation, credit, and economic cycles. I realized the importance of managing credit carefully and implementing sound policies to support economic growth. Sustainable development, I learned, is only possible through a balanced approach that mitigates risks while ensuring growth.

# 2024/10/24
 **Claim**: 
Transitioning from a fee-for-service healthcare model to one that compensates healthcare providers for maintaining patient health will lead to better health outcomes and reduced healthcare costs.

 **Testable Hypothesis**: 
Implementing a healthcare payment system that rewards providers for keeping patients healthy will result in a measurable decrease in the incidence of preventable diseases and lower overall healthcare expenditures within a specified timeframe.

 **Supporting Evidence**:

i）Preventive Care Effectiveness: Studies have shown that preventive care, such as regular screenings and lifestyle interventions, can significantly reduce the incidence of chronic diseases. For example, early detection of cancer through blood tests for circulating tumor DNA can lead to higher survival rates. 


ii）Value-Based Insurance Design (V-BID): This approach aligns patient cost-sharing with the value of services, encouraging the use of high-value care and discouraging low-value care. V-BID has been associated with improved health outcomes and cost savings. 


iii）Accountable Care Organizations (ACOs): ACOs that adopt value-based payment models have demonstrated improved patient outcomes and cost reductions by focusing on preventive care and efficient management of chronic diseases.

 **Three Rules for Living a Healthy Life:**

1. **Regular Health Check-ups and Monitoring**  
Advanced technologies like DNA testing and miniaturized sensors can help identify disease risks early, allowing preventive measures to be taken to reduce the likelihood of developing long-term illnesses. Regular health assessments keep track of your body’s condition and ensure long-term wellness.

2. **Maintain Consistent Exercise**  
Scientific research indicates that regular physical activity improves cardiovascular health, reduces stress, strengthens the immune system, and protects brain function. These benefits help prevent chronic diseases and promote overall well-being. Establishing an exercise routine supports both physical and mental health.

3. **Balanced Diet and Proper Nutrition**  
A nutritious diet is crucial for disease prevention. Eating plenty of vegetables, fruits, whole grains, and lean proteins reduces the risk of chronic conditions.Developing healthy eating habits strengthens the body and contributes to a long-lasting, healthy lifestyle.


# 2024/11/07
This class gave me a profound understanding of the importance of mental health. Watching the TED Talks taught me to be more observant and listen with greater care, realizing that offering words of comfort isn't always necessary; sometimes, simply being there and showing genuine understanding can be incredibly impactful. I also learned how crucial self-care is, as constantly absorbing the negative emotions of others can take a toll on me, making it essential to seek professional support when needed. Overall, this experience emphasized the need to be there for others while also remembering to prioritize and protect my own well-being.

### **Self-Evaluation Task: 2024/12/19 to 2025/01/02**

---

#### **2024/12/23 (Mon)**  
**Feeling:** Productive, Successful  
- I completed all the homework tasks assigned by the teacher and went for a 5 km walk to start improving my vital capacity.  
**Reflection:** Regular physical activity can boost both mental and physical health.  

---

#### **2024/12/24 (Tue)**  
**Feeling:** Productive, Successful  
- I started preparing for the exam and made 35% progress.  
**Reflection:** Break large tasks into smaller, manageable chunks to stay on track.  

---

#### **2024/12/25 (Wed)**  
**Feeling:** Productive, Unsuccessful  
- I made 75% progress on exam preparation. However, due to the renovation noise next door, I took longer to study and ended up playing games after 4-5 hours.  
**Reflection:** Find a quiet place to study or use noise-canceling earphones.  

---

#### **2024/12/26 (Thu)**  
**Feeling:** Unproductive, Successful  
- I completed 95% of my exam preparation but lacked confidence in the remaining 5%. I also did some pull-ups for exercise.  
**Reflection:** Build confidence through practice and review.  

---

### **5 Rules for Success**  
1. Break tasks into smaller, achievable steps.  
2. Balance work, study, and rest.  
3. Create a quiet study environment to maintain focus.  
4. Celebrate achievements to stay motivated.  
5. Reflect regularly to improve and set goals.  
---

#### **2024/12/27 (Fri)**  
**Feeling:** Unproductive, Successful  
- I finished the exam and decided to enjoy the weekend.  
**Reflection:** Rest and recharge after significant tasks.  

---

#### **2024/12/28 (Sat)**  
**Feeling:** Unproductive, Successful  
- I worked from 9 a.m. to 5 p.m. and trained with my basketball friends in the evening.  
**Reflection:** Balance work and leisure for better well-being.  

---

#### **2024/12/29 (Sun)**  
**Feeling:** Unproductive, Successful  
- I worked from 9 a.m. to 5 p.m. and went to bed early to prepare for the next day.  
**Reflection:** Getting enough rest is essential for productivity.  

---

#### **2024/12/30 (Mon)**  
**Feeling:** Unproductive, Successful  
- I finished some planned homework, but I didn’t complete as much as I had hoped.  
**Reflection:** Stick to your schedule and minimize distractions.  

---

#### **2024/12/31 (Tue)**  
**Feeling:** Unproductive, Relaxed  
- I enjoyed a New Year’s Eve gathering and didn’t study much, but it was worth spending time with friends.  
**Reflection:** Allow yourself to celebrate important moments.  

---

#### **2025/01/01 (Wed)**  
**Feeling:** Relaxed, Reflective  
- I spent the day reflecting on my goals for the year and mentally preparing to return to Tainan.  
**Reflection:** Start the year positively with clear, achievable goals.  

---

#### **2025/01/02 (Thu)**  
**Feeling:** Productive, Successful  
- I resumed studying and made significant progress. I also planned my week ahead.  
**Reflection:** A well-structured plan leads to consistent success.  


Chen Shu-chu  
Chen Shu-chu is a veggie seller from Taiwan who gave away over NT$10 million to help people in need. Even though she came from a modest background, she worked tirelessly and lived simply to support others. Her story is all about her generosity and dedication to making a difference.  

Dato' Azhari  
Dato' Azhari, a famous Malaysian diver, has done amazing work for marine conservation. He’s at the forefront of ocean clean-up projects and coral reef restoration. His passion for nature and steady efforts have driven his success.
---



---




 