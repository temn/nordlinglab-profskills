# 2024-9-26
1.The 17 sustainable Development Goals (SDGs) issue,for example:No poverty, Zero hunger, Good health, Quality education
2.Less of percentage of percentage of population below the international poverty line
3.Some speech concerning how to cope wtih fake news, be objective

# HW  2024-9-26
What work will I do?  I think I will become a securities trader .Using AI model to do the trade in the market.

Five current trend that you affect you most  and their effect on later 10 years
1.AI, replace repetitive jobs, focus on personal experience.
2.SDGs, raise awareness of being eco-friendly, shorten the gap of the poor  and the rich , renewable energy growth.
3.Digital service, there is no physical keys or dollars and all of the keys ,payment,transactions can be used in the digital device.
4.Decentralized finance, cryptocurrencies become the daily use currency,
5.circular and sharing economy, there will be more sharing platforms like airbnb ,more resources can be shared in use, and people  will be less willing to buy the car .

# 2024-10-17
1.Amount of money and deposit is always in the trend of increasing
2.What is  quasi money?
3.Properties of money ,like fungible ,durable, portable, recognizable
4.Deposits and loan in Taiwan increases tremendously before New Year
5.Flat money
6.Bitcoin is not a better currency than Taiwan dollar
7.Bitcoin can only be used as currency in the country where their currency are more unstable than  bitcoin.
Salvador is the only country which regard the bitcoin as the official currency .

# 2024-10-24
1.Find your potholes, forgive yourself and walk out of it
2.Speech from classmate,reflecting on 'Is it necessary or worthy for us to hate the China people?'
3.Is it better for country to expand its continent,which means more people more chaos?
4.We actually lived in the fictional reality
5.How to define knowledge? 'well justifield true belief' from plato
6.Do the exercises  three or four times a week, at least 30minutes for once.

# HW 2024-10-24
Three ways to keep healthy:
1.Maintain deep friendships: Happier with their life and less likely to suffer from depression 
People with no friends or poor-quality friendships are twice as likely to die prematurely (risk is equal to 20 cigarettes per day)

2.Get plenty of sleep: Sleep also affects different parts of your immune system, which become more active at different times of day.
People who do not sleep enough may have a higher risk of Coronary heart disease,high blood pressure,obesity and stroke.

3.Drink water and stay hydrated: 
Key role in many of our body’s functions, including bringing nutrients to cells, getting rid of wastes, protecting joints and organs, and maintaining body temperature.

# HW 2024-10-24
Three fiction stories:
1.currency,I went to the PX mart last week.I usually pay by apple pay.But at that day. Their credit card machine is broken.So you can only paid in cash.Suddenly,I just 
realized that even though digital payment is convient but currency is more common to be used in Tainan.

2.Happiness,I just redefine what is my real happiness after reading a book about how to live in the better life.After focusing my feeling for a week.
There is a new explanation to my happiness.It's calm and no desire.When I take a rest,I will prefer doing nothing.Especially for no desire, I feel less stressed.
There is no need for me to chase after something.Just focus on the moment.

3.Logic,always be a logical person,when I had a meeting for debugging a project.The meaning of logic has strengthened in my mind.Fisrt ,you should definitely know what you are doing?
and ask some constructive questions. Only by doing that,you can be a real helper to the group and the community.

# 2024-11-7
1.When we get along with the people who is depressed or have depresion,the speaker recommend what we should do is only listening to them.

# 2024-11-14
1.Teacher said that we should give our friends a hug when meeting them,by doing this, we can give them the feeling of closeness and make them feel better.

# 2024-11-21
1.The diiference between different countries in the law and policy related to the carbon emissions.
2.When using some services without paying ,then we should be aware of where the money go. In the end , we might become the product, we are being sold.

# 2024-12-19
1.Interesting debate between capitalists, environmentalists and workers on the topics of why the inequality exits and why is the third industrial revolution needed.
2.Website called street dollar,we can see the income of family all over the world, how much do they need each month,it make me realize that I maybe have some power to improve their life by just donating a few dollars.

12/21六
a.unsuccessful and unproductive
b.I felt unsuccessful because of that I woke up late at 10. I plan to shave my eyebrow for bringing me luck.After waking up, I felt terribly hungry,so I ordered tremendous breakfast to eat.As I expected, I felt asleep after eating too much things .Then I woke up at 3pm.
c.I need to wake up earlier tomorrow and deal with the most difficult hw first.

12/22日
a.successful and productive
b.Wake up earlier.Finishing practicing the final exam of managerial mathematics and did some shopping online.Feeling chill and great.
c.I should spend less time on watching shopping websites.

12/23一
a.unsuccessful and productive
b.Take my mom to several places for dealing so many issues.But I did not learn something when waiting for her.
c.I should use the trivial time to learn something new or watch some beneficial articles

12/24二
a.successful and unproductive
b.Getting on the train just on time when back to Tainan.Having a good grade on statistics quiz.Playing volleyball in the evening and performing some great shots.
c.Even though playing volleyball is happy, I think that I should spend less time on it. Since after playing it ,both of body and mind would become exhausted.

12/25三
a.successful and productive
b.Finishing the final exam of math and it's not really difficult for me.Eat a good brunch.Go to the gym in the afternoon and feel awesome because I feel my muscle is growing bigger and Exchanging the Christmas gifts and meet someone special at night.Happy.
c.I think it's a good day.

# 2025-1-12 Suggestion on class
1.Know the Current World Demographics 
Spend 15 minutes daily reading demographic reports from trusted sources like the UN Population Division, World Bank, or Pew Research Center, and develop a clear understanding of population trends, distribution, and growth rates globally.

2.Use Demographics to Explain Engineering Needs
Analyze case studies where demographic insights were applied to solve engineering challenges (e.g., urban planning, resource allocation) and build the ability to link population trends with engineering requirements effectively.

3.Understand Planetary Boundaries
Enroll in a free online course like "Planetary Boundaries" on Coursera or edX, supplemented by readings from the Stockholm Resilience Centre, to grasp the limits of Earth's systems and how human activities impact them.

4.Understand Economic System Failures 
Watch videos or attend lectures on resource distribution  and reflect on these concepts by writing short summaries to gain insights into economic inefficiencies and inequities.

5.Be Familiar with Future Visions (e.g., AI)
Follow industry leaders on LinkedIn, subscribe to AI-related newsletters like OpenAI or MIT Technology Review, and engage in discussions to stay updated on technological advancements and their societal impact.

6.Enable Healthier Lives Through Data and Engineering  
Explore examples where engineering improved public health, like wearables or telemedicine, and summarize findings in a presentation to recognize the role of technology in enhancing life quality.

7.Social Relationships for Survival
Read studies or watch TED Talks about the impact of social connections and practice relationship-building by engaging more with peers or colleagues to appreciate and apply the importance of meaningful social bonds.

8.Mental Health Awareness
Attend webinars or workshops on mental health awareness and learn basic mental health first aid through programs like Mental Health First Aid (MHFA) to understand and empathize with mental health challenges, supporting a positive environment.

9.Algorithm for Partner Selection  
Study decision-making frameworks like the "optimal stopping theory" and reflect on its application in relationships and career to make informed personal and professional decisions.

10.Questioning Claims
Practice fact-checking claims using tools like Snopes or FactCheck.org and engage in critical discussions to develop skepticism and analytical thinking to counter misinformation.

11.Analyze Time Series Data
Take an online introductory course in data analysis with Python or Excel focusing on time series, and apply learning by analyzing simple datasets like weather or sales trends to gain practical experience in time series interpretation.

12.Collaborative Problem-Based Learning
Join a study group or collaborative project in your area of interest, using platforms like Slack or Trello for teamwork, to experience and improve team dynamics and problem-solving skills.

13.Social Skills for Success
Participate in networking events or workshops on communication and teamwork, and practice active listening and empathy in daily interactions to enhance interpersonal and professional relationships.

14.Workplace Culture and Performance
Study examples of organizational cultures (e.g., Google, Tesla) and their impact on productivity, and reflect on what aspects resonate with your values to understand how to contribute to and thrive in various workplace environments.


