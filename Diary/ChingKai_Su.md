# 2024-09-19 #

* We are having our first presentation ,I've learned a lot from the other's speech.
* The word type have to be corrected ,each type of word has its way to use.
* We have to add the reference under every charts or figures.
* The TED talk speaker's topic is quite attractive.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2024-09-21 #
* This week lecture is about fake news.
* learn how to spot a bad statistic. (3 ways: 1. Do you see uncertainty in the data? 2. Do you see yourself in the data? 3. How was the data collected?)
* I feel the second ted speaker talk too fast, so its hard to understand what she saying. but in the end I got the point.
* My team did not got a chance to present this week.

# Homework:Summary of what I think my future would be like after 10 years #

   Ten years from now, I envision a future that is shaped by several key factors, including my place of residence, my professional pursuits, and the prominent trends that will significantly impact my life. While it's essential to acknowledge that the future is uncertain and subject to change, I can speculate on how these aspects might evolve based on current trends and my aspirations.


   First,in the next decade, I expect to live in a more interconnected and sustainable world. The concept of smart cities will have advanced, incorporating cutting-edge technologies to enhance urban living. I envision residing in a well-designed, eco-friendly apartment or home within one of these smart cities. This urban environment will prioritize green spaces, efficient public transportation, and renewable energy sources, contributing to a cleaner and more sustainable lifestyle.


   Second,as for my professional life, I anticipate that the workforce will experience significant transformation. Automation and artificial intelligence (AI) will continue to advance, affecting a wide range of industries. To stay relevant and adapt to these changes, I will have transitioned into a role that combines creativity, problem-solving, and human interaction areas that are less susceptible to automation. Whether it's in a creative field, education, or a leadership role, my work will revolve around tasks that require emotional intelligence and innovation.


   Third,remote work will remain a prominent trend, further accelerated by the experiences of the COVID-19 pandemic. However, the nature of remote work will evolve, with more companies adopting hybrid models. Advanced virtual collaboration tools and augmented reality will enable seamless remote teamwork. I'll continue to work from my home office, collaborating with colleagues and partners from around the world, thanks to enhanced digital communication technologies.


   fourth,environmental sustainability will be a defining factor in my future. Climate change will continue to drive global efforts to reduce carbon emissions, leading to more sustainable practices in various aspects of life. I'll strive to minimize my carbon footprint by using electric vehicles, consuming locally sourced and plant-based foods, and reducing single-use plastic. Sustainable living will become not just a trend but a necessity for our planet's well-being.


   Fifth,the importance of health and well-being will be more apparent than ever. Advances in healthcare technology, personalized medicine, and data-driven health management will enable proactive health monitoring. I'll regularly use wearable devices that track my health metrics and provide personalized recommendations for diet, exercise, and mental well-being. Telemedicine will continue to grow, making healthcare more accessible and convenient.

   Overall, my future will be characterized by adaptability and resilience in the face of rapid technological advancements and global challenges. I'll embrace sustainable living practices, leverage digital tools for work and collaboration, prioritize my health and well-being, and continue to explore opportunities for personal growth and development. While the specifics of my future may vary, these overarching trends and values will guide my life in the coming decade, shaping a more interconnected, sustainable, and fulfilling existence.
   
# Topic: Bank of England to start unlimited bond purchases to stabilise market
* left : The Bank of England will not sell British government bonds into a dysfunctional market, but a sharp repricing that reflects changing fundamentals would be not be a reason to pause the programme.
* Center : Only stated out what did the bank do and it's cause so far.
* Right : The dramatic move came at 11am, as the Pound continued to fall and as top bankers had been meeting the Chancellor as he sought to defuse the economic crisis which he has sparked.

# 2024-10-17 #
* Professor talked about financial system today, and introduce some interesting facts about deposits and loans.
* Gold is often one of the assets that is considered to be the ultimate safe haven, which value usually doesn't fluctuate dramatically.
* If one bank's deposit-to-loan ratio is greater than 1, which implies that the bank has loaned out every cent of its deposit.
* It is the danger zone because it has no reserves to pay customers for demand deposits.
* It's interesting to learn that whenever Chinese's new year has come, the amount of money government held increased drastically
  due to the Chinese's envelope custom.
* I also learned a tricky word "fungible", which is something may be replaced by another equal part or quantity.
* Money is the typical instance that is fungible.
* The purchasing power of each currency has droped exponentially for the last hundred year.
* The inflation of currency has become so severe that I can't help wonder what can we afford to buy
  with a thousand dollars in the next or two decads to come, perhapes not even a drink.
* If the government keep letting the inflation sabotages our currency purchasing power, then we must publish a new currency.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.
* Every politician tells that we must need to fix this gap, and makes promises to their voters.

# 2023-10-24 #
* The presentation give by classmate which talking about hated between China and Taiwan really impressed me.

* Three frictional stories:
* people nowadays can easily change their apperance just by pressing bottoms in front of the mirror.
* The currency in Taiwan is no longer money, besides we spend our time for goods.
* one plus one equal three.

* The future offered by some famous politicians in Taiwan:
* The government shut down all the TV stations, the press freedom in Taiwan is no longer exist.

# 2024-11-09 #
* some people shared their experiences with depression and anxiety, to be honest it was a pretty relatable moment
* anxiety has a really heavy impact on college students, I got curious on how english speakers would have to do if they wanted pychologist support
* find purpose on life was already something I was aware, and I still chasing for one until this day, but it really is not a easy task
* we sit with our big group together today, we discuss about our big project next week

#  2024-11-16 #

*  Our group had a physical meet-up yesterday to discuss today's presentation.
*  To my surprise, all members showed up and the meeting went smoothly.
*  It was a very productive discussion! I can't wait to implement our action ideas.
*  In another note, I think it will be very empowering to see how our action affects actual peple.
*  The weather has been getting chilly these days, perhaps winter's finally here? I sure hope so.

# 2024-11-21
* I couldn't go to physical class today so I lost a part of the experience, professor propose a experiment related to our dependency of smartphones
* the groups discuss many ideas, I didn't had any expectations but some ideas were actually feasible and realistic
* regarding to noise problems from constructions in taiwan, it's indeed a problem and the authorities usually neglect these cases
* some groups also had the same ideas as my group like the use of EVs, it was interesting to see different perspectives on the same solution
* today's class was really relatable since I have many problems with anxiety
* the last ted talk that we watched was quite boring

# 2024-11-28 #
* The professor encourages us to form a more concrete action idea.
* In order to be concrete, we should include where, who, when, what, how, etc.
* I'm particularly interested in the presentation about sleep, cause I really want adequate sleep.
* The more chaotic the event is, the less you should follow it every few minutes.
* Verify the truth before spreading on social media.
* We have more freedom on the flow of information, but with that freedom comes responsibilities.
* The speaker suggests that multiple factors combined led to the destruction of the mediterranean societies around 1177BC, and the cities couldn't survive when one is destroyed because they are connected, or globalised. In nowadays world, the situation could be even worse should similar events happen, and I think it's important to strengthen the relationships between countries. It may be difficult for a single country to solve the problem, but together, human can have a better chance surviving.After watching the video, I think I will have to ask my boss to give me gold and silver rather than money as salary and build an undergroung vault to store them.

# 2024-12-05 #

* I've learned from the lecture about the planet boundary. It's quite surprising to me that except for issues frequently heard like climate change the nitrogen and phosphorous are overly consumed by us as well.
* I am not optimistic about what the world is gonna look like many years later if we don't make some changes. I really don't understand why this isn't an issue that people really care. The news I've read recently are mainly about the Isarel-Hamas conflicts. The wars initiated by stupid people are really distracting us.
* In the TED Talk about planetary boundary, the idea of threshold is interesting to me. We don't experience a very serious consequence now, but if we go beyond the threshold, there would be deadly consequences for us.

# 2024-12-19 #
* I've learned some more precise knowledges on how to plan things, including WBS, PERT, Gantt chart.
* The professor encourged us to focus on outcome. Indeed, if one don't even have an idea about what the outcome is going to be, it'd be impossible to finish a thing.
* After learning from lectures on issues regarding planetary boundary, I've understood that our planet is sicker than I'd previously thought. What we can do to solve this problem, based on scientific evidences, is surprising and kind of weird. If we have less children, can human survive? (In Taiwan, for example, most family nowadays have only one kid, if they reduce the number to zero, then there won't be next generation!)

# 2023-12-21 #

* 2024_12_20 Friday:  
 	A: Successful and productive  
	B: I felt both successful and productive becasue I finished 2 projects today that I needed to submit until the end of the week. Besides that I had enough time to hang out and enjoy the nice weather.  
	C: To be more productive tommorow I will try to wake up earlier than today, at least that should not be hard.  

* 2024_12_21 Saturday:  
 	A: Successful and unproductive  
	B: I felt successuful from the morning since I achieved my goal of waking up earlier today. Even though I woke up early, I spent most of the day doing nothing so I feel unproductive.  
	C: To be more productive tommorow I will set some specific goals and checkpoints for my assignments so that I can track my progress easier.  

* 2024_12_22 Sunday:  
	A: Successful and productive.  
	B: I felt more productive than on Saturday since I set some goals for my projects and successfully accomplished them.  
	C: To keep the level of productivity for tommorow I have set myself some new goals, although I plan to be less productive tommorow since it's Christmas.  
	
* 2024_12_23 Monday:  
	A: Successful and unproductive.  
	B: I felt successful since my day was great and I enyjoyed spending it with my friends. I was not productive since I didn't even do the small tasks I planned to do. I don't mind since today should be a holiday.  
	C: To be more productive tommorow I'll set some bigger goals and try to finish tasks I should've done today.  

* 2024_12_24 Tuesday:  
	A: Unsuccessful and productive.  
	B: Today I felt totally unsuccessful since my friends told me I did wrong tasks for homework. Apparently there is a similar undergraduate course to the one I take and on File Managing course website it was not really obvious that one folder is for graduate and the other folder is for undergraduate students. I'll have to do this again tommorow, properly.  
	C: To be more successful tommorow I'll check requirements twice before I start working on my assignments.  
	
* 2024_12_25 Wednesday:  
	A: Successful and unproductive.  
	B: I finished my homework so I felt successful. Even though I have exam tommorow I found no motivation to study for it. I hope it goes well.  
	C: To be more productive in general I'll relax and enjoy my last days on exchange with my friends. I probably did enough to pass all my courses here and I don't really care about the grades I get since they are not relevant for my studies.  

# 2024-12-27 #
* How to tie a tie: Windsor knot and half windsor(suitable for informal occasions) are simple and useful.
* The idea that we can randomly choose who to be our leader randomly is interesting, and I like it.
* However, I think there's a problem. It is that not everyone want to be in the parliament.
* The idea of verticle farms work may fine in cities in developed countries, but actually, a lot of people living in poor countries may not be able to access this kind of technology, and they are people who are truly threatened by hunger.
