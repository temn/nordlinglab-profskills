This diary file is written by James Tzeng E14086575 in the course Professional skills for engineering the third industrial revolution.

# 2023-09-24 #
* Its getting close to autumn, the weather is getting a little comfortable then weeks beafore.
*	From my point of view, the future is unpredictable. The reason is that I believe we are on the very edge of some technology breakthrough. Meanwhile, our generation is facing some severe problems that will possibly end our entire civilization if we do not take them seriously. 
*	Suppose we do survive the problem we are currently facing. In that case, I think I will be an engineer designing or maintaining machines. Although robots will replace many jobs with their general intelligence and precision, I can still do some jobs that require communicating with other human and translating their thought into the language robots can understand.
*	With AI, we don't need to code ourselves. Still, we need to understand the structure and how to correct the script if something goes wrong. Future programmers will need to learn the difference between AI models produced by different companies. 
*	Next, we can talk about the problem of where I will live in ten years. I am now living in an apartment with my friends in college. The reason I choose to live in this place is because it is near the campus, it only takes 5 minutes from my bad to the classroom.
*	However, with the 5G Internet, maybe we can do our job without actually being there. We can get everything done using our AR or VR device to control robots. In this case, we no longer need to live in urban areas, so I will probably live in the countryside because I like to be close to nature. I can go hiking on the weekend and relax in the forest.
*	Actually, I don't have my plane this far. I still wonder what job I will have and what kind of job fits me the most. Sometimes, I felt a bit lost because I didn't know what to do in the future, but I realized that I could manifest my own reality, I would be just fine.

# 2023-10-01 #
*I just came back from a three days trip with my parents.
*The view was magnificant, there is a lake on the top of the mountain.
*The sun was raising when we came and the air was cold but clear. 
*After reaching the top, we can saw the highest mountain in our view.
*It is trually rememberable moon festable holiday.

# 2023-10-09 #
*This week, I was prepared for my automatic control test on Thursday. 
*However, The typhoon came in, and my test was postponed to next Thursday.
*The good news is that I have more time to prepare. 
*The bad news is the test will be more difficult because it combines the test later. 
*If I want to graduate on schedule, I have to make it count.

# 2023-10-15 #
*The weather is getting chillier day by day. 
*Although sometimes the temperature rises up to 33 degrees, most of the time, it is around 25~29, which is rather comfortable compared to the summertime.
*Midterm exams are around the corner, but it is hard to focus on studying in this beautiful season because the views could disappear in the blink of an eye.

# 2023-10-22 #
#The lecture this week talks about how those 'radical people' were raised.
#Surprisingly, many of them weren't born or raised as one. 
#They all came from ordinary families, but because of the lack of attention, they were having questions about the meaning of their lives. 
#That is the time when someone took advantage of it and recruited them to attend some radical movement by giving they the feeling of belonging to some group.
#All we need to do to stop this from spreading is to pay a little attention to the people around us.
#Although it may seem trivial, our society really could use some more human connection instead of polarisation and hatred.

# 2023-10-29 #
As we all know, the key to being healthy is by exercising at least 150 minutes a week.
However, we can still observe some people struggle to reach this requirement.
From my point of view, there are some reasons.
First, there is no immediate effect when exercising. 
Our bodies will not magically become fit or strong overnight. 
Secondly, we often have a tight schedule for education or work. 
Especially those who fought hard for their lives and careers.
But Science has proven that the human body can really benefit from doing exercise regularly. 
Not only can we become healthier and happier, but also can we improve our mental ability and prevent aging. 
I am happy that in my country, more and more people have started to exercise regularly, including me.

# 2023-11-05 #
This week, we learned about depression. It is an emotion that many of us have been through.
Most of the time, we are able to break free from the sadness. But someone is bothered by this for a long time.
From the medical perspective, it is caused by the malfunction of the brain.
I sometimes get depressed because of the overwhelming reports and tests, but I feel better after those, no matter the outcome.
I used to encounter a situation when a member of some club was trying to hurt herself. 
The only thing I can do is try to calm her down and talk her to sleep. 
That kind of sense of powerlessness makes me fear to say something wrong, causing her to commit suicide.
I hope I have more knowledge on how to feel better.

# 2023-11-13#
This week, there was a lot of work, and the deadline was very close.
On the one hand, I felt the challenge and was determined to accomplish it.
On the other hand, I felt exhausted because I didn't have time for other things, like exercise.
Luckily, it is temporal. If this is permanent, I will definitely wear off quickly.

# 2023-11-19 #
This week, I went to the National Intercollegiate Athletic Games of Climbing and entered the final.
There are a total of 19 people competing with me this year, and I was able to take 7th place, which is great for someone who takes this sport as a hobbit in college.
But I was a little sorrowful because I made a mistake no others had made, which caused me to be unable to use all my strength.
It was possible because I only climbed on NCKU campus. Now I know I can still improve my skills.

# 2023-11-29#
This week, we learned about how to reach a consensus. 
From my past experience, people in my country are usually more passive. 
That is why I prefer to avoid asking for ideas because no one wants to speak first.
My solution is to propose my ideas and then ask their opinions about how we can improve on this topic.
By doing so, the total amount of time spent on the teamwork can be more efficient.

# 2023-12-3 #
We have an instinct to search for news in our lives.
Yet, it is preposterous to verify that every single one of them was false or not.
I think we are bound to be misled by others now and then. 
But it is good to be exposed to this kind of hazard because we have to develop an immunity to fake news.
These days, some governments are trying to avoid fake news by censoring everything online or blocking every hazardous website. 
However, creating a 100% "healthy" environment simplely won't work because technological improvement is way faster than establishing new laws.

# 2023-12-11 #
This week, we talked about the planetary boundary. And what challenges we are facing right now. 
However, I have made a point that from Earth's point of view or evolution's point of view, the crisis that we made is just another mass extinction that has repeated at least five times in Earth's history.
What I want to say is that the life will live on. Of course, we need to try hard for our kind to remain.
But if we fail, Earth will continue to make another creature that can create another civilization.
It's not like everything is doom. Don't be so dramatic. 
Some student has argued that the temperature rising too fast for creatures to evolve.
But first, our DNA has a memory that can change some of its expression based on environmental changes, which is faster than evolution.
Second, let's not forget creatures that can endure extreme environments.
When massive extinction occurs, they will take their place and thrive, eventually making a new world.

# 2023-12-17 #
As the end of this semester is getting closer and closer, I can't help but think about what I could do and what I should do when the vacation starts.
There are tons of things I want to accomplish before I go for the exchange.
Maybe I should write a to-do list so I won't forget.
But for now, I need to concentrate on the incoming final tests and essays.
It will be terrible if I have to use my winter vacation in order to get the credit I lost.

**231221 Thu**  
Unsucessful and Productive  
I felt unsuccessful because I had a test today but I felt uncertain about the outcome.  
I felt productive because I am making progress on my course project with my teammates.  
I will get up earlier even temperature gets colder.  
  
**231222 Fri**  
Successful and Unproductive  
Successful: I didn't make any mistake or delay on my scedule.  
Unproductive: I didn't arrange much for myself this day.  
I want to go to the library and study for more then 3hr tomorrow.  
  
**231222 Sat**  
Successful and Productive  
Successful: I wake up early and spend my time in library.  
Productive: I Understand some important ideas in automatic-control course.  
Tomorrow I will visit my grandpa with my parents, no work tomorrow :D  
  
**231223 Sun**  
Successful and Unproductive  
Successful: I visit my grandpa and having a grat time wih my family.  
Unproductive: I didn't read or do anything for my class today.  
The weather is getting cold again, it is harder and harder to get out of my bad.  
  
**231224 Mon**  
Successful and Unproductive  
Successful: I used this day as a resting day and get a good rest.  
Unproductive: I still fighting to get out of my bed.  
I promise tomorrow will make some progress on my final exam.  
  
**231225 Tue**  
Successful and Unproductive  
Successful: I didn't do anything wrong.  
Unproductive: The weather is very cold, I didn't go to the gym.  
I slept for like 10 hour today, didn't feel very energetic still.  
  
**231226 Wed**
Successful and Productive  
Successful: I haven't did anything wrong.  
Productive: I made some progress on my final test.  
I am still charging for a final blow, maybe the energy bar is full and ready to go.  
