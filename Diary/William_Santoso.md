This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #

* The first lecture was just mainly introduction.
* The test was quite hard since it is just general knowledge which not everyone would know.

# 2022-09-15 #

* Second lecture was just reintroducing a bit for the new students that just took the class.
* lecture is for this week was quite boring since I have read a few things about it.
* I learned about bitbucket and how to create a diary.

# 2022-09-22 #

* On 26 September when I tried to edit my diary I saw my previous edit was not saved by bitbucket so I had to write it all over again.
* topic was great but I feel it was a bit challenging for some people to understand.
* How the future will look like in 10 years form now would be very different from what people would imagine, 10 years is a short amount of time if we can say as humans we live up to around 70 years and 10 years is only 1/7 of our life.
* In such short time not only technology can change but also culture, trends, and maybe much more would change but I would feel that it would not change that drastically.
* That is my point of view in which how the world would be changing in 10 years but talking about my future, I would be working back in Indonesia as a civil engineering running a company that my family had prepared for me.
* Five changes that would be affecting my future would be technology, culture, climate, language, and politics.
* First of all is technology I feel it would heavily affect how things work in thsi world especailly with jobs, my futue job requires and heavily relies on machine which is considered as old technology, but I believe old machines would be replaced into more advanced technologies that could help with physical labor that we have right now.
* Second, Culture, I feel that it would heavily impact on the current generation since it has changed a lot of youger poeple's view on the society.
* Third of all is climate change, global warming is a serious thing that we cannot ignore, we can see that some islands and cities are starting to sink in which could affect the current living place that I am in.
* Fourth, language, In 10 years I believe that people would be learning more and more languages and we can see a rapid growth, while this cna be helpful for my future work since I would be able to communicate more precisely with foreign countries.\
* Lastly, Politics, politics are heavily affecting my countries every decision as in what my future work is in the hands f the government since my family has been working with them for more than 40 years.