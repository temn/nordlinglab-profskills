# 2023-09-07 #
Lecture 1

Through the introduction in the first class, I learned about the general course content for this semester.

# 2023-09-14 #
Lecture 2

This week, we learn about conflicts.
I've learned how to strike a balance between persistence and compromise, and I've also gained an understanding of the five different states during conflicts.

1. Fight – One winner and one loser
2. Avoid – Unsolved conflict
3. Dampen – One sacrifices his own needs
4. Collaboration – Mutual discussion and solution satisfying both parties
5. Compromise – Trade-off between needs

# 2023-09-21 #
Lecture 3
## future
As we look ahead to the next decade, it's not hard to envision significant changes in our lives. While this period may seem distant, we can attempt to outline a possible future.
Firstly, in terms of living location, after graduation, I might choose to return to Kaohsiung due to considerations of living costs. The high cost of living and rent in the northern regions might lead me to opt for Kaohsiung, where I can handle financial pressures more comfortably. I'll be looking for a job related to mechanical engineering in the Kaohsiung Science Park, hoping to apply what I've learned in college to contribute to the company's development.
However, there are several trends over the next decade that could profoundly impact my life and work. Firstly, the rise of online currencies will reshape the financial world. Despite its convenience, this shift might pose challenges for those accustomed to using physical currency. Nevertheless, I can adapt to this new payment method to ensure my financial management remains unaffected.
Secondly, climate change is an undeniable issue. Rising sea levels and extreme weather events could significantly affect life in Kaohsiung. We may need more climate adaptation measures and environmental awareness to address these challenges.
Thirdly, the transformation of the energy industry will affect our energy supply and consumption. The proliferation of renewable energy and the use of electric vehicles may alter our energy market landscape and influence our lifestyles.
Furthermore, the rapid development of artificial intelligence, automation, and machine learning is changing the job market. This means certain jobs may become automated, but it also creates new opportunities, especially for those with relevant skills.
Lastly, remote work and virtual socializing have become the norm, which might change the way we work and interact socially in the future. We may need more robust virtual collaboration tools and communication skills to adapt to this new work environment.
In summary, the next decade is filled with challenges and opportunities. It's crucial to keep learning and adapting to ensure we can navigate change and achieve our goals and aspirations. Regardless of what the future holds, persistently pursuing our dreams is paramount.

## news
In summary, the recent incident involving armed militants kidnapping over 30 individuals, including at least 24 female students, at the Zamfara State University in Nigeria highlights a grave security challenge facing the country. This marks the first major student abduction since President Buhari's inauguration, underscoring Nigeria's serious security issues. Zamfara State has long been plagued by banditry, with attacks on villages, abductions of residents, and arson being common occurrences. Nigeria faces multiple security problems, including the longstanding insurgency in the northeast.
Most reports share similar details, with variations in their perspectives. Some reports blame the incident on issues with the newly inaugurated president, while others aim to evoke panic, suggesting security concerns in readers' own areas. In summary, left-leaning commentators focus on the recent large-scale abduction, highlighting the ongoing security crisis in Nigeria. Neutral reports emphasize the frequency of school abductions in Zamfara State and mention rescue efforts. Right-leaning perspectives emphasize the urgency of Nigeria's security situation and call for immediate government action.

# 2023-09-28 #

Happy Mid-Autumn Festival! Enjoy a barbecue!

# 2023-10-12 #
Lecture 4

1. knowing the five features of money
  (1)fungible
  (2)durable
  (3)portable
  (4)recongnizable
  (5)stable
2. watch three video and understand how economy work
3. spending drives economy
4. credit is the most important part in economy
5. I actually felt strong wind caused by typhoon last week

# 2023-10-19 #
Lecture 5

1. I heard the term fascism before, but it wasn't until today's class that I fully understood its meaning.
2. People's repulsive behavior often reflects the struggles they have in themselves. For instance, Extremists often feel a lack of belongingness.
3. Learning from Picciolini, even if one has made mistakes, recognizing those mistakes and beginning to make amends is never too late.

# 2023-10-26 #
Lecture 6

1. From group 16's presentation,what is advertisement? and how it works?   Advertising is a means of promoting products, services, events, or ideas through various media forms such as television, radio, newspapers, magazines, the internet, and social media. Advertising is designed to capture the attention of a target audience and influence their purchasing decisions. It typically includes images, text, and sound to convey specific information and evoke emotions or interest in the audience.Advertising can take various forms, including television ads, radio ads, online ads, print ads, outdoor ads, social media ads, and other forms of promotional activities. The success of advertising often depends on factors such as the target market, the way information is conveyed, creativity, and budget.   The primary objectives of advertising may include:

- Increasing the sales of products or services.
- Building brand awareness and loyalty.
- Providing information about products or services to assist consumers in making informed purchasing decisions.
- Creating or maintaining a brand image.
- Advocating for a social issue or political stance.
- Creating entertaining content to capture the audience's attention.

2. Because this week is mid-term exam week, many exams are concentrated in these days, and I do feel a lot of pressure. However, I think sometimes the pressure can actually help me grow. The right amount of pressure can motivate me to improve.
3. Two vedios in today's lecture.
4. In the first video, I could relate to the speaker's initial experience of unconsciously applying their experiment. After every workout, I feel calmer, more focused, and it helps me release my emotions.
5. I agree with the speaker; it makes sense to shift our focus towards a healthcare system that emphasizes overall well-being rather than just treating diseases. However, I also believe that while the concept is straightforward, implementing it can be extremely challenging. There are many complementary measures to be designed and put in place. It's possible to address these challenges, but it may take a considerable amount of time.

# 2023-11-02 #
Lecture 7

1. Today's lesson was about depression. We watched three TED videos.
2. The videos mentioned that we don't need to be psychiatrists and carry the responsibility of having to cure someone. Just attentive listening and expressing care and concern through words are enough to make the other person feel valued.
3. The beautiful Golden Gate Bridge in San Francisco was initially designed with the claim that it was absolutely impossible for suicides to occur. However, since its construction, several thousand individuals have tragically ended their lives here, which is indeed quite ironic.
4. Closing your mouth and listening to others attentively can very likely become a significant turning point in their lives.
5. The speaker was asked, "What would you do if there was no hope inside Pandora's box?" Before the speaker could respond, the person jumped off the bridge. If it were me, my response would be, "The box hasn't vanished. Even if hope is missing, it doesn't mean you can't put diseases, pain, disasters, and other calamities back inside and seal them again."
6. Survivors who jump off the bridge but manage to stay alive have often expressed that most people who contemplate suicide only realize they made a wrong decision at the moment they leap and still want to live.

# 2023-11-09 #
Lecture 8

1. During an interview, it's not just the company interviewing you; at the same time, you are also interviewing the company.
2. For me, speaking calmly to the killer of my son, who had no apparent reason to harm him, and forgiving him, as well as assisting his grandfather in helping more victims, would be virtually impossible.
3. I believe there might be underlying interests or connections between them that we cannot see, leading to the apparent harmony on the surface.
4. For me, the purpose of life can be described as the pursuit of happiness and the unrestrained experience of everything.
5. The noise from the construction upstairs is really annoying.

# 2023-11-16 #
Lecture 9

This week's presentation was a large group report composed of six people. However, one of our team members frequently didn't attend, so we rarely saw him during class, making it challenging to contact him. Consequently, during our team discussions for the report, we only had five members present. Despite being one member short, our progress at that time went quite smoothly.

However, during the presentation, the teacher pointed out deficiencies in our delivery and content, which was a bit disheartening. It became a learning opportunity, though. The teacher's feedback highlighted areas for improvement, allowing us to learn from our mistakes. It is through these identified areas that we can strive to make our next presentation better.

# 2023-11-23 #
Lecture 10

1. I have mid-term exams coming up again recently, and it's really tiring.
2. In the first half of this week's class, everyone presented the relevant legal regulations regarding noise in their respective countries. One classmate shared an experience she had during the Chinese New Year. She invited several friends over for a party, and the noise level was undoubtedly high. She mentioned that if the party had been until 11 PM, she could understand, but it continued until 3 AM. Unsurprisingly, neighbors complained, and she acknowledged that she would have done the same if roles were reversed. Considering the late hour, people might be tired after a day of work and just want to rest, especially with work again the next day. Interestingly, when the police arrived, they only advised them to go home without issuing any fines. The amusing part was that they later chose to continue the party at a bar.
3. Loud volume within reasonable limits can be acceptable, but at the same time, it's important to consider things from others' perspectives and practice empathy.
4. In the latter part of the class, we watched a TED talk video. I felt that compared to the characteristics of the millennial generation discussed in the video, I am relatively patient. However, most people around me do exhibit some of the traits he described. For example, regarding YouTube viewing habits, many are accustomed to watching videos at double speed, and videos longer than 15 minutes are generally not well-received.

# 2023-11-30 #
Lecture 11

1. I have a fluid mechanics exam tomorrow, and I'm feeling very nervous.
2. This week, our group has a presentation involving five people. I am the one responsible for presenting. Through this experience, I have learned a lot. We realized that some of our actions were not thoroughly thought out, the implementation was not carefully considered, and certain aspects were too vague.
3. I believe that changing education is extremely challenging. Frankly, I don't think that a plan conceived by just a few individuals can alter education, and it might not even have a significant impact. It could be like trying to change just the tip of an iceberg.
4. Today, we conducted an experiment where we placed our phones on the table in front of us to test how many times we felt the urge to take them out and check. Personally, I did it about three times, so I can say that I'm not that addicted to using my phone.

# 2023-12-07 #
Lecture 12

1. The topic of today's class is about planetary boundaries, and I took the opportunity to reflect on it as well.
2. The boundary temperature set by the Paris Agreement is 2 degrees Celsius, and it seems like the average global temperature is on the verge of surpassing this limit. This is making me increasingly concerned.
3. I believe that a rise in Earth's temperature is a natural phenomenon. However, the issue lies in the fact that before we accelerated global warming, human civilization could potentially have lasted for tens of thousands of years. After the onset of greenhouse gas emissions, we've shortened the lifespan of human civilization, perhaps to less than half of what it could have been. Human extinction is inevitable; it's just a matter of time. However, if we consider the possibility of migrating to Mars or other planets, that opens up a different perspective.

# 2023-12-14 #
Lecture 13

1. Today's group presentation also followed the format of dividing into groups of five. In this presentation, my responsibility was to create the Gantt chart. In terms of time allocation, some tasks were given too little time, resulting in the actual operation taking more time than expected. It was challenging for my team members to catch up with the schedule. With this experience, I've learned a lesson for future time allocation, and I will arrange more suitable schedule distribution to avoid putting too much pressure on my team members.
2. In the course, the professor discussed the differences in results among various voting systems on the same topic. From the outcomes, it was observed that there were differences in the number of votes between anonymous and non-anonymous voting, but the disparities were not significant. It can be inferred that anonymous voting allows individuals to conceal their identity, enabling them to express themselves more freely. This attitude may stem from the perception that accountability is challenging to establish in anonymous situations, as it is difficult to trace the origin of a statement. Simultaneously, with the increase in freedom of speech, anonymous expressions tend to be more extreme, posing a potential risk of causing harm to others and leading to significant issues that cannot be ignored.

# 2023-12-21 #
Lecture 14

1. Recently, as the semester is coming to an end, a bunch of assignments suddenly popped up. There are so many things crammed together, and on top of that, I have to prepare for the final exams. I feel like there's not enough time in a day—just 24 hours—and the pressure is immense. Moreover, I don't have the ability to handle everything perfectly, so I can only reluctantly give up on some tasks to free up time to complete other assignments.
2. When it comes to success, it might be too early for us at this age. A more rigid definition could be earning a significant amount of money, achieving great success in one's career, or standing out and becoming a prominent public figure. While gaining some awareness of success can be a good thing, I believe it might restrict our values because having a predefined notion of what success is could lead to a painful experience if we don't meet those ideal goals. Moreover, we haven't even experienced the real world yet, and the future holds limitless possibilities for each of us. Success that satisfies each individual can vary greatly, and it's essential to recognize that.

## Diary for each day
2023-12-21 Thursday
Today, I went to the model shop to purchase parts because our teacher assigned us a project in our required class. Some of the components of the mechanism we had built were malfunctioning, requiring me to allocate extra time to buy replacements. Unfortunately, the originally desired parts were out of stock. Luckily, there were alternative components available, albeit at a higher cost. Due to time constraints, I decided to purchase the substitutes directly. I consider today to be successful but only partially productive.

2023-12-22 Friday
Today, I returned to Kaohsiung and really didn't feel like doing anything. I consider today to be unsuccessful and unproductive.

2023-12-23 Saturday
I initially planned to start studying, but various miscellaneous matters ended up consuming my time, resulting in an ineffective study session. I consider today to be unsuccessful and unproductive. However, I also acknowledge some partial success and effectiveness.

2023-12-24 Sunday
Today, I returned to my grandfather's house and had the opportunity to meet my relatives. We gathered together to celebrate the Winter Solstice on Friday, choosing the weekend when everyone was available. We enjoyed eating tangyuan (sweet rice dumplings) and delighted in my grandfather's handmade oil rice, making the occasion very joyful for me.

2023-12-25 Monday
Today, I took two mid-term exams, and it left me feeling quite exhausted. The morning exam had a question that I pondered for a long time, but I couldn't solve it. Later, I found out from classmates that the question was actually beyond the scope of the exam. The teacher had specified the exam content in class, but surprisingly, he included material outside that scope, which was disappointing. The afternoon exam was relatively easier, but there were some questions I wasn't prepared for due to my own lack of readiness. Today, I also discussed a project with team members. Unfortunately, the motor suddenly stopped working, and despite spending a lot of time, we couldn't find the reason. I consider today to be somewhat unsuccessful, but with some productive aspects.

2023-12-26 Tuesday
After yesterday's exams, I became a bit lax. Tomorrow, there's another crucial exam, but I'm finding it hard to muster the motivation to prepare. My study efficiency has plummeted. I consider today to be partially successful and somewhat productive, but I'm struggling to get in the right mindset for tomorrow's test.

2023-12-26 Wednesday
Finally finished the final exams for this week and made progress in the project I'm working on with my team. Despite the considerable time and effort it took, I hope that tomorrow's presentation to the professor goes as smoothly as today. I consider today to be successful and productive.

## Five Rules for my Success and Productivity
1. Simplify
2. Time management
3. Advance planning
4. Rest
5. Focus

# 2023-12-28 #
Lecture 15

1. Today is also the day for the large group presentation.
2. In the video we watched during class, it mentioned that future farming operations may not require soil and can significantly reduce water usage. If we can truly achieve this level of innovation, it could be more environmentally friendly and reduce environmental damage. Some current farming practices involve improper land use, and prolonged cultivation can deplete soil nutrients, rendering the land unusable in the long run.
3. While the idea of randomly selecting individuals from society to replace politicians might sound interesting, in practice, it could be problematic. It's undeniable that currently, becoming a politician typically requires a certain level of education. If someone with a lower level of knowledge is randomly selected, it could potentially lead to a disaster.

## Diary for each day

2023-12-28 Thursday
Our group presented the semester project on automatic control today, and fortunately, everything went smoothly. However, when I was giving the presentation, I didn't manage the time well and ended up encroaching on the time allocated for the presentations of a few classmates. I feel sincerely sorry about that. After several busy days, I have decided to take a break today. I consider today's presentation a success, but it feels like it lacked effectiveness.

2023-12-29 Friday
Today, I secretly went home without telling my parents. Instead, I went to my grandma's house in Kaohsiung to surprise her. I pretended to be a delivery person, covering my face with an item so she wouldn't immediately recognize me. Fortunately, my plan worked, and seeing my grandma made me very happy. I consider today a success, even though it may have lacked a tangible outcome.

2023-12-30 Saturday

Feeling down about having final exams next week during the rare New Year holiday, I decided to take it easy today and not do anything productive. I plan to rest today and then focus on preparing for the finals in the next two days. I consider today unsuccessful and lacking in effectiveness.

2023-12-31 Sunday
It's disheartening to have to stay in the dormitory studying before New Year's Eve. I spent the entire day studying electronics, and it feels exhausting. Despite that, I consider today successful and productive.

2023-1-1 Monday
I have an electronics exam tomorrow, and I feel very nervous. I spent the entire day studying electronics, but I didn't cover automatic control and fluid mechanics. I feel that my skills are lacking. Overall, I consider today partially successful and somewhat effective.

2023-1-2 Tuesday
Today, I completed the final exam for electronics. I managed to handle the main questions, but after the exam, I feel mentally drained. Although there are still more final exams to come, I've decided to take a day off today. I consider today not entirely successful, but there was some effectiveness.

2023-1-3 Wednesday
Today, I had no classes, so I spent the entire day in my dorm preparing for the final exams. However, I feel like there isn't enough time to cover everything. I only managed to prepare for automatic control and didn't get to fluid mechanics as planned. Time is really tight, and both automatic control and fluid mechanics are challenging subjects. There are many concepts that I still haven't fully grasped. I consider today not entirely successful, but it was somewhat effective.
