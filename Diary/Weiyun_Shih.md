This diary file is written by Weiyun Shih E14096431 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-15 #

* Second lecture!
* Join the bitbucket of the lecture!
* Learn how to write my own diary of the lecture

# 2022-09-22 #

* It's the third week of the lecture, and with the online course.
* Listen to classmate's presentation and I learn some knowledge.

# 2022-09-29 #

* I got COVID-19 so I absent the lecture
* I still communicated the presentation with my teammate

# 2022-10-06 #
* Listened to classmates' presentation and classmates discussed a lot of economic issue with teacher.
* Watch two TED vedio and I learned the Fascism was not only from the government also from the companies.
* Heard to the story of a white man escaped from the Nazi and eliminated the racism from his heart.

# 2022-10-13 #
* Listened to some Ted talks and learn a lot of new things.
* Listen to other classmate' presentation and some of them are funny.

# 2022-10-27 #
* I learn ome knowledge of healthy life from classmates' presentation.
* Join in a big group to communicate the presentation.

# 2022-11-02 #
* Interesting lecture today,learn a lot of things that I should be prepared.
* Learn how to recognize professional skills and technical skills.

# 2022-11-10 #
* Today has a lot of presentations.
* The pressentation from supergroup are more interesting than other weekly preentation.

# 2022-11-17 #
* We have the course project gruop preentation next week.
* There are a lot of exams next two weeks,so we maybe discuss for our presentation as soon as possible for studying.

# 2022-11-24 #
* Today we watched two videos.
* From these presentations,I knew the actions that those groups will do.
* I still have a lot of exams next week.

# 2022-12-01 #
* The presentation this week is 2-3person group.
* The interview of the presentation next week seems hard,but my group and I will try our best.
* I have Fluid Mechanics exam tomorrow,I wish I can get good grades.

# 2022-12-08 #
* This week has course project group presentation.
* I made the PERT Chart in our presentation.
* The exams all finished this week and I can take a rest.
* There are no exams so I can watch the WORLD CUP all night!

# 2022-12-15 #
* The week I have new little group.
* We have debate this week.
* The presentation last week was made by me and I think the subject was interesting.
* The weather is gettimg colder,I don't want to leave my bed every day.

# 2022-12-16(Friday)
* Successful and productive
* I got the TOP6 in the paper airplane competition of fluid mechanics.
* I think I need to start studying for the final exams.

# 2022-12-17(Saturday)
* Successsful and unproductive.
* I went to Kaohsiung with my friends.
* We won som money from WORL CUP!

# 2022-12-18(Sunday)
* Successful and productive.
* I came back to Tainan.
* I started to study for the final exams.
* Argentina won the WORLD CUP Champion!I love Messi!Congratulations!!

# 2022-12-19(Monday)
* Unsuccessful and unproductive
* I got bad score in Mechanicla design.
* I felt asleep because I watched WORLD CUP until 3 a.m. yesturday.

# 2022-12-20(Tuesday)
* Unsuccessful and productive
* I have a class at 8 a.m. but I sleep until 10 a.m.
* I buy a new pants.
* I finish my written report of general course.

# 2022-12-21(Wednesday)
* Unsuccessful and productive
* I did my homework of fluid mechanics.

# 2022-12-22(Thursday) #
* Successful and productive
* I finish my fluid mechanics homework.
* I ate some sweet dumplings.

# 2022-12-23(Friday) #
* Successful and unproductive
* I date with my girlfriend.
* I didn't study that day.

# 2022-12-24(Saturday) #
* Successful and productive
* I joined the X'mas party in Chimei Museum with my girlfrind.
* The date is excellent.

# 2022-12-25(Sunday) #
* Successful and productive
* I studying at Cafe with my girlfriend.
* I taught her the Statics.
* I finished studying the Machine design.

# 2022-12-26(Monday) #
* Successful and productive
* The final exam od Machine design is easy.

# 2022-12-27(Tuesday) #
* Unsuccessful and productive
* I oversleep today.
* I study a lot of Complex Analysis.

# 2022-12-28(Wednesday) #
* Succesful and productive
* Our group finished the 5th presentation today.
* I have done the final project of Automatic control.

# 2022-12-29 #
* It's live presentation today.
* I was the presentor of my group this week.
* When I was on the stage, I was very nervous.
* We saw a lot of vedios today.