# Class Notes

## 2024-9-19

The first time that I attended this class.

I know that conflicts are not all bad. In some cases, they might be a good thing. It still depends on the occasion.

## 2024-9-26

I think that I will still live in Taiwan ten years from now. I might be an engineer or have an occupation like teacher or something else. I think I will do that job because of my main course of study in college. But sometimes, I think what you study in the past can't define what job you do in your future. That's why I think I am not being defined by a certain job.

When it comes to the five current trends of change that might affect me the most, I think:

1. **Artificial Intelligence**: It has many relationships with human beings. When artificial intelligence develops to be much more mature, it might even affect how human future life will be.

2. **Personal Abilities**: One's abilities will affect how a person will be in the future. Some abilities are necessary in our lives, some can help us get a better job, some can even destroy one's life. All in all, it depends on what abilities people acquire.

3. **Company Requirements**: The type of people companies need affects us a lot. If somebody is good at engineering, he or she might be the person they want. On the other hand, those who are only good at something that isn't needed now might find it hard to find even just a small job.

4. **Global Environment**: The surroundings will affect a lot. When global warming is getting worse, not talking about our jobs, even our lives will be in danger. I think this is one of the most important things.

5. **Food Security**: The last one is the problem we have faced now - the food problem. The population on Earth is still increasing. If there is not enough food, even if you have a lot of money, you might not even be able to buy a banana.

The above are the five current trends I thought will affect our future. All in all, it can be serious if we don't deal with it right now.

## 2024-10-17

Today, I thought about the impact of the financial system on daily life. Banks and investment markets enable the flow of funds, influencing both consumer spending and investment choices. By understanding the financial system, I can better manage my financial decisions and gain a clearer view of how the economy affects our everyday lives.

## 2024-10-24
Today, I thought about simple ways to live healthier in the short term. First, I’ll focus on my diet—eating more fruits and vegetables, drinking more water, and avoiding too much sugar. Exercise is next, even if it’s just a short walk each day or a quick stretch in the morning. Finally, sleep is key, so I’ll aim for a consistent schedule to feel more rested. Small changes like these can make a big difference, helping me feel more energetic and balanced day by day.

## 2024-11-07
Today,we watch a lecture which is talking about depression.In the lecture,we can find the way to help with those patients with depression,like provide them some mental support or listen them express their feelings without any judgement.All in all,we must cherish the people around us,made us away from the depression. 

## 2024-11-14
In the beginning of the class,we listened to classmate's diary and presntation about helping depression people around us.After that,we go to find the other ways to help with people with depression ,like feeding a pets like a puppy or cat,which can relax the patient.In the end,we watch a video about how to understand the true maening we did to the patients.Therefore,we can use a better method to deal with different kinds of patients. 

## 2024-11-21
In today's lecture, there is many groups demonstrating their presentations about the laws of environment. Take our group for example,we talk about the demission of the carbon dioxide, also the law that restrict the co2. In the end of the class, we watched a ted talk which is talking about the social media negative effection like the fake news, we should figure out some way to deal with it, like establish an agency to verify all the news we see on the internet, do so can let us make sure whether the news are true or fake.

## 2024-11-28
In today's class, the professor do a easy survey to find out how are we addicted to the smartphone and social media. After that, we get started to the group presentations, in those presentations, I learn lots of new knowledges and methods to some problems we met in normal life. In the end of the class, we talk a little bit about the noun "dark age", which means a dark history in europe at about 19 century.

## 2024-12-05
I have learned some useful skills to plan the things we need to do in the future, this skill can help us plan our future more sufficiently,after this,the professor talked about an issue" planetary boundary ", which is talking about the situation about earth, actually, because human always ignore the damage we do to the earth, in the end, we don't get any advantages, in the otherhand, we might even destroy our planet by ourselves if we didn't stop our action and keep hurt it.

## 2024-12-12
In today's class, all groups are talking about their final project plan and their next step to execute it, however, some group seems to be unsure about their direction to their project, I thnk we might need to spend more time to find out what we really want to do, so we can make our direction to the article more accurately.Although he discussion in our group may differ from each other, we can integrate all our opinion together and find the best to our article, this way can let our group work more smoothly. 

## 2024-12-19
* successful/productive
* I learn a lot on today class and learn many skills, so I feel successful. I solve many calculus problems today, so I feel productive.
* I think I can learn more study skills on tomorrow class, so I can learn more productive.

## 2024-12-20
* unsuccessful/productive
* I think that I mess up my math test on that day, so I feel unsuccessful. I study on physics a lots on that day, so I feel productive.
* I think I should prepare more on the math test on the next test, so that I won't mess up.

## 2024-12-21
* successful/unproductive
* I deal with many physics problems on today, so I feel successful. I sleep so late on that day without doind anything, so I feel unproductive.
* I think that I can get to bed earlier yesterday, so that I can get up earlier tomorrow.

## 2024-12-22
* successful/productive
* I get up early today, and do lots of thing, so I feel successful. When I get back to the dormitory, I finish the English report, so I feel productive.
* I think I should keep up with today's performance, so that I can work more efficiently.

## 2024-12-23
* successful/productive
* I have completed the physics homework before lunch, so I feel successful. And I study the calculus tonight and understand many important point of it, so I feel productive.
* i think my behavior is perfect today, I just need to keep up for it.

## 2024-12-24
* successful/unproductive
* I got a perfect on physics exam, so I feel successful. I go to billiards for all night without doing anything important, so I feel unoroductive.
* I think I should spend more time dealing with homework instead doing something meaningless.

## 2024-12-25
* unsuccessful/productive
* I learn nothingfrom today class, so I feel unsuccessful. But I spend lots of time on my final exam this friday, so I feel productive.
* I think I should focus more on the lecture in class. 