# 2023-09-07 #

* The first lecture let me know more clearly about this lecture and what we are going to learn.
* It inspired me to care about the resources after watching the vedio.

# 2023-09-14 #

* The second lecture talked about conflicts.
* I think that it must happen when we interact with others.
* We should not affraid of encountering conflicts, instead we should think about how to balance different opinion.

# 2023-09-21 #

# HW #

* After ten years, I live in Changhua County which is my hometown. I'm a engineer on materials and work in a company which produces environmental friendly materials. Environmentally protecting has been discussed many years because ofglobal warming and energy crisis. Therefore, my company is dedicated to manufacturing some better materials.
  The first trend is less use of paper. Digital payment method is spreading everywhere. We don't need to bring cash in nowadays. The government quit to print the money. Also, we don't use white paper to record information or notes.
  The second trend is electric and automated car. Due to air pollution, electric cars become a new choice. Maybe they will replace driver in a few years.
  The third trend is artificial intelligence. More and more people rely on this tool to achieve their work or goals. Many industries also use it to make more effectively produce.
  The fourth trend is the change in the lifestyle. Because the COVID-19 brought big impact in the past, people develop other types of activities as well as internet learning which can avoid infectious disease lead to so many inconvenience.
  The final trend is privacy problems. To manage, the government masters people's information, including medical history, property even whereabouts. It is still discussed now because of invasion of privacy.

* News: Pakistan's prime minister says manipulation of coming elections by military is 'absolutely absurd'
* Left source: They emphasize that military will not intervene elections, and also highlight supporters' beliefs in a military rule threatening democracy.
* Right source: They focus on the interim prime minister was denial the military manipulation.
* Center source: They emphasize that military has significant influence in politics.

# 2023-10-12 #

* The topic of this week is financial which is something unfamiliar to me.
* Economic consists of numerous market, and market are made from a lots of cycles due to transcation between people.
* It is important to realize how the financial system works and what elements form it. 

# 2023-10-19 #

* The TED talk videos were great and helpful to fully understand what is fascism and how does it affect our mind. Those vedios really useful to let me handle the unfamiliar concept like fascism.
* The fascismmay come back if there was a small group of people control our preference. They can manipulate our consciousness and we even unkonw the facts.
* Potholes will affect our mental health over time, so we should not neglect or overlook them.
* By kowning how the extremist and fascist think, we can develop that instead of believing in their creed or slogan they joined the group just because of wanting to find belongingness and confidence.

# 2023-10-26 #
 
* The vedio about benefits of exercise showed us that how does exercise change the brain specifically.
* Before watching this vedio, I had known that exercise has a lots of benefits but I did not know what it will effect. After realizing what exercise actually brings about, I think I will try to do it.
* The other vedio is health system. It sounds good that doctors get pay if we change the system into another way which is more efficient to keeping people healthy. However, it must have numerous difficulties to overcome. For example, we can not sure whether doctors would cheat the patient by telling them they are healthy.

# 2023-11-02 #

* Nowadays, society has numerous expectations to a certain identity, and this may lead to huge pressure to somebody. Once we can not handle these responsibilities, we will feel uncomfortable or depression. It is not good for our mental health.
* I also learned how to talk with people who have depression. We do not have treat them as a patient, instead just communicate with them like interact with our normal friends. An important point is listen to them carefully and express sincerely, then they will feel more free to talk to.

# 2023-11-09 #

* It is interesting to me that we may not have a reason before making a choice or desicion. Instead, we usually find some evdiences after deciding, and this situation had been proven by the vedio. Although "flexible" sounds not bad, I still believe that we should keep some fixed standard in mind which can help us make more reasonable choice.
* Forgiveness is a virtue but when it comes to forgive someone who killed your family? Well, this behavior deserves respect but I think that is a slowly process consuming lots of time.
* Belonging, purpose, transcendence, and storyling are four pillars of a meaningful life.
* The vedio provides a totally different opinion which is we should not persue happiness. Finding out things we can dedicate to and people you treasure mutually are important steps to build meaningful life. Also don not forget we can change the way when we are telling our life story. It will give new perspective to regard happening.

# 2023-11-16 #

* This week is big groups presentation. There was not enough time left that teacher did not lecture much time. 
* I think that the group that presented on the topic of sleep did an excellent job, and there is a lot of things we can learn from them.
* After class, I inquired about how they prepared for this presentation. They mentioned that every Wednesday, they had a designated time for discussing the content of the report and how to distribute tasks. I felt that they were truly dedicated.
* As for our group, we started our preparation too late, so I found the final task allocation to be quite unfair. Due to unfamiliarity and a lack of prior collaboration experience, there wasn't much active participation from everyone initially. Although we managed to complete the presentation in the end, I believe there was still a lot of space for improvement.

# 2023-11-23 #

* In class, we watched a video discussing the characteristics of the millennial generation, such as impatience, difficulty in sustaining prolonged focus, and other negative labels. These traits are attributed to the influence of the education and environment they live in nowadays.
* I believe whether one is influenced in this way largely depends on parental upbringing. For instance, if parents can avoid exposing their children too much to electronic devices such as smartphones, it might reduce the likelihood of struggling with prolonged attention spans later in life. Of course, it could also prevent issues like poor eyesight.
* The lack of patience trait is also evident among my friends. I often hear some people watching videos at double speed, which I find quite surprising. To me, it disrupts the viewing experience, but they seem unfazed by it, and some even believe that watching at the regular speed is a waste of time.

# 2023-11-30 #

* This week is the presentation for our large group project, and I feel extremely nervous because I am representing the group on stage. Additionally, I have holiday classes and other exams this month, leaving me with limited time for practice. As a result, I can only read through the prepared script, and I feel quite uneasy about it.
* This week, the professor instructed us to place our phones on the front desks. The absence of phones seemed to help me stay more focused in class without distractions. However, it also meant I couldn't look up information when I didn't fully understand the content. I usually find it convenient to use my phone for quick searches in such situations, but now, I can only use my iPad, which feels a bit unfamiliar to me.
* I am aware that prolonged use of the phone for entertainment and relaxation can reduce cognitive thinking in the brain. However, when under significant stress, it helps me adjust my mood. Of course, there are risks involved, as it's easy to exceed the allotted time and end up delaying tasks that should have been completed. I believe the crucial aspect lies in how we use it, considering the phone itself isn't inherently good or bad.

# 2023-12-07 #

* Today's class topic revolves around the Earth's boundaries, particularly focusing on climate change and temperature. The temperature limit set by the Paris Agreement is 2 degrees Celsius, and it appears that the global average temperature is on the verge of surpassing this limit.
* The rise in Earth's temperature is a natural phenomenon, as evidenced by the planet's history, which shows temperature fluctuations occurring in natural cycles. However, human activities accelerate this process, disrupting the natural cycles and hastening the process of temperature increase. This shortens the lifespan of human civilization, pushing the Earth into environmental conditions where human survival becomes increasingly challenging.
* Most of the Earth's boundaries have already been exceeded. Perhaps human actions should be more proactive, considering there are still many environmental issues waiting for us to address.

# 2023-12-14 #

* In this presentation, one team member volunteered to report, and others took on various tasks. I believe our collaboration has improved, and everyone has become more actively involved. We also work together to find solutions to problems, and I hope we can maintain this teamwork until the final presentation.
* The professor discussed variations in the results of different voting systems on the same topic during the class. The outcomes revealed differences in the number of votes between anonymous and non-anonymous voting, although these differences were not substantial. It can be inferred that anonymous voting allows individuals to conceal their identity, enabling them to express their opinions more freely. This attitude may arise from the challenge of establishing accountability in anonymous situations, as it is difficult to trace the origin of statements. Simultaneously, with the increasing freedom of speech, anonymous expressions tend to be more extreme, potentially causing harm to others.

# 2023-12-21 #

* We have a project experiment that needs to be completed within a month, but the progress is very slow, and we often encounter issues with the components. We have spent a lot of time without making much headway, and this is causing me a lot of anxiety.
* Taiwan's next industrial revoluation is called "Productivity 4.0". For Taiwan's electricity generation, nuclear generation is very constant.
* Top 50 richest people in the world have seen the most wealth growth rate. The middle class is losing out in wealth growth rate, "The Shrinking Middle class". Bottom 50% captured 2% of global wealth growth. World inequlity index, earning $300,000 NTD/ month is the top 15% in the world.
* You got to be able to save money to be rich, have reserve. 

# 2023-12-22 #

* unsuccessful and unproductive
* In the afternoon, I went to the laboratory, and the experimental data did not meet expectations. Moreover, the instruments seemed to be malfunctioning. I spent a lot of time rechecking and calculating averages, only to eventually confirm that the instruments were faulty.

# 2023-12-23 #

* successful and productive
* I have a lot of progress to catch up on, but fortunately, there are no other commitments this weekend. I can use these two days to study. I managed to complete everything planned for today, and I have two final exams on Monday. I plan to schedule them for tomorrow.

# 2023-12-24 #

* unsuccessful and partially productive
* I overslept in the morning, which disrupted my plans. In the afternoon, I decided to take a break after completing some scheduled tasks. Unfortunately, I lost track of time and unintentionally wasted nearly three hours.

# 2023-12-25 #

* unsuccessful and unproductive
* Today is Christmas, but I have two final exams, and on top of that, I haven't completed the project experiment. We've spent a lot of time on the experiment, and just before wrapping up for the day, the system encountered another issue. Unfortunately, we don't know if it's a problem with the components or the battery. Everyone is feeling very exhausted.

# 2023-12-26 #

* successful and productive
* Tomorrow, I also have two final exams. In the evening, I reviewed my materials diligently, and I believe I can achieve good results. Additionally, there is a final presentation for my general education course next week. Even though it's only ten minutes, we are preparing very seriously. Most of the team members are highly involved, and I feel the positive atmosphere is conducive to success.

# 2023-12-27 #

* successful and unproductive
* Tomorrow is the presentation for the project experiment. After resolving the issues from the last time, we encountered different challenges. We asked the teaching assistant for help in examining other system issues and tried various methods tirelessly. Finally, around eleven o'clock in the evening, we succeeded unexpectedly. Everyone was thrilled, and we hope that tomorrow will conclude smoothly. However, we did spend a significant amount of time on the experiment.

# Five rules for more successful and productive #
* Set Clear Goals: Define specific, measurable, achievable, relevant, and time-bound (SMART) goals for yourself. Having clear objectives provides a roadmap for your efforts and helps you stay focused.
* Prioritize Tasks: Identify and prioritize tasks based on their importance and urgency. Use techniques like the Eisenhower Matrix to categorize tasks into four quadrants: urgent and important, important but not urgent, urgent but not important, and neither urgent nor important.
* Time Management: Develop effective time management skills. Create a schedule or to-do list, allocate time blocks for different activities, and minimize distractions. Use techniques like the Pomodoro Technique to break work into focused intervals with short breaks.
* Continuous Learning: Cultivate a mindset of continuous learning and improvement. Stay updated on industry trends, acquire new skills, and seek opportunities for professional development. This proactive approach enhances your expertise and adaptability.
* Healthy Work-Life Balance: Maintain a healthy balance between work and personal life. Prioritize self-care, get adequate rest, exercise regularly, and spend quality time with loved ones. A balanced life contributes to long-term productivity and overall well-being.

# 2023-12-28 #

* successful and unproductive
* Today's class involved everyone sharing their own five rules of success, and I found it very valuable for reference.
* The presentation for the project experiment has concluded, and fortunately, we managed to complete it in the end. Since other groups with the same topic have also succeeded, the TA mentioned that our scores should be good. Hopefully, it turns out that way.

# 2023-12-29 #

* successful and unproductive
* In the afternoon, I went to the B1 factory to work on molds. Since it had been a while since I last used a milling machine, I spent some time getting familiar with it. Achieving precision in milling was crucial for ensuring the proper coordination of the two molds. Despite being focused, the efficiency was not high.
* Next week is the final week of the semester, and there are many exams and reports to prepare for. I don't think I'll be able to take a break.

# 2023-12-30 #

* successful and productive
* Spent the entire day studying, fluid mechanics is really challenging.
