This diary file is written by KAMWAI LEI E14115316 in the course Professional skills for engineering the third industrial revolution.

#2024-09-19#

In our daily challenges , it’s easy to overlook the fact that the world is actually getting better. Despite the crises and difficulties we often see in the news, many indicators show that humanity has made remarkable progress in areas such as health, economic development, and social advancement. Today, we are not only living longer, but more people have been lifted out of poverty and are enjoying a higher quality of life. These advancements highlight humanity’s ability to overcome obstacles and innovate. The world may still face numerous challenges, but it is evident that collective efforts are paving the way toward a brighter future.

#2024-09-26#

In today’s class, the lecture centered on the 17 Sustainable Development Goals (SDGs). I also presented my Week 2 task, which was a nerve-wracking experience. During my presentation, I received feedback about an image I had used showing sea level rise in the Netherlands and Belgium. I learned that these countries face unique circumstances where sea level changes are not solely due to global warming but also due to post-ice age isostatic rebound. The professor advised us to avoid using sea level data from regions like Southern Europe, where natural geological phenomena significantly affect measurements.
We also watched three insightful videos during the class:
A talk show featuring Christiane Amanpour, a well-known journalist.
Mona Chalabi discussing how to identify misleading statistics.
Hans Rosling presenting the evolution of global life expectancy in his iconic talk, "The Best Stats You’ve Ever Seen."
The discussions emphasized how easily fake news spreads on social media, often amplified by AI, and how this misleads people, especially on sensitive topics like humanitarian efforts, politics, and elections.
When analyzing statistics, three critical questions stood out:
Can you see uncertainty?
Can I see myself in the data?
How was the data collected?

other task

The topic for this analysis is: “SpaceX launches rescue mission for 2 NASA astronauts stuck in space until next year.” Using Ground News, I reviewed three sources representing left, center, and right-leaning perspectives.
Left-Leaning News: This coverage focused on logistical and safety concerns, particularly highlighting technical issues and delays such as the Boeing spacecraft's failure.
Center-Leaning News: The emphasis was on the broader significance of the mission, showcasing SpaceX's operational advancements and its implications for space exploration.
Right-Leaning News: The framing highlighted the mission's success and SpaceX’s capabilities, omitting details about Boeing's issues or potential safety concerns.
This exercise demonstrated how different political biases influence the framing of the same event, shaping public perception in nuanced ways.Analyzing news sources with a critical mindset allows us to uncover hidden perspectives and question the narrative presented.

#2024‐10-03#

Money is a medium of exchange that facilitates trade, allowing individuals and businesses to buy and sell goods and services without relying on bartering. It serves three primary functions: a unit of account to measure value, a store of value to preserve wealth over time, and a standard of deferred payment for future transactions. Money can take various forms, such as coins, banknotes, and digital currencies. Its value is based on trust, often backed by governments or institutions, rather than intrinsic worth.

Beyond its basic functions, money plays a pivotal role in shaping economies, influencing both individual and collective decisions. The way money is distributed and managed affects social equity, economic growth, and stability. Digital transformation, such as the rise of cryptocurrencies, is redefining traditional notions of money and challenging existing financial systems. Additionally, understanding how inflation, interest rates, and monetary policy work offers valuable insights into managing personal and national wealth. Grasping the evolution and impact of money enables individuals to navigate the complexities of global markets while making informed financial choices, ultimately connecting them to the broader dynamics of economic interdependence and societal progress.

#2024‐10-17#

**My Future in 10 Years: A Reflection**

Ten years from now, I envision myself living in a vibrant, forward-thinking city—perhaps Macau, my hometown, or a major engineering hub like Singapore or the Netherlands. By then, I will have established myself as a skilled professional in mechanical engineering, possibly specializing in renewable energy or automation. I hope to contribute to projects that drive sustainable development and technological progress, creating solutions that balance efficiency with environmental stewardship.  

My career will likely revolve around solving complex problems in the energy or manufacturing sectors, areas where mechanical engineering expertise is in high demand. I also see myself pursuing continuous learning, possibly through a master's degree or industry certifications, as engineering is a field that evolves rapidly with new technologies. Beyond work, I aim to balance my career with personal aspirations, building a fulfilling family life while championing inclusivity and equality in the workplace.

**Five Trends That Will Shape My Future**  

1. **Sustainability and Climate Change**  
   As the world becomes more focused on mitigating climate change, industries are rapidly adapting to meet new sustainability standards. This shift will influence my work as a mechanical engineer, pushing me to design systems and products that consume less energy and rely on renewable resources. I may find myself involved in green technology development, such as wind turbines, electric vehicles, or carbon capture systems. This trend will also impact my personal life, as living sustainably will likely become a societal norm.

2. **Advances in Automation and Artificial Intelligence (AI)**  
   Automation and AI are transforming industries, creating smarter, more efficient systems. In mechanical engineering, these technologies will redefine how we approach design, manufacturing, and maintenance. Ten years from now, I will likely use AI-driven tools to optimize workflows and develop innovative solutions. However, I’ll need to stay adaptable, as automation could also alter job roles and require me to focus on uniquely human skills like creativity and problem-solving.

3. **Globalization and Cross-Border Collaboration**  
   With global collaboration becoming more seamless due to technology, I anticipate working on international projects and interacting with diverse teams. This trend might take me to other countries, broadening my perspective and skills. For instance, the Netherlands, known for its engineering and sustainability leadership, could play a role in my professional development. Such experiences would enrich both my career and personal growth.

4. **Remote Work and the Gig Economy**  
   The rise of remote work and freelance opportunities will likely redefine traditional employment. While I aim for a stable career, I see the potential for hybrid or remote work arrangements that allow for greater flexibility. This could enable me to pursue global opportunities while staying connected to my roots in Macau. Additionally, side projects or consultancy roles could become viable income streams.

5. **Technological Convergence**  
   The integration of multiple technologies—such as the Internet of Things (IoT), robotics, and advanced materials—will shape engineering solutions in the next decade. My expertise will need to span across disciplines, requiring me to collaborate with professionals from IT, data science, and other fields. This convergence will likely lead to groundbreaking innovations but also challenge me to remain adaptable and open to lifelong learning.

**Conclusion**  

While predicting the future is inherently uncertain, these trends offer a glimpse into the opportunities and challenges I might face. By staying proactive, adaptable, and grounded in my values, I am confident I can build a future that aligns with both my professional ambitions and personal goals.  

Ten years from now, I hope to look back with pride at how far I’ve come, not only in my career but also in creating a balanced and meaningful life. I aspire to be someone who contributes to society through engineering solutions while supporting global sustainability efforts. At the same time, I want to nurture a family and a strong network of relationships, building a life that reflects both my cultural heritage and the global influences I embrace.  

In this rapidly changing world, the key to thriving will be adaptability and lifelong learning. By embracing these principles, I am optimistic about navigating the challenges and possibilities the next decade will bring. 

#2024-11-07#

Recognizing DepressionDepression is a multifaceted mental health condition that manifests in various ways, making it essential to recognize its symptoms to provide timely help. A loss of interest in activities once enjoyed is a hallmark sign, often accompanied by feelings of apathy and disconnection. Additionally, changes in sleep and appetite are common indicators. These may include insomnia, hypersomnia, a significant decrease or increase in food intake, or noticeable weight fluctuations. Emotional changes such as persistent feelings of hopelessness, worthlessness, or guilt may arise, creating an overwhelming sense of being a burden to others. These feelings can often spiral into social withdrawal, further isolating the individual from potential support systems. Recognizing these signs early is the first step toward fostering a supportive environment for someone battling depression.

What to Say to a Depressed FriendCommunication is a powerful tool when helping a friend or loved one struggling with depression. The emphasis should be on creating a space of understanding and empathy. Avoid judgmental language or unsolicited advice, as these can unintentionally dismiss their emotions or experiences. Instead, focus on conveying compassion through simple, supportive phrases like “You matter to me” or “I’m here for you.” These affirmations validate their feelings without placing pressure on them to explain or justify their state. Phrases like “You’re not alone in this” can also reinforce their sense of belonging and support. Be patient, understanding that opening up may take time, and reassure them that their feelings are valid and not a burden to share.

Helping a Friend with DepressionSupporting a friend with depression requires a balanced approach that respects their boundaries while gently encouraging them toward healthier routines. Physical activities like regular exercise, or even short walks, can uplift mood through the release of endorphins, but the suggestion should be made without coercion. Similarly, encouraging a balanced diet can improve their physical well-being, contributing to emotional stability. However, these recommendations should be offered with patience and sensitivity, as the lethargy or disinterest common in depression can make even small actions feel monumental. More important than giving advice is listening without judgment. Simply being present can provide immense comfort, showing your friend that they don’t have to face their struggles alone. Consistent follow-ups, like checking in with a simple message or phone call, also reinforce your commitment to their well-being.

It’s important to recognize when professional help is needed. Suggesting they reach out to a mental health professional or assisting with finding resources can bridge the gap to professional care. If they’re open to it, offer to accompany them to appointments or assist in scheduling them. During moments of severe distress, ensure they are safe, and consider reaching out to crisis resources if necessary. Remember that your role as a friend is to provide support, not to take on the role of a therapist.

Building Awareness and Emphasizing SupportMental health awareness plays a critical role in destigmatizing conversations around depression and empowering individuals to seek help. Nikki Webber Allen’s personal experience highlights the importance of self-care and reaching out for support, breaking the silence that often surrounds mental health struggles. Her message resonates deeply, emphasizing that vulnerability is not a weakness but a vital step toward healing. Similarly, Bill Bernat’s advice on offering genuine support underscores the need to be patient and non-intrusive, creating a space where friends feel understood rather than isolated.

Expanding awareness goes beyond individual interactions; it’s about cultivating a broader culture of empathy and understanding. Educational campaigns and community events can help disseminate information about recognizing depression, offering support, and connecting with mental health resources. Schools, workplaces, and social groups can incorporate mental health discussions into their routines, normalizing the topic and reducing the stigma associated with seeking help. Encouraging conversations about self-care strategies, such as mindfulness, journaling, or pursuing creative outlets, can provide individuals with tools to cope and express themselves.

As friends and allies, we must understand that our support, while vital, is only part of the journey. Recovery from depression often involves a multifaceted approach, including professional therapy, medication, and personal efforts toward healing. By being empathetic, patient, and supportive, we can play an essential role in helping someone navigate their way toward better mental health. Each conversation, no matter how small, is a step toward building a society where no one feels isolated in their struggles.

#2024-11-14#

The debate between two classmates at the beginning of the class was fascinating, and the topic they discussed is very relevant to our daily lives. I believe nuclear power is much more eco-friendly than thermal power generation, but the challenge lies in how to store nuclear waste, as current technology has not yet solved this issue.


#2024-11-21#

We had a group discussion about our previous homework and prepared for an upcoming presentation. I also learned simple ways to support friends experiencing depression, like listening without judgment and just being there for them. At the same time, I was reminded of the importance of self-care when supporting others. It’s all about balance. While it's crucial to be there for people, I also realized how easy it is to neglect my own mental health while focusing on others. We explored different strategies for supporting loved ones and the importance of setting boundaries. This class made me reflect on the value of empathy and the role of active listening. It also helped me understand that it’s okay to seek help when I need it, as self-care is essential to avoid burnout.

#2024-11-28#

In class, several groups shared their ideas for the final project. After each presentation, we voted on their proposals to decide which were the most promising. We also explored the lifecycle of civilizations and discussed how societies recover from dark ages, highlighting the resilience and adaptability of human history.

During the session, we watched a YouTube video about riots in Amsterdam involving Maccabi supporters. The video shed light on how media narratives often omit key facts, prompting us to think critically about the information we consume.

At the end of the class, we were allowed to pick up our phones, and the professor asked how many times we had felt the urge to check them during the session. It was a subtle reminder of our growing dependence on technology.

#2024-12-05#
In class, some groups presented their findings on how news sentiment varies between countries and how this correlates with each country's global press freedom score. During the subsequent group discussion, we analyzed the perspectives presented in the news and considered who might benefit from these narratives.

We then moved on to a new topic: "planetary boundaries." This was my first time encountering the concept, and I found it alarming to learn that six out of the nine planetary boundaries have already been crossed. Among these, the "novel entities" boundary—referring to human-made substances like plastics, chemicals, and pollutants—has been crossed the most, posing significant risks to ecosystems.

To deepen our understanding, we watched Johan Rockström’s video "Human Growth Has Strained the Earth's Resources," which highlighted the dangers of crossing ecological tipping points and the Earth's finite capacity to buffer these changes. We also watched Kate Raworth’s talk, "A Healthy Economy Should Be Designed to Thrive, Not Grow," where she argued for an economic model centered on thriving within ecological limits, rather than pursuing infinite growth.

#2024-12-12#

Each group presented their action plans, which included a Work Breakdown Structure (WBS), PERT chart, and GANTT chart, detailing how their plans address one of the planetary boundaries. A representative from each group briefly explained their action plan to the class, followed by a Q&A session to gather feedback, discuss ideas, and provide suggestions. This collaborative process encouraged critical thinking and refinement of each plan, fostering a sense of shared responsibility among students.

We also watched the documentary "The Third Industrial Revolution: A Radical New Sharing Economy" by Jeremy Rifkin, which explored solutions to global economic crises. The documentary emphasized the need for a shift toward new economic systems centered on sustainability and equality. It highlighted the potential of renewable energy, digital interconnectivity, and collaborative consumption to transform global economies. This thought-provoking film inspired discussions on how innovation and systemic change could address pressing global challenges, tying directly to the goals outlined in the class projects. Together, these activities underscored the importance of interdisciplinary collaboration and forward-thinking approaches in creating a sustainable future.



#2024-12-19 to 2024-12-25#

Thursday: Unsuccessful
Analysis: I felt mentally drained after back-to-back lectures and ended up wasting the rest of the day. 
Change for Tomorrow: Break my study sessions into smaller chunks with proper rest intervals to avoid burnout.

Friday: Productive
Analysis: I reviewed the week’s notes in preparation for the midterm exams .
Change for Tomorrow: Dedicate at least 30 minutes to relaxing or a hobby to maintain balance.

Saturday: Successful
Analysis: Today, I decided to take a break from academic pressure and went out to take photos with my camera. I captured some beautiful landscapes and everyday moments. This photography session helped me relax and sparked new inspiration, giving me more motivation for my upcoming studies. Although I didn’t work on academic tasks, this relaxation was essential for my mental and emotional well-being.
Change for Tomorrow: While I took a break today, tomorrow I’ll readjust my schedule to balance creative activities with making progress in my studies.

Sunday: Productive
Analysis: Focused on self-care and preparing for the week ahead by organizing my notes, cleaning my workspace, and outlining a study plan. Although I didn’t dive into new coursework, I felt well-prepared.
Change for Next Week: Start reading next week’s materials in advance for a head start on classes.

Monday: Successful
Analysis: I completed all my lectures and managed to finish two problem sets in Fluid Mechanics. I asked a question during class and gained clarity on a concept I struggled with before. However, I spent too much time on social media during my break.
Change for Tomorrow: Limit social media use to 15 minutes during breaks to focus more on study time.

Tuesday: Unsuccessful
Analysis: I procrastinated studying for Thermodynamics and missed submitting a homework on time.
Change for Tomorrow: Create a clear schedule for the day, setting reminders for deadlines and specific tasks.

Wednesday: Successful
Analysis: I go to library to do and study my homework and quiz .
Change for Tomorrow: Try to extend my study hours by waking up earlier, as mornings feel like my most productive time.

#2024-12-26 to 2025-1-1 #

Thursday:
Successful and productive
I attended all my lectures today and finally completed a long-overdue lab report for Fluid Mechanics. In the evening, I attended a networking event for engineering students, which provided great insights into potential internships. Although the day was tiring, it felt very rewarding.
To improve tomorrow, I’ll focus on prioritizing tasks to manage my energy levels better.

Friday:
Unsuccessful and unproductive
I struggled to stay focused all day. Despite having plenty of time, I didn’t complete my assigned coursework or review for next week’s Strength of Materials quiz. I ended up procrastinating on my phone instead of making progress.
To turn things around, I’ll schedule clear study blocks for tomorrow with planned breaks to stay disciplined.

Saturday:
Successful and productive
I bounced back today by sticking to the study schedule I created. I successfully completed a Thermodynamics practice set and clarified some difficult concepts through a study group session. I also took a short walk in the evening to recharge.
Tomorrow, I’ll aim to review one more lecture topic to stay ahead for the upcoming week.

Sunday:
Successful and unproductive
I had a relaxing day catching up on sleep and spending time with family, which was good for my well-being. However, I didn’t study or complete the prep work I planned for the evening. While it wasn’t productive, I enjoyed taking a well-deserved break.
To make tomorrow productive, I’ll start the day early and allocate time for studying before distractions arise.

Monday:
Successful and productive
I woke up early and managed to complete all my planned tasks, including revising for the Strength of Materials quiz and drafting my lab report. I also used Pomodoro timers to stay focused, which made a big difference.
I’ll continue using time management techniques tomorrow to stay on track.

Tuesday:
Successful and unproductive
Today felt successful because I attended a New Year’s gathering with friends, reflecting on the year and setting goals for next year. However, I didn’t work on any of my assignments, so it wasn’t productive academically.
To make the first day of the new year productive, I’ll start by planning the week ahead and completing one pending task.

Wednesday:
Successful and productive
I started the new year strong by organizing my study materials, reviewing for the Fluid Mechanics quiz, and completing one pending assignment. I also went for a run, which helped me stay energized and positive throughout the day.
To build on today’s success, I’ll continue reviewing key concepts daily to prepare for upcoming exams.

#2025-1-2#

Two Persons Who Inspire My Vision

Marie Curie (1867-1934)Achievements: Marie Curie stands as a beacon of inspiration through her groundbreaking contributions to science. As the first woman to win a Nobel Prize and the only person to win in two different scientific fields (Physics and Chemistry), she revolutionized our understanding of radioactivity. Her research laid the groundwork for significant advancements in medicine, particularly in cancer treatment, as well as energy development. Beyond her discoveries, Curie’s establishment of the Curie Institutes in Paris and Warsaw ensured a lasting legacy in scientific research and education.

What Enabled Her Success: Curie’s insatiable curiosity and resilience in overcoming societal and institutional barriers were critical to her success. Despite facing discrimination as a woman in a male-dominated field, she remained undeterred, pursuing her passion with relentless dedication and an extraordinary work ethic. Her humility and focus on the greater good, even in the face of immense personal sacrifice, serve as enduring lessons for aspiring innovators.

Christiana Figueres (Living)Achievements: Christiana Figueres’ legacy is encapsulated in her role as the architect of the historic 2015 Paris Agreement, a landmark global accord to combat climate change. Under her leadership as Executive Secretary of the United Nations Framework Convention on Climate Change (UNFCCC), Figueres transformed climate negotiations, bringing countries together to commit to ambitious emissions reduction goals. Her advocacy extends to promoting renewable energy and sustainable development, influencing both policy and grassroots movements worldwide.

What Enabled Her Success: Figueres' success stems from her exceptional ability to inspire collaboration among diverse stakeholders—governments, corporations, and civil society alike. Her visionary leadership and strategic negotiation skills were instrumental in aligning conflicting interests to achieve a common purpose. Central to her approach is the concept of "stubborn optimism," a belief that persistence and a positive outlook are essential in addressing global challenges. By empowering others and fostering a spirit of unity, Figueres demonstrates how vision and determination can shape a better future.

Both figures exemplify how perseverance, innovation, and leadership can overcome barriers and drive transformative change. Their stories remind us that achieving greatness often requires courage to challenge the status quo, a commitment to collaboration, and a focus on making a lasting impact on humanity.

My obituary:

Lei Kamwai, a mechanical engineer and passionate photographer, passed away peacefully at the age of 80. Born in Macau, he dedicated his life to crafting innovative solutions and capturing the beauty of the world through his lens. 
 
As a mechanical engineer,His projects, often a blend of technical brilliance and environmental consciousness, made a meaningful impact, contributing to a better, greener future.  

Beyond his career, Lei was a storyteller through photography. He found joy in immortalizing everyday moments and nature’s splendor, believing every image had a story worth telling. His photographs graced exhibitions and brought inspiration to those fortunate enough to see the world through his eyes.  

KAMWAI was a kind soul, equally passionate about his profession and the simple pleasures in life. He was a mentor, a friend, and a source of inspiration to many, always willing to share his knowledge and creativity with others.  

As a mechanical engineer,He leaves behind a legacy of innovation and artistry that continues to inspire. Lei Kamwai will be remembered not just for the life he lived but for the lives he touched, the beauty he created, and the optimism he brought to every endeavor.  
 
Rest in peace,KAMWAI,Your vision and kindness will live on in our hearts..