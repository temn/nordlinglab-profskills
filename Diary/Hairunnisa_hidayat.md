This diary file is written by Hairunnisa Hidayat I84085306 in the course Professional skills for engineering the third industrial revolution.


# 2022-09-08 #

* Today was the first day of class and we learnt about exponential growth.
* The quiz we did today was very interesting but there were still a lot of things that I wasn't aware of.


# 2022-09-15 #

* Today we learnt that solar and wind energy is going to dominate the renewable resources in the future.
* The latter part of the class was a little bit more difficult to understand
* We listened to a few groups' presentation and it was very interesting and insightful.

# 2022-09-22 #
* Today we learnt about being able to spot fake news and it is pretty interesting for me personally.
* In around 10 years from now I hope that I will be financially successful, and maybe have a famiily of myself too.
* About where to live, I myself personally like to explore the world, so I hope that I will be able to live somewhere abroad, maybe in an Englsih speaking country (?)
* I don't have a preference about the size of the house I will be living in, as long as its adequate for me to carry out daily activities is fine by me.
* About what work I will do, since I am currently studying in the Department of Pharmacy, I hope that I will be a clinical pharmacist by then, helping others have a better quality of life.
* I also hope that by then I can be a good pharmacist, and maybe if luck permits, maybe open a small business of my own (?) since just being a pharmacist may be boring in the long term.
* One of the current trends of change is that more and more things are controlled by artificial intelligence. This can be beneficial for some while it can be disastrous for others.
* Why it might be disastrous is when that person's career will be replaced by artificial intelligence in the future and therefore that prson will lose his or her job.
* On the other hand I feel that the development of artificial intelligence can be seen in a positive light as it may improve our quality of life by making a lot of things more efficient.
* Another trend is about the green pressure. Everyone is starting to go eco friendly and I think that I am also starting to be more conscious about this and makes me more eco friendly.
* By being eco friendly, I am able to reduce the amount of plastics and one time use utensils that will be taking lots of space at home.
* Another trend is the increase in use of social media, how it might affect me in 10 years is that maybe it will allow me to make lots of new connections that can be useful for me when doing business.
* As social media is easy to use and is at the tip of our fingers, socialising is even easier in 10 years.
* Next trend is digital first workplace. This might make working easier in the future as if everything is starting to be digitalized, searching and doing things will be easier and it will be more efficient.
* Unlike the past when everything is written and done manually, it is hard to search for things if everything is on paper.
* Last trend is about how people is paying more attention to their mental health. This is probably something good for everyone as it will allow people to work better and reduce burnouts.
* This will affect me a lot since pharmacy can be stressful and demanding. if mental health days are implemented, it will be good for everyone.
* From the ground news I chose about why UK is going after flu-covid twindemic this year.
* Some source urges people to get vaccinations to prevent explosive number of cases during winter.
* Other sources only gives fact about the vaccination, why we should get it and where to get it. It also writes about why UK is worried about an increase in covid cases during winter.

# 2022-09-29 #
* Today we learnt about money and its value.
* Its pretty interesting as it reminds me of an economics class.
* Although it is interesting, some parts are a bit more complicated to understand too.

# 2022-10-06 #
* Today we heard presentations about the financial system of different countries and it was pretty interesting to see the difference in the economies of different countries.
* The first fictional story/myth I believed in was: A retirement plan before 40 is too early.
* In reality, last week when I was in a phone call with my mom, she told me about her friend's younger sister who retired at the age of 40, and that broke the belief that I believed in.
* Next fictional story/myth I believed in was related  to career: Once I land my dream job and make money, I'LL be happy.
* A few days back I had dinner with my senior who graduated and was working her dream job. When I asked if she was happy, she only said that it was tiring and that she says that she will be happier if she could earn more money.
* So I personally feel that as humans, we are greedy and that we would never be content as we will have another goal after achieving a previous goal.
* Another fictional story/myth I believed in was that I should go to grad school to figure out what I want to do.
* A couple days back I saw my friends deciding to open up a business to pursue their passion in the food industry, and that particular friend was the friend who have always said that she has to go to grad school to figure out what to do.

# 2022-10-13 #
* Today one of the TED talks mentioned about how our response time decreases with habitual exercise.
* When the woman mentioned about exercise, the professor asked us to follow the woman exercising for around 30 seconds to 1 minute. 

# 2022-10-27 #
* Today's course might be a bit more sensitive for some people - depression. 
* Depression is actually not as uncommon as we might think.
* Quoting Thomas Insel, Director of National Institute of Mental Health USA, "The economic costs of mental illness will be more than cancer, diabetes and respiratory ailments put together."

# 2022-11-03 #
* It is said that exercise is shown to have a positive effect during treatment for depression.
* I totally agree with that statement as I have had a friend who had depression said that she was always in a better mood after exercising.
* About the onboarding experiences of new engineers, it was said that 31% of the newcomers quit and there were 5 top reasons why they quit. 
* The reason that I personally feel might be the main problem is being unable to handle the workload and the pressure in the new workplace.
* Moreover, work and school might differ a lot and it might be hard to adapt to it.

# 2022-11-17 #
* We listened to the presentations made about laws and it was a little bit hard to keep up.
* But it was interesting how the laws of different countries differ from each other.

# 2022-11-24 #
* We were asked to put our phones on the table in front of the class and to count the number of times we missed our phones or checked our phones.
* I did not check my phone at all nor miss it so it might be counted as 0 i guess(?)
* I even almost forgot to take my phone back after class.

# 2022-12-01 #
* Today we had a discussion together with different groups about the points of view of different news media on a particular news.
* The professor talked about plametary boundaries which I am still super confused about. 
* The course projects are getting a bit harder and finals are coming too so I guess busy days are coming.

# 2022-12-08 #
* Last week we listened to everyone's presentation and it was pretty interesting.
* We also had a small debate about each groups' action ideas.

# 2022-12-15 #
* A: Sucessful and productive
* B: Felt successful because I managed to finish all the tasks I wanted myself to finish today.
* C: I'll try to maintain the same productivity levels tomorrow.

# 2022-12-16 #
* A: Successful and productive
* B: Felt super productive because me and my friends had a group discussion and study session and we managed to conclude a lot of important things we thought we wouldn't be able to do.
* C: Tomorrow is the weekend, I hope that i coudld wake up early to start another productive day.

# 2022-12-17 #
* A: Mid successful and mid productive
* B: Achieved almost all the tasks I set for myself for today, but not 100% because the weather is super cold and its the perfect temperature to wrqap myself in a blanket and sleep.
* C: I'll motivate myself more tomorrow.

# 2022-12-18 #
* A: Unsuccessful and unproductive
* B: It was super hard for me to get out of bed because of the low temperature so I lied in bed for a really long time and when I got out of bed, it was to drink hot chocolate and watch netflix.
* C: Tomorrow is a weekday, so classes will force me to get out of bed and start a productive day.

# 2022-12-19 #
* A: Mid successful and mid productive
* B: I did a lot of things today but it wasn't enough to satisfy my expectations for the day. I was super tired and fell asleep for an hour and a half.
* C: Sleep more at night so that I'm not too tired during the day, or drink coffee.

# 2022-12-20 #
* A: Successful and productive
* B: Had no classes today because it was cancelled, but I managed to wake up and start doing assignments and presentations as well as study for the finals as most of my finals are on the upcoming week.
* C: Keep motivating myself I guess?

# 2022-12-21 #
* A: Successful and productive
* B: I did everything I had to do for the day
* C: Have a positive mind.
* PS: I forgot to write this down, but I wrote every day's log on my notebook, I just input it late sorry.

# 2022-12-22 #
* Listened to a lot of course project presentations and it was still as interesting as ever by how people have different perspectives on the same topic.
* I hope the semester ends soon, its so cold and I just want to hide under the blankets and do  nothing.
* A: Successful and productive
* B: Today was cold and it was a surprise that I was able to pull everything off
* C: I hope it can be warmer so that it'll be easier for me to wake up in the morning

# 2022-12-23 #
* A: Successful and productive
* B: Woke up feeling extra productive, and I had a lot of plans for today so that pushed me harder to finish my work for the day so that I don't have to worry about my unfinished work.
* C: I made plans with my friends to discuss about the exam things so I think through discussion I can be motivated to participate and be more active.

# 2022-12-24 #
* A: Successful and productive
* B: Was successful today because we had a group discussion that lasted for 4 hours and I had to prepare beforehand too, so that definitely pushed me to finish what I had to finish.
* Today was christmas eve too so we went to grab a late dinner together after finishing the group discussion
* C: I made plans again for tomorrow! So i think tomorrow will be a good day too eventhough I'm super exhausted.

# 2022-12-25 #
* A: Successful and productive
* B: Woke up feeling happy because its Christmas, who doesn't love Christmas right? Its cold too so I woke up early and drank hot chocolate and proceeded to do my unfinished work and studies since I have an exam coming next Tuesday.
* C: Tomorrow will be the last day for me to study before my exam so whether or not I wamt it, I will be productive tomorrow (studying).

# 2022-12-26 #
* A: Successful and productive
* B: Productive because I was driven by stress the whole day. Super anxious for the test tomorrow.
* C: Exam tomorrow is on 10am so I guess I'll have to wake up early to prepare for the exam. Wouldn't want to sleep past the time.

# 2022-12-27 #
* A: Successful and productive
* B: Exam day!! I'll count today as a productive day because I finished an exam :)
* C: Have another exam on Friday so I have to be productive and grind as hard as I can

# 2022-12-28 #
* A: Successful and productive
* B: Results are out (super quick). We discussed the exam paper at 10 am today and I proceeded on studying for the rest of the day because I'm not ready for the exam on Friday.
* C: Hopeful stress will drive me to be productive tomorrow. I tend to be super productive during exam weeks due to the stress drive :)
* PS: I tend to write all the diary once because I have them written down manually in my diary (because yes, I do hv a diary of my own) and prefer to write them in bitbucket after I have finished a week's worth of diary.

# 2022-12-29 #
* Today we listened to presentations too. 
* Its finals week and I'm exhausted after continuously burning the midnight oil to prepare for the exams. I can't wait for exams to be over.
* Its almost the new year and we have 2 classes left for this course. Am i excited? Definitely. This semester has been tiring.
* Its almost the new year too so I guess I have to start thinking about my new year resolution? 