This diary file is written by Dascha Calista H44097053 in the course Professional skills for engineering the third industrial revolution.

# 2022-09-08 #
* I'm so excited because its the first class.
* I'm confused with the grouping system.
* I hope to learn new things throughout this class.
* Happy to join another english class.

# 2022-09-15 #
* Its Interesting to hear other group's presentation.
* I wish my group have a chance to present next week.
* There are 5 types of conflict behavior which are fight with 1 winner and 1 loser, avoid which is an unsolved conflict, dampen where one sacrifices his own needs, collaboration where there is mutual discussion and sollution satisfying both parties and last but not least is compromise the trade off between needs.
* I learned about 3 levels of conflict which are win-win, win-lose and lose-lose.
* In today's class professor also introduced about Linud and GIT.
* There are 5 major innovation platform :blockchain, genome sequencing, energy storage, robotics, and artificial intelligence.
* From the TED talk video by Steven Pinker about "Is the world getting better or worse? A look at the numbers". I got that, the world will getting better in the future.

# 2022-09-22 #
 With that being said, 10 years from now I will be 30 years old and I have imagined myself being a housewife,  living happily with my 2 children. 
 Besides that, I will have my own boarding house business back in my home country,  a business that allows people to rent a room from my building for them to live in, with a monthly subscription payment. 
 I will most likely live back in my hometown in Jakarta, Indonesia, or another beautiful and people’s favorite island in Indonesia, Bali or Lombok island. 
 And to reach that point in life needs time and a lot of hard work as well as  determination.

 There are 5 things that I do  now that will most affect my future life. 
 The first one is to gain knowledge whether academic or non-academic. 
 My academic journey began when I was in Pre-school and finally I'm here in NCKU pursuing my Business Administration Bachelor’s Degree. 
 After graduating from NCKU I plan to do 2 things: (a) to start a business (b) to work as a corporate staff in a prestigious company. 
 If life goes to plan B, I plan to work for probably 2 years and continue to pursue my master’s degree. 
 Apart from these academic plans in my life, non-academic knowledge and being street smart in life will surely help a lot for one to excel. 
 Having basic life survival skills such as cooking and social skills, creativity and personality, as well as having a good attitude and how to create passive income. 

 The second thing that I plan to do is to build a huge connection that allows me to do networking with a lot of quality people. 
 I can do this by meeting new people regularly, and keeping in contact with people that I find will be helpful for my character development. 
 It might sound very mean and being very much selective, but I see this is a really important aspect in life as we don't know who we will need in the future to help pursue our life plans ahead. 

 For the third I do, it is clearly inevitable that everybody in this life needs money as long as they are alive. 
 For my plans for the near future is that I'm beginning to build my portfolio by improving my CV. 
 This can be done by working part time in restaurants and starting looking for summer internships in corporate fields. 
 I plan to do this as from my point of view about life, this is the most important aspect in life. 
 The way I see it, After graduating from university, there are only two options: either we’re going to continue to study for a Master's or going to work. 
 Seeing the options made me realize that even if I haven't got the chance to continue for a master’s I already have experience in fields that I know I can excel in. 
 Think of it as a safety net when life doesn’t go as planned. 

 Apart from what was said above, having passive income is also as important. 
 Gaining passive income could be done in several ways such as through stock investments. 
 By circulating our cash flow through investments, our money wouldn’t lose its value because of inflation. 
 Last but not least is living a happy life. 
 Living a happy life leads to a healthy life, for ourselves, for our family, and for the people around us. 

* For today's lecture I learn about 17 SDGs.
* I also learn a lot after watching all of the TED talk.
* Its interesting to hear other group's  presentation. 
* I hope I got the chance to present  next week.

* Part 2
* The article I choose is "Roger Federer ends tennis carreer with doubles loss alongside Rafael Nadal at Laver Cup".
* The article states that Roger Federer retored from tennis at the age of 40.
* He play at the final game at 2022 Laver Cup in London.
* He loses the game againts Rafael Nadal.
* There are 40 bias distributor that are involved, 24 leaning left (disagree), 14 leaning right (agree) and 22 in the center.

# 2022-09-29 #
* I'm happy to hear other classmate's vision for the next 10 years.
* I learned about how the  financial system works.
* Money and credit are used for trading or transactions.(consist buyers and seller).
* I Learned about how money was determined, the value of money, and about economic growth.
* We fill a questionnaire about money and economy in the world.
* Actually I don't really understand the video that professor played today.
* I'm  excited to work with a new team.

# 2022-10-06 #
* It is the first offline lecture.
* There are some groups present in today's lecture. 
* Today we discussed about the financial situation in some countries. Starting from Finland, Indonesia and also Singapore.
* From today's presentations, I learned that we have to master what we want to present. 
* Being able to record our presentation will give us more chance to get scores.
* Learn about facism which I never heard before.
* First story:Forrest Gump. The first movie that I would like to describe is Forrest Gump, a movie based on the novel of the same name about a slow-witted man that never thinks of himself as disadvantaged. Thanks to his mother’s support and his undying passion, leads him on an extraordinary journey of life. This movie inspired me that whenever I feel tired about college, I get to remember that I'm very proud to be where I am right now and this keeps me wanting to finish it. 
* Second story:The hummingbird project. Moving on to the next movie, the hummingbird project, is about two friends that are in a high stakes game of big money trading, where winning is measured in milliseconds. They dream of building a straight fiber optic cable connecting cities that would make them millionaires. This movie taught me about finishing what I've started until it's complete, no matter how long it takes and whether we win or lose.
* Third story:13 going on 30. The last film I would like to talk about is 13 going on 30, it is a movie about a kid that wishes so badly to be popular and once she reached that point, he was opened up to new points of view in life that would later make her a wise person. This movie is really relatable in my life as I have so many unchecked wishes but along the way, sometimes it's nice to appreciate what we have and dont always adhere to future plans.

# 2022-10-13 #
* This week's lesson is about anxiety and healhty life.
* We learn how anxiety can affect our heart rate and the way we thinking.
* When under pressure, we cannot think clearly.
* According to Plato,knowledge is well-justified true belief.
* I'm inspired to do more exercise after watching neuroscientist Wendy Suzuki's TED talk.
* Exercise is one of the most transformative thing that you can do for your brain today.
* Matthias Müllenbeck explain how to shift from a sick system to a true health care system could save us from unnecessary costs and risky procedures.

# 2022-10-27 #
* There is no lecture last week and it will be changed to january 7.
* I am happy to be able to work with other group members.
* Looking forward to work with a new group next week.
* In today's class we watched TED talks about "how to connet with depression friends", "the bridge between suicide and life", and "don't suffer from your depression in silene".
* From the supergroup presentation,knowing that if drink too much water can cause death.
* I think mental illnesses such as depression is a little scary because we can't determine the damage caused by it.
* In serious cases,depression may cause us to suicide.Even the patient who are suffered did now want to do that, but they are just being manipulated by their emotions at the time.
* I think the best medicine for depression people is someone to talk to. In order word, we need to let them feel society and don't let them feel lonely.

# 2022-11-3 #
* In today's class we discuss about helping others with depression.
* Based on the survey and story from classmate, there was many people got depression or anxiety.
* Before helping others, we should pay attention on our mental health first.
* Learning about work life and let me know about working environment, which is learnt how to deal with toxic work environment.
* Living in good environment, we can have a healthier life and have a better mindset.
* I learned the importance of professional skills.

# 2022-11-10 #
* Some groups do their presentation.
* We have to watch a video about law but I'm not too interested to it.

# 2022-11-17 #
* I skipped today's class because I'm sick.
* My friend told me that the professor picked our video for the last presentation.
* I was the one who supposed to present but I didn't come so I feel bad because my friend have to replace me doing the presentaion.
* Looking forward for next week's group presentation.

# 2022-11-24 #
* In today's class the professor ask us to put our phone in front of the class.
* I didn't missed my phone because I have my ipad with me.
* Today we present our 3 ideas for our course project.
* Lots of tasks to do for next week.

# 2022-12-1 #
* I feel kinda tired this week because my group is not really responsive they either didn't reply to my chat or didn't read at all.
* One of my teammate dropped the class.
* Some groups present their task.
* We have a big group discussion about different news around the world.
* The Professor asks us to present our 3 action ideas.
* Not every group are able to present their ideas because there is not enough time.
* There are a lot of graph to do for next week's presentation and also an interview.

# 2022-12-8 #
* Today is the 3rd course project presentation.
* I interviewd my friends about their motivating instagram account that could motivate my group to do our course project.
* There is another course project presentation due in 2 weeks.
* I hope my team member will be able to contribute more actively.
* We have a task about the 3rd and 4th industrial revolution for next week.
* I'm tired of this course, I hope it will end soon.

# 2022-12-15 #
* In today's class we learned about the 3rd and 4th industrial revolution.
* The third industrial revolution started in 1969 and the fourth industrial revolution started in 2011.
* We discussed about the cost of energy.
* We have a debate about "why does inequality exist?" and we didn't have any conclusion for that because there is not enough time.
* We learned about 7 habits of highly effective people by Steven Covey.
* There is a quote by Kim Scott that I like “The purpose of criticism is to help others improve. The purpose of praise is to help others know what to keep doing more of”.
* I also liked one more statement which says "Don't pick a job. Pick a Boss." “Your first boss is the biggest factor in your career success. A boss who doesn’t trust you won’t give you opportunities to grow.” - William Raduchel
* Next week we have to make a diary everyday.

# 2022-12-16 (FRIDAY) #
* I feel Successful and unproductive.
* I feel successful because I attended Taiwanese course this morning.
* After the class,I feel so sleepy so I decided to sleep so I feel unproductive today.
* I'm thinking whether I should study or just watching movies or youtube.
* I ended up just watched youtube at the rest of the day.
* Tomorroe I will make a schedule.

# 2022-12-17 (SATURDAY) #
* I feel successful and productive today.
* Today I work 2 shifts in the afternoon and night.
* Today's weather is so bad so I have to walk to work and also get wet because its raining I hope the weather is better tomorrow.
* At 9.30 my group have a little discussion for our final project.
* I feel happy because today went well even though the weather is bad.
* Next time when its raining I will wear my slippers and raining coat.

# 2022-12-18 (SUNDAY) #
* I feel Successful and productive.
* Same like yesterday,I also work 2 shifts today.
* Today's weather is better than yesterday but its verry cold.
* I think I like summer more than winter .
* Today my group started working on our final project. We made a facebook group to help people increase their sleep quality.
* At the end of the day,I watched world cup final with my friend. I feel happy to see Argentina won.
* I will sleep earlier tomorrow.

# 2022-12-19 (MONDAY) #
* I feel Unsuccessful and unproductive.
* Today I woke up really late because yesterday I slept at 5.
* Today I attended my class offline.Usually its online.
* Today's weather is really cold.
* I've done some of my task but there are still so many presentaions and tests to do next week.
* I hope I could be more productive tomorrow.
* I failed to sleep earlier today so I hope I can sleep earlier tomorrow.

# 2022-12-20 (TUESDAY) #
* I feel Successful and unproductive.
* I attended my class this morning but eneded up sleeping.After the class, I have a discussion for next week's final presentation.
* After lunch I just lay in my bed because its cold. I don't feel like doing anything when its too cold.
* In the afternoon, my friend suddenly called me and say she hit a motorcycle so I accompany her to the hospital.
* I feel unproductive because I don't do anything.
* I will wake up early and do my homework tomorrow.

# 2022-12-21 (WEDNESDAY) #
* Today I feel successful and productive.
* I woke up early today and study Germany.
* In the evening I went to px mart and do a bit grocery shopping.
* I recorded the presentation for this week's course project.

5 rules to be more successful and productive:

* Get plenty of sleep. A lack of sleep will destroy your productivity.With that in mind, make sure that you maintain a consistent sleep-wake cycle, reduce your exposure to blue light, optimize your bedroom and create a soothing bedtime ritual.
* Schedule your time. If you don't schedule your time,then you're going to wander aimlessly. Sure.You know that certain things need to get done. But, which ones are your priorities? When exactly are you going to cross them off your to-do-list? Are there deadlines that you must adhere to? Creating a schedule helps you answer these questions and plan accordingly.
* Socialize with friends.After a day of activities it's good if we can chat and relax with our friends.
* Organize your workspace.Tidying up and organizing your area prevents your mind from drifting to those pile of papers stacked on your desk.
* Practice gratitude. It's been found that those who spend five minutes writing down what they're grateful for have increased their sense of well-being by 10 percent. And, as you know, when you're a better mood, you're more productive.

# 2022-12-22 #
* Today we do a live presentation for our course project 4.
* We have to dress formally like we want to go to an interview. But unfortunately so one dressed properly.Only the professor did.
* We share our 5 rules to be more successful and productive.
* Today's topic is "how to solve our problems?"
* We got to see what people thinks on their secret to success.
* We heard a TED talk about "Doing democracy differently | Brett Hennig".
* We also heard TED talk from Bhutan's prime minister "This country isn't just carbon neutral — it's carbon negative |Tshering Tobgay".

# 2022-12-23 (FRIDAY) #
* I feel successful and productive.
* I attended Taiwanese class this morning.
* I study a little bit for next week's final test.
* I have an early christmas dinner with my friends and it went well.
* I hope I can wake up early tomorrow and be ready for christmas eve.

# 2022-12-24 (SATURDAY) #
* I feel unsuccessful and productive.
* Today I work 2 shifts and its so hactic because its christmas eve. It turns out messy because they sell a special menu without telling us how to make it before but it is a bit better in the evening.
* We eat Indonesian food for lunch.
* I hope I can work better tomorrow.

# 2022-12-25 (SUNDAY) #
* I feel unsuccessful and productive.
* Today's christmas and I'm abit sad because I can't spend christmas day with my family.
* Just like yesterday, I work 2 shifts today and there are so many people. I go home at 11pm and still need to prepare my presentation tomorrow.
* I feel unsuccessful because I'm not happy today. 
* I hope I can be happier than today.

# 2022-12-26 (MONDAY) #
* I feel successful and productive.
* Today is the second day of christmas. My family celebrate christmas togeher. Still hoping I could celebrate with them.
* I woke up late because I'm so tired yesterday.
* I did my final presentation and also preparing for tomorrow's presentation.
* I hope I can study harder for this week's final.

# 2022-12-27 (TUESDAY) #
* Today I feel succesful and productive.
* My group did quite a good job for the final presentation although there are some missing parts because of misstranslation.
* I study for tomorrow's exam. I have 2 exams tomorrow.
* I hope I can do well in my exams tomorrow.

# 2022-12-28 (WEDNESDAY) #
* I feel successful and very productive.
* I have 2 final exams today. German and Customer Relationship Management. Its hard but I think I do well in both of my exams.
* I also work today so I feel I'm very productive.
* I hope I can rest a bit tomorrow and continue preparing other exams.

# 2022-12-29 #
* Today's topic is about "How to make a difference?"
* The first TED talk is about "What makes something go viral?" by Dao Nguyen BuzzFeed's Publisher. She explains about the cultural cartography. There are some reasons for a video to go viral. Wheather it relates to other people, blow someone else's live, makes other people laugh, etc.
* The second Ted talk is about "To solve the world's biggest problems, invest in women and girls" by Musimbi Kanyoro the CEO of the Global Fund for Women.
* The third TED talk is about "What I learned from 2,000 obituaries"
* Lastly, "How teachers can help kids find their political voices" by Sydney Chaffee.
* The professor told us about why he asks us to make a daily diary.
* The professor also told us about why he is teaching this course. He opens this class to empower students to grasp their big dreams for the good of all of us, and hope to make a difference.
* We hear live presentation from the course project group.
* Next week will be the final exam.I hope I do well.

# 2022-12-30 (FRIDAY) #
* I feel succesful ad productive.
* I have my Taiwanese final exam today and it went well.
* I relaxed a bit by watching Emily in Paris but suddenly my store manager ask me to come for work.
* I worked until 9 pm.
* At night, I continued to watch emily in paris.

# 2022-12-31 (SATURDAY) #
* I feel successful and productive.
* I worked 2 shifts today and I feel tired because there are so many people in the restaurant.
* I eat Indonesian food for lunch and its delicious.
* Today is new year's eve so after work me and my friend try to find place to watch the fireworks. At first we don't know where to go but at the end we watched the new year's concert. We walked around 1 hour there and 1 hour back to the dormitory.
* We didn't make it on time so we watched the fireworks on our way.
* I'm happy to celebrate new year with my friends.

# 2023-01-01 (SUNDAY) #
* Today I feel successful and productive.
* Today I went to Chiayi with my friends and eat Chiayi's famous smart fish. We waited for 2 hours but its worth it.
* We went to other places too and its so fun.
* I feel productive because we walk more than 20.000 steps today.

# 2023-01-02 (MONDAY) #
* I feel unsuccesful and unproductive.
* I wake up really late because I'm so tired.
* I went to the mall and eat sushi with my friends.
* I didn't study for tomorrow's exam because I'm too lazy.

# 2023-01-03 (TUESDAY) #
* I feel succesful and a bit productive.
* This morning I went to class to do my final exam and I finished in 5 minutes.
* After the exam I went back to my room and sleep until 2pm.
* I watched youtube and netflix for the rest of the day because I have no exam tomorrow.

# 2023-01-04 (WEDNESDAY) #
* I feel successful and productive.
* I attended my last german class. We watched movie there but I don't understand the movie because its in German and the subtitle is in chinese.
* I finished editing the video for the final project.
* I studied for tomorrow's exam and I hope I'll get a good score.

# 2023-01-05 #
* Today we have an exam in 25 minutes.
* After the exam, today's topic is about "How to find the best partner?"
* Professor said that this is one of the most important decision we will make in our life but there are no teacher told us about it.
* We watched a TED talk about The mathematics of love by Hannah Fry. "Finding the right mate is no cakewalk -- but is it even mathematically likely? In a charming talk,mathematician Hannah Fry shows patterns in how we look for love, and gives her top three tips (verified by math!) for finding that special someone.”
* The second TED talk is about How to stop swiping and find your person on dating apps by Christina Wallance. "Wallace shares how she used her MBA skill set to invent a "zero date" approach and get off swipe-based apps -- and how you can, too.”
* Lastly, professor asks us to listen this TED talk at home its about How to fix a broken heart by Guy Winch. "Psychologist Guy Winch reveals how recovering from heartbreak starts with a determination to fight our instincts to idealize and search for answers that aren't there -- and offers a toolkit on how to, eventually, move on."
* Saturday is the last lecture and we have to present our final course project video.

Course Objectives

1. Know the current world demographics
	
	* Suggestion: The current world demographics is much better than the previous century ago. Today, people's life expectancy rate is much higher than before.
	
2. Ability to use demographics to explain engineering needs

	* Suggestion: Demographic analysis is used to study the population based on factors such as age, race, and sex.
	* These data refer to socio-economic information expressed statistically, also including employment, education, income, birth and death rates, etc.
	
3. Understand planetary boundaries and the current state
	
	* Suggestion: We need to know these planetary boundaries so that we won't exceed its limit so that the future generation will not be living in a bad environment.

4. Understand how the current economic system fail to distribute resources
	
	* Suggestion: The current economic system produces an irreconcilable conflict between the goal of creating economically just and environmentally sustainable societies and embracing sustained economic growth. 
	* The current policies are constructed to produce more millionaires and billionaires.

5. Be familiar with future visions and their implications, such as artificial intelligence
	
	* Suggestion: As technology is developing more and more, we also need to adapt to the development so that we will not fall behind. New technologies also open door for many business chances as now evrything is done online.

6. Understand how data and engineering enable a healthier life
	
	* Suggestion: Providers and healthcare systems that master the data and then use it to improve quality of care for better population health and at less cost will benefit from financial incentives.
	
7. know that social relationships gives a 50% increased likelihood of survival
	
	* Suggestion: We as a human being need to socialize. Loneliness can lead one to become depression or other mental health issue.
	
8. Be familiar with depression and mental health issues
	
	* Suggestion: Depression is an illness which we must take it seriously as it can lead to death or suicide. 
	* There are plenty of ways to help one who is depressed such as listening to them.
	
9. Know the optimal algorithm for finding a partner in life
	
	* Suggestion: A partner in life is crucial as he or she can become a motivation or support for you in this life. 
	
10. Develop a custom of questioning claims to avoid "fake news"
	
	* Suggestion: Fake news are really a common things nowadays as social media arise which make fake news easy to appear. We always need to analyze every news we read if it is real or fake.
	* People tend to be polarized by fake news so it is better to have the claim to support every news we read.
	
11. Be able to do basic analysis and interpretation of time series data
	
	* Suggestion: Basic analysis is important for us human so that we will not easily being fooled.
	
12. Experience of collaborative and problem based learning
	
	* Suggestion: One way to increase collaborative and problem based learning is by doing a group project.
	
13. Understand that professional success depends on social skills
	
	* Suggestion: We need to socialize with many people to develop or connection and networking. these things can help us in th efuture in term of doing things such as businesses.
	
14. Know that the culture of the work place affect performance
	
	* Suggestion: People who have a good work environment tend to do better at their work rather than people who work is not supporting environment.
	* I think that CEO or the boss must know how to provide his/her staffs and workers a better work environment which will make them do better at work.
    
# 2023-01-07 #
* Today is the last lecture and also my last diary.
* We presented our final video and I think my group made a pretty good job.
* Some groups have an interesting video.
* I hope I'll get a good score.