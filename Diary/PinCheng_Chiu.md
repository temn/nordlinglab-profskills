This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2024-09-12 #

* The first lesson shows me how interactive the class was
* The lesson is great where it shows us what should we prepare in the future 
* i am a bit surprised by the fact of getting an exam at the first class
* it also improve our teamwork since group homework is given

# 2024-09-17 #

* it's the first time i know about git repository 
* i think it's quite complicated to use 
* the class today taught us about renewable energy 
* some tips are given to improve our presentation 

# 2024-09-26 #

* NASA's Webb telescope detects tracea of carbon dioxide on the surface of Pluto's largest moon
* Astronomers have detected carbon dioxide and hydrogen peroxide on the frozen surface of Pluto's 
  largest moon, which is called Charon.
* This week talks about SDGS.I learned about 17 different targets of SDGS.
* I think my future will work in a technology company, and I will live just nearby the company
  which I work in. 
* Global warming is an important thing that will change my life. The temperature is soaring 
  , and the ice is melting. So many places will be submerged in the near future. Therefore, I can't
  visit such places in the future. It's sad to think about that.
* High housing prices is also affects me a lot. THe housing prices is too high for people to buy houses.
  We earn approximately 50K per month. However, it costs us at least 10 million NT dollars to buy a new house. 
  We have to save money for 20 years to buy a house, and we haven't factored in daily expenses yet.
  I wish the high housing prices will decrease one day.
* By the way, air pollution may threaten my health. We breathe in lots of PM, which contains dust, dirt, soot, smoke, 
  and liquid droplets. Major sources of air pollution include inefficient modes of transport(polluting fuels and vehicals)
  ,inefficient combustion of household fuels for cooking. Air pollution nay cause different diseases, such as heart disease,
  lung cancer, and stroke. Furthermore, poor air quality increases the risk of stillbirth, miscarriage, 
  and neurological conditions such as cognitive impairment and dementia.We  must reduce air pollution by developing sustainable transport in cities
  or developing market for renewable energies and energy efficiency.
* Depletion of Marine Resources is another current trend affect me life. Due to overfishing, our marine biodiversity is declining.
  I don't want coral reefs vanish in the future.
* My life is getting more and more convenient because of the development of AI. Also, AI is related to my work, too.
  It can enhance our work efficiency by automating routine tasks, such as customer service chatbots,
  personalized recommendations, and smart home devices.In education, AI tools offer tailored learning experiences, while in healthcare,
  they assist with diagnostics and treatment planning.AI-powered applications also improve accessibility, helping those with disabilities. 
  However, the rise of AI raises concerns about job displacement and ethical issues, highlighting the need for responsible development and use.
  
# 2024-10-17 #

* This week I learned anout money and financial.
* I knew what is money, the functions of money, loans, what gives currency its value......
* Also, everyone who presented on the stage presented well.
* I am happy to join the class today.

# 2024-10-24 #

* I think about what is knowledge in class.It is a good problem for me to think.I learn new things and study books every day.
  However, I haven't thought about this problem before. After thinking seriously, I consider knowledge to be something that is helpful to 
  mtself, such as language or how to cook dishes......
* There are 3 stories that affects me.
* Wolf is coming is on of the stories.In my opinion, not lying is the purpose of this story. Everytime I would like to lie, I think of this story.
  It's also a good story for children to read. This story can help them develop a good habit of being honest.
* The second story is The tortoise and the hare. The story is telling about perseverance. Although the hare runs faster than the tortoise, he doesn't insist.
  Finally, he lost the game. I learned that persistence will lead to success.So if I want to give up, I will think of this story.
* The little prince is the story affects me the most. As I grow up, I learn more and more new things. However, I lost my childlike innocence.
  Keep childlike innocence as I grow up is an important thing I learned from the book. 
* Three ways to live healthy:
* The first way is to sleep early and wake up early. This habit is benefit to our heart health and enhance our immunity.
* Thinking positively is the second way. According to report,this habit can reduce stress and anxiety.
* The third way is to eat vegetables and fruits.This habit can lower the risk of chronic and enhance the health of digestive system.

# 2024-11-07 #

* I learned about depression this week. It's really a serious issue.
* When your friends are suffering depression, there are some ways to help them.
* 1.You can let them share their feelings without judgment or advice unless they ask.
* 2.You can encourage them to do gentle activities, such as hiking, creative hobbies, or even mindfulness practices, but don’t pressure them if they aren’t up for it.
* 3.You can also encourage them to search for professional help.Suggesting speaking to a mental health professional.
* Finally, I hope no one suffers from depression.

# 2024-11-14 #

* I learned about six areas of a job new hires.
* I also learned how to spot and help a depressed friend.

# 2024-11-21 #
 
* In order to reduce greenhouse effect, we need to reduce the emission of CO2.
* In my opinion, rreducing greenhouse effect needs every citizens assistance. Not just governments or Greenpeace that are dedicated to the problem.
* Also, I learned how to reach a consensus today.
* I have a good time in today's class.

# 2024-11-28 #

* We discussed the truth of news today. We browse much news every day. However, we don't question the truth of them.
  The news which we browse is in fact edited by reporters.Adding some personal or subjective opinions. 
  If I want to know the original news, I have to find the initial reports or photos which aren't commented by others and read them.
  Using these informations to make my own judgment.
* I also listen to many presentations in the class.All of them present well.
* The wrist alarm is an interesting idea.Also, I am amused by the story the teacher talked. 
* I enjoyed the class today.
    
# 2024-12-05 #  

* I feel happy today.

# 2024-12-12 #

* I present for whole class.
* It's really an amazing experience. Although my English speaking ability is poor, I try my best to present. 
* After this class, I believe that my English ability is improved by this experience.
* If I have a chance next time, I will immediately go to present.  

# 2024-12-19 #

* I learned about 4th industrail revolution in class today.
* The UK is the important country for the 4th industrail revolution.
* Many countries imply manufacturing upgrade schemes to stimulate the economy of their country.
* We also have a debate about why inequality exists.
* I learned a lot in class today.
* Five rules for more successful or more productive
* 1.Continuously Learn and Adapt:Invest time in learning new skills or knowledge that align with your goals and industry trends.
* 2.Optimize Time Management:Limit distractions by turning off unnecessary notifications and creating a dedicated workspace.
* 3.Maintain Discipline and Consistency:Develop habits that support your objectives, such as setting aside specific times for focused work.
* 4.Set Clear Goals:Break larger goals into manageable tasks to maintain focus and motivation.
* 5.Take Care of Your Well-Being:Prioritize physical health with regular exercise, a balanced diet, and sufficient sleep to sustain energy and focus.

# 2024-12-20 #

* A.Successful and Productive
* B.I felt successful because I completed many task today.
* C.I will start my day by keeping working toward my goal.

# 2024-12-21 #

* A.Successful but Unproductive
* B.I felt successful because I went exercising and I had sufficient sleep.However, I felt unproductive because I didn't complete any task today.
* C.I will strike a balance between enough sleep and completing my task.

# 2024-12-22 #

* A.Unsuccessful and Unproductive
* B.I felt unsuccessful because I let negative thoughts affect my motivation. I felt unproductive because I kept postponing tasks and didn’t make meaningful progress on my goals.
* C.Tomorrow, I will practice positive self-talk and commit to completing at least one significant task to build momentum.

# 2024-12-23 #

* A.Successful but Unproductive
* B.I felt successful because I spent quality time with my family and took care of my mental health. However, I felt unproductive because I procrastinated on important tasks.
* C.I will plan my day better by allocating specific time slots for both relaxation and work to achieve balance.

# 2024-12-24 #

* A.Successful and Productive
* B.I felt successful because I stayed focused and avoided distractions while working.
* C.I will start my day by prioritizing my most important tasks first.

# 2024-12-25 #

* A.Successful but Unproductive
* B.I felt successful because I finished many reports and had fun playing with my friends. However, I felt unproductive because I didn’t dedicate enough time to advancing my major projects and let the day slip by without much progress.
* C.I will allocate specific hours for focused work and stick to my planned schedule to ensure steady progress.

# 2024-12-26 #

* A.Successful but Unproductive
* B.Although I finished a major task at work, I wasted the rest of my day procrastinating on less important assignments.
* C.I will prioritize my tasks and allocate time for each to ensure I maintain productivity throughout the day.

# 2024-12-27 #

* A.Unsuccessful and Unproductive
* B.I struggled to stay focused during the day and was unable to complete my tasks. I spent too much time scrolling on my phone.
* C.I will set specific time blocks for work and avoid distractions by putting my phone in another room.

# 2024-12-28 #

* A.Successful and Productive
* B.I completed all my planned tasks, and found time to exercise.
* C.I will continue following my schedule and start every day by listing my priorities.

# 2024-12-29 #

* A.Unsuccessful and Unproductive
* B.I felt overwhelmed by the workload and ended up doing nothing. 
* C.I will write a clear to-do list the night before to reduce stress and keep myself on track.

# 2024-12-30 #

* A.Successful but Unproductive
* B.I managed to finish an important project, but I spent too much time on one task and neglected smaller ones.
* C.I will use a timer to ensure I don’t over-focus on a single task.

# 2024-12-31 #

* A.Successful and Productive
* B.I divided my work into manageable chunks, and completed everything I planned for the day.
* C.I will continue using this balanced approach and reward myself for staying on track.

# 2025-01-01 #

* A.Unsuccessful but Productive
* B.I completed many small tasks but failed to address the most important one. This made me feel dissatisfied by the end of the day.
* C.I will tackle the most important task first before moving on to other activities to ensure better use of my time.

# 2025-01-02 #

* Today, I wrote my obituary as a reflection of the legacy I wish to leave. 
  It read: "A visionary who dedicated their life to advancing humanity through innovation and compassion." 
  Two figures who inspire me are Marie Curie and Elon Musk. Marie Curie achieved groundbreaking discoveries in science through resilience and unyielding curiosity, while Elon Musk reshaped industries with his bold ambition and creativity. 
  Their success stems from perseverance, passion, and the courage to challenge boundaries. I hope to emulate these traits and create meaningful change in my lifetime.

# 2025-01-09 #

* In my opinion, the test is hard for me.
 
* 1.Know the current world demographics
*  Regularly review global demographic data from sources like the UN or World Bank to stay updated on trends.
*  Participate in discussions or workshops about demographic impacts on global issues.
* 2.Ability to use demographics to explain engineering needs
*  Study case studies where population statistics were used to design infrastructure or public services.
*  Practice using demographic data in project-based scenarios to understand its application in engineering contexts.
* 3.Understand planetary boundaries and the current state
*  Read scientific reports on planetary boundaries (e.g., Stockholm Resilience Centre publications).
*  Watch documentaries and engage in group discussions to better understand Earth’s limits.
* 4.Understand how the current economic system fails to distribute resources
*  Analyze articles or books on economic inequality (e.g., “Capital in the Twenty-First Century” by Thomas Piketty).
*  Simulate resource distribution problems in class activities or online tools.
* 5.Be familiar with future visions and their implications, such as artificial intelligence
*  Attend seminars or webinars on emerging technologies like AI, automation, and their societal impacts.
*  Explore hands-on AI tools to understand their applications and limitations.
* 6.Understand how data and engineering enable a healthier life
*  Study real-world applications of health technologies, like wearable devices or hospital systems.
*  Propose a project using data or engineering to address a specific health challenge.
* 7.Know that social relationships give a 50% increased likelihood of survival
*  Join student organizations or community groups to build strong interpersonal connections.
*  Research and present case studies on how social ties positively impact mental and physical health.
* 8.Be familiar with depression and mental health issues
*  Attend mental health awareness workshops or counseling sessions to gain insight.
*  Read materials or watch videos that address mental health topics and coping strategies.
* 9.Know the optimal algorithm for finding a partner for life
*  Participate in discussions or role-play activities on building healthy relationships.
*  Study behavioral sciences or psychology articles about relationship-building strategies. 
* 10.Develop a custom of questioning claims to avoid “fake news”
*  Practice critical thinking by analyzing news articles and identifying biases or inaccuracies.
*  Take part in media literacy training programs or courses.
* 11.Be able to do basic analysis and interpretation of time series data
*  Use tools like Excel or Python to analyze time-series data, starting with simple datasets.
*  Work on group projects that involve real-world time-series problems (e.g., climate data, stock prices).
* 12.Experience collaborative and problem-based learning
*  Form study groups or join team-based projects to solve practical problems together.
*  Reflect on teamwork experiences and identify areas for personal improvement.
* 13.Understand that professional success depends on social skills
*  Attend soft-skills training sessions focusing on communication, teamwork, and networking.
*  Engage in mock interviews or role-plays to enhance interpersonal skills.
* 14.Know that the culture of the workplace affects performance
*  Research company case studies that highlight how workplace culture influences employee satisfaction and performance.
*  Participate in role-play activities to practice adapting to diverse workplace environments.

