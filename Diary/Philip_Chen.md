This diary file is written by Philip Chen E14096122 in the course Professional skills for engineering the third industrial revolution.

---

# 2022-09-14 #

* Since 9/17, an Richter scale 6.4 earthquake happened in Taitung, there are many more earthquake happened in the next few days.
* Especial on the afternoon of 9/18.
* Due to the rapid development of social media, people nowadays are more eaisly to feel anxious.
* I am no exception. Seeing other people's bright appearence, it seems I am so lazy.
* Though being a better person is what I am looking for, the reality seems to hit me harmly.
* People always say "Be yourself. " But who can follow this all the time?

---
# 2022-09-21 #

* I think I will be an architect or an interior designer. It’s so different from what I am studying now. While I was choosing department and university, I choose the latter one. I don’t reject what we have to learn in this program but I’m not interested in these courses, though. When I finish my graduation credits, I’m going to take the class of Planning and Design college. Due to the spread of internet, I can learn lots of things from Google, Youtube, etc. Moreover, sharing my works to others can be more easily, and even I can create a Youtube channel as my source of income. I think I will live in a small but warm apartment since I care about my life of quality. Also because I am a interior designer, I will try my best to fulfill my imagination of what a warm house should be like. I think Youtube may affect me  the most. The salary in this industry in Taiwan is not treated fairly, we have to do more works than other industry but only get the same paycheck. However, if I upload my works to my Youtube channel, I can get more income than work for other people. I will stay in Tainan because of the lower house price. Although I was living in Taipei, the high consumption keep me away. After maybe 20 years while I earn more money or invest the right company I will move to Taipei.


---
# 2022-09-29 #

* I still had difficulties to concentrate. Three hour lecture is too much for me.
* I hope I can just listen what the speaker said without subtitle someday.

---
# 2022-10-06 #


* First one is a well-known fairy tale. It's about the moon.
* Parents always told us do not point to the moon with your finger. Otherwise, your ear will be cut off.
* It made me feel the fear of the moon and really can't point to it.

* Second one
* In ancient times, there were ten "suns" shining on the earth.
* This cause serious damage to the ground, people were not able to plant crops.
* In order to save mankind, Hou Yi bowed and arrowed, shooting at the nine suns. 
* See the sky burst fireballs, fell a three-legged crow. Finally, there is only one sun left in the sky.
* This story made me have a misconception that the sun is close to us.

* Third
* Prometheus was a Titan .In the war between Zeus the giants he had stood on the side of the new Olympiangods.
* Out of the clay he made the first man，to whom Athena gave soul and holy breath.
* Prometheus spent a lot of time and energy in creating the gift of fire.And fire raised man above all animals.
* Later，there held a joint meeting of gods and men.The meeting was to decide what part of burnt animals should begiven to gods and what to men.
* Prometheus cut up an ox and divided it into two parts:under the skin he placed the fresh，and under the fat he put the bones， for he knew the selfish Zeus loved fat.
* Zeus saw through the trick and felt displeased at the Prometheus' favor towards men.
* So in a masterful way he took away the gift of fire from mankind.
* However，Prometheus managed to steal fire from heaven and secretly brought it down tomen.
* Flying into an anger at this unjustified act of rebellion，Zeus let the other gods chain Prometheus to a rock on Mountain Caucasus，where a hungry eagle ever tore at his liver which ever grew again.
* His period of pain was to be thirty-thousand years.Prometheus faced his bitter fate firmly and never lost courage before Zeus.
* At last Heracles made Prometheus and Zeus restore to friend ship，when Heracles came over in search of the golden apple and killed the eagle and set the friend of mankind free.

---
# 2022-10-13 #

* An apple a day, keeps the doctor away. Eat some fruits and vegetable can give your body some vital nutritions.
* Exercise regularly can improve your academic performance and keep you in a good mood.
* Plato: ”well-justified true belief"

---
# 2022-10-27 #

* Today's topic is depression. There are several suicide incident recently.
* I have lots of exam last week, four exam and two quiz in a single week!
* I am glad that I was so positive or maybe I will do some bad things to myself.
* We should care more about the condition of our classmates, friends and relatives.
* Care and companion is the way to heal those in depression.

---
# 2022-11-03 #

* Mental illness has grown in recent years, it means that people are under more pressure.
* One of the reason why employees quit their jobs more frequently is they didn't know how to do the work while empolyeers didn't have clear directions.
* How to live in my own way is a question for me.

---
# 2022-11-10 #

* The wether is getting cold but it still hot in the day time.
* I learn how to live healthy in this class. 
* I was so appreciate those who standed out to present.

---
# 2022-11-17 #
* After 2 times of presentation, I think I speak English more and more smoothly.
* I am truely a phone addict.

---
# 2022-11-24 #
* I have a lot of exam this week...
* Since the begining of this semester, I have never exercise again...

---
# 2022-12-01 #
* A lot of exam again this week :(
* I feel so confused about why did I take so much class this semester...
* It seems our group misunderstand the task last week...

---
# 2022-12-22 #
* A. Unsuccessful and productive
* B. I studied very late last night, so I got up too late in the morning to have the Automatic Control quiz.
*    Though I prepared very well and the quiz was so easy, I am so upset.
* C. I'm going to prepare the quiz next day, and try not to study too late today.

---
# 2022-12-23 #
* A. Successful and productive
* B. I took the quiz of fluid mechanics today, I read it very well last night, so I did well in the test today.
* C. In the following days, I have to prepare two other exam on the next Monday.
*	 I have already wrote my weekend schedule.

---
# 2022-12-24 #
* A. Successful and productive
* B. I finished what I wrote on my weekend schedule, also finished a presentation for the wednesday.
* C. I wish I can stick to the plan so that I won't study too late tomorrow night.

---
# 2022-12-25 #
* A. Unuccessful and productive
* B. I found that I left too much things for today so I can't finish everythind on time.
*	 But I think I still have to finish them today, what a mess.
* C. I think I worth a day off, I didn't write too much things on my schedule.

---
# 2022-12-26 #
* A. Successful and Unproductive
* B. Fortunately, I finished my study well yesterday, and today's exam went on my expectation.
*	 I think I'll pass these two subject!
* C. I shouldn't let down my guard because there is still so many exam waiting for me.

---
# 2022-12-27 #
* A. Unuccessful and unproductive
* B. Today is my freind's birthday and we have celebrated for him.
*	 Unfortunately, we spent too much time on it which make me have less time to prepare the quiz tomorrow.
* C. I think I should stick to the plan I set.

---
# 2022-12-28 #
* A. Unsuccessful and productive
* B. Since I didn't study for today's quiz, I fail to get good grades on it.
*	 But I still get another chance to make up this since professor pick two grades in three quizes. 
* C. I shouldn't let down my guard because there is still so many exam waiting for me.


