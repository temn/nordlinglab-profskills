This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.
This diary file is written by Hsiao-Yu Yang H54121082 in the course Professional skills for engineering the third industrial revolution.

2024-09-12
I got 54 on the exam and was surprised that it's above average.
Being taught entirely in English requires me to stay focused.
I think everyone in this class is good at English except for me.
After listening to the course introduction, I feel I will gain a lot of interesting new knowledge in this class.

2024-09-17
I think the title of the TED Talk shown in class is interesting.
The examples for explaining win-win, win-lose, and lose-lose situations of conflicts are very vivid.
If possible, I also hope to maintain harmony.
From now on, I will work on developing the habit of writing a course reflection diary.

2024-09-26
In today's class, we discussed SDGs.
After watching the TED Talk played in class, I believe that media literacy is a skill that people must have nowadays.
Listen to each group's opinions on whether the world is getting better, and to hear perspectives from different angles, such as transportation issues is interesting.
One group mentioned in their presentation that the trends in life satisfaction vary by region (with satisfaction increasing in East Asia but decreasing in Western Europe). I think this phenomenon is really cool.

TASK 1

As a student majoring in Industrial and Information Management, I envision myself becoming a software project engineer in the future. In 10 years, I will likely still be living in Taiwan, where I’m familiar with and where my family lives, though I might work in other cities. However, what intrigues me more is how the world and my profession will evolve, and how these changes will impact my life.

1. One of the biggest trends is the growing use of AI. AI is advancing quickly, and in the next 10 years, it will become an important part of software development. AI tools will help by automating tasks, improving code, and supporting system design. As a software engineer, I will need to work with AI, focusing on creative and complicated tasks that machines cannot do. This shift will make engineers adapt quickly and learn how to work with AI to stay competitive. AI won’t replace humans, but it will help us do things faster and more efficiently.

2. Another big trend is the move toward fully remote or hybrid work. In 10 years, the idea of "the office" might be very different. I expect to work with global teams through virtual tools, allowing people to communicate easily across time zones. This will give me more freedom to choose where to live, while still working on international projects. It will also require strong communication skills and the ability to work independently. Tools like virtual reality (VR) or augmented reality (AR) might be used for meetings, making remote collaboration feel more like in-person interactions.

3. Sustainability will also play a big role in my future. Climate change will push for reducing carbon emissions and improving energy efficiency, which will become part of technology and software projects. As an engineer, I could help create energy-saving algorithms, reduce the environmental impact of cloud computing, or work on green technology to make industries more sustainable. This trend will also affect the technologies we use, promoting eco-friendly practices in both personal and professional settings.

4. And the cybersecurity will be very important. As the world relies more on digital systems, the risks will grow. In 10 years, cyberattacks will become more advanced, targeting key systems and large networks. Data privacy will be a major concern for governments, companies, and individuals. I will need to ensure that every system I develop is secure, using encryption, access controls, and continuous monitoring. With the rise of smart cities and the Internet of Things (IoT), engineers will need to be extra careful to protect these connected systems.

5. Finally, automation in software development will grow. Automation will take care of more complex tasks like code generation, debugging, and refactoring. This will give engineers more time to focus on decision-making and creative problem-solving. However, it will also mean that engineers must keep learning new skills to keep up with the fast changes in technology. Automation won’t replace engineers, but it will change the role, making it more about innovation and problem-solving while machines handle repetitive tasks.
In conclusion, 10 years from now, my life will be shaped by trends in AI, remote work, sustainability, cybersecurity, and automation. Living in Taiwan, I will probably work in a very collaborative and technology-focused environment, using advanced tools to create secure, efficient, and environmentally responsible software. The future will bring challenges, but it will also give me exciting chances to grow and innovate, allowing me to play a part in shaping the digital world.

TASK 2

news: US military's mission in Iraq to wrap up by 2026

1. The left: Emphasizes the U.S.'s hesitance to provide clear details about its military strategy, focusing on Washington's goal for a long-term security partnership.

2. The center: Points out the political challenges faced by the Iran's influence and portraying the U.S. presence as a political liability.

3. The right: Focuses on the strategic value of countering Iranian influence and presents the shift as a political advantages for the Iraqi Minister.

2024-10-17

Listening to others' visions of their life and work 10 years from now is very inspiring. I hope I can find a clearer goal for my own future.

I was surprised to learn that the annual spike in the amount of New Taiwan dollars is due to people withdrawing new bills for red envelopes during the 
Lunar New Year, and then depositing the money back into banks after the holiday. This was something I never thought of before.

I thought that the money borrowed from the bank came from another person’s deposit and the bank earned interest on it, but after watching the video "Richard Werner: Today’s Source of Money Creation", I discovered that this is not entirely the case.

It's shocking to know that in most country, banks create 97% of the money supply out of nothing through a process known as credit creation.

It is a concern that the poor and middle class used to see the largest income growth in 1980, but by 2014, it is the very affluent (the 99.999th percentile) who experienced the largest income growth. This has led to a widening gap between the rich and the poor.

2024-10-23
It's quite a cute scene when everyone follows the TED Talk speaker, doing simple exercises while engaging in some pep talk.

I really respect Christian Picciolini for courageously sharing his own partholes and the story of once going down a dark path, while also helping more people disengage from extremist movements.

I agree with the saying, 'Hatred is born of ignorance, fear is its father and isolation is its mother.'

A classmate expressed his thoughts about the hatred between China and Taiwan, which made me start to reflect on why I instinctively feel a sense of rejection whenever I hear about China.

We learned in class that Plato considered knowledge to be 'well-justified true belief'.

Three fictional stories:

1. I saw a cafe owner not charging a regular customer for their coffee because he know that the customer would pay next time. This made me realize that whether it's cash or credit, it's all based on trust and this encouraged me to cherish my relationships with those around me and reflect on my own spending habits.

2. I attended a gathering for friends who recently got married. They shared how they overcame challenges together and supported each other. Watching their interaction mad eme reflect on my own relationship, encouraging me to communicate more openly with my partner and create a more healthy relationship.

3. We discussed Taiwan's culture and history in class and everyone shared their family stories and traditions. It made me feel that despite our different backgrounds, we are all working together for the future on this country and even made me want to know more about this community.

2024-11-07

In today’s class, three groups were merged into a 9-person supergroup, and I noticed that my other two group members disappeared.

This quote from the TED Talk resonated with me deeply: "Having feelings isn't a sign of weakness; feelings mean we're human." It's important to learn how to coexist peacefully with our emotions.

When Nikki Webber Allen ended with the words, "Life is beautiful; sometimes it's messy and it's always unpredictable, but it will all be okay when you have your support system to help you prove it," she seemed on the verge of tears. I believe this message will bring great comfort to those going through tough times

I have friends around me who struggle with depression, but whenever I see them post something with negative emotions, I often don't know how to respond. It feels like anything I say might come off as inappropriate. After listening to Bill Bernat's TED Talk, I realized I've been overthinking it. Just like we don't sneeze when talking to someone who’s sick, we don’t need to speak in a sad tone and hide our optimism when talking to someone with depression.

I think Kevin Briggs and other frontline officers who try to prevent suicides are truly admirable. People who want to end their lives may not have the intention of hurting others; they just want to end their own suffering. However, this action indirectly causes a lot of impact, not only on their close friends and family but also on those who try to convince them not to take their lives. The pressure those individuals carry is surely immense.

2024-11-14

The topic of today's presentation was 'A guide for how to spot and help a depressed friend.' A classmate mentioned that when starting a conversation, one should approach with empathy, use open-ended questions, and listen actively.

I found Petter Johansson's TED Talk about the phenomenon of 'choice blindness' very interesting. When faced with a choice, people, even if they don’t get what they truly want, tend to rationalize their decision and convince themselves that it is what they wanted.

Azim Khamisa developed a peace formula that starts with goodwill, friendship, empathy, compassion, and leads to peace.

I deeply admire Azim Khamisa. After his son was innocently shot and killed, he not only managed to rebuild his life but also chose to forgive the killer. He even took the stage with the killer’s grandfather to share their story. Moreover, he mentioned his efforts to help the killer get released from prison and plans to invite him to join him in future talks. All of this is truly unbelievable.

Emily Esfahani Smith calls belonging, purpose, transcendence, and storytelling the four pillars of a meaningful life.

2024-11-21

I agree with Jaron Lanier's statement that we cannot have a society where two people can only communicate if it is funded by a third party with the intent to manipulate them.

In the video "Simon Sinek on Millennials in the Workplace", I think I finally understand why people often say that today's young generation (Millennials) is becoming more fragile, like the so-called "Strawberry Generation." I believe social media has had a significant impact on this.

Nowadays, there are many competitions or systems where everyone gets a prize. Before hearing the speaker's perspective, I thought it was simply a way to encourage more participation. However, after listening, I realized it might actually have negative effects on the whole. For instance, it could devalue the efforts of those who truly work hard, make those who know they don't deserve the recognition feel awkward, or leave people unprepared for the reality that, in the real world, they are not particularly special and can't easily earn a medal or influence like they used to.

In the TED Talk, Cathy O'Neil mentioned, "Algorithms don't make things fair; they repeat our past practices, our patterns, and they automate the status quo. That would be great if we lived in a perfect world, but we don’t."

I believe that big data is a great tool, but we cannot blindly trust it. Humans are inherently biased, and if those biases are encoded into programs, they will only worsen the current situation.

2024-11-27

At the beginning of the class, the professor asked us to put our phones on the desk at the front of the classroom and count how many times we tried to reach for them during the class. Not touching my phone at all for three hours was like a Mission: Impossible task.

I was impressed by this passage: "Today we are more free than ever from the old media gatekeepers who used to control the flow of information, but with freedom comes responsibility: the responsibility to curate our own experience and ensure that this flow does not become a flood, leading us less informed than before we took the plunge."

This statement made me realize the double-edged nature of the information age and reminds me that critical thinking and selective consumption are key to truly benefiting from the information we have.

After watching the video "What REALLY Happened in Amsterdam," it’s clear that authorities must prioritize their citizens’ safety and well-being. Portraying extremist groups as victims while overlooking the harm done to local communities undermines public trust and accountability. Media coverage should focus on truth and responsibility to avoid spreading misinformation and to ensure all citizens are protected.

Especially now, with the rise of social media, spreading unverified information can lead to unimaginable consequences, such as inciting public emotions or escalating chaos.

2024-12-05

It is interesting to hear different groups share the perspectives and viewpoints of news from different countries on the same event.

Johan Rockström mentioned that the Earth is currently facing two major problems: the first is buffering capacity, and the second is the risk of crossing tipping points, both of which are heading in the wrong direction.

There's one consistent theme in the scientific message for humanity: the more we understand Earth's systems, the greater the risks appear to be. After listening to Johan Rockström present and substantiate this perspective, I felt the complexity and fragility of Earth's systems.

Before listening to Kate Raworth's TED Talk, I also naively believed that economic growth is naturally better the larger it becomes. However, blindly pursuing growth can lead to overlooking many issues, such as the widening gap in resource distribution and the imbalance of ecosystems.

We should shift our focus to prosperity and balance in order to overcome the structural dependence on growth.

2024-12-12

One of the groups presented their final project on sleep today. I feel like I haven’t been getting enough sleep lately either.

Today, each group sent a representative to participate in a debate and promote their proposal. The audience was responsible for asking questions, which was quite interesting and helped us quickly understand what each group aimed to do.

I was impressed by the actions proposed by one of the groups—planting plants. Although I know that planting plants can reduce air pollution, it seems challenging to measure air quality effectively.

During the final voting activity, it felt like most of the students were just voting for their own group.

At the end of the class, we watched a video about the Third Industrial Revolution. I feel that while the economy has grown and advanced significantly compared to the past, there are many potential crises lurking beneath the surface.

。241219 Thu

	A. Successful and unproductive.
	
	B. I feel successful because I started this journal and attended every class today. I feel unproductive because I didn’t study outside of class.
	
	C. I should make a study plan.

。241220 Fri

	A. Successful and productive.
	
	B. I feel successful because, although I only had two classes today, I didn’t waste the entire day. I feel productive because I completed all my to-do tasks, including programming homework, cleaning my room, shopping, and exercising.
	
	C. Tomorrow, I’ll get up early to prepare for the final exams coming on Tuesday and Thursday.

。241221 Sat

	A. Unsuccessful and unproductive.
	
	B. I feel unsuccessful because I slept in. I was unproductive because I kept getting distracted by scrolling on my phone.
	
	C. I should turn off my phone and place it somewhere out of reach.

。241222 Sun

	A. Unsuccessful and productive
	
	B. I feel unsuccessful because, during a holiday filled with Christmas events and markets, I was stuck in my room studying. I feel productive because I completed the study plan I scheduled for today.
	
	C. I need to change my mindset.

。241223 Mon

	A. Unsuccessful and productive
	
	B. I feel unsuccessful because I couldn’t answer the questions in today’s Introduction to Computers quiz. I feel productive because I finished reviewing for tomorrow’s final exam.
	
	C. This weekend, I’ll start reviewing for the remaining subjects.

。241224 Tue
	
	A. Successful and unproductive
	
	B. I feel successful because I was able to answer almost all the questions in today’s exam. I feel unproductive because I completely relaxed after the exam and didn’t study at all.
	
	C. Tomorrow, I’ll take advantage of my free time to finish studying for the final exam the day after tomorrow.

。241225 Wed
	
	A. Successful and unproductive
	
	B. I feel successful because I had a great time celebrating Christmas with my friends in the evening. I feel unproductive because I didn’t finish studying the material for the exam.
	
	C. I’ll pull an all-nighter to cram.
	
	
。241226 Thu

A. Unsuccessful and unproductive.

B. I think I failed because I messed up my final exam today. I was unproductive because I went out for late-night snacks with friends after a department event and didn’t have time to study for tomorrow’s quiz.

C. I need to learn to prioritize tasks better.

。241227 Fri

A. Unsuccessful and unproductive.

B. I failed because I overslept and was late for the exam, which I didn’t know how to answer anyway. I was unproductive because I only slept 3 hours in two days, so I went back to my room and slept all day after class.

C. I need to establish a regular sleep schedule.

。241228 Sat

A.Unsuccessful and unproductive.

B. I was unproductive because I spent the day watching Squid Game 2 instead of studying. I failed because I thought it was awful and a waste of time.

C. I should just stick to studying.

。241229 Sun

A. Unsuccessful but productive.

B. I failed because I accidentally spent too much money while browsing online shops. However, I was productive because I finished all my tasks for the day.

C. I need to improve my financial planning.

。241230 Mon

A. Successful and productive.

B. I succeeded because I finally made it through the troublesome general education course, and I stuck it out until the end. However, I was still somewhat unproductive because I haven’t finished my final report for Thursday.

C. I’ll get that report done tomorrow.

。241231 Tue

A. Unsuccessful and unproductive.

B. I failed because I attempted the Spanish New Year tradition of eating 12 grapes and making wishes in 36 seconds but didn’t finish in time, and I was too focused on eating to make any wishes. I was unproductive because I spent the whole night out partying and didn’t get anything meaningful done.

C. I need to use tomorrow’s holiday to catch up on my work.

。240101 Wed

A. Unsuccessful and unproductive.

B. Both my failure and unproductivity were caused by staying up too late last night, leaving me so exhausted that I slept until the afternoon. As a result, I only managed to finish my final report but didn’t study at all.

C. I need to adjust my sleep schedule and plan my holiday time properly, or the final exams will be a disaster.

2025-1-2

Yang Hsiao-Yu, aged 88, dedicated her life to creating wealth and driving economic development, becoming one of the world’s most successful investors and entrepreneurs. Through unparalleled business acumen and a bold entrepreneurial spirit, she built a global business empire and amassed a fortune worth billions. Her name became synonymous with "wealth and success," and her life was celebrated as an inspiring legend.


person 1: Elon Musk

Reasons for Success:
Risk-taking and Innovation: Musk fearlessly embraced high-risk ventures, turning groundbreaking ideas into profitable models, such as the successes of Tesla and SpaceX.
Expanding Business Horizons: He constantly sought to break into new markets, pioneering future industries like space exploration and sustainable energy.

person 2: Warren Buffett

Reasons for Success:
Long-term Investment and Focus: Buffett excels at investing with a long-term perspective, adhering firmly to his strategies, and has accumulated an extraordinary amount of wealth.
Giving Back to Society: He has pledged to donate nearly all his wealth to improve global education, healthcare, and other critical issues.




