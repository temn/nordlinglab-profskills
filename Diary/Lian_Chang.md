# 2023-09-07 #

* I think that the development of technology may lead to wealth inequality.  Becuase those with greater financial resources can access more technological resources to generate more income. However, impoverished people may be hard to escape poverty.
* The climate changing may lead to sea leavel increaseing and some countries have been submerged.  Maybe we will learn what we can do to save the world.
* I got a poor grade in the exam, but I think I did it better than the kitten.
* As the second week unfolded, I encountered my first set of challenges. Balancing the workload of multiple courses and adapting to the rigorous demands of engineering courses presented initial hurdles. However, by seeking guidance from professors and collaborating with peers, I began to grasp the importance of teamwork and problem-solving within the field. This week taught me valuable lessons about resilience and the power of collective effort.

# 2023-09-14 #
* Conflict is not just a matter between individuals but can also becomes a problem between nations.  Therefore, we should listen to each other's voices and find the best solution to solve it. 
* The homework we made last week is to find the reason why world become wealth or poor.  After we did the research, we thought the world won't be wealth because of the inecreasing number of produce.  The reason is the high cost in the produce.

# 2023-09-21 #
* How to use the reference and how to judeg fake news are an important topic for us.  
* The homework we did last week is to show a evidence of a claim that the speecher made in the video.  We find the literacy is increasing in the world and there are many chart can show the evidence.

-Homework 1

In your Diary for this week, write a 400-600 word summary of what you think your future will look like and why it will be so 10 years from now. Start by defining where you will live and what work you will do. Then describe the five current trends of change that you think will affect you the most and their effect on your life 10 years from now.

My Answer:

I think I will be a mechanical engineer because my major is that in the university.  I believe that in the future, my job will involve designing various types of machinery to advance and facilitate society.   And about where I live, I want to live in Japan.  The reason is that I feel that there are more job opportunities in Japan compared to Taiwan, and I really like this country. Therefore, I believe that in the future, I will live in Japan.

The five current trends of change that will affect the most:

* AI will affect my life the most because it has the potential to replace engineers.
* Technological advancements will also impact the future because they will change the way people think and lifestyles.
* Climate change may affect my life a lot becuase people should change their lifestyles to solve the world warming.
* The use of the internet will change my life because the advancement of internet could make various way of communication.
* The trend of emphasis on health will also impact my life, as it will change people's dietary habits and exercise routines.

-Homework 2

* Topic: Taiwan factory fire leaves at least 5 dead, more than 100 injured.
* Site: https://ground.news/article/taiwan-factory-fire-leaves-at-least-5-dead-more-than-100-injured_7c979a

The comparisons:

* "Left sources focus on" the "devastation and loss of life", indicating an emotive perspective.
* "Center coverage" highlights the "impact" and "evacuation" emphasizing more on rescue efforts.
* "Right emphasizes" the "deadly" aspect and gives a timeline, maintaining a descriptive tone.
* The right also underscores "extensive damage" unlike the left and center versions.
 
# 2023-10-12 #
* The theme for this week is about money. Before the explanation of what money is, I was completely unable to answer the question of why money can have value.
* Money can have value because it possess universally recognized and equivalent attributes, and also have qualities that make it easy to carry.
* The primary revenue source for banks is the net interest income, where banks lend money to customers at a higher interest rate and pay lower interest rates to deposit customers.
* I believe that understanding the essence of money and using it appropriately is crucial for us.
# 2023-10-19 #
* In the first video, I think this main idea is the most impressive thing for me: Democracy is not an instinct of human nature; it is merely a sensation. This makes democracy vulnerable when power and technology converge, and it is controlled by data. Therefore, we should understand our weaknesses and avoid being exploited by democracy's adversaries.
* Moreover, I think that Closed-mindedness restricts our senses, preventing us from seeing other ideas and thoughts. Therefore, we should actively engage with diverse perspectives to avoid falling into the trap of closed-mindedness.
* This week marked the beginning of a new semester, and I found myself immersed in the world of mechanical engineering. From the introductory lectures to meeting fellow students, the excitement of diving into the intricacies of machinery and design became palpable. Navigating through the course syllabus, I look forward to the challenges and growth this semester promises.

# 2023-10-26 #
* The question in the class: pay everyday for staying healty or pay for getting sick?
      * I think I would choose pay when getting sick and pay for it.  Becuase  don't think Taiwan has enough medical resources to allow everyone who pays to maintain their health. 
      * I would choose to pay when I get sick and regularly undergo health check-ups to maintain my health on my own.
* Medical resources have a significant contribution and impact on society, and improving them will be a future challenge for humanity.

# 2023-11-2 #
* The points I learn from today's class are:
      * There are many sources of stress, which can stem from factors like one's social status or racial discrimination. However, the most crucial thing is not to keep your depression silent. You should confront your condition and seek resolution.
      * Everyone experiences moments of sadness, and there's no need to specifically address sadness when communicating with someone who has depression. Simply engaging in everyday conversations can help them feel trusted, and it's essential to ensure they feel respected and comforted during these interactions.
      * When dealing with someone who is contemplating suicide, it's important not only to talk to them but also to listen and understand their thoughts.

# 2023-11-9 #
* In today's class I learn about jobs:
       * a company's attention to the performance of processes determine future success or failure.
       * The colleagues determine the effectiveness of team collaboration, so it's advisable to establish friendships with them when entering a new environment.
* Violence is a learned behavior, so we should focus on cultivating self-reflection, confidence, and fostering positive interactions with others.
* Maintaining kindness, trust, empathy, compassion, and peace can foster the creation of friendships.

# 2023-11-16 #
* Today's class focused on reviewing everyone's first final presentation. Our group addressed the topic of education and student rights. The teacher's comments highlighted areas we hadn't paid attention to before, such as the presentation of data and the formatting of the slides. We will take these comments into consideration and make improvements in these aspects for our next presentation.
* As the month concludes, I reflect on the strides made in my understanding of mechanical engineering. The combination of theoretical knowledge, collaborative efforts, and hands-on experience has significantly contributed to my growth. This week highlighted the importance of continuous learning and adaptation in the ever-evolving realm of engineering. Looking ahead, I am excited about the upcoming challenges and the opportunities they present for further development.
# 2023-11-23 #
* Through the presentation I found that noise pollution indeed has physical and psychological impacts, and it is essential for the government to implement relevant measures to prevent noise pollution. Citizens, too, should be mindful of whether they are causing inconvenience to others by generating noise.
* In the video, the speaker mentions that the current generation of young people is facing challenges in coping with societal pressures due to differences between family and social life. I believe receiving encouragement during the learning process is a valuable experience, but it is important not to become excessively complacent or arrogant.

# 2023-11-30 #
* This week is the group presentation, and I am part of the second group, our topic is how to enhance student empowerment, and there are two problems in our presentat: 
       * Firstly, our presentation consists of two aspects: providing practical examples and addressing the issues related to analyzing actions, as well as considering real-life situations.
       *  Secondly, I believe that implementing our ideas for change in Taiwan may pose challenges. However, starting on a smaller scale could allow us to assess the feasibility based on partial results and determine whether it is necessary to continue with the initiative to bring about positive change.
* Education in Taiwan is quite entrenched, and while we may not currently have a definitive solution, at least we have identified the issues within the education system.

# 2023-12-7 #
* The news cannot have completely impartial content in every region or country. I believe this is true because news is written by people, and emotions can influence the way news is portrayed.
* I still think that this type of news is much better than fake news because it is biased due to real content, rather than reporting false information to the public.
* We are finding it challenging to execute all three actions in our group, so we are still searching for suitable solutions.

# 2023-12-14 #
* Today's agenda mainly involves listening to presentations and reviews from everyone. I am a member of the second group.  Our previous reports were rejected several times due to implementation difficulties. The proposal we presented this time has finally been approved, and we hope to achieve tangible results by the end of the semester.
* Our report has shortcomings in the evaluation of the charts. We will work on supplementing and improving them later.
* Today's video covered topics related to climate change and technological development. It seems that it will also discuss what we need to do in the context of the Third Industrial Revolution. However, I haven't finished watching the entire video yet due to its length. I will watch the rest later.

# 2023-12-21 #
* The task in this week is to record that how successful and productive I felt each day.
* 231221 Thu.
      * Unsuccessful and unproductive
      * I felt unsuccessful because I feel like I messed up my final exams, and the grades I received during the review of the exam paper were not satisfactory.
      I felt unproductive because I fell asleep in the class.
      * I think I should take a break at the noon to relax and adjust my mindest.
* 231222 Fri.
      * Unsuccessful and unproductive
      * Today, I messed up another quiz, and I took too long of a break at noon, accomplishing nothing productive.
      * I can only strive to prepare for the exams more diligently next time and try my best not to become complacent.
* 231223 Sat.
      * Successful and productive
      * I woke up early today and prepared a delicious breakfast for myself, so I feel successful throughout the entire day. I also went to a coffee shop to study, so I feel more productive in getting things done.
      * I should maintain the habit of waking up early and not indulge myself for too long.
* 231224 Sun.
      * Successful and unproducitve
      * I have final exams next week, but my progress in studying has been slow, so I don't have much productivity. 
      * But I feel a great sense of accomplishment, so I have a feeling of success.
      * I believe I can adjust my study methods to not only experience a sense of achievement but also increase productivity.
* 231225 Mon.
      * Unsuccessful and unproductive
      * I took the final exam today, but the results were not ideal, so I feel a lack of success.
      * Because I didn't do well on the exam today, my mood is very low, and I feel very unproductive.
      * I feel that in the future, I must adjust my mood and face exam results more positively.
* 231226 Tue.
      * I forgot to record this day, so I can't give the comment.
* 231227 Wed.
      * Unsuccessful and productive
      * I completed the final exam for one of my courses today and feel that the performance was not satisfactory, so I consider it unsuccessful.
      * However, I engaged in physical activity today and also submitted other reports, so I feel very productive.

# 2023-12-28 #
* Today's report was a group project, and it served as the final summary before the video presentation at the end of the semester. After this, everyone will start filming the video, and I hope everything goes smoothly to conclude successfully.
* 231229
      * Unsuccessful and productive
      * I finished a quiz today and feel that the performance was not ideal, so I consider it unsuccessful.
      * But this afternoon, I worked on organizing some final reports, so I feel productive.
* 231230
      * Successful and productive
      * I woke up early today and went to a coffee shop for breakfast and some studying, so I feel very successful and accomplished.
      * I hope I can maintain this level of dedication in the future.
* 231231
      * Successful and productive
      * I also woke up early today, so my schedule is similar to yesterday's.
* 240101
      * Successful and Unproductive
      * Today is the start of a new year. Although I stayed up late last night for New Year's Eve, I managed to wake up early today, so I feel successful.
      * But I feel that my study efficiency wasn't very good today, so I consider it unproductive.
      * I feel great today, even though my productivity wasn't high. I have a sense of accomplishment, and perhaps I should maintain this positive mindset when facing various things in the future.
* 240102
      * I've been taking exams all day today, so I haven't been able to keep a record.
* 240103
      * Successful and Unproductive
      * I feel that I did well on the exam today, so I consider it quite successful.
      * I studied very diligently today, so I feel a great sense of accomplishment.
      * I believe the positive feelings I have today stem from the tangible results in my studies and exams. To ensure success and productivity, it's important to achieve concrete outcomes and maintain efficiency, creating a sense of accomplishment.

