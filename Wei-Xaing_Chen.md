# 2024-09-12 #
* The first lesson shows me the mean of the third industrial revolution
* We had an exam during the class
* It's really depress that I know how ignorance I am by getting the low score in the exam
* I'm little bit look forward to the next lecture

# 2024-09-19 #
* It is a littile pity that I didn't get the chance to presentation
* We watched a TED talk : "Is the world getting better or worse? A look at the numbers"   
* With TED video, I know that is the world become better or not actully can be analysis on data
* Steven Pinker's speech is impressive, "the progress is problem solve, not miracle"

# 2024-09-26 #
* Doing the presentation I realize that the world is actaully become better but not enough,there are some problem people need to get through it together
* We discussed SDGs in today's class
my future ten years from now:
After being admitted to a public bank, becoming a supervisor

# 2024-10-03 #
* Took a day off because of the typhoon situatiion

# 2024-10-10 #
* Holiday

# 2024-10-17 #
* We discuss some topics around money, currency creation, income distribution, recessions and so on.
* Although I am an economics student, I still learned a lot of financial-related knowledge.   
* I gained a deep understanding of the need for financial market supervision and prudence.
Money Creation in the Modern Economy by Ryland Thomas:
The video explains how the majority of money in the modern economy is created by commercial banks making loans.
There is a common misconception that contrary to popular belief, banks do not simply act as intermediaries lending out deposits that savers place with them. 

# 2024-10-24 #
* During this course, I understood the importance of proper exercise to health.
* In my opinion the movie "A.I. Artificial Intelligence" may become true in the future
Three fictional stories:
1.When working in a restaurant, I saw colleagues helping each other to make the overall work smoother, which made me understand the importance of division of labor, cooperation and mutual help.
2.When riding a motorcycle on the road, seeing drivers and pedestrians being courteous to each other not only makes traffic flow smoother, but also makes both parties feel better. I learned the importance of politeness
3.A man was driving at night and passed an intersection. The yellow light had turned into red. He thought there was no car anyway, so he sped over and was stopped by the police. The policeman asked him: "Didn't you see the red light?" "Yes!" he replied. "Then why did you still run through the passing lights?" the policeman asked again. He said: "Because I didn't see you!"I understand that I cannot make excuses or reasons to deceive myself into rationalizing what is right or wrong.

# 2024-10-31 #
* Took a day off because of the typhoon situatiion

# 2024-11-07 #
1.How to recognize when a friend is depressed?
Spending more time sleeping than usual but still feeling tired;
difficulty falling asleep;
Loss of appetite or eating more than usual;
Loss of interest in things you used to like;
Have difficulty understanding and communicating with others, and difficulty participating in class discussions;
Unable to attend class;
Academic performance deteriorates;
Unable to complete homework on time;
Become less concerned about personal hygiene and appearance;
Physical discomfort, such as: headache, chest pain, stomachache, fast heart rate or disorder;
Becoming irritable and irritable when interacting with family or friends;
Prone to conflicts or aggressive behavior with family or friends;
Cut off contact with family or friends;
Spend more time with friends who are also depressed;
Refusing to meet new friends or participate in group social activities, and only interact with a few friends whom he knows well;
Avoid talking about important things about the future, such as advancement and career plans;
Drinking more alcohol or abusing drugs.
2.What to say to a depressed friend?
Don't comfort them with "Don't be nervous" or "Take it easy"...
Don't say things that can't be implemented concretely...
Don't use your own experience to deny the experiences of your relatives and friends...
"Positive" words and deeds should be avoided as much as possible...
Don't pretend to be relaxed or deliberately understate...
Don't comfort each other with "facts"...
Don't say things like "You are in the midst of blessings and don't know how blessed you are"...
Convert "should" into "can"
3.How to help a friend with depression?
The companion must first prepare his or her own state and confirm the patient's state, wishes and needs.
Show goodwill, regular concern or invitations, and at the same time give space and flexibility, do not force the other party to respond, and avoid putting too much pressure on others.
Adhere to the attitude of patient listening and empathetic companionship, replace suggestions with empathy, and replace criticism and negation with quiet listening.
Accompanying you slows down, stays away from stress sources, and increases the other person’s choice and sense of control.

# 2024-11-14 #
* We continue discuss the depreesion
* the TED Talk helped me understand the importance of living with emotions
6 great ways to get along with your depressed friend
Companionship: Companionship is the most important thing during a friend's depression. Be there for them, make them feel cared for and supported, and don't let them feel alone.
Listen: Listen patiently to your friends and let them describe their inner feelings and emotions. Don’t interrupt or try to give advice, just listen to understand and make them feel included.
Give support: Give your friends positive encouragement and positive support. Increase their confidence by letting them know you believe they can get through this difficult time.
Encourage seeking medical attention or contacting a professional: Depression is a mental illness that requires professional treatment. Encourage your friend to seek medical help or help them connect with a mental health professional so they can receive appropriate treatment and support.
Timely help: When your friends are depressed or unable to take care of themselves, take the initiative to provide help, such as helping with daily affairs or housework, so that they feel cared for.
Don’t blame or denigrate: Never blame or denigrate your friends as this can add to their guilt and emotional distress. Face them with understanding and concern, and provide warm support.

# 2024-11-21 #
* Today’s lecture discusses algorithms and big data
* Advantages of big data
1. Commercial marketing
With the arrival of the era of data economy, big data has become the key for enterprises to compete in the market. e-commerce industry through
Analyze consumers' digital footprints and behavioral patterns, conduct age-based stratified marketing for consumers, and increase sales improvement and completion rates.
In the past, business executives’ judgments when making decisions were based on personal experience and intuition. “But with big data
In this era, this kind of decision-making method that relies on "people" is easily caused by managers being overconfident or ignoring market changes." (Data
Bit Times, 2016), resulting in less than expected results. Big data analysis can be used in various aspects, such as: sales
volume, consumer group and age, analyze operating costs, etc.
Analyzing product customer base and predicting future possibilities are top priorities in commercial marketing. Both of these can be combined with big data.
Connect and collect all aspects of consumer data from enterprises, analyze text and sensor data, and establish
Data model makes predictions. For example, Walmart has used big data to conduct text analysis, machine learning and synonyms
Dig and accurately predict which product will sell well.
2. Public security
"Big data has now been widely used in the process of security law enforcement" (School of Financial Management, National Sun Yat-sen University
Department, 2015). A famous example is the use of big data by the US National Security Agency to combat terrorism and even monitor
control people's lives. Not only that, in the past, the police could only rely on experience to judge cases, but if a big data database is established,
A suspect's basic information, criminal record and network can be easily controlled.
In addition, big data is also used to reduce the occurrence of crime, such as introducing data analysis technology into the police system,
Using big data analysis technology to analyze the data of each case and improve the ability to predict beforehand can effectively reduce the incidence of crime.
born. For example, the New York City Police Department analyzes relevant data through the data collected and reported every day, determines the causes and patterns of crimes, and provides reference for police deployment in each jurisdiction. In our country, many counties and cities have also used big data technology
Use technology to build an information security information platform to assist the police in handling cases and improve the efficiency of police case handling.
3. Medical treatment
In the past, it was difficult to connect big data and medical care. However, today’s aging society requires medical care services.
The number of patients is increasing rapidly. With the shortage of medical staff, big data combined with medical technology has become a solution to these problems.
one. At present, there are five main application areas of big data in medical treatment, including precision medicine, auxiliary diagnosis, pharmaceutical
drug research and development, medical imaging and health management.
Big data analysis can decode DNA in a very short time and formulate the most appropriate treatment plan. It can not only analyze
and predict the patient's condition, it can also enable patients to receive better treatment and care, and can be applied in tracking, analysis and recording.
Record the patient's condition and predict their symptoms to help doctors rescue patients more efficiently.
In addition, big data also makes a great contribution to controlling the spread of infectious diseases, such as the U.S. Centers for Disease Control and Prevention
The use of big data to combat the Ebola virus and other epidemic diseases is a well-known example. This prevention center integrates population data, health statistics and population movements in affected areas to track epidemic diseases and thereby control the epidemic.
4. Transportation
In recent years, the government has actively promoted the construction of public transportation, and integrated big data technology into basic transportation construction to enable
The intelligent transportation industry is gradually emerging.
The intelligent transportation system consists of multiple systems, including advanced vehicle control systems, electronic road toll systems,
Commercial vehicle operating systems. Utilize the information provided by vehicles in the smart transportation system to collect and analyze vehicle information in real time
information to inform the general public about traffic jams, which can improve the efficiency of the transportation system and achieve smart transportation.
purpose.
In addition to intelligent transportation systems, big data analysis is also widely used in the transportation field, from basic management systems to
systems, such as car navigation systems, traffic signal control systems, automatic license plate recognition technology, and monitoring systems.
We can see its traces and are inseparable from our lives.
* Disadvantages of big data
1. Data accuracy
In an era where data is becoming more and more complex and diverse, the accuracy of data is a very important issue.
Small mistakes may cause irreparable losses. For example, when a doctor makes a mistake before treating a patient,
First use the appropriate method of big data analysis, but if there are any mistakes in the process, such as system calculation errors
Or missing a step, etc., leading to the worsening of the disease or even life-threatening consequences. Such consequences are difficult to cure.
borne.
In addition, the information on the Internet can be modified at will, so we cannot guarantee the knowledge we obtain.
Knowledge must be correct. Therefore, in a world where data is huge, the ability we need is to analyze data.
For correctness, search from multiple sources and interpret it from different angles. For example, like the surgical problems mentioned above, before analysis
It is necessary to collect complete information from the past to avoid any mistakes in the calculation and analysis process.
2. Privacy
“It is impossible to hide or remain anonymous in the era of Big Data” (Nano, 2017). When we join a
When you become a member of a website, you often need to provide personal information such as your name, phone number, address, and email.
At this time, we have sold our personal information; in addition, when we use Facebook to "check in", in fact, our
Your whereabouts and preferences have been recorded and analyzed, and there may even be unscrupulous people selling personal information. For example, when you turn on
When using LINE, you will always receive some inexplicable friend invitations, and these invitations are linked to your account through your mobile phone number. This is evidence that our mobile phone number is exposed.
In addition, through the comprehensive analysis of big data, our preferences, frequently visited places and frequently visited websites will be
Once recorded, companies can even use this information to display ads on web pages related to our browsing history and preferences.
The content of these ads will also change as we change the content.
Therefore, before we accept a request from a website or software to provide our personal information, we must also review our behavior.
Be responsible for something. On the other hand, in order to prevent these websites from violating the rules and infringing on our privacy, the government
The government also needs to actively formulate legal norms and restrictions.
3. Data dictatorship
Big data is a convenient and efficient tool, but if we rely too much on it, it may have negative consequences
The impact is the "data dictatorship problem", which is no less than the benefits brought by big data. "Data dictatorship refers to allowing data to
We use data to control us and are blindly restricted by the results of analysis, leading to the abuse or misuse of data. ” (Li Xinyi, 2015)
Big data helps us improve our lives, but data is not everything. Don’t rely too much on data and lose your humanity.
That is to say, the application of big data is more likely to bring about potential discrimination problems. For example, when using data analysis, it may be inadvertent.
Unintentionally classifying groups of people, but this may label and stigmatize individuals or ethnic groups, or even be regarded as discriminating against a certain group of people.
some ethnic groups.
4. Excessive reliance on big data
Big data has two sides. Proper use can help enterprises create benefits, but improper use may cause problems for enterprises.
losses. Some people are too superstitious about big data. Even if the data is objective and scientific, it still cannot prevent certain harmful events.
Unscrupulous people use data to mislead or deceive the public, which will lead to irreparable and serious consequences. And big data
Although future possibilities can be foreseen, innovation and change are unpredictable. If all companies rely on big data
When making decisions, sometimes the data is too large, which may lead to paradoxical phenomena and even stifle new ideas. The most important thing is
Unfortunately, big data may not necessarily be able to adjust to emergencies. For example, the British retail industry Tesco
Failure caused by excessive reliance on big data and the degradation of creativity brings a major warning to the retail industry.
Another thing that deserves our attention is the "hidden concern of dehumanization." If we completely follow the results obtained from big data,
Analyze the results to infer whether others will commit crimes in the future, and the police can prevent them from committing crimes before they commit crimes.
To arrest him for any reason can be said to be a violation of his human rights and a trampling on human dignity.

# 2024-11-28 #
* We conducted an experiment on cell phone addiction where we needed to keep our phones in the foreground and track how often we used them.
* The TED talk made me understand the dangers of fake news
* How to identify fake news
1. Be skeptical of headlines: Fake news/fake news usually uses sensational headlines. If the content of the headline is unbelievable, it is likely to be a false report.
2. Investigate the source: If the report comes from an unfamiliar institution or organization, please investigate their details and learn more background information before judging the authenticity of the information.
3. Check related reports: If multiple credible sources report the same content, the news content is more likely to be true.
4. Only share credible news: Do not share news/information online before confirming its authenticity.
5. Make good use of various false information verification channels (such as the "LINE Message Verification" account officially provided by LINE, Trend Micro's "Fraud Prevention Expert", etc.), compile the information provided by all parties, and then verify the authenticity of the news/information Make judgments.

# 2024-12-05 #
* South Korea's sudden martial law has become the most discussed topic in class
* I was corrected by the professor when I went on stage to report, and I also learned more ways to make presentations correctly.
* Johan Rockström's TED talk "Let the environment guide our development". Human growth has strained the earth's resources, but as Johan Rockström reminds us, our advances also give us the science to recognize this and change behavior. His research has found nine "planetary boundaries" that can guide us in protecting our planet's many overlapping ecosystems.

# 2024-12-12 #
* We wacth th TED talk which speak by Jeremy Rifkin’s documentary, “The Third Industrial Revolution: A Radical New Sharing Economy.”
* The speech about the importance of cooperation and practical solutions to environmental problems reminded me of my visit to Sharon Green Energy City in another class last month. During the visit, I also understood the urgent need between environmental sustainability and technological progress.

# 2024-12-20 #
* unsuccessful + unproductive
* I accomplished nothing
# 2024-12-21 #
* unsuccessful + unproductive
* I'm useless
# 2024-12-22 #
* unsuccessful + unproductive
* I have no merit
# 2024-12-23 #
* unsuccessful + unproductive
* I'm a mess
# 2024-12-24 #
* unsuccessful + unproductive
* I'm a loser